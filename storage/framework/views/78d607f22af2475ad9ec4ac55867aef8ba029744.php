<?php $__env->startSection('content'); ?> 
<!-- CKeditor start here-->
<?php echo e(Html::script('js/admin/ckeditor/ckeditor.js')); ?>

<script type="text/javascript" src="<?php echo e(asset('js/admin/ckeditor/ckeditor.js')); ?>"></script>
<section class="content-header">
	<h1>
		<?php echo e(trans("Add New Checklist")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/platform')); ?>"><?php echo e(trans("Checklist")); ?></a></li>
		<li class="active"><?php echo e(trans("Add New Checklist")); ?> </li>
	</ol>
</section>
<section class="content"> 
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/checklist/add-checklist','class' => 'mws-form','files'=>'true'])); ?>

	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('title')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('title', trans("Title"), ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text('title','', ['class' => 'form-control ','placeholder'=>'Title','id'=>'title'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('title'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('description', trans("Description"), ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::textarea("description",'', ['class' => 'form-control textarea_resize','id' => 'body' ,"rows"=>3,"cols"=>3])); ?>

					<script type="text/javascript">
					/* For CKEDITOR */
						CKEDITOR.replace( <?php echo 'body'; ?>,
						{
							height: 250,
							width: 507,
							filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
							filebrowserImageWindowWidth : '640',
							filebrowserImageWindowHeight : '480',
							enterMode : CKEDITOR.ENTER_BR
						});
							
					</script>
				</div>
			</div>
		</div>
	</div>

	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
			<a href="<?php echo e(URL::to('admin/checklist/add-checklist')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans("Reset")); ?></a>
			<a href="<?php echo e(URL::to('admin/checklist/')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans("Cancel")); ?></a>
		</div>
	</div>
	<?php echo e(Form::close()); ?>

	
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/checklist/add.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end" style="background-image: url(<?php echo e(asset('img/inner_banner.png')); ?>);">
        <div class="container py-3">
            <h2 class="block_title"><small>MyClubtap</small>Help &amp; Support</h2>
        </div>
    </section>
    <section class="chooseus_block pb-5">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-12 col-lg-8 pt-4 pt-md-0">
                    <div class="help_support_form_sec p-4">
                        <script type="text/javascript" src="https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js"></script>
                        <style type="text/css" media="screen, projection">
                        @import  url(https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.css); 
                        </style> 
                        <iframe title="Feedback Form" class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://myclubtap.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&screenshot=No&captcha=yes" scrolling="no" height="500px" width="100%" frameborder="0" >
                        </iframe>
                    </div>
            	</div>
                <div class="col-12 col-md-4 offset-md-4 col-lg-4 offset-lg-0 d-flex">
                    <div class="w-100 reachus_block my-4">
                        <ul class="reachus_sec border-bottom p-3">
                            <li class="heading_title mb-3">Reach Us</li>
                            <li class="mb-3"><a class="w-100" href="tel:<?php echo e(Config::get('Site.contact_number')); ?>"><img class="mr-2" src="img/phone.svg" alt="phone"> <?php echo e(Config::get('Site.contact_number')); ?></a></li>
                            <li class="mb-3"><a class="w-100" href="mailto:<?php echo e(Config::get('Site.contact_email')); ?>"><img class="mr-2" src="img/mail.svg" alt="email"> <?php echo e(Config::get('Site.contact_email')); ?></a></li>
                            <li><img class="mr-2" src="img/location.svg" alt="location"> <?php echo e(Config::get('Site.address')); ?></li>
                        </ul>
                        <ul class="reachus_sec border-bottom p-3">
                            <li class="heading_title mb-2">Knowledge Center</li>
                            <li class="articles_btn"><a href="">Articles</a></li>
                        </ul>
                        <ul class="reachus_sec border-bottom p-3">
                            <li class="heading_title mb-2">Connect Now</li>
                            <li class="mb-2">
                                <ul class="social_links d-flex justify-content-center justify-content-md-start flex-wrap">

                                    <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" href="<?php echo e(Config::get('Social.facebook')); ?>" target="_blank" title="facebook"><i class="fab fa-facebook-f"></i></a></li>

                                    <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" href="<?php echo e(Config::get('Social.twitter')); ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a></li>

                                    <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" href="<?php echo e(Config::get('Social.instagram_url')); ?>" target="_blank" title="instagram"><i class="fab fa-instagram"></i></a></li>

                                    <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" href="<?php echo e(Config::get('Social.youtube_url')); ?>" target="_blank" title="youtube"><i class="fab fa-youtube"></i></a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="reachus_sec p-3">
                            <li class="heading_title mb-2">Schedule Call</li>
                            <li><a href="https://calendly.com/vickyatmyclubtap/myclubtapintroforgameadminsin30mins" target="_blank"><img style="max-width: 1.8rem;" class="mr-2" src="img/schedule.png" alt="schedule"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    function contact_us() { 
    	$('#loader_img').show();
    	$('.help-inline').html('');
    	$('.help-inline').removeClass('error');
    	$.ajax({ 
    		url: '<?php echo e(URL("contact-us")); ?>',
    		type:'post',
    		data: $('#contact_form').serialize(),
    		success: function(r){
    			error_array 	= 	JSON.stringify(r);
    			data			=	JSON.parse(error_array);
    			console.log(data['success']);
    			if(data['success'] == 1) {
    				window.location.href	 =	"<?php echo e(URL('contact-us')); ?>";
    			}else { console.log(data['errors']);
    				$.each(data['errors'],function(index,html){
    					$("#"+index).next().addClass('error');
    					$("#"+index).next().html(html);
    				});
    				$('#loader_img').hide();
    			}
    			$('#loader_img').hide();
    		}
    	});
    }
    
     $('#contact_form').each(function() {
    	$(this).find('input').keypress(function(e) {
              if(e.which == 10 || e.which == 13) {
    			contact_us();
    			return false;
               }
           });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/cms/contact_us.blade.php ENDPATH**/ ?>
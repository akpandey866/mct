<?php $__env->startSection('content'); ?>
<style type="text/css">
    .owl-carousel .owl-item img {
        display: block;
        width: 100%;
        color: black;
        height: 100%;
    }
</style>
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end pb-3" style="background-image: url(<?php echo e(asset('img/inner_banner.png')); ?>);">
        <div class="container py-3 py-lg-4">
            <h2 class="block_title"><small>About</small>MyClubtap</h2>
            <!--<a href="" class="btn py-2 px-5">Join Now</a>-->
        </div>
    </section>
    
    <section class="chooseus_block pt-0 pt-lg-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12 pt-4 pt-md-0">
                    <?php echo $result->body; ?>

                </div>
            </div>
        </div>
    </section>
    
    <section class="platform_block py-5" style="background-image: url(img/platform_sec_bg.jpg);">
        <div class="container">
            <h2 class="block_title mb-4"><small>Our</small>Platform</h2>
            <div class="row">
                <?php if(!$platform->isEmpty()): ?>
                    <?php $__currentLoopData = $platform; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-12 col-sm-6 col-md-4 my-3">
                        <div class="register_list_sec d-flex align-items-center">
                            <div class="w-100 text-center position-relative p-4">
                                <div class="register_list_icon d-inline-block mb-4" style="background-image: url(<?php echo e(PLATFORM_IMAGE_URL.'/'.$values->logo); ?>);">&nbsp;</div>
                                <div class="register_list_heading text-uppercase mb-3"><?php echo e($values->title); ?></div>
                                <div class="register_list_text"><?php echo e($values->description); ?></div>
                            </div>
                        </div>
                    </div> 
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

            </div>
        </div>
    </section>
    
    <section class="partners_block py-5">
        <div class="container">
            <h2 class="block_title my-4"><small>Clubs or Leagues</small>We Serve</h2>
            <div class="partners_sliders owl-carousel">
                <?php if(!$serve->isEmpty()): ?>
                    <?php $__currentLoopData = $serve; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item">
                        <div class="partners_slide_sec d-flex align-items-center justify-content-center p-3">
                            <div class="line_effect_top_right">&nbsp;</div>
                            <img src="<?php echo e(SERVE_IMAGE_URL.'/'.$values->image); ?>" alt="partner">
                            <div class="line_effect_bottom_left">&nbsp;</div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    
    <section class="partners_block py-5" style="background-color: #f1f1f1;">
        <div class="container">
            <h2 class="block_title my-4"><small>Our</small>Partners</h2>
            <div class="partners_sliders owl-carousel">
                <?php if(!$partner->isEmpty()): ?>
                    <?php $__currentLoopData = $partner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="item">
                        <div class="partners_slide_sec d-flex align-items-center justify-content-center p-3">
                            <div class="line_effect_top_right">&nbsp;</div>
                            <img src="<?php echo e(PARTNER_IMAGE_URL.'/'.$values->image); ?>" alt="partner">
                            <div class="line_effect_bottom_left">&nbsp;</div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    
    <section class="say_about_block py-5">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 col-xl-5">
                    <h2 class="block_title"><small>What our users</small>Say About Us</h2>
                </div>
                <div class="col-12 col-lg-6 col-xl-7">
                    <div class="say_about_sec p-5 mt-5 mt-lg-0">
                        <div class="say_about_client_slider owl-carousel">
                            <?php if(!$userSliders->isEmpty()): ?>
                                <?php $__currentLoopData = $userSliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="item">
                                    <div class="say_about_client mx-auto mb-4"><img src="<?php echo e(USER_SLIDER_IMAGE_URL.'/'.$values->image); ?>" alt="client"></div>
                                    <div class="say_about_client_review text-center mb-4"><?php echo !empty($values->description) ? $values->description :''; ?></div>
                                    <div class="say_about_client_name text-center"><?php echo e($values->name); ?> <span><?php echo e($values->club_name); ?></span></div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="our_team_block py-5" style="background-color: #f1f1f1;">
        <div class="container py-4">
            <h2 class="block_title"><small>Our</small>Team</h2>
            <div class="our_team_slider owl-carousel">
                <?php if(!$teamSliders->isEmpty()): ?>
                    <?php $__currentLoopData = $teamSliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="item">
                            <div class="our_team_list py-5 px-3">
                                <div class="line_effect_top_right">&nbsp;</div>
                                <div class="say_about_client mx-auto mb-4"><img src="<?php echo e(TEAM_SLIDER_IMAGE_URL.$values->image); ?>" alt="client"></div>
                                <div class="say_about_client_name text-center mb-3"><?php echo e($values->name); ?> <span><?php echo e($values->position); ?></span></div>
                                <div class="say_about_client_name text-center mb-3"><?php echo e($values->description); ?></div>
                                <ul class="our_team_social d-flex align-items-center justify-content-center">
                                    <li class="mx-1"><a href="<?php echo e($values->facebook); ?>" target="_blank" title="facebook"><img src="<?php echo e(asset('img/facebook.png')); ?>"></a></li>
                                    <li class="mx-1"><a href="<?php echo $values->twitter;?>" target="_blank" title="twitter"><img src="<?php echo e(asset('img/twitter.png')); ?>"></a></li>
                                    <li class="mx-1"><a href="<?php echo e($values->instagram); ?>" target="_blank" title="instagram"><img src="<?php echo e(asset('img/instagram.png')); ?>"></a></a></li>
                                    <li class="mx-1"><a href="<?php echo e($values->linkedin); ?>" target="_blank" title="linkedin"><img src="<?php echo e(asset('img/linkedin.png')); ?>"></a></a></li>
                                </ul>
                                <div class="line_effect_bottom_left">&nbsp;</div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

            </div>
        </div>
    </section>
     <?php echo $__env->make('front.elements.subscribe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<?php echo $__env->make('front.elements.subscribe_pop_up', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript">
        function contact_us() { 
        $('#loader_img').show();
        $('.help-inline').html('');
        $('.help-inline').removeClass('error');
        $.ajax({ 
            url: '<?php echo e(URL("subscribe")); ?>',
            type:'post',
            data: $('#contact_form').serialize(),
            success: function(r){
                error_array     =   JSON.stringify(r);
                data            =   JSON.parse(error_array);
                if(data['success'] == 1) {
                    window.location.href     =  "<?php echo e(URL('/')); ?>";
                }else { 
                    $.each(data['errors'],function(index,html){
                        $("#"+index).next().addClass('error');
                        $("#"+index).next().html(html);
                    });
                    $('#loader_img').hide();
                }
                $('#loader_img').hide();
            }
        });
    }
    $('#contact_form').each(function() {
        $(this).find('input').keypress(function(e) {
          if(e.which == 10 || e.which == 13) {
            contact_us();
            return false;
           }
       });
    });
$('.partners_sliders').owlCarousel({
    //rtl:false,            
    loop: false,
    margin: 0,
    nav: true,
    //navText : ["",""],
    dots: false,
    autoplay: true,
    responsive: {
        320: {
            items: 1
        },
        480: {
            items: 2
        },
        576: {
            items: 3
        },
        768: {
            items: 3
        },
        992: {
            items: 4
        },
        1200: {
            items: 4
        }
    }
});
//say_about_client_slider
$('.say_about_client_slider').owlCarousel({
    //rtl:false,            
    loop: false,
    margin: 0,
    nav: true,
    //navText : ["",""],
    dots: false,
    autoplay: true,
    items: 1,
});
//our_team_slider
$('.our_team_slider').owlCarousel({
    //rtl:false,            
    loop: false,
    margin: 0,
    nav: true,
    //navText : ["",""],
    dots: false,
    autoplay: true,
    responsive: {
        320: {
            items: 1
        },
        480: {
            items: 2
        },
        576: {
            items: 2
        },
        768: {
            items: 3
        },
        992: {
            items: 4
        },
        1200: {
            items: 4
        }
    }
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/cms/aboutus.blade.php ENDPATH**/ ?>
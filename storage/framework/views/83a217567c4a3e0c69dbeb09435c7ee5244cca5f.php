<?php $__env->startSection('content'); ?>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">


<section class="content-header">
	<h1>
	  <?php echo e(trans("Gameweeks")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Lockout Log")); ?></li>
	</ol>
</section>
<section class="content">  
	<div class="box">
		<div class="box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">GW</th>
						<th width="20%">Start Day</th>
						<th width="20%">End Day</th>
						<th width="20%">Start Time</th>
						<th width="20%">End Time</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!empty($result)): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr class="items-inner " style="<?php echo e($record['current'] == 1 ? 'background: yellow;' : ''); ?>">
							<td>GW<?php echo e($key); ?></td>
							<td><?php echo e($record['gw_start']->format('d.m.y')); ?></td>
							<td><?php echo e($record['gw_end']->format('d.m.y')); ?></td>
							<td><?php echo e($record['gw_start']->format('g:i A')); ?></td>
							<td><?php echo e($record['gw_end']->format('g:i A')); ?></td>
						</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="4" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<?php /*
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
		*/ ?>

<?php /*
<div class="table-responsive table-mobile">
                        <table id="example" width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table states_table mb-0">
                        <thead>
                            <tr>
                                <!-- <th width="70px">Rank</th> -->
                                <th class="colmn1">Player</th>
                                <th class="colmn2">Trades</th>
                                <th class="colmn2">Triple Cap</th>
                                <th class="colmn2">12th Man</th>
                                <th class="colmn2">Dealer</th>
                                <th class="colmn2">Flipper</th>
                                <th class="colmn2">Shield Steal</th>


                            </tr>
                        </thead>
                        <tbody>
                    <?php $i = 1; ?>
                    @if(!empty($all_users) && $all_users->isNotEmpty())
                        @foreach($all_users as $val)

                            <tr>

                            	<td>{{$val->full_name}}</td>
                            	<td>{{ !empty($trades_count[$val->id]) ? $trades_count[$val->id] : 0 }}</td>
                            	<td>{{ !empty($capton_card[$val->id]) ? $capton_card[$val->id] : 0 }}</td>
                            	<td>{{ !empty($twelve_man_card[$val->id]) ? $twelve_man_card[$val->id] : 0 }}</td>
                            	<td>{{ !empty($dealer_card[$val->id]) ? $dealer_card[$val->id] : 0 }}</td>
                            	<td>{{ !empty($flipper_card[$val->id]) ? $flipper_card[$val->id] : 0 }}</td>
                            	<td>{{ !empty($shield_steal[$val->id]) ? $shield_steal[$val->id] : 0 }}</td>

                            	


                            </tr>
                        @endforeach
                   @else 
                    <tr><td colspan="5">No players have yet been added to the game.</td></tr>

                    @endif

</tbody>
   
                        </table>
                    </div>



*/ ?>






<?php 


/* 

                 
                        {{ Form::open(array('name' => 'myFormName' )) }}
                        <div class="row">
                            <div class="col-sm-12">
                                <select class="custom-select " name="gw_no" style="width: 10rem; " >
                                    <!-- <option value="" >Choose GW</option> -->
                                    @for ($i = 0; $i < $most_last_gw; $i++)
                                        <option {{isset($gw_no) && $gw_no == $i ? 'selected' : ''}} value="{{$i}}">GW{{$i+1}}</option>
                                    @endfor
                                </select>
                                <div class="table-responsive table-mobile">
                                    <table  id="example_player_gwpt"  width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>Player Name</th>
                                                <th>GW Pts</th>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            
                                                <?php $i = 1;  // dump($temp_gw_data); die;  ?>
                                            @if(!empty($temp_gw_data))
                                                @foreach($temp_gw_data as $key=>$val)
                                                    @if(!empty($val) && !empty($val['player_data']))

                                                       
                                                    <tr>
                                                        <td>{{$i++}}</td>
                                                        <td>{{!empty($val['player_data']) ? $val['player_data']->full_name : ''}}</td>
                                                        <td>{{$val['player_point']}}</td>
                                                    </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                            
                                    
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            

                            
                        </div>
                    {{ Form::close() }}





*/ 


?>








	</div> 
</section> 

<?php /*
@if(!empty($most_trade_in))
	1.	Most traded in {{$most_trade_in->full_name}}
@endif
@if(!empty($most_trade_out))
	2.	Most traded out {{$most_trade_out->full_name}}
@endif
@if(!empty($most_picked_capton))
	3.	Most picked captain {{$most_picked_capton->full_name}}
@endif
@if(!empty($most_picked_v_capton))
	4.	Most picked v-captain {{$most_picked_v_capton->full_name}}
@endif
*/ ?>

<script type="text/javascript">
	
	// var table =   $('#example').DataTable(); 

</script>


<script type="text/javascript">

    $( document ).ready(function() {
        
        var table =   $('#example_player_gwpt').DataTable( );

        


      $('select').on('change', function() {
         document.forms['myFormName'].submit();
      });


    }); 




</script>


<style type="text/css">
    
    #example_player_gwpt_length, #example_player_gwpt_filter, #example_player_gwpt_info { display: none; }
</style>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/common/lockout_logs.blade.php ENDPATH**/ ?>
<div class="form-group">
	<?php echo e(Form::label('state',trans("State"), ['class' => 'mws-form-label'])); ?>

	<div class="mws-form-item ">
		<?php echo e(Form::select(
			 'state',
			 [null => 'Please Select State']+$stateList,
			 !empty($state_old_id) ? $state_old_id:'',
			 ['id' => 'state_id','class'=>'form-control']
			)); ?>

		<div class="error-message help-inline">
			<?php echo $errors->first('state'); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#state_id').on('change',function(){
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	var state_id = $('#state_id').val();
	$.ajax({ 
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: "<?php echo e(URL('admin/get-city')); ?>",
		type:'post',
		data:{'state_id':state_id},
		success: function(r){ 
			$('.get_city_class').html(r);
			$('#loader_img').hide();
		}
	});
})
</script><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/usermgmt/state.blade.php ENDPATH**/ ?>
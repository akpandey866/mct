<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Availability")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Availability")); ?></li>
	</ol>
</section>
<section class="content"> 
	
	<div class="row">
	<?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>	
		<?php echo e(Form::open(['role' => 'form','url' => 'admin/availability','class' => 'mws-form',"method"=>"get"])); ?>

		<?php echo e(Form::hidden('display')); ?>

			<?php if(Auth::guard('admin')->user()->user_role_id == 1): ?>
				<div class="col-md-2 col-sm-1">
					<div class="form-group ">  
						<?php echo e(Form::select('mode',[null => 'Please Select mode'] +Config::get('home_club'),((isset($searchVariable['mode'])) ? $searchVariable['mode'] : ''),['id' => 'mode','class'=>'form-control choosen_selct'])); ?>

					</div>
				</div>	
				<div class="col-md-2 col-sm-1">
					<div class="form-group put_html">  
						<?php echo e(Form::select('club',[null => 'Please Select Club'],((isset($searchVariable['club'])) ? $searchVariable['club'] : ''),['id' => 'club','class'=>'form-control choosen_selct'])); ?>

					</div>
				</div>	
				<div class="col-md-2 col-sm-1">
					<div class="form-group put_player_html">  
						<?php echo e(Form::select('player',[null => 'Please Select Player'],((isset($searchVariable['player'])) ? $searchVariable['player'] : ''),['id' => 'player','class'=>'form-control choosen_selct'])); ?>

					</div>
				</div>
			<?php else: ?>
				<div class="col-md-2 col-sm-2">
				<?php echo e(Form::select('player',[null => 'Please Select Player'] + $playerList,((isset($searchVariable['player'])) ? $searchVariable['player'] : ''),['id' => 'player','class'=>'form-control choosen_selct'])); ?>

				</div>	
			<?php endif; ?>
			<div class="col-md-2 col-sm-2">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> <?php echo e(trans('Search')); ?></button>
				<a href="<?php echo e(URL::to('admin/availability')); ?>"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> <?php echo e(trans("Reset")); ?></a>
			</div>
			<?php echo e(Form::close()); ?>

			<div class="col-md-4 col-sm-12">
			<div class="form-group">  
				<a href="<?php echo e(URL::to('admin/availability/add-availability')); ?>" class="btn btn-success btn-small  pull-right" style="margin:0;"><?php echo e(trans("Add New Availability")); ?> </a>
			</div>
		</div>
			<?php else: ?>
		<div class="col-md-12 col-sm-12">
			<div class="form-group">  
				<a href="<?php echo e(URL::to('admin/availability/add-availability')); ?>" class="btn btn-success btn-small  pull-right" style="margin:0;"><?php echo e(trans("Add New Availability")); ?> </a>
			</div>
		</div>
	<?php endif; ?>
		
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="11%">
							<?php echo e(link_to_route(
									"availability.index",
									trans("Player Name"),
									array(
										'sortBy' => 'player_name',
										'order' => ($sortBy == 'player_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'player_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'player_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"availability.index",
									trans("Date From"),
									array(
										'sortBy' => 'date_from',
										'order' => ($sortBy == 'date_from' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'date_from' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'date_from' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"availability.index",
									trans("Date Till"),
									array(
										'sortBy' => 'date_till',
										'order' => ($sortBy == 'date_till' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'date_till' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'date_till' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"availability.index",
									trans("Reason"),
									array(
										'sortBy' => 'reason',
										'order' => ($sortBy == 'reason' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'reason' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'reason' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"availability.index",
									trans("Status"),
									array(
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									"player.index",
									trans("Created On"),
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							Action
						</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr class="items-inner">
							<td>
								<?php echo e($record->player_name); ?> 
							</td>
							<td> 
								<?php echo e($record->date_from); ?> 
							</td>
							<td>
								<?php echo e($record->date_till); ?>

							</td>
							<td>
								<?php echo e($record->reason); ?>

							</td>
							<td>
								<?php if($record->is_active	==1): ?>
									<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
								<?php else: ?>
									<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
								<?php endif; ?>
							</td>
							<td>
								<?php echo e(date(config::get("Reading.date_format"),strtotime($record->created_at))); ?>

							</td>
							<td data-th='Action'>
								<a title="<?php echo e(trans('Edit')); ?>" href="<?php echo e(URL::to('admin/availability/edit-availability/'.$record->id)); ?>" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>
								<?php if($record->is_active == 1): ?>
									<a  title="Click To Deactivate" href="<?php echo e(URL::to('admin/availability/update-status/'.$record->id.'/0')); ?>" class="btn btn-success btn-small status_any_item"><span class="fa fa-check"></span>
									</a>
								<?php else: ?>
									<a title="Click To Activate" href="<?php echo e(URL::to('admin/availability/update-status/'.$record->id.'/1')); ?>" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
									</a> 
								<?php endif; ?>							
								<a title="<?php echo e(trans('Delete')); ?>" href="<?php echo e(URL::to('admin/availability/delete-availability/'.$record->id)); ?>"  class="delete_any_item btn btn-danger">
									<i class="fa fa-trash-o"></i>
								</a>
								
							</td>
						</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="9" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div> 
</section> 
    
<script> 
$('#loader_img').show();
$("#mode").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"<?php echo e(url('admin/get-club-mode-listing')); ?>",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});
/*adter search club searhcing*/
var mode = "<?php  echo !empty($searchVariable['mode']) ? $searchVariable['mode']:'';  ?>";
var searchClubVar = "<?php  echo !empty($searchVariable['club']) ? $searchVariable['club']:'';  ?>";
if(mode !=''){ 
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"<?php echo e(url('admin/get-club-mode-listing')); ?>",
		'type':'post',
		data:{'id':mode,'club_id':searchClubVar},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
}

/*After search player searching*/
var club_id = "<?php  echo !empty($searchVariable['club']) ? $searchVariable['club']:0;  ?>";
var searchPlayerVar = "<?php  echo !empty($searchVariable['player']) ? $searchVariable['player']:0;  ?>";
$(document).on("change",'#get_club_name',function(){
	var club_id = $(this).val();
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"<?php echo e(url('admin/get-club-mode-player')); ?>",
		'type':'post',
		data:{'id':club_id,'player_id':searchPlayerVar},
		async : false,
		success:function(response){
			$('.put_player_html').html(response);
			$('#loader_img').hide();
		}
	});
});
if(club_id !=''){  
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"<?php echo e(url('admin/get-club-mode-player')); ?>",
		'type':'post',
		data:{'id':club_id,'player_id':searchPlayerVar},
		async : false,
		success:function(response){
			$('.put_player_html').html(response);
			$('#loader_img').hide();
		}
	});
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/availabilities/index.blade.php ENDPATH**/ ?>
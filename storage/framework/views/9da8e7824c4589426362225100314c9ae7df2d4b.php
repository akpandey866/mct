<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>Notification Management</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">Notification Management</li>
	</ol>
</section>
	
<section class="content"> 
	<div class="row">
		<div class="col-md-12 col-sm-12 ">
			<div class="form-group pull-right">  
				<a href="<?php echo e(route('notification.addNotificaiton')); ?>" class="btn btn-success btn-small align"><?php echo e('Add New notification'); ?> </a>
			</div>
		</div>
	</div> 
		
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
			<thead>
				<tr>
					<th>Body</th>
					<th>
						<?php echo e(link_to_route(
								"notification.listing",
								trans("Status"),
								array(
									'id'=>isset($user_role_id)?$user_role_id:'',
									'sortBy' => 'is_active',
									'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
							   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)); ?>

					</th>
					<th>
						<?php echo e(link_to_route(
								'notification.listing',
								'Created ',
								array(
									'sortBy' => 'created_at',
									'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc'
								),
							   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)); ?>

					</th>
					<th><?php echo e('Action'); ?></th>
				</tr>
			</thead>
			<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
				<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr class="items-inner">
					<td data-th='Body'>
						<?php echo Str::limit($record->body,20); ?>

					</td>
					<td>
						<?php if($record->is_active	==1): ?>
							<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
						<?php else: ?>
							<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
						<?php endif; ?>
						<?php if($record->status==1): ?>
							<span class="label label-success" ><?php echo e(trans("Draft")); ?></span>
						<?php endif; ?>
					</td>
					<td data-th='Created At'><?php echo e(date(Config::get("Reading.date_format") , strtotime($record->created_at))); ?></td>
					<td data-th='Action'>						
						<a title="Edit" href="<?php echo e(route('notification.editNotification',$record->id)); ?>" class="btn btn-info btn-small"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						<a title="Delete" href="<?php echo e(route('notification.deleteNotification',$record->id)); ?>"  class="delete_any_item btn btn-danger btn-small"><i class="fa fa-trash" aria-hidden="true"></i></a>
						<a title="Send as  email" href="<?php echo e(route('notification.sendEmail',$record->id)); ?>" class="btn btn-info btn-small"><i class="fa fa-envelope" aria-hidden="true"></i></a>
						<a title="Push as Notification" href="<?php echo e(route('notification.editNotification',$record->id)); ?>" class="btn btn-info btn-small"><i class="fa fa-bell" aria-hidden="true"></i></a>
					</td>
				</tr>
				 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
			</tbody>
		</table>
		<?php else: ?>
			<tr>
			<td align="center" style="text-align:center;" colspan="6" > No Result Found</td>
		  </tr>			
		<?php endif; ?> 
	</div>
	<?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/notifications/index.blade.php ENDPATH**/ ?>
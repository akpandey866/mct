<?php $__env->startSection('content'); ?>

<section class="content-header">
	<h1>
		<?php echo e(trans("Add New Team")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/team/')); ?>"><?php echo e(trans("Team")); ?></a></li>
		<li class="active"><?php echo e(trans("Add New Team")); ?> </li>
	</ol>
</section>
<section class="content"> 
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/team/add-team','class' => 'mws-form','files'=>'true', 'id' => 'add_team'])); ?>

	<div class="row">
	
		      <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('name',trans("Team Name").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text('name','',['class' => 'form-control'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div></div>
		  </div>
		 <?php if(Auth::guard('admin')->user()->user_role_id !=1): ?>
		 <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('grade_name')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('grade_name', trans("Grade This Team Plays In").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::select(
						 'grade_name',
						 [null => 'Please Select Grade Name'] + $gradeName,
						 '',
						 ['id' => 'grade_name','class'=>'form-control']
						)); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('grade_name'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php else: ?>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('type')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('type', trans("Team Type").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::select(
						 'type',
						 [null => 'Please Select Team Type'] + $teamType,
						 '',
						 ['id' => 'type','class'=>'form-control']
						)); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('type'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="row">
		<?php if(Auth::guard('admin')->user()->user_role_id !=1): ?>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('type')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('type', trans("Team Type").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::select(
						 'type',
						 [null => 'Please Select Team Type'] + $teamType,
						 '',
						 ['id' => 'type','class'=>'form-control']
						)); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('type'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('team_category')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('team_category', trans("Team Category").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::select(
						 'team_category',
						 [null => 'Please Select Team Category'] + $teamCategory,
						 '',
						 ['id' => 'team_category','class'=>'form-control']
						)); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('team_category'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
		    <div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
					<?php echo e(Form::label('club', trans("Club").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::select('club',[null => 'Please Select Club'] + $cludDetails,'',['id' => 'club','class'=>'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>

	<?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('grade_name')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('grade_name', trans("Grade This Team Plays In").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item put_html">
					<?php echo e(Form::select('grade_name',[null => 'Please Select Grade Name'],'',['id' => 'grade_name','class'=>'form-control'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('grade_name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>		
	<?php if(Auth::guard('admin')->user()->user_role_id != SUPER_ADMIN_ROLE_ID): ?>
	<?php echo e(Form::hidden('club',Auth::guard('admin')->user()->id)); ?>

	<?php endif; ?>
	<div class="mws-button-row">
		<div class="input" >
			<input type="button" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger" onclick="save_team();">
			<a href="<?php echo e(URL::to('admin/team/add-team/')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans("Reset")); ?></a>
			<a href="<?php echo e(URL::to('admin/team/')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans("Cancel")); ?></a>
		</div>
	</div>
	<?php echo e(Form::close()); ?>

	
</section>
<script type="text/javascript">
$('#club').on('change',function(){
	$('#loader_img').show();
	var id = $(this).val();
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:' <?php echo e(route("Team.getGrade")); ?> ',
		'type':'post',
		data:{'id':id},
		async : false,
		success:function(response){ 
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});
function save_team(){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
	var form = $('#add_team')[0];
	var formData = new FormData(form);
    $.ajax({
        url: '<?php echo e(route("Team.saveTeam")); ?>',
        type:'post',
        data: formData,
		processData: false,
		contentType: false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
				show_message(data['message'],"success");
                window.location.href   =  "<?php echo e(route('team.index')); ?>";
            }else {
                $.each(data['errors'],function(index,html){
                    $("#"+index).next().addClass('error');
                    $("#"+index).next().html(html);
                });
                $('#loader_img').hide();
            }
            $('#loader_img').hide();
        }
    });
}
$('#add_team').each(function() {
		$(this).find('input').keypress(function(e) {
	       if(e.which == 10 || e.which == 13) {
				save_team();
				return false;
	        }
	    });
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/team/add.blade.php ENDPATH**/ ?>
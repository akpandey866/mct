<?php $__env->startSection('content'); ?>
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end pb-3" style="background-image: url(<?php echo e(asset('img/inner_banner.png')); ?>);">
        <div class="container py-3 py-lg-4">
            <h2 class="block_title"><small>
                <?php echo e(!empty($result->name) ?  $result->name :''); ?>

            </small>MyClubtap</h2>
            <!--<a href="" class="btn py-2 px-5">Join Now</a>-->
        </div>
    </section>
    
    <section class="chooseus_block pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12 pt-4 pt-md-0">
                    <h2 class="block_title my-3"></h2>
                    <?php if(!empty($result->body)): ?>
                        <?php echo $result->body; ?>

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php echo $__env->make('front.elements.subscribe_pop_up', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/cms/index.blade.php ENDPATH**/ ?>
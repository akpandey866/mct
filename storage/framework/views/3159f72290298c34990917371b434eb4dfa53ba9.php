<?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('js/bootstrap-datepicker.js')); ?>"></script>
<link href="<?php echo e(asset('css/bootstrap-datepicker.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('css/datepicker-custom-theme.css')); ?>" rel="stylesheet">
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end" style="background-image: url(<?php echo e(asset('img/inner_banner.png')); ?>);">
    </section>



  <!-- <button type="button" class="btn btn-info btn-lg show-modal" data-toggle="modal" data-target="#myModal">Open Modal</button> -->


  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Junior Mode</h4>
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
                    <section class="chooseus_block " >
                        <div class="container">                                        
                            <div class="row">                                
                                           MyClubtap runs routine checks on the right use of Junior club mode and can ask your club for further age-proof on players added in the junior game, to ensure no Senior players (19+) are added in the junior game. Any game found in breach of using Junior Club mode to run a fantasy game based on Senior players will be immediately blocked from further use on MyClubtap without notice and is non-refundable.                                    
                            </div>                             
                        </div>
                    </section>         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Read & Agree</button>
        </div>
      </div>
      
    </div>
  </div>
 <!-- Modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Senior Mode</h4>
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
                    <section class="chooseus_block " >
                        <div class="container">                                        
                            <div class="row">                                 
                                          Senior mode is best to have a fantasy game based on your Senior grade players (mens, women & veterans) only. You agree that you have the authorization to set-up this game on-behalf of your club and players.                                    
                            </div>                             
                        </div>
                    </section>         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Read & Agree</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">League Mode</h4>
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <div class="modal-body">
                    <section class="chooseus_block " >
                        <div class="container">                                        
                            <div class="row">                                
                                           League mode is best for a fantasy game based on a tournament or competition with multiple teams only. You agree that you have the authorization to set-up this game on-behalf of your league/competition and players.                                
                            </div>                             
                        </div>
                    </section>         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Read & Agree</button>
        </div>
      </div>
      
    </div>
  </div>


    <section class="signin_signup_block py-5">
        <div class="container main_signup_contenar">
            <h2 class="form_title mb-3">Sign Up as Club or a League?</h2>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                    <?php echo e(Form::open(['role' => 'form','route' => "Home.saveSignup",'class' => 'form_block p-4','id'=>'signup','files' => true])); ?>

                        <h3 class="form_heading mb-4">Hello! <small>Create an account to continue</small></h3>
                        <h4 class="form_sec_heading text-uppercase mb-2">Game Mode <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="<h6>Junior Club Mode</h6><ul><li>Best for a fantasy game based on our Junior grade players (boys & girls) only.</li></ul><h6>Senior Club Mode</h6>
<ul><li>Best for a fantasy game based on our Senior grade players (mens, women & veterans) only.</li></ul><h6>League Mode</h6><ul><li>Best for a fantasy game based on a tournament or competition with multiple teams.</li></ul>"><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></h4>
                        <div class="form-group">
                            <?php echo e(Form::select('game_mode',[null => 'Please Select Mode *']+Config::get('home_club'),'', ['id' => 'game_mode', 'class' => 'custom-select'])); ?>

                            <span class="help-inline game_mode"></span>
                        </div>
                        <div class="club_detail_sec mt-4">
                            <h4 class="form_sec_heading text-uppercase mb-2"><span class="change_name">Club</span> Details</h4>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Club Name *" id="club_name" name="club_name" />
                                <span class="help-inline club_name"></span>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Game Name *" name="game_name" />
                                <span class="help-inline game_name"></span>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Username *" name="username" autocomplete="off" />
                                <span class="help-inline username"></span>
                            </div>
                            <div class="row mx-n2">
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::text('sport_name','Cricket',['class' => 'form-control','readonly'])); ?>

                                        <span class="help-inline"></span>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::select('country',[null => 'Select Country *']+$countryList,'', ['class' => 'custom-select','id' => 'country_id'])); ?>

                                        <span class="help-inline country"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mx-n2">
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="get_state_class">
                                        <div class="form-group">
                                            <?php echo e(Form::select('state', [null => 'Select State *'],'',['id' => 'state_id','class'=>'custom-select'])); ?>

                                            <span class="help-inline state"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="get_city_class">
                                        <div class="form-group">
                                            <?php echo e(Form::text("city",'', ['class'=>'form-control city'])); ?>

                                            <span class="help-inline city"></span>
                                        </div>
                                    </div>
                                </div>
                            </div> 
							    <h4 class="form_sec_heading text-uppercase mb-2"><span class="change_name">Club</span> Logo <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="<ul><li><?php echo "The attachment must be a file of type:".IMAGE_EXTENSION; ?></li></ul>"><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></h4>
                                 <div class="form-group"><input class="form-control" type="file" placeholder="Club Logo" name="club_logo" /></div>
                        </div>
                        <div class="game_rep_detail_sec mt-4">
                            <h4 class="form_sec_heading text-uppercase mb-2">Game Admin Details</h4>
                            <div class="row mx-n2">
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="First Name*" name="first_name" />
                                        <span class="help-inline first_name"></span>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Last Name*" name="last_name" />
                                        <span class="help-inline last_name"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo e(Form::text('email','',['class' => 'form-control', 'placeholder' => trans('Email Address')])); ?>

                                <span class="help-inline email"></span>
                            </div>
                            <div class="row mx-n2">
                                
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::password('password', ['class' => 'form-control', 'placeholder' => trans('Password')])); ?>

                                        <span class="help-inline password"></span>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::password('confirm_password', ['class' => 'form-control', 'placeholder' => trans('Confirm Password')])); ?>

                                        <span class="help-inline confirm_password"></span>
                                    </div>
                                </div>
                                <div class="erro_msg"><em>Must be a minimum of 8 characters in length, contain at least a number and a special character.</em></div>
                            </div>
                            
                            <div class="row mx-n2">
                                <!-- <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Phone No." name="phone" />
                                        <span class="help-inline"></span>
                                    </div>
                                </div> -->
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <div class="text_iconview_block d-flex align-items-center p-0">
                                            <input type="text" placeholder="DOB" class="dob" name="dob" />
                                            <span class="help-inline"></span>
                                            <a class="text_ivon d-flex align-items-center justify-content-center" href="javascript:void(0)"><i class="far fa-calendar-alt"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::select('gender',[null => 'Select Gender']+Config::get('gender_type'),'', ['class' => 'custom-select choosen_selct'])); ?>

                                    </div>
                                </div>
                            </div>
                            <div class="row mx-n2">
                                <!-- <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::select('gender',[null => 'Select Gender']+Config::get('gender_type'),'', ['class' => 'custom-select choosen_selct'])); ?>

                                    </div>
                                </div> -->
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                         <?php echo e(Form::select('position',[null => 'Select Position *']+$positionList,'', ['class' => 'custom-select choosen_selct'])); ?>

                                         <span class="help-inline position"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="game_rep_detail_sec py-3">
                            <h4 class="form_sec_heading text-uppercase mb-2">Authorize</h4>
                            <ul class="authorize_list">
                                <li class="py-2">
                                    <label class="join_our_trems d-flex position-absolute p-0">
                                        <span class="custom_check">
                                            <input type="checkbox" name="terms">
                                            <span class="check_indicator">&nbsp;</span>
                                        </span>

                                    </label>

                                    Agree to <a href="<?php echo e(url('pages/terms')); ?>" target="_blank">Terms</a> &amp; <a href="<?php echo e(url('pages/conditions')); ?>" target="_blank">Conditions</a>.<br>
                                    <span class="help-inline terms"></span>
                                </li>
                                <li class="py-2">
                                    <label class="join_our_trems d-flex position-absolute p-0">
                                        <span class="custom_check">
                                            <input type="checkbox" name="privacy">
                                            <span class="check_indicator">&nbsp;</span>
                                        </span>
                                    </label>
                                    Agree to <a href="<?php echo e(url('pages/terms')); ?>">Privacy Policy</a>.<br>
                                    <span class="help-inline privacy"></span>
                                </li>
                                <li class="py-2">
                                    <label class="join_our_trems d-flex position-absolute p-0">
                                        <span class="custom_check">
                                            <input type="checkbox" name="authorize">
                                            <span class="check_indicator">&nbsp;</span>
                                        </span>
                                    </label>
                                    I am authoriezed by club/league to take this action.<br>
                                    <span class="help-inline authorize"></span>
                                </li>
                            </ul>
                        </div>
                        <input class="w-100 btn text-uppercase" type="button" value="Sign Up" onclick='signup();'>
                        <div class="already_logon_text text-center mt-3">Already have an account yet? <a href="<?php echo e(route('Home.login')); ?>">Login</a></div>
                    <?php echo e(Form::close()); ?>

                </div>
                <div class="col-12 col-md-6 offset-lg-1 join_user_sec">
                    <div class="user_signup_btn">
                      <!--   <div class="mb-3">Don't have h an account yet?</div> -->
                        <div class="mb-2"><a class="btn" href="<?php echo e(route('Home.userSignup')); ?>">Sign up as User</a></div>
                    </div>

                    <!--   <section class="chooseus_block py-4" style="border: 1px solid #0f70b7; border-radius: 6px; padding: 4px; ">
                      <div class="container">
                            <h2 class="block_title mb-3"><small>What you get </small>As a User</h2>
                            <div class="row">
                                 <ul class="main_point">
                                     <li class="pb-2">Pick your own fantasy team and join any fantasy game on the platform.</li>
                                     <li class="pb-2">Pick a captain and vice-captain for your fantasy team.</li>
                                     <li class="pb-2">Collect fantasy points as players perform on-field in real-matches.</li>
                                     <li class="pb-2">Trade players in and out and use power boosters as available in the fantasy game.</li>
                                     <li class="pb-2">Set and follow your favourite players.</li>
                                     <li class="pb-2">Access your detailed trade and gameweek history.</li>
                                 </ul>
                             </div>
                            <div class="mb-4">
                                <h4 class="pricing_info_title mb-2">Junior Club Mode</h4>
                                <p class="join_user_text">Best for a fantasy game based on our Junior grade players (boys & girls) only.</p>
                            </div>
                            <div class="mb-4">
                                <h4 class="pricing_info_title mb-2">Senior Club Mode</h4>
                                <p class="join_user_text">Best for a fantasy game based on our Senior grade players (mens, women & veterans) only.</p>
                            </div>
                            <div class="mb-4">
                                <h4 class="pricing_info_title mb-2">League Mode</h4>
                                <p class="join_user_text">Best for a fantasy game based on a tournament or competition with multiple teams.</p> 
                            </div>
                            <div class="mb-0">
                                <h4 class="pricing_info_title mb-2">NOTE</h4>
                                <p class="join_user_text mb-0">MyClubtap runs routine checks on the right use of Junior club mode and can ask your club for further age-proof on players added in the junior game, to ensure no Senior players (19+) are added in the junior game. Any game found in breach of using Junior Club mode to run a fantasy game based on Senior players will be immediately blocked from further use on MyClubtap without notice and is non-refundable.</p>
                            </div>                         
                        </div>
                    </section> -->  
                </div>
            </div>
        </div>
    </section>    
</div>
<script type="text/javascript">
    $(document).ready(function(){

      var sc_width = screen.width;
      if(sc_width < 750){
        // alert(sc_width); 
        $(".user_signup_btn").prependTo(".main_signup_contenar"); 

      }

    }); 

function signup() { 
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    var form = $('#signup')[0];
    var formData = new FormData(form);
    $.ajax({ 
        url: '<?php echo e(route("Home.saveSignup")); ?>',
        type:'post',
        data: formData,
        processData: false,
        contentType: false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
                window.location.href     =  "<?php echo e(URL('signup')); ?>";
            }else { console.log(data['errors']);
                $.each(data['errors'],function(index,html){
                    if(index=="terms"){ 
                        $(".terms").addClass('error');
                        $(".terms").html(html);
                    }
                    if(index=="privacy"){ 
                        $(".privacy").addClass('error');
                        $(".privacy").html(html);
                    }
                    if(index=="authorize"){ 
                        $(".authorize").addClass('error');
                        $(".authorize").html(html);
                    }
                    if(index=="club_name"){ 
                        $(".club_name").addClass('error');
                        $(".club_name").html(html);
                    }
                    if(index=="game_mode"){ 
                        $(".game_mode").addClass('error');
                        $(".game_mode").html(html);
                    }
                    if(index=="country"){ 
                        $(".country").addClass('error');
                        $(".country").html(html);
                    }if(index=="state"){ 
                        $(".state").addClass('error');
                        $(".state").html(html);
                    }
                    if(index=="city"){ 
                        $(".city").addClass('error');
                        $(".city").html(html);
                    }
                    if(index=="first_name"){ 
                        $(".first_name").addClass('error');
                        $(".first_name").html(html);
                    }if(index=="email"){ 
                        $(".email").addClass('error');
                        $(".email").html(html);
                    }if(index=="last_name"){ 
                        $(".last_name").addClass('error');
                        $(".last_name").html(html);
                    }if(index=="position"){ 
                        $(".position").addClass('error');
                        $(".position").html(html);
                    }if(index=="game_name"){ 
                        $(".game_name").addClass('error');
                        $(".game_name").html(html);
                    }if(index=="username"){ 
                        $(".username").addClass('error');
                        $(".username").html(html);
                    }if(index=="password"){ 
                        $(".password").addClass('error');
                        $(".password").html(html);
                    }if(index=="confirm_password"){ 
                        $(".confirm_password").addClass('error');
                        $(".confirm_password").html(html);
                    }    

                });
                
                $('#loader_img').hide();
            }
            $('#loader_img').hide();
        }
    });
}
    
$('#signup').each(function() {
    $(this).find('input').keypress(function(e) {
        if(e.which == 10 || e.which == 13) {
        signup();
        return false;
       }
    });
});

$(function(){
    $('.dob').datepicker({
        format  : 'yyyy-mm-dd',
        endDate: "today",
        autoclose:true,
    }); 
});

$('.fa-calendar-alt').on('click', function (Event) {        
    $('.dob').datepicker('show');
});

$('#country_id').on('change',function(){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    var country_id = $('#country_id').val();
    $.ajax({ 
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "<?php echo e(URL('get-state')); ?>",
        type:'post',
        data:{'country_id':country_id},
        success: function(r){
            $('.get_state_class').html(r);
            $('#loader_img').hide();
        }
    });
});
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   


$( "#game_mode" ).change(function() {
    if($(this).val() == 2){
        $('.change_name').text('Club');
        // $('#club_name').attr("placeholder", "Club Name *");
        $("#myModal").modal({
            backdrop: 'static',
            keyboard: false
        });
    }else   if($(this).val() == 1){
        $('.change_name').text('Club');
         // $('#club_name').attr("placeholder", "Club Name *");
        $("#myModal1").modal({
            backdrop: 'static',
            keyboard: false
        });
    }else   if($(this).val() == 3){
        $('.change_name').text('League');
         // $('#club_name').attr("placeholder", "League Name *");
        $("#myModal2").modal({
            backdrop: 'static',
            keyboard: false
        });
    } 
});



});
</script>
<style type="text/css">
    div.tooltip-inner {
    max-width: 350px;
    text-align: justify;
    }
    div.tooltip-inner h6{ font-weight: bold;  }
    div.tooltip-inner li { list-style: disc; }
    #myModal li{ list-style: none; }
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/home/signup.blade.php ENDPATH**/ ?>
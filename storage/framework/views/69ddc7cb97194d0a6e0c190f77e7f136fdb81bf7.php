<?php $__env->startSection('content'); ?>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">



<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<style type="text/css">
.col_cstm_width{
    min-width: 180px;
}

div.dataTables_wrapper div.dataTables_filter {

    text-align: right;
    display: flex;
    justify-content: flex-end;

}
@media(max-width:575.98px){
    div.dataTables_wrapper div.dataTables_filter{
            justify-content: flex-start !important;
    }
    .col_cstm_width{
        min-width: auto;
    }
  /* .sortBy_class+label{
        width:25% !important;
        margin-right: 0px;
    }*/
    div.dataTables_wrapper div.dataTables_filter input { 
         width: 100px;
    }

    #example_filter {
        text-align: center;
        margin-top: 10px;
    }
    .full_width{
        width: 100% !important;
    }
}
</style>


<div class="full_container">
<div class="body_section">
    <!-- top_header element -->
    <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>       
    <section class="status_tab_block mb-5">
        <div class="container mobile_cont">
            <!-- navbar element -->
            <?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="tab_content_sec border  p-0  p-md-4">




<!-- <div class="d-block d-md-flex align-items-center mb-4">
                        <h4 class="status_title mr-auto">Statistics</h4>
                        <form action="" class="statistics_form ml-auto">
                            <div class="row">
                                <div class="col-12 col-sm-6 py-2">
                                    <select class="custom-select">
                                        <option value="">All Players</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6 py-2">
                                    <select class="custom-select">
                                        <option value="">Fantasy Points</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div> -->



<?php /*
            <div class="d-block d-md-flex align-items-center">
                    <h3 class="players_title d-flex align-items-center mb-3 mb-md-0">Statistics </h3>
                    {{ Form::open(['role' => 'form','url' => Route('Common.stats'),'class' => 'top_search_block position-relative mws-form ml-0 ml-md-auto',"method"=>"get"]) }}
                    <button class="input_search" type="submit"><img src="{{asset('img/search.svg')}}" alt="search"></button>
                    {{ Form::text('search',((isset($search)) ? $search : ''), ['class' => 'form-control','placeholder'=>'Search']) }}
                    {{ Form::close() }}
                    
                </div>

*/ ?>
        <!--             <h4 class="status_title ">Stats</h4> -->
                    <div class="table-responsive table-mobile">
                        <table id="example" width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table states_table mb-0">
                        <thead>
                            <tr>
                                <!-- <th width="70px">Rank</th> -->
                                <th class="colmn1">Player</th>
                                <th class="colmn2">Position</th>
                                <th class="colmn3">Picked In Teams</th>
                                <th class="colmn4">Fantasy Pts</th>
                                <!-- <th class="colmn5 d_none sum_fp">FP</th> -->
                                <th class="colmn5 d_none sum_rs full_width">Runs</th>
                                <th class="colmn5 d_none sum_fours full_width">Fours</th>
                                <th class="colmn5 d_none sum_sixes full_width">Sixes</th>
                                <th class="colmn5 d_none sum_wks full_width">Wickets</th>
                                <th class="colmn5 d_none sum_mdns full_width">Maidens</th>
                                <th class="colmn5 d_none sum_hattrick full_width">Hattrick</th>
                                <th class="colmn5 d_none sum_cs full_width">Catches</th>
                                <th class="colmn5 d_none sum_sts full_width">Stump</th>
                                <th class="colmn5 d_none sum_overs full_width">Overs</th>
                                <th class="colmn5 d_none pcap full_width">PCap  (%)</th>
                                <th class="colmn5 d_none pvcap full_width">PVCap  (%)</th>
                                <th  class="d_none full_width">P. Value</th>
                            </tr>
                        </thead>
                        <tbody>
                    <?php $i = 1; ?>
                    <?php if(!empty($result)): ?>
                        <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>
                                <!-- <td><?php echo e($i++); ?></td> -->
                                
                                <td  data-search="<?php echo e($val->full_name); ?>" class="col_cstm_width">

                                 <!--  <a href="<?php echo e(url('player-profile/'.$val->id)); ?>"> -->
                                  <a href="<?php echo e(url('player-profile/'.$val->id)); ?>">

                                 <!--    <span class="player d-block d-md-flex  align-items-center flex-nowrap dt_player_name" player_id="<?php echo e($val->id); ?>"  style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" > -->
                                    
                                  <span class="player d-block d-md-flex  align-items-center flex-nowrap dt_player_name">
                                    <?php if(!empty($val->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.$val->image)): ?>
                                        <img class="player_pic" src="<?php echo WEBSITE_URL.'image.php?width=34px&height=34px&cropratio=1:1&image='.PLAYER_IMAGE_URL.$val->image ?>" alt="player">
                                    <?php else: ?>
                                        <?php if($val->position == 1): ?>
                                            <a href="<?php echo e(url('player-profile/'.$val->id)); ?>">
                                            <img  style="width: 40px; height: 40px"  src="<?php echo e(asset('img/bm.png')); ?>" alt="top players"></a>
                                        <?php endif; ?>
                                        <?php if($val->position == 2): ?>
                                            <a href="<?php echo e(url('player-profile/'.$val->id)); ?>">
                                            <img  style="width: 40px;height: 40px"  src="<?php echo e(asset('img/bow.png')); ?>" alt="top players"></a>
                                        <?php endif; ?>
                                        <?php if($val->position == 3): ?>
                                            <a href="<?php echo e(url('player-profile/'.$val->id)); ?>">
                                            <img  style="width: 40px;height: 40px"  src="<?php echo e(asset('img/ar.png')); ?>" alt="top players"></a>
                                        <?php endif; ?>
                                        <?php if($val->position == 4): ?>
                                            <a href="<?php echo e(url('player-profile/'.$val->id)); ?>">
                                            <img  style="width: 40px;height: 40px"  src="<?php echo e(asset('img/wc.png')); ?>" alt="top players"></a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                        <span class="player_name pl-0 pt-1 pt-md-0 pl-md-3 d-block">

                                <?php echo e(!empty($val->first_name) ? ucfirst(substr($val->first_name, 0, 2).'. ') : ''); ?>

                                <?php echo e(!empty($val->last_name) ? $val->last_name : ''); ?><br>
                                          <small class="d-inline-block player_name player_name_red">$<?php echo e($val->svalue); ?>m 
                                            <?php $playerTeam = getPrimaryTeamName($val->team_id); if(!empty($playerTeam)){ ?>
                                            <small class="t_icon" data-html="true" data-toggle="tooltip" title="" data-original-title="<?php echo e($playerTeam); ?>">T</small>
                                            <?php } ?>
                                        </small>
                                      </span>
                                    </span>
                                    </a>
                                </td>
                               <td><?php echo e($position[$val->position]); ?></td> 

                                <!--  <td>
                                    <?php if($val->position == 1): ?>
                                        <img width="20px"  src="<?php echo e(asset('img/balla.png')); ?>" alt="Bat">
                                    <?php elseif($val->position == 2): ?>
                                        <img width="20px"  src="<?php echo e(asset('img/ball.png')); ?>" alt="Ball">
                                    <?php elseif($val->position == 3): ?>
                                        <img width="20px"  src="<?php echo e(asset('img/balla.png')); ?>" alt="Bat"><img width="20px" src="<?php echo e(asset('img/ball.png')); ?>" alt="Ball">
                                    <?php else: ?>
                                        <img width="20px"  src="<?php echo e(asset('img/balla.png')); ?>" alt="Bat">
                                    <?php endif; ?>
                                </td> -->

                                <td><?php echo e($val->selected_by); ?>%</td>
                                <td><?php echo e(!empty($val->score_count) ? round($val->score_count) : 0); ?></td>



                                
                                <td class="d_none sum_rs"><?php echo e(isset($other_data[$val->id]['sum_rs']) ? $other_data[$val->id]['sum_rs'] : 0); ?></td>
                                <td class="d_none sum_fours"><?php echo e(isset($other_data[$val->id]['sum_fours']) ? $other_data[$val->id]['sum_fours'] : 0); ?></td>
                                <td class="d_none sum_sixes"><?php echo e(isset($other_data[$val->id]['sum_sixes']) ? $other_data[$val->id]['sum_sixes'] : 0); ?></td>
                                <td class="d_none sum_wks"><?php echo e(isset($other_data[$val->id]['sum_wks']) ? $other_data[$val->id]['sum_wks'] : 0); ?></td>
                                <td class="d_none sum_mdns"><?php echo e(isset($other_data[$val->id]['sum_mdns']) ? $other_data[$val->id]['sum_mdns'] : 0); ?></td>
                                <td class="d_none sum_hattrick"><?php echo e(isset($other_data[$val->id]['sum_hattrick']) ? $other_data[$val->id]['sum_hattrick'] : 0); ?></td>
                                <td class="d_none sum_cs"><?php echo e(isset($other_data[$val->id]['sum_cs']) ? $other_data[$val->id]['sum_cs'] : 0); ?></td>
                                <td class="d_none sum_sts"><?php echo e(isset($other_data[$val->id]['sum_sts']) ? $other_data[$val->id]['sum_sts'] : 0); ?></td>
                                <td class="d_none sum_overs"><?php echo e(isset($other_data[$val->id]['sum_overs']) ? $other_data[$val->id]['sum_overs'] : 0); ?></td>
                                <td class="d_none pcap"><?php echo e(isset($other_data[$val->id]['pcap']) ? $other_data[$val->id]['pcap'] : 0); ?>%</td>
                                <td class="d_none pvcap"><?php echo e(isset($other_data[$val->id]['pvcap']) ? $other_data[$val->id]['pvcap'] : 0); ?>%</td>
                                <td class="d_none"><?php echo e($val->svalue); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   <?php else: ?> 
                    <tr><td colspan="5">No players have yet been added to the game.</td></tr>

                    <?php endif; ?>

</tbody>
   
                        </table>
                    </div>
                </div>
    </section>
        <div class="container">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
            </div>
        </div>
    </div>
</div>






<!-- Modal -->
<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> 
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">


<section class="status_tab_block mb-2">
            <div class="">
                
                <div class="player_star"><img src="img/player_star_bg.jpg" alt="player"></div>
                <div class="tab_content_sec border">
                    <div class="player_star_info">
                        <div class="player_star_pic position-relative mb-3"><img style="width: 200px; height: 130px; " class="player_image" src="img/player_star.jpg" alt="player_star"></div>
                        <h3 class="player_star_name text-center"><small class="player_name"></small><span class="club_name"></span></h3>
                    </div>
                    <div class="player_star_data game_info_block py-4 mx-3 mx-md-5">
                        <ul class="game_week_info_list d-flex my-n2 mb-4">
                            <li class="d-inline-flex align-items-center p-3 my-2">
                                <div class="w-100 game_week_info d-flex align-items-center">
                                    <div class="game_week_info_icon text-center"><img src="img/game_week_info_icon10.png" alt="game_week_info"></div>
                                    <div class="game_week_info_status pl-2"><span class="player_fps"></span> <small class="mt-1">Fantasy Pts</small></div>
                                </div>
                            </li>
                            <li class="d-inline-flex align-items-center p-3 my-2">
                                <div class="w-100 game_week_info d-flex align-items-center">
                                    <div class="game_week_info_icon text-center"><img src="img/game_week_info_icon11.png" alt="game_week_info"></div>
                                    <div class="game_week_info_status pl-2"><span class="player_value"></span> <small class="mt-1">Value</small></div>
                                </div>
                            </li>
                            <li class="d-inline-flex align-items-center p-3 my-2">
                                <div class="w-100 game_week_info d-flex align-items-center">
                                    <div class="game_week_info_icon text-center"><img src="img/game_week_info_icon9.png" alt="game_week_info"></div>
                                    <div class="game_week_info_status pl-2"><span class="selected_by"></span>% <small class="mt-1">Selected by</small></div>
                                </div>
                            </li>
                        </ul>
                        
                        <div class="games_tabs_sec mt-5">
                            <ul class="nav nav-tabs d-flex" role="tablist">
                              <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#form" role="tab" aria-selected="true">Form</a></li>
                              <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#game_log" role="tab" aria-selected="false">Game Log</a></li>
                            </ul>
                            <div class="tab-content border p-4 mb-3">
                              <div class="tab-pane fade show active" id="form">
                              <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                              </div>
                              <div class="tab-pane fade" id="game_log">
                                  <div class="table-responsive">                                
                                  <table class="table table-hover">
                                    <thead>
                                      <tr>
                                        <th scope="col">Runs</th>
                                        <th scope="col">4s</th>
                                        <th scope="col">6s</th>
                                        <th scope="col">Maiden</th>
                                        <th scope="col">Wks</th>
                                        <th scope="col">FP</th>      
                                      </tr>
                                    </thead>
                                    <tbody class="player_last_5">

                                    </tbody>
                                  </table>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>








      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      
      </div>
    </div>
  </div>
</div> -->





<script type="text/javascript">


var sc_width = screen.width;

$( document ).ready(function() {
 if(sc_width < 640){ 
  // alert(sc_width); 
 
  $('.colmn1').text('Player');
  $('.colmn2').text('POS');
  $('.colmn3').text('PIT');
  $('.colmn4').text('FPS');
 

}
});


    
/*var table =   $('#example').DataTable({"pageLength": 25,"lengthChange": false,  language: { search: '', searchPlaceholder: "Search" }});*/


//  // table.fnSort( [ [2,'desc'] ] );
//  // table.order([0, 'desc']).draw(); 
//  table.fnSort( [ [1,'desc'] ] ).draw(); 



 table =   $('#example').DataTable({"pageLength": 25,"lengthChange": false,columnDefs: [
        { "width": "400px", "targets": [0] },       
      ] , language: { search: '', searchPlaceholder: "Search" }, "order": [[ 3, "desc" ]]});

/*var table = $('#example').DataTable( {  
        pageLength: 25 ,    
        language: { search: '', searchPlaceholder: "Search" },   
        columnDefs: [
        { "width": "450px", "targets": [0,1] },       
      ]
    } ); */ 

  $('#example_filter').prepend('<label class="ml-1 sortBy_class" style="width:25%;"><select id="customselect" class="custom-select"><option value="">Sort By</option><option value="1">Player (A-Z)</option><option value="2">Player (Z-A)</option><option value="3">FP (High to Low)</option><option value="4">FP (Low to High)</option><option value="5">F. Value (H to L)</option><option value="6">F. Value (L to H)</option></select></label>'); 



 $('#example_filter').prepend('<label style="width:25%;"><select id="extra_colmns" class="custom-select"><option>Add Col.</option><option class="sum_rs">Runs</option><option class="sum_fours">Fours</option><option class="sum_sixes">Sixes</option><option class="sum_wks">Wickets</option><option class="sum_mdns">Maidens</option><option class="sum_hattrick">Hattrick</option><option class="sum_cs">Catches</option><option class="sum_sts">Stump</option><option class="sum_overs">Overs</option><option class="pcap">PCap  (%)</option><option class="pvcap">PVCap (%)</option></select></label>'); 




$(document).on('change', '#customselect', function(){ 

  var temp = parseInt($(this).val()); 

   // alert(temp); 
    // table.fnSort( [ [temp,'asc'] ] ).draw(); 
    if(temp == 1){
          table.order([0, 'asc']).draw(); 
    }else if(temp == 2){
          table.order([0, 'desc']).draw(); 
    }else if(temp == 3){
          table.order([3, 'desc']).draw(); 
    }else if(temp == 4){
          table.order([3, 'asc']).draw(); 
    }else if(temp == 5){
          table.order([15, 'desc']).draw();
    }else if(temp == 6){
          table.order([15, 'asc']).draw();
    }
 
}); 


$(document).on('change', '#extra_colmns', function(){ 
    var tmp_cls = $('#extra_colmns :selected').attr('class');
    $('.d_none').css('display', 'none');
    $('.'+tmp_cls).css('display', 'block');
    table.rows().eq(0).each( function ( index ) {
        var row = table.row( index );
     $(row.node()).find('.d_none').css('display', 'none');


    } );
    table.rows().eq(0).each( function ( index ) {
        var row = table.row( index );
     $(row.node()).find('.'+tmp_cls).css('display', 'block');


    } );



    
}); 


</script>


<script type="text/javascript">
  
/*
var chartt = Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
    
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Fantasy Points'
        }
    },
    // tooltip: {
    //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
    //         '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
    //     footerFormat: '</table>',
    //     shared: true,
    //     useHTML: true
    // },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: '',
        data: []

    }, 
    // {
    //     name: 'New York',
    //     data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

    // }, {
    //     name: 'London',
    //     data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

    // }, {
    //     name: 'Berlin',
    //     data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

    // }
    ]
});




// $('#temptemptemp').click(function() {
//   chartt.series[0].setData([48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2],true);
//   // chartt.series[1].setData([49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],true);
//   // chartt.series[2].setData([49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],true);
//   // chartt.series[3].setData([49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],true);
//   chartt.xAxis[0].setCategories(['Jan', 'Jan', 'Jan', 'Jan', 'Jan', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
// });





// $('.dt_player_name').click(function() {
   $(document).on("click",".dt_player_name",function() { 

   var player_id =  $(this).attr('player_id'); 

    $.ajax({
      url: 'get-player-data',
      type: 'POST',
      data: {_token: '<?php echo e(csrf_token()); ?>', player_id: player_id},
      dataType: 'HTML',
      success: function (data) { 
        var obj = $.parseJSON(data);
        // console.log(obj.player_name); 
        $('#exampleModal .player_name').text(obj.player_name);
        // $('#exampleModal .club_name').text('Western Australia');
        if(obj.player_image)
          $('#exampleModal img.player_image').attr('src', '<?php echo e(asset("uploads/player")); ?>/'+obj.player_image);
        $('#exampleModal .player_fps').text(obj.fantasy_points);
        $('#exampleModal .player_value').text(obj.player_price);

        // console.log(obj); 
        var table_str = ''; 
        obj.last_5_record.forEach(function (item, index) {
          table_str += '<tr>'
          table_str += '<td>'+(item.runs ? item.runs : 0)+'</td>'; 
          table_str += '<td>'+(item.four ? item.four : 0)+'</td>'; 
          table_str += '<td>'+(item.six ? item.six : 0)+'</td>'; 
          table_str += '<td>'+(item.maiden ? item.maiden : 0)+'</td>'; 
          table_str += '<td>'+(item.wks ? item.wks : 0)+'</td>'; 
          table_str += '<td>'+(item.fp ? item.fp : 0)+'</td>'; 
          table_str += '</tr>'
        });
        if(!table_str)  table_str +='<tr><td align="center" colspan="6">No record is yet available.</td></tr>' 
        $('#exampleModal .selected_by').text(obj.selected_by); 
        $('#exampleModal .player_last_5').html(table_str); 
// player_last_5
obj.last_10_record.fx_month_fp = obj.last_10_record.fx_month_fp.map( Number );
chartt.series[0].setName(obj.player_name,true);
 chartt.series[0].setData(obj.last_10_record.fx_month_fp,true);
  chartt.xAxis[0].setCategories(obj.last_10_record.fx_month);



      }
   }); 



});

$('#exampleModal').on('hidden.bs.modal', function () {
  $('#exampleModal .player_name').text('');
 $('#exampleModal img.player_image').attr('src', '<?php echo e(asset("img/logo_white.png")); ?>');
  $('#exampleModal .player_fps').text('');
  $('#exampleModal .player_value').text('');
  $('#exampleModal .selected_by').text(''); 
   $('#exampleModal .player_last_5').html(''); 
chartt.series[0].setName([],true);
 chartt.series[0].setData([],true);
  chartt.xAxis[0].setCategories([]);   
})


$('.append_club_name select').on('change', function(){
    $(this).closest('form').submit();
});
*/ 

/*
$( "#capton_point_x_3" ).click(function() {


      club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
      
      // if(parseInt('<?php echo e(MAX_NO_OF_CAPTON_CARDS); ?>') >= 5){
      //   showMessage('Your max. limit of capton card reached.',"error");
      //   return false;
      // }


  toastr.success("<button type='button' id='confirm_capton_point_x_3' style='padding: 1px 2px; line-height: 1; ' class='btn clear'>OK</button> &nbsp; &nbsp; <button type='button'  style='padding: 1px 2px; line-height: 1; '  class='btn clear'>Cancel</button>",'Activate Capton Card?',
  {
      closeButton: false,
      preventDuplicates: true,
     preventOpenDuplicates: true,
      allowHtml: true,
      timeOut: 110000 ,
      onShown: function (toast) {
          $("#confirm_capton_point_x_3").click(function(){
            console.log('helllo'); 
            $.ajax({
              url: 'activate-capton-card',
              type: 'POST',
              data: {_token: '<?php echo e(csrf_token()); ?>',  club_id: club_id},
              dataType: 'HTML',
              success: function (data) { 
                if(data == 1){
                  showMessage('Capton Card activated successfully.',"success");
                }else if(data == 0){
                  showMessage('To activate Capton card, please create your Team first.',"error");
                }else if(data == 2){
                  showMessage('Capton card already active for this week.',"error");
                }else if(data == 3){
                  showMessage('Your max. limit of capton card reached.',"error");
                }else {
                  showMessage('Internal error occured.',"error");
                }
              }
            }); 
          });
        }
  });

});

*/ 
$('body').tooltip({
    selector: '[data-toggle="tooltip"]'
});
</script>




<style type="text/css">
   #example_wrapper .dataTables_info { display: none; }
   /*#example_filter { text-align: right; }*/
 
   .d_none{ display: none; }
   table.dataTable thead .sorting:before, table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_desc:after { display: none; }
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/common/stats.blade.php ENDPATH**/ ?>
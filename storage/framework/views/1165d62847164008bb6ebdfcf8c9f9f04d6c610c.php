<?php $__env->startSection('content'); ?>
<style type="text/css">
.model_extended .modal-dialog{
	width: 1266px;
}
.chosen-container-single .chosen-single{
    height:34px !important;
    padding:3px 6px;
}
</style>
<script>
    jQuery(document).ready(function(){
        $(".choosen_selct").chosen();
    });
</script>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Player")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Player")); ?></li>
	</ol>
</section>
<section class="content"> 
	
	<div class="row">
		<?php echo e(Form::open(['role' => 'form','url' => 'admin/player','class' => 'mws-form',"method"=>"get"])); ?>

		<?php echo e(Form::hidden('display')); ?>	
			<?php if(auth()->guard('admin')->user()->id == 1): ?>
				<div class="col-md-2 col-sm-3">
		                <div class="form-group ">  
		                    <div class="form-group ">  
		                    <?php echo e(Form::select('game_mode',[null => 'Please Select Mode'] + Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''), ['class' => 'form-control choosen_selct game_mode'])); ?>

		                    </div>
		                </div>
		            </div>
		            <div class="col-md-2 col-sm-3">
		                <div class="form-group ">  
		                    <div class="form-group get_game_name">  
		                    <?php echo e(Form::select('game_name',array(''=>trans('Select Game')),((isset($searchVariable['game_name'])) ? $searchVariable['game_name'] : ''), ['class' => 'form-control choosen_selct'])); ?>

		                    </div>
		                </div>
		            </div>
			<?php endif; ?>		
			<!-- <div class="col-md-2 col-sm-2">
				<div class="form-group ">  
				<?php echo e(Form::text("full_name",(isset($searchVariable['full_name'])) ? $searchVariable['full_name'] : '',['class' => 'form-control','id'=>"full_name", 'placeholder' => 'Enter player name'])); ?>

				</div>
			</div>
 -->
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select(
						 'position',
						 [null => 'Please Select Position'] + $position,
						 (isset($searchVariable['position'])) ? $searchVariable['position'] : '',
						 ['id' => 'position','class'=>'form-control choosen_selct']
						)); ?>

				</div>
			</div>
		<!-- 	<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select(
						 'svalue',
						 [null => 'Please Select Value'] + $svalue,
						 (isset($searchVariable['svalue'])) ? $searchVariable['svalue'] : '',
						 ['id' => 'svalue','class'=>'form-control']
						)); ?>

				</div>
			</div>
 -->
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select(
						 'is_active',
						 [null => 'Choose Status'] + [1 => 'Active', 0 => 'Inactive'],
						 (isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : '',
						 ['id' => 'is_active','class'=>'form-control choosen_selct']
						)); ?>

				</div>
			</div>


			<div class="col-md-2 col-sm-2">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> <?php echo e(trans('Search')); ?></button>
				<a href="<?php echo e(URL::to('admin/player')); ?>"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> <?php echo e(trans("Reset")); ?></a>
			</div>
			
			<?php echo e(Form::close()); ?>

			<div class="col-md-2 col-sm-2 ">
			<div class="form-group ">  
				 <button type="button" class="btn btn-success btn-small" id="addPlayer"><?php echo e(trans("Add New Player")); ?></button>
			</div>
		</div>
	</div>
	
		<?php if(Auth::guard('admin')->user()->game_mode == 1 && Auth::guard('admin')->user()->is_game_activate == 1): ?>
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<a href="<?php echo e(URL::to('admin/purchase-player-packs')); ?>"  class="btn btn-success" style="margin:0;">Purchase Plan</a>
				<a href="<?php echo e(asset('inputCSV.csv')); ?>"  class="btn btn-success" style="margin:0;">Download Sample Player</a>
				<a href="javascript:void(0)" id="addImportPlayer" class="btn btn-success" style="margin:0;">Import Player</a>
			</div>
		</div>
		<?php else: ?>
			<div class="row">
			<div class="col-md-6 col-sm-6">
				<a href="<?php echo e(asset('inputCSV.csv')); ?>"  class="btn btn-success" style="margin:0;">Download Sample Player</a>
				<a href="<?php echo e(URL::to('admin/player/add-import-player')); ?>"  class="btn btn-success" style="margin:0;">Import Player</a>
			</div>
		</div>
		<?php endif; ?>	
	<br>
	<?php if(Auth::guard('admin')->user()->game_mode == 1): ?>
	<div class="row">
		<div class="col-md-4">
			<div class="info-box">
				<span class="info-box-icon bg-green"><i class="fa fa-product-hunt"></i></span>
				<div class="info-box-content">
				  <span class="info-box-text"><b>Free Player</b></span>
				  <span class="info-box-number"> <?php echo e(MAXIMUMPLAYER); ?> </span>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="info-box">
			<span class="info-box-icon bg-green"><i class="fa fa-product-hunt"></i></span>
			<div class="info-box-content">
			   <span class="info-box-text"><b>Free Used Player</b></span>
			  <span class="info-box-number"><?php echo e($freeusedPlayer); ?></span>
			</div>
		  </div>
		</div>
		<div class="col-md-4">
			<div class="info-box">
			<span class="info-box-icon bg-green"><i class="fa fa-product-hunt"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text"><b>Available Free Player</b></span>
			  <span class="info-box-number"><?php echo e($availableFreePlayer); ?></span>
			</div>
		  </div>
		</div>
	</div> 
	<div class="row">
		<div class="col-md-4">
			<div class="info-box">
			<span class="info-box-icon bg-orange"><i class="fa fa-product-hunt"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text"><b>Paid Player</b></span>
			  <span class="info-box-number"><?php echo e($purchasePlayerSum); ?></span>
			</div>
		  </div>
		</div>
		<div class="col-md-4">
			<div class="info-box">
				<span class="info-box-icon bg-orange"><i class="fa fa-product-hunt"></i></span>
				<div class="info-box-content">
				  <span class="info-box-text"><b>Paid Used Player</b></span>
				  <span class="info-box-number"> <?php echo e($usedPaidPlayer); ?> </span>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="info-box">
				<span class="info-box-icon bg-orange"><i class="fa fa-product-hunt"></i></span>
				<div class="info-box-content">
				  <span class="info-box-text"><b>Available Paid Player</b></span>
				  <span class="info-box-number"> <?php echo e($availablePaidPlayer); ?> </span>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
					<th width="11%">Image</th>
					<th width="11%">
							<?php echo e(link_to_route(
									"player.index",
									trans("Full Name"),
									array(
										'sortBy' => 'full_name',
										'order' => ($sortBy == 'full_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'full_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'full_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>

						<th width="11%">
							<?php echo e(link_to_route(
									"player.index",
									trans("Value"),
									array(
										'sortBy' => 'svalue',
										'order' => ($sortBy == 'svalue' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'svalue' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'svalue' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
					<?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
						<th width="11%">
							<?php echo e(link_to_route(
									"player.index",
									trans("Club Name"),
									array(
										'sortBy' => 'club_name',
										'order' => ($sortBy == 'club_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'club_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'club_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
					<?php endif; ?>
					<th width="11%">
							<?php echo e(link_to_route(
									"player.index",
									trans("Position"),
									array(
										'sortBy' => 'position_name',
										'order' => ($sortBy == 'position_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'position_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'position_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"player.index",
									trans("Status"),
									array(
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									"player.index",
									trans("Created On"),
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							Action
						</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr class="items-inner">
							<td>
								<?php if(File::exists(PLAYER_IMAGE_ROOT_PATH.$record->image) && !empty($record->image)): ?>
									<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo PLAYER_IMAGE_URL.$record->image; ?>">
										<div class="usermgmt_image">
											<img style="height: 60px; "  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&cropratio=1:1&image='.PLAYER_IMAGE_URL.'/'.$record->image ?>">
										</div>
									</a>
								<?php else: ?>
									<img  style="height: 60px; "  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&cropratio=1:1&image='.WEBSITE_IMG_URL.'no_image.png' ?>">
								<?php endif; ?>
							</td>
							<td>
								<?php echo e($record->full_name); ?> 
							</td>
							<td>
								$<?php echo e($record->svalue); ?>m 
							</td>
							<?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
								<td>
									<?php echo e($record->club_name); ?>

								</td>
							<?php endif; ?>
							<td>
								<?php echo e($record->position_name); ?>

							</td>
							
							<td>
								<?php if($record->is_active	==1): ?>
									<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
								<?php else: ?>
									<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
								<?php endif; ?>
							</td>
							<td>
								<?php echo e(date(config::get("Reading.date_format"),strtotime($record->created_at))); ?>

							</td>
							<td data-th='Action'>
								<a title="<?php echo e(trans('Edit')); ?>" href="<?php echo e(URL::to('admin/player/edit-player/'.$record->id)); ?>" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>
								<a href="<?php echo e(URL::to('admin/player/view-player/'.$record->id)); ?>" title="<?php echo e(trans('View')); ?>" class="btn btn-info">
									<i class="fa fa-eye"></i>
								</a>
								<?php $checkPlayerId = playerIdInTeam($record->id); ?>

								<?php if($checkPlayerId == 0): ?>
									<?php if($record->is_active == 1): ?>
										<a  title="Click To Deactivate" href="<?php echo e(URL::to('admin/player/update-status/'.$record->id.'/0')); ?>" class="btn btn-success btn-small status_any_item1"><span class="fa fa-check"></span>
										</a>
									<?php else: ?>
										<a title="Click To Activate" href="<?php echo e(URL::to('admin/player/update-status/'.$record->id.'/1')); ?>" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
										</a> 
									<?php endif; ?>
									<a title="<?php echo e(trans('Delete')); ?>" href="<?php echo e(URL::to('admin/player/delete-player/'.$record->id)); ?>"  class="delete_any_item btn btn-danger">
										<i class="fa fa-trash-o"></i>
									</a> 
								<?php endif; ?>
								
							</td>
						</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="9" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div> 
</section> 
<div class="modal fade model_extended" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Player</h4>
              </div>
              <?php echo e(Form::open(['role' => 'form','url' => 'admin/player/add-player','class' => 'mws-form','files'=>'true','id'=>'add_player'])); ?>

              <div class="modal-body">
              	 <?php if(Auth::guard('admin')->user()->user_role_id != SUPER_ADMIN_ROLE_ID): ?>
              		<?php echo e(Form::hidden('club',Auth::guard('admin')->user()->id)); ?>

              	<?php endif; ?>
                <table class="table table-responsive table-bordered lastrow_feature_detail" width="100%">
				    <thead>
				      <tr>
				        <th width="15%">First Name*</th>
				        <th width="15%">Last Name*</th>
				        <th width="15%">Position*</th>
				     <!--    <th width="14%">Category</th> -->
				        <th width="15%">Value*</th>
				        <th width="15%">Bat style</th>
				        <th width="15%">Bowl style</th>
				        <?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
				       	 <th width="15%">Club</th>
				       	<?php endif; ?>
				        <th width="15%">Image</th>
				        <td><input  type="button" value="Add More" class="btn btn-primary" id="add_more_feature_detail"/></td>
				      </tr>
				    </thead>
				    <tbody>
				    	<tr class="add_feature_detail" id="0">
					        <td>
					        	<div class="mws-form-item add_feature_input">
					        		<?php if(Auth::guard('admin')->user()->user_role_id != SUPER_ADMIN_ROLE_ID): ?>
					        			<?php echo e(Form::hidden("data[0][club]",auth()->guard('admin')->user()->id,['id'=>'last_name0'])); ?>	
					        		<?php endif; ?>
					        		<?php echo e(Form::text("data[0][first_name]",'',['class' => 'form-control','id'=>"data.0.first_name"])); ?> 
					        		<div class="help-inline"></div>
					       		</div>
					       </td>
					        <td><div class="mws-form-item add_feature_input">
					        	<?php echo e(Form::text("data[0][last_name]",'',['class' => 'form-control','id'=>'last_name0'])); ?><div class="help-inline"></div></div></div></td>
					        <td>
					        	<div class="mws-form-item add_feature_input">
					        	<?php echo e(Form::select("data[0][position]",[null => 'Select Position'] + $position,'',['id' => 'position','class'=>'form-control','id'=>'last_name0'])); ?>

					        	<div class="help-inline"></div>
					        	</div>
					        </td>
					       <!--  <td><div class="mws-form-item add_feature_input"><?php echo e(Form::select("data[0][category]",[null => 'Select Category'] + $category,'',['id' => 'category','class'=>'form-control','id'=>'category0'])); ?><div class="help-inline"></div></div></td> -->
					        <td><div class="mws-form-item add_feature_input">
					        	<?php echo e(Form::select("data[0][value]",[null => 'Select Value'] + $svalue,'',['id' => 'svalue0','class'=>'form-control','id'=>'svalue0'])); ?>


					        	<div class="help-inline" id="svalueerror0"></div></div></td>
					        <td>
					        	
						<?php echo e(Form::select(
						 'data[0][bat_style]',
						 [null => 'Please Select Bat Style'] + $batStyle,
						 isset($userDetails->batStyle) ? $userDetails->batStyle :'',
						 ['id' => 'batStyle','class'=>'form-control ']
						)); ?>


					        </td>
					        <td>
					        	
						<?php echo e(Form::select(
						 'data[0][bowl_style]',
						 [null => 'Please Select Bowl Style'] + $bowlStyle,
						 isset($userDetails->bowl_style) ? $userDetails->bowl_style :'',
						 ['id' => 'bowl_style','class'=>'form-control ']
						)); ?>

					        </td>

					        <?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
					        <td>
					         	<div class="mws-form-item add_feature_input">
					         		<?php echo e(Form::select("data[0][club]",[null => 'Select Club'] + $cludDetails,'',['id' => 'club','class'=>'form-control '])); ?>

					         		<div class="help-inline"></div>
								</div>
					        </td>
					        <?php endif; ?>
					         <td><div class="mws-form-item"><?php echo e(Form::file("data[0][image]", array('accept' => 'image/*','id' => 'image'))); ?><div class="help-inline"></div></div></td>
					         <td></td>
					     </tr>
					</tbody>
				</table>
              </div>
              <?php echo e(Form::close()); ?>


              <div class="modal-footer">
                <input type="button" value="Save" class="btn btn-primary" onclick="save_player();">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<script>
$('#addPlayer').on('click', function() { 
	$('#loader_img').show();
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:'<?php echo e(url("admin/check-player-count")); ?>',
		'type':'post',
		success:function(r){
			error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
				show_message(data['msg'],"error");
            }else{            	
            	$('#modal-default').modal('show');
            }
			$('#loader_img').hide();
		}
	});
});
$('#addImportPlayer').on('click', function() { 
	$('#loader_img').show();
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:'<?php echo e(url("admin/check-player-count")); ?>',
		'type':'post',
		success:function(r){
			error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
				show_message(data['msg'],"error");
            }else{            	
            	window.location.href   =  "<?php echo e(url('admin/player/add-import-player')); ?>";
            }
			$('#loader_img').hide();
		}
	});
});
$('#add_more_feature_detail').on('click', function() {
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	var get_last_id			=	$('.lastrow_feature_detail:last tr:last').attr('rel');
	if(typeof get_last_id === "undefined") {
		get_last_id			=	0;
	}
	var first_name = $("#first_name"+get_last_id).val();
	var counter  = parseInt(get_last_id) + 1;
	if((first_name != '')){ 
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url:'<?php echo e(url("admin/add-more-player")); ?>',
			'type':'post',
			data:{'counter':counter},
			success:function(response){
				$('#loader_img').hide();
				$('.add_feature_detail').last().after(response);
			}
		});
	}else{    
		$(".add_feature_input").each(function(){  
			var dataVal = $(this).find("input,text,file,select,file").val();
			if(dataVal == "" || dataVal == "undefined"){
				$(this).find("input,text,select").next().addClass('error');
				$(this).find("input,text,select").next().html(" <?php echo e(trans('This field is required.')); ?> ");
			}else{
				console.log("else box");
			}
		});
		$('#loader_img').hide();
		return false;
	}
});
function del_feature(_this,id){ 
	bootbox.confirm("Are you sure want to remove this ?",
		function(result){
			if(result){
				_this.closest('tr').remove();
			}
		});	
} 
function save_player(){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
	var form = $('#add_player')[0];
	var formData = new FormData(form);
    $.ajax({
        url: '<?php echo e(route("Player.savePlayer")); ?>',
        type:'post',
        data: formData,
		processData: false,
		contentType: false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
                window.location.href   =  "<?php echo e(route('player.index')); ?>";
            }else {
                $.each(data['errors'],function(index,html){ 
                	//alert(index);
                	res = index.split(".");
                	var convertedString = res[0]+'['+res[1]+']'+'['+res[2]+']';
                   $('[name="'+convertedString+'"]').next().addClass('error');
                   $('[name="'+convertedString+'"]').next().html(html);
                    
                });
                $('#loader_img').hide();
            }
            $('#loader_img').hide();
        }
    });
}

$('#add_player').each(function() {
	$(this).find('input').keypress(function(e) {
       if(e.which == 10 || e.which == 13) {
			save_player();
			return false;
        }
    });
});
/*Filter section game mode and game name*/
var game_name = "<?php echo !empty($searchVariable['game_name']) ? $searchVariable['game_name'] :'' ?>";
var game_mode = "<?php echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode'] :'' ?>";
if(game_name !="" || game_mode !=""){
    get_game_name();
}
function get_game_name(){
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:' <?php echo e("admin-get-game-name"); ?> ',
        'type':'post',
        data:{'mode':game_mode,'game_name':game_name},
        success:function(response){ 
            $(".get_game_name").html(response);
        }
    });
}
$(".game_mode").on('change',function(){
    var mode = $(this).val();
    $('#loader_img').show();
     $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:' <?php echo e(route("GamePrize.adminGetGameName")); ?> ',
        'type':'post',
        data:{'mode':mode,'game_name':game_name},
        success:function(response){ 
            $(".get_game_name").html(response);
        }
    }); 
    $('#loader_img').hide();
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/player/index.blade.php ENDPATH**/ ?>
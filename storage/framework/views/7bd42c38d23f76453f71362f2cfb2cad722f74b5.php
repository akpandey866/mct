<table class="table table-hover">
    <tbody>
        <tr>
            <th>Players</th>
            <th>RS</th>
            <th>4S</th>
            <th>6S</th>
            <th>OVRS</th>
            <th>MDNS</th>
            <th>WKS</th>
            <th>CS</th>
            <th>CWKS</th>
            <th>STS</th>
            <th>RODS</th>
            <th>ROAS</th>
            <th>DKS</th>
            <th>HT</th>
            <th>FP</th>
        </tr>
        <tr>
            <?php if(!empty($playerScoreSum)){ ?>
                <td><?php echo e(!empty($playerScoreSum->player_name) ? $playerScoreSum->player_name :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->runs) ? $playerScoreSum->runs :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->fours) ? $playerScoreSum->fours :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->sixes) ? $playerScoreSum->sixes :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->overs) ? $playerScoreSum->overs :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->mdns) ? $playerScoreSum->mdns :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->wks) ? $playerScoreSum->wks :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->cs) ? $playerScoreSum->cs :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->cwks) ? $playerScoreSum->cwks :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->sts) ? $playerScoreSum->sts :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->rods) ? $playerScoreSum->rods :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->roas) ? $playerScoreSum->roas :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->dks) ? $playerScoreSum->dks :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->hattrick) ? $playerScoreSum->hattrick :'-'); ?></td>
                <td><?php echo e(!empty($playerScoreSum->fantasy_points) ? $playerScoreSum->fantasy_points :'-'); ?></td>
            <?php } ?>
        </tr>
    </tbody>
</table><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/player/get_player_list_for_club.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<?php 
  use Carbon\Carbon; 
  ?>
<style type="text/css">
    .position_img_style{
      width: 20px;
      height: : 20px;
  }
  span.t_icon {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: #262a60;
    color: #fff;
    font-size: 0.74rem;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    font-weight: 700;
    padding: 5px 5px 4px 5px;
    box-sizing: border-box;
    cursor: pointer;
}
.team_icon{
  margin-left: 10px;
}
.highcharts-credits {
    display: none;
}
</style>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<div class="body_section">
  <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <section class="status_tab_block mb-5">
    <div class="container mobile_cont">
      <?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <div class="tab_content_sec border p-0 p-md-4">
        <div class=" d-flex align-items-center">
          <h4 class="status_title my-2">Gameweek #<?php echo e($cur_gameweek_no >= 1 ? $cur_gameweek_no : ''); ?>

            <?php if(!empty($club_data)): ?>
            <small>
            <?php /*
              @if(!empty($club_data->lockout_start_date) && (Carbon::parse($club_data->lockout_start_date)->startOfDay() <= Carbon::now()->startOfDay()  &&   Carbon::now()->startOfDay() <= Carbon::parse($club_data->lockout_end_date)->startOfDay())  )
              */  ?>
            <?php /*
              @if(!empty($club_data->lockout_start_date) && (   Carbon::now()->startOfDay() <= Carbon::parse($club_data->lockout_end_date)->startOfDay())  )
                @if( !empty($club_data->lockout_start_day))
                    @if(!$lockout_club ) {{-- && $lockout_time_remaining_seconds - Carbon::now()->timestamp * 1000 > 0 --}}
                      Upcoming Lockout on 
                      {{Carbon::parse($club_data->lockout_start_day)->isoFormat('MMM D YYYY')}}
                    @else
                      Reopen on 
                      {{Carbon::now()->next($club_data->lockout_end_day)->isoFormat('MMM D YYYY')}}
                    @endif
                @endif
              @endif  
              
              */ ?>
            <?php if($force_unlock): ?>
            
            Upcoming Lockout on <?php echo e(Carbon::now()->subDay()->next($club_data->lockout_start_day)->startOfDay()->isoFormat('MMM D YYYY')); ?>

            <?php elseif(!$lockout_club ): ?>
            Upcoming Lockout on
            <?php if(!empty($club_data->lockout_start_date) && Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_date)->startOfDay()): ?>
            <?php if( Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_end_day) < Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)): ?>
            <?php echo e(Carbon::parse($club_data->lockout_start_date)->startOfDay()->isoFormat('MMM D YYYY')); ?>

            <?php else: ?>
            <?php echo e(Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)->startOfDay()->isoFormat('MMM D YYYY')); ?>

            <?php endif; ?>
            <?php elseif(Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_day)->startOfDay()): ?>
            <?php echo e(Carbon::now()->next($club_data->lockout_start_day)->startOfDay()->isoFormat('MMM D YYYY')); ?> 
            <?php else: ?>
            <?php echo e(Carbon::parse($club_data->lockout_start_day)->isoFormat('MMM D YYYY')); ?>

            <?php endif; ?>
            <?php else: ?>
            <span style="color: #ef429e; ">
            <em>Unlocks 
            <?php if(isset($club_data->lockout_end_time[0]) && isset($club_data->lockout_end_time[1]) ): ?>
            <?php echo e('@ '.date("g:i a", strtotime($club_data->lockout_end_time[0].":".$club_data->lockout_end_time[1]))); ?>

            <?php endif; ?>
            on
            
            <?php if(Carbon::parse($club_data->lockout_end_day)->isSameDay(Carbon::now())): ?>
            <?php echo e(Carbon::now()->parse($club_data->lockout_end_day)->isoFormat('MMM D')); ?>

            <?php else: ?>
            <?php echo e(Carbon::now()->next($club_data->lockout_end_day)->isoFormat('MMM D')); ?>

            <?php endif; ?>
            <?php endif; ?>
            </em>
            </span>
            </small>
          </h4>
          <?php /*
            @if(!empty($club_data->lockout_start_date) && (Carbon::parse($club_data->lockout_start_date)->startOfDay() <= Carbon::now()->startOfDay()  &&   Carbon::now()->startOfDay() <= Carbon::parse($club_data->lockout_end_date)->startOfDay())  )
            */ ?>
          <?php if(!empty($club_data->lockout_start_date) && (   Carbon::now()->startOfDay() <= Carbon::parse($club_data->lockout_end_date)->startOfDay())  ): ?>        
          <div class="match_remaining_info text-left text-sm-right ml-auto my-2">
            <!-- <div>Updated on Apr 30, 2019 at 09:22 pm</div> -->
            <span class="match_remaining_time d-inline-flex mt-2" id="demo">
            <?php /*
              @if($lockout_club)
                Locked
              @elseif(!empty($remain_lock_out_time) && is_array($remain_lock_out_time))
                <?php
                echo  (!empty($remain_lock_out_time['d']) ? $remain_lock_out_time['d'].'d' : '').'&nbsp;'; 
                echo (!empty($remain_lock_out_time['h']) ? $remain_lock_out_time['h'].'h' : '').'&nbsp;'; 
                echo (!empty($remain_lock_out_time['m']) ? $remain_lock_out_time['m'].'m' : '').'&nbsp;'; 
                
                
                
                ?>
            Left
            @endif
            */ ?>
            </span>
          </div>
          <?php endif; ?>
          <script>
            // Set the date we're counting down to
            
            var force_unlock = '<?php echo e($force_unlock); ?>' ; 
            
            // alert(force_unlock); 
            
            // if(force_unlock){
            //     document.getElementById("demo").innerHTML = 'Unlocked';
            // }else
             if((parseInt('<?php echo e($lockout_time_remaining_seconds); ?>') * 1000) - new Date().getTime() > 0){
            
            var countDownDate =  parseInt('<?php echo e($lockout_time_remaining_seconds); ?>') * 1000 // new Date("Sep 27, 2019 15:37:25").getTime(); //  parseInt('<?php echo e($lockout_time_remaining_seconds); ?>') * 1000 ; //
            // alert(countDownDate+' '+new Date().getTime()+' '+'<?php echo e(Carbon::now()->timestamp); ?>'+' '+'<?php echo e(time()); ?>');
            
            // Update the count down every 1 second
            var x = setInterval(function() {
            
              // Get today's date and time
              var now = new Date().getTime();
                
              // Find the distance between now and the count down date
              var distance =  countDownDate - now;
            
              // Time calculations for days, hours, minutes and seconds
              var days = Math.floor(distance / (1000 * 60 * 60 * 24));
              var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
              var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
              var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                
              // Output the result in an element with id="demo"
              if(days >= 0 && hours >= 0 && minutes >= 0 && seconds >= 0){
                 document.getElementById("demo").innerHTML = days + "d " + hours + "h "  + minutes + "m " + seconds + "s "; 
              }
            
                
              // If the count down is over, write some text 
              if (distance < 0) {
                clearInterval(x);
                location.reload(true);
                // document.getElementById("demo").innerHTML = "EXPIRED";
              }
            }, 1000);
            
            
            }else{
              document.getElementById("demo").innerHTML = 'LOCKED';
            }
            
            
          </script>
          <?php endif; ?>
        </div>
        <?php $tmpmatch = []; $i = 0; ?>
        <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $temp->id): ?> <!-- 12th man not included --> 
        <?php  preg_match_all('!\d+\.*\d*!', $temp->svalue ,$match);   $tmpmatch[] = end($match)[0]; $i++;  ?>
        <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php //dump($tmpmatch); die;  ?>
        <div class="game_info_block mt-4">
          <div class="row">
            <div class="col-12 col-lg-5">
              <div class="game_user_info_block d-flex">
                <div class="game_user_pic">
                  <!-- <img class="mb-2" src="<?php echo e(asset('uploads/club/'.Auth::user()->club_logo)); ?>" alt="game user"> -->
                  <?php if(!empty(Auth::user()->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.Auth::user()->image)): ?>
                  <img class="mb-2" src="<?php echo e(asset('uploads/club/'.Auth::user()->image)); ?>" alt="game user">
                  <?php elseif(!empty(Auth::user()->club_logo) && File::exists(CLUB_IMAGE_ROOT_PATH.Auth::user()->club_logo)): ?>
                  <img class="cricketclub_logo float-left mr-4 club_logo" src="<?php echo e(CLUB_IMAGE_URL.Auth::user()->club_logo); ?>" alt="cricketclub" >
                  <?php else: ?>                                
                  <img class="mb-2" src="<?php echo e(asset('img/user_pic.jpg')); ?>" alt="game user">
                  <?php endif; ?>
                  <a class="howuse_btn d-block d-sm-flex align-items-center justify-content-center" href="https://myclubtap.freshdesk.com/a/solutions/articles/43000536360" target="_blank"><i class="far fa-play-circle mr-2"></i><span class="how_mobile">How To Use</span></a>
                </div>
                <div class="w-100 game_info pl-4">
                  <div class="game_user_name d-flex align-items-center flex-wrap mb-0">
                    <?php echo e(Auth::user()->full_name); ?> 
                    <!--   <img class="ml-2" src="img/right.png" alt="right"> -->
                  </div>
                  <div class="game_user_name d-flex align-items-center flex-wrap mb-0">
                    <small class="mt-2">Team: <?php echo e(Auth::user()->my_team_name); ?></small> 
                  </div>
                  <ul class="w-100 game_info_list game_info_list_my_team py-2 mt-2">
                    <!--  <li class="px-3 py-1"><i class="fas fa-angle-right"></i> View Gameweeks</li>
                      <li class="px-3 py-1"><i class="fas fa-angle-right"></i> View Gameweek History</li>
                      <li class="px-3 py-1"><i class="fas fa-angle-right"></i> View Traders History</li> -->
                    <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a target="_blank" href="<?php echo e(url('/pages/game-rules')); ?>">Rules</a></li>
                    <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a target="_blank" href="<?php echo e(url('/pages/faq')); ?>">FAQ</a></li>
                    <li class="px-3 py-1" style="cursor: pointer;" data-toggle="modal" data-target="#club_points_model" ><i class="fas fa-angle-right"></i>Points</li>

                   <!--  <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a href="<?php echo e(url('points')); ?>">Gameweek</a></li> -->
                    <!-- <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a href="<?php echo e(url('history')); ?>">Gameweek History</a></li> -->
                    <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a href="<?php echo e(url('trade-history')); ?>">Trade History</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-7 pt-4 pt-lg-0">
              <ul class="game_week_info_list d-flex my-n2 mb-4 mobile_teamdeta  liborderblock">
                <li class="d-inline-flex align-items-center p-3 my-2">
                  <div class="w-100 game_week_info d-flex align-items-center">
                    <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon7.png')); ?>" alt="game_week_info"></div>
                    <?php /* <div class="game_week_info_status pl-2">{{intval($userteam_fantasy_points)}} <small class="mt-1">Overall Pts.</small></div> */ ?>
                    <div class="game_week_info_status pl-2"><?php echo e(!empty($user_teams_data->id) && !empty($temp_points_arr[$user_teams_data->id]) ? $temp_points_arr[$user_teams_data->id] : 0); ?> <small class="mt-1">Overall Pts.</small></div>
                  </div>
                </li>
                <li class="d-inline-flex align-items-center p-3 my-2">
                  <div class="w-100 game_week_info d-flex align-items-center">
                    <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon8.png')); ?>" alt="game_week_info"></div>
                    <div class="game_week_info_status pl-2"><?php echo e(!empty($user_teams_data->id) && !empty($team_ranks_arr[$user_teams_data->id]) ? $team_ranks_arr[$user_teams_data->id] : 0); ?> <small class="mt-1">Overall Rank</small></div>
                  </div>
                </li>
                <li class="d-inline-flex align-items-center p-3 my-2">
                  <div class="w-100 game_week_info d-flex align-items-center">
                    <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon9.png')); ?>" alt="game_week_info"></div>
                    <div class="game_week_info_status pl-2"><?php echo e(!empty($tot_club_users) ? $tot_club_users : 0); ?> <small class="mt-1">Game Users</small></div>
                  </div>
                </li>
                <li class="d-inline-flex align-items-center p-3 my-2">
                  <div class="w-100 game_week_info d-flex align-items-center">
                    <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon10.png')); ?>" alt="game_week_info"></div>
                    <div class="game_week_info_status pl-2"><?php echo e(!empty($gameweek_fantasy_points) ? $gameweek_fantasy_points : 0); ?> <small class="mt-1">Last Week Pts.</small></div>
                  </div>
                </li>
                <li class="d-inline-flex align-items-center p-3 my-2">
                  <div class="w-100 game_week_info d-flex align-items-center">
                    <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon11.png')); ?>" alt="game_week_info"></div>
                    <div class="game_week_info_status pl-2"> $<?php echo $tot_salary;  // echo 100 - array_sum($tmpmatch) < 0 ? number_format((float)array_sum($tmpmatch), 2, '.', '') : 100 ; ?> <small class="mt-1 mb-2">Budget ($m)</small></div>
                  </div>
                </li>
                <li class="d-inline-flex align-items-center p-3 my-2">
                  <div class="w-100 game_week_info d-flex align-items-center">
                    <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon11.png')); ?>" alt="game_week_info"></div>
                    <div class="game_week_info_status pl-2"><?php echo e(!empty($no_of_trade) ? $no_of_trade : 0); ?>/<?php echo e(MAX_NO_OF_TRADE); ?> <small class="mt-1">Free Trades</small></div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row"  id="team_image_sec_move_mobile_parent_row">
        </div>
        <div class="match_info_block border-top p-4 mt-2 mt-md-5 mx-n4">
          <div class="row">
            <div class="col-12 col-lg-8 my-3">
              <div class="row align-items-end">
                <div class="col-12 col-md-6 my-2">
                  <div class="progress">
                    <?php if(!empty($chosen_player)): ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="" style="width: <?php echo e($chosen_player->count() * 9.09090909091); ?>%"></div>
                    <?php else: ?>
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="" style="width: 0%"></div>
                    <?php endif; ?>
                  </div>
                  <?php if(!empty($chosen_player)): ?>
                  <span class="progress_bar_info d-inline-flex mt-2"><?php echo e(round($chosen_player->count() * 9.09090909091)); ?>%</span>
                  <?php else: ?>
                  <span class="progress_bar_info d-inline-flex mt-2">0%</span>
                  <?php endif; ?>
                </div>
                <div class="col-12 col-md-6 my-2">
                  <ul class="game_progress_info d-flex mobile_teamdeta liborderblock Salary_block ">
                    <?php $tmpmatch = []; $i = 0; ?>
                    <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if(!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $temp->id): ?> <!-- 12th man not included --> 
                    <?php  preg_match_all('!\d+\.*\d*!', $temp->svalue ,$match);   $tmpmatch[] = end($match)[0]; $i++;  ?>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php 
                    // code commented because now we need the total salary as the maximum points earned by player till now 
                      // $tot_salary = !empty($tmpmatch) && (array_sum($tmpmatch) > 100) ? (float)array_sum($tmpmatch) : 100 ; 
                    $tot_salary = !empty($tot_salary) && ($tot_salary > 100) ? $tot_salary : 100; 
                      ?>
                    <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-3 my-2"><span id="total_pl_price">$<?php echo e(number_format(($tot_salary > array_sum($tmpmatch) ? $tot_salary-((float)array_sum($tmpmatch)) : 0.00), 2, '.', '')); ?>m</span> <small class="w-100">Budget Left</small></li>
                    <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-3 my-2">
                      <span id="avg_salary">  
                      <?php
                      // dump(array_sum($tmpmatch));
                      // dump($i); 
                        if($i>0){ 
                          echo '$'.number_format((float)( ($tot_salary > array_sum($tmpmatch) ? array_sum($tmpmatch) : $tot_salary )/$i), 2, '.', '').'m'; 
                        }else{
                         echo '$0.00m'; 
                        }
                          ?>
                      </span><small class="w-100">Avg.</small>
                    </li>
                    <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-3 my-2">
                      <span id="salary_used_sec"> $<?php echo  number_format((float)(array_sum($tmpmatch) > $tot_salary ? $tot_salary : array_sum($tmpmatch)), 2, '.', '') ; ?>m </span> <small class="w-100">Budget Used</small>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row align-items-center">
                <div class="col-12 col-md-7 my-2">
                  <ul class="batsman_info d-flex">
                    <?php $bwl_count = 0; $bat_count = 0; $ar_count = 0; $wk_count = 0;  ?>                                        
                    <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($temp->position == 1 ): ?>
                    <?php $bat_count++;  ?>
                    <?php endif; ?>
                    <?php if($temp->position == 2 ): ?>
                    <?php $bwl_count++;  ?>
                    <?php endif; ?>
                    <?php if($temp->position == 3 ): ?>
                    <?php $ar_count++;  ?>
                    <?php endif; ?>
                    <?php if($temp->position == 4 ): ?>
                    <?php $wk_count++;  ?>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php $__currentLoopData = $position; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ky => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-2 active ml-1 mr-1">
                      <small pos_id="<?php echo e($ky); ?>" class="w-100"><?php echo e($val); ?> <i class="fas fa-check ml-1"></i></small>
                      <?php if($ky == 1): ?>
                      <span class="bt_cnt" style="color: #000; "><?php echo e($bat_count); ?></span>&nbsp;of <span class="ml-1">3-4</span>
                      <?php elseif($ky == 2): ?>
                      <span class="bwl_cnt" style="color: #000; "><?php echo e($bwl_count); ?></span>&nbsp;of <span class="ml-1">3-5</span>
                      <?php elseif($ky == 3): ?>
                      <span class="ar_cnt" style="color: #000; "><?php echo e($ar_count); ?></span>&nbsp;of <span class="ml-1">2-3</span>
                      <?php elseif($ky == 4): ?>
                      <span class="wk_cnt" style="color: #000; "><?php echo e($wk_count); ?></span>&nbsp;of <span class="ml-1">1-1</span>
                      <?php endif; ?>
                    </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-2 active ml-1 mr-1" style="width: 50px; ">
                      <small  class="w-100">All <i class="fas fa-check ml-1"></i></small>
                      <!-- <span class="bt_cnt" style="color: #000; "></span>&nbsp;of <span class="ml-1"></span> -->
                    </li>
                    <!-- <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-2"><small class="w-100">WK <i class="fas fa-check ml-1"></i></small>1 of <span class="ml-1">1-1</span></li>
                      <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-2"><small class="w-100">Bat <i class="fas fa-check ml-1"></i></small>4 of <span class="ml-1">3-4</span></li>
                      <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-2"><small class="w-100">AR <i class="fas fa-check ml-1"></i></small>3 of <span class="ml-1">2-1</span></li>
                      <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-2"><small class="w-100">Bowl <i class="fas fa-check ml-1"></i></small>3 of <span class="ml-1">3-5</span></li> -->
                  </ul>
                </div>
                <div class="col-12 col-md-5 my-2">
                  <form class="search_players_form" action="">
                    <div class="search_players position-relative">
                      <input class="form-control" type="text" id="myInputTextFieldSrch" placeholder="Search a Player" />
                      <span class="search_players_icon"><i class="fas fa-search"></i></span>
                    </div>
                  </form>
                </div>
              </div>
              <div class="table-responsive table-mobile mt-3">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th>Player</th>
                      <th style="display: none;">Position</th>
                      <th>Value</th>
                      <th>Points</th>
                      <th>PIT</th>
                      <?php if(!$lockout_club): ?>  
                      <th>Action</th>
                      <?php endif; ?>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $players; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $player): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <?php /*
                        <td><i player_id="{{$player->id}}" player_name="{{$player->full_name}}" player_position="{{$position[$player->position]}}" player_price="{{$svalue[$player->svalue]}}" class="fas fa-star {{ in_array($player->id, $user_teams) ? 'yellow_color' : 'grey_color' }} mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">{{$player->full_name}}</span></td>
                        */ ?>
                      <td data-search="<?php echo e($player->full_name); ?>" data-order="<?php echo e($player->full_name); ?>">
                        <!--  <i  class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i> -->
                        <span style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class="pl-2 dt_player_name" player_id="<?php echo e($player->id); ?>">
                        
                         <?php if($player->position == 1){ ?>
                              <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Batsman"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>bat-icon.png" /></a>
                         <?php }elseif($player->position == 2){ ?>
                              <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Bowler"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>ball-icon.png" /></a>
                        <?php  }elseif ($player->position == 3) { ?>
                              <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="All Rounder"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>bat-ball-icon.png" /></a>
                        <?php  }elseif ($player->position == 4) { ?>
                              <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Wicket-Keeper"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>gloves-icon.png" /></a>
                         <?php } ?>
                          <?php $playerTeam = getPrimaryTeamName($player->team_id); 
                                if(!empty($playerTeam)){
                            ?>
                               <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="<?php echo e($playerTeam); ?>"><small class="t_icon">T</small></a>
                            <?php } ?>
                        <?php echo e(!empty($player->first_name) ? ucfirst(substr($player->first_name, 0, 2).'. ') : ''); ?>

                        <?php echo e(!empty($player->last_name) ? $player->last_name : ''); ?>

                       
                       

                        </span>  
                        <?php if(array_key_exists($player->id, $availability) && ( !empty(Carbon::parse($availability[$player->id]->date_from)) && ( ( Carbon::parse($availability[$player->id]->date_from) >= Carbon::now() && Carbon::now()->diffInDays(Carbon::parse($availability[$player->id]->date_from)) <= 10 ) || Carbon::parse($availability[$player->id]->date_from) <= Carbon::now() ) ) && !empty(Carbon::parse($availability[$player->id]->date_till)) && Carbon::now() <= Carbon::parse($availability[$player->id]->date_till) ): ?>
                        <?php 
                          // dump(Carbon::parse($availability[$player->id]->date_from)); 
                          // dump(Carbon::now()->diffInDays(Carbon::parse($availability[$player->id]->date_from)));  die; 
                          ?>
                        <a  data-html="true" data-toggle="tooltip" title="<?php echo e($player->full_name); ?> will not available from <?php echo e(!empty($availability[$player->id]->date_from) ? Carbon::parse($availability[$player->id]->date_from)->isoFormat('D/M/Y') : '--'); ?> to <?php echo e(!empty($availability[$player->id]->date_till) ? Carbon::parse($availability[$player->id]->date_till)->isoFormat('D/M/Y') : '--'); ?>"><i style="color: #FF0000; " class="fa fa-info-circle" aria-hidden="true"></i></a>
                        <?php endif; ?>
                      </td>
                      <td style="display: none;"><?php echo e($position[$player->position]); ?></td>
                      <td data-order="<?php echo e($player->svalue); ?>">$<?php echo e($player->svalue); ?>m</td>

                      <td><?php echo e(isset($club_players_fantasy_points[$player->id]) ? $club_players_fantasy_points[$player->id] : 0); ?></td>

                      <td data-order="<?php echo e($player->selected_by); ?>"><?php echo e(!empty($player->selected_by) ? $player->selected_by.'%' : '0%'); ?></td>

                      <?php if(!$lockout_club): ?>  
                      <td class="action_plus_minus">
                        <?php if((!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $player->id) || empty($user_teams_data->twelveth_man_id)): ?> <!-- not for 12th player   -->
                        <div class="action_plus_minus_data d-flex align-items-center justify-content-center">
                          <?php if($player->dummy_player == 1): ?>
                          <i class="fa fa-ban mx-2" style="color: <?php echo e(in_array($player->id, $user_teams) ? '#fc0' : '#7a7a7a'); ?> !important; font-size: 18px; "></i>
                          <span style="visibility: hidden;" class="captonbox_disabled <?php echo e($capton_id == $player->id ? 'captonbox_active_disabled' : ''); ?>">C</span>
                          <span style="visibility: hidden;"  class="vice_captonbox_disabled <?php echo e($vice_capton_id == $player->id ? 'vice_captonbox_active_disabled' : ''); ?>">VC</span>
                          <?php elseif(!in_array($player->id, $locked_player)): ?>
                          <i player_id="<?php echo e($player->id); ?>" player_img="<?php echo e(!empty($player->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.$player->image) ? asset('uploads/player/'.$player->image) : ''); ?>" player_name="<?php echo e($player->full_name); ?>" player_position="<?php echo e($position[$player->position]); ?>" player_price="<?php echo e($player->svalue); ?>" class="fas add_rem_player <?php echo e(in_array($player->id, $user_teams) ? 'fa-minus' : 'fa-plus'); ?> mx-2"></i>
                          <span class="captonbox <?php echo e($capton_id == $player->id ? 'captonbox_active' : ''); ?>">C</span>
                          <span class="vice_captonbox <?php echo e($vice_capton_id == $player->id ? 'vice_captonbox_active' : ''); ?>">VC</span>
                          <?php else: ?>
                          <i class="fa fa-ban mx-2" style="color: <?php echo e(in_array($player->id, $user_teams) ? '#fc0' : '#7a7a7a'); ?> !important; font-size: 18px; "></i>
                          <span class="captonbox_disabled <?php echo e($capton_id == $player->id ? 'captonbox_active_disabled' : ''); ?>">C</span>
                          <span class="vice_captonbox_disabled <?php echo e($vice_capton_id == $player->id ? 'vice_captonbox_active_disabled' : ''); ?>">VC</span>
                          <?php endif; ?>
                        </div>
                        <?php else: ?>
                        <span style="color: #fc0;">12th Player</span>
                        <?php endif; ?>
                      </td>
                      <?php endif; ?>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Player</th>
                      <th style="display: none;">Position</th>
                      <th>Value</th>
                      <th>Points</th>
                      <th>PIT</th>
                      <?php if(!$lockout_club): ?>  
                      <th>Action</th>
                      <?php endif; ?>
                    </tr>
                  </tfoot>
                </table>
                <script type="text/javascript">
                  $(document).ready(function(){
                  locked_player = JSON.parse('<?php echo e(json_encode($locked_player)); ?>');
                  
                    var tot_salary = parseFloat('<?php echo e($tot_salary); ?>'); 
                  
                    var sc_width = screen.width;
                    
                    if(sc_width < 640){
                      // alert(sc_width); 
                      $("#team_image_sec_move_mobile").prependTo("#team_image_sec_move_mobile_parent_row"); 
                      
                      setTimeout(function(){ $("#example_info").css('display', 'none'); }, 1000);
                  
                    }
                  
                  var total_player_count_curr = 0; 
                  // $('[data-toggle="tooltip"]').tooltip(); 
                  
                  
                  $('body').tooltip({
                  selector: '[data-toggle="tooltip"]'
                  });
                  
                  // $(document).on('mouseenter','[rel=tooltip]', function(){
                  //     $(this).tooltip('show');
                  // });
                  
                  // $(document).on('mouseleave','[rel=tooltip]', function(){
                  //     $(this).tooltip('hide');
                  // });
                  
                  
                  
                     twelve_player_select = <?php echo e($twelve_player_select); ?>;
                    var twelveth_player_id = 0; 
                      twelveth_man_id = '<?php echo e(!empty($user_teams_data->twelveth_man_id) ? $user_teams_data->twelveth_man_id : ''); ?>';  // assing the 12th man id 
                    // console.log( twelve_player_select ? 'hi' : 'hello'); 
                  
                    var table =   $('#example').DataTable( {
                  "language": {                
                  "infoFiltered": ""
                  },"order": [[ 3, "desc" ]]
                  } );
                   
                    // $('.team_image_sec #refresh_btn').click(function(){
                  $(document).on("click",".team_image_sec #refresh_btn",function() {   
                      wk = 0; 
                      bat = 0; 
                      ar = 0; 
                      bowl = 0; 
                      capton_id = 0; 
                      vice_capton_id = 0; 
                      $('.captonbox_active').removeClass('captonbox_active'); 
                      $('.action_plus_minus .fa-minus').addClass('fa-plus'); 
                      $('.action_plus_minus .fa-minus').removeClass('fa-minus');
                      $('.grid_view_tab ul').empty(); 
                      $('.list_view_tab ul div').empty(); 
                      $('.progress .progress-bar').css('width', '0%'); 
                      $('.progress_bar_info').text('0%'); 
                      $('.tot_player_in_11').text((wk+bat+ar+bowl)+'/'+(twelve_player_select ? 12 : 11)); 
                      // $("#total_pl_price").text('$100.00m'); 
                      $("#total_pl_price").text('$'+ new Number(parseFloat(tot_salary)).toFixed(2) +'m'); 
                      
                    // table.draw()
                    // var data = table.rows().data().find('.fa-minus');
                    // console.log(data); 
                  
                  
                      $('ul.batsman_info span.bt_cnt').text(bat); 
                      $('ul.batsman_info span.bwl_cnt').text(bowl); 
                      $('ul.batsman_info span.ar_cnt').text(ar); 
                      $('ul.batsman_info span.wk_cnt').text(wk); 
                      $('#salary_used_sec').text('$0.00m'); 
                      $('#avg_salary').text('$0.00m'); 
                      
                  
                  
                  
                  table.rows().eq(0).each( function ( index ) {
                  var row = table.row( index );
                  $(row.node()).find('.vice_captonbox_active').removeClass('vice_captonbox_active');
                  $(row.node()).find('.captonbox_active').removeClass('captonbox_active');
                  $(row.node()).find('.fa-minus').addClass('fa-plus').removeClass('captonbox_active fa-minus');
                  
                  } );
                  
                  
                  
                  
                    }); 
                  
                  
                  
                  
                  $(document).on("click",".grid_view_tab i.remove-player",function() {   
                  
                  // var temp_pl_id = $(this).parent('li').attr('id') ; 
                  // $('.grid_view_tab #'+temp_pl_id).remove(); 
                  // var temp_pl_id = temp_pl_id.replace( /^\D+/g, ''); 
                  var temp_pl_id = $(this).parent('li').attr('id') ; 
                  $('.grid_view_tab #'+temp_pl_id).remove(); 
                  var temp_pl_id = temp_pl_id.replace( /^\D+/g, ''); 
                  
                  
                  table.rows().eq(0).each( function ( index ) {
                  var row = table.row( index );
                  
                  if($(row.node()).find('.fa-minus').attr('player_id') == temp_pl_id){
                  var tmp_player_position = $(row.node()).find('.fa-minus').attr('player_position'); 
                  $(row.node()).find('.fa-minus').addClass('fa-plus').removeClass('fa-minus');
                  
                  
                  if($(row.node()).find('.vice_captonbox_active').length > 0 ){
                  $(row.node()).find('.vice_captonbox_active').removeClass('vice_captonbox_active');
                  vice_capton_id = 0; 
                  }
                  if($(row.node()).find('.captonbox_active').length > 0 ){
                  $(row.node()).find('.captonbox_active').removeClass('captonbox_active');
                  capton_id = 0; 
                  }
                  if(tmp_player_position.toLowerCase() == 'wk') wk--; 
                  if(tmp_player_position.toLowerCase() == 'bat') bat--; 
                  if(tmp_player_position.toLowerCase() == 'ar') ar--; 
                  if(tmp_player_position.toLowerCase() == 'bowl') bowl--; 
                  
                  $('ul.batsman_info span.bt_cnt').text(bat); 
                  $('ul.batsman_info span.bwl_cnt').text(bowl); 
                  $('ul.batsman_info span.ar_cnt').text(ar); 
                  $('ul.batsman_info span.wk_cnt').text(wk); 
                  // stop field update
                  $('.progress .progress-bar').css('width', ((wk+bat+ar+bowl)*9.09090909091)+'%'); 
                  $('.progress_bar_info').text(Math.round((wk+bat+ar+bowl)*9.09090909091)+'%'); 
                  $('.tot_player_in_11').text((wk+bat+ar+bowl)+'/'+(twelve_player_select ? 12 : 11)); 
                  
                       
                  $('.team_image_sec #prefix_'+tmp_player_position.toLowerCase()+' li#list'+temp_pl_id).remove(); 
                  
                  
                  
                  
                  }
                  
                  // $(row.node()).find('.vice_captonbox_active').removeClass('vice_captonbox_active');
                  // $(row.node()).find('.captonbox_active').removeClass('captonbox_active');
                  // $(row.node()).find('.fa-minus').addClass('fa-plus').removeClass('captonbox_active');
                  
                  } );
                  
                  
                  
                  var regex = /[+-]?\d+(\.\d+)?/g;
                  var tot_pr = 0.00; 
                  var i = 0; 
                  $( ".pl_price" ).each(function( index ) {
                  i++; 
                  var str = $(this).text();
                  var floats = str.match(regex).map(function(v) { return parseFloat(v); });
                  if(floats.length > 0){
                  // console.log(floats[0]); 
                  tot_pr += floats[0]; 
                  }
                  });
                  
                  
                  
                  
                  // var regex = /[+-]?\d+(\.\d+)?/g;
                  // var total_pl_price = $("#total_pl_price").text(); 
                  
                  // var total_pl_price = total_pl_price.match(regex).map(function(v) { return parseFloat(v); });
                  // $("#total_pl_price").text('$'+(100-parseFloat(tot_pr))+'m'); 
                  // alert(tot_salary); alert(tot_pr); 
                  $("#total_pl_price").text('$'+ new Number(tot_salary-parseFloat(tot_pr) > 0 ? tot_salary-parseFloat(tot_pr) : 0.00).toFixed(2)+'m'); 
                  
                  // alert(tot_salary); 
                  $('#salary_used_sec').text('$'+new Number(parseFloat(tot_pr > tot_salary ? tot_salary : tot_pr)).toFixed(2)+'m'); 
                  // alert(tot_pr); alert(i);
                  if(i>0){
                  $('#avg_salary').text('$'+new Number(parseFloat(tot_pr)/i).toFixed(2)+'m'); 
                  }else{
                  $('#avg_salary').text('$0.00m'); 
                  }
                  
                  }); 
                  
                  // $('#table-filter').on('change', function(){
                     // table.search('AR').draw();   
                     // table.column( 1 ).search( 'bat' ).draw(); 
                  // });
                  var capton_id = parseInt('<?php echo e($capton_id ? $capton_id : 0); ?>');  
                  var vice_capton_id = parseInt('<?php echo e($vice_capton_id ? $vice_capton_id : 0); ?>');  
                  var wk = parseInt('<?php echo e($wk_count ? $wk_count : 0); ?>'); 
                  var bat = parseInt('<?php echo e($bat_count ? $bat_count : 0); ?>'); 
                  var ar = parseInt('<?php echo e($ar_count ? $ar_count : 0); ?>'); 
                  var bowl =  parseInt('<?php echo e($bwl_count ? $bwl_count : 0); ?>'); 
                  // alert(wk); alert(bat); alert(ar); alert(bowl); 
                  // $(".batsman_info li").click(function(){
                   $(document).on("click",".batsman_info li",function() {    
                      $('.batsman_info li small').css({"font-weight": "normal", "color": "#8d8d8d"})
                    var srch_str = $(this).find('small').css({"font-weight": "bold", "color": "#000"}).attr('pos_id'); 
                    // alert(srch_str);
                    if(!srch_str) srch_str = ''; 
                  
                    if(srch_str == 1) srch_str = 'bat' ; if(srch_str == 2) srch_str = 'bowl' ; if(srch_str == 3) srch_str = 'ar' ; if(srch_str == 4) srch_str = 'wk' ; 
                    // alert(srch_str); 
                     table.column( 1 ).search( srch_str ).draw(); 
                  });
                  
                  // $("td i.fa-star").click(function(){
                  $(document).on("click","td i.add_rem_player",function() {    
                  
                      var player_id = $(this).attr('player_id'); 
                      var player_img = $(this).attr('player_img');
                      var player_name = $(this).attr('player_name');
                      var player_position = $(this).attr('player_position');
                      var player_price = $(this).attr('player_price');
                      
                      var flag_12 = twelve_player_select && (wk+bat+ar+bowl == 11) ; 
                      // alert(flag_12); 
                  // alert(wk+bat+ar+bowl); 
                      
                      if($(this).hasClass( "fa-plus" )){
                        var regex = /[+-]?\d+(\.\d+)?/g;
                        var tmp_tot_sal = $('#salary_used_sec').text(); 
                        var tmp_tot_sal = tmp_tot_sal.match(regex).map(function(v) { return parseFloat(v); });
                          total_player_count_curr = $('.pl_price').length + 1  ; 
                          if(twelve_player_select && wk+bat+ar+bowl == 12){
                            showMessage('You can not select more than 12 players.',"error");
                            return false; 
                          }
                  
                  // alert(twelveth_man_id); 
                  // alert('asdf'); alert((!flag_12 || !twelveth_man_id)); alert(!twelveth_man_id); alert((parseFloat(tmp_tot_sal)+parseFloat(player_price))); alert(tot_salary);
                        if((!flag_12 || twelveth_man_id) && (parseFloat(tmp_tot_sal)+parseFloat(player_price)) > tot_salary){
                          // alert(123);
                          // toastr.error('Player price can not exceed $100.'); 
                          showMessage('Player price can not exceed $'+ new Number(parseFloat(tot_salary)).toFixed(2) +'.',"error");
                          return false; 
                        }
                  
                  // var floats = str.match(regex).map(function(v) { return parseFloat(v); });
                  
                  
                          // console.log(player_position.toLowerCase()); 
                  
                            if(player_position.toLowerCase() == 'wk') wk++; 
                            if(player_position.toLowerCase() == 'bat') bat++; 
                            if(player_position.toLowerCase() == 'ar') ar++; 
                            if(player_position.toLowerCase() == 'bowl') bowl++; 
                  
                  // alert(wk); alert(bat); alert(ar); alert(bowl); 
                  // console.log(wk); console.log(bat); console.log(ar); console.log(bowl); 
                  // alert(wk+bat+ar+bowl); toastr.success('Are you the 6 fingered man?')
                  // alert(bat); 
                          if(!flag_12 && (wk > 1 || bat > 4 || ar > 3 || bowl > 5)){
                             if(wk > 1) { wk--; showMessage('You must choose only one WK.',"error"); //toastr.error('You must choose only one WK.');  
                           }
                             if(!flag_12 && bat > 4) { bat--; showMessage('You should not choose more than 4 BAT.',"error"); // toastr.error('You should not choose more than 4 BAT.'); 
                           } 
                             if(!flag_12 && ar > 3) { ar--; showMessage('You should not choose more than 3 AR.',"error"); // toastr.error('You should not choose more than 3 AR.'); 
                           } 
                             if(!flag_12 && bowl > 5) { bowl--; showMessage('You should not choose more than 5 BOWL.',"error"); // toastr.error('You should not choose more than 5 BOWL.'); 
                           } 
                          }else if(!flag_12 && (wk+bat+ar+bowl > 11)){
                  
                            if(player_position.toLowerCase() == 'wk') wk--; 
                            if(player_position.toLowerCase() == 'bat') bat--; 
                            if(player_position.toLowerCase() == 'ar') ar--; 
                            if(player_position.toLowerCase() == 'bowl') bowl--; 
                  
                              // toastr.error('Total 11 players completed, Please remove some player to add more.'); 
                              showMessage('Total 11 players completed, Please remove some player to add more.',"error");
                          }else{
                  
                            var regex = /[+-]?\d+(\.\d+)?/g;
                            var total_pl_price = $("#total_pl_price").text(); 
                  
                            var total_pl_price = total_pl_price.match(regex).map(function(v) { return parseFloat(v); });
                                total_pl_price = parseFloat(total_pl_price); 
                                // alert(total_player_count_curr); alert(twelveth_man_id); 
                            if(!(total_player_count_curr == 12 && !twelveth_man_id)){  // for 12th player no update     
                              $("#total_pl_price").text('$'+ new Number(parseFloat((total_pl_price > player_price ? (total_pl_price - player_price) : 0.00))).toFixed(2)+'m'); 
                            }
                            // alert(total_pl_price); 
                            // console.log(total_pl_price);
                            // alert(total_player_count_curr); 
                            if(total_player_count_curr > 11){
                              twelveth_player_id = player_id ; 
                            }else{
                              twelveth_player_id = 0; 
                            }

                  
                  
                  
                             $('.team_image_sec #prefix_'+player_position.toLowerCase()).append('<li  class="player_data"  id="list'+player_id+'">'+player_name+'<span class="pl_price">$'+player_price+'m</span><span class="c_text"></span><span class="vc_text"></span></li>');  
                             $(this).removeClass('fa-plus'); 
                             $(this).addClass('fa-minus'); 
                  
                  
                             // start field update 
                  // if(!flag_12){
                            $('ul.batsman_info span.bt_cnt').text(bat); 
                            $('ul.batsman_info span.bwl_cnt').text(bowl); 
                            $('ul.batsman_info span.ar_cnt').text(ar); 
                            $('ul.batsman_info span.wk_cnt').text(wk); 
                  // }
                   var player_img = $(this).attr('player_img');
                  if(!player_img) player_img = "<?php echo e(asset('img/wc.png')); ?>"; 
                  
                  var tog_tmp = $("<div />").append($(this).closest('tr').find('a[data-toggle="tooltip"]').clone().addClass('unavailable-player')).html(); 
                  
                  var wk_div = '<li id="img'+player_id+'">\
                  <i class="fas fa-times remove-player"></i>'+tog_tmp+'\
                  <img  src="'+player_img+'" alt=""  width="56" height="60"  style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="'+player_id+'">\
                  <img src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img">\
                  <img src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img">\
                  <span class="player-position">WK</span>\
                  <div class="player-name"> '+player_name+' </div>\
                  <div class="player-points"><span class="team-salary">$'+player_price+'m</span></div>\
                  </li>'; 
                   var player_img = $(this).attr('player_img');
                  if(!player_img) player_img = "<?php echo e(asset('img/bm.png')); ?>"; 
                  
                  var bat_div = '<li id="img'+player_id+'">\
                  <i class="fas fa-times remove-player"></i>'+tog_tmp+'\
                  <img src="'+player_img+'" alt=""  width="56" height="60"  style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="'+player_id+'">\
                  <img src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img">\
                  <img src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img">\
                  <span class="player-position">BAT</span>\
                  <div class="player-name"> '+player_name+' </div>\
                  <div class="player-points"> <span class="team-salary">$'+player_price+'m</span></div>\
                  </li>';
                  var player_img = $(this).attr('player_img');
                  if(!player_img) player_img = "<?php echo e(asset('img/ar.png')); ?>"; 
                  
                  var ar_div = '<li id="img'+player_id+'">\
                  <i class="fas fa-times remove-player"></i>'+tog_tmp+'\
                  <img src="'+player_img+'" alt=""  width="56" height="60"  style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="'+player_id+'">\
                  <img src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img">\
                  <img src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img">\
                  <span class="player-position">AR</span>\
                  <div class="player-name"> '+player_name+' </div>\
                  <div class="player-points"> <span class="team-salary">$'+player_price+'m</span></div>\
                  </li>';
                   var player_img = $(this).attr('player_img');
                  if(!player_img) player_img = "<?php echo e(asset('img/bow.png')); ?>"; 
                  
                  var bow_div = '<li id="img'+player_id+'">\
                  <i class="fas fa-times remove-player"></i>'+tog_tmp+'\
                  <img src="'+player_img+'" alt=""  width="56" height="60"  style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="'+player_id+'">\
                  <img src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img">\
                  <img src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img">\
                  <span class="player-position">Bow</span>\
                  <div class="player-name"> '+player_name+' </div>\
                  <div class="player-points"> <span class="team-salary">$'+player_price+'m</span></div>\
                  </li>'; 
                  
                  
                  
                            if(player_position.toLowerCase() == 'wk'){
                              $('.wk_div').append(wk_div); 
                            }
                            if(player_position.toLowerCase() == 'bat'){
                              $('.bat_div').append(bat_div); 
                            }
                            if(player_position.toLowerCase() == 'ar'){
                              $('.ar_div').append(ar_div); 
                  
                            }
                            if(player_position.toLowerCase() == 'bowl'){
                              $('.bow_div').append(bow_div); 
                  
                            }
                  
                  if(!flag_12){
                             // stop field update
                      $('.progress .progress-bar').css('width', ((wk+bat+ar+bowl)*9.09090909091)+'%'); 
                      $('.progress_bar_info').text(Math.round((wk+bat+ar+bowl)*9.09090909091)+'%'); 
                    }
                  
                          }
                         
                       
                        
                      }else{

                        total_player_count_curr = $('.pl_price').length - 1; 

                          $(this).removeClass('fa-minus'); 
                          $(this).addClass('fa-plus'); 
                  
                        var flag_12 = twelve_player_select && (wk+bat+ar+bowl == 12) ; 
                  
                            var regex = /[+-]?\d+(\.\d+)?/g;
                            var total_pl_price = $("#total_pl_price").text(); 
                  
                            var total_pl_price = total_pl_price.match(regex).map(function(v) { return parseFloat(v); });
                                total_pl_price = parseFloat(total_pl_price);
 // alert(total_player_count_curr); alert(twelveth_man_id); 
                            if(!(total_player_count_curr > 10 && !twelveth_man_id)){  // for 12th player no update , >10 because removing player here
                              $("#total_pl_price").text('$'+ new Number(parseFloat(total_pl_price)+parseFloat(player_price)).toFixed(2) +'m'); 
                            }
                  
                          $('.team_image_sec #prefix_'+player_position.toLowerCase()+' li#list'+player_id).remove(); 
                  
                   $('.team_image_sec .grid_view_tab ul li#img'+player_id).remove(); 
                           if(player_position.toLowerCase() == 'wk') wk--; 
                           if(player_position.toLowerCase() == 'bat') bat--; 
                           if(player_position.toLowerCase() == 'ar') ar--; 
                           if(player_position.toLowerCase() == 'bowl') bowl--; 
                           
                  
                            $('ul.batsman_info span.bt_cnt').text(bat); 
                            $('ul.batsman_info span.bwl_cnt').text(bowl); 
                            $('ul.batsman_info span.ar_cnt').text(ar); 
                            $('ul.batsman_info span.wk_cnt').text(wk); 
                  
                  
                  
                  
                  
                           if($(this).next('span.captonbox').hasClass('captonbox_active')){
                           $(this).next('span.captonbox').removeClass('captonbox_active'); 
                           capton_id = 0; 
                  
                           }
                           if($(this).next().next('span.vice_captonbox').hasClass('vice_captonbox_active')){
                           $(this).next().next('span.vice_captonbox').removeClass('vice_captonbox_active'); 
                           vice_capton_id = 0; 
                           }
                  
                  if(!flag_12){
                      $('.progress .progress-bar').css('width', ((wk+bat+ar+bowl)*9.09090909091)+'%'); 
                      $('.progress_bar_info').text(Math.round((wk+bat+ar+bowl)*9.09090909091)+'%'); 
                  }
                  
                  
                      }
                  
                      $('.tot_player_in_11').text((wk+bat+ar+bowl)+'/'+(twelve_player_select ? 12 : 11)); 
                  
                      
                  
                      
                  
                  
                  
                  var regex = /[+-]?\d+(\.\d+)?/g;
                  var tot_pr = 0.00; 
                  var i = 0; 
                  $( ".pl_price" ).each(function( index ) {
                  i++; 
                  var str = $(this).text();
                  var floats = str.match(regex).map(function(v) { return parseFloat(v); });
                  if(floats.length > 0){
                  // console.log(floats[0]); 
                  tot_pr += floats[0]; 
                  }
                  });
                  
                  // alert(total_player_count_curr); 

         // twelve_player_select = <?php echo e($twelve_player_select); ?>;
         //            var twelveth_player_id = 0; 
         //              twelveth_man_id = '<?php echo e(!empty($user_teams_data->twelveth_man_id) ? $user_teams_data->twelveth_man_id : ''); ?>';  // assing the 12th man id 

                 // alert(twelve_player_select); alert(twelveth_man_id);  
                 if(!(total_player_count_curr == 12 && !twelveth_man_id)){  // for 12th player no update 
                     $('#salary_used_sec').text('$'+new Number(parseFloat(tot_pr > tot_salary ? tot_salary : tot_pr)).toFixed(2)+'m'); 
                 }
       
                  // alert(tot_pr); alert(i);
                  // alert(total_player_count_curr); alert(twelveth_man_id); 
// if(!(total_player_count_curr == 12 && !twelveth_man_id)){  // for 12th player no update 

                  if(i>0){
                    if(!(total_player_count_curr == 12 && !twelveth_man_id)){  // for 12th player no update 
                       $('#avg_salary').text('$'+new Number(parseFloat(tot_pr > tot_salary ? tot_salary : tot_pr)/i).toFixed(2)+'m'); 
                    }
                  }else{
                  $('#avg_salary').text('$0.00m'); 
                  }
                  
                  }); 
                  
                  
                  $('#myInputTextFieldSrch').keyup(function(){
                        table.search($(this).val()).draw() ;
                  })
                  
                  
                  
                  
                  // $('#update_my_team_player_imagebtn').click(function(){
                     $(document).on("click","#update_my_team_player_imagebtn",function() { 
                  $( "#update_my_team_player" ).trigger( "click" );
                  }); 
                  
                  
                  // $('#update_my_team_player').click(function(){
                  $(document).on("click","#update_my_team_player",function() { 
                      var team_player_arr = []; 
                  
                  
                  
                  var flag_12 = twelve_player_select && (wk+bat+ar+bowl == 12) ; 
                  // alert(flag_12); 
                      club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
                      if(!club_id){
                        // toastr.error('Please choose club to create team.'); 
                        showMessage('Please choose club to create team.',"error");
                        return false;
                      }
                  
                      if(twelve_player_select && (wk+bat+ar+bowl < 12)){
                          // toastr.error('You should choose total 11 players.'); 
                          showMessage('You should choose total 12 players.',"error");
                          return false; 
                      }
                  
                      if(!flag_12 && (wk+bat+ar+bowl != 11)){
                          // toastr.error('You should choose total 11 players.'); 
                          showMessage('Your must pick a minimum of 11 players in your team.',"error");
                          return false; 
                      }
                      if(!flag_12 && wk < 1){
                          // toastr.error('You should choose atleast one WK.'); 
                          showMessage('You should choose atleast one WK.',"error");
                          return false; 
                      }   
                      if(!flag_12 && ar < 2){
                         // toastr.error('You should choose atleast two AR.'); 
                         showMessage('You should choose atleast two AR.',"error");
                          return false;
                      }
                      if(!flag_12 && bat < 3){
                          // toastr.error('You should choose atleast three BAT.'); 
                          showMessage('You should choose atleast three BAT.',"error");
                          return false;
                      }
                      if(!flag_12 && bowl < 3){
                          // toastr.error('You should choose atleast three BOWL.'); 
                          showMessage('You should choose atleast three BOWL.',"error");
                          return false;
                      }
                  
                      var cnt = 0; 
                      $('.team_image_sec .list_view_tab li').each(function(i, obj) {
                          if($(obj).attr('id')){
                           
                              team_player_arr[cnt] = $(obj).attr('id').replace( /^\D+/g, '');
                              cnt++; 
                          }
                          
                      });
                  
                      if(capton_id == 0){
                        // toastr.error('You should choose a capton before creating team.'); 
                        showMessage('You should choose a captain before creating team.',"error");
                        return false;
                      }
                      if(vice_capton_id == 0){
                        // toastr.error('You should choose a vice capton before creating team.'); 
                        showMessage('You should choose a vice captain before creating team.',"error");
                        return false;
                      }    
                  
                      my_team_name = $('#my_team_name').val();
                      // if(!my_team_name){
                      //    // toastr.error('Please Enter Team Name.'); 
                      //    $('#teamname_required_msg').show().hide(3000); 
                      //   return false; 
                  
                      // }
                  
                      // alert(twelve_player_select); alert(wk+bat+ar+bowl); 
                      if(total_player_count_curr < 12 || (wk+bat+ar+bowl < 12)){
                        twelveth_player_id = 0; 
                      }

                      // alert(twelveth_man_id); 

                       var twelveth_man_id = '<?php echo e(!empty($user_teams_data->twelveth_man_id) ? $user_teams_data->twelveth_man_id : ''); ?>';  // assing the 12th man id 

                       // alert(twelveth_man_id); 
                       // alert(twelveth_player_id); 

                      if(twelveth_man_id){  // if 12th player all ready selected then assign it 
                        twelveth_player_id = twelveth_man_id; 
                      }

                       game_mode = $('#game_mode').val();
                      // alert(game_mode); 
                      // console.log(team_player_arr); 
                  // alert(team_player_arr); 
                  $('#loading_img').show(); 
                        $.ajax({
                          url: '<?php echo e(url("save-my-team")); ?>',
                          type: 'POST',
                          data: {_token: '<?php echo e(csrf_token()); ?>', message:team_player_arr, capton_id: capton_id, vice_capton_id: vice_capton_id, my_team_name: my_team_name, club_id: club_id, twelveth_player_id: twelveth_player_id, my_game_mode: game_mode},
                          dataType: 'HTML',
                          success: function (data) { 
                         
                              console.log(data); 
                              $('#loading_img').hide(); 
                              if(data == 3){
                                showMessage('With dealer power you can not do more than 1 trade.',"error");
                              }else if(data == 2){
                                showMessage('Max. trade limit of <?php echo e(MAX_NO_OF_TRADE); ?> reached.',"error");
                              }else if(data == 1){
                                $('#success_msg').show().hide(3000); 
                              }else if(data == 0){
                                $('#error_msg').show().hide(3000); 
                              }else{
                                $('#error_msg_server').show().hide(3000); 
                              }
                          },
                           error: function(xhr, error){
                            $('#loading_img').hide(); 
                                   $('#error_msg_server').show().hide(3000); 
                            }
                       });
                  }); 
                  // $('.grid_view').click(function(){
                  $(document).on("click",".grid_view",function() { 
                  $('.list_view_tab').css('display', 'none');
                  $('.grid_view_tab').css('display', 'block');
                  $('.grid_view').addClass('active'); 
                  $('.list_view').removeClass('active');                         
                  });                         
                  // $('.list_view').click(function(){
                    $(document).on("click",".list_view",function() { 
                  $('.grid_view_tab').css('display', 'none').removeClass('active'); 
                  $('.list_view_tab').css('display', 'block').addClass('active');                         
                  $('.grid_view').removeClass('active'); 
                  $('.list_view').addClass('active');                         
                  });
                   $('.tot_player_in_11').text((wk+bat+ar+bowl)+'/'+(twelve_player_select ? 12 : 11));                        
                   $(document).on("click",".action_plus_minus span.captonbox",function(){
                  
                  
                  
                    var capt = $(this).prev('i').attr('player_id'); 
                  
                  
                    // console.log( locked_player);
                    // console.log(typeof locked_player); 
                    // console.log(locked_player.indexOf(6)); 
                  
                    if(locked_player.indexOf(capton_id) != -1){
                      showMessage('Captain player is locked.',"error");
                      return false; 
                    }
                  
                  
                    if(capt != 0 && vice_capton_id != 0 && capt == vice_capton_id){
                      // toastr.error('Capton and Vice Capton should be different player.');
                      showMessage('Captain and Vice Captain should be different player.',"error");
                      return false; 
                    }
                    if($(this).closest(".action_plus_minus").find('i').hasClass('fa-plus')){
                      // toastr.error('You should first choose this player to make him captain.');
                      showMessage('You should first choose this player to make him captain.',"error");
                    }else{
                      table.rows().eq(0).each( function ( index){
                          var row = table.row( index );
                          // var data = row.data();
                          // console.log( row.node()); 
                       $(row.node()).find('.captonbox_active').removeClass('captonbox_active');
                          // ... do something with data(), or row.node(), etc
                      });
                      $('.captonbox_active').removeClass('captonbox_active');
                      $(this).addClass('captonbox_active'); 
                      
                      $('.capt_img_active').removeClass('capt_img_active'); 
                      $('.grid_view_tab li#img'+capt).find('img.capt_img').addClass('capt_img_active'); 
                      capton_id = capt; 
                  $('.team_image_sec .list_view_tab  span.c_text').empty();
                  $('.team_image_sec .list_view_tab li#list'+capton_id+' span.c_text').text(' (C) ');
                    }
                   });
                   $(document).on("click",".action_plus_minus span.vice_captonbox",function() {    
                    var vice_capt = $(this).prev().prev('i').attr('player_id'); 
                  
                  
                    if(locked_player.indexOf(vice_capton_id) != -1){
                      showMessage('Vice Captain player is locked.',"error");
                      return false; 
                    }
                  
                  
                    if(vice_capt != 0 && capton_id != 0 && vice_capt == capton_id){
                      // toastr.error('Capton and Vice Capton should be different player.');
                      showMessage('Captain and Vice Captain should be different player.',"error");
                      return false; 
                    }
                    if($(this).closest(".action_plus_minus").find('i').hasClass('fa-plus')){
                      // toastr.error('You should first choose this player to make him vice capton.');
                      showMessage('You should first choose this player to make him vice Captain.',"error");
                    }else{
                  
                  
                  table.rows().eq(0).each( function ( index ) {
                  var row = table.row( index );
                  
                  // var data = row.data();
                  // console.log( row.node()); 
                  $(row.node()).find('.vice_captonbox_active').removeClass('vice_captonbox_active');
                  // ... do something with data(), or row.node(), etc
                  } );
                  table.order( [ 2, 'desc' ] ).draw();
                  
                  
                  
                      $('.vice_captonbox_active').removeClass('vice_captonbox_active');
                      $(this).addClass('vice_captonbox_active'); 
                      
                      // alert(vice_capt); 
                      $('.vice_capt_img_active').removeClass('vice_capt_img_active'); 
                      $('.grid_view_tab li#img'+vice_capt).find('img.vice_capt_img').addClass('vice_capt_img_active'); 
                      vice_capton_id = vice_capt; 
                  
                  $('.team_image_sec .list_view_tab  span.vc_text').empty();
                  $('.team_image_sec .list_view_tab li#list'+vice_capton_id+' span.vc_text').text(' (VC) ');
                    }
                  
                   }); 
                  } );                        
                </script>                                
              </div>
            </div>
            <div class="col-12 col-lg-4 my-3 team_image_sec" id="team_image_sec_move_mobile">
              <!-- <img class="img-fluid" src="img/play_ground.png" alt="play ground" /> -->
              <div class="lineup-header">
                <div class="grid-header">
                  <div class="view_list">
                    <a class="active grid_view" href="javascript:void(0);"><i class="fas fa-th-large"></i></a>
                    <a  class="list_view"  href="javascript:void(0);"><i class="fas fa-list"></i></a>
                  </div>
                  <h6><span class="tot_player_in_11">11/11</span> Selected</h6>
                  <div id="refresh_btn" class="refresh">
                    <?php if(!$lockout_club): ?>                           
                    <a href="javascript:void(0); "><i class="fas fa-redo-alt"></i></a>
                    <?php endif; ?>
                  </div>
                </div>
                <!-- start play ground image ************** -->
                <div class="team-tab grid_view_tab">
                  <div class="team-tab-inner">
                    <span class="sponsor">
                    <?php if(!empty($sess_club_name) && !empty($sess_club_name->brand_image) && File::exists(BRANDING_IMAGE_ROOT_PATH.$sess_club_name->brand_image)): ?>
                    <a target="_blank" href="<?php echo e(!empty($sess_club_name->brand_url) ? $sess_club_name->brand_url : '#'); ?>">
                    <img  src="<?php echo e(BRANDING_IMAGE_URL.$sess_club_name->brand_image); ?>" alt="brought_by"> 
                    </a>
                    <?php elseif(!empty($sess_club_name) && !empty($sess_club_name->club_logo)  && File::exists(CLUB_IMAGE_ROOT_PATH.$sess_club_name->club_logo)): ?>
                    <img src="<?php echo e(CLUB_IMAGE_URL.$sess_club_name->club_logo); ?>" alt="">
                    <?php else: ?>
                    <img src="<?php echo e(asset('img/no_image.png')); ?>" alt="">
                    <?php endif; ?>
                    </span>
                    <span class="sponsor_right">
                    <img src="<?php echo e(asset('img/logo_white.png')); ?>" alt="">
                    </span>
                    <?php if($booster_active['name'] == '3x' && $booster_active['status'] == true): ?>
                    <span class="sponsor_right_power">
                      <div>
                        <img class="booster_img" src="<?php echo e(asset('img/booster_yellow1.svg')); ?>" alt="booster_circle" />
                        <span class="three_blocks">
                          3X
                          <!-- <i booster_type="3x" style="position: absolute; cursor: pointer;" class="fas fa-times remove_booster"></i> -->
                        </span>
                      </div>
                    </span>
                    <?php endif; ?>
                    <?php if($booster_active['name'] == '12th' && $booster_active['status'] == true): ?>
                    <span class="sponsor_right_power">
                      <div>
                        <img class="booster_img" src="<?php echo e(asset('img/booster_yellow1.svg')); ?>" alt="booster_circle" />
                        <span class="three_blocks">
                          12th
                          <!-- <i booster_type="3x" style="position: absolute; cursor: pointer;" class="fas fa-times remove_booster"></i> -->
                        </span>
                      </div>
                    </span>
                    <?php endif; ?>
                    <?php if($booster_active['name'] == 'Dealer' && $booster_active['status'] == true): ?>
                    <span class="sponsor_right_power">
                      <div>
                        <img class="booster_img" src="<?php echo e(asset('img/booster_yellow1.svg')); ?>" alt="booster_circle" />
                        <span class="three_blocks">
                          Dlr.
                          <!-- <i booster_type="3x" style="position: absolute; cursor: pointer;" class="fas fa-times remove_booster"></i> -->
                        </span>
                      </div>
                    </span>
                    <?php endif; ?>
                    <?php if($booster_active['name'] == 'Flipper' && $booster_active['status'] == true): ?>
                    <span class="sponsor_right_power">
                      <div>
                        <img class="booster_img" src="<?php echo e(asset('img/booster_yellow1.svg')); ?>" alt="booster_circle" />
                        <span class="three_blocks">
                          Flp.
                          <!-- <i booster_type="3x" style="position: absolute; cursor: pointer;" class="fas fa-times remove_booster"></i> -->
                        </span>
                      </div>
                    </span>
                    <?php endif; ?>
                    <span class="sponsor_bottom_text text-center"><?php echo e(!empty($sess_club_name->game_name) ? $sess_club_name->game_name : ''); ?> <br> Team: <?php echo e(Auth::user()->my_team_name); ?> | <?php echo e(!empty($user_teams_data->id) && !empty($temp_points_arr[$user_teams_data->id]) ? $temp_points_arr[$user_teams_data->id] : 0); ?> pts</span>
                    <div class="ground-wrapper less-height  gridv ">
                      <div class="team-filedview">
                        <ul class="wk_div">
                          <!-- <li>
                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                            <img src="img/wc.png" alt="" height="60">
                            <img src="img/captain.png" alt="" class="caption-img">
                            <span class="player-position">WK</span>
                            <div class="player-name"> Clark </div>
                            <div class="player-points"><span class="team-salary">$0.10 M</span></div>
                            </li> -->
                          <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php if($temp->position == 4 ): ?>
                          <li id="img<?php echo e($temp->id); ?>">
                            <?php if(!$lockout_club && !in_array($temp->id , $locked_player) && $temp->dummy_player != 1 && (!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $temp->id)): ?> 
                            <i class="fas fa-times remove-player"></i>
                            <?php endif; ?>
                            <?php if(array_key_exists($temp->id, $availability) && ( !empty(Carbon::parse($availability[$temp->id]->date_from)) && ( ( Carbon::parse($availability[$temp->id]->date_from) >= Carbon::now() && Carbon::now()->diffInDays(Carbon::parse($availability[$temp->id]->date_from)) <= 10 ) || Carbon::parse($availability[$temp->id]->date_from) <= Carbon::now() ) ) && !empty(Carbon::parse($availability[$temp->id]->date_till)) && Carbon::now() <= Carbon::parse($availability[$temp->id]->date_till) ): ?>
                            <a   data-html="true" data-toggle="tooltip" title="<?php echo e($temp->full_name); ?> will not available from <?php echo e(!empty($availability[$temp->id]->date_from) ? Carbon::parse($availability[$temp->id]->date_from)->isoFormat('D/M/Y') : '--'); ?> to <?php echo e(!empty($availability[$temp->id]->date_till) ? Carbon::parse($availability[$temp->id]->date_till)->isoFormat('D/M/Y') : '--'); ?>" class="unavailable-player"><i style="color: #FF0000; " class="fa fa-info-circle " aria-hidden="true"></i></a>
                            <?php endif; ?>
                            <?php if(!empty($temp->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.$temp->image)): ?>
                            <img  src="<?php echo e(asset('uploads/player/'.$temp->image)); ?>" alt=""  height="60" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                            <?php else: ?>
                            <img src="<?php echo e(asset('img/wc.png')); ?>" alt="" width="56" height="60" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                            <?php endif; ?>

                            <?php if(!empty($user_teams_data) && $user_teams_data->twelveth_man_id == $temp->id): ?>
                             <img src="<?php echo e(asset('img/12th.png')); ?>" alt="" class="caption-img capt_img capt_img_active">
                            <?php endif; ?>
                            <img src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img <?php echo e($capton_id == $temp->id ? 'capt_img_active' : ''); ?>">
                            <img src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img <?php echo e($vice_capton_id == $temp->id ? 'vice_capt_img_active' : ''); ?>">
                            <span class="player-position">WK</span>
                            <span class="player-points-position"><?php echo e(isset($club_players_fantasy_points[$temp->id]) ? round($club_players_fantasy_points[$temp->id]) : 0); ?>pt</span>
                            <div class="player-name"> <?php echo e($temp->full_name); ?> </div>
                            <div class="player-points"><span class="team-salary">$<?php echo e($temp->svalue); ?>m</span></div>
                          </li>
                          <?php endif; ?>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                        <ul class="bat_div">
                          <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php if($temp->position == 1 ): ?>
                          <li id="img<?php echo e($temp->id); ?>">
                            <?php if(!$lockout_club && !in_array($temp->id , $locked_player) && $temp->dummy_player != 1 && (!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $temp->id)): ?> 
                            <i class="fas fa-times remove-player"></i>
                            <?php endif; ?>
                            <?php if(array_key_exists($temp->id, $availability) && ( !empty(Carbon::parse($availability[$temp->id]->date_from)) && ( ( Carbon::parse($availability[$temp->id]->date_from) >= Carbon::now() && Carbon::now()->diffInDays(Carbon::parse($availability[$temp->id]->date_from)) <= 10 ) || Carbon::parse($availability[$temp->id]->date_from) <= Carbon::now() ) ) && !empty(Carbon::parse($availability[$temp->id]->date_till)) && Carbon::now() <= Carbon::parse($availability[$temp->id]->date_till) ): ?>
                            <a   data-html="true" data-toggle="tooltip" title="<?php echo e($temp->full_name); ?> will not available from <?php echo e(!empty($availability[$temp->id]->date_from) ? Carbon::parse($availability[$temp->id]->date_from)->isoFormat('D/M/Y') : '--'); ?> to <?php echo e(!empty($availability[$temp->id]->date_till) ? Carbon::parse($availability[$temp->id]->date_till)->isoFormat('D/M/Y') : '--'); ?>" class="unavailable-player"><i style="color: #FF0000; " class="fa fa-info-circle " aria-hidden="true"></i></a>
                            <?php endif; ?>
                            <?php if(!empty($temp->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.$temp->image)): ?>
                            <img  src="<?php echo e(asset('uploads/player/'.$temp->image)); ?>" alt="" width="56" height="60" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                            <?php else: ?>
                            <img src="<?php echo e(asset('img/bm.png')); ?>" alt="" height="60"  style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                            <?php endif; ?>

                            <?php if(!empty($user_teams_data) && $user_teams_data->twelveth_man_id == $temp->id): ?>
                             <img src="<?php echo e(asset('img/12th.png')); ?>" alt="" class="caption-img capt_img capt_img_active">
                            <?php endif; ?>

                            <img  src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img <?php echo e($capton_id == $temp->id ? 'capt_img_active' : ''); ?>">
                            <img  src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img <?php echo e($vice_capton_id == $temp->id ? 'vice_capt_img_active' : ''); ?>">
                            <span class="player-position">BAT</span>
                            <span class="player-points-position"><?php echo e(isset($club_players_fantasy_points[$temp->id]) ? round($club_players_fantasy_points[$temp->id]) : 0); ?>pt</span>
                            <div class="player-name"> <?php echo e($temp->full_name); ?> </div>
                            <div class="player-points"> <span class="team-salary">$<?php echo e($temp->svalue); ?>m</span></div>
                          </li>
                          <?php endif; ?>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   
                        </ul>
                        <ul class="ar_div">
                          <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php if($temp->position == 3 ): ?>
                          <li id="img<?php echo e($temp->id); ?>">
                            <?php if(!$lockout_club && !in_array($temp->id , $locked_player) && $temp->dummy_player != 1 && (!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $temp->id)): ?> 
                            <i class="fas fa-times remove-player"></i>
                            <?php endif; ?>
                            <?php if(array_key_exists($temp->id, $availability) && ( !empty(Carbon::parse($availability[$temp->id]->date_from)) && ( ( Carbon::parse($availability[$temp->id]->date_from) >= Carbon::now() && Carbon::now()->diffInDays(Carbon::parse($availability[$temp->id]->date_from)) <= 10 ) || Carbon::parse($availability[$temp->id]->date_from) <= Carbon::now() ) ) && !empty(Carbon::parse($availability[$temp->id]->date_till)) && Carbon::now() <= Carbon::parse($availability[$temp->id]->date_till) ): ?>
                            <a   data-html="true" data-toggle="tooltip" title="<?php echo e($temp->full_name); ?> will not available from <?php echo e(!empty($availability[$temp->id]->date_from) ? Carbon::parse($availability[$temp->id]->date_from)->isoFormat('D/M/Y') : '--'); ?> to <?php echo e(!empty($availability[$temp->id]->date_till) ? Carbon::parse($availability[$temp->id]->date_till)->isoFormat('D/M/Y') : '--'); ?>" class="unavailable-player"><i style="color: #FF0000; " class="fa fa-info-circle " aria-hidden="true"></i></a>
                            <?php endif; ?>
                            <?php if(!empty($temp->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.$temp->image)): ?>
                            <img  src="<?php echo e(asset('uploads/player/'.$temp->image)); ?>" alt=""  width="56" height="60" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                            <?php else: ?>
                            <img src="<?php echo e(asset('img/ar.png')); ?>" alt="" height="60" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                            <?php endif; ?>

                            <?php if(!empty($user_teams_data) && $user_teams_data->twelveth_man_id == $temp->id): ?>
                             <img src="<?php echo e(asset('img/12th.png')); ?>" alt="" class="caption-img capt_img capt_img_active">
                            <?php endif; ?>

                            <img  src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img <?php echo e($capton_id == $temp->id ? 'capt_img_active' : ''); ?>">
                            <img  src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img <?php echo e($vice_capton_id == $temp->id ? 'vice_capt_img_active' : ''); ?>">
                            <span class="player-position">AR</span>
                            <span class="player-points-position"><?php echo e(isset($club_players_fantasy_points[$temp->id]) ? round($club_players_fantasy_points[$temp->id]) : 0); ?>pt</span>
                            <div class="player-name"> <?php echo e($temp->full_name); ?> </div>
                            <div class="player-points"> <span class="team-salary">$<?php echo e($temp->svalue); ?>m</span></div>
                          </li>
                          <?php endif; ?>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                        <ul class="bow_div">
                          <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <?php if($temp->position == 2 ): ?>
                          <li id="img<?php echo e($temp->id); ?>">
                            <?php if(!$lockout_club && !in_array($temp->id , $locked_player) && $temp->dummy_player != 1 && (!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $temp->id)): ?> 
                            <i class="fas fa-times remove-player"></i>
                            <?php endif; ?>
                            <?php if(array_key_exists($temp->id, $availability) && ( !empty(Carbon::parse($availability[$temp->id]->date_from)) && ( ( Carbon::parse($availability[$temp->id]->date_from) >= Carbon::now() && Carbon::now()->diffInDays(Carbon::parse($availability[$temp->id]->date_from)) <= 10 ) || Carbon::parse($availability[$temp->id]->date_from) <= Carbon::now() ) ) && !empty(Carbon::parse($availability[$temp->id]->date_till)) && Carbon::now() <= Carbon::parse($availability[$temp->id]->date_till) ): ?>
                            <a   data-html="true" data-toggle="tooltip" title="<?php echo e($temp->full_name); ?> will not available from <?php echo e(!empty($availability[$temp->id]->date_from) ? Carbon::parse($availability[$temp->id]->date_from)->isoFormat('D/M/Y') : '--'); ?> to <?php echo e(!empty($availability[$temp->id]->date_till) ? Carbon::parse($availability[$temp->id]->date_till)->isoFormat('D/M/Y') : '--'); ?>" class="unavailable-player"><i style="color: #FF0000; " class="fa fa-info-circle " aria-hidden="true"></i></a>
                            <?php endif; ?>
                            <?php if(!empty($temp->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.$temp->image)): ?>
                            <img  src="<?php echo e(asset('uploads/player/'.$temp->image)); ?>" alt=""  width="56" height="60" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                            <?php else: ?>
                            <img src="<?php echo e(asset('img/bow.png')); ?>" alt="" height="60" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                            <?php endif; ?>

                            <?php if(!empty($user_teams_data) && $user_teams_data->twelveth_man_id == $temp->id): ?>
                             <img src="<?php echo e(asset('img/12th.png')); ?>" alt="" class="caption-img capt_img capt_img_active">
                            <?php endif; ?>

                            <img  src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img <?php echo e($capton_id == $temp->id ? 'capt_img_active' : ''); ?>">
                            <img  src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img <?php echo e($vice_capton_id == $temp->id ? 'vice_capt_img_active' : ''); ?>">
                            <span class="player-position">Bow</span>
                            <span class="player-points-position"><?php echo e(isset($club_players_fantasy_points[$temp->id]) ? round($club_players_fantasy_points[$temp->id]) : 0); ?>pt</span>
                            <div class="player-name"> <?php echo e($temp->full_name); ?> </div>
                            <div class="player-points"> <span class="team-salary">$<?php echo e($temp->svalue); ?>m</span></div>
                          </li>
                          <?php endif; ?>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <?php if(!$lockout_club): ?>  
                  <button id="update_my_team_player_imagebtn" class="d-block btn btn-secondary btn-linup w-100 rounded-0">UPDATE LINEUP</button>
                  <?php endif; ?>
                </div>
                <!-- stop play ground image ************* -->
                <!-- start listing player --> 
                <div  class="list_view_tab" style="display: none;">
                  <ul>
                    <!--   <li>
                      <hr>
                      </li> -->
                    <li class="player_heading"><b>WK</b></li>
                    <div id="prefix_wk">
                      <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if($temp->position == 4 ): ?>
                      <li  class="player_data" id="list<?php echo e($temp->id); ?>"><?php echo e($temp->full_name); ?><span class="<?php echo e(!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $temp->id ? 'pl_price' : 'pl_12_price'); ?>">$<?php echo e($temp->svalue); ?>m</span><span class="c_text"><?php echo e($capton_id == $temp->id ? ' (C) ' : ''); ?></span><span class="vc_text"><?php echo e($vice_capton_id == $temp->id ? ' (VC) ' : ''); ?></span></li>
                      <?php endif; ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <!--        <li>
                      <hr>
                      </li> -->
                    <li  class="player_heading"><b>BAT</b></li>
                    <div id="prefix_bat">
                      <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if($temp->position == 1 ): ?>
                      <li  class="player_data"  id="list<?php echo e($temp->id); ?>"><?php echo e($temp->full_name); ?><span class="<?php echo e(!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $temp->id ? 'pl_price' : 'pl_12_price'); ?>">$<?php echo e($temp->svalue); ?>m</span><span class="c_text"><?php echo e($capton_id == $temp->id ? ' (C) ' : ''); ?></span><span class="vc_text"><?php echo e($vice_capton_id == $temp->id ? ' (VC) ' : ''); ?></span></li>
                      <?php endif; ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                    </div>
                    <!--           <li>
                      <hr>
                      </li> -->
                    <li class="player_heading"><b>BOWL</b></li>
                    <div id="prefix_bowl">
                      <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if($temp->position == 2 ): ?>
                      <li  class="player_data"  id="list<?php echo e($temp->id); ?>"><?php echo e($temp->full_name); ?><span class="<?php echo e(!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $temp->id ? 'pl_price' : 'pl_12_price'); ?>">$<?php echo e($temp->svalue); ?>m</span><span class="c_text"><?php echo e($capton_id == $temp->id ? ' (C) ' : ''); ?></span><span class="vc_text"><?php echo e($vice_capton_id == $temp->id ? ' (VC) ' : ''); ?></span></li>
                      <?php endif; ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <!--         <li>
                      <hr>
                      </li> -->
                    <li class="player_heading"><b>AR</b></li>
                    <div id="prefix_ar">
                      <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if($temp->position == 3 ): ?>
                      <li  class="player_data"  id="list<?php echo e($temp->id); ?>"><?php echo e($temp->full_name); ?><span class="<?php echo e(!empty($user_teams_data) && $user_teams_data->twelveth_man_id != $temp->id ? 'pl_price' : 'pl_12_price'); ?>">$<?php echo e($temp->svalue); ?>m</span><span class="c_text"><?php echo e($capton_id == $temp->id ? ' (C) ' : ''); ?></span><span class="vc_text"><?php echo e($vice_capton_id == $temp->id ? ' (VC) ' : ''); ?></span></li>
                      <?php endif; ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <!--         <li>
                      <hr>
                      </li> -->
                  </ul>
                  <!-- stop liting player -->
                  <?php if(!$lockout_club): ?>  
                  <button  id="update_my_team_player"  class="d-block btn btn-secondary btn-linup w-100 rounded-0">UPDATE LINEUP</button>
                  <?php endif; ?>
                </div>
                <!-- <a id="update_my_team_player" class="howuse_btn ml-auto my-2" href="javascript:void(0);">Update Line</a> -->
                <?php /* 
                  <div class="row align-items-center pt-3">
                      <div class="col-12"><label class="mb-1">Team Name:</label></div>
                      <div class="col-12"><input type="text" class="form-control" id="my_team_name" value="{{ !empty($my_team_name) ? $my_team_name : ''}}" placeholder="Enter Team Name*" /></div>
                  </div>
                  */ ?>
                <div id="success_msg" style="display: none; color: green; ">Team updated successfully.</div>
                <div id="error_msg" style="display: none; color: red; ">Failed!, Pelase try again.</div>
                <div id="teamname_required_msg" style="display: none; color: red; ">Please enter team name and try again!</div>
                <div id="error_msg_server" style="display: none; color: red; ">Error!, Pelase refresh the page.</div>
                <img id="loading_img" style="display: none; margin-top: 3px; " src="<?php echo e(asset('img/loading_img.gif')); ?>" />
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">
          // remove_booster
          
          $( ".remove_booster" ).click(function() {
            if (confirm("Are you sure to deactivate booster?")) {
                  // your deletion code
              
            $.ajax({
              method: "GET",
              url: "<?php echo e(url('remove-booster')); ?>",
              success: function (data) { 
                // console.log(); 
                if(data==1){
                  $('.remove_booster').parent('span.sponsor_right_power').remove(); 
                  showMessage('Booster successfully removed.',"success");
                  setTimeout(function(){
                      location.reload(true);
                  }, 3000);
                }else{
                  showMessage('Failed.',"error");
                }
          
              }
            });
          }
          
          });
          
          
        </script>
        <div class="boosters_block border-top p-4 mx-n4">
          <div class="d-block d-sm-flex align-items-center">
            <h4 class="status_title my-2">Powers
              <a  data-html="true" data-toggle="tooltip" title="You can use a maximum of 1 of your powers only during a trading period."><i style="color: #007bff; " class="fa fa-info-circle" aria-hidden="true"></i></a>
            </h4>
            <a class="howuse_btn ml-auto my-2" href="https://myclubtap.freshdesk.com/a/solutions/articles/43000534304" target="_blank"><i class="far fa-play-circle mr-2"></i>More About Powers</a>
          </div>
          <ul class="booster_list d-flex flex-wrap mt-4 mobile_teamdeta justify-content-center ">
            <li class="yellow_circle1 d-inline-flex position-relative p-2 <?php echo e($lockout_club ? 'locked_booster' : ( !empty($club_data->lockout_start_date) && Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay() ? ($booster_active['name'] == '3x' && $booster_active['status'] ? 'already_active_booster active' : ($booster_active['status'] ? 'already_active_booster' : 'capton_point_x_3' ) )  : 'not_active_booster' )); ?>" >
              <img class="booster_img" src="<?php echo e(asset('img/booster_yellow1.svg')); ?>" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                <div class="text-center">Triple Cap <small></small><span><?php echo e(!empty($user_teams_data->capton_card_count) ? $user_teams_data->capton_card_count : 0); ?>/<?php echo e(MAX_NO_OF_CAPTON_CARDS); ?></span></div>
              </div>
              <!-- <div class="booster_circle_hoverdata">3 Times of Captain points.</div> -->
              <?php if($lockout_club): ?>
              <div class="booster_circle_hoverdata">LOCKED</div>
              <?php elseif($booster_active['name'] == '3x' && $booster_active['status'] == true): ?>
              <div class="booster_circle_hoverdata">APPLIED</div>
              <?php elseif($booster_active['status'] == true): ?>
              <div class="booster_circle_hoverdata">LOCKED</div>
              <?php else: ?>
              <div class="booster_circle_hoverdata">PLAY</div>
              <?php endif; ?>
            </li>
            <li class="yellow_circle2 d-inline-flex position-relative p-2  <?php echo e($lockout_club ? 'locked_booster' : (!empty($club_data->lockout_start_date) && Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay() ?  ($booster_active['name'] == '12th' && $booster_active['status'] ? 'already_active_booster active' : ($booster_active['status'] ? 'already_active_booster' : 'twelve_man_card' ) )  : 'not_active_booster' )); ?>" >
              <img class="booster_img" src="<?php echo e(asset('img/booster_yellow2.svg')); ?>" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                <div class="text-center">12th Man<small></small><span><?php echo e(!empty($user_teams_data->twelve_man_card_count) ? $user_teams_data->twelve_man_card_count : 0); ?>/<?php echo e(MAX_NO_OF_12_MAN_CARDS); ?></span></div>
              </div>
              <!-- <div class="booster_circle_hoverdata">Add an extra 12th player in Team.</div> -->
              <?php if($lockout_club): ?>
              <div class="booster_circle_hoverdata">LOCKED</div>
              <?php elseif($booster_active['name'] == '12th' && $booster_active['status'] == true): ?>
              <div class="booster_circle_hoverdata">APPLIED</div>
              <?php elseif($booster_active['status'] == true): ?>
              <div class="booster_circle_hoverdata">LOCKED</div>
              <?php else: ?>
              <div class="booster_circle_hoverdata">PLAY</div>
              <?php endif; ?>
            </li>
            <!-- <li class="blue_circle d-inline-flex position-relative p-2">
              <img class="booster_img" src="img/booster_blue.svg" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                  <div class="text-center">Trade <small>Trade Cards</small><span><?php echo e(!empty($no_of_trade) ? $no_of_trade : 0); ?>/<?php echo e(MAX_NO_OF_TRADE); ?></span></div>
              </div>
              <div class="booster_circle_hoverdata">Change/swap player in Team.</div>
              </li> -->
            <!-- <li class="red_circle d-inline-flex position-relative p-2">
              <img class="booster_img" src="img/booster_red.svg" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                  <div class="text-center">1.5x <small>Player Cards</small><span>0/5</span></div>
              </div>
              <div class="booster_circle_hoverdata">1.5 times of each player point.</div>
              </li> -->
            <!-- <li class="yellow_circle2 d-inline-flex position-relative p-2">
              <img class="booster_img" src="img/booster_yellow2.svg" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                  <div class="text-center">Flip <small>Flipper Card</small><span>0/5</span></div>
              </div>
              <div class="booster_circle_hoverdata">Lorem Ipsum is simply dummy text.</div>
              </li> -->
            <!-- <li class="yellow_circle2 d-inline-flex position-relative p-2">
              <img class="booster_img" src="img/booster_yellow2.svg" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                  <div class="text-center">Trader <small>Extra Trader Card</small><span>coming soon</span></div>
              </div>
              <div class="booster_circle_hoverdata">Lorem Ipsum is simply dummy text.</div>
              </li> -->
            <!-- <li class="yellow_circle2 d-inline-flex position-relative p-2">
              <img class="booster_img" src="img/booster_yellow2.svg" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                  <div class="text-center">Flipper <small>Flipper Card</small><span>coming soon</span></div>
              </div>
              <div class="booster_circle_hoverdata">Lorem Ipsum is simply dummy text.</div>
              </li> -->
            <!-- <li class="yellow_circle2 d-inline-flex position-relative p-2">
              <img class="booster_img" src="img/booster_yellow2.svg" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                  <div class="text-center">Shield <small>Shield & Steal Card</small><span>coming soon</span></div>
              </div>
              <div class="booster_circle_hoverdata">Lorem Ipsum is simply dummy text.</div>
              </li> -->
            <li class="blue_circle d-inline-flex position-relative p-2  <?php echo e($lockout_club ? 'locked_booster' : (!empty($club_data->lockout_start_date) && Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay() ?  ($booster_active['name'] == 'Dealer' && $booster_active['status'] ? 'already_active_booster active' : ($booster_active['status'] ? 'already_active_booster' : 'dealer_card' ) )  : 'not_active_booster' )); ?>">
              <img class="booster_img" src="<?php echo e(asset('img/booster_blue.svg')); ?>" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                <div class="text-center">Dealer<small></small><span><?php echo e(!empty($user_teams_data->dealer_card_count) ? $user_teams_data->dealer_card_count : 0); ?>/<?php echo e(MAX_NO_OF_DEALER_CARDS); ?></span></div>
              </div>
              <!-- <div class="booster_circle_hoverdata">Extra trade after 20 free trade.</div> -->
              <?php if($lockout_club): ?>
              <div class="booster_circle_hoverdata">LOCKED</div>
              <?php elseif($booster_active['name'] == 'Dealer' && $booster_active['status'] == true): ?>
              <div class="booster_circle_hoverdata">APPLIED</div>
              <?php elseif($booster_active['status'] == true): ?>
              <div class="booster_circle_hoverdata">LOCKED</div>
              <?php else: ?>
              <div class="booster_circle_hoverdata">PLAY</div>
              <?php endif; ?>
            </li>
            <li class="red_circle d-inline-flex position-relative p-2  <?php echo e($lockout_club ? 'locked_booster' : (!empty($club_data->lockout_start_date) && Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay() ?  ($booster_active['name'] == 'Flipper' && $booster_active['status'] ? 'already_active_booster active' : ($booster_active['status'] ? 'already_active_booster' : 'flipper_card' ) )  : 'not_active_booster' )); ?>">
              <img class="booster_img" src="<?php echo e(asset('img/booster_red.svg')); ?>" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                <div class="text-center">Flipper <small></small><span><?php echo e(!empty($user_teams_data->flipper_card_count) ? $user_teams_data->flipper_card_count : 0); ?>/<?php echo e(MAX_NO_OF_FLIPPER_CARDS); ?></span></div>
              </div>
              <!-- <div class="booster_circle_hoverdata">Change full team for free.</div> -->
              <?php if($lockout_club): ?>
              <div class="booster_circle_hoverdata">LOCKED</div>
              <?php elseif($booster_active['name'] == 'Flipper' && $booster_active['status'] == true): ?>
              <div class="booster_circle_hoverdata">APPLIED</div>
              <?php elseif($booster_active['status'] == true): ?>
              <div class="booster_circle_hoverdata">LOCKED</div>
              <?php else: ?>
              <div class="booster_circle_hoverdata">PLAY</div>
              <?php endif; ?>
            </li>

            <li class="yellow_circle2 d-inline-flex position-relative p-2  <?php echo e($lockout_club ? 'locked_booster' : (!empty($club_data->lockout_start_date) && Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay() ?  ($booster_active['name'] == 'ShieldSteal' && $booster_active['status'] ? 'already_active_booster active' : ($booster_active['status'] ? 'already_active_booster' : 'shield_steal_card' ) )  : 'not_active_booster' )); ?>">
              <img class="booster_img" src="<?php echo e(asset('img/booster_yellow2.svg')); ?>" alt="booster_circle" />
              <div class="booster_circle_data d-flex align-items-center justify-content-center">
                <div class="text-center">Shield Steal <small></small><span><?php echo e(!empty($user_teams_data->shield_steal_card_count) ? $user_teams_data->shield_steal_card_count : 0); ?>/<?php echo e(MAX_NO_OF_SHIELD_STEAL_CARDS); ?></span></div>
              </div>
              <!-- <div class="booster_circle_hoverdata">Steal points from others.</div> -->
              <!-- <div class="booster_circle_hoverdata">Coming Soon</div> -->

              <?php if($lockout_club): ?>
              <div class="booster_circle_hoverdata">LOCKED</div>
              <?php elseif($booster_active['name'] == 'ShieldSteal' && $booster_active['status'] == true): ?>
              <div class="booster_circle_hoverdata">APPLIED</div>
              <?php elseif($booster_active['status'] == true): ?>
              <div class="booster_circle_hoverdata">LOCKED</div>
              <?php else: ?>
              <div class="booster_circle_hoverdata">PLAY</div>
              <?php endif; ?>


            </li>

          </ul>
        </div>
        <div class="boosters_block border-top p-4 mx-n4">
          <div class="d-block d-sm-flex align-items-center">
            <h4 class="status_title my-2">
              GW Fixtures
              <!-- <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Fixtures in this gameweek."><i style="color: #007bff; " class="fa fa-info-circle" aria-hidden="true"></i></a> -->
            </h4>
            <!-- <a class="howuse_btn ml-auto my-2" href="https://myclubtap.freshdesk.com/a/solutions/articles/43000534304" target="_blank"><i class="far fa-play-circle mr-2"></i>More About Powers</a> -->
          </div>
          <div class="table-responsive table-mobile">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
              <tbody>
                <tr>
                  <th>Date</th>
                  <th>Team</th>
                  <th>Grade</th>
                  <th>Scorecard</th>
                </tr>
                <?php if(!empty($result_fix) && $result_fix->isNotEmpty()): ?>
                <?php $__currentLoopData = $result_fix; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php //prd($v); ?>
                <?php 
                  // if(!empty($v->start_date) && Carbon::parse($v->start_date) <= Carbon::now()){
                  //     $icnt++;  
                  // }
                  ?>
                <tr>
                  <td><?php echo e(!empty($v->start_date) ? Carbon::parse($v->start_date)->format('d.m.y') : ''); ?></td>
                  <td><?php echo e(ucwords($v->team_name)); ?></td>
                  <td><?php echo e(!empty($v->grade_name) ? ucwords($v->grade_name) : '-'); ?></td>
                  <!-- <td><?php echo e($v->fixture_scorecard->sum('fantasy_points')); ?></td>    -->
                  <td>
                    <!-- <a href="<?php echo e(route('Global.matchScorecard',$v->id)); ?>" class="more_link border ml-auto"  data-toggle="modal" data-target="#myModal<?php echo e($v->id); ?>">Scorecard</a> -->
                    <a href="<?php echo e(route('Global.matchScorecard',$v->id)); ?>" class="more_link border ml-auto">Scorecard</a>
                  </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                <tr>
                  <td colspan="4">No fixtures available in this gameweek.</td>
                </tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- <section class="most_players_chart_block mb-5">
    <div class="container">
       <div class="row">
          <div class="col-12 col-md-6 my-3">
             <h3 class="players_title d-flex align-items-center mb-3">Top Players by Gameweeks <a href="" class="more_link border ml-auto">More</a></h3>
             <div class="table-responsive">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                   <tbody>
                      <tr>
                         <th>Post <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th>
                         <th>Team <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th>
                         <th>Value</th>
                      </tr>
                      <tr>
                         <td>1</td>
                         <td>ABC Superstarts</td>
                         <td>$5.95m</td>
                      </tr>
                      <tr>
                         <td>2</td>
                         <td>ABC Superstarts</td>
                         <td>$2.95m</td>
                      </tr>
                      <tr>
                         <td>3</td>
                         <td>ABC Superstarts</td>
                         <td>$3.95m</td>
                      </tr>
                      <tr>
                         <td>4</td>
                         <td>ABC Superstarts</td>
                         <td>$7.95m</td>
                      </tr>
                      <tr>
                         <td>5</td>
                         <td>ABC Superstarts</td>
                         <td>$4.95m</td>
                      </tr>
                   </tbody>
                </table>
             </div>
          </div>
          <div class="col-12 col-md-6 my-3">
             <h3 class="players_title d-flex align-items-center mb-3">Player Availability Chart <a href="" class="more_link border ml-auto">More</a></h3>
             <div class="table-responsive">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                   <tbody>
                      <tr>
                         <th>Post <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th>
                         <th>Team <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th>
                         <th>Value</th>
                      </tr>
                      <tr>
                         <td>1</td>
                         <td>ABC Superstarts</td>
                         <td>$5.95m</td>
                      </tr>
                      <tr>
                         <td>2</td>
                         <td>ABC Superstarts</td>
                         <td>$2.95m</td>
                      </tr>
                      <tr>
                         <td>3</td>
                         <td>ABC Superstarts</td>
                         <td>$3.95m</td>
                      </tr>
                      <tr>
                         <td>4</td>
                         <td>ABC Superstarts</td>
                         <td>$7.95m</td>
                      </tr>
                      <tr>
                         <td>5</td>
                         <td>ABC Superstarts</td>
                         <td>$4.95m</td>
                      </tr>
                   </tbody>
                </table>
             </div>
          </div>
       </div>
    </div>
    </section> -->
  <div class="container">
    <!-- <section class="over_all_team_block p-4 mb-5">
      <h2 class="game_week2_title text-center mb-4">Over All : My Team</h2>
      <div class="my_team_banner_block d-flex flex-nowrap align-items-end position-relative mb-5">
         <div class="w-100 d-flex flex-wrap align-items-end position-relative">
            <div class="my_team_banner_top_text">Gameweek #</div>
            <div class="team_banner_img"><img src="img/team_banner.png" alt="team"></div>
            <div class="team_banner_btn px-1 py-3">
               <div>
                  <label for="" class="w-100 team_banner_label_name px-2">Prizes</label>
                  <a class="text-center mx-2" href="">My Team</a>
                  <a class="active text-center mx-2" href="">Become Premier Now</a>
               </div>
            </div>
         </div>
      </div>
      <a class="my_team_banner mb-5" href=""><img class="img-fluid" src="img/my_team_banner.jpg" alt="my_team_banner"></a>
      <ul class="game_week_info_list d-flex my-n3 mb-4">
         <li class="d-inline-flex align-items-center px-4 py-5 my-3">
            <div class="w-100 game_week_info d-flex align-items-center">
               <div class="w-50 game_week_info_icon text-center p-2"><img src="img/game_week_info_icon1.png" alt="game_week_info"></div>
               <div class="w-50 game_week_info_status p-2 ml-auto">150 <small>Highest Pts.</small></div>
            </div>
         </li>
         <li class="d-inline-flex align-items-center px-4 py-5 my-3">
            <div class="w-100 game_week_info d-flex align-items-center">
               <div class="w-50 game_week_info_icon text-center p-2"><img src="img/game_week_info_icon2.png" alt="game_week_info"></div>
               <div class="w-50 game_week_info_status p-2 ml-auto">150 <small>Average Pts</small></div>
            </div>
         </li>
         <li class="d-inline-flex align-items-center px-4 py-5 my-3">
            <div class="w-100 game_week_info d-flex align-items-center">
               <div class="w-50 game_week_info_icon text-center p-2"><img src="img/game_week_info_icon3.png" alt="game_week_info"></div>
               <div class="w-50 game_week_info_status p-2 ml-auto">3045 <small>Trades Used</small></div>
            </div>
         </li>
         <li class="d-inline-flex align-items-center px-4 py-5 my-3">
            <div class="w-100 game_week_info d-flex align-items-center">
               <div class="w-50 game_week_info_icon text-center p-2"><img src="img/game_week_info_icon4.png" alt="game_week_info"></div>
               <div class="w-50 game_week_info_status p-2 ml-auto">725 <small>Boosters Used</small></div>
            </div>
         </li>
         <li class="d-inline-flex align-items-center px-4 py-5 my-3">
            <div class="w-100 game_week_info d-flex align-items-center">
               <div class="w-50 game_week_info_icon text-center p-2"><img src="img/game_week_info_icon5.png" alt="game_week_info"></div>
               <div class="w-50 game_week_info_status p-2 ml-auto">Vickey <small>Most Picked Players</small></div>
            </div>
         </li>
         <li class="d-inline-flex align-items-center px-4 py-5 my-3">
            <div class="w-100 game_week_info d-flex align-items-center">
               <div class="w-50 game_week_info_icon text-center p-2"><img src="img/game_week_info_icon5.png" alt="game_week_info"></div>
               <div class="w-50 game_week_info_status p-2 ml-auto">Virat <small>Most Picked Caption</small></div>
            </div>
         </li>
      </ul>
      </section> -->
    <!-- <div class="top_player_block mb-5">
      <h3 class="players_title mb-3">Top Players by Gameweeks</h3>
      <div class="top_players_slider123 owl-carousel border p-4">
         <div class="item">
            <div class="top_players_sec text-center">
               <div class="players_img mb-2"><img src="img/top_players1.png" alt="top players"></div>
               <div class="players_info">Jason Roy <small>GW1 144 pts</small></div>
            </div>
         </div>
         <div class="item">
            <div class="top_players_sec text-center">
               <div class="players_img mb-2"><img src="img/top_players2.png" alt="top players"></div>
               <div class="players_info">Jasprit Bumrah <small>GW2 144 pts</small></div>
            </div>
         </div>
         <div class="item">
            <div class="top_players_sec text-center">
               <div class="players_img mb-2"><img src="img/top_players3.png" alt="top players"></div>
               <div class="players_info">Malinga <small>GW2 244 pts</small></div>
            </div>
         </div>
         <div class="item">
            <div class="top_players_sec text-center">
               <div class="players_img mb-2"><img src="img/top_players4.png" alt="top players"></div>
               <div class="players_info">Rohit Sharma <small>GW2 244 pts</small></div>
            </div>
         </div>
         <div class="item">
            <div class="top_players_sec text-center">
               <div class="players_img mb-2"><img src="img/top_players5.png" alt="top players"></div>
               <div class="players_info">Imam ul haq <small>Imam ul haq</small></div>
            </div>
         </div>
         <div class="item">
            <div class="top_players_sec text-center">
               <div class="players_img mb-2"><img src="img/top_players6.png" alt="top players"></div>
               <div class="players_info">Ian Morgan <small>GW2 244 pts</small></div>
            </div>
         </div>
         <div class="item">
            <div class="top_players_sec text-center">
               <div class="players_img mb-2"><img src="img/top_players3.png" alt="top players"></div>
               <div class="players_info">Malinga <small>GW2 244 pts</small></div>
            </div>
         </div>
      </div>
      </div> -->
    <div class="favorite_players_block mb-5">
      <!-- <h4 class="status_title mb-3">Status : Gameweek</h4> -->
      <!-- <ul class="gameweek_list favorite_players_list d-flex mb-4">
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Bartholomew <small class="d-flex align-items-center text-uppercase"><img class="mr-2" src="img/balla.png" alt="balla">Bat</small></div>
              <div class="gameweek_status ml-auto">90<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Archibald Atwater <small class="d-flex align-items-center text-uppercase red_color"><img class="mr-2" src="img/ball.png" alt="balla">Bowl</small></div>
              <div class="gameweek_status ml-auto">150<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Bernard Cary <small class="d-flex align-items-center text-uppercase"><img class="mr-2" src="img/balla.png" alt="balla">Bat</small></div>
              <div class="gameweek_status ml-auto">150<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Culver Conan <small class="d-flex align-items-center text-uppercase"><img class="mr-2" src="img/balla.png" alt="balla">Bat</small></div>
              <div class="gameweek_status ml-auto">250<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Culver Crispin <small class="d-flex align-items-center text-uppercase red_color"><img class="mr-2" src="img/ball.png" alt="balla">Bowl</small></div>
              <div class="gameweek_status ml-auto">200<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Conway Crosby <small class="d-flex align-items-center text-uppercase red_color"><img class="mr-2" src="img/ball.png" alt="balla">Bowl</small></div>
              <div class="gameweek_status ml-auto">300<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Bartholomew <small class="d-flex align-items-center text-uppercase"><img class="mr-2" src="img/balla.png" alt="balla">Bat</small></div>
              <div class="gameweek_status ml-auto">90<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Archibald Atwater <small class="d-flex align-items-center text-uppercase red_color"><img class="mr-2" src="img/ball.png" alt="balla">Bowl</small></div>
              <div class="gameweek_status ml-auto">150<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Bernard Cary <small class="d-flex align-items-center text-uppercase"><img class="mr-2" src="img/balla.png" alt="balla">Bat</small></div>
              <div class="gameweek_status ml-auto">150<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Culver Conan <small class="d-flex align-items-center text-uppercase"><img class="mr-2" src="img/balla.png" alt="balla">Bat</small></div>
              <div class="gameweek_status ml-auto">250<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Culver Crispin <small class="d-flex align-items-center text-uppercase red_color"><img class="mr-2" src="img/ball.png" alt="balla">Bowl</small></div>
              <div class="gameweek_status ml-auto">200<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Conway Crosby <small class="d-flex align-items-center text-uppercase red_color"><img class="mr-2" src="img/ball.png" alt="balla">Bowl</small></div>
              <div class="gameweek_status ml-auto">300<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Bartholomew <small class="d-flex align-items-center text-uppercase"><img class="mr-2" src="img/balla.png" alt="balla">Bat</small></div>
              <div class="gameweek_status ml-auto">90<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Archibald Atwater <small class="d-flex align-items-center text-uppercase red_color"><img class="mr-2" src="img/ball.png" alt="balla">Bowl</small></div>
              <div class="gameweek_status ml-auto">150<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Bernard Cary <small class="d-flex align-items-center text-uppercase"><img class="mr-2" src="img/balla.png" alt="balla">Bat</small></div>
              <div class="gameweek_status ml-auto">150<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Culver Conan <small class="d-flex align-items-center text-uppercase"><img class="mr-2" src="img/balla.png" alt="balla">Bat</small></div>
              <div class="gameweek_status ml-auto">250<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Culver Crispin <small class="d-flex align-items-center text-uppercase red_color"><img class="mr-2" src="img/ball.png" alt="balla">Bowl</small></div>
              <div class="gameweek_status ml-auto">200<small>pts</small></div>
           </div>
        </li>
        <li class="d-inline-flex align-items-center p-3 my-2">
           <div>
              <div class="gameweek_day mr-3">Conway Crosby <small class="d-flex align-items-center text-uppercase red_color"><img class="mr-2" src="img/ball.png" alt="balla">Bowl</small></div>
              <div class="gameweek_status ml-auto">300<small>pts</small></div>
           </div>
        </li>
        </ul> -->
      <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="<?php echo e(asset('img/play_now_banner.jpg')); ?>" alt="my_team_banner"></a>
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="w-100 d-flex align-items-center player_star mt-1">
            <img width="200" src="<?php echo e(asset('img/playercardlogo.png')); ?>" alt="MyClubTap">
            <ul class="d-inline-flex ml-auto"> 
              <li class="mx-2"></li>
              <li class="mx-2 club_mode1"></li>
            </ul>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <section class="status_tab_block mb-2">
            <div class="">
              <div class="player_star">
                <div class="player_star_info mt-1">
                  <div class="player_star_pic position-relative mr-3 my-0 ml-0"><a class="player_profile_url" href="#">
                    <img class="player_image" src="<?php echo e(asset('img/player_star.jpg')); ?>" alt="player_star">
                  </a></div>
                  <!--  <div class="PlayerName"> -->
                  <div class="player_description">
                    <h3 class="player_star_name">
                    <small class="player_name"></small><span class="img_position"></span><span style="margin-left: 10px;" class="team_icon"></span></h3>
                    <small class="text-white playr_game_name"></small>
                    <small class="text-white playr_club_name"></small> 
                    <small class="text-white playr_bat_style"> </small>
                    <small class="text-white playr_bowl_style"></small>
                  </div>
                  <div class="club_logo ml-5 d-none d-lg-block">

                    <!-- <img src="<?php echo e(asset('img/player_star.jpg')); ?>" alt="player_star"> -->
                    <?php if(!empty($sess_club_name) && !empty($sess_club_name->brand_image) && File::exists(BRANDING_IMAGE_ROOT_PATH.$sess_club_name->brand_image)): ?>
                    <a target="_blank" href="<?php echo e(!empty($sess_club_name->brand_url) ? $sess_club_name->brand_url : '#'); ?>">
                    <img src="<?php echo e(BRANDING_IMAGE_URL.$sess_club_name->brand_image); ?>" alt="brought_by"> 
                    </a>
                    <?php elseif(!empty($sess_club_name) && !empty($sess_club_name->club_logo)  && File::exists(CLUB_IMAGE_ROOT_PATH.$sess_club_name->club_logo)): ?>
                    <img src="<?php echo e(CLUB_IMAGE_URL.$sess_club_name->club_logo); ?>" alt="">
                    <?php else: ?>
                    <img src="<?php echo e(asset('img/no_image.png')); ?>" alt="">
                    <?php endif; ?>                    

                  </div>
                </div>
              </div>
              <div class="tab_content_sec border p-0 p-md-4>
                <div class="player_star_data game_info_block py-4 mx-3 mx-md-5">
                <ul class="game_progress_info d-flex mobile_teamdeta liborderblock Salary_block player_pack_box">
                  <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-3 my-2">
                    <div class="w-100 game_week_info d-flex align-items-center">
                      <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon10.png')); ?>" alt="game_week_info"></div>
                      <div class="game_week_info_status pl-2"><span class="player_fps"></span> <small class="mt-1">Fantasy Pts</small></div>
                    </div>
                  </li>
                  <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-3 my-2">
                    <div class="w-100 game_week_info d-flex align-items-center">
                      <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon11.png')); ?>" alt="game_week_info"></div>
                      <div class="game_week_info_status pl-2"><span class="player_value"></span> <small class="mt-1">Fantasy Value</small></div>
                    </div>
                  </li>
                  <li class="d-inline-flex flex-wrap align-items-center justify-content-center text-center p-3 my-2">
                    <div class="w-100 game_week_info d-flex align-items-center">
                      <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon9.png')); ?>" alt="game_week_info"></div>
                      <div class="game_week_info_status pl-2"><span class="selected_by"></span>% <small class="mt-1">Picked in Teams</small></div>
                    </div>
                  </li>
                </ul>
                <div class="games_tabs_sec mt-5">
                  <ul class="nav nav-tabs d-flex" role="tablist">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#form" role="tab" aria-selected="true">Form</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#game_log" role="tab" aria-selected="false">Game Log</a></li>
                  </ul>
                  <div class="tab-content border p-sm-4 p-2  mb-3">
                    <div class="tab-pane fade show active" id="form">
                      <div style="max-height: 400px;overflow: auto;">
                      <div id="container" style="min-width: 260px; height: 400px; margin: 0 auto"></div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="game_log">
                      <div style="max-height: 400px !important;overflow: auto;">
                        <div class="table-responsive table-mobile">
                          <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                            <thead>
                              <tr>
                                <th scope="col">Team</th>
                                <th scope="col">R</th>
                                <th scope="col">4</th>
                                <th scope="col">6</th>
                                <th scope="col">MDS</th>
                                <th scope="col">WK</th>
                                <th scope="col">FP</th>
                                <th scope="col">FV</th>
                              </tr>
                            </thead>
                            <tbody class="player_last_5">
                            </tbody>
                          </table>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </section>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" id="temptemptemp" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var chartt = Highcharts.chart('container', {
      chart: {
          type: 'column'
      },
      title: {
          text: ''
      },
      subtitle: {
          text: ''
      },
      xAxis: {
          categories: [
      
          ],
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Fantasy Points'
          }
      },
      // tooltip: {
      //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      //         '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      //     footerFormat: '</table>',
      //     shared: true,
      //     useHTML: true
      // },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [{
          name: '',
          data: []
  
      }, 
      // {
      //     name: 'New York',
      //     data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
  
      // }, {
      //     name: 'London',
      //     data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
  
      // }, {
      //     name: 'Berlin',
      //     data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
  
      // }
      ]
  });
  
  
  
  
  // $('#temptemptemp').click(function() {
  //   chartt.series[0].setData([48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2],true);
  //   // chartt.series[1].setData([49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],true);
  //   // chartt.series[2].setData([49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],true);
  //   // chartt.series[3].setData([49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],true);
  //   chartt.xAxis[0].setCategories(['Jan', 'Jan', 'Jan', 'Jan', 'Jan', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
  // });
  
  
  
  
  
  // $('.dt_player_name').click(function() {
     $(document).on("click",".dt_player_name",function() { 
  
     var player_id =  $(this).closest('tr').find('.action_plus_minus i').attr('player_id'); 
     if(!player_id){
      player_id =  $(this).attr('player_id'); 
     }
      $.ajax({
        url: '<?php echo e(url("get-player-data")); ?>',
        type: 'POST',
        data: {_token: '<?php echo e(csrf_token()); ?>', player_id: player_id},
        dataType: 'HTML',
        success: function (data) { 
          var obj = $.parseJSON(data); 
          // console.log(obj.player_name); 
          $('#exampleModal .player_name').text(obj.player_name);
          $('#exampleModal .playr_club_name').text(obj.playr_club_name);
          $('#exampleModal .playr_game_name').text(obj.playr_game_name);
          $('#exampleModal .playr_bat_style').text(obj.playr_bat_style);
          $('#exampleModal .playr_bowl_style').text(obj.playr_bowl_style);
  
          var imgSource = "<?php echo e(WEBSITE_IMG_URL); ?>";
          var player_team_name = obj.player_team_name;
          if(obj.playerPosition == "BAT"){ 
            $('#exampleModal .img_position').html('<a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Batsman"><img width="25px" height="25px" id="theImg" src='+imgSource+"bat-icon.png"+' /></a>');
          }else if(obj.playerPosition == "BOWL"){ 
             $('#exampleModal .img_position').html('<a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Bowler"><img width="25px" height="25px" id="theImg" src='+imgSource+"ball-icon.png"+' /></a>');
          }else if(obj.playerPosition == "AR"){ 
             $('#exampleModal .img_position').html('<a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="All-Rounder"><img width="25px" height="25px" id="theImg" src='+imgSource+"bat-ball-icon.png"+' /></a>');
          }else if(obj.playerPosition == "WK"){ 
             $('#exampleModal .img_position').html('<a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Wicket-Keeper"><img width="25px" height="25px" id="theImg" src='+imgSource+"gloves-icon.png"+' /></a>');
          }
          $('#exampleModal .playerPosition').text(obj.playerPosition);
          if(player_team_name !=null ){
            $('#exampleModal .team_icon').html('<a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="'+player_team_name+'"><span class="t_icon">T</span></a>');
          }
          
  
          $('#exampleModal .player_profile_url').attr('href', '<?php echo e(url("player-profile/")); ?>/'+player_id);
          $('.club_mode').text(obj.club_mode);
          // $('#exampleModal .club_name').text('Western Australia');
          if(obj.player_image == 1 || obj.player_image == 2 || obj.player_image == 3 || obj.player_image == 4 ){
            if(obj.player_image == 1){
              $('#exampleModal img.player_image').attr('src', '<?php echo e(asset("img/bm.png")); ?>');
            }else if(obj.player_image == 2){
              $('#exampleModal img.player_image').attr('src', '<?php echo e(asset("img/bow.png")); ?>');
            }else if(obj.player_image == 3){
              $('#exampleModal img.player_image').attr('src', '<?php echo e(asset("img/ar.png")); ?>');
            }else if(obj.player_image == 4){
              $('#exampleModal img.player_image').attr('src', '<?php echo e(asset("img/wc.png")); ?>');
            }
            
          }else{
            $('#exampleModal img.player_image').attr('src', '<?php echo e(asset("uploads/player")); ?>/'+obj.player_image);
          }
          
          $('#exampleModal .player_fps').text(obj.fantasy_points);
          $('#exampleModal .player_value').text('$'+obj.player_price+'m');
  
          // console.log(obj); 
          var table_str = ''; 
          obj.last_5_record.forEach(function (item, index) {
            table_str += '<tr>'
            
            table_str += '<td>'+(item.team_name ? item.team_name : '--')+'</td>'; 
            table_str += '<td>'+(item.runs ? item.runs : 0)+'</td>'; 
            table_str += '<td>'+(item.four ? item.four : 0)+'</td>'; 
            table_str += '<td>'+(item.six ? item.six : 0)+'</td>'; 
            table_str += '<td>'+(item.maiden ? item.maiden : 0)+'</td>'; 
            table_str += '<td>'+(item.wks ? item.wks : 0)+'</td>'; 
            table_str += '<td>'+(item.fp ? item.fp : 0)+'</td>'; 
            table_str += '<td>$'+(item.fv ? item.fv : 0)+'m</td>'; 
            table_str += '</tr>'
          });
          if(!table_str)  table_str +='<tr><td align="center" colspan="8">No record is yet available.</td></tr>' 
          $('#exampleModal .selected_by').text(obj.selected_by); 
          $('#exampleModal .player_last_5').html(table_str); 
  // player_last_5
  obj.last_10_record.fx_month_fp = obj.last_10_record.fx_month_fp.map( Number );
  chartt.series[0].setName(obj.player_name,true);
   chartt.series[0].setData(obj.last_10_record.fx_month_fp,true);
    chartt.xAxis[0].setCategories(obj.last_10_record.fx_month);
  
  
  
        }
     }); 
  
  
  
  });
  
  $('#exampleModal').on('hidden.bs.modal', function () {
    $('#exampleModal .player_name').text('');
   $('#exampleModal img.player_image').attr('src', '<?php echo e(asset("img/logo_white.png")); ?>');
    $('#exampleModal .player_fps').text('');
    $('#exampleModal .player_value').text('');
    $('#exampleModal .selected_by').text(''); 
     $('#exampleModal .player_last_5').html(''); 
  chartt.series[0].setName([],true);
   chartt.series[0].setData([],true);
    chartt.xAxis[0].setCategories([]);   
  })
  
  
  $('.append_club_name select').on('change', function(){
      $(this).closest('form').submit();
  });
  
  /*
  $( "#capton_point_x_3" ).click(function() {
  
  
        club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
        if(!club_id){
          // toastr.error('Please choose club to create team.'); 
          showMessage('Please choose club to create team.',"error");
          return false;
        }
        
        // if(parseInt('<?php echo e(MAX_NO_OF_CAPTON_CARDS); ?>') >= 5){
        //   showMessage('Your max. limit of capton card reached.',"error");
        //   return false;
        // }
  
  
    toastr.success("<button type='button' id='confirm_capton_point_x_3' style='padding: 1px 2px; line-height: 1; ' class='btn clear'>OK</button> &nbsp; &nbsp; <button type='button'  style='padding: 1px 2px; line-height: 1; '  class='btn clear'>Cancel</button>",'Activate Capton Card?',
    {
        closeButton: false,
        preventDuplicates: true,
       preventOpenDuplicates: true,
        allowHtml: true,
        timeOut: 110000 ,
        onShown: function (toast) {
            $("#confirm_capton_point_x_3").click(function(){
              console.log('helllo'); 
              $.ajax({
                url: 'activate-capton-card',
                type: 'POST',
                data: {_token: '<?php echo e(csrf_token()); ?>',  club_id: club_id},
                dataType: 'HTML',
                success: function (data) { 
                  if(data == 1){
                    showMessage('Capton Card activated successfully.',"success");
                  }else if(data == 0){
                    showMessage('To activate Capton card, please create your Team first.',"error");
                  }else if(data == 2){
                    showMessage('Capton card already active for this week.',"error");
                  }else if(data == 3){
                    showMessage('Your max. limit of capton card reached.',"error");
                  }else {
                    showMessage('Internal error occured.',"error");
                  }
                }
              }); 
            });
          }
    });
  
  });
  
  */ 
</script>
<!-- **************************** -->
<!-- <button class="btn btn-default" id="capton_point_x_3">Confirm</button> -->
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!--     <h4 class="modal-title" id="myModalLabel">Confirmar</h4> -->
      </div>
      <div class="modal-body">
        <h5>Do you want to use your Triple Cap power?</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="modal-btn-si">OK</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">Cancel</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="twelve-man-modal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!--     <h4 class="modal-title" id="myModalLabel">Confirmar</h4> -->
      </div>
      <div class="modal-body">
        <h5>Do you want to use your 12th Man power?</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="twelve-man-modal-btn-si">OK</button>
        <button type="button" class="btn btn-primary" id="twelve-man-modal-btn-no">Cancel</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="dealer_card-modal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!--     <h4 class="modal-title" id="myModalLabel">Confirmar</h4> -->
      </div>
      <div class="modal-body">
        <h5>You will loose 50 points for an extra trade. Do you still want to use your Dealer power?</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="dealer_card-modal-btn-si">OK</button>
        <button type="button" class="btn btn-primary" id="dealer_card-modal-btn-no">Cancel</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="flipper_card-modal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!--     <h4 class="modal-title" id="myModalLabel">Confirmar</h4> -->
      </div>
      <div class="modal-body">
        <h5>Using this power, You can flip/change one or more players of your team in a single go?</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="flipper_card-modal-btn-si">OK</button>
        <button type="button" class="btn btn-primary" id="flipper_card-modal-btn-no">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade"  role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="shield_steal_card-modal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!--     <h4 class="modal-title" id="myModalLabel">Confirmar</h4> -->
      </div>
      <div class="modal-body">
        <!-- <h5>Using this power, You can Shield your team from stealing or steal points from other team?</h5> -->

        <button type="button" class="btn btn-default" id="shield_steal_card-modal-btn-shield">Shield</button>
        <button type="button" class="btn btn-primary" id="shield_steal_card-modal-btn-steal">Steal</button>

<div class="choose_steal_user"  style="margin: 10px 0px; display: none;">
<h6>Choose User/Team to steal points</h6>
<select name="steal_user">
     <option></option>
    <?php $__currentLoopData = $all_other_club_team_arr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ky => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <option value="<?php echo e($val->id); ?>"><?php echo e($val->userdata->first_name.' '.$val->userdata->last_name); ?> (<?php echo e(!empty($val->userdata->my_team_name) ? $val->userdata->my_team_name : (!empty($val->my_team_name) ? $val->my_team_name : 'Anonymous Team')); ?>)</option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  

</select>
</div>
          <ul style="background: #eee; margin: 10px 0px; ">
            <li><b>Using Shield Power</b> you can save your points from Steal attack of another teams.</li>
            <li><b>Using Steal Power</b> you can steal points from another team.</li>
          </ul>


          <a style="color: #ef429e;" href="<?php echo e(WEBSITE_URL.'/pages/game-rules'); ?>">Read more about Powers here.</a>

      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" id="shield_steal_card-modal-btn-si">OK</button> -->
        <button type="button" class="btn btn-primary" id="shield_steal_card-modal-btn-no">Cancel</button>
      </div>
    </div>
  </div>
</div>

<!-- <div class="alert" role="alert" id="result"></div> -->
<!-- Modal -->
<div class="modal fade" id="club_points_model" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Point System</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!-- <h4 class="modal-title">Modal Header</h4> -->
      </div>
      <div class="modal-body">
        <div class="table-responsive table-mobile">
          <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
            <tr>
              <th>CATEGORY</th>
              <th>BOWL</th>
              <th>BAT/WK/AR</th>
            </tr>
            <?php   
              if(!empty($club_points) && $club_points->isNotEmpty()){
               foreach($club_points as $key => $val){  ?>
            <tr>
              <td><?php echo e($val->attribute_name); ?></td>
              <td><?php echo e($val->bowler); ?></td>
              <td><?php echo e($val->bats_wk_ar); ?></td>
            </tr>
            <?php }
              }else{ ?>
            <tr>
              <td colspan="4"><?php echo e(trans("No record is yet available.")); ?></td>
            </tr>
            <?php } ?>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  capton_locked = '<?php echo e(in_array($capton_id, $locked_player) ? 1 : 0); ?>'; 
  
  dealer_locked = '<?php echo e($no_of_trade < MAX_NO_OF_TRADE ? 1 : 0); ?>'; 
  
  
  $(".locked_booster").click(function() {
  showMessage('Booster is locked.',"error");
  });
  
  
  $( ".not_active_booster" ).click(function() {
  var msg = "<?php echo e(!empty($sess_club_name->lockout_start_date) ? 'Game powers to be available from '.Carbon::parse($sess_club_name->lockout_start_date)->isoFormat('DD.MM.YY') : 'Booster is not active.'); ?>"; 
  showMessage(msg ,"error");
  });
  
  
  
  
  $( ".already_active_booster" ).click(function() {
  if($(this).hasClass('active')){
     showMessage('This Booster is already active.',"error");
  }else{
     showMessage('More than one booster can not be active at same time.',"error");
  }
  });
  
  
  var modalConfirm = function(callback){
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  $(".capton_point_x_3").on("click", function(){
  
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
  
      if(capton_locked == 1){
        showMessage('This power cannot be applied while your Captain is Locked!',"error");
        return false;
      }
  
  
  
    $("#mi-modal").modal({
      show: true,
      backdrop: 'static',
      keyboard: false
  });
  });
  
  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
  };
  
  modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
    // $("#result").html("CONFIRMADO");
  
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
  
      if(capton_locked == 1){
        showMessage('This power cannot be applied while your Captain is Locked!',"error");
        return false;
      }
  
  
  
  
            $.ajax({
              url: '<?php echo e(url("activate-capton-card")); ?>',
              type: 'POST',
              data: {_token: '<?php echo e(csrf_token()); ?>',  club_id: club_id},
              dataType: 'HTML',
              success: function (data) { 
                if(data == 1){
                  showMessage('Triple Cap has been applied!',"success");
  
                  setTimeout(function(){
                      location.reload(true);
                  }, 3000);
  
                }else if(data == 0){
                  showMessage('To activate Captain card, please create your Team first.',"error");
                }else if(data == 2){
                  showMessage('Triple Cap has already been activated and used for this trading period. You cannot deactivate or reuse it anymore!',"error");
                }else if(data == 3){
                  showMessage('Your max. limit of Captain card reached.',"error");
                }else {
                  showMessage('Internal error occured.',"error");
                }
              }
            });
  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
  });
  
  
  
  
  
  var twelve_man_modalConfirm = function(callback){
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  $(".twelve_man_card").on("click", function(){
  
  
  
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
  
    $("#twelve-man-modal").modal({
      show: true,
      backdrop: 'static',
      keyboard: false
  });
  });
  
  $("#twelve-man-modal-btn-si").on("click", function(){
    callback(true);
    $("#twelve-man-modal").modal('hide');
  });
  
  $("#twelve-man-modal-btn-no").on("click", function(){
    callback(false);
    $("#twelve-man-modal").modal('hide');
  });
  };
  
  twelve_man_modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
    // $("#result").html("CONFIRMADO");
  
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
  
  
            $.ajax({
              url: '<?php echo e(url("activate-twelve-man-card")); ?>',
              type: 'POST',
              data: {_token: '<?php echo e(csrf_token()); ?>',  club_id: club_id},
              dataType: 'HTML',
              success: function (data) { 
                // console.log(data); 
                if(data == 1){
                  showMessage('12th Man has been applied!',"success");
  
                  setTimeout(function(){
                      location.reload(true);
                  }, 3000);
                  
                  // setTimeout(function() { location.reload(); }, 4000);
                  twelve_player_select = true; 
                }else if(data == 0){
                  showMessage('To activate 12th Man Card, please create your Team first.',"error");
                }else if(data == 2){
                  showMessage('12th Man has already been activated and used for this trading period. You cannot deactivate or reuse it anymore!',"error");
                }else if(data == 3){
                  showMessage('Your max. limit of 12th Man Card reached.',"error");
                }else {
                  showMessage('Internal error occured.',"error");
                }
              }
            });
  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
  });
  
  
  
  
  
  
  
  // dealer card modal start 
  
  
  
  var dealer_card_modalConfirm = function(callback){
  
  // alert('hi'); 
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  $(".dealer_card").on("click", function(){
  
  
  
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
  
  
      if(dealer_locked == 1){
        showMessage('Dealer booster is locked as your free trade is remaining.',"error");
        return false;
      }
  
  
  
  
    $("#dealer_card-modal").modal({
      show: true,
      backdrop: 'static',
      keyboard: false
  });
  });
  
  $("#dealer_card-modal-btn-si").on("click", function(){
    callback(true);
    $("#dealer_card-modal").modal('hide');
  });
  
  $("#dealer_card-modal-btn-no").on("click", function(){
    callback(false);
    $("#dealer_card-modal").modal('hide');
  });
  };
  
  dealer_card_modalConfirm(function(confirm){
  // alert('hello');
  if(confirm){
    //Acciones si el usuario confirma
    // $("#result").html("CONFIRMADO");
  
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
  
  
  
      if(dealer_locked == 1){
        showMessage('Dealer booster is locked as your free trade is remaining.',"error");
        return false;
      }
  
  
            $.ajax({
              url: '<?php echo e(url("activate-dealer-card")); ?>',
              type: 'POST',
              data: {_token: '<?php echo e(csrf_token()); ?>',  club_id: club_id},
              dataType: 'HTML',
              success: function (data) { 
                // console.log(data); 
                if(data == 1){
                  showMessage('Dealer power has been applied!',"success");
  
                  setTimeout(function(){
                      location.reload(true);
                  }, 3000);
                  
                  // setTimeout(function() { location.reload(); }, 4000);
                  // twelve_player_select = true; 
                }else if(data == 0){
                  showMessage('To activate Dealer power, please create your Team first.',"error");
                }else if(data == 2){
                  showMessage('Dealer power has already been activated and used for this trading period. You cannot deactivate or reuse it anymore!',"error");
                }else if(data == 3){
                  showMessage('Your max. limit of Dealer power reached.',"error");
                }else {
                  showMessage('Internal error occured.',"error");
                }
              }
            });
  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
  });
  
  
  
  
  
  
  
  
  
  // flipper card 
  
  
  
  var flipper_card_modalConfirm = function(callback){
  
  // alert('hi'); 
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  $(".flipper_card").on("click", function(){
  
  
  
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
  
  
  
      // if(dealer_locked == 1){
      //   showMessage('Flipper booster is locked as your free trade is remaining!.',"error");
      //   return false;
      // }
  
  
  
  
    $("#flipper_card-modal").modal({
      show: true,
      backdrop: 'static',
      keyboard: false
  });
  });
  
  $("#flipper_card-modal-btn-si").on("click", function(){
    callback(true);
    $("#flipper_card-modal").modal('hide');
  });
  
  $("#flipper_card-modal-btn-no").on("click", function(){
    callback(false);
    $("#flipper_card-modal").modal('hide');
  });
  };
  
  flipper_card_modalConfirm(function(confirm){
  // alert('hello');
  if(confirm){
    //Acciones si el usuario confirma
    // $("#result").html("CONFIRMADO");
  
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
  
  
      // if(dealer_locked == 1){
      //   showMessage('Flipper booster is locked as your free trade is remaining!.',"error");
      //   return false;
      // }
  
  
  
  
            $.ajax({
              url: '<?php echo e(url("activate-flipper-card")); ?>',
              type: 'POST',
              data: {_token: '<?php echo e(csrf_token()); ?>',  club_id: club_id},
              dataType: 'HTML',
              success: function (data) { 
                // console.log(data); 
                if(data == 1){
                  showMessage('Flipper power has been applied!',"success");
  
                  setTimeout(function(){
                      location.reload(true);
                  }, 3000);
                  
                  // setTimeout(function() { location.reload(); }, 4000);
                  // twelve_player_select = true; 
                }else if(data == 0){
                  showMessage('To activate Flipper power, please create your Team first.',"error");
                }else if(data == 2){
                  showMessage('Flipper power has already been activated and used for this trading period. You cannot deactivate or reuse it anymore!',"error");
                }else if(data == 3){
                  showMessage('Your max. limit of Flipper power reached.',"error");
                }else {
                  showMessage('Internal error occured.',"error");
                }
              }
            });
  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
  });
  
  
  
  
  
  
  
  
  



  
  // Shield/Steal card 
  
  
  
  var shield_steal_card_modalConfirm = function(callback){
  
  // alert('hi'); 
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  $(".shield_steal_card").on("click", function(){
  
  
  
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
  
  
  
      // if(dealer_locked == 1){
      //   showMessage('Flipper booster is locked as your free trade is remaining!.',"error");
      //   return false;
      // }
  
  
  
  
    $("#shield_steal_card-modal").modal({
      show: true,
      backdrop: 'static',
      keyboard: false
  });
  });
  
  // $("#shield_steal_card-modal-btn-si").on("click", function(){
  //   callback(true);
  //   $("#shield_steal_card-modal").modal('hide');
  // });

  $("#shield_steal_card-modal-btn-shield").on("click", function(){

    // if(confirm('Are you sure?')){
    //   console.log('yes'); 
    //   $("#shield_steal_card-modal").modal('hide');
    // }else{
    //   console.log('no'); 
    // }

    callback('shield');

  });

  $("#shield_steal_card-modal-btn-steal").on("click", function(){
    // callback(true);


    // if(confirm('Are you sure?')){
    //   console.log('yes'); 
    //   $("#shield_steal_card-modal").modal('hide');
    // }else{
    //   console.log('no'); 
    // }

    // callback('steal');

    $('.choose_steal_user').show(); 


  });

  $('.choose_steal_user select').on('change', function(){

    if(confirm('Are you sure?')){
      callback($(this).val()); // send team id 
    }

  });



  
  $("#shield_steal_card-modal-btn-no").on("click", function(){
    callback(false);
    $("#shield_steal_card-modal").modal('hide');
  });
  };
  
  shield_steal_card_modalConfirm(function(confirm){
  // alert(confirm);
  // return; 
  if(confirm){
    //Acciones si el usuario confirma
    // $("#result").html("CONFIRMADO");
  
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
  
  
      // if(dealer_locked == 1){
      //   showMessage('Flipper booster is locked as your free trade is remaining!.',"error");
      //   return false;
      // }
  
  
  
  
            $.ajax({
              url: '<?php echo e(url("activate-shield-steal-card")); ?>',
              type: 'POST',
              data: {_token: '<?php echo e(csrf_token()); ?>',  club_id: club_id, power_type: confirm},
              dataType: 'HTML',
              success: function (data) { 
                // console.log(data); 
                if(data == 1){
                  showMessage('Shield/Steal power has been applied!',"success");
  
                  setTimeout(function(){
                      location.reload(true);
                  }, 3000);
                  
                  // setTimeout(function() { location.reload(); }, 4000);
                  // twelve_player_select = true; 
                }else if(data == 0){
                  showMessage('To activate Shield/Steal power, please create your Team first.',"error");
                }else if(data == 2){
                  showMessage('Shield/Steal power has already been activated and used for this trading period. You cannot deactivate or reuse it anymore!',"error");
                }else if(data == 3){
                  showMessage('Your max. limit of Shield/Steal power reached.',"error");
                }else {
                  showMessage('Internal error occured.',"error");
                }
              }
            });
  
  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
  });
  
  
  
  
  $(document).ready(function() {
    $('.choose_steal_user select').select2({
    placeholder: "choose User/Team to steal points.",
});



});
  
   $(document).on('hide.bs.modal', function(){
    // alert('asdf');
    $('.choose_steal_user').hide(); 
  });
  
  
  
  
  
</script>
<!-- ************************************ -->
<style type="text/css">
  .batsman_info li{ cursor: pointer;}
  th.sorting_asc, 
  th.sorting, 
  th.sorting_desc{ background: #1c86d3; color: #fff;}
  #example_wrapper.dataTables_wrapper .row:first-child{ display: none;}
  /*.team_image_sec .gameweek_list{ max-height: none; overflow: visible;}
  .team_image_sec .gameweek_list li{ width: 100%;}*/
  .team-filedview ul li .remove-player{ width: 1.3rem; height: 1.3rem; border-radius: 50%; background-color: transparent; position: absolute; left: -0.6rem; top: -0.6rem; display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-align: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center; cursor: pointer; font-size: 1rem; color: #b70f0f; z-index: 2;}
  .list_view_tab .player_data span.pl_price{ float: right; margin-left: 20px;}
  .highcharts-exporting-group{ display: none;}
  ul.booster_list li{ cursor: pointer;}
  .unavailable-player{
  width: 1rem;
  height: 1rem;
  border-radius: 50%;
  background-color: white;
  position: absolute;
  left: -0.6rem;
  bottom: -0.6rem;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  cursor: pointer;
  font-size: 1rem;
  color: #b70f0f;
  z-index: 2;
  }
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/user/myteam.blade.php ENDPATH**/ ?>
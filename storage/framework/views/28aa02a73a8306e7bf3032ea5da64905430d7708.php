<?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('js/bootstrap-datepicker.js')); ?>"></script>
<link href="<?php echo e(asset('css/bootstrap-datepicker.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('css/datepicker-custom-theme.css')); ?>" rel="stylesheet">
 
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end" style="background-image: url(<?php echo e(asset('img/inner_banner.png')); ?>);">
    </section>
    <section class="signin_signup_block py-5">
        <div class="container">
            <h2 class="form_title mb-3">My Profile</h2>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                    <?php echo e(Form::open(['role' => 'form','route' => "User.SaveupdateProfile",'class' => 'form_block p-4','id'=>'signup','files' => true])); ?>

                        
                          <div class="game_rep_detail_sec mt-4">
                            <h4 class="form_sec_heading text-uppercase mb-2">Personal Details</h4>
                            <div class="row mx-n2">
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="First Name*" name="first_name" value="<?php echo e(auth()->guard('web')->user()->first_name); ?>" />
                                        <span class="help-inline first_name_error"></span>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Last Name*" name="last_name" value="<?php echo e(auth()->guard('web')->user()->last_name); ?>" />
                                        <span class="help-inline last_name_error"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mx-n2">
                                <div class="col-12 col-sm-12 px-2">                            
                                    <div class="form-group">
                                        <input class="form-control" type="text" placeholder="Email Address" name="email" value="<?php echo e(auth()->guard('web')->user()->email); ?>" />
                                        <span class="help-inline email_error"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mx-n2">
                               <?php /*   <div class="col-12 col-sm-6 px-2">
                                  <div class="form-group">
                                        <?php $phoneNumber = !empty(auth()->guard('web')->user()->phone) ? auth()->guard('web')->user()->phone :''; ?>
                                        <input class="form-control" type="text" placeholder="Phone No." name="phone" value="{{$phoneNumber}}" />
                                        <span class="help-inline phone_error"></span>
                                    </div>
                                </div>  */ ?>
                                <div class="col-12 col-sm-12 px-2">

                                    <div class="form-group">
                                        <div class="text_iconview_block d-flex align-items-center p-0">
                                            <input type="text" placeholder="DOB" class="dob" name="dob" value="<?php echo e(Carbon\Carbon::parse(auth()->guard('web')->user()->dob)->format('d/m/Y')); ?>" />

                                            <span class="help-inline"></span>
                                            <a class="text_ivon d-flex align-items-center justify-content-center" href=""><i class="far fa-calendar-alt"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mx-n2">
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::select('gender',[null => 'Select Gender']+Config::get('gender_type'),auth()->guard('web')->user()->gender, ['class' => 'custom-select choosen_selct'])); ?>

                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::select('about_us',[null => 'Hear about us ?']+$aboutUs,auth()->guard('web')->user()->gender, ['class' => 'custom-select choosen_selct'])); ?>

                                    </div>
                                </div>
                            </div>

                            <div class="row mx-n2">
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::text('sport_name','Cricket',['class' => 'form-control','readonly'])); ?>

                                        <span class="help-inline"></span>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::select('country',[null => 'Select Country *']+$countryList,!empty($userDetails->country) ? $userDetails->country
                                        :'', ['class' => 'custom-select country','id' => 'country_id'])); ?>

                                        <span class="help-inline country_error"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row mx-n2">
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="get_state_class">
                                        <div class="form-group">
                                            <?php echo e(Form::select('state', [null => 'Select State *'],'',['id' => 'state_id','class'=>'custom-select'])); ?>

                                            <span class="help-inline state_error"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 px-2">
                                    <div class="get_city_class">
                                        <div class="form-group">
                                            <?php echo e(Form::text("city",!empty($userDetails->city) ? $userDetails->city
                                        :'', ['placeholder' => 'City *','class'=>'form-control city'])); ?>

                                            <span class="help-inline city_error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <?php $authUser = auth()->guard('web')->user(); ?>
                            <div class="form-group">

<div class="mb-3 row ">
    <div class="col-12 col-sm-6">
    <span class="custom_check">Upload Profile &nbsp; <input type="checkbox" name="file_option" id="btnCheck1" value="1" <?php echo($authUser->is_avatar==0) ? 'checked' :''; ?>  /><span class="check_indicator">&nbsp;</span></span>
</div>
    <div class="col-12 col-sm-6">
                                <span class="custom_check">Upload Avatar &nbsp; <input type="checkbox" name="file_option" id="btnCheck2" value="2" <?php echo($authUser->is_avatar == 1) ? 'checked' :''; ?> /><span class="check_indicator">&nbsp;</span></span>
                            </div>
</div>
                                


                                <?php if(empty($authUser->is_avatar)){ ?>
                                <div class="uploadDiv">
                                <?php }else{ ?>
                                    <div class="uploadDiv d-none">
                                <?php } ?>
                                <input class="form-control" type="file" placeholder="Profile Image" name="image" id="userProfile" />
                                <?php if(!empty(auth()->guard('web')->user()->image) && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.auth()->guard('web')->user()->image)): ?>
                                    <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo USER_PROFILE_IMAGE_URL.auth()->guard('web')->user()->image; ?>"> <div class="usermgmt_image mt-2">
                                            <img class="rounded-circle-custom" src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.USER_PROFILE_IMAGE_URL.auth()->guard('web')->user()->image ?>">
                                        </div>
                                    </a>
                                <?php endif; ?>
                                </div>
                               
                                <input type="hidden" name="is_avatar" value="" id="is_avatar" class="is_avatar">
                                <?php if(!empty($authUser->is_avatar)){ ?>
                                    <div class="imageDiv">
                                <?php }else{ ?>   
                                    <div class="imageDiv d-none">
                                <?php } ?>
                                    <img src="<?php echo e(asset('uploads/user_profile/av01.jpg')); ?>" class="imageSelected rounded-circle <?php echo  ($authUser->image=='av01.jpg') ? "selected":''; ?>" data-name=""av01.jpg">

                                    <img src="<?php echo e(asset('uploads/user_profile/av02.jpg')); ?>" class="imageSelected rounded-circle <?php echo  ($authUser->image=='av02.jpg') ? "selected":''; ?>" data-name="av02.jpg">

                                    <img src="<?php echo e(asset('uploads/user_profile/av03.jpg')); ?>" class="imageSelected rounded-circle <?php echo  ($authUser->image=='av03.jpg') ? "selected":''; ?>" data-name="av03.jpg">

                                    <img src="<?php echo e(asset('uploads/user_profile/av04.jpg')); ?>" class="imageSelected rounded-circle <?php echo  ($authUser->image=='av04.jpg') ? "selected":''; ?>" data-name="av04.jpg">

                                    <img src="<?php echo e(asset('uploads/user_profile/av05.jpg')); ?>" class="imageSelected rounded-circle <?php echo  ($authUser->image=='av05.jpg') ? "selected":''; ?>" data-name="av05.jpg">

                                    <img src="<?php echo e(asset('uploads/user_profile/av06.jpg')); ?>" class="imageSelected rounded-circle <?php echo  ($authUser->image=='av06.jpg') ? "selected":''; ?>" data-name="av06.jpg">

                                    <img src="<?php echo e(asset('uploads/user_profile/av07.jpg')); ?>" class="imageSelected rounded-circle <?php echo  ($authUser->image=='av07.jpg') ? "selected":''; ?>" data-name="av07.jpg">

                                    <img src="<?php echo e(asset('uploads/user_profile/av08.jpg')); ?>" class="imageSelected rounded-circle <?php echo  ($authUser->image=='av08.jpg') ? "selected":''; ?>" data-name="av08.jpg">

                                    <img src="<?php echo e(asset('uploads/user_profile/av09.jpg')); ?>" class="imageSelected rounded-circle <?php echo  ($authUser->image=='av09.jpg') ? "selected":''; ?>" data-name="av09.jpg">

                                    <img src="<?php echo e(asset('uploads/user_profile/av10.jpg')); ?>" class="imageSelected rounded-circle <?php echo  ($authUser->image=='av10.jpg') ? "selected":''; ?>" data-name="av10.jpg">
                                </div>
                                 <span class="help-inline upload_file1" style="height: 24px;"></span>
                                <?php if($authUser->is_avatar == 1 && !empty($authUser->image)){ ?>
                                        <input type="hidden" name="upload_file" value="1" id="upload_file2">
                                <?php }elseif($authUser->is_avatar == 0 && !empty($authUser->image)){ ?>
                                        <input type="hidden" name="upload_file" value="1" id="upload_file3">
                                <?php }else{ ?>
                                        <input type="hidden" name="upload_file" value="" id="upload_file">
                                <?php } ?>
                            </div>

                        </div>
                       
                        <div class="club_detail_sec mt-4">
                            <h4 class="form_sec_heading text-uppercase mb-2">Set Default Mode And Games</h4>
                            <div class="row mx-n2">
                                <div class="col-12 col-sm-12 px-2">
                                    <div class="form-group">
                                        <?php echo e(Form::text('my_team_name',!empty(auth()->guard('web')->user()->my_team_name) ? auth()->guard('web')->user()->my_team_name:'',['class' => 'form-control', 'placeholder' => 'Enter team name'])); ?>

                                        <span class="help-inline my_team_name_error"></span>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group">
                                  <?php echo e(Form::select('my_game_mode',[null => 'Default Lobby Mode *']+Config::get('home_club'),!empty(auth()->guard('web')->user()->game_mode) ? auth()->guard('web')->user()->game_mode:'', ['class' => 'custom-select game_mode'])); ?> 
                                <span class="help-inline my_game_mode_error"></span>
                            </div>
                            <div class="form-group put_club_html">
                                <?php echo e(Form::select('my_club_name',[null => 'Default Senior Lobby Club *']+$seniorClub,!empty(auth()->guard('web')->user()->senior_club_name) ? auth()->guard('web')->user()->senior_club_name:'', ['class' => 'custom-select'])); ?>

                                <span class="help-inline my_club_name_error"></span>
                            </div>

                            <div class="form-group put_club_html">
                                <?php echo e(Form::select('junior_club_name',[null => 'Default Junior Lobby Club *']+$juniorClub,!empty(auth()->guard('web')->user()->junior_club_name) ? auth()->guard('web')->user()->junior_club_name:'', ['class' => 'custom-select'])); ?>

                                <span class="help-inline junior_club_name_error"></span>
                            </div>
                            <div class="form-group put_club_html">
                                <?php echo e(Form::select('league_club_name',[null => 'Default League Lobby Club *']+$leagueClub,!empty(auth()->guard('web')->user()->league_club_name) ? auth()->guard('web')->user()->league_club_name:'', ['class' => 'custom-select'])); ?>

                                <span class="help-inline league_club_name_error"></span>
                            </div>


                            <!-- <div class="form-group put_game_name_html">
                                <input class="form-control" type="text" placeholder="Game Name" name="game_name" readonly />
                                <span class="help-inline game_name"></span>
                            </div> -->

                        </div>
                      
                        <div class="game_rep_detail_sec py-3">
                            <h4 class="form_sec_heading text-uppercase mb-2">Notifications</h4>
                            <ul class="authorize_list">
                                <li class="py-2">
                                    <label class="join_our_trems d-flex position-absolute p-0">
                                        <span class="custom_check">
                                            <input type="checkbox" name="newslater" id="terms" <?php echo(auth()->guard('web')->user()->is_subscribe) ? 'checked' :'' ?>>
                                            <span class="check_indicator">&nbsp;</span>
                                        </span>

                                    </label>

                                    <label for="terms">Subscribe to newsletter.</label> <br>
                                    <span class="help-inline terms"></span>
                                </li>
                                <li class="py-2">
                                    <label class="join_our_trems d-flex position-absolute p-0">
                                        <span class="custom_check">
                                            <input type="checkbox" name="lockout_notification" id="lockout_notification" <?php echo(auth()->guard('web')->user()->lockout_notification) ? 'checked' :'' ?>>
                                            <span class="check_indicator">&nbsp;</span>
                                        </span>
                                    </label>
                                   <label for="lockout_notification">Notice for start of my game lockout.</label><br>
                                    <span class="help-inline privacy"></span>
                                </li>
                                <li class="py-2">
                                    <label class="join_our_trems d-flex position-absolute p-0">
                                        <span class="custom_check">
                                            <input type="checkbox" name="trading_lockout_notification" id="trading_lockout_notification" <?php echo(auth()->guard('web')->user()->trading_lockout_notification) ? 'checked' :'' ?>>
                                            <span class="check_indicator">&nbsp;</span>
                                        </span>
                                    </label>
                                   <label for="trading_lockout_notification">Notice for the start of trading window.</label><br>
                                    <span class="help-inline trading"></span>
                                </li>
                            </ul>
                        </div>
                        <input class="w-100 btn text-uppercase" type="button" value="Update" onclick='signup();'>
                        
                    <?php echo e(Form::close()); ?>

                </div>
            </div>
        </div>
    </section>    
</div>
<script type="text/javascript">
    $('#userProfile').on('change',function(){
        $("#upload_file").val(1);
        $("#upload_file2").val(1);
        $("#upload_file3").val(1);
    });
    $('#btnCheck1').on('click',function(){
        if($(this). prop("checked") == true){
            $("#btnCheck2"). prop("checked", false);
            $(".uploadDiv").removeClass("d-none");
            $(".imageDiv").addClass("d-none");
        }
    });
    $('#btnCheck2').on('click',function(){
        if($(this). prop("checked") == true){
            $("#btnCheck1"). prop("checked", false);
            $(".imageDiv").removeClass("d-none");
            $(".uploadDiv").addClass("d-none");
        }
    });

    $('.imageSelected').on('click',function(){
        $('.imageSelected').removeClass('selected');
        var imageName = $(this).data('name');
        $('.is_avatar').val(imageName);
        $("#upload_file").val(1);
        $("#upload_file2").val(1);
        $("#upload_file3").val(1);
        $(this).addClass('selected');
    });


 game_mode = "<?php echo !empty(auth()->guard('web')->user()->game_mode) ? auth()->guard('web')->user()->game_mode:'' ?>";
 /*
$(".game_mode").on('change',function(){ 
    var selectValue   =  $(this).val();
    game_mode = selectValue ; 
    // alert(game_mode); 
    $('#loader_img').show(); 
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"<?php echo e(url('get-club-mode-listing')); ?>",
        'type':'post',
        data:{'id':selectValue},
        success:function(response){           
            $('.put_club_html').html(response);
            $('#loader_img').hide();
        }
    });
});
*/
var user_role_id = "<?php echo !empty(auth()->guard('web')->user()->user_role_id) ? auth()->guard('web')->user()->user_role_id:'' ?>";


if(user_role_id == 2){
    var club_id = "<?php echo !empty(auth()->guard('web')->user()->club_name) ? auth()->guard('web')->user()->club_name:'' ?>";
}else{
    var club_id = "<?php echo !empty(auth()->guard('web')->user()->id) ? auth()->guard('web')->user()->id:'' ?>";
}
/*
if(game_mode !=""){ 
    $('#loader_img').show(); 
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"<?php echo e(url('get-club-mode-listing')); ?>",
        'type':'post',
        data:{'id':game_mode,'club_id':club_id},
        success:function(response){           
            $('.put_club_html').html(response);
            $('#loader_img').hide();
        }
    });
}if(club_id !=''){
    $('#loader_img').show(); 
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"<?php echo e(url('get-club-game-name')); ?>",
        'type':'post',
        data:{'id':club_id},
        success:function(response){
            $('.put_game_name_html').html(response);
            $('#loader_img').hide();
        }
    });
}
*/
function signup() { 
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    var form = $('#signup')[0];
    var formData = new FormData(form);
    // console.log(formData); 
    $.ajax({ 
        url: '<?php echo e(route("User.SaveupdateProfile")); ?>',
        type:'post',
        data: formData,
        processData: false,
        contentType: false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
                window.location.href     =  "<?php echo e(route('User.lobby')); ?>";
            }else { 
                // console.log(data['errors']); 
                $.each(data['errors'],function(index,html){

                    // console.log(index); 
                    // console.log(html); 
                    $('.'+index+'_error').addClass('error');
                    $('.'+index+'_error').html(html);
                    if(index=="state"){ 
                        $(".state_error").addClass('error');
                        $(".state_error").html(html);
                    }if(index=="country"){ 
                        $(".country_error").addClass('error');
                        $(".country_error").html(html);
                    } 
                    if(index=="city"){ 
                        $(".city").addClass('error');
                        $(".city").html(html);
                    }
                    if(index=="upload_file"){ 
                        $(".upload_file1").addClass('error');
                        $(".upload_file1").html(html);
                    }
                    // if(index=="terms"){ 
                    //     $(".terms").next().addClass('error');
                    //     $(".terms").next().html(html);
                    // }  
                    // if(index=="game_mode"){ 
                    //     $(".my_game_mode_error").addClass('error');
                    //     $(".my_game_mode_error").html(html);
                    // }
                });
                $('#loader_img').hide();
            }
            $('#loader_img').hide();
        }
    });
}
    
$('#signup').each(function() {
    $(this).find('input').keypress(function(e) {
        if(e.which == 10 || e.which == 13) {
        signup();
        return false;
       }
    });
});

$(function(){
    $('.dob').datepicker({
        format  : 'dd/mm/yyyy',
        endDate: "today",
        autoclose:true,
    }); 
});

var countryId = "<?php echo !empty($userDetails->country) ? $userDetails->country :''; ?>";
var state_id = "<?php echo !empty($userDetails->state) ? $userDetails->state :''; ?>";
var city_id = "<?php echo !empty($userDetails->city) ? $userDetails->city :''; ?>";
if(countryId !=''){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    $.ajax({ 
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: "<?php echo e(URL('get-state')); ?>",
        type:'post',
        data:{'country_id':countryId,'state_id':state_id},
        success: function(r){
            $('.get_state_class').html(r);
            $('#loader_img').hide();
        }
    });
}
/*if(state_id !=''){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    $.ajax({ 
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "<?php echo e(URL('get-city')); ?>",
        type:'post',
        data:{'state_id':state_id,'city_old_id':city_id},
        success: function(r){ 
            $('.get_city_class').html(r);
            $('#loader_img').hide();
        }
    });
}*/
$('#country_id').on('change',function(){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    var country_id = $('#country_id').val();
    $.ajax({ 
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "<?php echo e(URL('get-state')); ?>",
        type:'post',
        data:{'country_id':country_id},
        success: function(r){
            $('.get_state_class').html(r);
            $('#loader_img').hide();
        }
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/user/update_profile.blade.php ENDPATH**/ ?>
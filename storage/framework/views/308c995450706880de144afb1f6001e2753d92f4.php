<?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('js/bootstrap-datepicker.js')); ?>"></script>
<link href="<?php echo e(asset('css/bootstrap-datepicker.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('css/datepicker-custom-theme.css')); ?>" rel="stylesheet">
<script src="<?php echo e(asset('css/admin/fancybox/jquery.fancybox.js')); ?>"></script>
<link href="<?php echo e(asset('css/admin/fancybox/jquery.fancybox.css')); ?>" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">



<div class="full_container">
	<div class="body_section">
		<!-- top_header element -->
		<!-- <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>       -->
		<section class="status_tab_block mb-5 py-3">
			<div class="container mobile_cont">
				
				<div class="tab_content_sec border p-4">
					<h4 class="status_title mb-3">My Player Profile <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="Your account has been verified and linked to a player profile in the fantasy game listed below. You can now directly update the below available information on your player profile."><i style="color: #0f70b7; " class="fa fa-info-circle" aria-hidden="true"></i></a></h4>

					<!-- <div class="alert alert-success player_verified_message mb-4" role="alert"></div> -->


                    <?php echo e(Form::open(['role' => 'form','route' => 'Gloabal.savePlayerAccount','class' => 'statistics_form my_player_form mt-4','id'=>'savePlayerAccount','files'=>'true'])); ?>

                    <?php echo e(Form::hidden('player_id',!empty($playerId) ? $playerId:0)); ?>

                        <span class="d-inline-block game_name_btn mb-4">Game: <?php echo e(!empty($clubDetails->game_name) ? $clubDetails->game_name :''); ?></span>
                        
                    <?php /*
                        <div class="club_role_sec d-block d-sm-flex justify-content-between m-n2">
                            <div class="club_sec p-2">
                                <div class="club_role d-flex">
                                    <!-- <label class="club_role_name text-center py-2 px-3 m-0" for="">Club</label> -->
                                    <label class="game_week_info_status py-2 px-3 m-0" for="">Club <small>{{!empty($clubDetails->club_name) ? $clubDetails->club_name :'' }}</small></label>
                                </div>
                            </div>
                            <div class="club_sec p-2">
                                <div class="club_role d-flex">
                                    <label class="game_week_info_status py-2 px-3 m-0" for="">Player<small>{{!empty($playerDetails->full_name) ? $playerDetails->full_name :'' }}</small></label>
                                </div>
                            </div>
                            <div class="club_sec p-2">
                                <div class="club_role d-flex">
                                   <!--  <label class="club_role_name text-center py-2 px-3 m-0" for="">Position</label>
                                    <span class="club_role_value text-center py-2 px-3">
                                    </span> -->
                                    <label class="game_week_info_status py-2 px-3 m-0" for="">Position <small><?php
                                        if(!empty($playerDetails->position)){
                                         if($playerDetails->position ==1){
                                            echo "Bat";
                                        }elseif ($playerDetails->position ==2) {
                                            echo "Bowl";
                                        }elseif ($playerDetails->position ==3) {
                                            echo "AR";
                                        }elseif ($playerDetails->position ==4) {
                                            echo "WK";
                                        } }?></small></label>
                                </div>
                            </div> 
                           

                        </div>
 */?>
                        <div class="row pt-2 club_detail_class">
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">Club:</label>
                                  <span class=""> <?php echo e(!empty($clubDetails->club_name) ? $clubDetails->club_name :''); ?></span>
                                    <span class="help-inline"></span>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">Player:</label>
                                  <span class=""> <?php echo e(!empty($playerDetails->full_name) ? $playerDetails->full_name :''); ?></span>
                                    <span class="help-inline"></span>
                                </div>
                            </div>
                        </div>
                         <div class="row pt-2 club_detail_class">
                             <div class="col-12 col-sm-6 col-md-4">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">Position:</label>
                                  <span class=""> <?php
                                        if(!empty($playerDetails->position)){
                                         if($playerDetails->position ==1){
                                            echo "Bat";
                                        }elseif ($playerDetails->position ==2) {
                                            echo "Bowl";
                                        }elseif ($playerDetails->position ==3) {
                                            echo "AR";
                                        }elseif ($playerDetails->position ==4) {
                                            echo "WK";
                                        } }?></span>
                                    <span class="help-inline"></span>
                                </div>
                            </div>
                         </div>
					    <div class="row pt-2">
					        <div class="col-12 col-sm-6 col-md-4">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">Batting Style</label>
                                    <?php echo e(Form::select(
                                         'bat_style',
                                         [null => 'Please Select Bat Style'] + $batStyle,
                                         isset($playerDetails->bat_style) ? $playerDetails->bat_style :'',
                                         ['id' => 'bat_style','class'=>'custom-select']
                                        )); ?>

                                    <span class="help-inline"></span>
                                </div>
                            </div>
					        <div class="col-12 col-sm-6 col-md-4 offset-md-4">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">Bowling Style</label>
                                    <?php echo e(Form::select(
                                         'bowl_style',
                                         [null => 'Please Select Bowl Style'] + $bowlStyle,
                                         isset($playerDetails->bowl_style) ? $playerDetails->bowl_style :'',
                                         ['id' => 'bowl_style','class'=>'custom-select']
                                        )); ?>

                                    <span class="help-inline"></span>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">About</label>
                                    <textarea name="description" class="form-control" placeholder="Max 100 Words."><?php echo isset($playerDetails->description) ? $playerDetails->description :''?></textarea>
                                     <span class="help-inline"></span>
                                </div>
                            </div>

                            
                            <div class="col-12">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">Player Profile Photo</label>
                                    <div class="uploaded_file_sec d-flex">
                                        <div class="upload_file d-inline-flex align-items-center justify-content-center">
                                            <?php if(!empty($playerDetails->image)) { ?>
                                            <?php if(File::exists(PLAYER_IMAGE_ROOT_PATH.$playerDetails->image) && !empty($playerDetails->image)): ?>
                                            <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo PLAYER_IMAGE_URL.$playerDetails->image; ?>">
                                            <img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.PLAYER_IMAGE_URL.'/'.$playerDetails->image ?>">
                                            </a>
                                             <?php endif; ?>
                                         <?php } ?>
                                            </div>
                                        <div class="upload_file d-inline-flex align-items-center justify-content-center position-relative ml-3"><?php echo e(Form::file('image', array('accept' => 'image/*'))); ?> Upload</div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-12">
                                <label for="" class="feilds_name">Availability</label>
                                <div class="d-block d-md-flex">
                                     <!-- <div class="uploaded_file_sec d-block d-md-flex">
                                        <div class="upload_img d-inline-flex align-items-center justify-content-center">Photo</div>
                                    </div>  -->
                                    <div class="row abailability_sec_row pt-2 pt-md-0 pl-0">
                                        <div class="col-12 col-sm-6 col-md-4">
                                            <div class="form-group mb-4 mb-md-0">
                                                <label for="" class="feilds_name">From</label>
                                                <?php echo e(Form::text('date_from','', ['class' => 'form-control','id'=>'date_from'])); ?>

                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4">
                                            <div class="form-group mb-4 mb-md-0">
                                                <label for="" class="feilds_name">Till</label>
                                                 <?php echo e(Form::text('date_till','', ['class' => 'form-control','id'=>'date_till'])); ?>

                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-4">
                                            <div class="form-group mb-4 mb-md-0">
                                                <label for="" class="feilds_name">Reason</label>
                                                <input type="text" class="form-control" name="reason" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
					    </div>
					    <input type="button" class="btn mt-0 mt-md-4" value="Update" onclick='save_player_account();'>
					<?php echo e(Form::close()); ?>

                    <br>
                    <?php if(!$availabilityData->isEmpty()): ?>
                    <div class="table-responsive table-mobile">
                        <table id="example" width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                            <tbody><tr>
                                <th class="colmn1"> Reason
                                </th>
                                <th class="colmn2">From
                                </th class="colmn3">
                                <th>Till
                                </th>
                            </tr>
                            <?php if(!$availabilityData->isEmpty()): ?>
                            <?php $__currentLoopData = $availabilityData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($data->reason); ?></td>
                                <td data-search="<?php echo e($data->date_from); ?>"><?php echo e(date('d.m.y' ,strtotime($data->date_from))); ?></td>
                                <td data-search="<?php echo e($data->date_till); ?>"><?php echo e(date('d.m.y' ,strtotime($data->date_till))); ?></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                    
                             </tbody>

                         </table>
                    </div>
                    <?php endif; ?>
				</div>

			</div>

		</section>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   

    });
    function save_player_account() { 
        $('#loader_img').show();
        $('.help-inline').html('');
        $('.help-inline').removeClass('error');
        var form = $('#savePlayerAccount')[0];
        var formData = new FormData(form);
        $.ajax({ 
            url: '<?php echo e(URL("player-account")); ?>',
            type:'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function(r){
                error_array     =   JSON.stringify(r);
                data            =   JSON.parse(error_array);
                console.log(data['success']);
                if(data['success'] == 1) {
                    window.location.href     =  "<?php echo e(URL('player-account')); ?>";
                }else if(data['success'] == 2){
                     window.location.href     =  "<?php echo e(URL('player-account')); ?>";
                }else { 
                    $.each(data['errors'],function(index,html){
                        $("#"+index).next().addClass('error');
                        $("#"+index).next().html(html);
                    });
                    $('#loader_img').hide();
                }
                $('#loader_img').hide();
            }
        });
    }
    
    $('#savePlayerAccount').each(function() {
        $(this).find('input').keypress(function(e) {
              if(e.which == 10 || e.which == 13) {
                save_player_account();
                return false;
               }
           });
    });


    $("#date_from").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      startDate:new Date(),
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#date_till').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('#date_till').datepicker('setStartDate', null);
    });

    $("#date_till").datepicker({
       format: 'yyyy-mm-dd',
       autoclose: true,
    }).on('changeDate', function (selected) {
       var endDate = new Date(selected.date.valueOf());
       $('#date_from').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
       $('#date_from').datepicker('setEndDate', null);
    });

    $('.fancybox').fancybox();
    $('.fancybox-buttons').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        prevEffect : 'none',
        nextEffect : 'none',
    });
</script>
<style type="text/css">
   #example_wrapper .dataTables_info { display: none; }
   #example_filter { text-align: right; }
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/common/player_account.blade.php ENDPATH**/ ?>
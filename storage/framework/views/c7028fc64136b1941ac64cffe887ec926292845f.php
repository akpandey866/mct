<?php $__env->startSection('content'); ?>
<div class="body_section">
    <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <section class="status_tab_block mb-2">
        <div class="container mobile_cont">
            <?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="tab_content_sec border p-4">
                <div class="d-block d-lg-flex align-items-center mb-3">
                    <!-- <div class="d-inline-flex align-items-center mr-auto">
                            <h4 class="status_title mr-auto">Premier</h4>
                            <a href="" class="sponsors_btn py-2 ml-3 ml-md-3">Pay : $20</a>
                        </div> -->
             <!--    <h4 class="status_title mb-4">Rank</h4> -->
                    <!-- <form action="" class="statistics_form ml-auto">
                        <div class="row">
                            <div class="col-12 col-sm-6 py-2">
                                <select class="custom-select">
                                    <option value="">Overall</option>
                                </select>
                            </div>
                            <div class="col-12 col-sm-6 py-2">
                                <div class="search_players position-relative">
                                    <input class="form-control" type="text" placeholder="">
                                    <span class="search_players_icon"><i class="fas fa-search"></i></span>
                                </div>
                            </div>
                        </div>
                    </form> -->
                </div>
                <ul class="rank_list d-block d-sm-flex flex-wrap">
                    <?php if(!empty($user_teams)): ?>
                    <?php $n=1; ?>
                    <?php $__currentLoopData = $user_teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="d-inline-flex align-items-center">
                        <div class="mb-3 mb-sm-0">
                            <div class="rank_number_info"><?php echo e(isset($team_ranks_arr[$value->id]) ? $team_ranks_arr[$value->id] : 0); ?><small class="pt-1">Rank</small></div>
                        </div>
                        <div class="w-100 rank_info d-flex align-items-center p-3">
                            <div>
                                <div class="rank_player_pic">
                                    <?php if(!empty($value->userdata->image) && File::exists(CLUB_IMAGE_ROOT_PATH.$value->userdata->image) && !empty($value->userdata->image)): ?> 
                                    <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.USER_PROFILE_IMAGE_URL.'/'.$value->userdata->image ?>" alt="Partners">
               <?php elseif(!empty($value->userdata->club_logo) && File::exists(CLUB_IMAGE_ROOT_PATH.$value->userdata->club_logo)): ?>
                    <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=65px&height=65px&cropratio=1:1&image='.CLUB_IMAGE_URL.$value->userdata->club_logo ?>" alt="cricketclub" >


                    <?php else: ?>  
                         <?php if(!empty($value->userdata->gender)){ if($value->userdata->gender==MALE){ ?>
                            <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.WEBSITE_IMG_URL.'rank_male.png' ?>" alt="user_image">
                        <?php } elseif ($value->userdata->gender==FEMALE) { ?>
                            <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.WEBSITE_IMG_URL.'rank_female.png' ?>" alt="user_image">
                        <?php }elseif ($value->userdata->gender==DONOTSPECIFY) { ?> 
                            <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.WEBSITE_IMG_URL.'rank_male_female.png' ?>" alt="user_image">
                        <?php }else{ ?>
                            <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.WEBSITE_IMG_URL.'rank_male.png' ?>" alt="user_image">
                        <?php } }else{ ?>
                            <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.WEBSITE_IMG_URL.'rank_male.png' ?>" alt="user_image">
                        <?php } ?>
                                    
                 <?php endif; ?>
                                </div>
                            </div>
                            <div class="w-100 rank_player_info d-flex align-items-center pl-3">
                                <div class="w-50 mr-auto">
                                    <div class="rank_player_name d-flex align-items-center"><span><a href=""><?php echo e($value->my_team_name); ?></a></span> 
                                        <?php $fundraiserExists = showFundraiserUser($value->user_id); 
                                        if(!empty($fundraiserExists)){ ?>
                                        <img class="ml-2" src="<?php echo e(asset('img/right.png')); ?>" alt="right" title="Fundraiser">
                                        <?php } ?>
                                    </div>
                                    <div class="rank_player_sheild d-flex align-items-center">
   <?php echo e(!empty($value->userdata->first_name) ? ucfirst(substr($value->userdata->first_name, 0, 2).'. ') : ''); ?>

                                <?php echo e(!empty($value->userdata->last_name) ? $value->userdata->last_name : ''); ?>

                            </div>
                                </div>
                                <div class="w-50 d-flex pl-2 ml-auto">
                                     <div class="game_week_info_status d-none d-md-inline-block px-1 mr-auto"><?php echo e(isset($temp_points_arr_past_wk[$value->id]) ? $temp_points_arr_past_wk[$value->id] : 0); ?> <small>Last GW Pts</small></div>
                                    <div class="game_week_info_status px-1 ml-auto"><?php echo e(isset($temp_points_arr[$value->id]) ? $temp_points_arr[$value->id] : 0); ?> <small>Total Pts.</small></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php $n++; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                    <li class="d-inline-flex align-items-center">
                        <div class="w-100 sponsors_info pl-0 pl-md-4 py-4 py-md-0">
                            <div class="sponsors_list_heading mb-1">No record is yet available</div>
                        </div>
                    </li>
                    <?php endif; ?>
                </ul>
                <?php if($tot_team_count > $item_per_page): ?>
                <div style="text-align: center;"><a href="javascript:void();" data-toggle="modal"  class="more_link border ml-auto">Load More</a></div>
                <?php endif; ?>
            </div>

        </div>

    </section>


    <div class="container pt-4">
        <div class="favorite_players_block mb-5">
            <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
        </div>
    </div>
</div>
<style type="text/css">
    .search_players_icon{ position: absolute; top: 12px; right: 4px;}
</style>


    <script type="text/javascript">
$( document ).ready(function() {
    tot_team_count = parseInt('<?php echo e($tot_team_count); ?>');
    item_per_page = parseInt('<?php echo e($item_per_page); ?>');
  page = 0; 
    $( ".more_link" ).click(function() {
         $('#loader_img').show(); 
        page++; 
        $.ajax({
            url: '<?php echo e(url("rank")); ?>',
            type: 'GET',
            data: { page: page },
            dataType: 'HTML',
            success: function (data) {
                console.log(data); 
                $('.rank_list').append(data) ; 
                 $('#loader_img').hide(); 
                 if(((page+1) * item_per_page) >= tot_team_count){
                    $('.more_link').remove(); 
                 }
            }
        });  
    }); 
}); 

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/common/rank.blade.php ENDPATH**/ ?>
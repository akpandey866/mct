<?php 
    use Carbon\Carbon;
    ?>

<?php $__env->startSection('content'); ?>
<div class="full_container">
<div class="body_section">
    <!-- top_header element -->
    <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>       
    <section class="status_tab_block mb-5">
        <div class="container mobile_cont">
            <!-- navbar element -->
            <?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="tab_content_sec border p-2 p-md-4">
                <!-- <h4 class="status_title ">Fixtures</h4> -->
                <div class="fixtures_data_block position-relative py-4 px-1 px-md-5">
                    <?php if(empty($result)): ?>
                    <p style=" margin-top: 20px; ">No fixtures have yet been added in the game</p>
                    <?php endif; ?>
                     <div class="fixture_table_slide_toparrow_sec">
                        <a href="javascript:void(0)" class="previous_owl"><img src="<?php echo e(asset('img/arrow_prev.svg')); ?>"></a>
                        <a class="ml-auto next_owl" href="javascript:void(0)"><img src="<?php echo e(asset('img/arrow_next.svg')); ?>"></a>
                     </div> 

                     
                    <!--  <div class="fixture_table_slide_bottomarrow_sec">
                        <a href="javascript:void(0)" class="previous_owl"><img src="<?php echo e(asset('img/arrow_prev.svg')); ?>"></a>
                        <a class="ml-auto next_owl" href="javascript:void(0)"><img src="<?php echo e(asset('img/arrow_next.svg')); ?>"></a>
                     </div> -->
                    <div class="fixtures_table_slide owl-carousel fixture_table_owl_carousel">

                        <?php   $m = 1;   $icnt = 0 ;   ?>
                        <?php if(!empty($result)): ?>
                        <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if(!empty($value)): ?>
                        <div class="item">
                            <h3 class="players_title d-flex align-items-center mb-3"> Gameweek #<?php echo e($value['gw']); ?>

                            </h3>
                            <div class="table-responsive table-mobile">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                    <tbody>
                                        <tr>
                                            <th>Date</th>
                                            <th>Team</th>
                                            <th>Grade</th>
                                            <th>Scorecard</th>
                                        </tr>
                                        <?php $__currentLoopData = $value['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php //prd($v); ?>
                                        <?php 
                                            // if(!empty($v->start_date) && Carbon::parse($v->start_date) <= Carbon::now()){
                                            //     $icnt++;  
                                            // }
                                            ?>
                                        <tr>
                                            <td><?php echo e(!empty($v->start_date) ? Carbon::parse($v->start_date)->format('d.m.y') : ''); ?></td>
                                            <td><?php echo e(ucwords($v->team_name)); ?></td>
                                            <td><?php echo e(!empty($v->grade_name) ? ucwords($v->grade_name) : '-'); ?></td>
                                            <!-- <td><?php echo e($v->fixture_scorecard->sum('fantasy_points')); ?></td>    -->
                                            <td>
                                                <!-- <a href="<?php echo e(route('Global.matchScorecard',$v->id)); ?>" class="more_link border ml-auto"  data-toggle="modal" data-target="#myModal<?php echo e($v->id); ?>">Scorecard</a> -->
                                                <a href="<?php echo e(route('Global.matchScorecard',$v->id)); ?>" class="more_link border ml-auto">Scorecard</a>
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php if(!empty($result)): ?>
            <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if(!empty($value)): ?>
            <?php $__currentLoopData = $value['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="modal fade" id="myModal<?php echo e($v->id); ?>">
                <div class="modal-dialog modal-xl" >
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Fixture Scorecards</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <table class="table table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>Player</th>
                                        <th>Rs</th>
                                        <th>4s</th>
                                        <th>6s</th>
                                        <th>Ovrs</th>
                                        <th>Mdns </th>
                                        <th>RG</th>
                                        <th>Wks</th>
                                        <th>cs</th>
                                        <th>CWks</th>
                                        <th>Sts</th>
                                        <th>RODs</th>
                                        <th>ROAs</th>
                                        <th>Dks</th>
                                        <th>HT</th>
                                        <th>FP</th>
                                    </tr>
                                </thead>
                                <tbody id="powerwidgets">
                                    <?php 
                                        // dump($v->fixture_scorecards);   die; 
                                        ?>
                                    <?php if(!empty($v->fixture_scorecards) && $v->fixture_scorecards->isNotEmpty()): ?>
                                    <?php $__currentLoopData = $v->fixture_scorecards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                            <?php echo e($record->player_name); ?>

                                            <?php 
                                                $positionName = "";
                                                if($record->player_position == 1){
                                                    $positionName = 'Batsman';
                                                }elseif($record->player_position == 2){
                                                    $positionName = 'Bowler';
                                                }elseif ($record->player_position == 3) {
                                                    $positionName = 'All Rounder';
                                                }elseif ($record->player_position == 4) {
                                                    $positionName = 'Wicket Keeper';
                                                } ?>
                                            <br>(<em><?php echo e($positionName); ?></em>)
                                        </td>
                                        <td><?php echo e($record->rs); ?></td>
                                        <td><?php echo e($record->fours); ?></td>
                                        <td><?php echo e($record->sixes); ?></td>
                                        <td><?php echo e($record->overs); ?></td>
                                        <td><?php echo e($record->mdns); ?></td>
                                        <td><?php echo e($record->run); ?></td>
                                        <td><?php echo e($record->wks); ?></td>
                                        <td><?php echo e($record->cs); ?></td>
                                        <td><?php echo e($record->cwks); ?></td>
                                        <td><?php echo e($record->sts); ?></td>
                                        <td><?php echo e($record->rods); ?></td>
                                        <td><?php echo e($record->roas); ?></td>
                                        <td><?php echo e($record->dks); ?></td>
                                        <td><?php echo e($record->hattrick); ?></td>
                                        <td><?php echo e($record->fantasy_points); ?></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <tr>
                                        <td colspan="16">No record is yet available.</td>
                                    </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
    </section>
    <div class="container">
    <div class="favorite_players_block mb-5">
    <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
    </div>
    </div>
    </div>
</div>
<script type="text/javascript">
    $('.previous_owl').on('click',function(){
        $('.owl-carousel').trigger('prev.owl.carousel', [300]);
    });
    $('.next_owl').on('click',function(){
        $('.owl-carousel').trigger('next.owl.carousel', [300]);
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/common/fixtures.blade.php ENDPATH**/ ?>
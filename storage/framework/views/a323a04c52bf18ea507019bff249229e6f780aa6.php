<?php $__env->startSection('content'); ?>
    <div class="body_section">
        <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <section class="status_tab_block mb-2">
            <div class="container mobile_cont">
                <?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="tab_content_sec border p-4">
<!--                     <h4 class="status_title">Sponsors</h4> -->
                     <?php if($result->isEmpty()): ?>
                        <p style="margin-top: 15px;">No sponsors have yet been added.</p>
                     <?php endif; ?>

                    <ul class="sponsors_list">

                        <?php if(!$result->isEmpty()): ?>
                            <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="d-block d-md-flex align-items-center p-3 p-md-4 mb-4">
                                <div class="sponsors_img">
                                    <?php if(File::exists(SPONSOR_IMAGE_ROOT_PATH.$value->logo)): ?>
                                    <a href="<?php echo e($value->website); ?>"> <img src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.SPONSOR_IMAGE_URL.'/'.$value->logo ?>" alt="Sponsors"></a>
                                    <?php endif; ?>
                                </div>
                                <div class="w-100 sponsors_info pl-0 pl-md-4 py-4 py-md-0">
                                    <div class="sponsors_list_heading mb-1"> <a href="<?php echo e($value->website); ?>"> <?php echo e($value->name); ?></a></div>
                                    <div class="mb-2"><?php echo e(!empty($value->about) ? $value->about :''); ?></div>
                                    <ul class="social_links d-flex align-items-center">
                                        <?php if($value->facebook): ?>
                                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" target="_blank" href="<?php echo e($value->facebook); ?>" title="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                        <?php endif; ?>
                                        <?php if($value->twitter): ?>
                                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" target="_blank" href="<?php echo e($value->twitter); ?>" title="twitter"><i class="fab fa-twitter"></i></a></li>
                                         <?php endif; ?>
                                        <?php if($value->instagram): ?>
                                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" target="_blank" href="<?php echo e($value->instagram); ?>" title="instagram"><i class="fab fa-instagram"></i></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <?php if($value->user_id ==1): ?>
                                <a href="<?php echo e($value->website); ?>" target="_blank" class="sponsors_btn ml-0 ml-md-4">
                                    <?php if(!empty($value->offer)): ?>
                                        <?php echo e($value->offer); ?>

                                    <?php else: ?>
                                        Offer 5% off
                                    <?php endif; ?>
                                </a>
                                <?php else: ?>
                                <a href="<?php echo e($value->website); ?>" target="_blank" class="sponsors_btn ml-0 ml-md-4">Visit</a>
                                <?php endif; ?>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php /*
                        @else
                             <li class="d-block d-md-flex align-items-center p-3 p-md-4 mb-4">
                                <div class="w-100 sponsors_info pl-0 pl-md-4 py-4 py-md-0">
                                    <div class="sponsors_list_heading mb-1">No sponsors have yet been added.</div>
                                </div>
                            </li>
                            */ ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </section>
        <div class="container pt-4">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
            </div>
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/common/game_sponsors.blade.php ENDPATH**/ ?>
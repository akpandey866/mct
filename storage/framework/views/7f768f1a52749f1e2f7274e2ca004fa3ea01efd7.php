<?php $__env->startSection('content'); ?>
<script>
    jQuery(document).ready(function(){
        $(".choosen_selct").chosen();
    });
</script>
<style>
.chosen-container-single .chosen-single{
    height:34px !important;
    padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Fixture")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Fixture")); ?></li>
	</ol>
</section>
<?php if(Auth::guard('admin')->user()->is_lockout == 1): ?>
<div class="alert alert-info alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<h4><i class="icon fa fa-info"></i> &nbsp;&nbsp;System is running in Locked Down Mode!</h4>
Day From: <b><?php echo e(ucfirst(Auth::guard('admin')->user()->lockout_start_day)); ?></b> - Start Time: <b><?php echo e(Auth::guard('admin')->user()->lockout_start_time); ?></b> <br>Day To: <b><?php echo e(ucfirst(Auth::guard('admin')->user()->lockout_end_day)); ?></b> - End Time: <b><?php echo e(Auth::guard('admin')->user()->lockout_end_time); ?></b>
</div>
<?php endif; ?>
<section class="content"> 
	
	<div class="row">
		<?php echo e(Form::open(['role' => 'form','url' => 'admin/fixture','class' => 'mws-form',"method"=>"get"])); ?>

		<?php echo e(Form::hidden('display')); ?>

			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<div class="form-group ">  
					<?php echo e(Form::select('game_mode',[null => 'Please Select Mode'] + Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''), ['class' => 'form-control game_mode choosen_selct'])); ?>

					</div>
				</div>
			</div>

			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<div class="form-group get_game_name">  
					<?php echo e(Form::select('game_name',array(''=>trans('Select Game')),((isset($searchVariable['game_name'])) ? $searchVariable['game_name'] : ''), ['class' => 'form-control choosen_selct game_name'])); ?>

					</div>
				</div>
			</div>
			<?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
			<div class="col-md-2 col-sm-2">
				<div class="form-group get_team ">  
					<?php echo e(Form::select(
						 'team',
						 [null => 'Please Select Team'] ,
						 (isset($searchVariable['team'])) ? $searchVariable['team'] : '',
						 ['id' => 'team','class'=>'form-control choosen_selct']
						)); ?>

				</div>
			</div>
			<?php else: ?>
			<div class="col-md-2 col-sm-2">
				<div class="form-group">  
					<?php echo e(Form::select(
						 'team',
						 [null => 'Please Select Team']+$teamList ,
						 (isset($searchVariable['team'])) ? $searchVariable['team'] : '',
						 ['id' => 'team','class'=>'form-control choosen_selct']
						)); ?>

				</div>
			</div>
			<?php endif; ?>

			<div class="col-md-2 col-sm-2">
				<div class="form-group put_grade_html ">  
					<?php echo e(Form::select(
						 'grade',
						 [null => 'Please Select Grade'] + $gradeList ,
						 (isset($searchVariable['grade'])) ? $searchVariable['grade'] : '',
						 ['id' => 'grade','class'=>'form-control choosen_selct']
						)); ?>

				</div>
			</div>


			<div class="col-md-2 col-sm-2">
				<div class="form-group put_grade_html ">  
					<?php echo e(Form::text('keyword', (isset($searchVariable['keyword'])) ? $searchVariable['keyword'] : '',  ['id' => 'keyword','class'=>'form-control', 'placeholder' => 'Enter Keyword'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select('is_active',array(''=>trans('Select Status'),0=>'Inactive',1=>'Active'),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control choosen_selct'])); ?>

				</div>
			</div>

			<div class="col-md-2 col-sm-2">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> <?php echo e(trans('Search')); ?></button>
				<a href="<?php echo e(URL::to('admin/fixture')); ?>"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> <?php echo e(trans("Reset")); ?></a>
			</div>
			<?php echo e(Form::close()); ?>

		<?php if(Auth::guard('admin')->user()->user_role_id != 4): ?>
		<div class="col-md-10 col-sm-2">
			<div class="form-group">  
				<a href="<?php echo e(URL::to('admin/fixture/add-fixture')); ?>" class="btn btn-success btn-small  pull-right" style="margin:0;"><?php echo e(trans("Add New Fixture")); ?> </a>
			</div>
		</div>
		<?php endif; ?>
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<!-- <th width="11%">
							<?php echo e(link_to_route(
									"fixture.index",
									trans("Match"),
									array(
										'sortBy' => 'match',
										'order' => ($sortBy == 'match' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'match' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'match' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> -->
						<!-- <th width="11%">
							<?php echo e(link_to_route(
									"fixture.index",
									trans("Club"),
									array(
										'sortBy' => 'club_name',
										'order' => ($sortBy == 'club_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'club_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'club_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> -->
						<!-- <th width="11%">
							<?php echo e(link_to_route(
									"fixture.index",
									trans("Opposition Club"),
									array(
										'sortBy' => 'opposition_club',
										'order' => ($sortBy == 'opposition_club' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'opposition_club' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'opposition_club' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> -->
						<th width="11%">
							<?php echo e(link_to_route(
									"fixture.index",
									trans("Team Name"),
									array(
										'sortBy' => 'team',
										'order' => ($sortBy == 'team' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'team' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'team' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"fixture.index",
									trans("Grade Name"),
									array(
										'sortBy' => 'fixtures.grade',
										'order' => ($sortBy == 'fixtures.grade' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'fixtures.grade' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'grade' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<a href="javascript:void(0);" class="sorting" > <?php echo e(trans("Match Type")); ?> </a>
	
						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"fixture.index",
									trans("Start Date"),
									array(
										'sortBy' => 'start_date',
										'order' => ($sortBy == 'start_date' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'start_date' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'start_date' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"fixture.index",
									trans("End Date"),
									array(
										'sortBy' => 'end_date',
										'order' => ($sortBy == 'end_date' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'end_date' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'end_date' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						
						<!-- <th width="11%">
							<?php echo e(link_to_route(
									"fixture.index",
									trans("Team Type"),
									array(
										'sortBy' => 'team_type',
										'order' => ($sortBy == 'team_type' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'team_type' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'team_type' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> -->
						<!-- <th width="11%">
							<?php echo e(link_to_route(
									"fixture.index",
									trans("Team Category"),
									array(
										'sortBy' => 'team_category',
										'order' => ($sortBy == 'team_category' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'team_category' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'team_category' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> -->
						<th width="11%">
							<?php echo e(link_to_route(
									"fixture.index",
									trans("Status"),
									array(
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							Action
						</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr class="items-inner">
							<!-- <td>
								<?php echo e($record->match_type); ?> 
							</td> -->
							<!-- <td>
								<?php echo e($record->club_name); ?> 
							</td> -->
							<!-- <td>
								<?php echo e($record->opposition_club); ?> 
							</td> -->
							<td>
								<?php echo e($record->team_name); ?>

							</td>
							<td>
								<?php echo e($record->grade); ?>

							</td>
							<td>
								<?php echo e($record->match_type); ?>

							</td>
							<td>
								<?php echo Date('d/m/Y',strtotime($record->start_date)) ?>
							</td>
							<td>
								<?php echo Date('d/m/Y',strtotime($record->end_date)) ?>
							</td>
							
						<!-- 	<td>
								<?php echo e($record->team_type); ?>

							</td>
							<td>
								<?php echo e($record->team_category); ?>

							</td> -->
							<td> 
								<?php $gameStatus = getGameStatus($record->id); ?>
								<?php if($gameStatus == 2): ?>
									<span class="label label-danger" ><?php echo e(trans("In-Progress")); ?></span>
								<?php elseif($gameStatus	== 3): ?>
									<span class="label label-success" ><?php echo e(trans("Completed")); ?></span>
								<?php elseif(empty($gameStatus)): ?>
									<span class="label label-warning" ><?php echo e(trans("Not yet started")); ?></span>
								<?php endif; ?>
							</td>
							<td data-th='Action'>
							<!-- 	<a title="<?php echo e(trans('Edit')); ?>" href="<?php echo e(URL::to('admin/fixture/edit-fixture/'.$record->id)); ?>" class="btn btn-primary">
												<i class="fa fa-pencil"></i>
											</a> -->
								<?php $getScorecardStatus = getScorecardStatus($record->id); ?>
								<?php if($getScorecardStatus !=3): ?>
									<?php if(Auth::guard('admin')->user()->user_role_id != 4): ?>
										<?php if(Auth::guard('admin')->user()->is_lockout == 0 || Auth::guard('admin')->user()->is_lockout == 2): ?>

											<a title="<?php echo e(trans('Edit')); ?>" href="<?php echo e(URL::to('admin/fixture/edit-fixture/'.$record->id)); ?>" class="btn btn-primary">
												<i class="fa fa-pencil"></i>
											</a>
											<?php if($record->is_active == 1): ?>
												<a  title="Click To Deactivate" href="<?php echo e(URL::to('admin/fixture/update-status/'.$record->id.'/0')); ?>" class="btn btn-success btn-small status_any_item"><span class="fa fa-check"></span>
												</a>
											<?php else: ?>
												<a title="Click To Activate" href="<?php echo e(URL::to('admin/fixture/update-status/'.$record->id.'/1')); ?>" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
												</a> 
											<?php endif; ?>
											<a title="<?php echo e(trans('Delete')); ?>" href="<?php echo e(URL::to('admin/fixture/delete-fixture/'.$record->id)); ?>"  class="delete_any_item btn btn-danger">
												<i class="fa fa-trash-o"></i>
											</a>
										<?php endif; ?>
										<a title="<?php echo e(trans('Squad')); ?>" href="<?php echo e(url('admin/team-player/'.$record->id)); ?>" class="btn btn-primary">Squad</a>
									<?php endif; ?>
									
									<a title="<?php echo e(trans('Scorecard')); ?>" href="<?php echo e(URL::to('admin/fixture/scorecards/'.$record->id)); ?>" class="btn btn-primary">
										Scorecard
									</a> 
								<?php else: ?>
									<a title="<?php echo e(trans('Scorecard')); ?>" href="<?php echo e(URL::to('admin/fixture/show-scorecard/'.$record->id)); ?>" class="btn btn-primary">Scorecard</a> 
									<a title="<?php echo e(trans('Squad')); ?>" href="<?php echo e(URL::to('admin/fixture/show-sqad/'.$record->id)); ?>" class="btn btn-primary">Squad</a> 
								<?php endif; ?>
								<a href="<?php echo e(URL::to('admin/fixture/view-fixture/'.$record->id)); ?>" title="<?php echo e(trans('View')); ?>" class="btn btn-info">
									<i class="fa fa-eye"></i>
								</a>
								
							</td>
						</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="9" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div> 
</section> 
<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Default Modal</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
var game_name = "<?php echo !empty($searchVariable['game_name']) ? $searchVariable['game_name'] :'' ?>";
var game_mode = "<?php echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode'] :'' ?>";
var team_id = "<?php echo !empty($searchVariable['team']) ? $searchVariable['team'] :'' ?>";
if(game_name !="" || game_mode !=""){
	get_game_name();

}
if(team_id !=""){
	get_team();

}
function get_team(){
	$.ajax({
	 	headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:' <?php echo e("getTeam"); ?> ',
		'type':'post',
		data:{'game_name':game_name,'team_id':team_id},
		success:function(response){ 
			$(".get_team").html(response);
		}
	});
}
function get_game_name(){
	$.ajax({
	 	headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:' <?php echo e("admin-get-game-name"); ?> ',
		'type':'post',
		data:{'mode':game_mode,'game_name':game_name},
		success:function(response){ 
			$(".get_game_name").html(response);
		}
	});
}
$(".game_mode").on('change',function(){
	var mode = $(this).val();
	$('#loader_img').show();
	 $.ajax({
	 	headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:' <?php echo e("admin-get-game-name"); ?> ',
		'type':'post',
		data:{'mode':mode,'game_name':game_name},
		success:function(response){ 
			$(".get_game_name").html(response);
		}
	}); 
	$('#loader_img').hide();
});
$(document).on('change',".game_name",function(){
	var game_name = $(this).val();
	$('#loader_img').show();
	 $.ajax({
	 	headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:' <?php echo e("getTeam"); ?> ',
		'type':'post',
		data:{'game_name':game_name,'team_id':team_id},
		success:function(response){ 
			$(".get_team").html(response);
		}
	}); 
	$('#loader_img').hide();
});

$(document).on('change',"#club",function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"<?php echo e(url('admin/get-club-mode-team')); ?>",
		'type':'post',
		data:{'id':selectValue},
		success:function(response){
			$('.put_team_html').html(response);
			$('#loader_img').hide();
		}
	});
});


$(document).on('change',"#club",function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"<?php echo e(url('admin/get-club-mode-grade')); ?>",
		'type':'post',
		data:{'id':selectValue},
		success:function(response){
			$('.put_grade_html').html(response);
			$('#loader_img').hide();
		}
	});
});


</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/fixture/index.blade.php ENDPATH**/ ?>
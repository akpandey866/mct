<?php $__env->startSection('content'); ?>
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-center" style="background-image: url(<?php echo e(asset('img/inner_banner.png')); ?>);">
        <div class="container py-3 py-lg-4">
            <h2 class="block_title"><small>Why</small>Choose Us?</h2>
        </div>

    </section>
    <section class="chooseus_block pt-0 pt-lg-5 pb-5">
        <div class="container">
           <!--  <p>Experienced playing fantasy cricket based on international cricket superstars? How about replacing them with yourself and your own local cricket superstars!</p> -->
            <p>Whether you are a cricket club with senior and junior teams, a local cricket league, a cricket association, an independent or a corporate cricket competition, with MyClubtap, it is now affordable and a breeze to set-up, manage and have your own personalized fantasy cricket game featuring players of your own cricket club or leagues!</p>
            <p>Sign up, set up your players, update your match scores as you go along and experience the fun of playing your own Senior Club, Junior Club or League based fantasy game.</p>
            <p><b>With a senior fantasy game starting at just $19, check out <a style="color: #0f70b7;" href="<?php echo e(url('pricing')); ?>">pricing</a> for more on Senior and League modes!</b></p>
             <!-- <ul class="main_point">
                 <li class="pb-2">Junior club fantasy games at $49 per season only!</li>
                 <li class="pb-2">Senior club fantasy games starting at $104* per season only!</li>
                 <li class="pb-2">League based fantasy games at $249 per season only!</li>
             </ul> -->
        </div>
    </section>
    <section class="platform_block py-5" style="background-image: url(img/platform_sec_bg.jpg);">
        <div class="container">
             <h2 class="block_title"><small>What you get as a</small>Club or League?</h2>
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4 my-3">
                    <div class="register_list_sec d-flex align-items-center">
                        <div class="w-100 text-center position-relative p-4">
                            <div class="register_list_icon d-inline-block mb-4" style="background-image: url(<?php echo e(asset('img/icon_platform1.png')); ?>)">&nbsp;</div>
                            <div class="register_list_heading text-uppercase mb-3">Your Fantasy Game </div>
                            <div class="register_list_text">Within minutes you can now have a fantasy game based on your own players, with the ability to create separate fantasy games for your senior and junior players.
</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 my-3">
                    <div class="register_list_sec d-flex align-items-center">
                        <div class="w-100 text-center position-relative p-4">
                            <div class="register_list_icon d-inline-block mb-4" style="background-image: url(img/icon_platform2.png);">&nbsp;</div>
                            <div class="register_list_heading text-uppercase mb-3">MyClubtap Admin Portal</div>
                            <div class="register_list_text">Our mission is to ensure you have to put in minimal effort in managing your fantasy game, hence, we have made your admin portal such that it's easy to understand and use, plus allow you to create shared access to reduce individual work.
</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 my-3">
                    <div class="register_list_sec d-flex align-items-center">
                        <div class="w-100 text-center position-relative p-4">
                            <div class="register_list_icon d-inline-block mb-4" style="background-image: url(img/icon_platform3.png);">&nbsp;</div>
                            <div class="register_list_heading text-uppercase mb-3">5 Power Boosters </div>
                            <div class="register_list_text">What would fantasy cricket be without the ability to wield some super powers! All users now have 5 special powers to boost the performance of their fantasy teams.
</div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4 my-3">
                    <div class="register_list_sec d-flex align-items-center">
                        <div class="w-100 text-center position-relative p-4">
                            <div class="register_list_icon d-inline-block mb-4" style="background-image: url(<?php echo e(asset('img/icon_platform1.png')); ?>)">&nbsp;</div>
                            <div class="register_list_heading text-uppercase mb-3">Fundraiser for Clubs </div>
                            <div class="register_list_text"> Raise extra $$'s for your club, with our Fundraiser add-on (in Senior mode only), set a fundraising amount and link payments direct to your club accounts (100% fundraising, minus payment gateway fee, with 0% fee to MyClubtap)!
</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 my-3">
                    <div class="register_list_sec d-flex align-items-center">
                        <div class="w-100 text-center position-relative p-4">
                            <div class="register_list_icon d-inline-block mb-4" style="background-image: url(img/icon_platform2.png);">&nbsp;</div>
                            <div class="register_list_heading text-uppercase mb-3">Admin Add-Ons </div>
                            <div class="register_list_text">Scorer, Verify, Availability, Sponsor, Fundraiser and Branding add-ons help your game admin to smoothly manage and power up your fantasy game.
</div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 my-3">
                    <div class="register_list_sec d-flex align-items-center">
                        <div class="w-100 text-center position-relative p-4">
                            <div class="register_list_icon d-inline-block mb-4" style="background-image: url(img/icon_platform3.png);">&nbsp;</div>
                            <div class="register_list_heading text-uppercase mb-3">Banter &amp; Engagement </div>
                            <div class="register_list_text">Connect and engage those family members, friends and followers who have always wanted to follow your club? Imagine the banter on and off the field, with members desperate to follow how the players they picked are doing!
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="say_about_block py-5" style="background-color: #0f70b7; color: #fff;">
        <div class="container">
            <h2>“MyClubtap is designed to empower grassroots cricket clubs and leagues across the world to have and enjoy fantasy cricket, where their own players are the real superstars. It’s about them, it’s about you!”</h2>
        </div>
    </section>
    <section class="chooseus_block py-5">
        <div class="container">
            <h2 class="block_title mb-3"><small>What you get </small>As a User</h2>
            <div class="row">
                 <ul class="main_point">
                     <li class="pb-2">Pick your own fantasy team and join any fantasy game on the platform.</li>
                     <li class="pb-2">Pick a captain and vice-captain for your fantasy team.</li>
                     <li class="pb-2">Collect fantasy points as players perform on-field in real-matches.</li>
                     <li class="pb-2">Trade players in and out and use power boosters as available in the fantasy game.</li>
                     <li class="pb-2">Set and follow your favourite players.</li>
                     <li class="pb-2">Access your detailed trade and gameweek history.</li>
                 </ul>
             </div>
        </div>
    </section>

    <!-- <section class="partners_block py-5" style="background-color: #f1f1f1;">
        <div class="container">
            <h2 class="block_title mb-4"></h2>
            <h2 class="block_title"><small>Fantasy Cricket</small>On MyClubtap</h2>

            <p>Pick players of your own club or league in your fantasy teams, collect fantasy points as they perform on-field in real-matches, trades players in and out of your team, use your power boosters to boost up the performance of your fantasy team and see if you have what it takes to be the next fantasy champion of your club or league.</p>

            <p>With all users able to sign-up, pick and enter their fantasy teams for free, clubs with Senior mode fantasy games can activate fundraising power boosters by setting a small entry fee, which allows your club to fundraise through the game, giving users access to 5 special power boosters to use on his fantasy team, a win-win for both!Remember 99% of this goes directly to your club account* giving a much needed cash boost to your club’s fundraising efforts.</p>

            <p>Your game admin can easily edit and manage all aspects of your fantasy game, like adding player names, setting up the point system, activate branding for fundraising, set up club profile, advertise your own sponsors and much more. Plus, MyClubtap exclusively empowers you to share your admin work by creating multiple scorers, who can assist in your score entries throughout the season!</p>
            <div class="highlight_text">
                <p class="m-0">MyClubtap is designed to empower clubs and leagues across the world to have and enjoy fantasy cricket, where they are the real superstars. It’s about you!</p>
            </div>
        </div>
    </section> -->

    <section class="partners_block py-5" style="background-color: #f1f1f1;">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5">
                    <!-- <h2>Platform Features</h2> -->
                    <h2 class="block_title"><small>Platform </small>Features</h2>
                    <div class="faq_collapse" id="accordion">
                        <?php if(!$data->isEmpty()): ?>
                            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="card border-top">
                                <div class="card-header">
                                  <a class="collapse_point" <?php if($key==0){ ?>aria-expanded="true" <?php } ?> data-toggle="collapse" href="#collapse<?php echo e($key); ?>">
                                    <?php echo $key+1 ?>.&nbsp;<?php echo e($values->text1); ?>

                                  </a>
                                </div>
                                <div id="collapse<?php echo e($key); ?>" class="collapse <?php echo e(($key==0)?'show':''); ?> " data-parent="#accordion">
                                  <div class="card-body">
                                    <?php echo e($values->text2); ?>

                                  </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>   
                    </div>
                </div>
                <div class="col-12 col-md-6 offset-md-1 pt-5 pt-md-0">
                    <div class="say_about_sec fantasy_game_sec  mt-5">
                        <div class="say_about_client_slider owl-carousel">
                            <?php $clubSlider = get_slider('platfrom-feature');?>
                            <?php if(!$clubSlider->isEmpty()): ?>
                            <?php $__currentLoopData = $clubSlider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sliders): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item">
                                <div class=" mx-auto mb-4"><img src="<?php echo e(SLIDER_IMAGE_URL.$sliders->image); ?>" alt="client"></div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
    
    <section class="say_about_block py-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 col-xl-5">
                    <h2 class="block_title"><small>What Our Users</small>Say About Us</h2>
                </div>
                <div class="col-12 col-lg-6 col-xl-7">
                    <div class="say_about_sec p-5 mt-5 mt-lg-0">
                        <div class="say_about_client_slider owl-carousel">
                            <?php if(!$userSliders->isEmpty()): ?>
                            <?php $__currentLoopData = $userSliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item">
                                <div class="say_about_client mx-auto mb-4"><img src="<?php echo e(USER_SLIDER_IMAGE_URL.'/'.$values->image); ?>" alt="client"></div>
                                <div class="say_about_client_review text-center mb-4"><?php echo !empty($values->description) ? $values->description :''; ?></div>
                                <div class="say_about_client_name text-center"><?php echo e($values->name); ?> <span><?php echo e($values->club_name); ?></span></div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php echo $__env->make('front.elements.subscribe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<?php echo $__env->make('front.elements.subscribe_pop_up', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript">
    function contact_us() { 
        $('#loader_img').show();
        $('.help-inline').html('');
        $('.help-inline').removeClass('error');
        $.ajax({ 
            url: '<?php echo e(URL("subscribe")); ?>',
            type:'post',
            data: $('#contact_form').serialize(),
            success: function(r){
                error_array     =   JSON.stringify(r);
                data            =   JSON.parse(error_array);
                if(data['success'] == 1) {
                    window.location.href     =  "<?php echo e(URL('/')); ?>";
                }else { 
                    $.each(data['errors'],function(index,html){
                        $("#"+index).next().addClass('error');
                        $("#"+index).next().html(html);
                    });
                    $('#loader_img').hide();
                }
                $('#loader_img').hide();
            }
        });
    }
    $('#contact_form').each(function() {
        $(this).find('input').keypress(function(e) {
          if(e.which == 10 || e.which == 13) {
            contact_us();
            return false;
           }
       });
    });
$('.partners_sliders').owlCarousel({
    //rtl:false,            
    loop: true,
    margin: 0,
    nav: true,
    //navText : ["",""],
    dots: false,
    autoplay: false,
    responsive: {
        320: {
            items: 1
        },
        480: {
            items: 2
        },
        576: {
            items: 3
        },
        768: {
            items: 3
        },
        992: {
            items: 4
        },
        1200: {
            items: 4
        }
    }
});
//say_about_client_slider
$('.say_about_client_slider').owlCarousel({
    //rtl:false,            
    loop: true,
    margin: 0,
    nav: true,
    //navText : ["",""],
    dots: false,
    autoplay: true,
    items: 1,
});
//our_team_slider
$('.our_team_slider').owlCarousel({
    //rtl:false,            
    loop: true,
    margin: 0,
    nav: true,
    //navText : ["",""],
    dots: false,
    autoplay: false,
    responsive: {
        320: {
            items: 1
        },
        480: {
            items: 2
        },
        576: {
            items: 2
        },
        768: {
            items: 3
        },
        992: {
            items: 4
        },
        1200: {
            items: 4
        }
    }
});

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/common/about_club.blade.php ENDPATH**/ ?>
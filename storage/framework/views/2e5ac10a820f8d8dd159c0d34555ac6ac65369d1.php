<?php $__env->startSection('content'); ?>
<?php echo e(Html::script('js/admin/ckeditor/ckeditor.js')); ?>

<section class="content-header">
	<h1>Add New Notification</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route('notification.listing')); ?>">Notification</a></li>
		<li class="active">Add New Notification</li>
	</ol>
</section>

<section class="content"> 
	<?php echo e(Form::open(['role' => 'form','route' => 'notification.addNotificaiton','class' => 'mws-form','files' => true])); ?>	
	<div class="row pad">
		<div class="col-md-6">	
			<div class="form-group <?php echo ($errors->first('body')) ? 'has-error' : ''; ?>">
					<?php echo e(Form::label('body',trans("Body").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::textarea('body','',['clas'=>'form-control','id' => 'body'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('body'); ?>
					</div>
				</div>
			</div>
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				<a href="<?php echo e(route('notification.addNotificaiton')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans('Reset')); ?></a>
				<a href="<?php echo e(route('notification.addNotificaiton')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
			</div>
		</div>
	</div>
	<?php echo e(Form::close()); ?> 
</section>
<script type="text/javascript">
/* For CKEDITOR */
	CKEDITOR.replace( 'body',
	{
		height: 350,
		width: 507,
		filebrowserUploadUrl : '<?php echo URL::to('admin/base/uploder'); ?>',
		filebrowserImageWindowWidth : '640',
		filebrowserImageWindowHeight : '480',
		enterMode : CKEDITOR.ENTER_BR
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/notifications/add.blade.php ENDPATH**/ ?>
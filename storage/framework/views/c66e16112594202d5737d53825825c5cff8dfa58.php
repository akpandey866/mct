<?php $__env->startSection('content'); ?> 
<section class="content-header">
	<h1>
		<?php echo e(trans("Add New Sponsor")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/sponsor')); ?>"><?php echo e(trans("Sponsor")); ?></a></li>
		<li class="active"><?php echo e(trans("Add New Sponsor")); ?> </li>
	</ol>
</section>
<section class="content"> 
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/sponsor/add-sponsor','class' => 'mws-form','files'=>'true'])); ?>

	<div class="row">
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('logo')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Sponsor logo *</label>
				<div class="mws-form-item">
					<?php echo e(Form::file('logo', array('accept' => 'image/*'))); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('logo'); ?>
					</div>
				</div>
			</div>	
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('name', trans("Name").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text('name','', ['class' => 'form-control ','id'=>'name'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('website')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Website</label>
				<div class="mws-form-item">
					<?php echo e(Form::text('website','', ['class' => 'form-control ','id'=>'website'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('website'); ?>
					</div>
				</div>
			</div>	
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('facebook')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('facebook', trans("Facebook URL"), ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text('facebook','', ['class' => 'form-control ','id'=>'facebook'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('facebook'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('twitter')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Twitter URL</label>
				<div class="mws-form-item">
					<?php echo e(Form::text('twitter','', ['class' => 'form-control ','id'=>'twitter'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('twitter'); ?>
					</div>
				</div>
			</div>	
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('instagram')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('instagram', trans("Instagram URL"), ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text('instagram','', ['class' => 'form-control ','id'=>'instagram'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('instagram'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<?php if(Auth::guard('admin')->user()->user_role_id == ADMIN_ID): ?>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('offer')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('offer', trans("Offer"), ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text('offer','', ['class' => 'form-control ','id'=>'offer'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('offer'); ?>
					</div>
				</div>
			</div>
		</div>
        <?php endif; ?>
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('about')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">About</label>
				<div class="mws-form-item">
					<?php echo e(Form::textarea('about','', ['class' => 'form-control ','id'=>'about','rows'=>2])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('about'); ?>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<!-- <div class="row">
		<div class="col-md-6">
			<div class="form-group <?php //echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('club_type', trans("Club Type"), ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::select(
						 'club_type',
						 [null => 'Please Select Club'] +$clubLists,
						 '',
						 ['id' => 'club_type','class'=>'form-control']
						)); ?>

					<div class="error-message help-inline">
						<?php //echo $errors->first('club_type'); ?>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
			<a href="<?php echo e(URL::to('admin/sponsor/add-sponsor/')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans("Reset")); ?></a>
			<a href="<?php echo e(URL::to('admin/sponsor/')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans("Cancel")); ?></a>
		</div>
	</div>
	<?php echo e(Form::close()); ?>

	
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/sponsor/add.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<?php echo e(Html::style('css/admin/dashboard.css')); ?>

<?php echo e(Html::style('css/admin/blogcomment.css')); ?>

<div class="row pad" >
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
			<span class="info-box-icon bg-orange"><i class="ion-person"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">Game User</span>
			  <span class="info-box-number"> <?php echo $totalClubUser; ?> </span>
				<a class="small-box-footer" href="<?php echo e(URL('admin/users/'.CLUBUSER)); ?>" >
					More Info   
				</a>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion-person"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">User</span>
			  <span class="info-box-number"> <?php echo $totalFrontUser; ?> </span>
				<a class="small-box-footer" href="<?php echo e(URL('admin/users/'.USER)); ?>" >
					More Info   
				</a>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
			<span class="info-box-icon bg-blue"><i class="ion-shuffle"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">Total Activated Game</span> 
			  <span class="info-box-number"> <?php echo $totalActivatedGame; ?> </span>
				<a class="small-box-footer" href="<?php echo e(URL('admin/users/'.CLUBUSER.'is_game_activate=1')); ?>" >
					More Info   
				</a>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
			<span class="info-box-icon bg-red"><i class="ion-shuffle"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">Total Inactive Game</span>
			  <span class="info-box-number"><?php echo $totalInActivatedGame; ?> </span>
				<a class="small-box-footer" href="<?php echo e(URL('admin/users/'.CLUBUSER.'?is_game_activate=0')); ?>" >
					More Info   
				</a>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-green"><i class="fa fa-money "></i></span>

		<div class="info-box-content">
		  <span class="info-box-text">Game Income</span>
		  <span class="info-box-number"> $<?php echo $clubIncome; ?> </span>
			<a class="small-box-footer" href="javascript:void(0);" >
				More Info   
			</a>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-purple"><i class="fa fa-money "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Players Income</span>
		  <span class="info-box-number"> $<?php echo $playersIncome; ?> </span>
			<a class="small-box-footer" href="javascript:void(0);" >
				More Info   
			</a>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-purple"><i class="fa fa-money "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Grades</span>
		  <span class="info-box-number"> $<?php echo $totalGrades; ?> </span>
			<a class="small-box-footer" href="javascript:void(0);" >
				More Info   
			</a>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-purple"><i class="fa fa-money "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Teams</span>
		  <span class="info-box-number"><?php echo $totalTeams; ?> </span>
			<a class="small-box-footer" href="javascript:void(0);" >
				More Info   
			</a>
		</div>
	  </div>
	</div>
</div>
<div class="row pad">
	<!-- Product List -->
	<div class="col-lg-6 col-xs-12 col-sm-12">
		<div class="portlet light portlet-fit bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-microphone font-dark hide"></i>
					<span class="caption-subject bold font-dark uppercase"> Recent Games</span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn green-haze btn-outline btn-circle btn-sm" href="<?php echo e(URL::to('admin/club')); ?>"> View All
						</a>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row pad">
					<?php if(!empty($recentGame)): ?>
						<?php $__currentLoopData = $recentGame; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="col-md-4">
								<div class="mt-widget-4">
									<div class="mt-img-container">
										<?php if($record->club_logo != '' && File::exists(CLUB_IMAGE_ROOT_PATH.$record->club_logo)): ?>
											<img class="img-circle" src="<?php echo WEBSITE_URL.'image.php?width=183px&height=250px&cropratio=183:250&image='.CLUB_IMAGE_URL.'/'.$record->club_logo ?>">
										<?php else: ?> 
											<img class="img-circle" src="<?php echo WEBSITE_URL.'image.php?width=183px&height=250px&cropratio=183:250&image='.asset('img/admin/no_image.jpg') ?>">
									<?php endif; ?>
										 
									</div>
									<?php if($key	==	0): ?>
										<div class="mt-container bg-purple-opacity">
											<div class="mt-head-title"> <?php echo e($record->game_name); ?> </div>
											<div class="mt-body-icons">
												<a href="<?php echo e(URL::to('admin/club/edit-club/'.$record->id)); ?>">
													<i class=" fa fa-pencil"></i>
												</a>
												<a href="<?php echo e(URL::to('admin/club/view-club/'.$record->id)); ?>">
													<i class=" fa fa-eye"></i>
												</a>
												
											</div>
											<div class="mt-footer-button">
												<button type="button" class="btn btn-circle btn-danger btn-sm"><?php echo e($record->club_name); ?></button>
											</div>
										</div>
									<?php elseif($key	==	1): ?>
										<div class="mt-container bg-green-opacity">
											<div class="mt-head-title"> <?php echo e($record->game_name); ?> </div>
											<div class="mt-body-icons">
												<a href="<?php echo e(URL::to('admin/club/edit-club/'.$record->id)); ?>">
													<i class=" fa fa-pencil"></i>
												</a>
												<a href="<?php echo e(URL::to('admin/club/view-club/'.$record->id)); ?>">
													<i class=" fa fa-eye"></i>
												</a>
											</div>
											<div class="mt-footer-button">
												<button type="button" class="btn btn-circle blue-ebonyclay btn-sm"><?php echo e($record->club_name); ?></button>
											</div>
										</div>
									<?php else: ?>
										<div class="mt-container bg-dark-opacity">
											<div class="mt-head-title"> <?php echo e($record->game_name); ?> </div>
											<div class="mt-body-icons">
												<a href="<?php echo e(URL::to('admin/club/edit-club/'.$record->id)); ?>">
													<i class=" fa fa-pencil"></i>
												</a>
												<a href="<?php echo e(URL::to('admin/club/view-club/'.$record->id)); ?>">
													<i class=" fa fa-eye"></i>
												</a>
											</div>
											<div class="mt-footer-button">
												<button type="button" class="btn btn-circle btn-success btn-sm"><?php echo e($record->club_name); ?></button>
											</div>
										</div>
									<?php endif; ?>
									
								</div>
							</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
						<center><label>No Record found</label></center>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row pad">
	<!-- club and front users tables -->
	<div class="col-lg-6 col-xs-12 col-sm-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-bubble font-dark hide"></i>
					<span class="caption-subject font-hide bold uppercase">Recent Game Users</span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn green-haze btn-outline btn-circle btn-sm" href=" <?php echo e(URL::to('admin/users/'.CLUBUSER)); ?> "> View All
						</a>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row pad">
					<?php if(isset($clubUser) && !empty($clubUser)): ?>
						<?php $__currentLoopData = $clubUser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="col-md-4">
									<!--begin: widget 1-1 -->
									<div class="mt-widget-1 dash_user">
										<div class="mt-icon">
											<a href="#">
												<i class="icon-plus"></i>
											</a>
										</div>
										<div class="mt-img">
											<?php if($record->image != '' && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.$record->image)): ?>
												<img class="" src="<?php echo WEBSITE_URL.'image.php?width=80px&height=80px&cropratio=1:1&image='.USER_PROFILE_IMAGE_URL.'/'.$record->image ?>">
											<?php else: ?>
												<img width="80px" height="80px" src="<?php echo WEBSITE_IMG_URL ?>admin/no_image.jpg">
											<?php endif; ?>
										</div>
										<div class="mt-body">
											<h3 class="mt-username"> <a href=" <?php echo e(URL::to('admin/users/edit-user/'.$record->user_role_id.'/'.$record->id)); ?> " ><?php echo e($record->full_name); ?></a> </h3>
											<p class="mt-user-title"> <?php echo e($record->created_at); ?> </p>
										</div>
									</div>
									<!--end: widget 1-1 -->
								</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
						<center><label>No Record found</label></center>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<!-- Front Users -->
	<div class="col-lg-6 col-xs-12 col-sm-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-bubble font-dark hide"></i>
					<span class="caption-subject font-hide bold uppercase">Recent Users</span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn green-haze btn-outline btn-circle btn-sm" href=" <?php echo e(URL::to('admin/users/'.USER)); ?> "> View All
						</a>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row pad">
					<?php if(isset($frontUser) && !empty($frontUser)): ?>
						<?php $__currentLoopData = $frontUser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="col-md-4">
									<!--begin: widget 1-1 -->
									<div class="mt-widget-1 dash_user">
										<div class="mt-icon">
											<a href="#">
												<i class="icon-plus"></i>
											</a>
										</div>
										<div class="mt-img">
											<?php if($record->image != '' && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.$record->image)): ?>
												<img class="" src="<?php echo WEBSITE_URL.'image.php?width=80px&height=80px&cropratio=1:1&image='.USER_PROFILE_IMAGE_URL.'/'.$record->image ?>">
											<?php else: ?>
												<img width="80px" height="80px" src="<?php echo WEBSITE_IMG_URL ?>admin/no_image.jpg">
											<?php endif; ?>
										</div>
										<div class="mt-body">
											<h3 class="mt-username"> <a href=" <?php echo e(URL::to('admin/users/edit-user/'.$record->user_role_id.'/'.$record->id)); ?> " ><?php echo e($record->full_name); ?></a> </h3>
											<p class="mt-user-title"> <?php echo e($record->created_at); ?> </p>
										</div>
									</div>
									<!--end: widget 1-1 -->
								</div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php else: ?>
						<center><label>No Record found</label></center>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- club and front users tables -->
<div class="row pad">
	<!-- Fitness Enthusiast Users Graph-->
	<div class="col-md-12 col-sm-12">
		<div class="box box-info">
			<div id="info1"></div>
			<div class="portlet-title">	
				<div class="caption">
					<span class="caption-subject font-hide bold uppercase">Game Users</span>
					<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse" type="button">
						<i class="fa fa-minus"></i>
					</button>
					<button class="btn btn-box-tool" data-widget="remove" type="button">
						<i class="fa fa-times"></i>
					</button>
					</div>
				</div>
				<?php /**/?>
			</div>
			<div class="box-body" style="display: block;padding:15px;">  
				<div id="newBarChart">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row pad">
	<div class="col-md-12 col-sm-12">	
		<div class="box box-info">
			<div id="info1"></div>
			<div class="portlet-title">	
				<div class="caption">
					<span class="caption-subject font-hide bold uppercase">Users</span>
					<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse" type="button">
						<i class="fa fa-minus"></i>
					</button>
					<button class="btn btn-box-tool" data-widget="remove" type="button">
						<i class="fa fa-times"></i>
					</button>
					</div>
				</div>
			</div>
			<div class="box-body" style="display: block;padding:15px;">  
				<div id="newBarChart1">
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo e(Html::script('js/admin/d3/d3.v3.min.js')); ?>

<?php echo e(Html::script('js/admin/d3/d3.tip.v0.6.3.js')); ?>

<?php echo e(Html::style('css/admin/d3/d3.css')); ?>

<?php echo e(Html::style('css/admin/dashboard.css')); ?>

<script type="text/javascript">
//Graph For Fitness Enthusiast User
//----------------------------------------------------------------
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 1050 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

var formatPercent = d3.format("1.0");

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .55);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
	.orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
	.tickFormat(d3.format("d"));
    //.tickFormat(formatPercent);

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>Total Game Users:</strong> <span style='color:red'>" + d.frequency + "</span>";
  });

 var tip1 = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>Total Users:</strong> <span style='color:red'>" + d.frequency + "</span>";
  })

var svg = d3.select("#newBarChart").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.call(tip);

// The new data variable.
var data = [
  <?php foreach($allClubUsers as $user){?>
  {letter: "<?php echo  $user['month']; ?>", frequency: <?php echo  $user['users']; ?>},
  <?php }  ?>
];

// The following code was contained in the callback function.
x.domain(data.map(function(d) { return d.letter; }));
y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
  .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("Game Users");

svg.selectAll(".bar")
    .data(data)
  .enter().append("rect")
    .attr("class", "bar")
    .attr("x", function(d) { return x(d.letter); })
    .attr("width", x.rangeBand())
    .attr("y", function(d) { return y(d.frequency); })
    .attr("height", function(d) { return height - y(d.frequency); })
    .on('mouseover', tip.show)
    .on('mouseout', tip.hide)

/*Front Users Graph*/
	// Graph For Front User
//----------------------------------------------------------------
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 1050 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

var formatPercent = d3.format("1.0");

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .55);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
	.orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
	.tickFormat(d3.format("d"));
    //.tickFormat(formatPercent);

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>Total Users:</strong> <span style='color:red'>" + d.frequency + "</span>";
  })

var svg = d3.select("#newBarChart1").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.call(tip);

// The new data variable.
var data = [
  <?php foreach($frontUsers as $user){ ?>
  {letter: "<?php echo  $user['month']; ?>", frequency: <?php echo  $user['users']; ?>},
  <?php } ?>
];

// The following code was contained in the callback function.
x.domain(data.map(function(d) { return d.letter; }));
y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
  .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("Users");

svg.selectAll(".bar")
    .data(data)
  .enter().append("rect")
    .attr("class", "bar")
    .attr("x", function(d) { return x(d.letter); })
    .attr("width", x.rangeBand())
    .attr("y", function(d) { return y(d.frequency); })
    .attr("height", function(d) { return height - y(d.frequency); })
    .on('mouseover', tip.show)
    .on('mouseout', tip.hide)

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/dashboard/dashboard.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<?php echo e(Html::script('js/admin/ckeditor/ckeditor.js')); ?>

<style type="text/css">
	.hideCheckbox{
		display: none;
	}
	.chosen-choices .search-field input{ height: 30px !important; }
	.btn.btn-success{ margin-top: 0px;  }
</style>

<section class="content-header">
	<h1>Send New Email</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="<?php echo e(route('notification.listing')); ?>">Notification</a></li>
		<li class="active">Send New Email</li>
	</ol>
</section>

<section class="content"> 
	<?php echo e(Form::open(['role' => 'form','route' => 'notification.addNotificaiton','class' => 'mws-form','files' => true])); ?>	
	<div class="row box">
		<div class="col-md-12 box-footer">
				<?php echo e(Form::label('body',trans("Body"), ['class' => 'mws-form-label'])); ?><br>
			<?php echo $notificationsDescription; ?>

			
		</div>
	</div>
	<div class="row pad">
		<div class="col-md-6" style="margin-bottom: 100px;">
			<span class="custom_check"><label for="btnCheck1">Send single user</label> &nbsp; <input type="checkbox" name="file_option" id="btnCheck1" value="1" /><span class="check_indicator">&nbsp;</span></span>
			<div class="form-group ">  
				<select name="player_id[]" class="form-control chosen_select"  data-placeholder="Choose User..." multiple class="chosen-select">
					<!-- <option value="" selected=""  >Please Select Player</option> -->
					<?php foreach ($userList as $key => $value) {  ?>
					 <option value="<?php echo e($key); ?>"><?php echo e($value); ?></option>
					<?php } ?>
				</select>
				<div class="error-message help-inline">
					<?php echo $errors->first('player_id'); ?>
				</div>
				
			</div>
			
		</div>
		<div class="col-md-6">
			<span class="custom_check"><label for="btnCheck2">Send all game admin</label> &nbsp; <input type="checkbox" name="file_option" id="btnCheck2" value="2" /><span class="check_indicator">&nbsp;</span></span>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">	
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				<a href="<?php echo e(route('notification.addNotificaiton')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans('Reset')); ?></a>
				<a href="<?php echo e(route('notification.addNotificaiton')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
			</div>
		</div>
	</div>
	<?php echo e(Form::close()); ?> 
</section>
<script type="text/javascript">
	 $('#btnCheck1').on('click',function(){
        if($(this). prop("checked") == true){
            $("#btnCheck2"). prop("checked", false);
            //$(".uploadDiv").removeClass("hideCheckbox");
            //$(".imageDiv").addClass("hideCheckbox");
        }
    });
    $('#btnCheck2').on('click',function(){
        if($(this). prop("checked") == true){
            $("#btnCheck1"). prop("checked", false);
           // $(".imageDiv").removeClass("hideCheckbox");
           // $(".uploadDiv").addClass("hideCheckbox");
        }
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/notifications/send_emails.blade.php ENDPATH**/ ?>
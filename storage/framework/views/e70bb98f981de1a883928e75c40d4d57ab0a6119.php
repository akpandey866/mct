<?php $__env->startSection('content'); ?>


<script type="text/javascript">
var action_url = '<?php echo WEBSITE_URL; ?>admin/news-letter/delete-multiple-subscriber';
 /* for equal height of the div */	
</script>

<?php echo e(Html::script('js/admin/multiple_delete.js')); ?>


<section class="content-header">
	<h1>
		Newsletter Subscribers
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/news-letter/newsletter-templates')); ?>">Newsletter Templates</a></li>
		<li class="active">Newsletter Subscribers</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<?php echo e(Form::open(['method' => 'get','role' => 'form','url' => 'admin/news-letter/subscriber-list','class' => 'mws-form'])); ?>

		<?php echo e(Form::hidden('display')); ?>

		<div class="col-md-3 col-sm-3">
			<div class="form-group ">  
				<?php echo e(Form::text('email',((isset($searchVariable['email'])) ? $searchVariable['email'] : ''), ['class' => 'form-control','placeholder'=>'Email'])); ?>

			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
			<a href="<?php echo e(URL::to('admin/news-letter/subscriber-list')); ?>"  class="btn btn-primary"><i class='fa fa-refresh '></i>Reset</a>
		</div>
		<?php echo e(Form::close()); ?>

		<div class="pull-right form-group col-md-2 col-sm-2">
			<a href="<?php echo e(URL::to('admin/news-letter/add-subscriber')); ?>"  style="margin-left: 47px !important;" class="btn btn-success btn-small align">Add Subscriber </a>
		</div>
		</div>
		<div class="box">
			<div class="box-body ">
				<table class="table table-hover">
					<thead>
					<tr>
						<th width="40%">
							<?php echo e(link_to_route(
								'Subscriber.subscriberList',
								trans("Email"),
								array(
									'sortBy' => 'email',
									'order' => ($sortBy == 'email' && $order == 'desc') ? 'asc' : 'desc'
								),
								array('class' => (($sortBy == 'email' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'email' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
								'Subscriber.subscriberList',
								trans("Created"),
								array(
									'sortBy' => 'created_at',
									'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc'
								),
								array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody  id="powerwidgets">
					<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr class="items-inner">
						
						<td data-th='Email'><?php echo e($record->email); ?></td>
						<td data-th='Created'><?php echo e(date(Config::get("Reading.date_format"),strtotime($record->created_at))); ?></td>
						<td data-th='Action'>
							<?php if($record->status==1): ?>
								<a title="Click To Deactivate" href="<?php echo e(URL::to('admin/news-letter/subscriber-active/'.$record->id.'/0')); ?>" class="status_any_item btn btn-success btn-small status_user"> <span class="fa fa-check"></span> </a>
							<?php else: ?>
								<a title="Click To Activate" href="<?php echo e(URL::to('admin/news-letter/subscriber-active/'.$record->id.'/1')); ?>" class="status_any_item btn btn-warning btn-small status_user"> <span class="fa fa-ban"></span> </a>
							<?php endif; ?>
							
							<a title="Delete" href="<?php echo e(URL::to('admin/news-letter/subscriber-delete/'.$record->id)); ?>"  class="delete_any_item btn btn-danger btn-small delete_user no-ajax"> <span class="fa fa-trash-o"></span> </a>
						</td>
					</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
					<tr>
						<td class="alignCenterClass" colspan="4" >no records found.</td>
					</tr>
					<?php endif; ?>
				</tbody>
				</table>
			</div>
			<div class="box-footer clearfix">	
				<div class="col-md-3 col-sm-4 "></div>
				<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
			</div>
		</div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/newsletter/subscriber_list.blade.php ENDPATH**/ ?>
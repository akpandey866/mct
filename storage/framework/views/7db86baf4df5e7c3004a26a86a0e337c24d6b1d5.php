<?php $segment1 = Request::segment(1);  ?>
 <nav class="navbar navbar-expand-md p-lg-0 p-0">
 
    <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#collapsibleNavbar1">
   <div class="mobilemenus">
 
	
	<div class="page_title">
	    <?php echo e(!empty($segment1) ? ucfirst(str_replace('-', ' ', $segment1)) : 'Home'); ?>

	</div>
	  <div>
  <!---  <span class="navbar-toggler-icon"></span>
	<span class="navbar-toggler-icon"></span>
	<span class="navbar-toggler-icon"></span>--->
	
	<div class="more_menus">
	    Game menu
	    <div>
	     <span></span>
	    <span></span>
	    <span></span>
	    </div>
	    
	</div>
	
	</div>
	</div>
  </button>
 
 
 <div class="collapse navbar-collapse" id="collapsibleNavbar1">
 

<ul class="nav nav-tabs status_tabs_link d-flex border-0 navbar-nav w-100">
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='lobby') ? 'active' : ''); ?>" href="<?php echo e(route('User.lobby')); ?>">Lobby</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='my-team') ? 'active' : ''); ?>" href="<?php echo e(route('User.myTeam')); ?>">My Team</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='points') ? 'active' : ''); ?>" href="<?php echo e(url('points')); ?>">Points</a></li> 
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='rank') ? 'active' : ''); ?>" href="<?php echo e(url('rank')); ?>">Rank</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='fixtures') ? 'active' : ''); ?>" href="<?php echo e(route('Common.fixtures')); ?>">Fixtures</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='stats') ? 'active' : ''); ?>" href="<?php echo e(url('stats')); ?>">Stats</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='availability') ? 'active' : ''); ?>" href="<?php echo e(route('User.availability')); ?>">Availability</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='chat') ? 'active' : ''); ?>" href="<?php echo e(route('Common.chat')); ?>">Chat</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='prizes') ? 'active' : ''); ?>" href="<?php echo e(url('prizes')); ?>">Prizes</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='winner') ? 'active' : ''); ?>" href="<?php echo e(url('winner')); ?>">Winners</a></li> 
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='history') ? 'active' : ''); ?>" href="<?php echo e(url('history')); ?>">History</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link <?php echo e(($segment1=='sponsors') ? 'active' : ''); ?>" href="<?php echo e(url('sponsors')); ?>">Sponsors</a></li>
</ul>   
</div>
</nav><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/elements/navbar.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<script>
    jQuery(document).ready(function(){
        $(".choosen_selct").chosen();
    });
</script>
<style>
.chosen-container-single .chosen-single{
    height:34px !important;
    padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Point System")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>'><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Point System")); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
		<?php if(auth()->guard('admin')->user()->user_role_id == 1): ?>
			<?php echo e(Form::open(['role' => 'form','url' => 'admin/game-points','class' => 'mws-form',"method"=>"get"])); ?>

			<?php echo e(Form::hidden('display')); ?>	
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<?php echo e(Form::select('club',[null => 'default'] +$cludDetails,((isset($searchVariable['club'])) ? $searchVariable['club'] : ''),['id' => 'club','class'=>'form-control choosen_selct'])); ?>

				</div>
			</div>	
			<div class="col-md-3 col-sm-2">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> <?php echo e(trans('Search')); ?></button>
				<a href="<?php echo e(URL::to('admin/game-points')); ?>"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> <?php echo e(trans("Reset")); ?></a>
			</div>
			<?php echo e(Form::close()); ?>

			<div class="col-md-6 col-sm-6">
				<div class="form-group">  
					<a href="javascript:void(0)" class="btn btn-success btn-small  pull-right" onclick="savePoints()"><?php echo e(trans("Save Points")); ?> </a>
				</div>
			</div>
		<?php else: ?>
		<div class="col-md-6 col-sm-6"></div>
		<!-- <div class="col-md-6 col-sm-6">
			<div class="form-group">  
				<a href="javascript:void(0)" class="btn btn-success btn-small  pull-right" onclick="savePoints()"><?php echo e(trans("Save Points")); ?> </a>
			</div>
		</div> -->
		<?php endif; ?>	
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
					
					<th width="15%">
							Category
						</th>
						<th width="15%">
							Bowler
						</th>
						<th width="15%">
							Batsman / Wicket Keeper / All Rounder
						</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
				<?php echo e(Form::open(['role' => 'form','route' => "GamePoints.updateGamePoint",'class' => 'form_block p-4','id'=>'updateGamePoints','files' => true])); ?>

					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr class="items-inner">
							
							<td>
								<?php echo e($record->attribute_name); ?>   
							</td> 
							<td data-th='<?php echo e(trans("String")); ?>'>
								<?php echo e(Form::number("data[$record->id][bowler]",!empty($record->bowler) ? $record->bowler:'',['class' => 'form-control','step'=>'any'])); ?>

							</td>
							<td>
								
								<?php echo e(Form::number("data[$record->id][bats_wk_ar]",!empty($record->bats_wk_ar) ? $record->bats_wk_ar:'',['class' => 'form-control','step'=>'any'])); ?>

							</td>							
						</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="9" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<?php if((!empty($SeasonStartDate)) && (($SeasonStartDate > time()))): ?>
			<div class="box-footer clearfix">	
				<input type="submit" class="btn btn-success btn-small  pull-left" value="Save Points">
					<!-- <a href="javascript:void(0)" class="btn btn-success btn-small  pull-left" onclick="savePoints()"><?php echo e(trans("Save Points")); ?> </a> -->
			</div>
		<?php endif; ?>
		 <?php echo e(Form::close()); ?>

		</div>
	</div> 
</section> 
<script>
function savePoints() { 
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	$.ajax({
		url: '<?php echo e(URL("admin/game-points/edit-game-points")); ?>',
		type:'post',
		data: $('#updateGamePoints').serialize(),
		success: function(r){ 
			window.location.href	 =	"<?php echo e(URL('admin/game-points')); ?>";
			$('#loader_img').hide();
		}
	});
}
  
$('#updateGamePoints').each(function() {
    $(this).find('input').keypress(function(e) {
        if(e.which == 10 || e.which == 13) {
        savePoints();
        return false;
       }
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/game_points/index.blade.php ENDPATH**/ ?>
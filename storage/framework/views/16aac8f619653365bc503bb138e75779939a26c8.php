<?php $__env->startSection('content'); ?>
<script type="text/javascript"> 
	$(function(){
		/**
		 * For match height of div 
		 */
		$('.items-inner').equalHeights();
		/**
		 * For tooltip
		 */
		var tooltips = $( "[title]" ).tooltip({
			position: {
				my: "right bottom+50",
				at: "right+5 top-5"
			}
		});
	});	
	var action_url = '<?php echo WEBSITE_URL; ?>admin/cms-manager/multiple-action';
	
jQuery(document).ready(function(){
$(".change_status").chosen();
});
</script>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Manage Cms Pages")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Manage Cms Pages")); ?></li>
	</ol>
</section>

<section class="content"> 
	<div class="row">
		<?php echo e(Form::open(['method' => 'get','role' => 'form','url' => 'admin/cms-manager','class' => 'mws-form'])); ?>

		<?php echo e(Form::hidden('display')); ?>

			<div class="col-md-4 col-sm-3">
				<div class="form-group ">  
					<?php echo e(Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => 'form-control' , 'placeholder' => 'Page Name'])); ?>

				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href="<?php echo e(URL::to('admin/cms-manager')); ?>"  class="btn btn-primary"><i class='fa fa-refresh '></i> Reset</a>
			</div>
		<?php echo e(Form::close()); ?>

		<div class="col-md-4 col-sm-5 ">
			<div class="form-group pull-right">  
				<a href="<?php echo e(URL::to('admin/cms-manager/add-cms')); ?>" class="btn btn-success btn-small align"><?php echo e(trans("Add New Cms")); ?> </a>
			</div>
		</div>
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">
							<?php echo e(link_to_route(
								"Cms.index",
								trans("Page Name"),
								array(
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
								"Cms.index",
								 trans("page Title"),
								array(
									'sortBy' => 'title',
									'order' => ($sortBy == 'title' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'title' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'title' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%"><?php echo e(trans("Page Description")); ?></th>
						<th>
							<?php echo e(link_to_route(
								"Cms.index",
								trans("Status"),
								array(
									'sortBy' => 'is_active',
									'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> 
						<th width="20%">
							<?php echo e(link_to_route(
								"Cms.index",
								trans("Modified"),
								array(
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th><?php echo e(trans("Action")); ?></th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr class="items-inner">
						<td data-th='<?php echo e(trans("page name")); ?>'><?php echo e($record->name); ?></td>
						<td data-th='<?php echo e(trans("page title")); ?>'><?php echo e($record->title); ?></td>
						<td data-th='<?php echo e(trans("page desctiption")); ?>'><?php echo e(strip_tags(Str::limit($record->body, 300))); ?></td>
						 <td data-th='<?php echo e(trans("status")); ?>'>
						<?php if($record->is_active	== 1): ?>
							<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
						<?php else: ?>
							<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
						<?php endif; ?>
						</td> 
						<td data-th='<?php echo e(trans("messages.system_management.modified")); ?>'>
						<?php echo e(date(Config::get("Reading.date_format") , strtotime($record->updated_at))); ?>

						</td>
						<td data-th='<?php echo e(trans("action")); ?>'>
							<a title="Edit" href="<?php echo e(URL::to('admin/cms-manager/edit-cms/'.$record->id)); ?>" class="btn btn-primary"><span class="fa fa-pencil"></span></a>
							 <?php if($record->is_active == 1): ?>
								<a  title="Click To Deactivate" href="<?php echo e(URL::to('admin/cms-manager/update-status/'.$record->id.'/0')); ?>" class="btn btn-success btn-small status_any_item"><span class="fa fa-ban"></span>
								</a>
							<?php else: ?>
								<a title="Click To Activate" href="<?php echo e(URL::to('admin/cms-manager/update-status/'.$record->id.'/1')); ?>" class="btn btn-warning btn-small status_any_item"><span class="fa fa-check"></span>
								</a> 
							<?php endif; ?>  
						</td>
					</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="6" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?> 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div>
</section> 
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/Cms/index.blade.php ENDPATH**/ ?>
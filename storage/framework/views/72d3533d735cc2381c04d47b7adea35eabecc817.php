<tr class="add_feature_detail" rel="<?php echo e($counter); ?>">
	<td>
		<div class="mws-form-item add_feature_input">
			<?php if(auth()->guard('admin')->user()->user_role_id != SUPER_ADMIN_ROLE_ID): ?>
				<?php echo e(Form::hidden("data[$counter][club]",auth()->guard('admin')->user()->id,['id' => "club$counter"])); ?>

			<?php endif; ?>
			<?php echo e(Form::text("data[$counter][first_name]",'',['id' => "first_name$counter",'class'=>'form-control'])); ?>

			<span class="help-inline"></span>
		</div>
	</td>
	<td>
		<div class="mws-form-item add_feature_input">
			<?php echo e(Form::text("data[$counter][last_name]",'',['id' => "last_name$counter",'class'=>'form-control'])); ?>

			<span class="help-inline"></span>
		</div>
	</td>
	<td>
		<div class="mws-form-item add_feature_input">
			<?php echo e(Form::select("data[$counter][position]",[null => 'Select Position'] + $position,'',['id' => "position$counter",'class'=>'form-control '])); ?>

			<span class="help-inline"></span>
		</div>
	</td>
<!-- 	<td>
		<div class="mws-form-item add_feature_input">
			<?php echo e(Form::select("data[$counter][category]",[null => 'Select Category'] + $category,'',['id' => "category$counter",'class'=>'form-control '])); ?>

			<span class="help-inline"></span>
		</div>
	</td> -->
	<td>
		<div class="mws-form-item add_feature_input">
			<?php echo e(Form::select("data[$counter][value]",[null => 'Select Value'] + $svalue,'',['id' => "svalue$counter",'class'=>'form-control '])); ?>

			<span class="help-inline" id="svalueerror$counter"></span>
		</div>
	</td>
	<td>
					        	
	<?php echo e(Form::select("data[$counter][bat_style]",[null => 'Please Select Bat Style'] + $batStyle, isset($userDetails->batStyle) ? $userDetails->batStyle :'',['id' => 'batStyle','class'=>'form-control '])); ?>

	</td>
	<td>
	<?php echo e(Form::select("data[$counter][bowl_style]",[null => 'Please Select Bowl Style'] + $bowlStyle,isset($userDetails->bowl_style) ? $userDetails->bowl_style :'',['id' => 'bowl_style','class'=>'form-control '])); ?>

	</td>
	<?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
	<td>
		<div class="mws-form-item add_feature_input">
			<?php echo e(Form::select("data[$counter][club]",[null => 'Select Club'] + $cludDetails,'',['id' => "club$counter",'class'=>'form-control '])); ?>

			<span class="help-inline"></span>
		</div>
	</td>
	<?php endif; ?>
	<td>
		<div class="mws-form-item">
			<?php echo e(Form::file("data[$counter][image]", array('accept' => 'image/*','id' => "image$counter"))); ?>

			<span class="help-inline"></span>
		</div>
	</td>
	<td>
		<a href="javascript:void(0);" onclick="del_feature($(this),0);" id="<?php echo e($counter); ?>" class="btn btn-info btn-small align_button add_more " style="margin-left:15px;">
			<i class="fa fa-trash-o"></i>
		</a>
	</td>
</tr><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/player/add_more_player.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Branding")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Branding")); ?></li>
	</ol>
</section>
<section class="content"> 

<?php if(empty($userDetails->is_paid)): ?>
        <?php echo e(Form::checkbox('branding','1',null, array('id'=>'branding'))); ?>  <label for="branding">Activate Branding</label>  <br>
         <div class="activateBranding" style="display:none">
            <?php echo e(Form::open(['role' => 'form','url' => 'admin/updateBrandingForm','class' => 'mws-form','files'=>'true','id' => 'add_branding'])); ?>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
                                <?php echo e(Form::label('name',trans("Name").' *', ['class' => 'mws-form-label name'])); ?>

                                <div class="mws-form-item">
                                    <?php echo e(Form::text('name',isset($userDetails->name) ? $userDetails->name :'',['class' => 'form-control','required'])); ?>

                                    <div class="error-message help-inline">
                                        <?php echo $errors->first('name'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <?php echo ($errors->first('url')) ? 'has-error' : ''; ?>">
                                <?php echo e(Form::label('url',trans("Website URL").' *', ['class' => 'mws-form-label url'])); ?>

                                <div class="mws-form-item">
                                    <?php echo e(Form::text('url',isset($userDetails->url) ? $userDetails->url :'',['class' => 'form-control'])); ?>

                                    <div class="error-message help-inline">
                                        <?php echo $errors->first('url'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
                                <label for="image" class="mws-form-label image">Brand Logo *</label>
                                <div class="mws-form-item">
                                    <?php echo e(Form::file('logo', array('accept' => 'image/*'))); ?><br>
                                    <?php if(!empty($userDetails->logo) && (File::exists(BRANDING_IMAGE_ROOT_PATH.$userDetails->logo))): ?>
                                        <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo BRANDING_IMAGE_URL.$userDetails->logo; ?>">
                                            <div class="usermgmt_image">
                                                <img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.BRANDING_IMAGE_URL.'/'.$userDetails->logo ?>">
                                            </div>
                                        </a>
                                    <?php endif; ?>
                                    <div class="error-message help-inline">
                                        <?php echo $errors->first('logo'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mws-button-row">
                        <div class="input" >
                           <button type="button" class="btn btn-info" onclick="payAmount()">Pay Now</button>
                        </div>
                    </div>
            <?php echo e(Form::close()); ?>

     </div> 

<?php /*
@else
{{ Form::open(['role' => 'form','url' => 'admin/branding-update','class' => 'mws-form','files'=>'true','id' => 'add_branding']) }}
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
                {{ Form::label('name',trans("Name").' *', ['class' => 'mws-form-label name']) }}
                <div class="mws-form-item">
                    {{ Form::text('name',isset($userDetails->name) ? $userDetails->name :'',['class' => 'form-control']) }}
                    <div class="error-message help-inline">
                        <?php echo $errors->first('name'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('url')) ? 'has-error' : ''; ?>">
                {{ Form::label('url',trans("Website URL").' *', ['class' => 'mws-form-label url']) }}
                <div class="mws-form-item">
                    {{ Form::text('url',isset($userDetails->url) ? $userDetails->url :'',['class' => 'form-control']) }}
                    <div class="error-message help-inline">
                        <?php echo $errors->first('url'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
                <label for="image" class="mws-form-label image">Brand Logo *</label>
                <div class="mws-form-item">
                    {{ Form::file('logo', array('accept' => 'image/*')) }}<br>
                    @if(!empty($userDetails->logo) && (File::exists(BRANDING_IMAGE_ROOT_PATH.$userDetails->logo)))
                        <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo BRANDING_IMAGE_URL.$userDetails->logo; ?>">
                            <div class="usermgmt_image">
                                <img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.BRANDING_IMAGE_URL.'/'.$userDetails->logo ?>">
                            </div>
                        </a>
                    @endif
                    <div class="error-message help-inline">
                        <?php echo $errors->first('logo'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mws-button-row">
        <div class="input" >
           <input type="submit" class="btn btn-success" value="Update" />
        </div>
    </div>
{{ Form::close() }}
@endif   */ ?>
<?php else: ?>
<?php 
$gameMode = Auth::guard('admin')->user()->game_mode;
$price = 0;
if($gameMode == 1){
    $price = SENIORBRANDING;
}elseif ($gameMode == 2) {
    $price = JUNIORBRANDING;
}elseif ($gameMode == 3) {
    $price = LEAGUEBRANDING;
} ?>
<div class="alert alert-success"> 
This is a paid add-on for $<?php echo e($price); ?> (optional).
You can activate now or setup later.
</div>


 <?php echo e(Form::open(['role' => 'form','url' => 'admin/branding-update-new','class' => 'mws-form','files'=>'true','id' => 'add_branding'])); ?>

<div class="row">
    <div class="col-md-6">
        <div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
            <?php echo e(Form::label('name',trans("Name").' *', ['class' => 'mws-form-label name'])); ?>

            <div class="mws-form-item">
                <?php echo e(Form::text('name',isset($userDetails->name) ? $userDetails->name :'',['class' => 'form-control','required'])); ?>

                <div class="error-message help-inline">
                    <?php echo $errors->first('name'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group <?php echo ($errors->first('url')) ? 'has-error' : ''; ?>">
            <?php echo e(Form::label('url',trans("Website URL").' *', ['class' => 'mws-form-label url'])); ?>

            <div class="mws-form-item">
                <?php echo e(Form::text('url',isset($userDetails->url) ? $userDetails->url :'',['class' => 'form-control','required'])); ?>

                <div class="error-message help-inline">
                    <?php echo $errors->first('url'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
            <label for="image" class="mws-form-label image">Brand Logo * <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="Please upload 90px X 90px image for best viewing."><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></label>
            <div class="mws-form-item">
                <?php echo e(Form::file('logo', array('accept' => 'image/*'))); ?><br>
                <?php if(!empty($userDetails->logo) && (File::exists(BRANDING_IMAGE_ROOT_PATH.$userDetails->logo))): ?>
                    <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo BRANDING_IMAGE_URL.$userDetails->logo; ?>">
                        <div class="usermgmt_image">
                            <img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.BRANDING_IMAGE_URL.'/'.$userDetails->logo ?>">
                        </div>
                    </a>
                <?php endif; ?>
                <div class="error-message help-inline">
                    <?php echo $errors->first('logo'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mws-button-row">
    <div class="input" >
       <!-- <button type="button" class="btn btn-info" onclick="payAmount()">Pay Now</button> -->
      <input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
    </div>
</div>
<?php echo e(Form::close()); ?>

<?php endif; ?>
</section> 
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="panel-heading display-table" >
                        <div class="row display-tr" >
                            <h3 class="panel-title display-td" >Payment Details</h3>
                            <div class="display-td" >                            
                                <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-body append_data"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$("#branding").click(function () {
    var isChecked = $("#branding").is(":checked");
    if (isChecked) {
         $('.activateBranding').show();
    } else {
         $('.activateBranding').hide();
    }
});
function payAmount(){
    $('#loader_img').show();
    var club = "<?php echo auth()->guard('admin')->user()->id; ?>";
    var form = $('#add_branding')[0];
    var formData = new FormData(form);
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:' <?php echo e(route("Common.brandingPayment")); ?> ',
        'type':'post',
        data:formData,
        processData: false,
        contentType: false,
        success:function(response){ 
            $('.append_data').html(response);
            $('#myModal').modal('show');
            $('#loader_img').hide();
        }
    });
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/common/edit_branding.blade.php ENDPATH**/ ?>
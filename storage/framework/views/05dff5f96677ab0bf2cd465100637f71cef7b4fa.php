<?php $__env->startSection('content'); ?>
    <div class="body_section">
        <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <section class="status_tab_block mb-2">
            <div class="container mobile_cont">
                 <?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </ul>
                <div class="tab_content_sec border p-4">
                    <h4 class="status_title mb-4">Prizes <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="<?php echo e(!empty($clubMessage) ? $clubMessage:''); ?>"><i style="color: #0f70b7; " class="fa fa-info-circle" aria-hidden="true"></i></a></h4>

                    <?php if(empty($result)): ?>
                    <div class="row">
                        <div class="col-12 py-4">
                            <p>No prizes have yet been added for the game.</p>
                        </div>
                    </div>

                    <?php endif; ?>


                    <?php if(!empty($result)): ?>
                    <h4 class="status_title"><?php echo  Config::get('prize_type')[$result[0]['category_id']] ?></h4>
                    <div class="row">
                        <div class="col-12 py-4">
                            <h5 class="status_subtitle mb-3"><?php echo e($result[0]['title']); ?></h5>
                            <div class="prizes_block d-block d-sm-flex align-items-start py-3">
                                <div class="pb-4 pb-sm-0">
                                    <div class="prizes_img">
                                        <?php if(File::exists(PRIZE_IMAGE_ROOT_PATH.$result[0]['image'])): ?>
                                        <img src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&cropratio=1:1&image='.PRIZE_IMAGE_URL.'/'.$result[0]['image'] ?>" alt="Prizes">
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="w-100 prizes_info px-3 px-sm-4"><?php echo e($result[0]['description']); ?></div>
                            </div>
                        </div>
                        <?php if(isset($result[1])): ?>
                        <div class="col-12 py-4">
                            <h5 class="status_subtitle mb-3"><?php echo e($result[1]['title']); ?></h5>
                            <div class="prizes_block d-block d-sm-flex align-items-start py-3">
                                <div class="pb-4 pb-sm-0">
                                    <div class="prizes_img">
                                        <?php if(File::exists(PRIZE_IMAGE_ROOT_PATH.$result[1]['image'])): ?>
                                        <img src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&cropratio=1:1&image='.PRIZE_IMAGE_URL.'/'.$result[1]['image'] ?>" alt="Prizes">
                                        <?php endif; ?></div>
                                </div>
                               <div class="w-100 prizes_info px-3 px-sm-4"><?php echo e($result[1]['description']); ?></div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if(isset($result[2])): ?>
                         <div class="col-12 py-4">
                            <h5 class="status_subtitle mb-3"><?php echo e($result[2]['title']); ?></h5>
                            <div class="prizes_block d-block d-sm-flex align-items-start py-3">
                                <div class="pb-4 pb-sm-0">
                                    <div class="prizes_img">
                                        <?php if(File::exists(PRIZE_IMAGE_ROOT_PATH.$result[2]['image'])): ?>
                                        <img src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&cropratio=1:1&image='.PRIZE_IMAGE_URL.'/'.$result[2]['image'] ?>" alt="Prizes">
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="w-100 prizes_info px-3 px-sm-4"><?php echo e($result[2]['description']); ?></div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <hr>
                    <?php if(isset($result[3])): ?>
                    <h4 class="status_title"><?php echo  Config::get('prize_type')[$result[3]['category_id']] ?></h4>
                    <div class="row">
                        <div class="col-12 py-4">
                            <h5 class="status_subtitle mb-3"><?php echo e($result[3]['title']); ?></h5>
                            <div class="prizes_block d-block d-sm-flex align-items-start py-3">
                                <div class="pb-4 pb-sm-0">
                                    <div class="prizes_img">
                                        <?php if(File::exists(PRIZE_IMAGE_ROOT_PATH.$result[3]['image'])): ?>
                                        <img src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&cropratio=1:1&image='.PRIZE_IMAGE_URL.'/'.$result[3]['image'] ?>" alt="Prizes">
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="w-100 prizes_info px-3 px-sm-4"><?php echo e($result[3]['description']); ?></div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <hr>
                    <?php if(isset($result[4])): ?>
                    <h4 class="status_title"><?php echo  Config::get('prize_type')[$result[4]['category_id']] ?></h4>
                    <div class="row">
                        <div class="col-12 py-4">
                            <h5 class="status_subtitle mb-3"><?php echo e($result[4]['title']); ?></h5>
                            <div class="prizes_block d-block d-sm-flex align-items-start py-3">
                                <div class="pb-4 pb-sm-0">
                                   <div class="prizes_img">
                                        <?php if(File::exists(PRIZE_IMAGE_ROOT_PATH.$result[4]['image'])): ?>
                                        <img src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&cropratio=1:1&image='.PRIZE_IMAGE_URL.'/'.$result[4]['image'] ?>" alt="Prizes">
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="w-100 prizes_info px-3 px-sm-4"><?php echo e($result[4]['description']); ?></div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <div class="container pt-4">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
            </div>
        </div>
        <script type="text/javascript">
             $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();   

            });
        </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/common/prizes.blade.php ENDPATH**/ ?>
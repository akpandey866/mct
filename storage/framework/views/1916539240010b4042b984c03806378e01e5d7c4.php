<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
		<?php echo e(studly_case($type)); ?> Management
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(studly_case($type)); ?> Management</li>
	</ol>
</section>
	
<section class="content"> 
	<div class="row">
		<?php echo e(Form::open(['method' => 'get','role' => 'form','url' => 'admin/dropdown-manager/'.$type,'class' => 'mws-form'])); ?>

		<?php echo e(Form::hidden('display')); ?>

			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<?php echo e(Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => 'form-control','placeholder'=>studly_case($type)])); ?>

				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href="<?php echo e(URL::to('dropdown-manager/'.$type)); ?>"  class="btn btn-primary"><i class='fa fa-refresh '></i> Reset</a>
			</div>
		<?php echo e(Form::close()); ?>

		<div class="col-md-5 col-sm-3 ">
			<div class="form-group pull-right">  
				<a href="<?php echo e(URL::to('admin/dropdown-manager/add-dropdown/'.$type)); ?>" class="btn btn-success btn-small align"><?php echo e('Add New '.studly_case($type)); ?> </a>
			</div>
		</div>
	</div> 
		
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
			<thead>
				<tr>
					<th width="40%">
						<?php echo e(link_to_route(
								'DropDown.listDropDown',
								'Name',
								array(
									$type,
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc'
								),
							   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)); ?>

					</th>
					
					<th width="30%">
						<?php echo e(link_to_route(
								'DropDown.listDropDown',
								'Created ',
								array(
									$type,
									'sortBy' => 'created_at',
									'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc'
								),
							   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)); ?>

					</th>
					<th><?php echo e('Action'); ?></th>
				</tr>
			</thead>
			<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
				<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr class="items-inner">
					<td data-th='Name'><?php echo e($record->name); ?></td>
					<td data-th='Created At'><?php echo e(date(Config::get("Reading.date_format") , strtotime($record->created_at))); ?></td>
					<td data-th='Action'>						
						<a title="Edit" href="<?php echo e(URL::to('admin/dropdown-manager/edit-dropdown/'.$record->id.'/'.$type)); ?>" class="btn btn-info btn-small"><span class="ti-pencil"></span></a>

						<?php if($type !="position"): ?>
							<a title="Delete" href="<?php echo e(URL::to('admin/dropdown-manager/delete-dropdown/'.$record->id.'/'.$type)); ?>"  class="delete_any_item btn btn-danger btn-small"><span class="ti-trash"></span> </a>
						<?php endif; ?>
						
					</td>
				</tr>
				 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
			</tbody>
		</table>
		<?php else: ?>
			<tr>
			<td align="center" style="text-align:center;" colspan="6" > No Result Found</td>
		  </tr>			
		<?php endif; ?> 
	</div>
	<?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/dropdown/index.blade.php ENDPATH**/ ?>
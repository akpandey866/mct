<?php $__env->startSection('content'); ?>
<script type="text/javascript" src="https://cdn.firebase.com/js/client/2.4.2/firebase.js"></script>
<div class="full_container">
    <div class="body_section">
        <!-- top_header element -->
        <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <section class="status_tab_block mb-5">
            <div class="container mobile_cont">
                <?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
              <!--   <div class="tab_content_sec border  p-0  p-md-4"> -->
                <div class="tab_content_sec border">
              <!--   <div class="chat_wrapper mt-4"> -->
                <div class="chat_wrapper">
                    <!-- <div class="chat mb-3"> -->
                    <div class="chat">
                        <div class="chat_header border-bottom">
                            <h4><?php echo e($gamename); ?> Chat Box</h4>
                            <!--  <ul>
                                <li><img src="<?php echo e(asset('img/img_circle.png')); ?>" alt=""></li>
                                <li><img src="<?php echo e(asset('img/img_circle.png')); ?>" alt=""></li>
                                <li><img src="<?php echo e(asset('img/img_circle.png')); ?>" alt=""></li>
                                <li><img src="<?php echo e(asset('img/img_circle.png')); ?>" alt=""></li>
                                <li><img src="<?php echo e(asset('img/img_circle.png')); ?>" alt=""></li>
                                <li><img src="<?php echo e(asset('img/img_circle.png')); ?>" alt=""></li>
                                <li>6+</li>
                                </ul> -->
                        </div>
                        <div class="chat_message">
                            <div id="chatDiv" style="float: left;width: 100%;"></div>
                        </div>
                        <div class="chat_footer">
                            <div class="type_message">
                                <textarea name="" id="messageInput" cols="30" rows="10" placeholder="Send Message" id="messageInput"></textarea>
                            </div>
                            <div class="send_message">
                                <button type="button" id="submitButton"><i class="fas fa-paper-plane"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    var clubId = "<?php echo e($club_id); ?>";
    var userid = "<?php echo e($userid); ?>";
    var userName = "<?php echo e($userName); ?>";
    var time = "<?php echo e(time()); ?>";
    var image = "<?php echo e($profileImage); ?>";
    var imageURL = "<?php echo e(USER_PROFILE_IMAGE_URL); ?>";
    var d = new Date();
    var timestamp = d.getTime();
    var messageRef = new Firebase('https://myclubtap.firebaseio.com/hello'+clubId);
    $('#submitButton').click(function(e){
        var text = $('#messageInput').val();
        if(text == ""){
            return false;
        }
        messageRef.push({userid:userid,userName:userName, text:text,time:timestamp,image:image});
        $('#messageInput').val("");  
    });
    
    messageRef.on('child_added',function(snapshot){
        var message = snapshot.val();
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var dateFromTimestamp = new Date(message.time);
        var profileImage = message.image;
        var dayName = days[dateFromTimestamp.getDay()];
        var chatTime = dateFromTimestamp.getDate()+'.'+dateFromTimestamp.getMonth()+'.'+dateFromTimestamp.getFullYear().toString().substr(-2);
        if(message.userid == userid){
            document.getElementById('chatDiv').innerHTML +='<div class="outgoing_message"><div class="one_row"><div><h6 class="user_name">'+message.userName+'</h6><div class="actual_mes"><p>'+message.text+'</p><div class="time">'+dayName+' -'+chatTime+'</div></div></div><div class="name"><img class="chat_image" src="'+imageURL+'/'+profileImage+'" alt=""></div></div></div>';
        }else{
            document.getElementById('chatDiv').innerHTML +='<div class="incoming_message"><div class="one_row"><div class="name"><img class="chat_image"  src="'+imageURL+'/'+profileImage+'" alt=""></div><div><h6 class="user_name">'+message.userName+'</h6><div class="actual_mes"><p>'+message.text+'</p><div class="time">'+dayName+' -'+chatTime+'</div></div></div></div></div>';
        }
        $(".chat_message").scrollTop($("#chatDiv").height());    
    });    
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/common/user_chat.blade.php ENDPATH**/ ?>
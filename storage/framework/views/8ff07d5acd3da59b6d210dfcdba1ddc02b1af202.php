<?php $__env->startSection('content'); ?>
<link href="<?php echo e(asset('css/bootstrap-timepicker.min.css')); ?>" rel="stylesheet">
<script src="<?php echo e(asset('js/bootstrap-timepicker.min.js')); ?>"></script>
<section class="content-header">
	<h1>
		<?php echo e(ucfirst($lockoutDetails->game_name)); ?>&nbsp;<?php echo e(trans("Lockout")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Lockout")); ?></li>
	</ol>
</section>
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-primary">
<div class="box-body">
<?php echo e(Form::open(['role' => 'form','route' => 'updateAdminLockout', 'id' => 'lockout'])); ?>    
<input type="hidden" name="user_id" value="<?php echo e(!empty($lockoutDetails->id) ? $lockoutDetails->id :''); ?>">
<div class="row">
    <div class="col-md-6">
       <div class="form-group <?php echo ($errors->first('lockout_start_day')) ? 'has-error' : ''; ?>">
		<?php echo e(Form::label('lockout_start_day',trans("Gameweek Lockout Start Day").' *', ['class' => 'mws-form-label lockout_start_day'])); ?>

			<div class="mws-form-item">
				
				<?php echo e(Form::select('lockout_start_day',[null => 'Select Day'] + Config::get('weekdays'),!empty($lockoutDetails->lockout_start_day) ? $lockoutDetails->lockout_start_day:'', ['id' => 'country','class'=>'form-control'])); ?>

				<div class="error-message help-inline">
					<?php echo $errors->first('lockout_start_day'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group <?php echo ($errors->first('lockout_start_time')) ? 'has-error' : ''; ?>">
			<?php echo e(Form::label('lockout_start_time', trans("Gameweek Lockout Start Time").' *', ['class' => 'mws-form-label'])); ?>

			<div class="input-group date">
			  <div class="input-group-addon">
			    <i class="fa fa-calendar"></i>
			  </div>
			 <?php echo e(Form::text('lockout_start_time',!empty($lockoutDetails->lockout_start_time) ? $lockoutDetails->lockout_start_time:'', ['class' => 'form-control timepicker','placeholder'=>'Date From','id'=>'date_from'])); ?>

			</div>
			<div class="error-message help-inline lockout_start_time">
				<?php echo $errors->first('lockout_start_time'); ?>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-md-6">
       <div class="form-group <?php echo ($errors->first('lockout_end_day')) ? 'has-error' : ''; ?>">
		<?php echo e(Form::label('lockout_end_day',trans("Gameweek Lockout End Day").' *', ['class' => 'mws-form-label'])); ?>

			<div class="mws-form-item">
				<?php echo e(Form::select('lockout_end_day',[null=>'Select Day']+Config::get('weekdays'),!empty($lockoutDetails->lockout_end_day) ? $lockoutDetails->lockout_end_day:'',['class' => 'form-control'])); ?>

				<div class="error-message help-inline">
					<?php echo $errors->first('lockout_end_day'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group <?php echo ($errors->first('lockout_end_time')) ? 'has-error' : ''; ?>">
			<?php echo e(Form::label('lockout_end_time', trans("Gameweek Lockout End Time").' *', ['class' => 'mws-form-label'])); ?>

			<div class="input-group date">
			  <div class="input-group-addon">
			    <i class="fa fa-calendar"></i>
			  </div>
			 <?php echo e(Form::text('lockout_end_time',!empty($lockoutDetails->lockout_end_time) ? $lockoutDetails->lockout_end_time:'', ['class' => 'form-control timepicker','placeholder'=>'Lockout end time','id'=>'lockout_end_time'])); ?>

			</div>
			<div class="error-message help-inline lockout_end_time">
				<?php echo $errors->first('lockout_end_time'); ?>
			</div>
		</div>
	</div>
</div>
<div class="mws-button-row">
	<div class="input" >
		<?php if(Auth::guard('admin')->user()->is_lockout == 0  || Auth::guard('admin')->user()->is_lockout == 2): ?>
			<input type="submit" value="<?php echo e(trans('Update Lockout')); ?>" class="btn btn-danger">
			<span class="label label-info" style="margin : 3px 0px 0px;padding:6px 12px;height: 33px;">Game is currently open</span>
		<?php elseif(Auth::guard('admin')->user()->is_lockout == 1): ?>
			<a href="javascript:void(0)" class="btn btn-danger">Your game is currently in lockout mode</a>
			<!-- When scheduled period is over then msg will be like this
				YOur game is currently in lockout mode.
			 -->
			<a href="javascript:;" class="btn btn-primary force_unlock">Force Unlock Lockout</a> 
		<?php endif; ?>
	</div>
</div>
<?php echo e(Form::close()); ?>

<script type="text/javascript">
$('.timepicker').timepicker({
  showInputs: false,
  showMeridian:false,
})
 $(document).on('click', '.force_unlock', function(e){
    e.stopImmediatePropagation();
    url = $(this).attr('href');
    bootbox.confirm("<b>Only perform FORCE UNLOCK, if you have already completed score entries for all your matches for this week and want to open your game early (as it would at the end of the normal lockout period.Performing this action will end the current lockout period, force close all 1-Day matches and not yet started 2-Day matches within this lockout period as ‘Completed’. It will lockout any 2-Day matches currently ‘In-Progress’ to be scored next week, execute calculation of all Player Values and allocate fantasy Points for players performance to fantasy teams. THIS ACTION IS NON-REVERSIBLE. If you are unsure, please do not proceed or email <em>help@myclubtap</em>.com for help. If you are happy to proceed, please press the button below to force unlock now!</b>",
    function(result){
        if(result){
        	$('#loader_img').show();
				$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				url:' <?php echo e(route("lockout.forceUnlock")); ?> ',
				'type':'post',
				success:function(response){ 
					window.location.reload();
					$('#loader_img').hide();
				}
			}); 
            window.location.replace(url);
        }
    });
    e.preventDefault();
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/common/admin_edit_lockout.blade.php ENDPATH**/ ?>
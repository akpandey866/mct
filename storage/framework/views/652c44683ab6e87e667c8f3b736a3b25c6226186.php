<select name="player_id" class="form-control">
	<option value="" selected="" >Please Select Player</option>
	<?php foreach ($playerlist as $key => $value) {  ?>
		<option value="<?php echo e($key); ?>" <?php echo (in_array($key, $playerIds) ? "disabled":"") ?> ><?php echo e($value); ?></option>
	<?php } ?>
</select>
<div class="error-message help-inline">
	<?php echo $errors->first('player_id'); ?>
</div><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/common/get_verify_players_listing.blade.php ENDPATH**/ ?>
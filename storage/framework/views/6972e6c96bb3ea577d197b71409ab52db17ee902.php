<?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('js/bootstrap-dropdown.js')); ?>"></script>
<section class="content-header">
	<h1>
	  Email Templates
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">Email Templates</li>
	</ol>
</section>

<section class="content"> 
	<div class="row">
		<?php echo e(Form::open(['method' => 'get','role' => 'form','url' => 'admin/email-manager','class' => ''])); ?>

		<?php echo e(Form::hidden('display')); ?>

			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<?php echo e(Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => 'form-control' , 'placeholder' => 'Name'])); ?>

				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="form-group "> 
					<?php echo e(Form::text('subject',((isset($searchVariable['subject'])) ? $searchVariable['subject'] : ''), ['class' => 'form-control' ,'placeholder' => 'Subject'])); ?>

				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary"><i class='fa fa-search '></i> <?php echo e(trans('Search')); ?></button>
				<a href="<?php echo e(URL::to('admin/email-manager')); ?>"  class="btn btn-primary"><i class='fa fa-refresh '></i> <?php echo e(trans("Reset")); ?></a>
			</div>
		<?php echo e(Form::close()); ?>

			
		<?php if(Config::get('app.debug')): ?>
			<div class="col-md-3 col-sm-3 ">
				<div class="form-group pull-right">  
					<a href="<?php echo e(URL::to('admin/email-manager/add-template')); ?>" class="btn btn-success btn-small align"> Add Email Template</a>
				</div>
			</div>
		<?php endif; ?>
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>
							<?php echo e(link_to_route(
								'EmailTemplate.index',
								 'Name' ,
								array(
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th>
							<?php echo e(link_to_route(
								'EmailTemplate.index',
								 'Subject',
								array(
									'sortBy' => 'subject',
									'order' => ($sortBy == 'subject' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'subject' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'subject' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th>
							
							<?php echo e(link_to_route(
								'EmailTemplate.index',
								 'Created',
								array(
									'sortBy' => 'created_at',
									'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					<?php
					if(!$result->isEmpty()){
					foreach($result as $record){?>
					<tr class="items-inner">
						<td><?php echo e($record->name); ?></td>
						<td><?php echo e($record->subject); ?></td>
						<td><?php echo e(date(Config::get("Reading.date_format"),strtotime($record->created_at))); ?></td>
						<td>
							<a title="Edit" href="<?php echo e(URL::to('admin/email-manager/edit-template/'.$record->id)); ?>" class ="btn btn-primary" >
								<span class="fa fa-pencil"></span>
							</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<?php }else{ 
				?>
					 <tr>
						<td align="center" style="text-align:center;" colspan="4" > No Result Found</td>
					  </tr>
				<?php
			} ?>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>                                  
		</div>
	</div>
</section> 
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/emailtemplates/index.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<!-- CKeditor start here-->
<?php echo e(Html::script('js/admin/ckeditor/ckeditor.js')); ?>

<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		<?php echo e(trans("Edit Fundraiser message")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/admin-fundraiser')); ?>">Fundraiser</a></li>
		<li class="active">Edit Fundraiser message</li>
	</ol>
</section>

<section class="content"> 
<?php echo e(Form::open(['role' => 'form','url' => 'admin/admin-fundraiser/edit-message/'.$Id,'class' => 'mws-form'])); ?>

	<div class="row">
		<div class="col-md-6">	
			<div class="form-group <?php echo ($errors->first('message')?'has-error':''); ?>">
				<div class="mws-form-row">
					<?php echo e(Form::label('message',trans("Message").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::text('message',!empty($result->message) ? $result->message:'', ['class' => 'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('message'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				
				<a href="<?php echo e(URL::to('admin/admin-fundraiser/edit-message',$Id)); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans('Reset')); ?></a>
				
				<a href="<?php echo e(URL::to('admin/admin-fundraiser')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
			</div>
		</div>
	</div>
<?php echo e(Form::close()); ?> 
</section>
<style>
	.textarea_resize {
		resize: vertical;
	}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/adminfundraiser/edit_message.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>

<section class="content-header">
	<h1>
		<?php echo e(trans("Edit Team")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/player/')); ?>"><?php echo e(trans("Team")); ?></a></li>
		<li class="active"><?php echo e(trans("Edit Team")); ?> </li>
	</ol>
</section>
<section class="content"> 
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/team/edit-team','class' => 'mws-form','files'=>'true'])); ?>

	<?php echo e(Form::hidden('team_id',$userDetails->id,['class' => ''])); ?>

	<div class="row">
	
		      <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('name',trans("Team Name").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text('name',isset($userDetails->name) ? $userDetails->name :'',['class' => 'form-control'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div></div>
		  </div>
		  <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('type')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('type', trans("Team Type").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::select(
						 'type',
						 [null => 'Please Select Team Type'] + $teamType,
						  isset($userDetails->type) ? $userDetails->type :'',
						 ['id' => 'type','class'=>'form-control']
						)); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('type'); ?>
					</div>
				</div>
			</div>
		</div>		  
		</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('team_category')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('team_category', trans("Team Category").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::select(
						 'team_category',
						 [null => 'Please Select Team Category'] + $teamCategory,
						 isset($userDetails->team_category) ? $userDetails->team_category :'',
						 ['id' => 'team_category','class'=>'form-control']
						)); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('team_category'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php if(Auth::guard('admin')->user()->user_role_id !=1): ?>
			 <div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('grade_name')) ? 'has-error' : ''; ?>">
					<?php echo e(Form::label('grade_name', trans("Grade Name").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::select(
							 'grade_name',
							 [null => 'Please Select Grade Name'] + $gradeName,
							 isset($userDetails->grade_name) ? $userDetails->grade_name :'',
							 ['id' => 'grade_name','class'=>'form-control']
							)); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('grade_name'); ?>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		<?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('club', trans("Club").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::select('club',[null => 'Please Select Club'] + $cludDetails,isset($userDetails->club) ? $userDetails->club :'',['id' => 'club','class'=>'form-control'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('club'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('grade_name')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('grade_name', trans("Grade Name").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item put_html">
					<?php echo e(Form::select('grade_name',[null => 'Please Select Grade Name']+$gradeName,isset($userDetails->grade_name) ? $userDetails->grade_name :'',['id' => 'grade_name','class'=>'form-control'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('grade_name'); ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>

	<?php if(Auth::guard('admin')->user()->user_role_id != SUPER_ADMIN_ROLE_ID): ?>
		<?php echo e(Form::hidden('club',Auth::guard('admin')->user()->id)); ?>

	<?php endif; ?>
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="<?php echo e(trans('Update')); ?>" class="btn btn-danger">
			<a href="<?php echo e(URL::to('admin/team/edit-team/'.$userId)); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans("Reset")); ?></a>
			<a href="<?php echo e(URL::to('admin/team/')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans("Cancel")); ?></a>
		</div>
	</div>
	<?php echo e(Form::close()); ?>

	
</section>
<script type="text/javascript">
$('#club').on('change',function(){
	$('#loader_img').show();
	var id = $(this).val();
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:' <?php echo e(route("Team.getGrade")); ?> ',
		'type':'post',
		data:{'id':id},
		success:function(response){ 
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/team/edit.blade.php ENDPATH**/ ?>
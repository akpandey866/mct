<?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('js/bootstrap-dropdown.js')); ?>"></script>
<section class="content-header">
	<h1>
		<?php echo e(trans("Edit Setting")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/settings')); ?>">Setting</a></li>
		<li class="active">Edit Setting</li>
	</ol>
</section>
<section class="content"> 
	<div class="row pad">
		<div class="col-md-6">	
		<?php echo e(Form::open(['role' => 'form','url' => 'admin/settings/edit-setting/'.$result->id,'class' => 'mws-form'])); ?>

		<div class="mws-panel-body no-padding tab-content">
			<div class="form-group <?php echo ($errors->first('title')?'has-error':''); ?>">
				<div class="mws-form-row">
					<?php echo e(Form::label('title',trans("Title").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::text('title',$result->title, ['class' => 'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('title'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group <?php echo ($errors->first('key')?'has-error':''); ?>">
				<div class="mws-form-row">
					<?php echo e(Form::label('key',trans("Key").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::text('key', $result->key, ['class' => 'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('key'); ?>
						</div>
						<small>e.g., 'Site.title'</small>
					</div>
				</div>
			</div>
			<div class="form-group <?php echo ($errors->first('value')?'has-error':''); ?>">
				<div class="mws-form-row">
					<?php echo e(Form::label('value',trans("Value").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::textarea('value',$result->value, ['class' => 'form-control small','rows'=>false,'cols'=>false,])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('value'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group <?php echo ($errors->first('input_type')?'has-error':''); ?>">
				<div class="mws-form-row">
					<?php echo e(Form::label('input_type',trans("Input Type").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::text('input_type', $result->input_type, ['class' => 'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('input_type'); ?>
						</div>
						<small><em><?php echo "e.g., 'text' or 'textarea'";?></em></small>
					</div>
				</div>
			</div>
			<div class="form-group ">
				<div class="mws-form-row">
					<?php echo e(Form::label('editable', 'Editable', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<div class="input-prepend">
							<span class="add-on"> 
								<?php echo e(Form::checkbox('editable', null, ['class' => 'small'])); ?>

							</span>
							<input type="text" size="16" name="prependedInput2" id="prependedInput2" value="<?php echo "Editable"; ?>" disabled="disabled" style="width:415px;" class="small">
						</div>
					</div>
				</div>
			</div>
			<div class="mws-button-row">
				<input type="submit" value="Save" class="btn btn-danger">
				
				<a href="<?php echo e(URL::to('admin/settings/edit-setting/'.$result->id)); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans('messages.system_management.reset')); ?></a>
				
				<a href="<?php echo e(URL::to('admin/settings')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
			</div>
		</div>
		<?php echo e(Form::close()); ?>

		</div>    	
	</div>
<section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/settings/edit.blade.php ENDPATH**/ ?>
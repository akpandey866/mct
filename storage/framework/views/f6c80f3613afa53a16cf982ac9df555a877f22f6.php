<?php $__env->startSection('content'); ?>

<section class="content-header">
	<h1>
		<?php echo e(trans("Player Packs")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Purchase Packs")); ?> </li>
	</ol>
</section>
<section class="content"> 
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/admin-pay-activate/admin-player-fee/'.$userDetails->id,'class' => 'mws-form','files'=>'true'])); ?>

	<input type="hidden" name="club" value="<?php echo e($userDetails->id); ?>">
	<div class="box">
		<div class="box-body ">
			<div class="row">
			    <div class="col-md-6">
					  <div class="form-group <?php echo ($errors->first('club_type')) ? 'has-error' : ''; ?>">
						<label for="address" class="mws-form-label">Mode</label>
						<div class="mws-form-item">
							<b><?php echo Config::get('home_club')[$userDetails->game_mode]; ?></b>
						</div>
					</div>
				</div>	
				 <div class="col-md-6">
				     <div class="form-group <?php echo ($errors->first('username')) ? 'has-error' : ''; ?>">
						<label for="username" class="mws-form-label">Username </label>
						<div class="mws-form-item">
							<b><?php echo isset($userDetails->username) ? $userDetails->username :''; ?></b>
							<!-- <?php echo e(Form::text('username',isset($userDetails->username) ? $userDetails->username :'',['class' => 'form-control','id'=>'username',"autocomplete"=>"off"])); ?> -->
							<div class="error-message help-inline">
								<?php echo $errors->first('username'); ?>
							</div>
						</div>
					</div>
				</div>
         	</div>
         	<div class="row">
			    <div class="col-md-6">
					  <div class="form-group <?php echo ($errors->first('club_type')) ? 'has-error' : ''; ?>">
						<label for="address" class="mws-form-label">Club Name</label>
						<div class="mws-form-item">
							<b><?php echo !empty($userDetails->club_name) ?  $userDetails->club_name:''; ?></b>
						</div>
					</div>
				</div>	
				 <div class="col-md-6">
				     <div class="form-group <?php echo ($errors->first('username')) ? 'has-error' : ''; ?>">
						<label for="username" class="mws-form-label">Game Name</label>
						<div class="mws-form-item">
							<b><?php echo !empty($userDetails->game_name) ?  $userDetails->game_name:''; ?></b>
							<div class="error-message help-inline">
								<?php echo $errors->first('username'); ?>
							</div>
						</div>
					</div>
				</div>
         	</div>
			<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6">	
					<div class="form-group ">  
						<?php echo e(Form::select('plan',$playerData,(isset($searchVariable['position'])) ? $searchVariable['position'] : '',['id' => 'position','class'=>'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('plan'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="mws-button-row">
				<div class="input">
					<!-- <input type="submit" value="<?php echo e(trans('Pay Now')); ?>" class="btn btn-danger"> -->
					<input type="submit" value="<?php echo e(trans('Update')); ?>" class="btn btn-success">
				</div>
			</div>
		</div>
	</div>
	<?php echo e(Form::close()); ?>

</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/common/admin_player_fee.blade.php ENDPATH**/ ?>
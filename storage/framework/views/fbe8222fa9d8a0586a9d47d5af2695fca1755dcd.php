<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
		<?php echo e(trans("Fundraiser")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Fundraiser")); ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<?php if($errors->any()): ?>
				<div class="bg-red">
					<?php echo implode('', $errors->all('
					<div style="margin-left: 10px;padding:5px 5px 5px 5px">:message</div>
					')); ?>

				</div>
				<?php endif; ?>
				<div class="box-body">
					<div>
					</div>
					<h3>Settings</h3>
					<?php echo e(Form::open(['role' => 'form','url' => 'admin/game-setting', 'id' => 'setting_form'])); ?>    
					<div class="form-group">
						<div class="row">
							<div class="col-sm-4 col-md-2"><label>Set your fundraising amount</label></div>
							<div class="col-sm-4 col-md-2">
								<div class="input-group">
									<span class="input-group-addon">$</span>
									<?php echo e(Form::text('entry_price', (!empty($this_club->entry_price) ? $this_club->entry_price : ''), ['id' => 'entry_price', 'placeholder' => 'Fundraising Amount', 'class' => 'form-control textboxsetting','style'=>'height: 38px;'])); ?>

								</div>
								<div class="error-message help-inline entry_price" style="display: none;">
									Please enter fundraising amount.
								</div>
								<!-- <div class="success_msg" style="color: green; display: none;">Saved successfully</div> -->
							</div>
							<!-- 	<div class="col-sm-4 col-md-2"><button style="width: 100px; " type="button" onclick="submitForm();" class="btn bg-olive btn-block">Save</button></div> -->
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-4 col-md-2"><label>Set your fundraising target</label></div>
							<div class="col-sm-4 col-md-2">
								<div class="input-group">
									<span class="input-group-addon">$</span>
									<?php echo e(Form::text('fundraising_target', (!empty($this_club->fundraising_target) ? $this_club->fundraising_target : ''), ['id' => 'fundraising_target', 'placeholder' => 'Fundraising target', 'class' => 'form-control textboxsetting','style'=>'height: 38px;'])); ?>

								</div>
								<div class="error-message help-inline fundraising_target" style="display: none;">
									Please enter fundraising target.
								</div>
							</div>
							<!-- 	<div class="col-sm-4 col-md-2"><button style="width: 100px; " type="button" onclick="submitForm();" class="btn bg-olive btn-block">Save</button></div> -->
						</div>
					</div>
					<h3>Payment Provider</h3>
					<?php if($gameSettingCount  <=0 ): ?>
					<!-- <div>Get payment directly in your account, connect your Stripe account below:</div>	 -->
					<?php 
						if (isset($_GET['code'])) { // Redirect w/ code
						   		$code = $_GET['code'];
						 
							   $token_request_body = array(
							 	'grant_type' => 'authorization_code',
							 	'client_id' => 'ca_DgfrZTFH6E9swZwKeQRlropefCekMTNh',
							 	'code' => $code,
							 	'client_secret' => 'sk_live_x5qmfFqJZEhq1FnBmVaXybEA'
							   );
							 
							   $req = curl_init(TOKEN_URI .'?' . http_build_query($token_request_body));
							   curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
							   curl_setopt($req, CURLOPT_POST, true );
							   curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
							 
							   // TODO: Additional error handling
							   $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
							   $resp = json_decode(curl_exec($req), true);
							   curl_close($req);
							   echo $resp['stripe_user_id'];
							} else if (isset($_GET['error'])) { 
								// Error
						   		echo $_GET['error_description'];
						
							 } else { // Show OAuth link
							   $authorize_request_body = array(
							 	'response_type' => 'code',
							 	'scope' => 'read_write',
							 	'client_id' => 'ca_DgfrZTFH6E9swZwKeQRlropefCekMTNh'
							   );
						 
						   $url = AUTHORIZE_URI. '?' . http_build_query($authorize_request_body);
						   $imgTag = "<img src=".asset('img/blue-on-dark.png')." />";
						   echo "<a class='' style='margin-top:10px;display:inline-block;' href='$url'>".$imgTag."</a>";
						 } ?>
					<p></p>
					<p>
						Creating or connecting a Stripe account is simple and allows you to set-up fundraising through your fantasy game in the Senior game mode, where members can pay and support the fundraising efforts of your club. They can make payment using their credit card. Payments are deposited within 7-days to your club’s bank account.
					</p>
					<p>
						Stripe is a world-leader in mobile and on-line payments, certified to PCA Service Provider Level 1, the most stringent level of certification.
					</p>
					<p>
						MyClubtap does not charge any fees (0%) from the Fundraiser value, other than fees charged by Stripe for payment processing. Stripe fees for your region can be found at: <a  target="_blank" href="https://stripe.com/pricing">www.stripe.com/pricing</a>.
					</p>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12 col-md-12">
								<div class="input-group">
									<input style="width: 20px; height: 20px; " type="checkbox" id="terms_condition" required="required"> <span style="vertical-align: top; margin-left: 10px; font-size: 18px; " href="#">I Agree to MyClubtap <a target="_blank" href="<?php echo e(url('pages/terms')); ?>"> Terms</a> & <a target="_blank" href="<?php echo e(url('pages/conditions')); ?>">Conditions</a></span>
									<div class="error-message help-inline terms_condition" style="display: none;">
										Please accept our terms and conditions.
									</div>
								</div>
							</div>
							<!-- 	<div class="col-sm-4 col-md-2"><button style="width: 100px; " type="button" onclick="submitForm();" class="btn bg-olive btn-block">Save</button></div> -->
						</div>
					</div>
					<div class="col-sm-4 col-md-2">
						<button style="width: 100px; " type="button" onclick="submitForm();" class="btn bg-olive btn-block">Save</button>
						<img id="loading_img" style="display: none; margin-top: 3px; " src="<?php echo e(asset('img/loading_img.gif')); ?>" />
						<div class="success_msg" style="color: green; display: none;">Saved successfully</div>
					</div>
				<?php else: ?>
				<span class="label label-success" style="padding: 10px 10px 10px 13px;margin-left: 5px;">Connected</span> &nbsp;&nbsp;
				<!-- <button style="width: 100px;float: left; " type="button" onclick="submitForm();" class="btn bg-olive btn-block">Update</button> -->
				<?php endif; ?>
					<?php echo e(Form::close()); ?>

				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('#setting_form').each(function() {
	$(this).find('input').keypress(function(e) {
	    if(e.which == 10 || e.which == 13) {
	submitForm();
	return false;
	     }
	 });
	});
	function submitForm(){
	// alert();
	$('.error-message').hide(); 
	$('.success_msg').hide(); 
	$('.terms_condition').hide(); 
	// alert($('#terms_condition').val()); 
	if(!$('#terms_condition').is(":checked")){
	$('.terms_condition').show(); 
	return false; 
	
	}
	$('#loading_img').show(); 
	$.ajax({
	method: "POST",
	url: "save-other-game-setting",
	data: { _token: "<?php echo e(csrf_token()); ?>", form_data: $("#setting_form").serialize() }
	})
	.done(function( msg ) {
	// console.log(msg); 
	$('#loading_img').hide(); 
	
	if(msg.entry_price){
	$('.entry_price').html(msg.entry_price); 

	$('.entry_price').show(); 
	}else if(msg.fundraising_target){
	$('.fundraising_target').show();
	}else if(msg ==1){
	$('.success_msg').show(); 
	$('.success_msg').hide(5000);
	}
	// else{
	// 	$('.entry_price').show(); 
	// }
	});
	
	}
	
</script>
<style type="text/css">
	.textboxsetting{ width: 200px; display: inline-block; }
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/common/game_setting.blade.php ENDPATH**/ ?>
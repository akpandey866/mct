<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
		 <?php echo e(trans("Users Detail")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/users/'.USER)); ?>"><?php echo e(trans("Userss")); ?></a></li>
		<li class="active"> <?php echo e(trans("Users Detail")); ?>  </li>
	</ol>
</section>
<div class="box box-warning "> 
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">PROFILE INFORMATION</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Full Name:</th>
						<td data-th='Full Name' class="txtFntSze"><?php echo e(isset($userDetails->full_name) ? $userDetails->full_name:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Username:</th>
						<td data-th='Full Name' class="txtFntSze"><?php echo e(isset($userDetails->username) ? $userDetails->username:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Email:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($userDetails->email) ? $userDetails->email :''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Date Of Birth:</th>
						<td data-th='Phone Number' class="txtFntSze"><?php echo e($userDetails->dob); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Gender:</th>
						<td data-th='Phone Number' class="txtFntSze"><?php echo e($userDetails->gender); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Phone:</th>
						<td data-th='Phone Number' class="txtFntSze"><?php echo e($userDetails->phone); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Country:</th>
						<td data-th='Phone Number' class="txtFntSze"><?php echo e($userDetails->country); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">State:</th>
						<td data-th='Phone Number' class="txtFntSze"><?php echo e($userDetails->state); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">City:</th>
						<td data-th='Phone Number' class="txtFntSze"><?php echo e($userDetails->city); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Address:</th>
						<td data-th='Address' class="txtFntSze"><?php echo e(str_replace('\n','<br />',$userDetails->address)); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Zip Code:</th>
						<td data-th='Address' class="txtFntSze"><?php echo e($userDetails->zipcode); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Home Club:</th>
						<td data-th='Address' class="txtFntSze"><?php echo e(!empty($userDetails->home_club) ? config::get('home_club')[$userDetails->home_club]: ''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">How did you hear about us?:</th>
						<td data-th='Address' class="txtFntSze"><?php echo e($userDetails->aboutus); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/usermgmt/view.blade.php ENDPATH**/ ?>
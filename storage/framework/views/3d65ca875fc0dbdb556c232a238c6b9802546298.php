<?php $__env->startSection('content'); ?>
<script>
    jQuery(document).ready(function(){
        $(".choosen_selct").chosen();
    });
</script>
<style>
.chosen-container-single .chosen-single{
    height:34px !important;
    padding:3px 6px;
}
</style>
<section class="content-header">
    <h1>
      <?php echo e(trans("Game Prizes")); ?>

    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><?php echo e(trans("Game Prizes")); ?></li>
    </ol>
</section>
<section class="content"> 
<?php if(auth()->guard('admin')->user()->id == 1): ?>
    <div class="row">
        <?php echo e(Form::open(['method' => 'get','role' => 'form','url' => 'admin/game-prizes','class' => 'mws-form'])); ?>

        <?php echo e(Form::hidden('display')); ?>

            <div class="col-md-2 col-sm-3">
                <div class="form-group ">  
                    <div class="form-group ">  
                    <?php echo e(Form::select('game_mode',[null => 'Please Select Mode'] + Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''), ['class' => 'form-control choosen_selct game_mode'])); ?>

                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3">
                <div class="form-group ">  
                    <div class="form-group get_game_name">  
                    <?php echo e(Form::select('game_name',array(''=>trans('Select Game')),((isset($searchVariable['game_name'])) ? $searchVariable['game_name'] : ''), ['class' => 'form-control choosen_selct'])); ?>

                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3">
                <div class="form-group ">  
                    <div class="form-group">  
                     <?php echo e(Form::select('category_id',[null => 'Please Select Category'] + Config::get('prize_type'),((isset($searchVariable['category_id'])) ? $searchVariable['category_id'] : ''), ['class' => 'form-control choosen_selct'])); ?>

                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3">
                <div class="form-group ">  
                    <div class="form-group ">  
                    <?php echo e(Form::select('is_active',array(''=>trans('Select Status'),0=>'Inactive',1=>'Active'),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control choosen_selct'])); ?>

                </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4">
                <button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
                <a href="<?php echo e(URL::to('admin/game-prizes')); ?>"  class="btn btn-primary"><i class='fa fa-refresh '></i> Reset</a>
            </div>
        <?php echo e(Form::close()); ?>

      <!--   <div class="col-md-4 col-sm-12">
            <div class="form-group">  
                <a href="<?php echo e(URL::to('admin/game-prizes/add-game-prize')); ?>" class="btn btn-success btn-small  pull-right" style="margin:0;"><?php echo e(trans("Add New Game Prize")); ?> </a>
            </div>
        </div> -->
    </div> 
<?php endif; ?>
    <div class="box">
        <div class="box-body ">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="11%">
                            Image
                        </th>
                        <th width="11%">
                            <?php echo e(link_to_route(
                                    "GamePrize.index",
                                    trans("Category"),
                                    array(
                                        'sortBy' => 'category_id',
                                        'order' => ($sortBy == 'category_id' && $order == 'desc') ? 'asc' : 'desc',
                                        $query_string
                                    ),
                                   array('class' => (($sortBy == 'category_id' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'category_id' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                )); ?>

                        </th>
                        <th width="11%">
                            <?php echo e(link_to_route(
                                    "GamePrize.index",
                                    trans("Title"),
                                    array(
                                        'sortBy' => 'title',
                                        'order' => ($sortBy == 'title' && $order == 'desc') ? 'asc' : 'desc',
                                        $query_string
                                    ),
                                   array('class' => (($sortBy == 'title' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'title' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                )); ?>

                        </th>
                        <th width="11%">
                            <?php echo e(link_to_route(
                                    "GamePrize.index",
                                    trans("Status"),
                                    array(
                                        'sortBy' => 'is_active',
                                        'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
                                        $query_string
                                    ),
                                   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                )); ?>

                        </th>
                        <th width="20%">
                            <?php echo e(link_to_route(
                                    "GamePrize.index",
                                    trans("Created On"),
                                    array(
                                        'sortBy' => 'created_at',
                                        'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
                                        $query_string
                                    ),
                                   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                )); ?>

                        </th>
                        <th width="20%">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody id="powerwidgets">
                <?php if(!$result->isEmpty()): ?>
                    <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
                        <tr class="items-inner">
                            <td>
                                <?php if(File::exists(PRIZE_IMAGE_ROOT_PATH.$record->image)): ?>
                                    <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo PRIZE_IMAGE_URL.$record->image; ?>">
                                        <div class="usermgmt_image">
                                            <img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&cropratio=1:1&image='.PRIZE_IMAGE_URL.'/'.$record->image ?>">
                                        </div>
                                    </a>
                                <?php endif; ?>
                            </td>
                            <td> 
                                <?php echo e(config('prize_type')[$record->category_id]); ?> 
                            </td>
                            <td>
                                <?php echo e($record->title); ?>

                            </td>
                            <td>
                                <?php if($record->is_active  ==1): ?>
                                    <span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
                                <?php else: ?>
                                    <span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo e(date(config::get("Reading.date_format"),strtotime($record->created_at))); ?>

                            </td>
                            <td data-th='Action'>
                                <a title="<?php echo e(trans('Edit')); ?>" href="<?php echo e(URL::to('admin/game-prizes/edit-game-prize/'.$record->id)); ?>" class="btn btn-primary">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <?php if($record->is_active == 1): ?>
                                    <a  title="Click To Deactivate" href="<?php echo e(URL::to('admin/game-prizes/update-status/'.$record->id.'/0')); ?>" class="btn btn-success btn-small status_any_item"><span class="fa fa-check"></span>
                                    </a>
                                <?php else: ?>
                                    <a title="Click To Activate" href="<?php echo e(URL::to('admin/game-prizes/update-status/'.$record->id.'/1')); ?>" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
                                    </a> 
                                <?php endif; ?>    
                                                 
                               <!--  <a title="<?php echo e(trans('Delete')); ?>" href="<?php echo e(URL::to('admin/game-prizes/delete-game-prize/'.$record->id)); ?>"  class="delete_any_item btn btn-danger">
                                    <i class="fa fa-trash-o"></i>
                                </a> -->
                                
                            </td>
                        </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                        <tr>
                            <td class="alignCenterClass" colspan="9" ><?php echo e(trans("No record is yet available.")); ?></td>
                        </tr>
                    <?php endif; ?>
        </tbody>                    
            </table>
        </div>
        <div class="box-footer clearfix">   
            <div class="col-md-3 col-sm-4 "></div>
            <div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
        </div>
    </div> 
</section> 
<script type="text/javascript">
    var game_name = "<?php echo !empty($searchVariable['game_name']) ? $searchVariable['game_name'] :'' ?>";
    var game_mode = "<?php echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode'] :'' ?>";
    if(game_name !="" || game_mode !=""){
        get_game_name();

    }
    function get_game_name(){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:' <?php echo e("admin-get-game-name"); ?> ',
            'type':'post',
            data:{'mode':game_mode,'game_name':game_name},
            success:function(response){ 
                $(".get_game_name").html(response);
            }
        });
    }
    $(".game_mode").on('change',function(){
        var mode = $(this).val();
        $('#loader_img').show();
         $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:' <?php echo e(route("GamePrize.adminGetGameName")); ?> ',
            'type':'post',
            data:{'mode':mode,'game_name':game_name},
            success:function(response){ 
                $(".get_game_name").html(response);
            }
        }); 
        $('#loader_img').hide();
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/game_prizes/index.blade.php ENDPATH**/ ?>
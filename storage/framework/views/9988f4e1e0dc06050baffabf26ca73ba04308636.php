<?php $__env->startSection('content'); ?>

<section class="content-header">
	<h1>
		<?php echo e(trans("Manage Player Packs")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Manage Player Packs")); ?> </li>
	</ol>
</section>
<section class="content"> 
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/player-packs','class' => 'mws-form','files'=>'true'])); ?>

	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-12">
			<?php if($errors->any()): ?>
			       <div class="bg-red"> <?php echo implode('', $errors->all('<div style="margin-left: 10px;">:message</div>')); ?></div>
			<?php endif; ?>
			<table class="table table-responsive table-bordered lastrow_feature_detail" width="100%">
				<?php if($packsDetails->isEmpty()): ?>
					<tr>
						<th>Players</th>
						<th>Price</th>
						<th></th>
					</tr>
					<tr class="add_brand0 add_more_feature_detail add_feature_detail" id="0">
						<td>
							<div class="mws-form-item add_feature_input">
								<?php echo e(Form::text('name[]','',['id' => 'name0','class'=>'form-control'])); ?>

								<span class="help-inline"></span>
							</div>
						</td>
						<td>
							<div class="mws-form-item add_feature_input">
								<?php echo e(Form::text('price[]','',['id' => 'price0','class'=>'form-control'])); ?>

								<span class="help-inline"></span>
							</div>
						</td>
						<td><input  type="button" value="Add More" class="btn btn-primary" id="add_more_feature_detail"/></td>
					</tr>
				<?php else: ?>
					<tr>
						<th>Players</th>
						<th>Price</th>
						<th><input  type="button" value="Add More" class="btn btn-primary" id="add_more_feature_detail"/></th>
					</tr>
					<?php $__currentLoopData = $packsDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr class="add_brand<?php echo e($key); ?> add_more_feature_detail add_feature_detail" id="<?php echo e($value->id); ?>">
							<td>
								<div class="mws-form-item add_feature_input">
									<?php echo e(Form::text("name[]",$value->name,['id' => 'name'.$value->id,'class'=>'form-control'])); ?>

									<span class="help-inline"></span>
								</div>
							</td>
							<td>
								<div class="mws-form-item add_feature_input">
									<?php echo e(Form::text("price[]",$value->price,['id' => 'price'.$value->id,'class'=>'form-control'])); ?>

									<span class="help-inline"></span>
								</div>
							</td>
							<td>
								<!-- <input type="hidden" name="id[]" value="<?php echo e($value->id); ?>">
								<a href="javascript:void(0);" onclick="del_feature($(this),'<?php echo e($value->id); ?>');" id="<?php echo e($value->id); ?>" class="btn btn-info btn-small align_button add_more " style="margin-left:15px;" data-id="<?php echo e($value->id); ?>">
									<i class="fa fa-trash-o"></i>
								</a> -->
							</td>
						</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
			</table>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input">
			<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
			<a href="<?php echo e(URL::to('admin/player-packs')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans("Cancel")); ?></a>
		</div>
	</div>
	<?php echo e(Form::close()); ?>

</section>
<script type="text/javascript">
$('#add_more_feature_detail').on('click', function() {
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	var get_last_id			=	$('.lastrow_feature_detail:last tr:last').attr('rel');
	if(typeof get_last_id === "undefined") {
		get_last_id			=	0;
	}
	var finalValNumber = $("#name"+get_last_id).val();
	var counter  = parseInt(get_last_id) + 1;
	if((finalValNumber != '')){ 
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url:'<?php echo e(url("admin/add-more-player-pack")); ?>',
			'type':'post',
			data:{'counter':counter},
			success:function(response){
				$('#loader_img').hide();
				$('.add_feature_detail').last().after(response);
			}
		});
	}else{    
		$(".add_feature_input").each(function(){ 
			var dataVal = $(this).find("input,text,file,select").val();
			if(dataVal == "" || dataVal == "undefined"){
				$(this).find("input,text,select").next().addClass('error');
				$(this).find("input,text,select").next().html("This field is required.");
			}
		});
		$('#loader_img').hide();
		return false;
	}
});
function del_feature(_this,id){ 
	if(id>0){
		bootbox.confirm("Are you sure want to remove this ?",
		function(result){
			if(result){
				$('#loader_img').show();
				$.ajax({
					headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					url:'<?php echo e(url("admin/delete-add-more-player-pack")); ?>',
					'type':'post',
					data:{'id':id},
					success:function(response){
						_this.closest('tr').remove();
						$('#loader_img').hide();

					}
				});
			}
		});
	}else{
		bootbox.confirm("Are you sure want to remove this ?",
		function(result){
			if(result){
				_this.closest('tr').remove();
			}
		});
	}
	
}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/common/additional_players.blade.php ENDPATH**/ ?>
<?php 

$segment1 = Request::segment(1); 
$login_user = Auth::user();
// dump($sessClubLists); die; 
use Carbon\Carbon;
?>
<div class="full_container">
<?php if(empty($login_user)) { ?>
    <header class="navigation before_login_header <?php echo e(!empty($segment1) ? 'header_fixed' : ''); ?>">
        <nav class="navbar navbar-expand-md">
            <div class="container">
                <a class="logo" href="<?php echo e(url('/')); ?>" title="MyClubTap"><img src="<?php echo e(asset('img/logo.png')); ?>" alt="MyClubTap"></a>
                <a class="logo_white" href="<?php echo e(url('/')); ?>" title="MyClubTap"><img src="<?php echo e(asset('img/logo_white.png')); ?>" alt="MyClubTap"></a>
                <ul class="home_toggle_block d-inline-flex d-lg-none ml-auto">
                    <!--<li class="switch"><input type="checkbox"><span class="slider">&nbsp;</span></li>
                    <li class="notification"><a href="" title="Notification"><i class="far fa-bell"></i><span class="info_dot">15</span></a></li>-->
                    <li><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span><span class="navbar-toggler-icon"></span><span class="navbar-toggler-icon"></span></button></li>
                </ul>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav d-flex  ml-5 ml-lg-auto top_menu">
                        <li class="nav-item"><a class="<?php echo e((empty($segment1) ? 'active' :'')); ?>" href="<?php echo e(url('/')); ?>" title="Home">HOME</a></li>
                        <li class="nav-item"><a class="<?php echo e((($segment1 == 'about') ? 'active' :'')); ?>" href="<?php echo e(URL::to('about')); ?>" title="About us">ABOUT</a></li>
                        <li class="nav-item"><a class="<?php echo e((($segment1 == 'features') ? 'active' :'')); ?>" href="<?php echo e(URL::to('features')); ?>" title="FEATURES">FEATURES</a></li>
                        <li class="nav-item"><a class="<?php echo e((($segment1 == 'pricing') ? 'active' :'')); ?>" href="<?php echo e(route('Common.pricing')); ?>" title="PRICING">PRICING</a></li>
                        <!-- <li class="nav-item"><a class="<?php echo e((($segment1 == 'pricing') ? 'active' :'')); ?>" href="<?php echo e(route('Home.signup')); ?>" title="PRICING">Sign-Up</a></li> -->
                       <!--  <li class="nav-item"><a href="" title="Signup/Login"><a href="javascript:void(0)">SIGNUP</a> <span class="d-inline-flex align-items-center px-2" style="color: #ffffff;">/</span> <a href="">LOGIN</a></a></li> -->
                       <!-- <li class="nav-item"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">SUBSCRIBE</a></li> -->
                       <?php if(!empty($login_user)): ?>
                        <li>
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                            hi, <?php echo e($login_user->first_name); ?>

                            </a>
                            <ul class="dropdown-menu nav_custom">
                                <li class="">
                                <a class="dropdown-item <?php echo e(($segment1 == 'dashboard') ? 'active' : ''); ?>" href="<?php echo e(URL('dashboard')); ?>"><?php echo e(trans("Dashboard")); ?></a>
                                </li>
                                <li class=""><a class="dropdown-item <?php echo e(($segment1 == 'edit-profile') ? 'active' : ''); ?>" href="<?php echo e(URL('edit-profile')); ?>"><?php echo e(trans("Edit Profile")); ?></a>
                                </li>
                                <li class=""><a class="dropdown-item <?php echo e(($segment1 == 'change-password') ? 'active' : ''); ?>" href="<?php echo e(URL('change-password')); ?>"><?php echo e(trans("Change Password")); ?></a></li>
                                <li class=""> <a class="dropdown-item" href="<?php echo e(URL('logout')); ?>"><?php echo e(trans("Logout")); ?></a></li>
                            </ul>
                        </li>
                        <?php else: ?>
                          <li class="nav-item"><a href="<?php echo e(route('Home.signupAsFront')); ?>">SIGN-UP</a></li>
                           <li class="nav-item"><a href="<?php echo e(route('Home.login')); ?>">LOGIN</a></li>  
                        <?php endif; ?> 
                    </ul>
                </div>
            </div>
        </nav>
    </header>
<?php }else{ ?>
    <header class="navigation before_login_header <?php echo e(!empty($segment1) ? 'header_fixed' : ''); ?>">
        <nav class="navbar navbar-expand-md">
            <div class="container">
                <a class="logo" href="<?php echo e(url('/')); ?>" title="MyClubTap"><img src="<?php echo e(asset('img/logo.png')); ?>" alt="MyClubTap"></a>
                <a class="logo_white" href="<?php echo e(url('/')); ?>" title="MyClubTap"><img src="<?php echo e(asset('img/logo_white.png')); ?>" alt="MyClubTap"></a>
                <ul class="home_toggle_block d-inline-flex d-lg-none ml-auto">
                    <!--<li class="switch"><input type="checkbox"><span class="slider">&nbsp;</span></li>
                    <li class="notification"><a href="" title="Notification"><i class="far fa-bell"></i><span class="info_dot">15</span></a></li>-->
                    <li class="top_user_profile position-relative d-inline-flex d-lg-none mr-3">
                        <a href="" data-toggle="dropdown"><span class="user_pic_after_login">
                        <?php if(Auth::user()->image): ?>
                                <img src="<?php echo e(asset('uploads/user_profile/'.Auth::user()->image)); ?>" alt="User">
                         <?php else: ?>
                                <img src="<?php echo e(asset('img/user_pic.jpg')); ?>" alt="User">                         
                         <?php endif; ?>                        
                        </span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo e(route('User.updateProfile')); ?>" title="My Profile">My Profile</a></li>
                            <?php if(auth()->guard('web')->user()->user_role_id == CLUBUSER): ?>
                                <li><a href="<?php echo e(url('logout-admin')); ?>" target="_blank" title="Admin Portal">Admin Portal</a></li>
                            <?php endif; ?>
                            <?php $playersIds = players_ids();?>
                            <?php if(in_array(auth()->guard('web')->user()->id,$playersIds)): ?>
                                <li><a href="<?php echo e(route('Gloabal.playerAccount')); ?>" title="My Player">My Player</a></li>
                            <?php endif; ?>
                            <?php if(auth()->guard('web')->user()->user_role_id == USER || auth()->guard('web')->user()->user_role_id == SCORER): ?>
                            <li><a href="<?php echo e(route('Gloabal.fundraiserListing')); ?>" title="My Fundraiser">My Fundraiser</a></li>
                            <?php endif; ?>
                            <?php $scorerData = scorer_ids();?>
                            <?php if(in_array(auth()->guard('web')->user()->id,$scorerData)): ?>
                                <li><a href="<?php echo e(url('logout-admin')); ?>" target="_blank" title="Scorer Portal">Scorer Portal</a></li>
                            <?php endif; ?>                            
                            <li><a class="red" href="<?php echo e(url('logout')); ?>" title="Logout">Logout</a></li>
                        </ul>
                    </li>
                    <li class="d-inline-flex cart_view position-relative dropdown d-lg-none mr-3">
                            <a class="dropdown-toggle" href="javascript:void(0);" id="notificationDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Notification"><i class="far fa-bell bell_class"></i><span class="info_dot"><?php echo e(getNotificationCount()); ?></span></a>
                            <?php $getNotifications = get_user_notification();
                                if(!$getNotifications->isEmpty()){
                            ?>
                            <ul class="dropdown-menu notification" aria-labelledby="notificationDropdown">
                                <?php foreach ($getNotifications as $key => $value) { ?>
                                <li class="d-block">
                                    <!-- <strong class="text-uppercase pb-2">New Laura Ashley Images</strong> -->
                                    <?php echo e(!empty($value->text) ? $value->text :''); ?> <!-- <a href="">3 new images</a> -->
                                    <div class="mt-4">
                                        <?php echo date('D,M d Y',strtotime($value->created_at)); ?> at <?php echo date('h:ia',strtotime($value->created_at));  ?>
                                        <!-- <?php echo e(Carbon::parse($value->created_at)->isoFormat('ddd').' - '.Carbon::parse($value->created_at)->isoFormat('D.M.YY')); ?> -->

                                    </div>
                                    <!-- <div class="notification_delete"><i class="fas fa-times time_class1"></i></div> -->
                                </li>
                                <?php } ?>
                            </ul>
                            <?php }else{ ?>
                            <ul class="dropdown-menu notification" aria-labelledby="notificationDropdown">
                                <li class="d-block">No record is yet available.</li>
                            </ul>
                            <?php } ?>
                    </li>
                    <li><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span><span class="navbar-toggler-icon"></span><span class="navbar-toggler-icon"></span></button></li>
                </ul>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav d-flex ml-0 ml-lg-5">
                        <li class="nav-item"><a class="<?php echo e(($segment1 == 'fantasy') ? 'active' : ''); ?>" href="<?php echo e(url('/lobby')); ?>" title="Fantasy">Fantasy</a></li>
                        <li class="nav-item"><a href="<?php echo e(url('/my-comps')); ?>"  class="<?php echo e(($segment1 == 'my-comps') ? 'active' : ''); ?>"  title="My Comps">My Comps</a></li>
                      <!--  <li class="nav-item"><a href="" title="Referral">Referral</a></li> -->
                        <li class="nav-item"><a class="<?php echo e(($segment1 == 'partners') ? 'active' : ''); ?>" href="<?php echo e(url('partners')); ?>" title="Partners">Partners</a></li>
                        <li class="nav-item"><a class="<?php echo e(($segment1 == 'refer') ? 'active' : ''); ?>" href="<?php echo e(url('refer')); ?>" title="Refer">Refer</a></li>
                        <li class="nav-item"><a class="<?php echo e(($segment1 == 'help&support') ? 'active' : ''); ?>" href="<?php echo e(url('help&support')); ?>" title="Help">Help</a></li>
                    </ul> 
                    <ul class="home_toggle_block d-inline-block d-lg-inline-flex ml-n4 ml-lg-auto">
                    <li class="d-block d-lg-inline-flex">
                        <form method="get">
                             <?php echo e(Form::select('game_mode',Config::get('home_club'),!empty(\Session::get('sess_game_mode')) ? \Session::get('sess_game_mode') : auth()->guard('web')->user()->game_mode, ['class' => 'custom-select','id'=>'game_mode'])); ?>

                        </form>
                     </li>
                        <li class="top_user_profile position-relative d-none d-lg-inline-flex ml-3">
                            <a href="" data-toggle="dropdown"><span class="user_pic_after_login">
							
							
							<?php if(Auth::user()->image): ?>
									<img src="<?php echo e(asset('uploads/user_profile/'.Auth::user()->image)); ?>" alt="User">
							 <?php else: ?>
								 	<img src="<?php echo e(asset('img/user_pic.jpg')); ?>" alt="User">
							 
							 <?php endif; ?>
							
							</span></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo e(route('User.updateProfile')); ?>" title="My Profile">My Profile</a></li>
                                <?php if(auth()->guard('web')->user()->user_role_id == CLUBUSER): ?>
                                    <li><a href="<?php echo e(url('logout-admin')); ?>" target="_blank" title="Admin Portal">Admin Portal</a></li>
                                <?php endif; ?>
                                <?php $playersIds = players_ids();?>
                                <?php if(in_array(auth()->guard('web')->user()->id,$playersIds)): ?>
                                    <li><a href="<?php echo e(route('Gloabal.playerAccount')); ?>" title="My Player">My Player</a></li>
                                <?php endif; ?>
                                <?php if(auth()->guard('web')->user()->user_role_id == USER || auth()->guard('web')->user()->user_role_id == SCORER): ?>
                                <li><a href="<?php echo e(route('Gloabal.fundraiserListing')); ?>" title="My Fundraiser">My Fundraiser</a></li>
                                <?php endif; ?>
                                <?php $scorerData = scorer_ids();?>
                                <?php if(in_array(auth()->guard('web')->user()->id,$scorerData)): ?>
                                    <li><a href="<?php echo e(url('logout-admin')); ?>" target="_blank" title="Scorer Portal">Scorer Portal</a></li>
                                <?php endif; ?>
                                
                                <li><a class="red" href="<?php echo e(url('logout')); ?>" title="Logout">Logout</a></li>
                            </ul>
                        </li>
                        <li class=" cart_view position-relative dropdown d-none d-lg-inline-flex ml-3">
                            <a class="dropdown-toggle" href="javascript:void(0);" id="notificationDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Notification"><i class="far fa-bell bell_class"></i><span class="info_dot"><?php echo e(getNotificationCount()); ?></span></a>
                            <?php $getNotifications = get_user_notification();
                                if(!$getNotifications->isEmpty()){
                            ?>
                            <ul class="dropdown-menu notification" aria-labelledby="notificationDropdown">
                                <?php foreach ($getNotifications as $key => $value) { ?>
                                <li class="d-block">
                                    <!-- <strong class="text-uppercase pb-2">New Laura Ashley Images</strong> -->
                                    <?php echo e(!empty($value->text) ? $value->text :''); ?> <!-- <a href="">3 new images</a> -->
                                     <div class="mt-4">
                                        <?php echo date('D,M d Y',strtotime($value->created_at)); ?> at <?php echo date('h:ia',strtotime($value->created_at));  ?>
                                        </div>
                                    <!-- <div class="notification_delete"><i class="fas fa-times time_class1"></i></div> -->
                                </li>
                                <?php } ?>
                            </ul>
                            <?php }else{ ?>
                            <ul class="dropdown-menu notification" aria-labelledby="notificationDropdown">
                                <li class="d-block">No record is yet available.</li>
                            </ul>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <script type="text/javascript">
        var scriptVar = "<?php echo e(URL::route('getClubList')); ?>";
        var getGameName = "<?php echo e(URL::route('getGameName')); ?>";
        var club_image_url = "<?php echo e(CLUB_IMAGE_URL); ?>";
    </script>
    <script src="<?php echo e(asset('js/header.js')); ?>"></script>


<script type="text/javascript">
    
$('#loader_img').show();
$(document).on("change","select#game_mode",function() {
    $('#loader_img').show(); 
    $(this).closest('form').submit();
});
$( document ).ready(function() {
    $('#loader_img').show();
});
jQuery(window).load(function () {
    $('#loader_img').show(); 

    setTimeout(function () {
       $('#loader_img').hide(); 
    }, 1000);

});


//Menu multiple dropdown
$('.dropdown-menu a.dropdown-toggle').on('click', function(e){
  if (!$(this).next().hasClass('show')){
    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
  }
  var $subMenu = $(this).next(".dropdown-menu");
  $subMenu.toggleClass('show');

  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e){
    $('.dropdown-submenu .show').removeClass("show");
  });
  return false;
}); 

$(".notification").click(function(e){
  e.stopPropagation();
});

    $("#notificationDropdown").on('click',function(){
       $('.info_dot').html(0);
        $.ajax({
            url:' <?php echo e(route("Global.checkNotification")); ?> ',
            /*data:{'id':productId,'quantity':productQuantity},*/
           /* async : false,*/
            success:function(response){ 
               $('.info_dot').html(0);
            }
        });
    });
</script>


<?php } ?>
<?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/elements/header.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<?php echo e(Html::script('js/admin/ckeditor/ckeditor.js')); ?>

<section class="content-header">
	<h1>Edit Notification</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::route('notification.listing')); ?>">Notifications</a></li>
		<li class="active">Edit Notification</li>
	</ol>
</section>
<section class="content">
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/user-notification/edit-notification/'.$Id,'class' => 'mws-form','files' => true])); ?>

	<div class="row pad">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('body')) ? 'has-error' : ''; ?>">
					<?php echo e(Form::label('body',trans("Body").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::textarea('body',$notifications->body,['clas'=>'form-control','id' => 'body'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('body'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-primary">
		<a href="<?php echo e(route('notification.editNotification',$notifications->id)); ?>" class="btn btn-danger"><i class=\"icon-refresh\"></i> Reset</a>
	</div>
	<?php echo e(Form::close()); ?> 
</section>
<script type="text/javascript">
/* For CKEDITOR */
	CKEDITOR.replace( 'body',
	{
		height: 350,
		width: 507,
		filebrowserUploadUrl : '<?php echo URL::to('admin/base/uploder'); ?>',
		filebrowserImageWindowWidth : '640',
		filebrowserImageWindowHeight : '480',
		enterMode : CKEDITOR.ENTER_BR
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/notifications/edit.blade.php ENDPATH**/ ?>
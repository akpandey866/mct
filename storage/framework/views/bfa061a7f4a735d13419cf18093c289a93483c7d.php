<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <title><?php echo e(Config::get("Site.title")); ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('img/favicon.ico')); ?>">
        <!-- Script -->
        <script src="<?php echo e(asset('js/admin/jquery.min.js')); ?>"></script>
        <link href="<?php echo e(asset('css/admin/bootstrap.min.css')); ?>" rel="stylesheet">
        <script src="<?php echo e(asset('js/admin/bootstrap.min.js')); ?>"></script>    
        <link href="<?php echo e(asset('css/admin/font-awesome.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/admin/ionicons.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/admin/morris.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/admin/jquery-jvectormap-1.2.2.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/admin/bootstrap3-wysihtml5.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/admin/themify-icons.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/admin/AdminLTE.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/admin/custom_admin.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/admin/bootmodel.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('css/admin/notification/jquery.toastmessage.css')); ?>" rel="stylesheet">
        <script src="<?php echo e(asset('css/admin/notification/jquery.toastmessage.js')); ?>"></script>
        <script src="<?php echo e(asset('js/admin/jquery.equalheights.js')); ?>"></script>
        <link href="<?php echo e(asset('css/admin/chosen.min.css')); ?>" rel="stylesheet">
        <script src="<?php echo e(asset('js/admin/chosen.jquery.min.js')); ?>"></script>
    </head>
    <body class="skin-black">
        <header class="header">
            <?php if(Auth::guard('admin')->user()->user_role_id == 1): ?>
                <a href="<?php echo e(URL::to('admin/dashboard')); ?>" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining --> 
            <?php else: ?>
                <a href="<?php echo e(URL::to('admin/club-dashboard')); ?>" class="logo">
            <?php endif; ?>
                    <img src="<?php echo e(asset('img/logo.png')); ?>">
                    
                </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button--> 
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
                <div class="navbar-right">

                    <ul class="nav navbar-nav">
                        <?php $authuser = Auth::guard('admin')->user(); ?>
                        <?php if($authuser->user_role_id !=1): ?>
                        <li><span class="label label-info" style="margin: 3px 3px;"><b><?php echo e($authuser->game_name); ?></b></span></li>
                        <?php if(!empty($authuser->game_mode)): ?>
                          
                             <li><span class="label label-success" style="margin: 3px 3px;"><b><?php echo e(Config::get('home_club')[$authuser->game_mode]); ?></b></span></li>
                        <?php endif; ?>
                       
                         <li>
                            <?php if($authuser->user_role_id ==1): ?>
                                <?php if($authuser->is_game_activate == 1): ?>
                                    <span class="label label-success" style="margin: 3px 3px;"><b>Active</b></span>
                                <?php else: ?>
                                    <span class="label label-danger" style="margin: 3px 3px;"><b>Inactive</b></span>
                                <?php endif; ?>

                            <?php endif; ?>
                             <?php if($authuser->user_role_id ==CLUBUSER): ?>
                                <?php if($authuser->is_game_activate == 1): ?>
                                    <span class="label label-success" style="margin: 3px 3px;"><b>Active</b></span>
                                <?php else: ?>
                                    <span class="label label-danger" style="margin: 3px 3px;"><b>Inactive</b></span>
                                <?php endif; ?>

                            <?php endif; ?>
                            <?php if($authuser->user_role_id == SCORER): ?>
                                <?php if(get_admin_role_id() == 1): ?>
                                    <span class="label label-success" style="margin: 3px 3px;"><b>Active</b></span>
                                <?php else: ?>
                                    <span class="label label-danger" style="margin: 3px 3px;"><b>Inactive</b></span>
                                <?php endif; ?>
                            <?php endif; ?>
                            </li>
                            <li>
                            <?php if($authuser->user_role_id == 4): ?>
                            <span class="label label-info" style="margin: 3px 3px;"><a style="color:#ffffff;" target="_blank" href="https://myclubtap.freshdesk.com/a/solutions/articles/43000534774" > Help For Scorer</a></span>
                            <?php else: ?>
                            <span class="label label-info" style="margin: 3px 3px;"><a style="color:#ffffff;" target="_blank" href="https://myclubtap.freshdesk.com/support/solutions/43000364012" > Help For Admins</a></span>
                            <?php endif; ?>
                            
                            </li>
                        <?php endif; ?>
                         
         
                       
                        <li class="dropdown user user-menu">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i> <span><?php echo e(auth()->guard('admin')->user()->full_name); ?> <i class="caret"></i></span> </a>
                            <ul class="dropdown-menu">
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left"><a class="btn btn-default btn-flat" href="<?php echo e(URL::to('admin/myaccount')); ?>">
                                        <?php echo e(trans("Edit Profile")); ?> </a> 
                                    </div>
                                    <div class="pull-right"> <a class="btn btn-default btn-flat" href="<?php echo e(URL::to('admin/logout')); ?>">
                                        <?php echo e(trans("Logout")); ?> </a>
                                    </div>
                                </li>
								
                            </ul>
						
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Start Main Wrapper -->
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php 
                $segment1   =   Request::segment(1); //admin
                $segment2   =   Request::segment(2); //url
                $segment3   =   Request::segment(3); //parameters
                $segment4   =   Request::segment(4); 
                $segment5   =   Request::segment(5); 
                 
                ?>
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <ul class='sidebar-menu'>
                        <!-- Super Admin Menu listing start here -->
                        <?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
                        <li class="<?php echo e(($segment2 == 'dashboard') ? 'active' : ''); ?> "><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-home  <?php echo e(($segment3 == 'dashboard') ? '' : ''); ?>"></i><?php echo e(trans("Dashboard")); ?> </a></li>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('club','list-grade','team','fixture','game-prizes','game-points')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment3 ,array('club','list-grade','team','fixture','game-prizes','game-points')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Game")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('club','list-grade','team','fixture','game-prizes','game-points')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('club','player-packs','purchase-player-packs','additional-player-logs')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='club'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/club')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Basics")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='list-grade'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/list-grade')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Grades")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='team'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/team')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Teams")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='fixture'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/fixture')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Fixtures")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='game-prizes'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/game-prizes')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Prizes")); ?> </a>
                                </li>
                                <?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
                                <li  <?php if($segment2 =='game-points'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/game-points')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Point System")); ?> </a>
                                </li>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('admin-verify-users','availability','admin-scorer-access','admin-edit-branding','admin-add-branding','admin-branding')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment3 ,array('admin-verify-users','availability','admin-scorer-access','admin-edit-branding','admin-add-branding','admin-branding')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Add-Ons")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('admin-verify-users','availability','admin-scorer-access','admin-edit-branding','admin-add-branding','admin-branding')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('club','player-packs','additional-player-logs','admin-add-branding','admin-branding')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='availability'): ?> class="active" <?php endif; ?>> <a href="<?php echo e(URL::to('admin/availability')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Availability")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='admin-verify-users'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/admin-verify-users')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Verify Users")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='admin-scorer-access'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/admin-scorer-access')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Scorers")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='admin-branding' || $segment2 =='admin-edit-branding' || $segment2 =='admin-add-branding'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/admin-branding')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Branding")); ?> </a>
                                </li>
                            </ul>
                        </li>
                         <li class="treeview <?php echo e(in_array($segment2 ,array('admin-lockout','lockout-log','admin-edit-lockout','admin-lockout-log')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment3 ,array('admin-lockout','lockout-log','admin-edit-lockout','admin-lockout-log')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Gameweek")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('admin-lockout','admin-lockout-log')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('admin-lockout','admin-lockout-log','admin-edit-lockout')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='admin-lockout'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/admin-lockout')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Lockout")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='admin-lockout-log'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/admin-lockout-log')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Gameweeks")); ?> </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('player','additional-player-logs','player-packs')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment3 ,array('player','additional-player-logs','player-packs')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Players")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('player','additional-player-logs')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('club','player-packs','additional-player-logs')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='player'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/player')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Players")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='player-packs'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/player-packs')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Purchase Players")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='additional-player-logs'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/additional-player-logs')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Purchase Log")); ?> </a>
                                </li>
                                <!-- <li  <?php if($segment2 =='availability'): ?> class="active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::to('admin/availability')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Availability")); ?> </a>
                                    </li> -->
                            </ul>
                        </li>
                        <li class="<?php echo e(($segment2 == 'admin-pay-activate') ? 'active in' : ''); ?>">
                             <a href="<?php echo e(URL::to('admin/admin-pay-activate')); ?>">
                            <i class="fa fa-dollar  <?php echo e($segment2 == 'admin-pay-activate' ? '' : ''); ?>"></i>
                            <?php echo e('Pay & Activate'); ?> 
                            </a>
                        </li>
                        <li class="<?php echo e(($segment2 == 'admin-fundraiser') ? 'active in' : ''); ?>">
                             <a href="<?php echo e(URL::to('admin/admin-fundraiser')); ?>">
                            <i class="fa fa-dollar  <?php echo e($segment2 == 'admin-fundraiser' ? '' : ''); ?>"></i>
                            <?php echo e('Fundraiser'); ?> 
                            </a>
                        </li>
                        <li class="<?php echo e(($segment2 == 'users' && $segment3 == USER) ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/users/'.USER)); ?>">
                            <i class="fa fa-users <?php echo e($segment2 == 'users' ? '' : ''); ?>"></i>
                            <?php echo e('Users'); ?> 
                            </a>
                        </li>

                        <li class="<?php echo e(($segment2 == 'sponsor') ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/sponsor')); ?>">
                            <i class="fa fa-cc-diners-club <?php echo e($segment2 == 'sponsor' ? '' : ''); ?>"></i> <?php echo e('MCT Partners '); ?> 
                            </a>
                        </li>
                        <li class="<?php echo e(($segment2 == 'about-club') ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/about-club')); ?>">
                            <i class="fa fa-cc-diners-club <?php echo e($segment2 == 'about-club' ? '' : ''); ?>"></i>
                            <?php echo e('MCT Features'); ?> 
                            </a>
                        </li>
                        <!-- About US -->
                        <li class="treeview <?php echo e(in_array($segment2 ,array('platform','serve','partner','user-slider','team-slider')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment3 ,array('platform','serve','team-slider')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("About MyClubtap")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('platform','serve','partner','user-slider')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('platform','partner','serve','user-slider','team-slider')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='platform' || $segment3 =='platform'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/platform')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Feature Box")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='serve'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/serve')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Partner Games")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='partner'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/partner')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Community Partners")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='user-slider'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/user-slider')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Feedback Slider")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='team-slider'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/team-slider')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Our Team")); ?> </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('slider-manager','clubs-slider')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment2 ,array('slider-manager','clubs-slider')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Sliders")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('slider-manager','clubs-slider')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment2 ,array('slider-manager','clubs-slider')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='slider-manager' && $segment3 == 'clubs-slider'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/slider-manager/clubs-slider')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Signup")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='slider-manager' && $segment3 == 'fantasy-slider'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/slider-manager/fantasy-slider')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("How to Play")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='slider-manager' && $segment3 == 'platfrom-feature'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/slider-manager/platfrom-feature')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Features")); ?> </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('dropdown-manager','food-category')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment2 ,array('dropdown-manager','bowl-style','bat-style')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("MCT Masters")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('dropdown-manager','bowl-style','bat-style')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment2 ,array('dropdown-manager','bowl-style','bat-style')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'position'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/position')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Position")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'club-position'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/club-position')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Admin Position")); ?> </a>
                                </li>

                                <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'club-position'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/bat-style')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Batting Style")); ?> </a>
                                </li>

                                 <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'club-position'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/bowl-style')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Bowling Style")); ?> </a>
                                </li>
                       
                                <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'svalue'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/svalue')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Fantasy Values")); ?> </a>
                                </li>
                                <!-- <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'jvalue'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/jvalue')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("JValue")); ?> </a>
                                </li> -->
                                <!-- <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'teamtype'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/teamtype')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Team Type")); ?> </a>
                                </li> -->
                                <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'teamcategory'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/teamcategory')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Team Category")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'matchtype'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/matchtype')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Match Type")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'vanue'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/vanue')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Match Venue")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='dropdown-manager' && $segment3 == 'about-us'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/dropdown-manager/about-us')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Hear About Us")); ?> </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('cms-manager','no-cms-manager','faqs-manager','categories','email-manager','email-logs','block-manager','subscriber','user-notification','news-letter','checklist')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-desktop  <?php echo e(in_array($segment2 ,array('cms-manager','email-manager','email-logs','block-manager','faqs-manager','subscriber','user-notification','news-letter','checklist')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("System Management")); ?> </a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('cms-manager','email-manager','faqs-manager','email-logs','block-manager','subscriber','user-notification','news-letter','checklist')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment2 ,array('cms-manager')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='cms-manager'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/cms-manager')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("CMS")); ?> </a>
                                </li>
                                <li <?php if($segment2=='subscriber'): ?> class="active" <?php endif; ?>><a href="<?php echo e(URL::to('admin/subscriber')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Subscriber")); ?> </a></li>
                                <li <?php if($segment2 =='email-manager'): ?> class="active" <?php endif; ?> ><a href="<?php echo e(URL::to('admin/email-manager')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Email Templete")); ?> </a></li>
                                <li <?php if($segment2=='email-logs'): ?> class="active" <?php endif; ?>><a href="<?php echo e(URL::to('admin/email-logs')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Email Logs")); ?> </a></li>

                                <li <?php if($segment2=='user-notification'): ?> class="active" <?php endif; ?>><a href="<?php echo e(URL::to('admin/user-notification')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Notifications")); ?> </a></li>

                                <li <?php if($segment2=='news-letter'): ?> class="active" <?php endif; ?>><a href="<?php echo e(URL::to('admin/news-letter/newsletter-templates')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Newsletter")); ?> </a></li>

                                <li <?php if($segment2=='checklist'): ?> class="active" <?php endif; ?>><a href="<?php echo e(URL::to('admin/checklist')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Set-up checklist")); ?> </a></li>

                            </ul>
                        </li>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('settings')) ? 'active in' : 'offer-reports'); ?> <?php echo e(in_array($segment2 ,array('language-settings')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-cogs  <?php echo e(in_array($segment2 ,array('settings')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Manage Settings")); ?> </a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('settings')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment2 ,array('settings')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2=='settings' && Request::segment(4)=='Site'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/settings/prefix/Site')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Site")); ?> </a>
                                </li>

                                <li  <?php if($segment2=='settings' && Request::segment(4)=='Reading'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/settings/prefix/Reading')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Reading")); ?> </a>
                                
                                </li>
                                <li  <?php if($segment2=='settings' && Request::segment(4)=='Social'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/settings/prefix/Social')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Social")); ?> </a>
                                </li> 
                            </ul>
                        </li>
                        <li class="<?php echo e(($segment2 == 'help&support') ? 'active in' : ''); ?>">
                             <a href="<?php echo e(URL::to('admin/help&support')); ?>">
                            <i class="fa fa-question  <?php echo e($segment2 == 'help&support' ? '' : ''); ?>"></i>
                            <?php echo e('Help & Support'); ?> 
                            </a>
                        </li>
                         <li class="<?php echo e(($segment2 == '') ? 'active in' : ''); ?>">
                             <a href="https://community.myclubtap.com">
                            <i class="fa fa-compass  <?php echo e($segment2 == 'help&support' ? '' : ''); ?>"></i>
                            <?php echo e('MyClubtap Community'); ?> 
                            </a>
                        </li>
                        <li class="<?php echo e(($segment2 == 'book-intro-sesssion') ? 'active in' : ''); ?>">
                             <a href="<?php echo e(URL::to('admin/book-intro-sesssion')); ?>">
                            <i class="fa fa-book <?php echo e($segment2 == 'book-intro-sesssion' ? '' : ''); ?>"></i>
                            <?php echo e('Book Intro Session'); ?> 
                            </a>
                        </li>
                        
                        <?php endif; ?>
                        <!-- Super Admin Menu listing finish here -->
                        <?php if(Auth::guard('admin')->user()->user_role_id == SCORER): ?>
                        <li class="<?php echo e(($segment2 == 'fixture' || $segment2=='team-player') ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/fixture')); ?>">
                            <i class="fa fa-cc-diners-club <?php echo e($segment2 == 'fixture' ? '' : ''); ?>"></i>
                            <?php echo e('Manage Fixture'); ?> 
                            </a>
                        </li>
                         <li class="<?php echo e(($segment2 == 'book-intro-sesssion') ? 'active in' : ''); ?>">
                             <a href="<?php echo e(URL::to('admin/book-intro-sesssion')); ?>">
                            <i class="fa fa-book <?php echo e($segment2 == 'book-intro-sesssion' ? '' : ''); ?>"></i>
                            <?php echo e('Book Intro Session'); ?> 
                            </a>
                        </li>
                        <li class="<?php echo e(($segment2 == 'help&support') ? 'active in' : ''); ?>">
                             <a href="<?php echo e(URL::to('admin/help&support')); ?>">
                            <i class="fa fa-question  <?php echo e($segment2 == 'help&support' ? '' : ''); ?>"></i>
                            <?php echo e('Help & Support'); ?> 
                            </a>
                        </li>
                        <?php endif; ?>
                        <!-- Club User menu List Start Here -->
                        <?php if(Auth::guard('admin')->user()->user_role_id == CLUBUSER): ?>
                        <li class="<?php echo e(($segment2 == 'club-dashboard') ? 'active' : ''); ?> "><a href="<?php echo e(URL::to('admin/club-dashboard')); ?>"><i class="fa fa-home  <?php echo e(($segment3 == 'club-dashboard') ? '' : ''); ?>"></i><?php echo e(trans("Dashboard")); ?> </a></li>
                        <li class="<?php echo e(($segment2 == 'set-up-checklist') ? 'active' : ''); ?> "><a href="<?php echo e(URL::to('admin/set-up-checklist')); ?>"><i class="fa fa-list  <?php echo e(($segment3 == 'set-up-checklist') ? '' : ''); ?>"></i><?php echo e(trans("Set-Up Checklist")); ?> </a></li>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('club','list-grade','add-grade','edit-grade','team','fixture','game-prizes','game-points','game-prizes-message')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment3 ,array('club','list-grade','add-grade','edit-grade','team','fixture','game-prizes','game-points','game-prizes-message')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Games")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('club','list-grade','team','fixture','game-prizes','game-points')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('club','player-packs','purchase-player-packs','additional-player-logs')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='club'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/club/edit-club/'.auth()->guard('admin')->user()->id)); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Basics")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='list-grade' || $segment2 =='add-grade'|| $segment2 =='edit-grade'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/list-grade')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Grades")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='team'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/team')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Teams")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='fixture'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/fixture')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Fixtures")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='game-prizes'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/game-prizes')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Prizes")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='game-prizes-message'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/game-prizes/game-prizes-message')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Prizes Message")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='game-points'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/game-points')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Point System")); ?> </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('lockout','lockout-log')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment3 ,array('lockout','lockout-log')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Gameweek")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('lockout','lockout-log')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('lockout','lockout-log')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='lockout'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/lockout')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Lockout")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='lockout-log'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/lockout-log')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Gameweeks")); ?> </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('player','additional-player-logs','purchase-player-packs','player-packs')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment3 ,array('player','additional-player-logs','player-packs')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Players")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('player','additional-player-logs','purchase-player-packs','player-packs')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('club','player-packs','purchase-player-packs','additional-player-logs','purchase-player-packs','player-packs')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='player'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/player')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Players")); ?> </a>
                                </li>
                                <?php if(Auth::guard('admin')->user()->game_mode == 1 && Auth::guard('admin')->user()->is_game_activate == 1): ?>

                                <li  <?php if($segment2 =='purchase-player-packs'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/purchase-player-packs')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Purchase Players")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='additional-player-logs'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/additional-player-logs')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Purchase Log")); ?> </a>
                                </li>

                                <?php endif; ?>
                            </ul>
                        </li>
                        <?php if(auth()->guard('admin')->user()->game_mode == 1): ?>
                        <li class="treeview <?php echo e(in_array($segment2 ,array('game-setting','fundraiser-message','fundraiser-history')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment3 ,array('game-setting','fundraiser-message','fundraiser-history')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Fundraiser")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('game-setting')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('game-setting','fundraiser-message','fundraiser-history')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='game-setting'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/game-setting')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Settings")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='fundraiser-message'): ?> class="active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::to('admin/fundraiser-message')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Message")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='fundraiser-history'): ?> class="active" <?php endif; ?>>
                                    <a href="<?php echo e(URL::to('admin/fundraiser-history')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("History")); ?> </a>
                                </li>
                            </ul>
                        </li>
                        <?php endif; ?>
                        
                        <li class="treeview <?php echo e(in_array($segment2 ,array('verify-users','availability','scorer-access', 'sponsor', 'branding')) ? 'active in' : 'offer-reports'); ?>">
                            <a href="javascript::void(0)"><i class="fa fa-th  <?php echo e(in_array($segment3 ,array('verify-users','availability','scorer-access', 'sponsor', 'branding')) ? '' : ''); ?>"></i><i class="fa pull-right fa-angle-left"></i><?php echo e(trans("Add-Ons")); ?></a>
                            <ul class="treeview-menu <?php echo e(in_array($segment2 ,array('verify-users','availability','scorer-access', 'sponsor', 'branding')) ? 'open' : 'closed'); ?>" style="treeview-menu <?php echo e(in_array($segment3 ,array('club','player-packs','additional-player-logs')) ? 'display:block;' : 'display:none;'); ?>">
                                <li  <?php if($segment2 =='availability'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/availability')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Availability")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='verify-users'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/verify-users')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Verify Users")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='scorer-access'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/scorer-access')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Scorers")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='sponsor'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/sponsor')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Sponsors")); ?> </a>
                                </li>
                                <li  <?php if($segment2 =='branding'): ?> class="active" <?php endif; ?>>
                                <a href="<?php echo e(URL::to('admin/branding')); ?>"><i class='fa fa-angle-double-right'></i><?php echo e(trans("Branding")); ?> </a>
                                </li>
                            </ul>
                        </li>

                         <li class="<?php echo e(($segment2 == 'show-activate-game') ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/show-activate-game')); ?>">
                            <i class="fa fa-lock <?php echo e($segment2 == 'show-activate-game' ? '' : ''); ?>"></i>
                            Pay & Activate
                            </a>
                        </li>

                        <li class="<?php echo e(($segment2 == 'users' && $segment3 == USER) ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/users/'.USER)); ?>">
                            <i class="fa fa-users <?php echo e($segment2 == 'users' ? '' : ''); ?>"></i>
                            <?php echo e('Users'); ?> 
                            </a>
                        </li>

                        <!--  <li class="<?php echo e(($segment2 == 'scorer-access') ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/scorer-access')); ?>">
                                <i class="fa fa-cc-diners-club <?php echo e($segment2 == 'about-club' ? '' : ''); ?>"></i>
                                <?php echo e('Scorer Access'); ?> 
                            </a>
                            </li>
                            <li class="<?php echo e(($segment2 == 'verify-users') ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/verify-users')); ?>">
                                <i class="fa fa-cc-diners-club <?php echo e($segment2 == 'verify-users' ? '' : ''); ?>"></i>
                                <?php echo e('Verify Users'); ?> 
                            </a>
                            </li> -->
                        <!-- <li class="<?php echo e(($segment2 == 'sponsor') ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/sponsor')); ?>">
                                <i class="fa fa-cc-diners-club <?php echo e($segment2 == 'sponsor' ? '' : ''); ?>"></i><?php echo e('Manage Sponsor'); ?> 
                            </a>
                            </li> -->
                        <?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
                        <li class="<?php echo e(($segment2 == 'game-points') ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/game-points')); ?>">
                            <i class="fa fa-cc-diners-club <?php echo e($segment2 == 'game-points' ? '' : ''); ?>"></i>
                            <?php echo e('Manage Game Points'); ?> 
                            </a>
                        </li>
                        <?php endif; ?>
                        <!-- <li class="<?php echo e(($segment2 == 'branding') ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/branding')); ?>">
                                <i class="fa fa-cc-diners-club <?php echo e($segment2 == 'branding' ? '' : ''); ?>"></i>
                                <?php echo e('Branding'); ?> 
                            </a>
                            </li> -->
                        <?php /*
                            @if(auth()->guard('admin')->user()->game_mode == 1)
                            <li class="{{ ($segment2 == 'game-setting') ? 'active in' : '' }}">
                                <a href="{{URL::to('admin/game-setting')}}">
                                    <i class="fa fa-cc-diners-club {{ $segment2 == 'game-setting' ? '' : '' }}"></i>
                                    {{'Package'}} 
                                </a>
                            </li>
                            @endif
                            */ ?>
                        <!--     <li class="<?php echo e(($segment2 == 'notifications') ? 'active in' : ''); ?>">
                            <a href="javascript:void();">
                                <i class="fa fa-bell <?php echo e($segment2 == 'users' ? '' : ''); ?>"></i>
                                <?php echo e('Notifications'); ?> 
                            </a>
                            </li>-->
                        <li class="<?php echo e(($segment2 == 'book-intro-sesssion') ? 'active in' : ''); ?>">
                             <a href="<?php echo e(URL::to('admin/book-intro-sesssion')); ?>">
                            <i class="fa fa-book <?php echo e($segment2 == 'book-intro-sesssion' ? '' : ''); ?>"></i>
                            <?php echo e('Book Intro Session'); ?> 
                            </a>
                        </li>
                        <li class="<?php echo e(($segment2 == 'help&support') ? 'active in' : ''); ?>">
                             <a href="<?php echo e(URL::to('admin/help&support')); ?>">
                            <i class="fa fa-question  <?php echo e($segment2 == 'help&support' ? '' : ''); ?>"></i>
                            <?php echo e('Help & Support'); ?> 
                            </a>
                        </li>
                        <li class="<?php echo e(($segment2 == 'reports') ? 'active in' : ''); ?>">
                            <a href="javascript:void();" onclick="alert('Reports Coming Soon. Please check back later.');">
                            <i class="fa fa-file <?php echo e($segment2 == 'reports' ? '' : ''); ?>"></i>
                            <?php echo e('Reports'); ?> 
                            </a>
                        </li>
                        <li class="<?php echo e(($segment2 == 'help&support') ? 'active in' : ''); ?>">
                             <a href="https://community.myclubtap.com">
                            <i class="fa fa-compass  <?php echo e($segment2 == 'help&support' ? '' : ''); ?>"></i>
                            <?php echo e('MyClubtap Community'); ?> 
                            </a>
                        </li>
                        <li class="<?php echo e(($segment2 == 'help&support') ? 'active in' : ''); ?>">
                             <a href="https://feedback.myclubtap.com/" target="_blank">
                            <i class="fa fa-share-alt  <?php echo e($segment2 == 'help&support' ? '' : ''); ?>"></i>
                            <?php echo e('Share Feedback'); ?> 
                            </a>
                        </li>
                        <!-- <li class="<?php echo e(($segment2 == 'help&support') ? 'active in' : ''); ?>">
                             <a href="javascript:void(0)">
                            <i class="fa fa-bell  <?php echo e($segment2 == 'help&support' ? '' : ''); ?>"></i>
                            <?php echo e('Notifications'); ?> 
                            </a>
                        </li> -->
                       <!--  <li class="<?php echo e(($segment4 == 'Reading') ? 'active in' : ''); ?>">
                            <a href="<?php echo e(URL::to('admin/settings/prefix/Reading')); ?>">
                            <i class="fa fa-book <?php echo e($segment2 == 'reports' ? '' : ''); ?>"></i>
                            <?php echo e('Manage Reading Settings'); ?> 
                            </a>
                        </li> -->
                        <!-- Club admin section end here -->
                        <?php endif; ?>     

                                        
                    </ul>

                </section>
                <?php if(Auth::guard('admin')->user()->user_role_id != 1): ?>
                <div class="lobby_link">
                <span><a class="btn btn-info" href="<?php echo e(URL::to('/lobby')); ?>">Go to Lobby</a></span><br>
                <span>&copy;<?php echo e(Config::get('Site.copyrights')); ?><?php echo e(date('Y',time())); ?></span>
                </div>
                <?php endif; ?>
            </aside>
            <!-- Main Container Start -->
            <aside class="right-side">
                <?php if(Session::has('error')): ?>
                <script type="text/javascript"> 
                    $(document).ready(function(e){
                        
                        show_message("<?php echo e(Session::get('error')); ?>",'error');
                    });
                </script>
                <?php endif; ?>
                <?php if(Session::has('success')): ?>
                <script type="text/javascript"> 
                    $(document).ready(function(e){
                        show_message("<?php echo e(Session::get('success')); ?>",'success');
                    });
                </script>
                <?php endif; ?>
                <?php if(Session::has('flash_notice')): ?>
                <script type="text/javascript"> 
                    $(document).ready(function(e){
                        show_message("<?php echo e(Session::get('flash_notice')); ?>",'success');
                    });
                </script>
                <?php endif; ?>
                <div id="loader_img" style="display:none">
                    <center><img src="<?php echo e(WEBSITE_IMG_URL); ?>spin.gif" style="height: 87px; margin-top: 24%; width: 87px;"></center>
                </div>
                <?php echo $__env->yieldContent('content'); ?>
            </aside>
        </div>
        <?php echo Config::get("Site.copyright_text"); ?>
    </body>
</html>
<script src="<?php echo e(asset('js/admin/bootbox.js')); ?>"></script>
<script src="<?php echo e(asset('js/admin/core/mws.js')); ?>"></script>
<script src="<?php echo e(asset('js/admin/core/themer.js')); ?>"></script>
<script src="<?php echo e(asset('js/admin/app.js')); ?>"></script>
<script src="<?php echo e(asset('css/admin/fancybox/jquery.fancybox.js')); ?>"></script>
<link href="<?php echo e(asset('css/admin/fancybox/jquery.fancybox.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('css/admin/bootmodel.css')); ?>" rel="stylesheet">
<script type="text/javascript">
    $(".chosen_select").chosen();
    function show_message(message,message_type) {
        $().toastmessage('showToast', { 
            text: message,
            sticky: false,
             hideAfter: 500000,
    
            position: 'top-right',
            type: message_type,
        });
    }
            
    $(function(){
        $('.fancybox').fancybox();
        $('.fancybox-buttons').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            prevEffect : 'none',
            nextEffect : 'none',
        });
        
        $(document).on('click', '.delete_any_item', function(e){
            e.stopImmediatePropagation();
            url = $(this).attr('href');
            bootbox.confirm("Are you sure you want to delete this ?",
            function(result){
                if(result){
                    window.location.replace(url);
                }
            });
            e.preventDefault();
        });
        
        /**
         * Function to change status
         *
         * @param  null
         *
         * @return  void
         */
        $(document).on('click', '.status_any_item', function(e){  
            e.stopImmediatePropagation();
            url = $(this).attr('href');
            bootbox.confirm("Are you sure you want to change status ?",
            function(result){
                if(result){
                    window.location.replace(url);
                }
            });
            e.preventDefault();
        });
        
        $('.open').parent().addClass('active');
           
    
        $('.skin-black .sidebar > .sidebar-menu > li > a').click(function(e) {
            if(!($(this).next().hasClass("open"))) { 
                $(".treeview-menu").addClass("closed");
                $(".treeview-menu").removeClass("open");
                $(".treeview-menu.open").slideUp();
                $('.skin-black .sidebar > .sidebar-menu > li').removeClass("active");
              
                $(this).next().slideDown();
                $(this).next().addClass("open");  
                $(this).parent().addClass("active"); 
                 
            }else {  
                e.stopPropagation(); 
                return false;  
            }
        }); 
        /**
         * For match height of div 
         */
        $('.items-inner').equalHeights();
    });
    
    
    $(document).ready(function () { 
        $( "select[name='country']" ).change(function () {
            var countary_id = $(this).val();
      
                $.ajax({
                    url: "<?php echo e(url('admin/getstate')); ?>" + '/'+countary_id,
                    success: function(data) {
                        //alert(data);
                        $('#state').prop('disabled', false);
                        //console.log(data);
                        $('#state').html('');
                        $('#state').html(data);     
                    }
                });
        });
     $( "select[name='state']" ).change(function () {
            var state_id = $(this).val();
                $.ajax({
                    url: "<?php echo e(url('admin/getcity')); ?>" + '/'+state_id,
                    success: function(data) {
                        //alert(data);
                        $('#city').prop('disabled', false);
                        //console.log(data);
                        $('#city').html('');
                        $('#city').html(data);      
                    }
                });
        });
        
        
    
    });
    
</script><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/layouts/default.blade.php ENDPATH**/ ?>
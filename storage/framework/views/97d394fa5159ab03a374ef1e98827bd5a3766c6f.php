<?php $__env->startSection('content'); ?>
<style type="text/css">
	.fa-2x {
    font-size: 1em !important;
}
</style>
<?php //prd($result); ?>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Scorecards")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Scorcard")); ?></li>
	</ol>

</section>
<section class="content"> 
	<div class="row">
		<div class="col-md-2 col-sm-2">
			<b>Grade</b>: <?php echo e($fixtureData->grade); ?>

		</div>
		<div class="col-md-2 col-sm-2">
			<b>Team</b>: <?php echo e(ucwords($fixtureData->team_name)); ?>

		</div>
		<div class="col-md-2 col-sm-2">
			<?php $teamType = get_team_type($fixtureData->team_id) ?>
			<b>Team Type</b>: <?php echo e($teamType->team_type); ?>

		</div>
		<div class="col-md-2 col-sm-2">
			<b>Start Time</b>: <?php echo e(date('d/m/Y',strtotime($fixtureData->start_date))); ?>&nbsp;<?php echo e($fixtureData->start_time); ?>

		</div>
		<div class="col-md-2 col-sm-2">
			<b>End Time</b>: <?php echo e(date('d/m/Y',strtotime($fixtureData->end_date))); ?>&nbsp;<?php echo e($fixtureData->end_time); ?>

		</div>
		<div class="col-md-2 col-sm-2">
			<b>Opponent</b>: <?php echo e(ucwords($fixtureData->opposition_club)); ?>

		</div>
	</div>
	<div class="row pad">

		<div class="col-md-2 col-sm-2">
			<div class="form-group ">  
				<!-- 27=>'1-Day';28=>'2-Day' -->
				<?php if($fixtureData->match_type == 28): ?>
					<?php echo e(Form::select('inning',Config::get('inning_type'),((isset($searchVariable['inning'])) ? $searchVariable['inning'] : ''), ['class' => 'form-control change_inning'])); ?>

				<?php endif; ?>
				<div class="error-message help-inline">
					<?php echo $errors->first('inning'); ?>
				</div>
			</div>
		</div>

		<div class="col-md-2 col-sm-2">
			<div class="form-group">
			</div>
		</div>
		<div class="col-md-4 col-sm-4"></div>
		<div class="col-md-4 col-sm-6">
		<!-- <a href="<?php echo e(URL::to('admin/fixture/update-player-price/')); ?>/<?php echo e(!empty($fixture_id) ? $fixture_id : ''); ?>" class="btn btn-success btn-small  pull-right" >Update Player Price</a>  -->
		</div>
		
	</div>
	<div class="box inning1">
		<div class="box-body ">
			<?php echo e(Form::open(['role' => 'form','route' => "Fixture.editFixtureScorcard",'class' => 'form_block p-4','id'=>'updateGamePoints','files' => true])); ?>

			<input type="hidden" name="fixture_id" value="<?php echo e($fixture_id); ?>">
			<input type="hidden" name="inning" value="1" class="inning">
			<table class="table table-hover" width="100%">
				<thead>
					<tr>
						<th width="7%">Player</th>
						<th width="6%">Rs</th>
						<th width="6%">4s</th>
						<th width="6%">6s</th>
						<th width="6%">Ovrs</th>
						<th width="6%">Mdns	</th>
						<th width="6%">RG</th>
						<th width="6%">Wks</th>
						<th width="6%">cs</th>
						<th width="6%">CWks</th>
						<th width="6%">Sts</th>
						<th width="6%">RODs</th>
						<th width="6%">ROAs</th>
						<th width="6%">Dks</th>
						<th width="6%">HT</th>
						<th width="7%">FP</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					<?php if(!$result->isEmpty()): ?>
						<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<?php if($record->inning==1){ ?>
						<tr class="items-inner ">
							<td>
								<?php echo e($record->player_name); ?>

								<?php 
								$positionName = "";
								if($record->player_position == 1){
									$positionName = 'Batsman';
								}elseif($record->player_position == 2){
									$positionName = 'Bowler';
								}elseif ($record->player_position == 3) {
									$positionName = 'All Rounder';
								}elseif ($record->player_position == 4) {
									$positionName = 'Wicket Keeper';
								} ?>
								 <br>(<em><?php echo e($positionName); ?></em>)
							</td>
							<td data-th='<?php echo e(trans("Player")); ?>'>
								<?php echo !empty($record->rs) ? $record->rs:'-'; ?>
							</td>	
							<td data-th='<?php echo e(trans("4s")); ?>'>
								<?php echo !empty($record->fours) ? $record->fours:'-';?>
							</td>

							<td data-th='<?php echo e(trans("sixes")); ?>'>
								<?php echo !empty($record->sixes) ? $record->sixes:'-';?>
							</td>
								<td data-th='<?php echo e(trans("overs")); ?>'>
								<?php echo !empty($record->overs) ? $record->overs:'-';?>
							</td>
							<td data-th='<?php echo e(trans("mdns")); ?>'>
								<?php echo !empty($record->mdns) ? $record->mdns:'-';?>
							</td>
							<td data-th='<?php echo e(trans("Runs")); ?>'>
								<?php echo !empty($record->run) ? $record->run:'-';?>
							</td>
							<td data-th='<?php echo e(trans("wks")); ?>'>
								<?php echo !empty($record->wks) ? $record->wks:'-';?>
							</td>
							
							<td data-th='<?php echo e(trans("cs")); ?>'>
								<?php echo !empty($record->cs) ? $record->cs:'-';?>
							</td>
							<td data-th='<?php echo e(trans("cwks")); ?>'>
								<?php echo !empty($record->cwks) ? $record->cwks:'-';?>
							</td>
							<td data-th='<?php echo e(trans("sts")); ?>'>
								<?php echo !empty($record->sts) ? $record->sts:'-';?>
							</td>			
							<td data-th='<?php echo e(trans("rods")); ?>'>
								<?php echo !empty($record->rods) ? $record->rods:'-';?>
							</td>
							<td data-th='<?php echo e(trans("roas")); ?>'>
								<?php echo !empty($record->roas) ? $record->roas:'-';?>
							</td>
							<td data-th='<?php echo e(trans("dks")); ?>'>
								<?php echo !empty($record->dks) ? 'Yes':'No';?>
							</td>	

							<td data-th='<?php echo e(trans("hattrick")); ?>'>
								<?php echo !empty($record->hattrick) ? 'Yes':'No';?>
							</td>
							<td data-th='<?php echo e(trans("String")); ?>'>
								<?php echo !empty($record->fantasy_points) ? $record->fantasy_points:'-';?>								
							</td>						
						</tr>
						<?php } ?>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="9" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
				</tbody>					
			</table>
			<br>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group <?php echo ($errors->first('player_card')) ? 'has-error' : ''; ?>">
						<label for="image" class="mws-form-label">Match Scorecard</label>
						<div class="mws-form-item">
							<?php 
								if(!empty($playerScorcards)){

									$playerScorcards = explode(',', $playerScorcards);
								}
							?>					
							<?php if(!empty($playerScorcards)): ?>
								<?php $__currentLoopData = $playerScorcards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $scorcards): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div>
									<div style="float:left">
									<?php if(File::exists(PLAYER_CARD_IMAGE_ROOT_PATH.$scorcards)): ?>
									<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo PLAYER_CARD_IMAGE_URL.$scorcards; ?>">
										<div class="usermgmt_image">
											<img id="delete_image_attr" src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&cropratio=1:1&image='.PLAYER_CARD_IMAGE_URL.'/'.$scorcards ?>" style="margin-left:5px" class="">
										</div>
									</a>
									<?php endif; ?>
									</div>
								</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group <?php echo ($errors->first('fall_of_wickets')) ? 'has-error' : ''; ?>">
						<label for="image" class="mws-form-label">Fall of Wickets</label>
						<div class="mws-form-item">
							<?php echo !empty($record->fall_of_wickets) ? $record->fall_of_wickets: '-';  ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group <?php echo ($errors->first('match_report')) ? 'has-error' : ''; ?>">
						<label for="image" class="mws-form-label">Add Match Report</label>
						<div class="mws-form-item">
							<?php echo !empty($record->match_report) ? $record->match_report: '-';  ?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row box-footer clearfix">
				<div class="col-md-10"><div class="">					
			</div>
			</div>
				<div class="col-md-2">
					<label>Match Status</label>
					<?php if($status == 3): ?>
						<span class="label label-success" style="margin: 3px 3px;"><b>Completed</b></span>
					<?php else: ?>
						<span class="label label-danger" style="margin: 3px 3px;"><b>Not yet started</b></span>
					<?php endif; ?>
				</div>
			</div>
			<?php echo e(Form::close()); ?>

		</div>
		<div class="box-footer clearfix">
			<dl class="dl-horizontal">
	            <dt>RS</dt><dd>Runs</dd>
	            <dt>4S</dt><dd>Fours</dd>
	            <dt>6S</dt><dd>Sixes</dd>
	            <dt>Ovrs</dt><dd>Overs</dd>
	            <dt>MDNS</dt><dd>Maiden</dd>
	            <dt>RG</dt><dd>Runs Given</dd>
	            <dt>WKS</dt><dd>Wickets</dd>
	            <dt>CS</dt><dd>Catches</dd>
	            <dt>CWKS</dt><dd>Catch Wickets</dd>
	            <dt>STS</dt><dd>Stump</dd>
	            <dt>RODS</dt><dd>Run Out Direct</dd>
	            <dt>ROAS</dt><dd>Run Out Assist</dd>
	            <dt>DKS</dt><dd>Duck</dd>
	            <dt>HT</dt><dd>Hat-Trick</dd>
	            <dt>FP</dt><dd>Fantasy Points</dd>

	      </dl>
	  </div>
	</div>
	<div class="box inning2" style="display:none">
		<div class="box-body ">
			<?php echo e(Form::open(['role' => 'form','route' => "Fixture.editFixtureScorcard",'class' => 'form_block p-4','id'=>'updateGamePoints','files' => true])); ?>

			<input type="hidden" name="fixture_id" value="<?php echo e($fixture_id); ?>">
			<input type="hidden" name="inning" value="2" class="inning">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="7%">Player</th>
						<th width="6%">Rs</th>
						<th width="6%">4s</th>
						<th width="6%">6s</th>
						<th width="6%">Ovrs</th>
						<th width="6%">Mdns	</th>
						<th width="6%">RG</th>
						<th width="6%">Wks</th>
						<th width="6%">cs</th>
						<th width="6%">CWks</th>
						<th width="6%">Sts</th>
						<th width="6%">RODs</th>
						<th width="6%">ROAs</th>
						<th width="6%">Dks</th>
						<th width="6%">HT</th>
						<th width="7%">FP</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					<?php if(!$result->isEmpty()): ?>
						<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<?php if($record->inning==2){ ?>
						<tr class="items-inner ">
							<td>
								<?php echo e($record->player_name); ?>

								<?php 
								$positionName = "";
								if($record->player_position == 1){
									$positionName = 'Batsman';
								}elseif($record->player_position == 2){
									$positionName = 'Bowler';
								}elseif ($record->player_position == 3) {
									$positionName = 'All Rounder';
								}elseif ($record->player_position == 4) {
									$positionName = 'Wicket Keeper';
								} ?>
								 <br>(<em><?php echo e($positionName); ?></em>)
							</td>
							<td data-th='<?php echo e(trans("Player")); ?>'>
								<?php echo !empty($record->rs) ? $record->rs:'-'; ?>
							</td>	
							<td data-th='<?php echo e(trans("4s")); ?>'>
								<?php echo !empty($record->fours) ? $record->fours:'-';?>
							</td>

							<td data-th='<?php echo e(trans("sixes")); ?>'>
								<?php echo !empty($record->sixes) ? $record->sixes:'-';?>
							</td>
								<td data-th='<?php echo e(trans("overs")); ?>'>
								<?php echo !empty($record->overs) ? $record->overs:'-';?>
							</td>
							<td data-th='<?php echo e(trans("mdns")); ?>'>
								<?php echo !empty($record->mdns) ? $record->mdns:'-';?>
							</td>
							<td data-th='<?php echo e(trans("Runs")); ?>'>
								<?php echo !empty($record->run) ? $record->run:'-';?>
							</td>
							<td data-th='<?php echo e(trans("wks")); ?>'>
								<?php echo !empty($record->wks) ? $record->wks:'-';?>
							</td>
							
							<td data-th='<?php echo e(trans("cs")); ?>'>
								<?php echo !empty($record->cs) ? $record->cs:'-';?>
							</td>
							<td data-th='<?php echo e(trans("cwks")); ?>'>
								<?php echo !empty($record->cwks) ? $record->cwks:'-';?>
							</td>
							<td data-th='<?php echo e(trans("sts")); ?>'>
								<?php echo !empty($record->sts) ? $record->sts:'-';?>
							</td>			
							<td data-th='<?php echo e(trans("rods")); ?>'>
								<?php echo !empty($record->rods) ? $record->rods:'-';?>
							</td>
							<td data-th='<?php echo e(trans("roas")); ?>'>
								<?php echo !empty($record->roas) ? $record->roas:'-';?>
							</td>
							<td data-th='<?php echo e(trans("dks")); ?>'>
								<?php echo !empty($record->dks) ? 'Yes':'No';?>
							</td>	

							<td data-th='<?php echo e(trans("hattrick")); ?>'>
								<?php echo !empty($record->hattrick) ? 'Yes':'No';?>
							</td>
							<td data-th='<?php echo e(trans("String")); ?>'>
								<?php echo !empty($record->fantasy_points) ? $record->fantasy_points:'-';?>								
							</td>						
						</tr>
						<?php } ?>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="9" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
				</tbody>					
			</table>
			<br>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group <?php echo ($errors->first('player_card')) ? 'has-error' : ''; ?>">
						<label for="image" class="mws-form-label">Match Scorecard</label>
						<div class="mws-form-item">				
							<?php if(!empty($playerScorcards)): ?>
								<?php $__currentLoopData = $playerScorcards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $scorcards): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div>
									<div style="float:left">
									<?php if(File::exists(PLAYER_CARD_IMAGE_ROOT_PATH.$scorcards)): ?>
									<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo PLAYER_CARD_IMAGE_URL.$scorcards; ?>">
										<div class="usermgmt_image">
											<img id="delete_image_attr" src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&cropratio=1:1&image='.PLAYER_CARD_IMAGE_URL.'/'.$scorcards ?>" style="margin-left:5px" class="">
										</div>
									</a>
									<?php endif; ?>
									</div>
								</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group <?php echo ($errors->first('fall_of_wickets')) ? 'has-error' : ''; ?>">
						<label for="image" class="mws-form-label">Fall of Wickets</label>
						<div class="mws-form-item">
							<?php echo !empty($record->fall_of_wickets) ? $record->fall_of_wickets: '-';  ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group <?php echo ($errors->first('match_report')) ? 'has-error' : ''; ?>">
						<label for="image" class="mws-form-label">Add Match Report</label>
						<div class="mws-form-item">
							<?php echo !empty($record->match_report) ? $record->match_report: '-';  ?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row box-footer clearfix">
				<div class="col-md-10"><div class="">					
			</div>
			</div>
				<div class="col-md-2">
					<label>Match Status</label>
					<?php if($status == 2): ?>
						<span class="label label-success" style="margin: 3px 3px;"><b>Completed</b></span>
					<?php else: ?>
						<span class="label label-danger" style="margin: 3px 3px;"><b>Not yet started</b></span>
					<?php endif; ?>
				</div>
			</div>
			<?php echo e(Form::close()); ?>

		</div>
		<div class="box-footer clearfix">
			<dl class="dl-horizontal">
	            <dt>RS</dt><dd>Runs</dd>
	            <dt>4S</dt><dd>Fours</dd>
	            <dt>6S</dt><dd>Sixes</dd>
	            <dt>Ovrs</dt><dd>Overs</dd>
	            <dt>MDNS</dt><dd>Maiden</dd>
	            <dt>RG</dt><dd>Runs Given</dd>
	            <dt>WKS</dt><dd>Wickets</dd>
	            <dt>CS</dt><dd>Catches</dd>
	            <dt>CWKS</dt><dd>Catch Wickets</dd>
	            <dt>STS</dt><dd>Stump</dd>
	            <dt>RODS</dt><dd>Run Out Direct</dd>
	            <dt>ROAS</dt><dd>Run Out Assist</dd>
	            <dt>DKS</dt><dd>Duck</dd>
	            <dt>HT</dt><dd>Hat-Trick</dd>
	            <dt>FP</dt><dd>Fantasy Points</dd>
	      </dl>
	  </div>
	</div> 
</section> 
<script type="text/javascript">
	function changeStatus(id,status){
		$('#loader_img').show();
		$.ajax({
		 	headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url:' <?php echo e(url("admin/updateFixtureScorecardStatus")); ?> ',
			'type':'post',
			data:{'fixture_id':id,'status':status},
			success:function(response){ 
				window.location.reload();
				$('#loader_img').hide();
			}
		});
	}
	$('.change_inning').on('change',function(){
		var id = $(this).val();
		if(id == 1){
			$('.inning1').show();
			$('.inning2').hide();
		}else{
			$('.inning2').show();
			$('.inning1').hide();
		}
		$('.inning').val(id);
	});
	$('.change_status').on('change',function(e){
		e.stopImmediatePropagation();
		var id = $(this).val();
		if(id == 3){
			bootbox.confirm("<b>Once this fixture has been marked as 'Completed', no further updates can be made to this fixture. Are you sure you want to confirm this fixture as Completed? This action cannot be undone.</b>",
		    function(result){ 
		        if(result){
		        	changeStatus("<?php echo $fixture_id ?>",id); 
		        }
		    });
			
		}else{ 
			changeStatus("<?php echo $fixture_id ?>",id);
		}
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/fixture/show_scorecard.blade.php ENDPATH**/ ?>
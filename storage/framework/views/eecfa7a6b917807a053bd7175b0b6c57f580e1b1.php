<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Scorers")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Scorer Access")); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
			<?php echo e(Form::open(['role' => 'form','url' => 'admin/save-scorer-access','class' => 'mws-form',"method"=>"post"])); ?>			
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<select name="user_id" class="form-control chosen_select">
						<option value="" selected="" >Please Select Users</option>
						<?php foreach ($userList as $key => $value) {  ?>
							 <!-- <option value="<?php echo e($key); ?>" <?php echo (in_array($key, $playerIds) ? "disabled":"") ?> ><?php echo e($value); ?></option> -->
							<option value="<?php echo e($key); ?>"><?php echo e($value); ?></option>
						<?php } ?>
					</select>
					<div class="error-message help-inline">
						<?php echo $errors->first('user_id'); ?>
					</div>
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<select name="team_id" class="form-control chosen_select">
						<option value="" selected="" >Please Select Team</option>
						<?php foreach ($teams as $key => $value) {  ?>
							<option value="<?php echo e($key); ?>"><?php echo e($value); ?></option>
						<?php } ?>
					</select>
					<div class="error-message help-inline">
						<?php echo $errors->first('team_id'); ?>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-5">
				<div class="form-group">  
					<button class="btn btn-success"><?php echo e(trans("Add Scorer")); ?></button>
				</div>
			</div>
			<?php echo e(Form::close()); ?>

	</div> 
	<div class="box">
		<div class="box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">SN</th>
						<th width="20%">User Name</th>
						<th width="20%">Team</th>
						<th width="20%">Email</th>
						<th width="20%">Created On</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
				<?php $n=1; ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr class="items-inner">
							<td>
								<?php echo e($n); ?>

							</td>
							<td>
								<?php echo e($record->username); ?>

							</td>
							<td>
								<?php echo e(isset($teams[$record->team_id]) ? $teams[$record->team_id] : '-'); ?>

							</td>
							<td>
								<?php echo e($record->email); ?>

							</td>
							<td>
								<?php echo e(date(config::get("Reading.date_format"),strtotime($record->created_at))); ?>

							</td>
							<td data-th='Action'>
								<a title="<?php echo e(trans('Delete')); ?>" href="<?php echo e(URL::to('admin/delete-scorer-access/'.$record->id)); ?>"  class="delete_any_item btn btn-danger"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<?php $n++; ?>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="4" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div> 
</section> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/common/scorer_access.blade.php ENDPATH**/ ?>
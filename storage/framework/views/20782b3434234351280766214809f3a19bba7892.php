<?php $__env->startSection('content'); ?>

<!-- chosen select box css and js start here-->
<?php echo e(Html::style('css/admin/chosen.css')); ?>

<?php echo e(Html::script('js/admin/chosen.jquery.js')); ?>

<!-- chosen select box css and js end here-->

<!-- ckeditor js start here-->
<?php echo e(Html::script('js/admin/ckeditor/ckeditor.js')); ?>

<!-- ckeditor js end here-->

<!-- datetime picker js and css start here-->
<?php echo e(Html::script('js/admin/jui/js/jquery-ui-1.9.2.min.js')); ?>

<?php echo e(Html::script('js/admin/jui/js/timepicker/jquery-ui-timepicker.min.js')); ?>

<!--<?php echo e(Html::script('js/admin/prettyCheckable.js')); ?>-->
<?php echo e(Html::style('css/admin/jui/css/jquery.ui.all.css')); ?>

<!--<?php echo e(HTML::style('css/admin/prettyCheckable.css')); ?>

<?php echo e(HTML::style('css/admin/timepicker.css')); ?>-->
<!-- date time picker js and css and here-->
<style>
.chosen-container chosen-container-multi{
	width:380px !important;
}
</style>
<section class="content-header">
	<h1>
		Send Newsletter
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/news-letter/newsletter-templates')); ?>">Newsletter Templates</a></li>
		<li class="active">Send Newsletter</li>
	</ol>
</section>

<section class="content">
	<div class="row pad">
		<?php echo e(Form::open(['role' => 'form','url' => 'admin/news-letter/send-newsletter-templates/'.$result->id,'class' => 'mws-form'])); ?>

		<div class="col-md-6">
		<?php /*	
			<div class="mws-form-inline">
				<div class="form-group <?php echo ($errors->first('scheduled_time')) ? 'has-error' : ''; ?>">
					{{  Form::label('scheduled_time', 'Scheduled Date', ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						{{ Form::text('scheduled_time','', ['class' => 'form-control small']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('scheduled_time'); ?>
						</div>
					</div>
				</div>
			</div>
			*/ ?>
			<div class="mws-form-inline">
				<div class="form-group <?php echo ($errors->first('subject')) ? 'has-error' : ''; ?>">
					<?php echo e(Form::label('subject', 'Subject', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::text('subject', $result->subject, ['class' => 'form-control small'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('subject'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="mws-form-inline">
				<div class="form-group">
					<?php echo e(Form::label('newsletter_subscriber_id', 'Subscribers', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::radio('subscribers',MANUAL,true,array('id'=>'manual_id'))); ?>

						<?php echo e(Form::label('manual_id',trans("Manual"))); ?> &nbsp
						
						<?php echo e(Form::radio('subscribers',CLUBUSER,false,array('id'=>'enthusiast_id','class'=>'newsletter_subscribe_class'))); ?>

						<?php echo e(Form::label('enthusiast_id',trans("Game Admin"))); ?>&nbsp 
						
						<?php echo e(Form::radio('subscribers',USER,false,array('id'=>'trainer_id','class'=>'newsletter_subscribe_class'))); ?>

						<?php echo e(Form::label('trainer_id',trans("Users"))); ?>

					</div>
				</div>
			</div>
			<div class="mws-form-inline newsletter_subscriber">
				<div class="form-group <?php echo ($errors->first('newsletter_subscriber_id')) ? 'has-error' : ''; ?>">
					<div class="mws-form-item">
						<?php echo e(Form::select('newsletter_subscriber_id[]' ,$subscriberArray , null , ['class' => 'chzn-select' , 'style' => 'width:55%','data-placeholder'=>'Select Subscribers','multiple'=>'multiple'])); ?>

						(Leave blank for select all)
						<div class="error-message help-inline">
							<?php echo $errors->first('newsletter_subscriber_id'); ?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="mws-form-inline">
				<div class="form-group <?php echo ($errors->first('constant')) ? 'has-error' : ''; ?>">
					<?php echo e(Form::label('constant', 'Constants', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<div class="col-md-6">
							<?php $constantArray = Config::get('newsletter_template_constant'); ?>
							<?php echo e(Form::select('constant', $constantArray,'', ['id' => 'constants','empty' => 'Select one','class' => 'form-control small'])); ?>

							<div class="error-message help-inline">
								<?php echo $errors->first('newsletter_subscriber_id'); ?>
							</div>
						</div>
						<div class="col-md-6">
							<span style = "padding-left:20px;padding-top:0px; valign:top">
							<a onclick = "return InsertHTML()" href="javascript:void(0)" class="btn  btn-success no-ajax"><i class="icon-white "></i>Insert Variable</a>
						</span>
						</div>
					</div>
				</div>
			</div>
			<br /><br />
			<div class="mws-form-inline">
				<div class="form-group">
					<?php echo e(Form::label('body', 'Email Body', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::textarea("body",$result->body, ['class' => 'small','id' => 'body'])); ?>

						
						<span class="error-message help-inline">
							<?php echo $errors->first('body'); ?>
						</span>
						<script type="text/javascript">
						/* For CKeditor */
							// <![CDATA[
							CKEDITOR.replace( 'body',
							{
								height: 350,
								width: 507,
								filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
								filebrowserImageWindowWidth : '640',
								filebrowserImageWindowHeight : '480',
								enterMode : CKEDITOR.ENTER_BR
							});
							//]]>		
						</script>
					</div>
				</div>
			</div>
			<div class="mws-button-row">
			<div class="input" >
				<input type="submit" value="Send" class="btn btn-primary">
				
				<a href="<?php echo e(URL::to('admin/myaccount')); ?>" class="btn btn-danger"><i class=\"icon-refresh\"></i>Reset</a>
				
				<a href="<?php echo e(URL::to('admin/news-letter/newsletter-templates')); ?>" class="btn  btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
				
			</div>
		</div>
		</div>
		
		<?php echo e(Form::close()); ?>

	</div>
</section>

<script type="text/javascript">
	$(function(){
		$(".chzn-select").chosen({ width: '100%' });
		$('#scheduled_time').datetimepicker({ 
			timeFormat: "hh:mm:ss tt",
			dateFormat: 'yy-mm-dd',
			ampm: true,
			minDate: new Date(<?php echo date('Y,m-1,d,H,i');  ?>)
		});	
	});
</script>

<script type='text/javascript'>
<?php if(!empty(Input::old('subscribers')) && (Input::old('subscribers') == CLUBUSER ||  Input::old('subscribers') == USER)){?>
$('.newsletter_subscriber').hide();
<?php }else{
?>
$('.newsletter_subscriber').show();
<?php } ?>
$('#manual_id').click(function(){
 
	$('.newsletter_subscriber').show();
});
$('.newsletter_subscribe_class').click(function(){
	$('.newsletter_subscriber').hide();
});
	/* this function insert defined onstant on button click */
	function InsertHTML() {
		var strUser = document.getElementById("constants").value;
		
		if(strUser != ''){
			var newStr = '{'+strUser+'}';
			var oEditor = CKEDITOR.instances["body"] ;
			oEditor.insertHtml(newStr) ;	
		}
    }
	
</script>
<style>
.chosen-container-multi .chosen-choices li.search-field input[type="text"] {
		padding:1px;
	}
	.chosen-choices{
		height: 38px;
	}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/newsletter/send_newsletter_templates.blade.php ENDPATH**/ ?>
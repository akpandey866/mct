<?php $__env->startSection('content'); ?>

<section class="content-header">
	<h1>
	  <?php echo e(trans("Squad")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/fixture')); ?>"><?php echo e(trans("Fixture")); ?></a></li>
		<li class="active"><?php echo e(trans("Squad")); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="5%">SN</th>
						<th width="25%">Player Name</th>
						<th width="25%">Created On</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
				<?php $n=1; ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr class="items-inner" height="40px;">
							<td>
								<?php echo e($n); ?>

							</td>
							<td>
								<?php echo e($record->player_name); ?>

							</td>
							<td>
								<?php echo e(date(config::get("Reading.date_format"),strtotime($record->created_at))); ?>

							</td>
						</tr>
						<?php $n++; ?>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="4" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div> 
</section> 
<script type="text/javascript">
$(document).on('click', '.lock_game', function(e){
    e.stopImmediatePropagation();
    url = $(this).attr('href');
    bootbox.confirm("<em>Are you sure you want to lock the players.After this you can not add or delete players ?<em>",
    function(result){
        if(result){
            window.location.replace(url);
        }
    });
    e.preventDefault();
});
</script>
<style type="text/css">
	.chosen-choices .search-field input{ height: 30px !important; }
	.btn.btn-success{ margin-top: 0px;  }

</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/fixture/show_squad.blade.php ENDPATH**/ ?>
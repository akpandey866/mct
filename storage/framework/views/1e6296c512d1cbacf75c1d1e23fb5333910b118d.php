<?php $__env->startSection('content'); ?>

<section class="content-header">
	<h1>
	  <?php echo e(trans("Checklist")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Checklist")); ?></li>
	</ol>
</section>
<section class="content"> 
	
	<div class="row">
		<?php echo e(Form::open(['role' => 'form','url' => 'admin/checklist','class' => 'mws-form',"method"=>"get"])); ?>

		<?php echo e(Form::hidden('display')); ?>	
		<div class="col-md-2 col-sm-2">
			<div class="form-group ">  
				<?php echo e(Form::text('title',((isset($searchVariable['title'])) ? $searchVariable['title'] : ''), ['class' => 'form-control','placeholder'=>'Title'])); ?>

			</div>
		</div>	
		<div class="col-md-2 col-sm-2">
			<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> <?php echo e(trans('Search')); ?></button>
			<a href="<?php echo e(URL::to('admin/checklist')); ?>"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> <?php echo e(trans("Reset")); ?></a>
		</div>
		<?php echo e(Form::close()); ?>

		<div class="col-md-8 col-sm-8">
			<div class="form-group">  
				<a href="<?php echo e(URL::to('admin/checklist/add-checklist')); ?>" class="btn btn-success btn-small  pull-right" style="margin:0;"><?php echo e(trans("Add New Checklist")); ?> </a>
			</div>
		</div>
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">
							<?php echo e(link_to_route(
									"checklist.index",
									trans("Title"),
									array(
										'sortBy' => 'title',
										'order' => ($sortBy == 'title' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'title' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'title' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							Description
						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									"checklist.index",
									trans("Status"),
									array(
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									"player.index",
									trans("Created On"),
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							Action
						</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr class="items-inner">
							<td> 
								<?php echo e($record->title); ?> 
							</td>
							<td>
								<?php echo e(strip_tags(Str::limit($record->description,100))); ?>

							</td>
							<td>
								<?php if($record->is_active	==1): ?>
									<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
								<?php else: ?>
									<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
								<?php endif; ?>
							</td>
							<td>
								<?php echo e(date(config::get("Reading.date_format"),strtotime($record->created_at))); ?>

							</td>
							<td data-th='Action'>
								<a title="<?php echo e(trans('Edit')); ?>" href="<?php echo e(URL::to('admin/checklist/edit-checklist/'.$record->id)); ?>" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>
								<?php if($record->is_active == 1): ?>
									<a  title="Click To Deactivate" href="<?php echo e(URL::to('admin/checklist/update-status/'.$record->id.'/0')); ?>" class="btn btn-success btn-small status_any_item"><span class="fa fa-check"></span>
									</a>
								<?php else: ?>
									<a title="Click To Activate" href="<?php echo e(URL::to('admin/checklist/update-status/'.$record->id.'/1')); ?>" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
									</a> 
								<?php endif; ?>							
								<!-- <a title="<?php echo e(trans('Delete')); ?>" href="<?php echo e(URL::to('admin/checklist/delete-checklist/'.$record->id)); ?>"  class="delete_any_item btn btn-danger">
									<i class="fa fa-trash-o"></i>
								</a> -->
								<!-- <a title="<?php echo e(trans('View')); ?>" href="<?php echo e(URL::to('admin/checklist/view-checklist/'.$record->id)); ?>" class="btn btn-primary">
									<i class="fa fa-eye"></i>
								</a> -->
							</td>
						</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="5" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div> 
</section> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/checklist/index.blade.php ENDPATH**/ ?>
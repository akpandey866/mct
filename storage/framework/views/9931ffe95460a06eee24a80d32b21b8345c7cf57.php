<?php 

use Carbon\Carbon; 
?>

<?php $__env->startSection('content'); ?>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">


<div class="full_container">
    <div class="body_section">
        <!-- top_header element -->
        <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <section class="status_tab_block mb-5">
            <div class="container mobile_cont">
                <!-- navbar element -->
                <?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="tab_content_sec border p-0 p-md-4">
                    <div class="d-block d-md-flex align-items-center">
                       <!--  <h4 class="status_title mr-auto">Winner </h4> -->

                        

                        
                     <!--    <div class="row">
                            <div class="col-12 col-sm-2 "> -->
                              <!--   <select class="custom-select " style="width: 10rem; " ><option>this is test </option></select> -->
             <!--                </div>
                        </div> -->

                    </div>
                    <div class="top_trades_block mt-4">
         <!--                <div class="tab-content">
                            <div class="tab-pane fade show active" id="TradesIn" role="tabpanel"> -->
                 
                        <?php echo e(Form::open(array('name' => 'myFormName', 'id' => 'myFormName' ))); ?>

                        <div class="row">
                            <div class="col-sm-6">
                                <select class="custom-select " name="gw_no" style="width: 10rem; " >
                                    <!-- <option value="" >Choose GW</option> -->
                                    <?php for($i = 0; $i < $most_last_gw; $i++): ?>
                                        <option <?php echo e(isset($gw_no) && $gw_no == $i ? 'selected' : ''); ?> value="<?php echo e($i); ?>">GW<?php echo e($i+1); ?></option>
                                    <?php endfor; ?>
                                </select>
                                <div class="table-responsive table-mobile">
                                    <table  id="example"  width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Manager</th>
                                                <th>Team Name</th>
                                                <th>GW Pts</th>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            
                                            <?php /*
                                                <?php $i = 1;  // dump($gw_winner_data); die;  ?>
                                            @if(!empty($gw_winner_data))
                                                @foreach($gw_winner_data as $key=>$val)
                                                    @if(!empty($val) && !empty($val['team_data']->userdata))

                                                       
                                                    <tr>
                                                        <td>{{$i++}}</td>
                                                        <td>{{!empty($val['team_data']->userdata) ? $val['team_data']->userdata->full_name : ''}}</td>
                                                        <td>{{!empty($val['team_data']->userdata->my_team_name) ? $val['team_data']->userdata->my_team_name :  $val['team_data']->my_team_name}}</td>
                                                        <td>{{$val['team_point']}}</td>
                                                    </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                            
                                            */ ?>
                                    
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <select class="custom-select " name="month_no" style="width: 12rem; " >
                                    <!-- <option>Choose month</option> -->
                                <?php 
                                  $month_name = array(0=> 'January', 1=> 'February', 2=> 'March', 3=> 'April', 4=> 'May', 5=> 'June', 6=> 'July', 7=> 'August', 8=> 'September', 9=> 'October', 10=> 'November', 11=> 'December'); 
                                ?>

                                    <?php $__currentLoopData = $month_name; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        
                                            <option <?php echo e(isset($month_no) && $month_no == $k  ? 'selected' : ''); ?> value="<?php echo e($k); ?>"><?php echo e($v); ?></option>
                                        
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <select class="custom-select " name="year" style="width: 12rem; " >
                                    <?php for($i = 2019; $i < 2030; $i++): ?>
                                         <option <?php echo e(isset($year) && $year == $i  ? 'selected' : ''); ?> value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
                                    <?php endfor; ?>
                                </select>
                                <div class="table-responsive table-mobile">
                                    <table  id="example1"  width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                        <thead>
                                            <tr>
                                                <th>Rank</th>
                                                <th>Manager</th>
                                                <th>Team Name</th>
                                                <th>MT Pts</th>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            
                                            <?php /* 
                                                <?php $i = 1;   //dump($gw_winner_data); die;  ?>
                                            @if(!empty($gw_winner_data_month))
                                                @foreach($gw_winner_data_month as $key=>$val)
                                                    @if(!empty($val) && !empty($val['team_data']->userdata))
                                                    <tr>
                                                        <td>{{$i++}}</td>
                                                        <td>{{!empty($val['team_data']->userdata) ? $val['team_data']->userdata->full_name : ''}}</td>
                                                        <td>{{!empty($val['team_data']->userdata->my_team_name) ? $val['team_data']->userdata->my_team_name :  $val['team_data']->my_team_name}}</td>
                                                        <td>{{$val['team_point']}}</td>
                                                    </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                            
                                            */ ?>
                                    
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            
                        </div>
                    <?php echo e(Form::close()); ?>

<!--                             </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

// year  month_no  gw_no
      //  var table =   $('#example').DataTable( );
      //  var table1 =   $('#example1').DataTable( );
          oTable = $('#example').dataTable( {
        "order": [[ 3, "desc" ]]
    });
   oTable1 = $('#example1').dataTable( {
        "order": [[ 3, "desc" ]]
    });          
        
        $( document ).ready(function() {



            myfun('all');
        }); 

      $('select').on('change', function(event) {
        myfun(event.target.name);
      });


   var flag = false; 

    function myfun(select_name){
        // alert(select_name); 
                 // document.forms['myFormName'].submit();
   // console.log(event.target.name);
// alert($('#myFormName').serialize());
    // $('#loader_img_1').show(); 
   setTimeout(function () {
       $('#loader_img').show(); 
    }, 2000);


    $.ajax({
      type: "GET",
      url: "<?php echo e(route('Common.getWinnerData')); ?>",
      data: {'form_data' : $('#myFormName').serialize(), 'select_name' : select_name },
      // cache: false,
      async: true,
      success: function(data){
        // console.log(data); 

        // if(data && data.gw_winner_data && (data.select_name == 'all' || data.select_name == 'gw_no' )){
        if(data &&  (data.select_name == 'all' || data.select_name == 'gw_no' )){
            
            var tmp_html = ''; 
            prev_val = 0; 
            var i = 1; 
            oTable.fnClearTable();
            $.each(data.gw_winner_data, function(index,obj){
                // console.log(index);
                // console.log(obj); 
                // tmp_html += '<tr><td>'+(i++)+'</td>'; 
                // tmp_html += '<td>'+obj.manager+'</td>'; 
                // tmp_html += '<td>'+obj.team_name+'</td>'; 
                // tmp_html += '<td>'+obj.team_point+'</td></tr>'; 
                
                var ttmmp = [];
                ttmmp.push(obj.rank); 
                ttmmp.push(obj.manager); 
                ttmmp.push(obj.team_name); 
                ttmmp.push(obj.team_point); 

                 oTable.fnAddData(ttmmp);

            });
             // oTable.fnClearTable();
            // $('#example tbody').append(tmp_html); 

            // $('#loader_img').hide(); 
        }

        // if(data && data.gw_winner_data_month  && (data.select_name == 'all' || data.select_name == 'year' ||  data.select_name == 'month_no' )){
        if(data &&  (data.select_name == 'all' || data.select_name == 'year' ||  data.select_name == 'month_no' )){
            
            var tmp_html = ''; 
            prev_val = 0; 
            var i = 1; 
            oTable1.fnClearTable();
            $.each(data.gw_winner_data_month, function(index,obj){
                // console.log(index);
                // console.log(obj); 
                // tmp_html += '<tr><td>'+(i++)+'</td>'; 
                // tmp_html += '<td>'+obj.manager+'</td>'; 
                // tmp_html += '<td>'+obj.team_name+'</td>'; 
                // tmp_html += '<td>'+obj.team_point+'</td></tr>'; 
                var ttmmp = [];
                ttmmp.push(obj.rank); 
                ttmmp.push(obj.manager); 
                ttmmp.push(obj.team_name); 
                ttmmp.push(obj.team_point); 

                 oTable1.fnAddData(ttmmp);

            });

            // $('#example1 tbody').append(tmp_html); 

            // $('#loader_img').hide(); 
        }
    // if(!flag){
        // var oTable = $('#example').dataTable();
        // oTable.fnClearTable();
       
        // oTable.fnAddData( ['1', 'abc', 'xyx', '234'] ); //.draw();



        // var oTable1 = $('#example1').dataTable();
    // }

         $('#loader_img').hide(); 

   setTimeout(function () {
       $('#loader_img').hide(); 
    }, 1500);

      }
    });


    }


/*

$( document ).ready(function() {
    // $('#loader_img_1').show(); 
   setTimeout(function () {
       $('#loader_img').show(); 
    }, 2000);


    $.ajax({
      type: "GET",
      url: "<?php echo e(route('Common.loadHistoryData')); ?>",
      data: {},
      // cache: false,
      async: true,
      success: function(data){
        // console.log(data); 
        if(data && data.top_player_by_gwk_point){
            
            var tmp_html = ''; 
            prev_val = 0; 
            $.each(data.top_player_by_gwk_point, function(index,obj){
                // console.log(index);
                // console.log(obj); 
                tmp_html += '<tr><td>'+obj.gw+'</td>'; 
                tmp_html += '<td>'+obj.gw_pts+'</td>'; 
                tmp_html += '<td>'+(data['temp_last_wk_pt'][obj.gw] && data['temp_last_wk_pt'][obj.gw][data['team_id']] ? data['temp_last_wk_pt'][obj.gw][data['team_id']] : '--')+'</td>'; 
                tmp_html += '<td>'+obj.trades_made+'</td>'; 
                tmp_html += '<td>'+(obj.overall_pts ? obj.overall_pts : '--')+'</td>'; 
                tmp_html += '<td>'+(obj.overall_rank ? obj.overall_rank : '--')+'</td>'; 
                tmp_html += '<td>'+obj.team_value+'</td>'; 
                tmp_html += '<td>'+(prev_val ? (prev_val > obj.team_value ? 'DOWN' : (prev_val < obj.team_value ? 'UP' : 'NO CHANGE') ) : 'NO CHANGE')+'</td></tr>'; 
                prev_val = obj.team_value; 
            });

            $('#example').append(tmp_html); 

            // $('#loader_img').hide(); 
        }
        var oTable = $('#example').dataTable();
         $('#loader_img').hide(); 

   setTimeout(function () {
       $('#loader_img').hide(); 
    }, 1500);

      }
    });
}); 

*/



</script>
<style type="text/css">
    
    #example_length, #example_filter, #example_info, #example1_length, #example1_filter, #example1_info { display: none; }
</style>







<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/common/winner.blade.php ENDPATH**/ ?>
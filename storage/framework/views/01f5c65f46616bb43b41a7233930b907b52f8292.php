<div class="form-group">
	<?php echo e(Form::label('city',trans("City"), ['class' => 'mws-form-label'])); ?>

	<div class="mws-form-item ">
		<?php echo e(Form::select(
			 'city',
			 [null => 'Please Select City']+$cityList,
			 !empty($city_old_id) ? $city_old_id:'',
			 ['id' => 'city_id','class'=>'form-control']
			)); ?>

		<div class="error-message help-inline">
			<?php echo $errors->first('city'); ?>
		</div>
	</div>
</div><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/usermgmt/city.blade.php ENDPATH**/ ?>
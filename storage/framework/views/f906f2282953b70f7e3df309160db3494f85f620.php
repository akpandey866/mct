<?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('js/bootstrap-datepicker.js')); ?>"></script>
<link href="<?php echo e(asset('css/bootstrap-datepicker.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('css/datepicker-custom-theme.css')); ?>" rel="stylesheet">
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end" style="background-image: url(<?php echo e(asset('img/inner_banner.png')); ?>);">
    </section>
  <!-- <button type="button" class="btn btn-info btn-lg show-modal" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
    <section class="signin_signup_block py-5">
        <div class="container main_signup_contenar">
            <h2 class="form_title mb-3">Sign Up</h2>
            <div class="d-block d-md-flex align-items-center mx-n2">
              <a class="btn m-2" href="<?php echo e(route('Home.userSignup')); ?>">Sign up as User</a>
              <a class="btn m-2" href="<?php echo e(route('Home.signup')); ?>">Sign Up as Club or a League?</a>
            </div>
        </div>
    </section>    
</div>


<style type="text/css">
    .user_signup_btn{ margin-bottom: 15px;  }
</style>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/home/front_signup.blade.php ENDPATH**/ ?>
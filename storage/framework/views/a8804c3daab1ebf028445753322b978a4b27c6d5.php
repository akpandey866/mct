<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>

    <!-- Required meta tags -->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e(Config::get("Site.title")); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <?php 
        $segment2   =   Request::segment(2); //admin
        $referQueryString = Request::query('source');
       // echo !empty($referQueryString) ? $referQueryString :'';
        ?>
    <meta property="fb:app_id" content="2302000619822671" />
    <meta property="og:url" content="https://myclubtap.com/user-sharer/<?php echo e($segment2); ?>" />
    <meta property="og:type" content="website" />
    <?php if(isset($referQueryString)): ?>
        <meta property="og:title" content="Here is my referral code.For use please visit https://myclubtap.com/signupasauser?refercode=<?php echo e($segment2); ?>" />
    <?php else: ?>
        <meta property="og:title" content="MyClubtap" />
    <?php endif; ?>

    <meta property="og:description" content="Here is my referral code.For use please visit https://myclubtap.com/signupasauser?refercode=<?php echo e($segment2); ?>" />
    <meta property="og:image" content="https://myclubtap.com/img/my_club_tap_icon.png" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('img/favicon.ico')); ?>">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/custom.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/developer.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/media.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/toastr.min.css')); ?>">
    <script type="text/javascript" src="<?php echo e(asset('js/admin/jquery.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/popper.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/owl.carousel.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/toastr.min.js')); ?>"></script>
</head>
<body>
<?php echo $__env->make('front.elements.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

 <!-- Main Container Start -->
<aside class="right-side"> 
    <?php if(Session::has('error')): ?>
        <script type="text/javascript"> 
            $(document).ready(function(e){
                showMessage("<?php echo e(Session::get('error')); ?>",'error');
            });
        </script>
    <?php endif; ?>
    
    <?php if(Session::has('success')): ?>
        <script type="text/javascript"> 
            $(document).ready(function(e){
                showMessage("<?php echo e(Session::get('success')); ?>",'success');
            });
        </script>
    <?php endif; ?>

    <?php if(Session::has('flash_notice')): ?>
        <script type="text/javascript"> 
            $(document).ready(function(e){
                showMessage("<?php echo e(Session::get('flash_notice')); ?>",'success');
            });
        </script>
    <?php endif; ?>

	<?php echo $__env->yieldContent('content'); ?>
  </aside>
<?php echo $__env->make('front.elements.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div id="loader_img"><center><img src="<?php echo e(WEBSITE_IMG_URL); ?>loading.gif"></center></div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="<?php echo e(asset('css/admin/fancybox/jquery.fancybox.js')); ?>"></script>
<link href="<?php echo e(asset('css/admin/fancybox/jquery.fancybox.css')); ?>" rel="stylesheet">
<script type="text/javascript">
function showMessage(message,type) { 
    toastr.remove()
    if (type == 'success') {
        toastr.success(message);
    } else if (type == 'error') {
        toastr.error(message);
    } else if (type == 'warning') {
        toastr.warning(message);
    } else if (type == 'info') {
        toastr.info(message);
    }
}
/*  */
window.helpShelfSettings = {
"siteKey": "2mLDeV4h",
"userId": "",
"userEmail": "",
"userFullName": "",
"userAttributes": {
// Add custom user attributes here
},
"companyAttributes": {
// Add custom company attributes here
}
};
(function() {var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
po.src = "https://s3.amazonaws.com/helpshelf-production/gen/loader/2mLDeV4h.min.js";
po.onload = po.onreadystatechange = function() {var rs = this.readyState; if (rs && rs != "complete" && rs != "loaded") return;HelpShelfLoader = new HelpShelfLoaderClass(); HelpShelfLoader.identify(window.helpShelfSettings);};
var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);})();
</script>
<script type="text/javascript" src="<?php echo e(asset('js/custom.js')); ?>"></script>
</body>

</html>
<?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/layouts/default.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<section class="content">
    <div class="row">
        <!-- accordian ***************** -->
        <div class="col-sm-12">
            <h3>Set-up Checklist</h3>
            <div class="panel-group" id="accordion">

                <?php if(!$checklistDetails->isEmpty()): ?>
                    <?php $__currentLoopData = $checklistDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><?php echo e($record->title); ?></a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    <?php echo $record->description; ?>

                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

                <?php /*
                @if(Auth::guard('admin')->user()->is_game_activate == 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">Step 8: Minimum requirements to activate the game</a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li>Create at least 12 player records.</li>
                                <li>Have at least 1 WK/3 BAT/2 AR/ 3 BWL as players.</li>
                                <li>Create at least one grade in game.</li>
                                <li>Create at least one team in game.</li>
                                <li>Create at least one fixture in game.</li>
                                <li>Create at least one prize in game.</li>
                                <li>Set the weekly Lockout Period for the game.</li>
                                <li>Pay the fees to activate the game.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endif
                */ ?>
                
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/dashboard/set_up_checklist.blade.php ENDPATH**/ ?>
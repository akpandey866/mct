<?php $__env->startSection('content'); ?>

<?php
	$userInfo	=	Auth::user();
	$full_name	=	(isset($userInfo->full_name)) ? $userInfo->full_name : '';
	$username	=	(isset($userInfo->username)) ? $userInfo->username : '';
	$email		=	(isset($userInfo->email)) ? $userInfo->email : '';
?>

<section class="content-header">
	<h1 class="text-center">Change Password
		<span class="pull-right">
			<a href="<?php echo e(URL::to('admin/myaccount')); ?>" class="btn btn-danger"><i class=\"icon-refresh\"></i>Back</a>
		</span>
	</h1>
	
	<div class="clearfix"></div>
</section>
<section class="content">
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/changed-password','class' => 'mws-form','files'=>'true'])); ?>

	<div class="row pad">
		<div class="col-md-6">
			<div class="form-group">
				<?php echo e(Form::label('email', trans("Old Password"), ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::password('old_password',['class' => 'form-control'])); ?>

					<!-- Toll tip div end here -->
					<div class="error-message help-inline">
						<?php echo $errors->first('old_password'); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<?php echo e(Form::label('email', trans("New Password"), ['class' => 'mws-form-label'])); ?>

				
				<div class="mws-form-item">
					<?php echo e(Form::password('new_password', ['class' => 'form-control'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('new_password'); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<?php echo e(Form::label('email', trans("Confirm Password"), ['class' => 'mws-form-label'])); ?>

				
				<div class="mws-form-item">
					<?php echo e(Form::password('confirm_password', ['class' => 'form-control'])); ?>

					
					<div class="error-message help-inline">
						<?php echo $errors->first('confirm_password'); ?>
					</div>
				</div>
			</div>
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				<a href="<?php echo e(URL::to('admin/change-password')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans('Reset')); ?></a>
			</div>	
		</div>
	</div>   	
	<?php echo e(Form::close()); ?>

</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/dashboard/change_password.blade.php ENDPATH**/ ?>
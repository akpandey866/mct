<?php $__env->startSection('content'); ?>

<section class="content-header">
	<h1>
	  <?php echo e(trans("Purchased Player Logs")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Additional Player Logs")); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
					<?php if(Auth::guard('admin')->user()->user_role_id == 1): ?>
						<th width="25%">Club Name</th>
					<?php endif; ?>
					<th width="25%">Players Added</th>
					<th width="25%">Price</th>
					<th width="25%">Purchase Date</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<?php if(!empty($record->player_number) || !empty($record->player_price)){ ?>
						<tr class="items-inner">
							<?php if(Auth::guard('admin')->user()->user_role_id == 1): ?>
								<td>
									<?php echo e($record->club_name); ?> 
								</td>
							<?php endif; ?>
							<td>
								<?php echo e($record->player_number); ?> 
							</td>
							<td>
								<?php echo e(!empty($record->player_price) ? "$".$record->player_price :''); ?>

							</td>
							<td>
								<?php echo e(date('d-M-Y h:i',strtotime($record->created_at))); ?>

							</td>
						</tr>
						<?php }else{ ?>
						<?php } ?>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="9" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div> 
</section> 
<script>

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/common/additional_player_logs.blade.php ENDPATH**/ ?>
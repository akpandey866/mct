<?php $__env->startSection('content'); ?>
<script>
    jQuery(document).ready(function(){
        $(".choosen_selct").chosen();
    });
</script>
<style>
.chosen-container-single .chosen-single{
    height:34px !important;
    padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Team")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Team")); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
	<?php if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID): ?>
		<?php echo e(Form::open(['role' => 'form','url' => 'admin/team','class' => 'mws-form',"method"=>"get"])); ?>

		<?php echo e(Form::hidden('display')); ?>

			<div class="col-md-2 col-sm-3">
	            <div class="form-group ">  
	                <div class="form-group ">  
	                <?php echo e(Form::select('game_mode',[null => 'Please Select Mode'] + Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''), ['class' => 'form-control choosen_selct game_mode'])); ?>

	                </div>
	            </div>
	        </div>
	        <div class="col-md-2 col-sm-3">
	            <div class="form-group ">  
	                <div class="form-group get_game_name">  
	                <?php echo e(Form::select('game_name',array(''=>trans('Select Game')),((isset($searchVariable['game_name'])) ? $searchVariable['game_name'] : ''), ['class' => 'form-control choosen_selct'])); ?>

	                </div>
	            </div>
	        </div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select('is_active',array(''=>trans('Select Status'),0=>'Inactive',1=>'Active'),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control choosen_selct'])); ?>

				</div>
			</div>
			
			<!-- <div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => 'form-control','placeholder'=>'Team Name'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select(
						 'club',
						 [null => 'Please Select Club'] + $cludDetails,
						 (isset($searchVariable['club'])) ? $searchVariable['club'] : '',
						 ['id' => 'club','class'=>'form-control']
						)); ?>

				</div>
			</div> -->
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
				
					<?php echo e(Form::select(
						 'position',
						 [null => 'Please Select Grade'] + $gradeName,
						 (isset($searchVariable['grade_name'])) ? $searchVariable['grade_name'] : '',
						 ['id' => 'position','class'=>'form-control']
						)); ?>

				</div>
			</div>
		<!-- 	<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select(
						 'type',
						 [null => 'Please Select Team Type'] + $teamType,
						 (isset($searchVariable['type'])) ? $searchVariable['type'] : '',
						 ['id' => 'type','class'=>'form-control']
						)); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select(
						 'svalue',
						 [null => 'Please Select Team Category'] + $teamCategory,
						 (isset($searchVariable['team_category'])) ? $searchVariable['team_category'] : '',
						 ['id' => 'svalue','class'=>'form-control']
						)); ?>

				</div>
			</div>
			 -->
			
			<div class="col-md-2 col-sm-2">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> <?php echo e(trans('Search')); ?></button>
				<a href="<?php echo e(URL::to('admin/team')); ?>"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> <?php echo e(trans("Reset")); ?></a>
			</div>
			<?php echo e(Form::close()); ?>

		<div class="col-md-2 col-sm-9">
			<div class="form-group">  
				<a href="<?php echo e(URL::to('admin/team/admin-add-team')); ?>" class="btn btn-success btn-small  pull-right" style="margin:0;"><?php echo e(trans("Add New Team")); ?> </a>
			</div>
		</div>
		<?php else: ?>
	<div class="col-md-12 col-sm-12">
		<div class="form-group">  
			<a href="<?php echo e(URL::to('admin/team/add-team')); ?>" class="btn btn-success btn-small  pull-right" style="margin:0;"><?php echo e(trans("Add New Team")); ?> </a>
		</div>
	</div> 
	<?php endif; ?>
	</div>

	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
					
					<th width="11%">
							<?php echo e(link_to_route(
									"team.index",
									trans("Team Name"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
					<?php if(auth()->guard('admin')->user()->id == 1): ?>
						<th width="11%">
							<?php echo e(link_to_route(
									"team.index",
									trans("Club Name"),
									array(
										'sortBy' => 'club_name',
										'order' => ($sortBy == 'club_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'club_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'club_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
					<?php endif; ?>
						<!-- <th width="11%">
							<?php echo e(link_to_route(
									"team.index",
									trans("Category Name"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> -->
						<th width="11%">
							<?php echo e(link_to_route(
									"team.index",
									trans("Status"),
									array(
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									"team.index",
									trans("Created On"),
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							Action
						</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr class="items-inner">
							
							<td>
								<?php echo e($record->team_name); ?> 
							</td>
						<?php if(auth()->guard('admin')->user()->id == 1): ?>
							<td>
								<?php echo e($record->club_name); ?>

							</td>
						<?php endif; ?>
							<!-- <td>
								<?php echo e($record->name); ?>

							</td> -->
							
							<td>
								<?php if($record->is_active	==1): ?>
									<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
								<?php else: ?>
									<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
								<?php endif; ?>
								<!--<?php if($record->is_verified==1): ?>
									<span class="label label-success" ><?php echo e(trans("Verified")); ?></span>
								<?php else: ?>
									<span class="label label-warning" ><?php echo e(trans("Not Verified")); ?></span>
								<?php endif; ?>-->
							</td>
							<td>
								<?php echo e(date(config::get("Reading.date_format"),strtotime($record->created_at))); ?>

							</td>
							<td data-th='Action'>
								<a title="<?php echo e(trans('Edit')); ?>" href="<?php echo e(URL::to('admin/team/edit-team/'.$record->id)); ?>" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>
								<?php if($record->is_active == 1): ?>
									<a  title="Click To Deactivate" href="<?php echo e(URL::to('admin/team/update-status/'.$record->id.'/0')); ?>" class="btn btn-success btn-small status_any_item"><span class="fa fa-check"></span>
									</a>
								<?php else: ?>
									<a title="Click To Activate" href="<?php echo e(URL::to('admin/team/update-status/'.$record->id.'/1')); ?>" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
									</a> 
								<?php endif; ?> 
								<a href="<?php echo e(URL::to('admin/team/view-team/'.$record->id)); ?>" title="<?php echo e(trans('View')); ?>" class="btn btn-info">
									<i class="fa fa-eye"></i>
								</a>
								<a title="<?php echo e(trans('Delete')); ?>" href="<?php echo e(URL::to('admin/team/delete-team/'.$record->id)); ?>"  class="delete_any_item btn btn-danger">
									<i class="fa fa-trash-o"></i>
								</a>
							</td>
						</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="9" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div> 
</section> 
<script>
	var game_name = "<?php echo !empty($searchVariable['game_name']) ? $searchVariable['game_name'] :'' ?>";
	var game_mode = "<?php echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode'] :'' ?>";
	if(game_name !="" || game_mode !=""){
		get_game_name();

	}
	function get_game_name(){
		$.ajax({
		 	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:' <?php echo e("admin-get-game-name"); ?> ',
			'type':'post',
			data:{'mode':game_mode,'game_name':game_name},
			success:function(response){ 
				$(".get_game_name").html(response);
			}
		});
	}
	$(".game_mode").on('change',function(){
		var mode = $(this).val();
		$('#loader_img').show();
		 $.ajax({
		 	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:' <?php echo e("admin-get-game-name"); ?> ',
			'type':'post',
			data:{'mode':mode,'game_name':game_name},
			success:function(response){ 
				$(".get_game_name").html(response);
			}
		}); 
		$('#loader_img').hide();
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/team/index.blade.php ENDPATH**/ ?>
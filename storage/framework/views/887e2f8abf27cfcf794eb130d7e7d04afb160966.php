<?php $__env->startSection('content'); ?>

<?php
	$userInfo	=	Auth::guard('admin')->user();
	$full_name	=	(isset($userInfo->full_name)) ? $userInfo->full_name : '';
	$username	=	(isset($userInfo->username)) ? $userInfo->username : '';
	$email		=	(isset($userInfo->email)) ? $userInfo->email : '';
?>
<section class="content-header">
	<h1 class="text-center">My Account
		<span class="pull-right">
			<a href="<?php echo e(URL::to('admin/change-password')); ?>" class="btn btn-danger"><i class=\"icon-refresh\"></i>Change Password</a>
		</span>
	</h1>
	<div class="clearfix"></div>
</section>
<section class="content">
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/myaccount','class' => 'mws-form','files'=>'true'])); ?>

	<div class="row pad">
		<div class="col-md-6">
			<div class="form-group">
				<?php echo e(Form::label('full_name', trans("Full Name").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text('full_name', $full_name, ['class' => 'form-control'])); ?>  
					<div class="error-message help-inline">
						<?php echo $errors->first('full_name'); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<?php echo e(Form::label('email', trans("Email").'*', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text('email', $email, ['class' => 'form-control'])); ?>  
					<div class="error-message help-inline">
						<?php echo $errors->first('email'); ?>
					</div>
				</div>
			</div>
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				<a href="<?php echo e(URL::to('admin/myaccount')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans('Reset')); ?></a>
			</div>	
		</div>
	</div>   	
	<?php echo e(Form::close()); ?>

</section>

<style>
#MyaccountAddress {
	resize: vertical; /* user can resize vertically, but width is fixed */
}
.error-message{
	color: #f56954 !important;
}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/dashboard/myaccount.blade.php ENDPATH**/ ?>
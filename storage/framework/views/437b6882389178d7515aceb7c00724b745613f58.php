<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
		 <?php echo e(trans("Player Detail")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/player/')); ?>"><?php echo e(trans("Player")); ?></a></li>
		<li class="active"> <?php echo e(trans("Player Detail")); ?>  </li>
	</ol>
</section>
<div class="box box-warning "> 
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">PLAYER INFORMATION</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">First Name:</th>
						<td data-th='First Name' class="txtFntSze"><?php echo e(isset($userDetails->first_name) ? $userDetails->first_name:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Last Name:</th>
						<td data-th='Last Name' class="txtFntSze"><?php echo e(isset($userDetails->last_name) ? $userDetails->last_name:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Value:</th>
						<td data-th='Value' class="txtFntSze"><?php echo e(isset($userDetails->svalue) ? $userDetails->svalue:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Position: </th>
						<td data-th='Position' class="txtFntSze"><?php echo e(isset($userDetails->position) ? $userDetails->position:''); ?></td>
					</tr>
					
					
				</tbody>
			</table>
		</div>
	</div>
</div>
<style>
.about_me {
    word-wrap:break-word;
	word-break: break-all;
}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/player/view.blade.php ENDPATH**/ ?>
<section class="top_social_sec">
<?php 
 $segment1 = Request::segment(1);
 // dump($segment1); 
// dump($sess_club_name_lobby); die ; 
if(!empty($sess_club_name_lobby)){
     $sess_club_name = $sess_club_name_lobby ;
}
if(!empty($clubLists_lobby)){
    $sessClubLists = $clubLists_lobby; 
}

?>
    <div class="container">
        <div class="row align-items-start align-items-sm-center">
            <div class="col-7 mt-0 mt-lg-3 mb-0 mb-lg-3">			
                <form  method="get" class="top_cricketclub_block d-flex align-items-center">
                    <?php if(!empty($sess_club_name->club_logo) && File::exists(CLUB_IMAGE_ROOT_PATH.$sess_club_name->club_logo)): ?>
                    <img class="cricketclub_logo d-none d-lg-block float-left mr-4 club_logo" src="<?php echo e(CLUB_IMAGE_URL.$sess_club_name->club_logo); ?>" alt="cricketclub" >
                    <?php else: ?>
                    <img class="cricketclub_logo d-none d-lg-block float-left mr-4 club_logo" src="<?php echo e(asset('img/no_image.png')); ?>" alt="cricketclub">
                    <?php endif; ?>
                    <div class="park_viking_sec pt-2 pt-md-0">
                        <?php if(auth()->guard('web')->user()->id): ?>
                            <h1 class="park_viking_title mb-0 mb-lg-2 game_name"><?php echo e(!empty($sess_club_name) ?  (strlen($sess_club_name->game_name) > 50 ? substr($sess_club_name->game_name,0,50)."..." : $sess_club_name->game_name) :''); ?></h1>
                        <?php endif; ?>
                        <div class="append_club_name">
                         <?php echo e(Form::select('club_name', !empty($sessClubLists) ? $sessClubLists : [], !empty($sess_club_name) ? $sess_club_name->id :'', ['class' => 'custom-select mb-0 mb-lg-3','id'=>'club_id','placeholder' => 'Choose Club...'])); ?>

                        </div>
                    </div>
                </form>
            </div>
            <div class="col-5 mt-2 mt-lg-3 mb-3">
                <div class="brought_by_sec d-flex align-items-center">
                    <div class="brought_by_img_block text-left text-lg-right"><span class="mt-0 mt-lg-2 mb-2 mb-lg-0">Presented by</span>
                      <a target="_blank" href="<?php echo e(!empty($sess_club_name->brand_url) ? $sess_club_name->brand_url : '#'); ?>">
                        <?php if(!empty($sess_club_name) && !empty($sess_club_name->brand_image) && File::exists(BRANDING_IMAGE_ROOT_PATH.$sess_club_name->brand_image)): ?>
                        <img style="width: 90px; height: 90px" src="<?php echo e(BRANDING_IMAGE_URL.$sess_club_name->brand_image); ?>" alt="brought_by"> 
                        <?php else: ?>
                        <img style="width: 90px; height: 90px" src="<?php echo e(asset('img/brand_icon.png')); ?>" alt="brought_by"> 
                        <?php endif; ?>
                      </a>
                    </div>
                    <ul class="top_social_list d-none d-lg-block ml-auto">
                        <li class="d-inline-flex"><a href="<?php echo e(!empty($sess_club_name) && !empty($sess_club_name->facebook) ? $sess_club_name->facebook : Config::get('Social.facebook')); ?>" target="_blank" title="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="d-inline-flex ml-1"><a href="<?php echo e(!empty($sess_club_name) && !empty($sess_club_name->twitter) ? $sess_club_name->twitter : Config::get('Social.twitter')); ?>" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a></li>
                        <li class="d-inline-flex ml-1"><a href="<?php echo e(!empty($sess_club_name) && !empty($sess_club_name->instagram) ? $sess_club_name->instagram :Config::get('Social.instagram_url')); ?>" target="_blank"  title="instagram"><i class="fab fa-instagram"></i></a></li>

                        <li class="d-inline-flex ml-1"><a href="<?php echo e(!empty($sess_club_name) && !empty($sess_club_name->website) ? $sess_club_name->website :''); ?>" target="_blank"  title="website"><i class="fas fa-globe"></i></a></li>
                        <li class="country_flag d-flex align-items-center mt-3 mt-lg-5"><span class="city_name">
                            <?php if(!empty($sess_club_name->city) && !ctype_digit($sess_club_name->city)): ?>
                                <?php echo e($sess_club_name->city); ?>

                            <?php endif; ?>
                        </span><img class="ml-3" src="<?php echo e(asset('img/sydney_flag.png')); ?>" alt="Sydney"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    
$('#loader_img').show();
$(document).on("change",".append_club_name select",function() {
    $('#loader_img').show(); 
    $(this).closest('form').attr('action', "<?php echo e(url($segment1)); ?>").submit();
});
$( document ).ready(function() {
    $('#loader_img').show();
});
jQuery(window).load(function () {
    $('#loader_img').show(); 

    setTimeout(function () {
       $('#loader_img').hide(); 
    }, 1000);

});
</script>

</section>  <?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/elements/common_header.blade.php ENDPATH**/ ?>
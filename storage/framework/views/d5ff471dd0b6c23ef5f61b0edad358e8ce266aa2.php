
                    <?php if(!empty($user_teams)): ?>
                    <?php $n=1; ?>
                    <?php $__currentLoopData = $user_teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="d-inline-flex align-items-center">
                        <div class="mb-3 mb-sm-0">
                            <div class="rank_number_info"><?php echo e(isset($team_ranks_arr[$value->id]) ? $team_ranks_arr[$value->id] : 0); ?><small class="pt-1">Rank</small></div>
                        </div>
                        <div class="w-100 rank_info d-flex align-items-center p-3">
                            <div>
                                <div class="rank_player_pic">
                                    <?php if(!empty($value->userdata->image) && File::exists(CLUB_IMAGE_ROOT_PATH.$value->userdata->image) && !empty($value->userdata->image)): ?>
                                    <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.USER_PROFILE_IMAGE_URL.'/'.$value->userdata->image ?>" alt="Partners">
               <?php elseif(!empty($value->userdata->club_logo) && File::exists(CLUB_IMAGE_ROOT_PATH.$value->userdata->club_logo)): ?>
                    <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=65px&height=65px&cropratio=1:1&image='.CLUB_IMAGE_URL.$value->userdata->club_logo ?>" alt="cricketclub" >
                    <?php else: ?>  

                                     <?php if(!empty($value->userdata->gender)){ if($value->userdata->gender==MALE){ ?>
                                        <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.WEBSITE_IMG_URL.'rank_male.png' ?>" alt="user_image">
                                    <?php } elseif ($value->userdata->gender==FEMALE) { ?>
                                        <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.WEBSITE_IMG_URL.'rank_female.png' ?>" alt="user_image">
                                    <?php }elseif ($value->userdata->gender==DONOTSPECIFY) { ?> 
                                        <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.WEBSITE_IMG_URL.'rank_male_female.png' ?>" alt="user_image">
                                    <?php }else{ ?>
                                        <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.WEBSITE_IMG_URL.'rank_male.png' ?>" alt="user_image">
                                    <?php } }else{ ?>
                                        <img class="rank_player_pic" src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.WEBSITE_IMG_URL.'rank_male.png' ?>" alt="user_image">
                                    <?php } ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="w-100 rank_player_info d-flex align-items-center pl-3">
                                <div class="w-50 mr-auto">
                                    <div class="rank_player_name d-flex align-items-center"><span><?php echo e($value->my_team_name); ?></span> 
                                        <?php $fundraiserExists = showFundraiserUser($value->user_id); 
                                        if(!empty($fundraiserExists)){ ?>
                                        <img class="ml-2" src="<?php echo e(asset('img/right.png')); ?>" alt="right" title="Fundraiser">
                                        <?php } ?>
                                    </div>
                                    <div class="rank_player_sheild d-flex align-items-center">
   <?php echo e(!empty($value->userdata->first_name) ? ucfirst(substr($value->userdata->first_name, 0, 2).'. ') : ''); ?>

                                <?php echo e(!empty($value->userdata->last_name) ? $value->userdata->last_name : ''); ?>

                            </div>
                                </div>
                                <div class="w-50 d-flex pl-2 ml-auto">
                                     <div class="game_week_info_status d-none d-md-inline-block px-1 mr-auto"><?php echo e(isset($temp_points_arr_past_wk[$value->id]) ? $temp_points_arr_past_wk[$value->id] : 0); ?> <small>Last Week Pts</small></div>
                                    <div class="game_week_info_status px-1 ml-auto"><?php echo e(isset($temp_points_arr[$value->id]) ? $temp_points_arr[$value->id] : 0); ?> <small>Total Pts.</small></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php $n++; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
            <?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/common/load_more_rank.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Fundraiser Message")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Fundraiser Message")); ?></li>
	</ol>
</section>
<section class="content"> 
<?php echo e(Form::open(['role' => 'form','url' => 'admin/fundraiser-message','class' => 'mws-form','files'=>'true','id' => 'add_branding'])); ?>

<div class="row">
    <div class="col-md-6">
        <div class="form-group <?php echo ($errors->first('message')) ? 'has-error' : ''; ?>">
            <?php echo e(Form::label('message',trans("Message").' *', ['class' => 'mws-form-label message'])); ?>

            <div class="mws-form-item">
                <?php echo e(Form::text('message',isset($result->message) ? $result->message :'',['class' => 'form-control'])); ?>

                <div class="error-message help-inline">
                    <?php echo $errors->first('message'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mws-button-row">
    <div class="input" >
       <!-- <button type="button" class="btn btn-info" onclick="payAmount()">Pay Now</button> -->
      <input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
    </div>
</div>
<?php echo e(Form::close()); ?>

</section> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/common/fundraiser_message.blade.php ENDPATH**/ ?>
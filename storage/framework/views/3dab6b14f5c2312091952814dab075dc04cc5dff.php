<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
		 <?php echo e(trans("Fixture Detail")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/fixture')); ?>"><?php echo e(trans("Fixture")); ?></a></li>
		<li class="active"> <?php echo e(trans("Fixture Detail")); ?>  </li>
	</ol>
</section>
<div class="box box-warning "> 
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">Fixture INFORMATION</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Match:</th>
						<td data-th='Full Name' class="txtFntSze"><?php echo e(isset($fixtureDetails->match_type) ? $fixtureDetails->match_type:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Vanue:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($fixtureDetails->vanue) ? $fixtureDetails->vanue:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Grade Name:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($fixtureDetails->grade_name) ? $fixtureDetails->grade_name:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Team Name:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($fixtureDetails->team_name) ? $fixtureDetails->team_name:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Team Category:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($fixtureDetails->team_category) ? $fixtureDetails->team_category:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Opposition Club:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($fixtureDetails->club_name) ? $fixtureDetails->club_name:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Start Date:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($fixtureDetails->start_date) ? $fixtureDetails->start_date:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">End Date:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($fixtureDetails->end_date) ? $fixtureDetails->end_date:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Start Time:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($fixtureDetails->start_time) ? $fixtureDetails->start_time:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">End Time:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($fixtureDetails->end_time) ? $fixtureDetails->end_time:''); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<style>
.about_me {
    word-wrap:break-word;
	word-break: break-all;
}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/fixture/view.blade.php ENDPATH**/ ?>
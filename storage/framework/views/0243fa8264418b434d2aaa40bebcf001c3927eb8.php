<?php $__env->startSection('content'); ?>

<div class="form-box" id="login-box">
	
	<div class="header"><img style="margin-bottom: 10px; " src="<?php echo e(asset('img/logo_white.png')); ?>"></div>
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/login'])); ?>    
	<div class="body bg-gray">
	<b>Login</b>
		<div class="form-group">
			<?php echo e(Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control'])); ?>

			<div class="error-message help-inline">
				<?php echo $errors->first('email'); ?>
			</div>
		</div>
		<div class="form-group">
		   <?php echo e(Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control','autocomplete'=>false])); ?>

		   <div class="error-message help-inline">
				<?php echo $errors->first('password'); ?>
			</div>
		</div>
		<?php if(Session::get('failed_attampt_login') >= 11): ?>
			<div class="form-group">
				<?php echo e(Form::text('captcha', null, ['placeholder' => 'Captcha Code', 'class' => 'form-control'])); ?>

				<div class="error-message help-inline">
					<?php echo $errors->first('captcha'); ?>
				</div>
				<?php echo e(captcha_img('flat')); ?>

			</div>
		<?php endif; ?>
		
	</div>
	<div class="footer">                                                               
		<button type="submit" class="btn bg-olive btn-block">Login</button> 
		<a href="<?php echo e(URL::to('admin/forget_password')); ?>">Forgot your password?</a>
		
	</div>
	<br /><a class="btn bg-olive btn-block" href="<?php echo e(URL::to('/')); ?>">Go to myclubtap.com</a>
	<?php echo e(Form::close()); ?>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.login_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/login/index.blade.php ENDPATH**/ ?>
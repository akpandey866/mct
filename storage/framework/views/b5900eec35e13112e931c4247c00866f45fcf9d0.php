<?php $__env->startSection('content'); ?>

<div class="form-box" id="login-box">
	<div class="header"><img style="margin-bottom: 10px; " src="<?php echo e(asset('img/logo_white.png')); ?>"></div>
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/send_password'])); ?>

	<div class="body bg-gray">
	<b> Forgot Password</b>
		<div class="form-group">
			<?php echo e(Form::text('email', null, ['placeholder' => 'Email','class'=>'form-control'])); ?>

			<div class="error-message help-inline">
				<?php echo $errors->first('email'); ?>
			</div>
		</div>
	</div>
	<div class="footer">                                                               
		<button type="submit" class="btn bg-olive btn-block">Submit</button> 
		<a class="btn bg-olive btn-block"  href="<?php echo e(URL::to('/admin')); ?>">Cancel</a>
	</div>
	<?php echo e(Form::close()); ?>

</div>
<?php echo $__env->make('admin.layouts.login_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/login/forget_password.blade.php ENDPATH**/ ?>
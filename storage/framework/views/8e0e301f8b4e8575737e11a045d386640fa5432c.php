<?php $__env->startSection('content'); ?> 
<script src="<?php echo e(asset('js/bootstrap-datepicker.js')); ?>"></script>
<link href="<?php echo e(asset('css/bootstrap-datepicker.css')); ?>" rel="stylesheet">
<script src="<?php echo e(asset('js/admin/bootstrap-timepicker.min.js')); ?>"></script> 
 <link rel="stylesheet" href="<?php echo e(asset('css/admin/bootstrap-timepicker.min.css')); ?>">
<section class="content-header">
    <h1>
        <?php echo e(trans("Add Grade")); ?> 
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo e(URL::to('admin/list-grade')); ?>"><?php echo e(trans("List Grade")); ?></a></li>
        <li class="active"><?php echo e(trans("Add Grade")); ?> </li>
    </ol>
</section>
<section class="content"> 
    <?php echo e(Form::open(['role' => 'form','url' => 'admin/add-grade','class' => 'mws-form','files'=>'true','id'=>'add_availability'])); ?>

    <?php if(Auth::guard('admin')->user()->user_role_id == 1): ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('mode')) ? 'has-error' : ''; ?>">
                    <?php echo e(Form::label('mode', trans("Game Mode"), ['class' => 'mws-form-label'])); ?>

                    <div class="mws-form-item">
                          <?php echo e(Form::select('mode',[null => 'Please Select Mode'] +Config::get('home_club'),'',['id' => 'mode','class'=>'form-control'])); ?>

                        <div class="error-message help-inline">
                            <?php echo $errors->first('mode'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
                    <?php echo e(Form::label('club', trans("Club"), ['class' => 'mws-form-label'])); ?>

                    <div class="mws-form-item put_club_html">
                          <?php echo e(Form::select('club',[null => 'Please Select Club'],'',['id' => 'club','class'=>'form-control'])); ?>

                        <div class="error-message help-inline">
                            <?php echo $errors->first('club'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('grade')) ? 'has-error' : ''; ?>">
                <?php echo e(Form::label('grade', trans("Grade"), ['class' => 'mws-form-label'])); ?>

                <div class="mws-form-item">
                     <?php echo e(Form::text('grade','', ['class' => 'form-control ','placeholder'=>'Grade','id'=>'grade'])); ?>

                    <div class="error-message help-inline">
                        <?php echo $errors->first('grade'); ?>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
    
    <div class="mws-button-row">
        <div class="input" >
            <input type="button" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger" onclick="save_availability();">
            <a href="<?php echo e(URL::to('admin/add-grade/')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans("Reset")); ?></a>
            <a href="<?php echo e(URL::to('admin/list-grade/')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans("Cancel")); ?></a>
        </div>
    </div>
    <?php echo e(Form::close()); ?>

    
</section>
<script type="text/javascript">

$("#mode").on('change',function(){
    var selectValue   =  $(this).val();
    $('#loader_img').show(); 
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"<?php echo e(url('admin/get-grade-club-mode-listing')); ?>",
        'type':'post',

        data:{'id':selectValue},
        async : true,
        success:function(response){
            console.log(response); 
            
            $('.put_club_html').html(response);
            $('#loader_img').hide();
        }
    });
});


function save_availability(){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    var form = $('#add_availability')[0];
    var formData = new FormData(form);
    $.ajax({
        url: '<?php echo e(route("Grade.addGrade")); ?>',
        type:'post',
        data: formData,
        processData: false,
        contentType: false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
                show_message(data['message'],"success");
                window.location.href   =  "<?php echo e(url('admin/list-grade')); ?>";
            }else {
                $.each(data['errors'],function(index,html){

                    $("#"+index).next().addClass('error');
                    $("#"+index).next().html(html);
                });
                $('#loader_img').hide();
            }
            $('#loader_img').hide();
        }
    });
}

$('#add_availability').each(function() {
    $(this).find('input').keypress(function(e) {
       if(e.which == 10 || e.which == 13) {
            save_availability();
            return false;
        }
    });
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/grade/add_grade.blade.php ENDPATH**/ ?>
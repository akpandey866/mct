<?php $__env->startSection('content'); ?>
<style type="text/css">
    .owl-carousel .owl-item img {
    display: block;
    width: 100%;
    color: black;
    height: 100%;
    }
</style>
<div class="body_section">
    <section class="top_banner_block d-flex align-items-end pb-5" style="background-image: url(<?php echo e(asset('img/banner_bg.png')); ?>);">
        <div class="container py-3 py-lg-5">
            <div class="row">
                <div class="col-12 col-lg-5 pt-3">
                    <h2 class="block_title my-3"><small>Welcome to the world of grassroots</small>Fantasy cricket</h2>
                    <p class="py-3">It’s time to experience the fun of playing fantasy cricket based on people you know, players at your own cricket club, with the power to create separate fantasy games for Seniors, Juniors or based on a league or tournament. It’s now easy to sign-up, set-up and get started!</p>
                    <!-- <a href="javascript:void(0)" class="btn py-2 px-5"  data-toggle="modal" data-target="#myModal">Sign-up as a game</a> -->
                    <a href="<?php echo e(url('signup')); ?>"  class="btn py-2 px-5">Sign up</a>&nbsp;
                     <a href="<?php echo e(url('login')); ?>"  class="btn py-2 px-5">Login</a>
                    <!-- <a href="javascript:void(0)" class="btn py-2 px-5"  data-toggle="modal" data-target="#myModal">Subscribe</a> -->
                </div>
                <div class="col-12 col-lg-6 offset-lg-1 pt-5 pt-lg-0">
                    <div class="register_list_slider owl-carousel">
                        <div class="item">
                            <div class="register_list_sec d-flex align-items-center">
                                <div class="w-100 text-center position-relative">
                                    <div class="register_list_icon d-inline-block mb-4" style="background-image: url(<?php echo e(asset('img/register_list_icon1.png')); ?>);">&nbsp;</div>
                                    <div class="register_list_heading mb-4">SENIOR CLUB <br>fantasy game</div>
                                    <a href="<?php echo e(url('features')); ?>" class="btn py-2 px-5">Features</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="register_list_sec d-flex align-items-center">
                                <div class="w-100 text-center position-relative">
                                    <div class="register_list_icon d-inline-block mb-4" style="background-image: url(<?php echo e(asset('img/register_list_icon2.png')); ?>);">&nbsp;</div>
                                    <div class="register_list_heading mb-4">JUNIOR CLUB<br>fantasy game</div>
                                    <a href="<?php echo e(url('features')); ?>" class="btn py-2 px-5">Features</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="register_list_sec d-flex align-items-center">
                                <div class="w-100 text-center position-relative">
                                    <div class="register_list_icon d-inline-block mb-4" style="background-image: url(<?php echo e(asset('img/register_list_icon3.png')); ?>);">&nbsp;</div>
                                    <div class="register_list_heading mb-4">LEAGUE <br>fantasy game</div>
                                    <a href="<?php echo e(url('features')); ?>" class="btn py-2 px-5">Features</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="py-5">
        <div class="container">
            <ul class="info_links d-flex flex-wrap">
                <li class="p-3">
                    <div class="top_right">&nbsp;</div>
                    <div class="d-flex align-items-center position-relative">
                        <span class="info_links_title mw-100" title="USERS"><span class="d-block"><?php echo e($tot_user_count + 2500); ?></span> USERS</span>
                    </div>
                    <div class="bottom_left">&nbsp;</div>
                </li>
                <li class="p-3">
                    <div class="top_right">&nbsp;</div>
                    <div class="d-flex align-items-center position-relative">
                        <span class="info_links_title mw-100" title="GAMES"><span class="d-block"><?php echo e($tot_club + 79); ?></span> GAMES</span>
                    </div>
                    <div class="bottom_left">&nbsp;</div>
                </li>
                <li class="p-3">
                    <div class="top_right">&nbsp;</div>
                    <div class="d-flex align-items-center position-relative">
                        <span class="info_links_title mw-100" title="SENIOR CLUB GAMES"><span class="d-block"><?php echo e($tot_senior_club + 74); ?></span>SENIOR GAMES</span>
                    </div>
                    <div class="bottom_left">&nbsp;</div>
                </li>
                <li class="p-3">
                    <div class="top_right">&nbsp;</div>
                    <div class="d-flex align-items-center position-relative">
                        <span class="info_links_title mw-100" title="JUNIOR CLUB GAMES"><span class="d-block"><?php echo e($tot_junior_club + 2); ?></span>JUNIOR GAMES</span>
                    </div>
                    <div class="bottom_left">&nbsp;</div>
                </li>
                <li class="p-3">
                    <div class="top_right">&nbsp;</div>
                    <div class="d-flex align-items-center position-relative">
                        <span class="info_links_title mw-100" title="LEAGUE GAMES"><span class="d-block"><?php echo e($tot_league_club + 3); ?></span>LEAGUE GAMES</span>
                    </div>
                    <div class="bottom_left">&nbsp;</div>
                </li>
                <li class="p-3">
                    <div class="top_right">&nbsp;</div>
                    <div class="d-flex align-items-center position-relative">
                        <span class="info_links_title mw-100" title="FANTASY TEAMS"><span class="d-block"><?php echo e($tot_user_team + 2700); ?></span>FANTASY TEAMS</span>
                    </div>
                    <div class="bottom_left">&nbsp;</div>
                </li>
            </ul>
        </div>
    </section>
    <section class="get_started_block py-5">
        <div class="container">
            <div class="d-block d-lg-flex align-items-center mb-5">
                <h2 class="block_title"><small>Get fantasy cricket for </small>Your club or league </h2>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6 col-xl-6 my-3">
                    <div class="howtoplay_inner">
                        <div class="step steps_two">
                            <div class="stepcount">
                                <span class="numbers_two">
                                1
                                </span>
                            </div>
                            <div class="step_info ">
                                <h2>Sign-up to have a fantasy game of your own for one of the modes (SENIOR CLUB, JUNIOR CLUB, LEAGUE).</h2>
                                
                            </div>
                        </div>
                        <div class="step steps_two">
                            <div class="stepcount">
                                <span class="numbers_two">
                                2
                                </span>
                            </div>
                            <div class="step_info">
                                <h2>Set-up your own fantasy game through an easy to use admin portal.</h2>
                            </div>
                        </div>
                        <div class="step steps_two">
                            <div class="stepcount">
                                <span class="numbers_two">
                                3
                                </span>
                            </div>
                            <div class="step_info">
                                <h2>Once ready to go live, simply pay and activate your game and make it available for all users.</h2>
                            </div>
                        </div>
                        <div class=""> 
                            <!-- lets_play<a href="javascript:void(0)"data-toggle="modal" data-target="#myModal">Subscribe</a> -->
                            <a href="<?php echo e(url('features')); ?>" class="btn py-2 px-5">Features</a>&nbsp
                            <a href="<?php echo e(url('pricing')); ?>"  class="btn py-2 px-5">Pricing</a>
                            <!-- <a href="<?php echo e(url('signup')); ?>">Sign-Up</a> -->
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-5 col-xl-5 my-3">
                    <div class="say_about_sec fantasy_game_sec mt-5 mt-lg-0">
                        <div class="say_about_client_slider owl-carousel">
                            <?php $clubSlider = get_slider('clubs-slider');?>
                            <?php if(!$clubSlider->isEmpty()): ?>
                            <?php $__currentLoopData = $clubSlider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sliders): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item">
                                <div class=" mx-auto mb-4"><img src="<?php echo e(SLIDER_IMAGE_URL.$sliders->image); ?>" alt="client"></div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="how_play_block py-5">
        <div class="container">
            <h2 class="block_title mb-5"><small>How to play</small>A fantasy game</h2>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-7 col-xl-7 my-3">
                    <div class="say_about_sec fantasy_game_sec mt-5 mt-lg-0">
                        <div class="say_about_client_slider owl-carousel">
                            <?php $clubSlider = get_slider('fantasy-slider');?>
                            <?php if(!$clubSlider->isEmpty()): ?>
                            <?php $__currentLoopData = $clubSlider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sliders): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item">
                                <div class=" mx-auto mb-4"><img src="<?php echo e(SLIDER_IMAGE_URL.$sliders->image); ?>" alt="client"></div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-5 col-xl-5 my-3">
                    <div class="howtoplay_inner">
                        <div class="step">
                            <div class="stepcount">
                                <span class="numbers">
                                1
                                </span>
                            </div>
                            <div class="step_info">
                                <h2>Sign-up and login to play any available fantasy game for free.</h2>
                            </div>
                        </div>
                        <div class="step">
                            <div class="stepcount">
                                <span class="numbers">
                                2
                                </span>
                            </div>
                            <div class="step_info">
                                <h2>Pick your own fantasy team of 11 players for a fantasy game.</h2>
                            </div>
                        </div>
                        <div class="step">
                            <div class="stepcount">
                                <span class="numbers">
                                3
                                </span>
                            </div>
                            <div class="step_info">
                                <h2>Use your Trades and Powers to boost the performance of your team.</h2>
                            </div>
                        </div>
                        <div class="step">
                            <div class="stepcount">
                                <span class="numbers">
                                4
                                </span>
                            </div>
                            <div class="step_info">
                                <h2>Play any game available across the 3 game modes.</h2>
                              </div>
                        </div>
                        <div class="step">
                            <div class="stepcount">
                                <span class="numbers">
                                5
                                </span>
                            </div>
                            <div class="step_info">
                                <h2>Play and win prizes on offer in the fantasy games.</h2>
                            </div>
                        </div>
                        <div class=""> <!-- lets_play -->
                            <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="btn py-2 px-5">Subscribe</a> -->
                            <a href="<?php echo e(url('signupasauser')); ?>"  class="btn py-2 px-5">Sign up</a> &nbsp;
                            <a href="<?php echo e(url('login')); ?>"  class="btn py-2 px-5">Login</a>
                            <!-- <a href="<?php echo e(url('signup')); ?>">Sign-Up / Login</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="say_about_block py-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 col-xl-5">
                    <h2 class="block_title"><small>What our users</small>Say about us</h2>
                </div>
                <div class="col-12 col-lg-6 col-xl-7">
                    <div class="say_about_sec p-5 mt-5 mt-lg-0">
                        <div class="say_about_client_slider owl-carousel">
                            <?php if(!$userSliders->isEmpty()): ?>
                            <?php $__currentLoopData = $userSliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $values): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="item">
                                <div class="say_about_client mx-auto mb-4"><img src="<?php echo e(USER_SLIDER_IMAGE_URL.'/'.$values->image); ?>" alt="client"></div>
                                <div class="say_about_client_review text-center mb-4"><?php echo !empty($values->description) ? $values->description :''; ?></div>
                                <div class="say_about_client_name text-center"><?php echo e($values->name); ?> <span><?php echo e($values->club_name); ?></span></div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
     <?php echo $__env->make('front.elements.subscribe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<?php echo $__env->make('front.elements.subscribe_pop_up', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
<script type="text/javascript">
            function contact_us() { 
        $('#loader_img').show();
        $('.help-inline').html('');
        $('.help-inline').removeClass('error');
        $.ajax({ 
            url: '<?php echo e(URL("subscribe")); ?>',
            type:'post',
            data: $('#contact_form').serialize(),
            success: function(r){
                error_array     =   JSON.stringify(r);
                data            =   JSON.parse(error_array);
                if(data['success'] == 1) {
                    window.location.href     =  "<?php echo e(URL('/')); ?>";
                }else { 
                    $.each(data['errors'],function(index,html){
                        $("#"+index).next().addClass('error');
                        $("#"+index).next().html(html);
                    });
                    $('#loader_img').hide();
                }
                $('#loader_img').hide();
            }
        });
    }
    $('#contact_form').each(function() {
        $(this).find('input').keypress(function(e) {
          if(e.which == 10 || e.which == 13) {
            contact_us();
            return false;
           }
       });
    });
    //say_about_client_slider
    $('.say_about_client_slider').owlCarousel({
        //rtl:false,            
        loop: false,
        margin: 0,
        nav: true,
        //navText : ["",""],
        dots: false,
        autoplay: true,
        items: 1,
    });
    //register_list_slider
    $('.register_list_slider').owlCarousel({
        //rtl:false,            
        loop: false,
        margin: 0,
        nav: true,
        //navText : ["",""],
        dots: false,
        autoplay: true,
        responsive: {
            320: {
                items: 1
            },
            480: {
                items: 1
            },
            576: {
                items: 2
            },
            768: {
                items: 2
            },
            992: {
                items: 2
            },
            1200: {
                items: 2
            }
        }
    });

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/user/index.blade.php ENDPATH**/ ?>
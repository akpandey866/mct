<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Branding")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Branding")); ?></li>
	</ol>
</section>
<section class="content"> 
         <div class="activateBranding">
            <?php echo e(Form::open(['role' => 'form','url' => 'admin/admin-update-branding','class' => 'mws-form','files'=>'true','id' => 'add_branding'])); ?>

            <input type="hidden" name="brand_id" value="<?php echo e(!empty($userDetails->id) ? $userDetails->id : 0); ?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
                                <?php echo e(Form::label('name',trans("Name").' *', ['class' => 'mws-form-label name'])); ?>

                                <div class="mws-form-item">
                                    <?php echo e(Form::text('name',isset($userDetails->name) ? $userDetails->name :'',['class' => 'form-control','required'])); ?>

                                    <div class="error-message help-inline">
                                        <?php echo $errors->first('name'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <?php echo ($errors->first('url')) ? 'has-error' : ''; ?>">
                                <?php echo e(Form::label('url',trans("Website URL").' *', ['class' => 'mws-form-label url'])); ?>

                                <div class="mws-form-item">
                                    <?php echo e(Form::text('url',isset($userDetails->url) ? $userDetails->url :'',['class' => 'form-control'])); ?>

                                    <div class="error-message help-inline">
                                        <?php echo $errors->first('url'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
                                <label for="image" class="mws-form-label image">Brand Logo *</label>
                                <div class="mws-form-item">
                                    <?php echo e(Form::file('logo', array('accept' => 'image/*'))); ?><br>
                                    <?php if(!empty($userDetails->logo) && (File::exists(BRANDING_IMAGE_ROOT_PATH.$userDetails->logo))): ?> 
                                        <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo BRANDING_IMAGE_URL.$userDetails->logo; ?>">
                                            <div class="usermgmt_image">
                                                <img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.BRANDING_IMAGE_URL.'/'.$userDetails->logo ?>">
                                            </div>
                                        </a>
                                    <?php endif; ?>
                                    <div class="error-message help-inline">
                                        <?php echo $errors->first('logo'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <?php echo ($errors->first('url')) ? 'has-error' : ''; ?>">
                                <?php echo e(Form::label('url',trans("Mark as paid").' *', ['class' => 'mws-form-label url'])); ?>

                                <div class="mws-form-item">
                                    <?php echo e(Form::checkbox('is_paid',1,!empty($userDetails->is_paid) ? 'checked' :'', array('id'=>'asap'))); ?>

                                    <div class="error-message help-inline">
                                        <?php echo $errors->first('url'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mws-button-row">
                        <div class="input" >
                           <button type="submit" class="btn btn-info">Update</button>
                        </div>
                    </div>
            <?php echo e(Form::close()); ?>

     </div> 
</section> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/common/edit_admin_branding.blade.php ENDPATH**/ ?>
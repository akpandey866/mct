<?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('js/bootstrap-datepicker.js')); ?>"></script>
<link href="<?php echo e(asset('css/bootstrap-datepicker.css')); ?>" rel="stylesheet">
<!-- date time picker js and css and here-->
<script>
	jQuery(document).ready(function(){
		$(".choosen_selct").chosen();
	});
</script>
<style>
.chosen-container-single .chosen-single{
	height:34px !important;
	padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>
	  <?php echo e(trans("Clubs")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Clubs")); ?></li>
	</ol>
</section>
<section class="content"> 
	
	<div class="row">
		<?php echo e(Form::open(['role' => 'form','url' => 'admin/club','class' => 'mws-form',"method"=>"get"])); ?>

		<?php echo e(Form::hidden('display')); ?>

			
			
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::text('full_name',((isset($searchVariable['full_name'])) ? $searchVariable['full_name'] : ''), ['class' => 'form-control','placeholder'=>'Full Name'])); ?>

				</div>
			</div>
			 <div class="col-md-2 col-sm-3">
                <div class="form-group ">  
                    <div class="form-group ">  
                    <?php echo e(Form::select('game_mode',[null => 'Please Select Mode'] + Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''), ['class' => 'form-control game_mode choosen_selct'])); ?>

                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3">
                <div class="form-group ">  
                    <div class="form-group get_game_name">  
                    <?php echo e(Form::select('game_name',array(''=>trans('Select Game')),((isset($searchVariable['game_name'])) ? $searchVariable['game_name'] : ''), ['class' => 'form-control choosen_selct'])); ?>

                    </div>
                </div>
            </div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::text('date_from',((isset($date_from)) ? $date_from : ''), ['class' => 'form-control ','placeholder'=>'Date From','id'=>'date_from'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::text('date_to',((isset($date_to)) ? $date_to : ''), ['class' => 'form-control ','placeholder'=>'Date To','id'=>'date_to'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					<?php echo e(Form::select('is_active',array(''=>trans('Select Status'),0=>'Inactive',1=>'Active'),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control choosen_selct'])); ?>

				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> <?php echo e(trans('Search')); ?></button>
				<a href="<?php echo e(URL::to('admin/club')); ?>"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> <?php echo e(trans("Reset")); ?></a>
			</div>
			<?php echo e(Form::close()); ?>

		<div class="col-md-10 col-sm-2">
			<div class="form-group">  
				<a href="<?php echo e(URL::to('admin/club/add-club')); ?>" class="btn btn-success btn-small  pull-right" style="margin:0;"><?php echo e(trans("Add New Club")); ?> </a>
			</div>
		</div>
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
					<th width="11%">
						<?php echo e(link_to_route(
								"club.index",
								trans("Name"),
								array(
									'sortBy' => 'full_name',
									'order' => ($sortBy == 'full_name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
							   array('class' => (($sortBy == 'full_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'full_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)); ?>

					</th>	
					<th width="11%">
							<?php echo e(link_to_route(
									"club.index",
									trans("Game Mode"),
									array(
										'sortBy' => 'club_type',
										'order' => ($sortBy == 'club_type' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'club_type' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'club_type' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"club.index",
									trans("Club Name"),
									array(
										'sortBy' => 'club_name',
										'order' => ($sortBy == 'club_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'club_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'club_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"club.index",
									trans("Game Name"),
									array(
										'sortBy' => 'game_name',
										'order' => ($sortBy == 'game_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'game_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'game_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"club.index",
									trans("Sport Name"),
									array(
										'sortBy' => 'sport_name',
										'order' => ($sortBy == 'sport_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'sport_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'sport_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="11%">
							<?php echo e(link_to_route(
									"club.index",
									trans("Status"),
									array(
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									"club.index",
									trans("Created On"),
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							Action
						</th>
					</tr>
				</thead>
				<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr>
						<td><?php echo e($record->full_name); ?></td>
						<td><?php echo e(!empty($record->game_mode) ? Config::get('home_club')[$record->game_mode]  :''); ?></td>
							<td>
								<?php echo e($record->club_name); ?>

							</td>
							<td>
								<?php echo e($record->game_name); ?>

							</td>
							<td>
								<?php echo e($record->sport_name); ?>

							</td>
							<!--<td>
								<a href="mailto:<?php echo e($record->email); ?>" class="redicon">
									<?php echo e($record->email); ?>

								</a>
							</td>-->
							<!--<td>
								<?php echo e($record->phone_number); ?>

							</td>-->
							<td>
								<?php if($record->is_active	==1): ?>
									<span class="label label-success" ><?php echo e(trans("Activated")); ?></span>
								<?php else: ?>
									<span class="label label-warning" ><?php echo e(trans("Deactivated")); ?></span>
								<?php endif; ?>
								<!--<?php if($record->is_verified==1): ?>
									<span class="label label-success" ><?php echo e(trans("Verified")); ?></span>
								<?php else: ?>
									<span class="label label-warning" ><?php echo e(trans("Not Verified")); ?></span>
								<?php endif; ?>-->
							</td>
							<td>
								<?php echo e(date(config::get("Reading.date_format"),strtotime($record->created_at))); ?>

							</td>
							<td>
								<a title="<?php echo e(trans('Edit')); ?>" href="<?php echo e(URL::to('admin/club/edit-club/'.$record->id)); ?>" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>
								<?php if($record->is_active == 1): ?>
											<a  title="Click To Deactivate" href="<?php echo e(URL::to('admin/club/update-status/'.$record->id.'/0')); ?>" class="btn btn-success btn-small status_any_item1"><span class="fa fa-check"></span>
											</a>
										<?php else: ?>
											<a title="Click To Activate" href="<?php echo e(URL::to('admin/club/update-status/'.$record->id.'/1')); ?>" class="btn btn-warning btn-small status_any_item1"><span class="fa fa-ban"></span>
											</a> 
										<?php endif; ?> 
								<a href="<?php echo e(URL::to('admin/club/view-club/'.$record->id)); ?>" title="<?php echo e(trans('View')); ?>" class="btn btn-info">
									<i class="fa fa-eye"></i>
								</a>
								
								<a title="<?php echo e(trans('Delete')); ?>" href="<?php echo e(URL::to('admin/club/delete-club/'.$record->id)); ?>"  class="delete_any_item btn btn-danger">
									<i class="fa fa-trash-o"></i>
								</a>
								<a href="<?php echo e(URL::to('admin/game-prizes/game-prizes-message/'.$record->id)); ?>" title="<?php echo e(trans('Prize Message')); ?>" class="btn btn-info">
									<i class="fa fa-trophy"></i>
								</a>
							
								<!--<a title="<?php echo e(trans('Send Login Credentials')); ?>" href="<?php echo e(URL::to('admin/users/send-credential/'.$record->id)); ?>" class="btn btn-success">
									<i class="fa fa-share"></i>
								</a>-->
							</td>
						</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="9" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?> 
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div> 
</section> 
<script>
$(document).ready(function() {
	 $( "#date_from" ).datepicker({
		format 	: 'yyyy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		onSelect	: function( selectedDate ){ $("#date_to").datepicker("option","minDate",selectedDate); }
	});
	$( "#date_to" ).datepicker({
		format 	: 'yyyy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		onSelect	: function( selectedDate ){ $("#date_from").datepicker("option","maxDate",selectedDate); }
	});
})
$(function(){
		$('.date_of_birth').datepicker({
			dateFormat 	: 'yy-mm-dd',
			changeMonth : true,
			changeYear 	: true,
			yearRange	: '-100y:c+nn',
			maxDate		: '-1'
		});	
	});

 $(document).on('click', '.show_profile', function(e){ 
			e.stopImmediatePropagation();
			url = $(this).attr('href');
			var full_name = $(this).attr('data-rel');
			bootbox.confirm("Are you sure want to show "+full_name+ '?',
			function(result){
				if(result){
					window.location.replace(url);
				}
			});
			e.preventDefault();
		});
		
		$(document).on('click', '.hide_profile', function(e){ 
			e.stopImmediatePropagation();
			url = $(this).attr('href');
			var full_name = $(this).attr('data-rel');
			bootbox.confirm("Are you sure want to hide "+full_name+ '?',
			function(result){
				if(result){
					window.location.replace(url);
				}
			});
			e.preventDefault();
		});
	$(".findUsers").change(function(){
		$("#search_users").submit();
	});
	var game_name = "<?php echo !empty($searchVariable['game_name']) ? $searchVariable['game_name'] :'' ?>";
	var game_mode = "<?php echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode'] :'' ?>";
	if(game_name !="" || game_mode !=""){
		get_game_name();

	}
	function get_game_name(){
		$.ajax({
		 	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:' <?php echo e("admin-get-game-name"); ?> ',
			'type':'post',
			data:{'mode':game_mode,'game_name':game_name},
			success:function(response){ 
				$(".get_game_name").html(response);
			}
		});
	}
	$(".game_mode").on('change',function(){
		var mode = $(this).val();
		$('#loader_img').show();
		 $.ajax({
		 	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:' <?php echo e("admin-get-game-name"); ?> ',
			'type':'post',
			data:{'mode':mode,'game_name':game_name},
			success:function(response){ 
				$(".get_game_name").html(response);
			}
		}); 
		$('#loader_img').hide();
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/clubs/index.blade.php ENDPATH**/ ?>
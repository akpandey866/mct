<tr class="add_feature<?php echo e($counter); ?> add_feature_detail" rel="<?php echo e($counter); ?>">
	<td>
		<div class="mws-form-item add_feature_input">
			<?php echo e(Form::text('name[]','',['id' => "name$counter",'class'=>'form-control'])); ?>

			<span class="help-inline"></span>
		</div>
	</td>
	<td>
		<div class="mws-form-item add_feature_input">
			<?php echo e(Form::text('price[]','',['id' => "price$counter",'class'=>'form-control'])); ?>

			<span class="help-inline"></span>
		</div>
	</td>
	<td>
		<a href="javascript:void(0);" onclick="del_feature($(this),0);" id="<?php echo e($counter); ?>" class="btn btn-info btn-small align_button add_more " style="margin-left:15px;">
			<i class="fa fa-trash-o"></i>
		</a>
	</td>
</tr><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/common/add_more_player_packs.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<?php 
    use Carbon\Carbon; 
    ?>
<div class="full_container">
    <div class="body_section">
        <!-- top_header element -->
        <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>      
        <section class="status_tab_block mb-5">
            <div class="container mobile_cont">
                <?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="tab_content_sec border  p-0  p-md-4">
                    <div class="next_prev_link_sec d-flex align-items-center mb-5">
                        
                        <span>Trade history</span>
                    </div>

                    <div class="game_info_block mt-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="game_user_info_block d-flex">
                                    <div class="game_user_pic">
                                        <?php if(!empty(Auth::user()->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.Auth::user()->image)): ?>
                                        <img class="mb-2" src="<?php echo e(asset('uploads/club/'.Auth::user()->image)); ?>" alt="game user">
                                        <?php elseif(!empty(Auth::user()->club_logo) && File::exists(CLUB_IMAGE_ROOT_PATH.Auth::user()->club_logo)): ?>
                                        <img class="cricketclub_logo float-left mr-4 club_logo" src="<?php echo e(CLUB_IMAGE_URL.Auth::user()->club_logo); ?>" alt="cricketclub" >
                                        <?php else: ?>                                
                                        <img class="mb-2" src="<?php echo e(asset('img/user_pic.jpg')); ?>" alt="game user">
                                        <?php endif; ?>
                                    </div>
                                    <div class="w-100 game_info pl-4">
                                        <div class="game_user_name d-flex align-items-center flex-wrap mb-0"><?php echo e(Auth::user()->full_name); ?>                     
                                        </div>
                                        <div class="game_user_name d-flex align-items-center flex-wrap mb-0">
                                            <small class="mt-2"><?php echo e(Auth::user()->my_team_name); ?></small> 
                                        </div>
                                        <ul class="w-100 game_info_list py-2 mt-2">
                                            <!-- <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a  href="<?php echo e(url('points')); ?>">Gameweek</a></li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a  href="<?php echo e(url('history')); ?>">Gameweek History</a></li> -->
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a  href="<?php echo e(url('trade-history')); ?>">Trade History</a></li>
                                           
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="game_info_block mt-4">
                        <div class="row">
                            <div class="col-12 col-lg-12">
                                <h4 class="status_title my-2">Trade In</h4>
                                <div class="table-responsive table-mobile">
                                    <table id="example" width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table  mb-0" width="100%">

                                        <tr>
                                            <th width="12.5%">Date</th>
                                            <th width="12.5%">Time</th>
                                            <th width="12.5%">In</th>
                                            <th width="12.5%">GW</th>

                                        </tr>
                                               
                                        <?php if(!empty($gw_trade_in)): ?>
                                            <?php $__currentLoopData = $gw_trade_in; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k2 => $v2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(!empty($v2->player)): ?>
                                                        <tr>
                                                            <td><?php echo e($v2->created_at->format('d.m.Y')); ?></td>
                                                            <td><?php echo e($v2->created_at->format('g:i A')); ?></td>
                                                            <td><?php echo e(!empty($v2->player) ? $v2->player->full_name : ''); ?></td>
                                                            <td><?php echo e($key); ?></td>
                                                        </tr>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                            <tr>
                                                <td  colspan="7">No record yet available.</td>
                                            </tr>
                                        <?php endif; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="game_info_block mt-4">
                        <div class="row">
                            <div class="col-12 col-lg-12">
                                <h4 class="status_title my-2">Trade Out</h4>
                                <div class="table-responsive table-mobile">
                                    <table id="example" width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table  mb-0" width="100%">

                                        <tr>
                                            <th width="12.5%">Date</th>
                                            <th width="12.5%">Time</th>
                                            <th width="12.5%">Out</th>
                                            <th width="12.5%">GW</th>

                                        </tr>
                                               
                                               
                                        <?php if(!empty($gw_trade_out)): ?>
                                            <?php $__currentLoopData = $gw_trade_out; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php $__currentLoopData = $value; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k2 => $v2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(!empty($v2->player)): ?>
                                                        <tr>
                                                            <td><?php echo e($v2->created_at->format('d.m.Y')); ?></td>
                                                            <td><?php echo e($v2->created_at->format('g:i A')); ?></td>
                                                            <td><?php echo e(!empty($v2->player) ? $v2->player->full_name : ''); ?></td>
                                                            <td><?php echo e($key); ?></td>
                                                        </tr>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                        <tr>
                                            <td  colspan="7">No record yet available.</td>
                                        </tr>
                                        <?php endif; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </section>
        <div class="container pt-4">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/common/trade_history.blade.php ENDPATH**/ ?>
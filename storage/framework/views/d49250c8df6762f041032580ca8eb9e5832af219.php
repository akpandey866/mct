<?php $__env->startSection('content'); ?>
<section class="content-header">
	<h1>
		 <?php echo e(trans("Club Detail")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/club')); ?>"><?php echo e(trans("Clubs")); ?></a></li>
		<li class="active"> <?php echo e(trans("Club Detail")); ?>  </li>
	</ol>
</section>
<div class="box box-warning "> 
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">CLUB USER</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Club Name:</th>
						<td data-th='Full Name' class="txtFntSze"><?php echo e(isset($userDetails->club_name) ? $userDetails->club_name:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Game Name:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($userDetails->game_name) ? $userDetails->game_name :''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Club Log:</th>
						<td data-th='Phone Number' class="txtFntSze"><?php if(File::exists(CLUB_IMAGE_ROOT_PATH.$userDetails->club_logo)): ?>
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo CLUB_IMAGE_URL.$userDetails->club_logo; ?>">
							<div class="usermgmt_image">
								<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.CLUB_IMAGE_URL.'/'.$userDetails->club_logo ?>">
							</div>
						</a>
					<?php endif; ?></td>
					</tr>
					
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">Club Details</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Sport Name:</th>
						<td data-th='Full Name' class="txtFntSze"><?php echo e(isset($userDetails->sport_name) ? $userDetails->sport_name:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Country:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($countryName->name) ? $countryName->name :''); ?></td>
					</tr>
					
					<tr>
						<th  width="30%" class="text-right txtFntSze">State:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($stateName->name) ? $userDetails->name :''); ?></td>
					</tr>
					
					<tr>
						<th  width="30%" class="text-right txtFntSze">City:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($cityName->name) ? $cityName->name :''); ?></td>
					</tr>
					
				</tbody>
			</table>
		</div>
	</div>
	
	
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">Club User Details</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Full Name:</th>
						<td data-th='Full Name' class="txtFntSze"><?php echo e(isset($userDetails->full_name) ? $userDetails->full_name:''); ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Email  :</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($userDetails->email) ? $userDetails->email :''); ?></td>
					</tr>
					
					<tr>
						<th  width="30%" class="text-right txtFntSze">Phone:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($userDetails->mobile) ? $userDetails->mobile :''); ?></td>
					</tr>
					
					<tr>
						<th  width="30%" class="text-right txtFntSze">Date of Birth:</th>
						<td data-th='Email' class="txtFntSze"><?php echo e(isset($userDetails->dob) ? $userDetails->dob :''); ?></td>
					</tr>
					
				</tbody>
			</table>
		</div>
	</div>
</div>

<style>
.about_me {
    word-wrap:break-word;
	word-break: break-all;
}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/clubs/view.blade.php ENDPATH**/ ?>
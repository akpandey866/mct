<?php $__env->startSection('content'); ?>

<section class="content-header">
	<h1>
		<?php echo e('Edit '.studly_case($type)); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/dropdown-manager',$type)); ?>">Masters</a></li>
		<li class="active"><?php echo e('Edit '.studly_case($type)); ?></li>
	</ol>
</section>
<section class="content">
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/dropdown-manager/edit-dropdown/'.$dropdown->id.'/'.$type,'class' => 'mws-form','files' => true])); ?>

	<div class="row pad">
		<div class="col-md-6">
			<div class="form-group">
				<?php echo e(Form::label('name',trans("Name").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text("name",isset($dropdown->name)?$dropdown->name:'', ['class' => 'form-control'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-primary">
		<a href="<?php echo e(URL::to('dropdown-manager/edit-dropdown/'.$dropdown->id.'/'.$type)); ?>" class="btn btn-danger"><i class=\"icon-refresh\"></i> Reset</a>
	</div>
	<?php echo e(Form::close()); ?> 
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/dropdown/edit.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
 <?php 
    use Carbon\Carbon;
    $userCode = getReferalCode();
    $fbShareURL = "https://www.facebook.com/sharer/sharer.php?u=".WEBSITE_URL.'user-sharer/'.$userCode;
    $twitter = "https://twitter.com/intent/tweet?text=".Config::get('Site.title')."&url=".WEBSITE_URL.'signupasauser?refercode='.$userCode;
    $linkedin = "https://www.linkedin.com/shareArticle?mini=true&url=".WEBSITE_URL.urlencode('signupasauser?refercode='.$userCode."&source=linkedin");
    $whatsapp = "https://wa.me/?text=".WEBSITE_URL.'signupasauser?refercode='.$userCode;
    //$linnkedINDeveloper= "https://www.linkedin.com/shareArticle?mini=true&url=".WEBSITE_URL."&title=".Config::get('Site.title');
?>
<div class="full_container">
    <div class="body_section">
        <!-- top_header element -->
        <section class="status_tab_block mb-5 py-3">
            <div class="container">
                <div class="refer_frnd_banner mb-4 mb-sm-5 position-relative">
                    <img src ="<?php echo e(asset('img/refer_frnd_banner.jpg')); ?>" alt="">
                    <h2>Refer a Friend</h2>
                </div>
                <div class="refer_frnd_tab">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane fade in active show" id="Share">
                          <h5 class="invite_heading">Invite Your Friends Now</h5>
                          <div class="invite_platform">
                              <div class="row">
                                    <div class="col-md-3 col-sm-4 col-6">
                                        <div class="box facebook">
                                          <a href="<?php echo e($fbShareURL); ?>" target="_blank"><span><i class="fab fa-facebook-f"></i></span>Facebook</a> 
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-6">
                                        <div class="box facebook">
                                          <a href="<?php echo e($twitter); ?>" target="_blank"><span><i class="fab fa-twitter"></i></span>Twitter</a> 
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-6">
                                        <div class="box linkedin">
                                          <a href="<?php echo e($linkedin); ?>" target="_blank"><span><i class="fab fa-linkedin-in"></i></span>Linkedin</a> 
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4 col-6">
                                        <div class="box whatsapp">
                                          <a href="<?php echo e($whatsapp); ?>" target="_blank"><span><i class="fab fa-whatsapp"></i></span>Whatsapp</a> 
                                        </div>
                                    </div>                                   
                              </div>
                          </div>
                             
                             <div class="coupon_code">
                                 <div class="coupon_box position-relative">
                                    <a href="javascript:void(0)" onclick="copyToClipboard('#p1')" class="copy_code"><i class="far fa-clone"></i></a>
                                     <h4 id="p1"><?php echo strtoupper(Auth::guard('web')->user()->referral_code); ?></h4>
                                 </div>
                             </div>
                             
                             <div class="note_share_ref">
                                <p><span>Share Referral Code</span>Tell your frineds to enter this code on signup</p>
                             </div>                          
                      </div>

                      <!--Refer List Apply here -->
                      <h4 class="status_title ">Refer list</h4>
                        <div class="table-responsive table-mobile">
                            <table id="example" width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table states_table mb-0">
                                <thead>
                                    <tr>
                                        <th class="colmn1">Image</th>
                                        <th class="colmn2" width="30%">User</th>
                                        <th class="colmn3">Created</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!$result->isEmpty()): ?>
                                        <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>                                
                                               <td>
                                                <?php if(!empty($val->image) && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.$val->image)): ?>
                                                    <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo USER_PROFILE_IMAGE_URL.auth()->guard('web')->user()->image; ?>"> <div class="usermgmt_image">
                                                            <img  src="<?php echo WEBSITE_URL.'image.php?width=50px&height=50px&cropratio=1:1&image='.USER_PROFILE_IMAGE_URL.auth()->guard('web')->user()->image ?>">
                                                        </div>
                                                    </a>
                                                <?php else: ?>
                                                <img width="50px" height="50px" src="<?php echo e(asset('img/user_pic.jpg')); ?>" alt="User">
                                                <?php endif; ?>
                                               </td> 
                                                <td><?php echo e($val->full_name); ?></td>
                                                <td><?php echo e(Carbon::parse($val->created_at)->isoFormat('ddd').' - '.Carbon::parse($val->created_at)->isoFormat('D.M.YY')); ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?> 
                                    <tr><td colspan="3">No users have yet been added.</td></tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>        
        </section>
    </div>
</div>
<script type="text/javascript">
function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
  showMessage('Copied','success');
}
table =   $('#example').DataTable({"pageLength": 10,"lengthChange": false,  language: { search: '', searchPlaceholder: "Search" }, "order": [[ 1, "asc" ]]});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/common/user_refer.blade.php ENDPATH**/ ?>
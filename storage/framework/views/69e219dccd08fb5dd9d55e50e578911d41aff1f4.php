<?php $__env->startSection('content'); ?>
<!-- CKeditor start here-->
<?php echo e(Html::script('js/admin/ckeditor/ckeditor.js')); ?>


<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		<?php echo e(trans("Add New Cms")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/cms-manager')); ?>">Cms Pages</a></li>
		<li class="active">Add New Cms</li>
	</ol>
</section>

<section class="content"> 
<?php echo e(Form::open(['role' => 'form','url' => 'admin/cms-manager/add-cms','class' => 'mws-form'])); ?>

	<div class="row">
		<div class="col-md-6">	
			<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
				<div class="mws-form-row">
					<?php echo e(Form::label('name',trans("Page Name").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::text('name','', ['class' => 'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('name'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">	
			<div class="form-group <?php echo ($errors->first('title')?'has-error':''); ?>">
				<div class="mws-form-row">
					<?php echo e(Form::label('title',trans("Page title").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::text('title','', ['class' => 'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('title'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
		<div class="form-group <?php echo ($errors->first('title')?'has-error':''); ?>">
			<div class="mws-form-row">
				<?php echo e(Form::label('meta_title',trans("Meta title").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					<?php echo e(Form::text('meta_title','', ['class' => 'form-control'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('meta_title'); ?>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('meta_description')?'has-error':''); ?>">
				<div class="mws-form-row">
					<?php echo e(Form::label('meta_description',trans("Meta Description").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::text('meta_description','', ['class' => 'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('meta_description'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('meta_keywords')?'has-error':''); ?>">
				<div class="mws-form-row">
					<?php echo e(Form::label('meta_keywords',trans("Meta Keywords").' *', ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						<?php echo e(Form::text('meta_keywords','', ['class' => 'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('meta_keywords'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<div class="form-group <?php echo ($errors->first('body')?'has-error':''); ?>">
			<div class="mws-form-row">
				<?php echo e(Form::label('body',trans("Description").' *', ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
				<?php echo e(Form::textarea("body",'', ['class' => 'form-control textarea_resize','id' => 'body' ,"rows"=>3,"cols"=>3])); ?>

					<script type="text/javascript">
					/* For CKEDITOR */
						CKEDITOR.replace( <?php echo 'body'; ?>,
						{
							height: 250,
							width: 507,
							filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
							filebrowserImageWindowWidth : '640',
							filebrowserImageWindowHeight : '480',
							enterMode : CKEDITOR.ENTER_BR
						});
							
					</script>
					<div class="error-message help-inline">
						<?php echo $errors->first('body'); ?>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="mws-button-row">
				<input type="submit" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger">
				
				<a href="<?php echo e(URL::to('admin/cms-manager/add-cms')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans('Reset')); ?></a>
				
				<a href="<?php echo e(URL::to('admin/cms-manager')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans('Cancel')); ?></a>
			</div>
		</div>
	</div>
<?php echo e(Form::close()); ?> 
</section>
<style>
	.textarea_resize {
		resize: vertical;
	}
</style>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/Cms/add.blade.php ENDPATH**/ ?>
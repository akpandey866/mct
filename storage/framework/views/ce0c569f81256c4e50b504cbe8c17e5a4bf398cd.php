<?php $__env->startSection('content'); ?>

<section class="content-header">
	<h1>
	  <?php echo e(trans("Lockout")); ?>

	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Lockout")); ?></li>
	</ol>
</section>
<section class="content"> 
	
	<div class="row">
		<?php echo e(Form::open(['role' => 'form','url' => 'admin/admin-lockout','class' => 'mws-form',"method"=>"get"])); ?>

		<?php echo e(Form::hidden('display')); ?>	
			<div class="col-md-2 col-sm-12">
				<div class="form-group ">  
					<?php echo e(Form::select('game_mode',[null => 'Please Select mode'] +Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''),['id' => 'mode','class'=>'form-control choosen_selct'])); ?>

				</div>
			</div>	
			<div class="col-md-2 col-sm-12">
				<div class="form-group put_html">  
					<?php echo e(Form::select('club',[null => 'Please Select Club'],((isset($searchVariable['club'])) ? $searchVariable['club'] : ''),['id' => 'club','class'=>'form-control choosen_selct'])); ?>

				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> <?php echo e(trans('Search')); ?></button>
				<a href="<?php echo e(URL::to('admin/admin-lockout')); ?>"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> <?php echo e(trans("Reset")); ?></a>
			</div>
		<?php echo e(Form::close()); ?>

		<!-- <div class="col-md-5 col-sm-12">
			<div class="form-group">  
				<a href="<?php echo e(URL::to('admin/add-admin-lockout')); ?>" class="btn btn-success btn-small  pull-right" style="margin:0;"><?php echo e(trans("Add New lockout")); ?> </a>
			</div>
		</div>	 -->	
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">
							<?php echo e(link_to_route(
									"lockout.adminIndex",
									trans("Start Day"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									"lockout.adminIndex",
									trans("End Day"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									"lockout.adminIndex",
									trans("Start Time"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
							<?php echo e(link_to_route(
									"lockout.adminIndex",
									trans("End Time"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
						<tr class="items-inner">
							<td> <?php echo e($record->lockout_start_day); ?> </td>
							<td> <?php echo e($record->lockout_end_day); ?> </td>
							<td> <?php echo e($record->lockout_start_time); ?> </td>
							<td> <?php echo e($record->lockout_end_time); ?> </td>
							<td data-th='Action'>
								<a title="<?php echo e(trans('Edit')); ?>" href="<?php echo e(URL::to('admin/admin-edit-lockout/'.$record->id)); ?>" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>								
							</td>
						</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="5" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?>
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div> 
</section> 
<script> 
$("#mode").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"<?php echo e(url('admin/get-club-mode-listing')); ?>",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});
/*adter search club searhcing*/
var mode = "<?php  echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode']:'';  ?>";
var searchClubVar = "<?php  echo !empty($searchVariable['club']) ? $searchVariable['club']:'';  ?>";
if(mode !=''){ 
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"<?php echo e(url('admin/get-club-mode-listing')); ?>",
		'type':'post',
		data:{'id':mode,'club_id':searchClubVar},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
}

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/common/admin_lockout.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<style type="text/css">
.info_box_custom	{
	max-height: 200px;
	min-height: 200px;
    width: 100%;
    overflow-y: auto;
}
</style>
<div class="row pad">
	<div class="col-md-3">
		<div class="info-box bg-orange">
            <div class="info-box-conten23t" style="margin-left: 10px;padding-top: 10px;">
              <span class="info-box-text" style="margin-top: 10px;">Platform Fee</span>
              <span class="info-box-number">$<small><?php echo e($price); ?></small></span>
            </div>
          </div>
	</div>
</div>
<!-- <div class="row pad">

	<div class="col-md-3">
		<div class="info-box bg-green">
            <div class="info-box-content" style="margin-left: 0px;padding-top: 10px;">
              <span class="info-box-text1">MCT19 Launch Discount on Platform Fee </span>
              <span class="info-box-number"><small><?php echo e(BIRDDISCOUNT); ?>%</small></span>
            </div>
          </div>
	</div>
</div>
<div class="row pad">

	<div class="col-md-3">
		<div class="info-box bg-blue">
            <div class="info-box-content12" style="margin-left: 10px;padding-top: 10px;">
              <span class="info-box-text">  Discount Platform Fee</span>
              <span class="info-box-number">$<small><?php echo e($offeredPrice); ?></small></span>
            </div>
          </div>
	</div>
</div> -->
	<?php if($is_branding_activated == 1): ?>
		<div class="row pad">
			<div class="col-md-3">
				<div class="info-box bg-blue">
		            <div class="info-box-content12" style="margin-left: 10px;padding-top: 10px;">
		              <span class="info-box-text">Branding Activation Fee</span>
		              <span class="info-box-number">$<small><?php echo e($brandingPrice); ?></small></span>
		            </div>
		          </div>
			</div>
		</div>
	<?php endif; ?>
	<?php if($extraPlayer != 0): ?>
		<div class="row pad">
			<div class="col-md-3">
				<div class="info-box bg-purple">
		            <div class="info-box-content12" style="margin-left: 10px;padding-top: 10px;">
		              <span class="info-box-text"><?php echo e($extraPlayer); ?> New Player Records Added</span>
		              <span class="info-box-number">$<small><?php echo e($extraPlayerPrice); ?></small></span>
		            </div>
		          </div>
			</div>
		</div>
	<?php endif; ?>
 <?php 
$brandingFeeApplied = !empty($is_branding_activated) ? $brandingPrice :0; 
if(!empty($brandingFeeApplied) || !empty($extraPlayerPrice) ){ ?>

 <div class="row pad">
	<div class="col-md-3">
		<div class="info-box bg-red">
            <div class="info-box-content12" style="margin-left: 10px;padding-top: 10px;">
              <span class="info-box-text">Total Price</span>
              <span class="info-box-number">$<small><?php echo e($brandingFeeApplied+$extraPlayerPrice+$price); ?></small></span>
            </div>
          </div>
	</div>
</div>
<?php } ?>
<?php if(Auth::guard('admin')->user()->is_game_activate == 0): ?> 

<div class="row pad" >
	<div class="col-md-3 col-sm-3 col-xs-3 float-left">
		<div class="bg-red1 bg-red"> <?php echo implode('', $errors->all('<div style="margin-left: 10px;">:message</div>')); ?></div>
		<div class="info-box-contentsdsd">
			<?php echo e(Form::open(['role' => 'form','url' => 'admin/activate-game','class' => 'mws-form','files'=>'true', 'id' => 'activate_game'])); ?>

			<input type="button" value="<?php echo e(trans('Pay & Activate')); ?>" class="btn btn-success btn-lg flot-left" onclick="activateGame();">
			<?php echo e(Form::close()); ?>

		</div>

	</div>
</div>
<?php else: ?>
<div class="row pad" >
	<div class="col-md-3 col-sm-3 col-xs-3 float-left">
		<div class="info-box-contentsdsd">
			<span class="label label-success" style="margin: 3px 3px;"><b>Game is activated.</b></span>
		</div>

	</div>
</div>
<?php endif; ?>
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="panel-heading display-table" >
						<div class="row display-tr" >
							<h3 class="panel-title display-td" >Payment Details</h3>
							<div class="display-td" >                            
								<img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
							</div>
						</div>
					</div>
			</div>
			<div class="modal-body append_data"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var club = "<?php echo auth()->guard('admin')->user()->id; ?>";
var premiumMember = "<?php echo $premiumMember ?>";
var extraPlayer = "<?php echo !empty($extraPlayer) ? $extraPlayer :0; ?>"
function callStripeModal(){
	$('#loader_img').show();
		$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:' <?php echo e(url("admin/activate-game")); ?> ',
		'type':'post',
		data:{'club':club},
		success:function(response){ 
			error_array     =   JSON.stringify(response);
            data            =   JSON.parse(error_array);
            $('#loader_img').hide();
            if(data['success'] == 0) {
            	$('.bg-red1').html(data['data']);
            }else{
            	$.ajax({
					headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					url:' <?php echo e(url("admin/save-activate-game")); ?> ',
					'type':'post',
					data:{'extraPlayer':extraPlayer,'club':9},
					success:function(response){ 
						$('.append_data').html(response);
						$('#myModal').modal('show');
						$('#loader_img').hide();
					}
				});
            }
		}
	});
}
function activateGame (){
	var seniorGameMode = "<?php echo e(auth()->guard('admin')->user()->game_mode); ?>";
	callStripeModal();
	/*if(seniorGameMode == 1){
		if(premiumMember > 0){
		callStripeModal();
		}else{ 
	        url = $(this).attr('href');
	        bootbox.confirm("<b>You haven't activated premium member without it you can't trade and add booster.<br>Are you sure want to go without it ?</b>",
	        function(result){
	            if(result){
	                callStripeModal();
	            }
	        });
		}
	}else{
		callStripeModal();
	}*/
	
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/common/show_activate_game.blade.php ENDPATH**/ ?>
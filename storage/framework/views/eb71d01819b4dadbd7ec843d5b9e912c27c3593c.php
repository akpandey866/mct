<?php $__env->startSection('content'); ?>
<?php 
use Carbon\Carbon; 
?>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


<div class="full_container">
<div class="body_section">
    <!-- top_header element -->
    <?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>      



<section class="status_tab_block mb-5">
            <div class="container mobile_cont">
                   <?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="tab_content_sec border p-4">
                    <div class="next_prev_link_sec d-flex align-items-center mb-5">
         <?php if($cur_gameweek_no > 1 && $prev_page_active): ?> 
                   
                        <a href="<?php echo e(route('Common.points')); ?>/<?php echo e($cur_gameweek_no-1); ?>"><i class="fas fa-angle-left mr-2"></i>Previous Gameweek</a>
                    <?php else: ?>
                        <a></a>
                    <?php endif; ?>
                        <!-- <a></a> -->
                        <span class="d-inline-flex mx-5">GW# <?php echo e($cur_gameweek_no); ?> </span>
                        <!-- <a></a> -->
                    <?php if($cur_gameweek_date->copy()->addWeeks(2)->startOfDay() <= Carbon::now()->startOfDay()): ?>
                        <a href="<?php echo e(route('Common.points')); ?>/<?php echo e($cur_gameweek_no+1); ?>">Next Gameweek<i class="fas fa-angle-right ml-2"></i></a>
                    <?php else: ?>
                        <a></a>
                    <?php endif; ?>
                    </div>
                    <!-- <div class="game_info_block mt-4">
                        <div class="row">
                            <div class="col-12 col-lg-5">
                                <div class="game_user_info_block d-flex">
                                    <div class="game_user_pic">
                                        <img class="mb-2" src="img/game_user_pic.jpg" alt="game user">
                                        <a class="howuse_btn d-flex align-items-center justify-content-center" href=""><i class="far fa-play-circle mr-2"></i>How To Use</a>
                                    </div>
                                    <div class="w-100 game_info pl-4">
                                        <div class="game_user_name d-flex align-items-center flex-wrap mb-2">David White <img class="ml-2" src="img/right.png" alt="right"><small class="mt-2">David Superstar XI</small></div>
                                        <ul class="w-100 game_info_list py-2 mt-2">
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> View Gameweeks</li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> View Gameweek History</li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> View Traders History</li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> See Rules</li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> See Points</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-7 pt-4 pt-lg-0">
                                <ul class="game_week_info_list d-flex my-n2 mb-4">
                                    <li class="d-inline-flex align-items-center p-3 my-2">
                                        <div class="w-100 game_week_info d-flex align-items-center">
                                            <div class="game_week_info_icon text-center"><img src="img/game_week_info_icon7.png" alt="game_week_info"></div>
                                            <div class="game_week_info_status pl-2">4577 <small class="mt-1">Overall Pts.</small></div>
                                        </div>
                                    </li>
                                    <li class="d-inline-flex align-items-center p-3 my-2">
                                        <div class="w-100 game_week_info d-flex align-items-center">
                                            <div class="game_week_info_icon text-center"><img src="img/game_week_info_icon8.png" alt="game_week_info"></div>
                                            <div class="game_week_info_status pl-2">59 <small class="mt-1">Overall Rank</small></div>
                                        </div>
                                    </li>
                                    <li class="d-inline-flex align-items-center p-3 my-2">
                                        <div class="w-100 game_week_info d-flex align-items-center">
                                            <div class="game_week_info_icon text-center"><img src="img/game_week_info_icon9.png" alt="game_week_info"></div>
                                            <div class="game_week_info_status pl-2">581 <small class="mt-1">Total Users</small></div>
                                        </div>
                                    </li>
                                    <li class="d-inline-flex align-items-center p-3 my-2">
                                        <div class="w-100 game_week_info d-flex align-items-center">
                                            <div class="game_week_info_icon text-center"><img src="img/game_week_info_icon10.png" alt="game_week_info"></div>
                                            <div class="game_week_info_status pl-2">1000 <small class="mt-1">Gameweek pts.</small></div>
                                        </div>
                                    </li>
                                    <li class="d-inline-flex align-items-center p-3 my-2">
                                        <div class="w-100 game_week_info d-flex align-items-center">
                                            <div class="game_week_info_icon text-center"><img src="img/game_week_info_icon11.png" alt="game_week_info"></div>
                                            <div class="game_week_info_status pl-2">$5.50 <small class="mt-1">Value</small></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div> -->

<?php $tmpmatch = []; $i = 0; ?>
 <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php  preg_match_all('!\d+\.*\d*!', $temp->svalue ,$match);   $tmpmatch[] = end($match)[0]; $i++;  ?>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<div class="game_info_block mt-4">
            <div class="row">
               <div class="col-12 col-lg-5">
                  <div class="game_user_info_block d-flex">
                     <div class="game_user_pic">
                        <!-- <img class="mb-2" src="<?php echo e(asset('uploads/club/'.Auth::user()->club_logo)); ?>" alt="game user"> -->
           <?php if(!empty(auth()->guard('web')->user()->image) && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.auth()->guard('web')->user()->image)): ?>
                      <img class="mb-2" src="<?php echo e(asset('uploads/club/'.auth()->guard('web')->user()->image)); ?>" alt="game user">
                                <?php else: ?>
                      <img class="mb-2" src="<?php echo e(asset('img/user_pic.jpg')); ?>" alt="game user">
                                <?php endif; ?>
                                


                        <!-- <a class="howuse_btn d-block d-sm-flex align-items-center justify-content-center" href="https://myclubtap.freshdesk.com/a/solutions/articles/43000536360" target="_blank"><i class="far fa-play-circle mr-2"></i><span class="how_mobile">How To Use</span></a> -->
                     </div>
                     <div class="w-100 game_info pl-4">
                        <div class="game_user_name d-flex align-items-center flex-wrap mb-0"><?php echo e(Auth::user()->full_name); ?> 
                        <!--   <img class="ml-2" src="img/right.png" alt="right"> -->
                    
        </div>
         <div class="game_user_name d-flex align-items-center flex-wrap mb-0">
          <small class="mt-2"><?php echo e(Auth::user()->my_team_name); ?></small> 
         </div>
                        <ul class="w-100 game_info_list py-2 mt-2">
                         <!--  <li class="px-3 py-1"><i class="fas fa-angle-right"></i> View Gameweeks</li>
                           <li class="px-3 py-1"><i class="fas fa-angle-right"></i> View Gameweek History</li>
                           <li class="px-3 py-1"><i class="fas fa-angle-right"></i> View Traders History</li> -->
                           <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a target="_blank" href="<?php echo e(url('/pages/game-rules')); ?>">See Rules</a></li>
                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a target="_blank" href="<?php echo e(url('/pages/faq')); ?>">See FAQ</a></li>
                           <li class="px-3 py-1" style="cursor: pointer;" data-toggle="modal" data-target="#club_points_model" ><i class="fas fa-angle-right"></i> See Points</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-12 col-lg-7 pt-4 pt-lg-0">
                  <ul class="game_week_info_list d-flex my-n2 mb-4 mobile_teamdeta  liborderblock">
                     <li class="d-inline-flex align-items-center p-3 my-2">
                        <div class="w-100 game_week_info d-flex align-items-center">
                           <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon7.png')); ?>" alt="game_week_info"></div>
                           <!-- <div class="game_week_info_status pl-2"><?php echo e(intval($userteam_fantasy_points)); ?> <small class="mt-1">Overall Pts.</small></div> -->
                           <div class="game_week_info_status pl-2">

                            <?php echo e(!empty($gameweek_team_value->overall_pt) ? $gameweek_team_value->overall_pt : 0); ?>



                            <small class="mt-1">Overall Pts.</small></div>
                        </div>
                     </li>
                     <li class="d-inline-flex align-items-center p-3 my-2">
                        <div class="w-100 game_week_info d-flex align-items-center">
                           <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon8.png')); ?>" alt="game_week_info"></div>
                           <div class="game_week_info_status pl-2">
                            

                            <?php echo e(!empty($gameweek_team_value->overall_rank) ? $gameweek_team_value->overall_rank : 0); ?>




                            <small class="mt-1">Overall Rank</small></div>
                        </div>
                     </li>
                     <li class="d-inline-flex align-items-center p-3 my-2">
                        <div class="w-100 game_week_info d-flex align-items-center">
                           <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon9.png')); ?>" alt="game_week_info"></div>
                           <div class="game_week_info_status pl-2"><?php echo e(!empty($tot_club_users) ? $tot_club_users : 0); ?> <small class="mt-1">Game Users</small></div>
                        </div>
                     </li>
                     <li class="d-inline-flex align-items-center p-3 my-2">
                        <div class="w-100 game_week_info d-flex align-items-center">
                           <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon10.png')); ?>" alt="game_week_info"></div>
                           <div class="game_week_info_status pl-2">
                            <?php echo e(!empty($gameweek_team_value->gw_pt) ? $gameweek_team_value->gw_pt : 0); ?>



                            <small class="mt-1">Gameweek pts.</small></div>
                        </div>
                     </li>
                     <li class="d-inline-flex align-items-center p-3 my-2">
                        <div class="w-100 game_week_info d-flex align-items-center">
                           <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon11.png')); ?>" alt="game_week_info"></div>
                    <?php /*   <div class="game_week_info_status pl-2"> $<?php echo  isset($gameweek_team_value) ? $gameweek_team_value : number_format((float)array_sum($tmpmatch), 2, '.', '') ; ?> <small class="mt-1">Value (in $m)</small></div> */ ?>

                    <div class="game_week_info_status pl-2"> <?php echo  !empty($gameweek_team_value_utvl) ? '$'.$gameweek_team_value_utvl->team_value : '--' ; ?> <small class="mt-1">Value (in $m)</small></div>

                        </div>
                     </li>
                     <li class="d-inline-flex align-items-center p-3 my-2">
                        <div class="w-100 game_week_info d-flex align-items-center">
                           <div class="game_week_info_icon text-center"><img src="<?php echo e(asset('img/game_week_info_icon11.png')); ?>" alt="game_week_info"></div>
                           <div class="game_week_info_status pl-2"><?php echo e(!empty($no_of_trade) ? $no_of_trade : 0); ?>/<?php echo e(MAX_NO_OF_TRADE); ?> <small class="mt-1">Free Trades</small></div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>




                    <div class="match_info_block border-top p-4 mt-5 mx-n4">
                        <div class="row">
                            <div class="col-12 col-lg-8 my-3">
                                <div class="row weekly_table_sec">
                                    <div class="col-12 col-md-6">
                                        <h3 class="players_title d-flex align-items-center mb-3">Trades In this week
            <a href="javascript:void();" data-toggle="modal" data-target="#top_trade_in_modal" class="more_link border ml-auto">More</a>
                                        </h3>
                                        <div class="table-responsive mt-3">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                                <tbody>
                                                    <tr>
                                                        <th>Player</th>
                                                        <th>Position</th>
                                                    </tr>
                                            <?php $i = 0; ?>
                                            <?php if(!empty($top_trade_in)): ?>
                                                <?php $__currentLoopData = $top_trade_in; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                <?php 
                                                    $i++; 
                                                    if($i>12) break; 
                                                ?>

                                                    <tr>
                                                        <td><span class="pl-2">
                                <?php echo e(!empty($val->first_name) ? ucfirst(substr($val->first_name, 0, 2).'. ') : ''); ?>

                                <?php echo e(!empty($val->last_name) ? $val->last_name : ''); ?>                                                            
                                                        </span></td>
                                                        <td><?php echo e($position[$val->position]); ?></td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                            <?php else: ?>
                                                <tr><td colspan="2">No record is yet available.</td></tr>
                                            <?php endif; ?>                                                


                                                <?php /*

                                                    <tr>
                                                        <td><i class="fas fa-star yellow_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Atwater Andrew</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star yellow_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Arnold Culver</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star yellow_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Barnaby</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Andrew Atwater</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Culver Barnaby</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Atwater Andrew</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Arnold Culver</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Barnaby</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Andrew Atwater</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Barnaby</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Andrew Atwater</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Barnaby</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Andrew Atwater</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    */ ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 pt-5 pt-md-0">
                                        <h3 class="players_title d-flex align-items-center mb-3">Trades out this week
     <a href="javascript:void();" data-toggle="modal" data-target="#top_trade_out_modal" class="more_link border ml-auto">More</a>
                                        </h3>
                                        <div class="table-responsive mt-3">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                                <tbody>
                                                    <tr>
                                                        <th>Player</th>
                                                        <th>Position</th>
                                                    </tr>
                                                    <?php $i = 0; ?>
                                            <?php if(!empty($top_trade_out)): ?>
                                                <?php $__currentLoopData = $top_trade_out; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php 
                                                $i++; 
                                                if($i>12) break; 
                                                ?>
                                                    <tr>
                                                        <td><span class="pl-2">
                               <?php echo e(!empty($val->first_name) ? ucfirst(substr($val->first_name, 0, 2).'. ') : ''); ?>

                                <?php echo e(!empty($val->last_name) ? $val->last_name : ''); ?>

                                                        </span></td>
                                                        <td><?php echo e($position[$val->position]); ?></td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            <?php else: ?>
                                                <tr><td colspan="2">No record is yet available.</td></tr>
                                            <?php endif; ?>                                                

                                                <?php /*
                                                    <tr>
                                                        <td><i class="fas fa-star yellow_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Atwater Andrew</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star yellow_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Arnold Culver</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star yellow_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Barnaby</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Andrew Atwater</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Culver Barnaby</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Atwater Andrew</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Arnold Culver</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Barnaby</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Andrew Atwater</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Barnaby</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Andrew Atwater</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Barnaby</span></td>
                                                        <td>BOWL</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-star grey_color mx-1"></i><i class="fas fa-bars grey_color mx-1"></i><span class="pl-2">Andrew Atwater</span></td>
                                                        <td>BAT</td>
                                                    </tr>
                                                    */ ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 my-3">
                                <!-- <div class="lineup-header">
                                    <div class="grid-header">
                                        <div class="view_list">
                                            <a class="active" href="#"><i class="fas fa-th-large"></i></a>
                                            <a href="#"><i class="fas fa-list"></i></a>
                                        </div>
                                        <h6><span>11/11</span> Selected</h6>
                                        <div class="refresh">
                                            <a href="#"><i class="fas fa-redo-alt"></i></a>
                                        </div>
                                    </div>

                                    <div class="team-tab">
                                        <div class="team-tab-inner">
                                            <span class="sponsor"><img src="img/brought_by_img.png" alt=""></span>
                                            <div class="ground-wrapper less-height  gridv ">
                                                <div class="team-filedview">
                                                    <ul>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/wc.png" alt="" height="60">
                                                            <img src="img/captain.png" alt="" class="caption-img">
                                                            <span class="player-position">WK</span>
                                                            <div class="player-name"> Clark </div>
                                                            <div class="player-points"><span class="team-salary">$0.10 M</span></div>
                                                        </li>
                                                    </ul>

                                                    <ul>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/bm.png" alt="" height="60">
                                                            <span class="player-position">BAT</span>
                                                            <div class="player-name"> Frey </div>
                                                            <div class="player-points"> <span class="team-salary">$0.25 M</span></div>
                                                        </li>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/bm.png" alt="" height="60">
                                                            <span class="player-position">BAT</span>
                                                            <div class="player-name"> Frey </div>
                                                            <div class="player-points"> <span class="team-salary">$0.25 M</span></div>
                                                        </li>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/bm.png" alt="" height="60">
                                                            <span class="player-position">BAT</span>
                                                            <div class="player-name"> Frey </div>
                                                            <div class="player-points"> <span class="team-salary">$0.25 M</span></div>
                                                        </li>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/bm.png" alt="" height="60">
                                                            <span class="player-position">BAT</span>
                                                            <div class="player-name"> Frey </div>
                                                            <div class="player-points"> <span class="team-salary">$0.25 M</span></div>
                                                        </li>
                                                    </ul>

                                                    <ul>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/ar.png" alt="" height="60">
                                                            <span class="player-position">AR</span>
                                                            <div class="player-name"> Frey </div>
                                                            <div class="player-points"> <span class="team-salary">$0.25 M</span></div>
                                                        </li>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/ar.png" alt="" height="60">
                                                            <span class="player-position">AR</span>
                                                            <div class="player-name"> Frey </div>
                                                            <div class="player-points"> <span class="team-salary">$0.25 M</span></div>
                                                        </li>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/ar.png" alt="" height="60">
                                                            <span class="player-position">AR</span>
                                                            <div class="player-name"> Frey </div>
                                                            <div class="player-points"> <span class="team-salary">$0.25 M</span></div>
                                                        </li>
                                                    </ul>

                                                    <ul>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/bow.png" alt="" height="60">
                                                            <span class="player-position">Bow</span>
                                                            <div class="player-name"> Frey </div>
                                                            <div class="player-points"> <span class="team-salary">$0.25 M</span></div>
                                                        </li>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/bow.png" alt="" height="60">
                                                            <span class="player-position">Bow</span>
                                                            <div class="player-name"> Frey </div>
                                                            <div class="player-points"> <span class="team-salary">$0.25 M</span></div>
                                                        </li>
                                                        <li>
                                                            <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                                            <img src="img/bow.png" alt="" height="60">
                                                            <span class="player-position">Bow</span>
                                                            <div class="player-name"> Frey </div>
                                                            <div class="player-points"> <span class="team-salary">$0.25 M</span></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="d-block btn btn-secondary btn-linup w-100 rounded-0">Update Lineup</button>
                                    </div>
                                </div> -->

<div class="lineup-header">
                     <div class="grid-header">
                        <div class="view_list">
                           <a class="active grid_view" href="javascript:void(0);"><i class="fas fa-th-large"></i></a>
                           <a  class="list_view"  href="javascript:void(0);"><i class="fas fa-list"></i></a>
                        </div>
                        <h6><span class="tot_player_in_11">11/11</span> Selected</h6>
 
                        <div id="refresh_btn" class="refresh">
<?php if(!$lockout_club): ?>                           
                           <a href="javascript:void(0); "><i class="fas fa-redo-alt"></i></a>
<?php endif; ?>
                        </div>

                     </div>
                     <!-- start play ground image ************** -->
                     <div class="team-tab grid_view_tab">
                        <div class="team-tab-inner">
                           <span class="sponsor">

                        <?php if(!empty($sess_club_name) && !empty($sess_club_name->brand_image) && File::exists(BRANDING_IMAGE_ROOT_PATH.$sess_club_name->brand_image)): ?>
                        
                        <a target="_blank" href="<?php echo e(!empty($sess_club_name->brand_url) ? $sess_club_name->brand_url : '#'); ?>">
                        <img  src="<?php echo e(BRANDING_IMAGE_URL.$sess_club_name->brand_image); ?>" alt="brought_by"> 
                        </a>
                        
                        <?php elseif(!empty($sess_club_name) && !empty($sess_club_name->club_logo)  && File::exists(CLUB_IMAGE_ROOT_PATH.$sess_club_name->club_logo)): ?>

                        <img src="<?php echo e(CLUB_IMAGE_URL.$sess_club_name->club_logo); ?>" alt="">
                        
                        <?php else: ?>


                        <img src="<?php echo e(asset('img/no_image.png')); ?>" alt="">
                        
                        <?php endif; ?>


                            

                          </span>
                           <span class="sponsor_right">
                            <img src="<?php echo e(asset('img/logo_white.png')); ?>" alt="">
                           </span>
                           
                            <?php if($booster_active['name'] == '3x' && $booster_active['status'] == true): ?>
                            <span class="sponsor_right_power">
                              <div>
                               <img class="booster_img" src="<?php echo e(asset('img/booster_yellow1.svg')); ?>" alt="booster_circle" />
                                 <span class="three_blocks">3X
                                <!-- <i booster_type="3x" style="position: absolute; cursor: pointer;" class="fas fa-times remove_booster"></i> -->
                              </span>
                             </div>
                            
                            </span>
                            <?php endif; ?>
                            <?php if($booster_active['name'] == '12th' && $booster_active['status'] == true): ?>
                            
                            <span class="sponsor_right_power">
                              <div>
                               <img class="booster_img" src="<?php echo e(asset('img/booster_yellow1.svg')); ?>" alt="booster_circle" />
                                 <span class="three_blocks">12th
                                <!-- <i booster_type="3x" style="position: absolute; cursor: pointer;" class="fas fa-times remove_booster"></i> -->
                              </span>
                             </div>
                            
                            </span>


                            <?php endif; ?>


                            <?php if($booster_active['name'] == 'Dealer' && $booster_active['status'] == true): ?>
                            
                            <span class="sponsor_right_power">
                              <div>
                               <img class="booster_img" src="<?php echo e(asset('img/booster_yellow1.svg')); ?>" alt="booster_circle" />
                                 <span class="three_blocks">Dlr.
                                <!-- <i booster_type="3x" style="position: absolute; cursor: pointer;" class="fas fa-times remove_booster"></i> -->
                              </span>
                             </div>
                            
                            </span>


                            <?php endif; ?>



                          
                           <span class="sponsor_bottom_text text-center"><?php echo e(!empty($sess_club_name->game_name) ? $sess_club_name->game_name : ''); ?></span>
                           <div class="ground-wrapper less-height  gridv ">
                              <div class="team-filedview">
                                 <ul class="wk_div">
                                    <!-- <li>
                                       <span class="remove-player d-none"><i class="fas fa-minus"></i></span>
                                       <img src="img/wc.png" alt="" height="60">
                                       <img src="img/captain.png" alt="" class="caption-img">
                                       <span class="player-position">WK</span>
                                       <div class="player-name"> Clark </div>
                                       <div class="player-points"><span class="team-salary">$0.10 M</span></div>
                                       </li> -->
                                    <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($temp->position == 4 ): ?>
                                    <li id="img<?php echo e($temp->id); ?>">
<?php if(!$lockout_club): ?> 
                                       <i class="fas fa-times remove-player"></i>
<?php endif; ?>
                                       <img src="<?php echo e(asset('img/wc.png')); ?>" alt="" height="60" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                                       <img src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img <?php echo e($capton_id == $temp->id ? 'capt_img_active' : ''); ?>">
                                       <img src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img <?php echo e($vice_capton_id == $temp->id ? 'vice_capt_img_active' : ''); ?>">
                                       <span class="player-position">WK</span>
               <span class="player-points-position"><?php echo e(isset($club_players_fantasy_points[$temp->id]) ? round($club_players_fantasy_points[$temp->id]) : 0); ?>pt</span> 


                                       <div class="player-name"> <?php echo e($temp->full_name); ?> </div>
                        
     <div class="player-points"><span class="team-salary"><?php echo e(isset($gtv_players_price[$temp->id]) ? '$'.number_format((float)$gtv_players_price[$temp->id], 2, '.', '').'m' : '--'); ?></span></div>




                                    </li>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 </ul>
                                 <ul class="bat_div">
                                   
                                    <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($temp->position == 1 ): ?>
                                    <li id="img<?php echo e($temp->id); ?>">
<?php if(!$lockout_club): ?> 
                                       <i class="fas fa-times remove-player"></i>
<?php endif; ?>
                                       <img src="<?php echo e(asset('img/bm.png')); ?>" alt="" height="60"  style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                                       <img  src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img <?php echo e($capton_id == $temp->id ? 'capt_img_active' : ''); ?>">
                                       <img  src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img <?php echo e($vice_capton_id == $temp->id ? 'vice_capt_img_active' : ''); ?>">
                                       <span class="player-position">BAT</span>
            <span class="player-points-position"><?php echo e(isset($club_players_fantasy_points[$temp->id]) ? round($club_players_fantasy_points[$temp->id]) : 0); ?>pt</span> 
    
                                       <div class="player-name"> <?php echo e($temp->full_name); ?> </div>
         
        <div class="player-points"><span class="team-salary"><?php echo e(isset($gtv_players_price[$temp->id]) ? '$'.number_format((float)$gtv_players_price[$temp->id], 2, '.', '').'m' : '--'); ?></span></div>
                                    </li>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>   
                                   
                                 </ul>
                                 <ul class="ar_div">
                                   
                                    <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($temp->position == 3 ): ?>
                                    <li id="img<?php echo e($temp->id); ?>">
<?php if(!$lockout_club): ?> 
                                       <i class="fas fa-times remove-player"></i>
<?php endif; ?>
                                       <img src="<?php echo e(asset('img/ar.png')); ?>" alt="" height="60" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                                       <img  src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img <?php echo e($capton_id == $temp->id ? 'capt_img_active' : ''); ?>">
                                       <img  src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img <?php echo e($vice_capton_id == $temp->id ? 'vice_capt_img_active' : ''); ?>">
                                       <span class="player-position">AR</span>
         <span class="player-points-position"><?php echo e(isset($club_players_fantasy_points[$temp->id]) ? round($club_players_fantasy_points[$temp->id]) : 0); ?>pt</span> 
     
                                       <div class="player-name"> <?php echo e($temp->full_name); ?> </div>
        
           <div class="player-points"><span class="team-salary"><?php echo e(isset($gtv_players_price[$temp->id]) ? '$'.number_format((float)$gtv_players_price[$temp->id], 2, '.', '').'m' : '--'); ?></span></div>
                                    </li>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   
                                 </ul>
                                 <ul class="bow_div">
                                   
                                    <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($temp->position == 2 ): ?>
                                    <li id="img<?php echo e($temp->id); ?>">
<?php if(!$lockout_club): ?> 
                                       <i class="fas fa-times remove-player"></i>
<?php endif; ?>
                                       <img src="<?php echo e(asset('img/bow.png')); ?>" alt="" height="60" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" class=" dt_player_name" player_id="<?php echo e($temp->id); ?>">
                                       <img  src="<?php echo e(asset('img/captain.png')); ?>" alt="" class="caption-img capt_img <?php echo e($capton_id == $temp->id ? 'capt_img_active' : ''); ?>">
                                       <img  src="<?php echo e(asset('img/v_captain.png')); ?>" alt="" class="caption-img vice_capt_img <?php echo e($vice_capton_id == $temp->id ? 'vice_capt_img_active' : ''); ?>">
                                       <span class="player-position">Bow</span>
         <span class="player-points-position"><?php echo e(isset($club_players_fantasy_points[$temp->id]) ? round($club_players_fantasy_points[$temp->id]) : 0); ?>pt</span> 
            
                                       <div class="player-name"> <?php echo e($temp->full_name); ?> </div>
        
           <div class="player-points"><span class="team-salary"><?php echo e(isset($gtv_players_price[$temp->id]) ? '$'.number_format((float)$gtv_players_price[$temp->id], 2, '.', '').'m' : '--'); ?></span></div>
                                    </li>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   
                                 </ul>
                              </div>
                           </div>
                        </div>
                      <?php if(!$lockout_club): ?>  

                        <button id="update_my_team_player_imagebtn" class="d-block btn btn-secondary btn-linup w-100 rounded-0">UPDATE LINEUP</button>
                      <?php endif; ?>
                     </div>
                     <!-- stop play ground image ************* -->
                     <!-- start listing player --> 
                     <div  class="list_view_tab" style="display: none;">
                        <ul>
                         <!--   <li>
                              <hr>
                           </li> -->
                           <li class="player_heading"><b>WK</b></li>
                           <div id="prefix_wk">
                              <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php if($temp->position == 4 ): ?>
                              <li  class="player_data" id="list<?php echo e($temp->id); ?>"><?php echo e($temp->full_name); ?>


                                
   <span class="pl_price"><?php echo e(isset($gtv_players_price[$temp->id]) ? '$'.number_format((float)$gtv_players_price[$temp->id], 2, '.', '').'m' : '--'); ?></span>

                                <span class="c_text"><?php echo e($capton_id == $temp->id ? ' (C) ' : ''); ?></span><span class="vc_text"><?php echo e($vice_capton_id == $temp->id ? ' (VC) ' : ''); ?></span></li>
                              <?php endif; ?>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </div>
                    <!--        <li>
                              <hr>
                           </li> -->
                           <li  class="player_heading"><b>BAT</b></li>
                           <div id="prefix_bat">
                              <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php if($temp->position == 1 ): ?>
                              <li  class="player_data"  id="list<?php echo e($temp->id); ?>"><?php echo e($temp->full_name); ?>


                       
                <span class="pl_price"><?php echo e(isset($gtv_players_price[$temp->id]) ? '$'.number_format((float)$gtv_players_price[$temp->id], 2, '.', '').'m' : '--'); ?></span>


                                <span class="c_text"><?php echo e($capton_id == $temp->id ? ' (C) ' : ''); ?></span><span class="vc_text"><?php echo e($vice_capton_id == $temp->id ? ' (VC) ' : ''); ?></span></li>
                              <?php endif; ?>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </div>
                   <!--         <li>
                              <hr>
                           </li> -->
                           <li class="player_heading"><b>AR</b></li>
                           <div id="prefix_ar">
                              <?php $__currentLoopData = $chosen_player; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $temp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php if($temp->position == 3 ): ?>
                              <li  class="player_data"  id="list<?php echo e($temp->id); ?>"><?php echo e($temp->full_name); ?>


                

                <span class="pl_price"><?php echo e(isset($gtv_players_price[$temp->id]) ? '$'.number_format((float)$gtv_players_price[$temp->id], 2, '.', '').'m' : '--'); ?></span>


                                <span class="c_text"><?php echo e($capton_id == $temp->id ? ' (C) ' : ''); ?></span><span class="vc_text"><?php echo e($vice_capton_id == $temp->id ? ' (VC) ' : ''); ?></span></li>
                              <?php endif; ?>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </div>
                   <!--         <li>
                              <hr>
                           </li> -->
                        </ul>
                        <!-- stop liting player -->
                      <?php if(!$lockout_club): ?>  
                                
                        <button  id="update_my_team_player"  class="d-block btn btn-secondary btn-linup w-100 rounded-0">UPDATE LINEUP</button>
                      <?php endif; ?>
                     </div>
                     <!-- <a id="update_my_team_player" class="howuse_btn ml-auto my-2" href="javascript:void(0);">Update Line</a> -->
                     <?php /* 
                     <div class="row align-items-center pt-3">
                         <div class="col-12"><label class="mb-1">Team Name:</label></div>
                         <div class="col-12"><input type="text" class="form-control" id="my_team_name" value="{{ !empty($my_team_name) ? $my_team_name : ''}}" placeholder="Enter Team Name*" /></div>
                     </div>
                     */ ?>
                     <div id="success_msg" style="display: none; color: green; ">Team updated successfully.</div>
                     <div id="error_msg" style="display: none; color: red; ">Failed!, Pelase try again.</div>
                      <div id="teamname_required_msg" style="display: none; color: red; ">Please enter team name and try again!</div>
                     
                     <div id="error_msg_server" style="display: none; color: red; ">Error!, Pelase refresh the page.</div>
                     <img id="loading_img" style="display: none; margin-top: 3px; " src="<?php echo e(asset('img/loading_img.gif')); ?>" />
                  </div>






                            </div>
                        </div>
                    </div>
                    <!-- <div class="boosters_block border-top p-4 mx-n4">
                        <div class="d-flex align-items-center">
                            <h4 class="status_title my-2">Boosters</h4>
                            <a class="howuse_btn ml-auto my-2" href=""><i class="far fa-play-circle mr-2"></i>More About Booster</a>
                        </div>
                        <ul class="booster_list d-flex flex-wrap justify-content-center mt-4">
                            <li class="red_circle d-inline-flex position-relative p-2">
                                <img class="booster_img" src="img/booster_red.svg" alt="booster_circle" />
                                <div class="booster_circle_data d-flex align-items-center justify-content-center">
                                    <div class="text-center">1.5x <small>Player Cards</small><span>0/5</span></div>
                                </div>
                                <div class="booster_circle_hoverdata">Lorem Ipsum is simply dummy text.</div>
                            </li>
                            <li class="yellow_circle1 d-inline-flex position-relative p-2">
                                <img class="booster_img" src="img/booster_yellow1.svg" alt="booster_circle" />
                                <div class="booster_circle_data d-flex align-items-center justify-content-center">
                                    <div class="text-center">3x <small>Caption Cards</small><span>0/5</span></div>
                                </div>
                                <div class="booster_circle_hoverdata">Lorem Ipsum is simply dummy text.</div>
                            </li>
                            <li class="blue_circle d-inline-flex position-relative p-2">
                                <img class="booster_img" src="img/booster_blue.svg" alt="booster_circle" />
                                <div class="booster_circle_data d-flex align-items-center justify-content-center">
                                    <div class="text-center">Trade <small>Trade Cards</small><span>0/5</span></div>
                                </div>
                                <div class="booster_circle_hoverdata">Lorem Ipsum is simply dummy text.</div>
                            </li>
                            <li class="yellow_circle1 d-inline-flex position-relative p-2">
                                <img class="booster_img" src="img/booster_yellow1.svg" alt="booster_circle" />
                                <div class="booster_circle_data d-flex align-items-center justify-content-center">
                                    <div class="text-center">12th <small>12th Man Cards</small><span>0/5</span></div>
                                </div>
                                <div class="booster_circle_hoverdata">Lorem Ipsum is simply dummy text.</div>
                            </li>
                            <li class="yellow_circle2 d-inline-flex position-relative p-2">
                                <img class="booster_img" src="img/booster_yellow2.svg" alt="booster_circle" />
                                <div class="booster_circle_data d-flex align-items-center justify-content-center">
                                    <div class="text-center">Flip <small>Flipper Card</small><span>0/5</span></div>
                                </div>
                                <div class="booster_circle_hoverdata">Lorem Ipsum is simply dummy text.</div>
                            </li>
                        </ul>
                    </div> -->


<div class="boosters_block border-top p-4 mx-n4">
            <div class="d-block d-sm-flex align-items-center">
               <h4 class="status_title my-2">Powers

<a href="javascript:void();" data-html="true" data-toggle="tooltip" title="You can use a maximum of 1 of your powers only during a trading period."><i style="color: #007bff; " class="fa fa-info-circle" aria-hidden="true"></i></a>
               </h4>
               <!-- <a class="howuse_btn ml-auto my-2" href="https://myclubtap.freshdesk.com/a/solutions/articles/43000534304" target="_blank"><i class="far fa-play-circle mr-2"></i>More About Powers</a> -->
            </div>
            <ul class="booster_list d-flex flex-wrap mt-4 mobile_teamdeta justify-content-center ">
               
               <li class="yellow_circle1 d-inline-flex position-relative p-2 " >
                  <img class="booster_img" src="<?php echo e(asset('img/booster_yellow1.svg')); ?>" alt="booster_circle" />
                  <div class="booster_circle_data d-flex align-items-center justify-content-center">
                      <div class="text-center">Triple Cap <small></small><span><?php echo e(!empty($user_teams_data->capton_card_count) ? $user_teams_data->capton_card_count : 0); ?>/<?php echo e(MAX_NO_OF_CAPTON_CARDS); ?></span></div>
                  </div>
                  <!-- <div class="booster_circle_hoverdata">3 Times of Captain points.</div> -->

                   <?php if($booster_active['name'] == '3x' && $booster_active['status'] == true): ?>
                    <div class="booster_circle_hoverdata">APPLIED</div>
                   <?php elseif($booster_active['status'] == true): ?>
                    <div class="booster_circle_hoverdata">LOCKED</div>
                   <?php else: ?>
                    <div class="booster_circle_hoverdata">LOCKED</div>
                   <?php endif; ?>

                  
               </li>


              <li class="yellow_circle2 d-inline-flex position-relative p-2 " >
                  <img class="booster_img" src="<?php echo e(asset('img/booster_yellow2.svg')); ?>" alt="booster_circle" />
                  <div class="booster_circle_data d-flex align-items-center justify-content-center">
                      <div class="text-center">12th Man<small></small><span><?php echo e(!empty($user_teams_data->twelve_man_card_count) ? $user_teams_data->twelve_man_card_count : 0); ?>/<?php echo e(MAX_NO_OF_12_MAN_CARDS); ?></span></div>
                  </div>
                  <!-- <div class="booster_circle_hoverdata">Add an extra 12th player in Team.</div> -->
                   <?php if($booster_active['name'] == '12th' && $booster_active['status'] == true): ?>
                    <div class="booster_circle_hoverdata">APPLIED</div>
                   <?php elseif($booster_active['status'] == true): ?>
                    <div class="booster_circle_hoverdata">LOCKED</div>
                   <?php else: ?>
                    <div class="booster_circle_hoverdata">LOCKED</div>
                   <?php endif; ?>
               </li>




<li class="blue_circle d-inline-flex position-relative p-2  ">

                  <img class="booster_img" src="<?php echo e(asset('img/booster_blue.svg')); ?>" alt="booster_circle" />

<div class="booster_circle_data d-flex align-items-center justify-content-center">
                      <div class="text-center">Dealer<small></small><span><?php echo e(!empty($user_teams_data->dealer_card_count) ? $user_teams_data->dealer_card_count : 0); ?>/<?php echo e(MAX_NO_OF_DEALER_CARDS); ?></span></div>
                  </div>

                  <!-- <div class="booster_circle_hoverdata">Extra trade after 20 free trade.</div> -->

                   <?php if($booster_active['name'] == 'Dealer' && $booster_active['status'] == true): ?>
                    <div class="booster_circle_hoverdata">APPLIED</div>
                   <?php else: ?>
                    <div class="booster_circle_hoverdata">LOCKED</div>
                   <?php endif; ?>
               </li>





<li class="red_circle d-inline-flex position-relative p-2  ">

                  <img class="booster_img" src="<?php echo e(asset('img/booster_red.svg')); ?>" alt="booster_circle" />
                  <div class="booster_circle_data d-flex align-items-center justify-content-center">
                      <div class="text-center">Flipper <small></small><span><?php echo e(!empty($user_teams_data->flipper_card_count) ? $user_teams_data->flipper_card_count : 0); ?>/<?php echo e(MAX_NO_OF_FLIPPER_CARDS); ?></span></div>
                  </div>
                  <!-- <div class="booster_circle_hoverdata">Change full team for free.</div> -->
                   <?php if($booster_active['name'] == 'Flipper' && $booster_active['status'] == true): ?>
                    <div class="booster_circle_hoverdata">APPLIED</div>
                   <?php else: ?>
                    <div class="booster_circle_hoverdata">LOCKED</div>
                   <?php endif; ?>
               </li>
               

              


               <li class="yellow_circle2 d-inline-flex position-relative p-2">
                  <img class="booster_img" src="<?php echo e(asset('img/booster_yellow2.svg')); ?>" alt="booster_circle" />
                  <div class="booster_circle_data d-flex align-items-center justify-content-center">
                      <div class="text-center">Shield Steal <small></small><span></span></div>
                  </div>
                  <!-- <div class="booster_circle_hoverdata">Steal points from others.</div> -->
                  <!-- <div class="booster_circle_hoverdata">Coming Soon</div> -->

                   <?php if($booster_active['status'] == true): ?>
                    <div class="booster_circle_hoverdata">LOCKED</div>
                   <?php else: ?>
                    <div class="booster_circle_hoverdata">LOCKED</div>
                   <?php endif; ?>

               </li>




            </ul>
         </div>





<div class="boosters_block border-top p-4 mx-n4">
            <div class="d-block d-sm-flex align-items-center">
               <h4 class="status_title my-2">GW Fixtures

<!-- <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Fixtures in this gameweek."><i style="color: #007bff; " class="fa fa-info-circle" aria-hidden="true"></i></a> -->
               </h4>
               <!-- <a class="howuse_btn ml-auto my-2" href="https://myclubtap.freshdesk.com/a/solutions/articles/43000534304" target="_blank"><i class="far fa-play-circle mr-2"></i>More About Powers</a> -->
            </div>
            <div class="table-responsive table-mobile">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                                <tbody>
                                               <tr>
                                                        <th>Date</th>
                                                        <th>Team</th>
                                                        <th>Grade</th>
                                                        <th>Scorecard</th>
                                                </tr>
                                             
                                                                                                                                                        
                                             <?php if(!empty($result_fix) && $result_fix->isNotEmpty()): ?>
                                                    <?php $__currentLoopData = $result_fix; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php //prd($v); ?>
                                                <?php 

                                                // if(!empty($v->start_date) && Carbon::parse($v->start_date) <= Carbon::now()){
                                                //     $icnt++;  
                                                // }
                                                ?>

                                                <tr>
                                                    <td><?php echo e(!empty($v->start_date) ? Carbon::parse($v->start_date)->format('d.m.y') : ''); ?></td>
                                                    <td><?php echo e(ucwords($v->team_name)); ?></td>
                                                    <td><?php echo e(!empty($v->grade_name) ? ucwords($v->grade_name) : '-'); ?></td>
                                                    <!-- <td><?php echo e($v->fixture_scorecard->sum('fantasy_points')); ?></td>    -->
                                                    <td>
                                                        <!-- <a href="<?php echo e(route('Global.matchScorecard',$v->id)); ?>" class="more_link border ml-auto"  data-toggle="modal" data-target="#myModal<?php echo e($v->id); ?>">Scorecard</a> -->

                                                        <a href="<?php echo e(route('Global.matchScorecard',$v->id)); ?>" class="more_link border ml-auto">Scorecard</a>
                                                    </td>
                                                </tr>



                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>

                                            <tr><td colspan="4">No fixtures available in this gameweek.</td></tr>
                                            <?php endif; ?>



                                                    
                                     
                                                </tbody>
                                            </table>
                                        </div>
         </div>




                </div>
            </div>
        </section>


        <!-- <section class="fixtures_sec mb-3">
            <div class="container">
                <div class="fixtures_block border">
                    <div class="fixtures_heading d-block d-sm-flex py-3 px-4">
                        <div class="mr-auto">Fixtures</div>
                        <div class="ml-auto">Gameweek 2</div>
                    </div>
                    <div class="fixtures_data_block py-4 px-5">
                        <div class="fixtures_table_slide owl-carousel">
                            <div class="item">
                                <h3 class="players_title d-flex align-items-center mb-3">Fri - 10 June 2019 </h3>
                                <div class="table-responsive">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                        <tbody>
                                            <tr>
                                                <th>Post <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th>
                                                <th>Team <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th>
                                                <th>Value</th>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>ABC Superstarts</td>
                                                <td>$5.95m</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>ABC Superstarts</td>
                                                <td>$2.95m</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>ABC Superstarts</td>
                                                <td>$3.95m</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>ABC Superstarts</td>
                                                <td>$7.95m</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>ABC Superstarts</td>
                                                <td>$4.95m</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="item">
                                <h3 class="players_title d-flex align-items-center mb-3">Sat  - 11 June 2019 </h3>
                                <div class="table-responsive">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                        <tbody>
                                            <tr>
                                                <th>Post <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th>
                                                <th>Team <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th>
                                                <th>Value</th>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>ABC Superstarts</td>
                                                <td>$5.95m</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>ABC Superstarts</td>
                                                <td>$2.95m</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>ABC Superstarts</td>
                                                <td>$3.95m</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>ABC Superstarts</td>
                                                <td>$7.95m</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>ABC Superstarts</td>
                                                <td>$4.95m</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="item">
                                <h3 class="players_title d-flex align-items-center mb-3">Fri - 10 June 2019 </h3>
                                <div class="table-responsive">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                        <tbody>
                                            <tr>
                                                <th>Post <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th>
                                                <th>Team <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th>
                                                <th>Value</th>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>ABC Superstarts</td>
                                                <td>$5.95m</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>ABC Superstarts</td>
                                                <td>$2.95m</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>ABC Superstarts</td>
                                                <td>$3.95m</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>ABC Superstarts</td>
                                                <td>$7.95m</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>ABC Superstarts</td>
                                                <td>$4.95m</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <div class="container pt-4">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="<?php echo e(asset('img/play_now_banner.jpg')); ?>" alt="my_team_banner"></a>
            </div>
        </div>

<?php /*

    <section class="status_tab_block mb-5">
        <div class="container">
            <!-- navbar element -->
            @include('front.elements.navbar')
            <div class="tab_content_sec border p-4">
                    <div class="d-block d-md-flex align-items-center mb-4">
                        <h4 class="status_title mr-auto">Points</h4>
                        <!-- <form action="" class="statistics_form ml-auto">
                            <div class="row">
                                <div class="col-12 col-sm-6 py-2">
                                    <select class="custom-select">
                                        <option value="">All Players</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6 py-2">
                                    <select class="custom-select">
                                        <option value="">Fantasy Points</option>
                                    </select>
                                </div>
                            </div>
                        </form> -->
                    </div>
                    <div class="table-responsive">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table states_table mb-0">

                            <tr>
                                <th width="70px">Rank</th>
                                <th width="40%">Player <!-- <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span> --></th>
                                <th>Position <!-- <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span> --></th>
                               <!--  <th>Picked In Teams <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th> -->
                                <th>Fantasy Pts</th>
                            </tr>


                        <?php $i = 1; ?>
                        @foreach($result as $val)

                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    <span class="player d-flex align-items-center flex-nowrap">
                                        <img class="player_pic" src="{{asset('uploads/player/'.$val->image)}}" alt="player">
                                        <span class="player_name pl-3">{{ $val->full_name }}</span>
                                    </span>
                                </td>
                                <td>{{$position[$val->position]}}</td>
                                <!-- <td>{{$val->selected_by}}%</td> -->
                                <td>{{!empty($val->score_count) ? $val->score_count : 0}}</td>
                            </tr>
                        @endforeach


   
                        </table>
                    </div>
                </div>
    </section>
        <div class="container">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
            </div>
        </div>


        */ ?>
    </div>
</div>


<div class="modal fade" id="top_trade_in_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Trades in this week</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<!-- <div class="col-12 col-md-6 my-3"> -->
                    <!-- <h3 class="players_title d-flex align-items-center mb-3">Top Players by Gameweeks <a href="javascript:void();" data-toggle="modal" data-target="#top_player_by_gameweek_modal" class="more_link border ml-auto">More</a></h3> -->
<div class="table-responsive mt-3">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                                <tbody>
                                                    <tr>
                                                        <th>Player</th>
                                                        <th>Position</th>
                                                    </tr>
                                            <?php if(!empty($top_trade_in)): ?>
                                                <?php $__currentLoopData = $top_trade_in; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><span class="pl-2">
                                <?php echo e(!empty($val->first_name) ? ucfirst(substr($val->first_name, 0, 2).'. ') : ''); ?>

                                <?php echo e(!empty($val->last_name) ? $val->last_name : ''); ?>                                                            
                                                        </span></td>
                                                        <td><?php echo e($position[$val->position]); ?></td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                            <tr><td colspan="2">No record is yet available.</td></tr>
                                            <?php endif; ?>

                                                </tbody>
                                            </table>
                                        </div>
              <!--   </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="top_trade_out_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Trades out this week</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<!-- <div class="col-12 col-md-6 my-3"> -->
                    <!-- <h3 class="players_title d-flex align-items-center mb-3">Top Players by Gameweeks <a href="javascript:void();" data-toggle="modal" data-target="#top_player_by_gameweek_modal" class="more_link border ml-auto">More</a></h3> -->
<div class="table-responsive mt-3">
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                                <tbody>
                                                    <tr>
                                                        <th>Player</th>
                                                        <th>Position</th>
                                                    </tr>

                                            <?php if(!empty($top_trade_out)): ?>
                                                <?php $__currentLoopData = $top_trade_out; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><span class="pl-2">
                               <?php echo e(!empty($val->first_name) ? ucfirst(substr($val->first_name, 0, 2).'. ') : ''); ?>

                                <?php echo e(!empty($val->last_name) ? $val->last_name : ''); ?>

                                                        </span></td>
                                                        <td><?php echo e($position[$val->position]); ?></td>
                                                    </tr>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php else: ?>
                                                <tr><td colspan="2">No record is yet available.</td></tr>
                                            <?php endif; ?>

                                                </tbody>
                                            </table>
                                        </div>
              <!--   </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <!--     <h4 class="modal-title" id="myModalLabel">Confirmar</h4> -->
      </div>
      <div class="modal-body">
       <h5>Do you want to use your Triple Cap power?</h5> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="modal-btn-si">OK</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="twelve-man-modal">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <!--     <h4 class="modal-title" id="myModalLabel">Confirmar</h4> -->
      </div>
      <div class="modal-body">
       <h5>Do you want to use your 12th Man power?</h5> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="twelve-man-modal-btn-si">OK</button>
        <button type="button" class="btn btn-primary" id="twelve-man-modal-btn-no">Cancel</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="club_points_model" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Point System</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal Header</h4> -->
        </div>
        <div class="modal-body">
         


<div class="table-responsive table-mobile">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                        <tr>
                                            <th>CATEGORY</th>
                                            <th>BOWL</th>
                                            <th>BAT/WK/AR</th>
                                        </tr>
                                        <?php   
                                    if(!empty($club_points) && $club_points->isNotEmpty()){
                                     foreach($club_points as $key => $val){  ?>
                                        <tr>
                                            <td><?php echo e($val->attribute_name); ?></td>
                                            <td><?php echo e($val->bowler); ?></td>
                                            <td><?php echo e($val->bats_wk_ar); ?></td>
                                        </tr>
                                        <?php }
                                     }else{ ?>
                                        <tr>
                                            <td colspan="4"><?php echo e(trans("No record is yet available.")); ?></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div>



        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>




<?php /*

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">


<section class="status_tab_block mb-2">
            <div class="">
                
                <div class="player_star"><img src="{{asset('img/player_star_bg.jpg')}}" alt="player"></div>
                <div class="tab_content_sec border">
                    <div class="player_star_info">
                        <div class="player_star_pic position-relative mb-3"><img style="width: 200px; height: 130px; " class="player_image" src="{{asset('img/player_star.jpg')}}" alt="player_star"></div>
                        <h3 class="player_star_name text-center"><small class="player_name"></small><span class="club_name"></span></h3>
                    </div>
                    <div class="player_star_data game_info_block py-4 mx-3 mx-md-5">
                        <ul class="game_week_info_list d-flex my-n2 mb-4">
                            <li class="d-inline-flex align-items-center p-3 my-2">
                                <div class="w-100 game_week_info d-flex align-items-center">
                                    <div class="game_week_info_icon text-center"><img src="{{asset('img/game_week_info_icon10.png')}}" alt="game_week_info"></div>
                                    <div class="game_week_info_status pl-2"><span class="player_fps"></span> <small class="mt-1">Fantasy Pts</small></div>
                                </div>
                            </li>
                            <li class="d-inline-flex align-items-center p-3 my-2">
                                <div class="w-100 game_week_info d-flex align-items-center">
                                    <div class="game_week_info_icon text-center"><img src="{{asset('img/game_week_info_icon11.png')}}" alt="game_week_info"></div>
                                    <div class="game_week_info_status pl-2"><span class="player_value"></span> <small class="mt-1">Value</small></div>
                                </div>
                            </li>
                            <li class="d-inline-flex align-items-center p-3 my-2">
                                <div class="w-100 game_week_info d-flex align-items-center">
                                    <div class="game_week_info_icon text-center"><img src="{{asset('img/game_week_info_icon9.png')}}" alt="game_week_info"></div>
                                    <div class="game_week_info_status pl-2"><span class="selected_by"></span>% <small class="mt-1">Selected by</small></div>
                                </div>
                            </li>
                        </ul>
                        
                        <div class="games_tabs_sec mt-5">
                            <ul class="nav nav-tabs d-flex" role="tablist">
                              <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#form" role="tab" aria-selected="true">Form</a></li>
                              <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#game_log" role="tab" aria-selected="false">Game Log</a></li>
                            </ul>
                            <div class="tab-content border p-4 mb-3">
                              <div class="tab-pane fade show active" id="form">
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                              </div>
                              <div class="tab-pane fade" id="game_log">
<div class="table-responsive">                                
<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Runs</th>
      <th scope="col">4s</th>
      <th scope="col">6s</th>
      <th scope="col">Maiden</th>
      <th scope="col">Wks</th>
      <th scope="col">FP</th>      
    </tr>
  </thead>
  <tbody class="player_last_5">

  </tbody>
</table>
</div>

                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>








      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" id="temptemptemp" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

*/ ?>

<script type="text/javascript">
  
/*
var chartt = Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
    
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Fantasy Points'
        }
    },
    // tooltip: {
    //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
    //         '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
    //     footerFormat: '</table>',
    //     shared: true,
    //     useHTML: true
    // },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: '',
        data: []

    }, 
    // {
    //     name: 'New York',
    //     data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

    // }, {
    //     name: 'London',
    //     data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

    // }, {
    //     name: 'Berlin',
    //     data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

    // }
    ]
});




// $('#temptemptemp').click(function() {
//   chartt.series[0].setData([48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2],true);
//   // chartt.series[1].setData([49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],true);
//   // chartt.series[2].setData([49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],true);
//   // chartt.series[3].setData([49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],true);
//   chartt.xAxis[0].setCategories(['Jan', 'Jan', 'Jan', 'Jan', 'Jan', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']);
// });





// $('.dt_player_name').click(function() {
   $(document).on("click",".dt_player_name",function() { 

   var player_id =  $(this).closest('tr').find('.action_plus_minus i').attr('player_id'); 
   if(!player_id){
    player_id =  $(this).attr('player_id'); 
   }

    $.ajax({
      url: "<?php echo e(route('User.getPlayerData')); ?>", // 'get-player-data',
      type: 'POST',
      data: {_token: '<?php echo e(csrf_token()); ?>', player_id: player_id},
      dataType: 'HTML',
      success: function (data) { 
        var obj = $.parseJSON(data);
        // console.log(obj.player_name); 
        $('#exampleModal .player_name').text(obj.player_name);
        // $('#exampleModal .club_name').text('Western Australia');
        if(obj.player_image)
          $('#exampleModal img.player_image').attr('src', '<?php echo e(asset("uploads/player")); ?>/'+obj.player_image);
        $('#exampleModal .player_fps').text(obj.fantasy_points);
        $('#exampleModal .player_value').text(obj.player_price);

        // console.log(obj); 
        var table_str = ''; 
        obj.last_5_record.forEach(function (item, index) {
          table_str += '<tr>'
          table_str += '<td>'+(item.runs ? item.runs : 0)+'</td>'; 
          table_str += '<td>'+(item.four ? item.four : 0)+'</td>'; 
          table_str += '<td>'+(item.six ? item.six : 0)+'</td>'; 
          table_str += '<td>'+(item.maiden ? item.maiden : 0)+'</td>'; 
          table_str += '<td>'+(item.wks ? item.wks : 0)+'</td>'; 
          table_str += '<td>'+(item.fp ? item.fp : 0)+'</td>'; 
          table_str += '</tr>'
        });
        if(!table_str)  table_str +='<tr><td align="center" colspan="6">No record is yet available.</td></tr>' 
        $('#exampleModal .selected_by').text(obj.selected_by); 
        $('#exampleModal .player_last_5').html(table_str); 
// player_last_5
obj.last_10_record.fx_month_fp = obj.last_10_record.fx_month_fp.map( Number );
chartt.series[0].setName(obj.player_name,true);
 chartt.series[0].setData(obj.last_10_record.fx_month_fp,true);
  chartt.xAxis[0].setCategories(obj.last_10_record.fx_month);



      }
   }); 



});

$('#exampleModal').on('hidden.bs.modal', function () {
  $('#exampleModal .player_name').text('');
 $('#exampleModal img.player_image').attr('src', '<?php echo e(asset("img/logo_white.png")); ?>');
  $('#exampleModal .player_fps').text('');
  $('#exampleModal .player_value').text('');
  $('#exampleModal .selected_by').text(''); 
   $('#exampleModal .player_last_5').html(''); 
chartt.series[0].setName([],true);
 chartt.series[0].setData([],true);
  chartt.xAxis[0].setCategories([]);   
})


$('.append_club_name select').on('change', function(){
    $(this).closest('form').submit();
});


*/


/*
$( "#capton_point_x_3" ).click(function() {


      club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }
      
      // if(parseInt('<?php echo e(MAX_NO_OF_CAPTON_CARDS); ?>') >= 5){
      //   showMessage('Your max. limit of capton card reached.',"error");
      //   return false;
      // }


  toastr.success("<button type='button' id='confirm_capton_point_x_3' style='padding: 1px 2px; line-height: 1; ' class='btn clear'>OK</button> &nbsp; &nbsp; <button type='button'  style='padding: 1px 2px; line-height: 1; '  class='btn clear'>Cancel</button>",'Activate Capton Card?',
  {
      closeButton: false,
      preventDuplicates: true,
     preventOpenDuplicates: true,
      allowHtml: true,
      timeOut: 110000 ,
      onShown: function (toast) {
          $("#confirm_capton_point_x_3").click(function(){
            console.log('helllo'); 
            $.ajax({
              url: 'activate-capton-card',
              type: 'POST',
              data: {_token: '<?php echo e(csrf_token()); ?>',  club_id: club_id},
              dataType: 'HTML',
              success: function (data) { 
                if(data == 1){
                  showMessage('Capton Card activated successfully.',"success");
                }else if(data == 0){
                  showMessage('To activate Capton card, please create your Team first.',"error");
                }else if(data == 2){
                  showMessage('Capton card already active for this week.',"error");
                }else if(data == 3){
                  showMessage('Your max. limit of capton card reached.',"error");
                }else {
                  showMessage('Internal error occured.',"error");
                }
              }
            }); 
          });
        }
  });

});

*/ 
</script>



<!-- <script type="text/javascript">


$( ".not_active_booster" ).click(function() {
  showMessage('Booster is not active.',"error");
});


$( ".already_active_booster" ).click(function() {
  if($(this).hasClass('active')){
     showMessage('This Booster is already active.',"error");
  }else{
     showMessage('More than one booster can not be active at same time.',"error");
  }
});


var modalConfirm = function(callback){
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  $(".capton_point_x_3").on("click", function(){

      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }

    $("#mi-modal").modal({
      show: true,
      backdrop: 'static',
      keyboard: false
  });
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};

modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
    // $("#result").html("CONFIRMADO");

club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');

      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }


            $.ajax({
              url: 'activate-capton-card',
              type: 'POST',
              data: {_token: '<?php echo e(csrf_token()); ?>',  club_id: club_id},
              dataType: 'HTML',
              success: function (data) { 
                if(data == 1){
                  showMessage('Triple Cap has been applied!',"success");

                  setTimeout(function(){
                      location.reload(true);
                  }, 3000);

                }else if(data == 0){
                  showMessage('To activate Captain card, please create your Team first.',"error");
                }else if(data == 2){
                  showMessage('Triple Cap has already been activated and used for this trading period. You cannot deactivate or reuse it anymore!',"error");
                }else if(data == 3){
                  showMessage('Your max. limit of Captain card reached.',"error");
                }else {
                  showMessage('Internal error occured.',"error");
                }
              }
            });

  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});





var twelve_man_modalConfirm = function(callback){
  club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');
  $(".twelve_man_card").on("click", function(){

  
 
      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }

    $("#twelve-man-modal").modal({
      show: true,
      backdrop: 'static',
      keyboard: false
  });
  });

  $("#twelve-man-modal-btn-si").on("click", function(){
    callback(true);
    $("#twelve-man-modal").modal('hide');
  });
  
  $("#twelve-man-modal-btn-no").on("click", function(){
    callback(false);
    $("#twelve-man-modal").modal('hide');
  });
};

twelve_man_modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
    // $("#result").html("CONFIRMADO");

club_id = parseInt('<?php echo e(!empty($sess_club_name) ? $sess_club_name->id : 0); ?>');

      if(!club_id){
        // toastr.error('Please choose club to create team.'); 
        showMessage('Please choose club to create team.',"error");
        return false;
      }


            $.ajax({
              url: 'activate-twelve-man-card',
              type: 'POST',
              data: {_token: '<?php echo e(csrf_token()); ?>',  club_id: club_id},
              dataType: 'HTML',
              success: function (data) { 
                // console.log(data); 
                if(data == 1){
                  showMessage('12th Man has been applied!',"success");

                  setTimeout(function(){
                      location.reload(true);
                  }, 3000);
                  
                  // setTimeout(function() { location.reload(); }, 4000);
                  twelve_player_select = true; 
                }else if(data == 0){
                  showMessage('To activate 12th Man Card, please create your Team first.',"error");
                }else if(data == 2){
                  showMessage('12th Man has already been activated and used for this trading period. You cannot deactivate or reuse it anymore!',"error");
                }else if(data == 3){
                  showMessage('Your max. limit of 12th Man Card reached.',"error");
                }else {
                  showMessage('Internal error occured.',"error");
                }
              }
            });

  }else{
    //Acciones si el usuario no confirma
    // $("#result").html("NO CONFIRMADO");
  }
});






</script> -->



<script type="text/javascript">
$(document).ready(function() {

  $('[data-toggle="tooltip"]').tooltip(); 
    $(document).on("click",".grid_view",function() { 
        $('.list_view_tab').css('display', 'none');
        $('.grid_view_tab').css('display', 'block');
        $('.grid_view').addClass('active'); 
        $('.list_view').removeClass('active');                         
    });                         
    // $('.list_view').click(function(){
    $(document).on("click",".list_view",function() { 
        $('.grid_view_tab').css('display', 'none').removeClass('active'); 
        $('.list_view_tab').css('display', 'block').addClass('active');                         
        $('.grid_view').removeClass('active'); 
        $('.list_view').addClass('active');                         
    });
}); 
</script>

<style type="text/css">
    
    ul.booster_list li{ cursor: pointer;}

</style>



<style type="text/css">
.list_view_tab .player_data span.pl_price{ float: right; margin-left: 20px;}
</style>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/front/common/points.blade.php ENDPATH**/ ?>
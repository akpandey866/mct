<?php $__env->startSection('content'); ?>

<script type="text/javascript">
$(function(){
	$('[data-delete]').click(function(e){
		
	     e.preventDefault();
		// If the user confirm the delete
		if (confirm('Do you really want to delete the element ?')) {
			// Get the route URL
			var url = $(this).prop('href');
			// Get the token
			var token = $(this).data('delete');
			// Create a form element
			var $form = $('<form/>', {action: url, method: 'post'});
			// Add the DELETE hidden input method
			var $inputMethod = $('<input/>', {type: 'hidden', name: '_method', value: 'delete'});
			// Add the token hidden input
			var $inputToken = $('<input/>', {type: 'hidden', name: '_token', value: token});
			// Append the inputs to the form, hide the form, append the form to the <body>, SUBMIT !
			$form.append($inputMethod, $inputToken).hide().appendTo('body').submit();
		} 
	});
});
</script>
<section class="content-header">
	<h1><?php echo e(trans('Setting')); ?></h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Setting")); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
		<div id="mws-themer-content" style="<?php echo ($searchVariable) ? 'right: 256px;' : ''; ?>">
		<div id="mws-themer-ribbon"></div>
		<div id="mws-themer-toggle" class="<?php echo ($searchVariable) ? 'opened' : ''; ?>">
			<i class="icon-bended-arrow-left"></i> 
			<i class="icon-bended-arrow-right"></i>
		</div>
		</div>
		<?php echo e(Form::open(['method' => 'get','role' => 'form','url' => 'admin/settings','class' => 'mws-form'])); ?>

		<?php echo e(Form::hidden('display')); ?>

		<div class="col-md-3 col-sm-3">
			<div class="form-group ">  
				<?php echo e(Form::text('title',((isset($searchVariable['title'])) ? $searchVariable['title'] : ''), ['class' => 'form-control', 'placeholder' => 'Title'])); ?>

			</div>
		</div>
		<!--<div class="mws-themer-separator"></div>
		<div class="mws-themer-section" style="height:0px">
			<ul><li class="clearfix"><span></span> <div id="mws-textglow-op"></div></li> </ul>
		</div>-->
		<div class="col-md-4 col-sm-4">
			<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
			<a href="<?php echo e(URL::to('admin/settings')); ?>" class="btn btn-primary"><i class='fa fa-refresh '></i> Reset</a>
		</div>
		<?php echo e(Form::close()); ?>

		<div class="col-md-5 col-sm-5 ">
			<div class="form-group pull-right">  
				<a href="<?php echo e(URL::to('admin/settings/add-setting')); ?>" class="btn btn-success btn-small align"><?php echo e(trans("Add New Setting")); ?> </a>
			</div>
		</div>
	</div>

	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="10%" >
						<?php echo e(link_to_route(
								'settings.listSetting',
								'Id',
								array(
									'sortBy' => 'id',
									'order' => ($sortBy == 'id' && $order == 'desc') ? 'asc' : 'desc'
								),
							   array('class' => (($sortBy == 'id' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'id' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)); ?>

						</th>
						<th width="30%">
						<?php echo e(link_to_route(
								'settings.listSetting',
								'Title',
								array(
									'sortBy' => 'title',
									'order' => ($sortBy == 'title' && $order == 'desc') ? 'asc' : 'desc'
								),
							   array('class' => (($sortBy == 'title' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'title' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)); ?>

						</th>
						<th width="20%">
						<?php echo e(link_to_route(
									'settings.listSetting',
									'Key',
									array(
										'sortBy' => 'key',
										'order' => ($sortBy == 'key' && $order == 'desc') ? 'asc' : 'desc'
									),
								   array('class' => (($sortBy == 'key' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'key' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="20%">
						<?php echo e(link_to_route(
								'settings.listSetting',
								'Value',
								array(
									'sortBy' => 'value',
									'order' => ($sortBy == 'value' && $order == 'desc') ? 'asc' : 'desc'
								),
							   array('class' => (($sortBy == 'value' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'value' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)); ?>

						</th>
						<th><?php echo e('Action'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php
						$key = $record->key;
						$keyE = explode('.', $key);
						$keyPrefix = $keyE['0'];
						if (isset($keyE['1'])) {
							$keyTitle = '.' . $keyE['1'];
						} else {
							$keyTitle = '';
						}
						?>
					<tr>
						<td data-th='Id'><?php echo e($record->id); ?></td>
						<td data-th='Title'><?php echo e($record->title); ?></td>
						<td data-th='Key'>
							<a target="_blank" href="<?php echo e(URL::to('admin/settings/prefix/'.$keyPrefix)); ?>" ><?php echo e($keyPrefix); ?></a><?php echo e($keyTitle); ?>

						</td>
						<td data-th='Value'>
							<?php echo e(strip_tags(Str::limit($record->value, 20))); ?>

						</td>	
						<td data-th='Action'>
							<a href="<?php echo e(URL::to('admin/settings/edit-setting/'.$record->id)); ?>" class="btn btn-info btn-small"><?php echo e(trans('Edit')); ?> </a>
							<a href="<?php echo e(URL::to('admin/settings/delete-setting/'.$record->id)); ?>"  class="btn btn-danger btn-small no-ajax delete_any_item"> <?php echo e(trans('Delete')); ?> </a>
						</td>
					</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="5" ><?php echo e(trans('No record is yet available.')); ?></td>
						</tr>
					<?php endif; ?> 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default',['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/settings/index.blade.php ENDPATH**/ ?>
<?php use Carbon\Carbon;?>

<?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('css/admin/fancybox/jquery.fancybox.js')); ?>"></script>
<script src="<?php echo e(asset('js/dreamimage.js')); ?>"></script>
<link href="<?php echo e(asset('css/admin/fancybox/jquery.fancybox.css')); ?>" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">

<style type="text/css">
  table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting {
    padding-right: 0px !important;
}
.position_img_style{
    width: 20px;
    height: : 20px;
}
#dreamland{
    z-index: 1000;
}
#dreamcontent+h1+span{
    right: 85px !important;
}
</style>
<div class="full_container">
	<div class="body_section">
		<!-- top_header element -->
		<?php echo $__env->make('front.elements.common_header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>      
		<section class="status_tab_block mb-5">
			<div class="container mobile_cont">
				<?php echo $__env->make('front.elements.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
				<div class="tab_content_sec border  p-0 p-md-4">
					<h4 class="status_title mb-3">Match Scorecard</h4>
					<div class="game_info_block mt-4">
                        <div class="row">
                            <div class="col-12 col-lg-8">
                                <div class="game_user_info_block d-flex">
                                    <!-- <div class="game_user_pic">
                                        <img class="mb-2" src="<?php echo e(CLUB_IMAGE_URL.$sess_club_name->club_logo); ?>" alt="game user">
                                    </div> -->
                                    <div class="w-100 game_info pl-0" style="padding-left: 0rem !important">
                                        <ul class="w-100 game_info_list d-flex flex-wrap mt-2">
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Team: <?php echo e($fixtureDetails->team_name); ?></li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Grade: <?php echo e($fixtureDetails->grade_name); ?></li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Opponent: <?php echo e($fixtureDetails->opposition_club); ?></li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Type: <?php echo e(get_team_type_from_fixture($fixtureDetails->club)); ?></li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Status:
                                                <?php 
                                                if(!empty($fixtureScorcardDetails->status)){ 
                                                    if($fixtureScorcardDetails->status == 1){
                                                    echo "Not yet started";
                                                    }elseif($fixtureScorcardDetails->status == 2){
                                                        echo "In-Progress";
                                                    }elseif($fixtureScorcardDetails->status == 3){
                                                        echo "Completed";
                                                    }
                                                }else{
                                                     echo "Not yet started"; 
                                                }
                                                ?>
                                            </li>
                                             <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Date: <?php echo e(Carbon::parse($fixtureDetails->start_date)->isoFormat('D.M.YY')); ?></li>

                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Match:
                                                <?php if($fixtureDetails->match_type == ONEDAYMATCH): ?>
                                                    1-Day
                                                <?php elseif($fixtureDetails->match_type == TWODAYMATCH): ?>
                                                    2-Day
                                                <?php endif; ?>
                                            </li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Venue: <?php echo e($fixtureDetails->vanue); ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 d-flex pt-4 pt-lg-0">
                                <div class="brought_by_img_block justify-content-start ml-auto">
                                    <?php if(!empty($sess_club_name) && !empty($sess_club_name->brand_image) && File::exists(BRANDING_IMAGE_ROOT_PATH.$sess_club_name->brand_image)): ?>
                                    <img style="width: 90px; height: 90px" src="<?php echo e(BRANDING_IMAGE_URL.$sess_club_name->brand_image); ?>" alt="brought_by"> 
                                    <?php else: ?>
                                    <img style="width: 90px; height: 90px" src="<?php echo e(asset('img/brand_icon.png')); ?>" alt="brought_by"> 
                                    <?php endif; ?>
                                    <span class="text-left mt-2" style="color:#000000;">

                                    Presented by</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="games_tabs_sec mt-5">
                        <ul class="nav nav-tabs d-flex" role="tablist">
                          <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#form" role="tab" aria-selected="true">1st Innings</a></li>
                          <?php if($fixtureDetails->match_type == TWODAYMATCH): ?>
                          <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#game_log" role="tab" aria-selected="false">2st Innings</a></li>
                          <?php endif; ?>
                        </ul>
                        <div class="tab-content border">
                          <div class="tab-pane fade show active table-responsive" id="form">
                                <table cellpadding="0" cellspacing="0" border="0" class="w-100 table table-striped table-bordered dt-responsive nowrap" id="example1">
                                    <thead>
                                        <tr>
                                            <th> Player</th>
                                            <th> R</th>
                                            <th> 4</th>
                                            <th> 6</th>
                                            <th> OVS</th>
                                            <th> MDS</th>
                                            <th> RG</th>
                                            <th> WK</th>
                                            <th> CS</th>
                                            <th> CW</th>
                                            <th> ST</th>
                                            <th> ROD</th>
                                            <th> ROA</th>
                                            <th> DK</th>
                                            <th> HT</th>
                                            <th> FPS</th>
                                        </tr>
                                    </thead>
                                    <tbody id="powerwidgetssd">
                                    <?php if($scorcard1->isNotEmpty()): ?>
                                        <?php $__currentLoopData = $scorcard1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td>
                                                     <?php 
                                                    $positionName = "";
                                                    if($record->player_position == 1){ ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Batsman"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>bat-icon.png"  /></a>
                                                   <?php }elseif($record->player_position == 2){ ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Bowler"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>ball-icon.png" /></a>
                                                  <?php  }elseif ($record->player_position == 3) { ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="All Rounder"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>bat-ball-icon.png" /></a>
                                                  <?php  }elseif ($record->player_position == 4) { ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Wicket-Keeper"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>gloves-icon.png" /></a>
                                                   <?php } ?>
                                               <a href="<?php echo e(route('Common.playerProfile',$record->player_id)); ?>" style="padding-right: 30px;">
                                                <?php echo e($record->player_name); ?></a>
                                                   
                                                </td>
                                                <td><?php echo e($record->rs); ?></td>
                                                <td><?php echo e($record->fours); ?></td>
                                                <td><?php echo e($record->sixes); ?></td>
                                                <td><?php echo e($record->overs); ?></td>
                                                <td><?php echo e($record->mdns); ?></td>
                                                <td><?php echo e($record->run); ?></td>
                                                <td><?php echo e($record->wks); ?></td>
                                                <td><?php echo e($record->cs); ?></td>
                                                <td><?php echo e($record->cwks); ?></td>
                                                <td><?php echo e($record->sts); ?></td>
                                                <td><?php echo e($record->rods); ?></td>
                                                <td><?php echo e($record->roas); ?></td>
                                                <td><?php echo e($record->dks); ?></td>
                                                <td><?php echo e($record->hattrick); ?></td>
                                                <td><?php echo e($record->fantasy_points); ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                        <tr><td colspan="16">No record is yet available.</td></tr>
                                    <?php endif; ?>


                                    </tbody>                    
                                </table>
                          </div>
                          <?php if($fixtureDetails->match_type == TWODAYMATCH): ?>
                          <div class="tab-pane fade" id="game_log">
                               <div class="tab-pane fade show active table-responsive" id="form-group">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0" id="example2">
                                    <thead>
                                        <tr>
                                            <tr>
                                            <th>Player</th>
                                            <th>R</th>
                                            <th>4</th>
                                            <th>6</th>
                                            <th>OVS</th>
                                            <th>MDS</th>
                                            <th>RG</th>
                                            <th>WK</th>
                                            <th>CS</th>
                                            <th>CW</th>
                                            <th>ST</th>
                                            <th>ROD</th>
                                            <th>ROA</th>
                                            <th>DK</th>
                                            <th>HT</th>
                                            <th>FPS</th>
                                        </tr>
                                        </tr>
                                    </thead>
                                    <tbody id="powerwidgetsas">
                                    <?php if($scorcard2->isNotEmpty()): ?>
                                        <?php $__currentLoopData = $scorcard2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php //prd($record); ?>
                                            <tr>
                                                <td>
                                                <?php 
                                                    $positionName = "";
                                                    if($record->player_position == 1){ ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Batsman"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>bat-icon.png" /></a>
                                                   <?php }elseif($record->player_position == 2){ ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Bowler"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>ball-icon.png" /></a>
                                                  <?php  }elseif ($record->player_position == 3) { ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="All Rounder"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>bat-ball-icon.png" /></a>
                                                  <?php  }elseif ($record->player_position == 4) { ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Wicket-Keeper"><img class="position_img_style" id="theImg" src="<?php echo e(WEBSITE_IMG_URL); ?>gloves-icon.png" /></a>
                                                   <?php } ?>
                                               <a href="<?php echo e(route('Common.playerProfile',$record->player_id)); ?>" style="padding-right: 30px;">
                                                <?php echo e($record->player_name); ?></a>
                                                </td>
                                                <td><?php echo e($record->rs); ?></td>
                                                <td><?php echo e($record->fours); ?></td>
                                                <td><?php echo e($record->sixes); ?></td>
                                                <td><?php echo e($record->overs); ?></td>
                                                <td><?php echo e($record->mdns); ?></td>
                                                <td><?php echo e($record->run); ?></td>
                                                <td><?php echo e($record->wks); ?></td>
                                                <td><?php echo e($record->cs); ?></td>
                                                <td><?php echo e($record->cwks); ?></td>
                                                <td><?php echo e($record->sts); ?></td>
                                                <td><?php echo e($record->rods); ?></td>
                                                <td><?php echo e($record->roas); ?></td>
                                                <td><?php echo e($record->dks); ?></td>
                                                <td><?php echo e($record->hattrick); ?></td>
                                                <td><?php echo e($record->fantasy_points); ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                        <tr><td colspan="16">No record is yet available.</td></tr>
                                    <?php endif; ?>


                                    </tbody>                    
                                </table>
                          </div>
                          <?php endif; ?>
                      
                        </div>
                    </div>
                    <form action="" class="statistics_form my_player_form mt-5">
                        <div class="row">
                            <?php if(!empty($fixtureScorcardDetails->fall_of_wickets)): ?>
                            <div class="col-12">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">Fall Of Wickets</label><br>
                                    <?php echo e($fixtureScorcardDetails->fall_of_wickets); ?>

                                </div>
                            </div>
                            <?php endif; ?>
                            <?php if(!empty($fixtureScorcardDetails->match_report)): ?>
                                <div class="col-12">
                                    <div class="form-group mb-4">
                                        <label for="" class="feilds_name">Match Report</label><br>
                                        <?php echo e($fixtureScorcardDetails->match_report); ?>

                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="col-12">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">Match Scorecard </label>
                                    <div class="uploaded_file_sec d-flex">
                                        <?php if(!empty($playerScorcards)){ $playerScorcards = explode(',', $playerScorcards); } ?>  
                                        <?php if(!empty($playerScorcards)): ?>
                                            <?php $__currentLoopData = $playerScorcards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $scorcards): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if(File::exists(PLAYER_CARD_IMAGE_ROOT_PATH.$scorcards)): ?>
                                                <div class="upload_img d-inline-flex align-items-center justify-content-center">
                                                    <img width="95px" height="86px"  id="delete_image_attr" class="fancyboxImg" src="<?php echo PLAYER_CARD_IMAGE_URL.'/'.$scorcards ?>" title="">
                                                    </a>
                                                </div>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                        N/A
                                        <?php endif; ?>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="legand_sec1 p-4 ml-n4 mr-n4 mb-n4">
                            <label class="feilds_name">Legend</label>
                            <div class="table-responsive">
                            <table class="legend_data_table" width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td><label for="" class="feilds_name">4: </label>&nbsp;Fours</td>
                                    <td><label for="" class="feilds_name">6: </label>&nbsp;Sixes</td>
                                    <td><label for="" class="feilds_name">OVS: </label>&nbsp;Overs</td>
                                    <td><label for="" class="feilds_name">MDS: </label>&nbsp;Maiden</td>
                                    <td><label for="" class="feilds_name">RG:</label>&nbsp;Runs Given</td>
                                    <td><label for="" class="feilds_name">WK:</label>&nbsp;Wickets</td>
                                    <td><label for="" class="feilds_name">CW:</label>&nbsp;Catch Wickets</td>
                                    <td><label for="" class="feilds_name">ST:</label>&nbsp;Stump</td>
                                    <td><label for="" class="feilds_name">ROD:</label>&nbsp;Run Out Direct</td>
                                    <td><label for="" class="feilds_name">ROA:</label>&nbsp;Run Out Assist</td>
                                    <td><label for="" class="feilds_name">DK:</label>&nbsp;Duck</td>
                                    <td><label for="" class="feilds_name">HT:</label>&nbsp;Hat-Trick</td>
                                    <td><label for="" class="feilds_name">FPS:</label>&nbsp;Fantasy Points</td>
                                </tr>
                            </table>
                        </div>
                        </div>
                    </form>
				</div>
			</div>
		</section>
	</div>
</div> 
<style type="text/css">
        .dataTables_info { display: none; }

</style>
<script type="text/javascript">
    $('body').tooltip({
    selector: '[data-toggle="tooltip"]'
}); 
table =   $('#example1').DataTable({"lengthChange": false,"scrollX": false, "paging": false, "bLengthChange": false,language: { search: '', searchPlaceholder: "Search"         }, "order": [[ 3, "desc" ]]});

table =   $('#example2').DataTable({"lengthChange": false,"paging": false,"bLengthChange": false, language: { search: '', searchPlaceholder: "Search" }, "order": [[ 3, "desc" ]]});
    $('.fancybox').fancybox();
    $('.fancybox-buttons').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        prevEffect : 'none',
        nextEffect : 'none'
    });

    $(document).ready(function(){ 
        $(".fancyboxImg").dreamimage();
    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/common/match_scorecards.blade.php ENDPATH**/ ?>
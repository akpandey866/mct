<div class="form-group">
    <?php echo e(Form::select('state', [null => 'Please Select State*']+$stateList,!empty($state_id) ? $state_id :'',['id' => 'state_id1','class'=>'custom-select state'])); ?>

    <span class="help-inline state_error"></span>
</div>
<script type="text/javascript">
	$('#state_id').on('change',function(){
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	var state_id = $('#state_id').val();
	$.ajax({ 
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: "<?php echo e(URL('get-city')); ?>",
		type:'post',
		data:{'state_id':state_id},
		success: function(r){ 
			$('.get_city_class').html(r);
			$('#loader_img').hide();
		}
	});
});
</script><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/front/common/state.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<section class="content">
	<div class="row">
        <!-- accordian ***************** -->
        <div class="col-sm-12">
            <h3>Set-up Checklist</h3>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
				          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Step 1: Games</a>
				        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse ">
                        <div class="panel-body">

                            <ul>
                                <li><b>Basics</b> - Enter basic details on all required fields</li>
                                <li><b>Grades</b>
                                    <ul>
                                        <li>Create a list of grades whose match scores would be included in your game</li>
                                        <li>For Senior Club mode game: Include Senior Mens, Senior Women and Veterans grades only</li>
                                        <li>For Junior Club mode game: Include Junior Boys and Junior Girls grades only</li>
                                        <li>For League mode game: Include 1 grade (competition/tournament name) only.</li>
                                    </ul>
                                </li>
                                <li><b>Teams</b> - Create teams which are associated with one of the grades created above.</li>
                                <li><b>Fixtures</b>
                                    <ul>
                                        <li>Create all fixtures for each of the team created above.</li>
                                        <li>You can create matches in one go, or create at-least 1 now to proceed</li>
                                        <li>Make sure to create all matches before the start of your season, or create upcoming matches before the lockout each week.</li>
                                        <li>During the season, you can make score entries through each fixture, more details on how to perform score entries would be made available soon.</li>
                                    </ul>
                                </li>
                                <li><b>Point System</b>
                                    <ul>
                                        <li>Use the default point system or create your own.</li>
                                        <li>Once the game has started, the point system cannot be changed.</li>
                                    </ul>
                                </li>
                                <li><b>Prizes</b>
                                    <ul>
                                        <li>Edit and activate the prize type you would like to share with your users.</li>
                                        <li>If you are giving away no prizes, leave all options as in-active.</li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Step 2: Set game lockout window</a>
        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">

                            <ul>
                                <li><b>Lockout </b>
                                    <ul>
                                        <li>The period during which the game will be locked out each week.</li>
                                        <li>All score updates on matches played by the players are performed during this time.</li>
                                        <li>Users cannot make changes during this time-period.</li>
                                        <li>Set the weekly lockout window for your game.</li>
                                        <li>This lockout window takes effect after/on the season start date set in Step 1.</li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Step 3: Players</a>
        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">

                            <ul>
                                <li>Create all players who would be playing in teams created above during your season.</li>
                                <li><b>For Senior Club mode game </b>
                                    <ul>
                                        <li>Include players who would be playing in the Senior Mens, Senior Women and Veterans grades/teams only.</li>
                                        <li>First 20 players are included for free.</li>
                                        <li>Add more players for $0.50 per player (cost included at time of activation of the game). After activation, you can purchase player blocks to add more player records.</li>
                                    </ul>
                                </li>
                                <li><b>For Junior Club mode game</b>
                                    <ul>
                                        <li>Include players who would be playing in the Junior Boys and Junior Girls grades/teams only.</li>
                                    </ul>
                                </li>
                                <li><b>For League mode game</b>
                                    <ul>
                                        <li>Include players who would be playing in the listed grade (competition) only.</li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Step 4: Fundraiser (available for Senior Club mode games, in AUS only)</a>
        </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">

                            <ul>
                                <li>Set up your fundraiser, if your club is looking to use the game as a fundraiser.</li>
                                <li>Set individual fundraiser contribution.</li>
                                <li>Set the target your club would like to achieve.</li>
                                <li>Set-up Stripe connection to connect for direct payments to your club account.</li>
                                <li>All fundraise amount (other than Stripe transaction fee) would be directed into the club account.</li>
                                <li>MyClubtap collects 0% fee for your fundraiser contributions.</li>
                                <li>If your club is not looking to raise funds (or for Junior Club/League mode games), please move to the next step.</li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Step 5: Add-Ons</a>
        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">

                            <ul>
                                <li><b>Availability</b>
                                    <ul>
                                        <li> Add unavailability of players who are unavailable at certain times during the season.</li>
                                        <li> You can add/deactivate unavailability of players at any time.
                                    </ul>
                                    </li>
                                    <li><b>Verify  </b>
                                        <ul>
                                            <li> Map the users of your game to their player profile in the game (optional).</li>
                                            <li> Allows you to get the users to directly update a few details of their own player profile.</li>
                                            <li> You can assign/revoke Verified users access at any time.</li>
                                        </ul>
                                    </li>
                                    <li><b>Scorers </b>
                                        <ul>
                                            <li>Assign Scorer access to end users of your game, who can assist you in Score updates for matches (optional).</li>
                                            <li>Scorer get the ability to enter score for matches of the team assigned to them only.</li>
                                            <li>Scorers have no access to any other option of your admin portal.</li>
                                            <li>You can assign/revoke Scorer access at any time.</li>
                                        </ul>
                                    </li>
                                    <li><b>Sponsors </b>
                                        <ul>
                                            <li>Add your club/league sponsors (optional).</li>
                                        </ul>
                                    </li>
                                    <li><b>Branding</b>
                                        <ul>
                                            <li>This is a paid add-on (optional)</li>
                                            <li>Sell the rights to your fantasy game to a sponsor.</li>
                                            <li>Pay and activate branding to give exclusive visibility to the branding rights owner on exclusive spaces within your fantasy game (optional);</li>
                                            <li>You can pay and activate Branding at any time.</li>
                                        </ul>
                                    </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">Step 6: Pay & Activate</a>
        </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">

                            <ul>
                                <li>Pay the calculated price of your game.</li>
                                <li>Once payment is confirmed, you can activate your game.</li>
                                <li>Once activated, your game is available to users on myclubtap.com.</li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">Step 7: Help & Support</a>
        </h4>
                    </div>
                    <div id="collapse7" class="panel-collapse collapse">
                        <div class="panel-body">

                            <ul>
                                <li>See Knowledge Base articles for more information.</li>
                                <li>Need help? Raise a ticket now.</li>
                                <li>Email help@myclubtap.com.</li>
                                <li>Call Vicky on +61 425 882 907 (for critical issues).</li>
                            </ul>

                        </div>
                    </div>
                </div>
                <?php if(Auth::guard('admin')->user()->is_game_activate == 0): ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">Step 8: Minimum requirements to activate the game</a>
        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li>Create at least 12 player records.</li>
                                <li>Have at least 1 WK/3 BAT/2 AR/ 3 BWL as players.</li>
                                <li>Create at least one grade in game.</li>
                                <li>Create at least one team in game.</li>
                                <li>Create at least one fixture in game.</li>
                                <li>Create at least one prize in game.</li>
                                <li>Set the weekly Lockout Period for the game.</li>
                                <li>Pay the fees to activate the game.</li>
                            </ul>

                        </div>
                    </div>
                </div>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/dashboard/set_up_checklist.blade.php ENDPATH**/ ?>
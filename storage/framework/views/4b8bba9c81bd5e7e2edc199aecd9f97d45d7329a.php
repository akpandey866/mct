<select name="team_id" class="form-control chosen_select">
	<option value="" selected="" >Please Select Team</option>
	<?php foreach ($teams as $key => $value) {  ?>
		<option value="<?php echo e($key); ?>"><?php echo e($value); ?></option>
	<?php } ?>
</select>
<div class="error-message help-inline">
	<?php echo $errors->first('team_id'); ?>
</div><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/common/get_scorer_team_listing.blade.php ENDPATH**/ ?>
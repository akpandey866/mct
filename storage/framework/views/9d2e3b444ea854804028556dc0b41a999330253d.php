<?php $__env->startSection('content'); ?> 
<style type="text/css">
	.chosen-choices .search-field input{ height: 30px !important; }
	.btn.btn-success{ margin-top: 0px;  }

</style>
<script src="<?php echo e(asset('js/bootstrap-datepicker.js')); ?>"></script>
<link href="<?php echo e(asset('css/bootstrap-datepicker.css')); ?>" rel="stylesheet">
<script src="<?php echo e(asset('js/admin/bootstrap-timepicker.min.js')); ?>"></script> 
 <link rel="stylesheet" href="<?php echo e(asset('css/admin/bootstrap-timepicker.min.css')); ?>">
<section class="content-header">
	<h1>
		<?php echo e(trans("Add New Availability")); ?> 
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo e(URL::to('admin/availability')); ?>"><?php echo e(trans("Availability")); ?></a></li>
		<li class="active"><?php echo e(trans("Add New Availability")); ?> </li>
	</ol>
</section>
<section class="content"> 
	<?php echo e(Form::open(['role' => 'form','url' => 'admin/availability/add-availability','class' => 'mws-form','files'=>'true','id'=>'add_availability'])); ?>

	<?php if(Auth::guard('admin')->user()->user_role_id == 1): ?>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('mode')) ? 'has-error' : ''; ?>">
					<?php echo e(Form::label('mode', trans("Game Mode"), ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item">
						  <?php echo e(Form::select('mode',[null => 'Please Select Mode'] +Config::get('home_club'),'',['id' => 'mode','class'=>'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
					<?php echo e(Form::label('club', trans("Club"), ['class' => 'mws-form-label'])); ?>

					<div class="mws-form-item put_club_html">
						  <?php echo e(Form::select('club',[null => 'Please Select Club'],'',['id' => 'get_player','class'=>'form-control'])); ?>

						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('player')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('player', trans("Player").' *', ['class' => 'mws-form-label'])); ?>

				<?php if(Auth::guard('admin')->user()->user_role_id == 1): ?>
				<div class="mws-form-item put_html">
				 	<?php echo e(Form::select('player',[null => 'Please Select Player'],'',['id' => 'player','class'=>'form-control'])); ?>

				 </div>
				 <div class="error-message help-inline">
					<?php echo $errors->first('player'); ?>
				</div>
				<?php else: ?>
				<div class="mws-form-item put_html">
					<?php echo e(Form::select('player',[null => 'Please Select Player']+$playerList,'',['id' => 'player','class'=>'form-control'])); ?> 
				</div>
				<div class="error-message help-inline">
					<?php echo $errors->first('player'); ?>
				</div>
				<?php endif; ?>
					
			</div>
		</div>
		
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('date_from')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('date_from', trans("Unavailable From*").' *', ['class' => 'mws-form-label'])); ?>

				<div class="input-group date">
				  <div class="input-group-addon">
				    <i class="fa fa-calendar"></i>
				  </div>
				 <?php echo e(Form::text('date_from','', ['class' => 'form-control ','placeholder'=>'Date From','id'=>'date_from'])); ?>

				</div>
				<div class="error-message help-inline date_from">
					<?php echo $errors->first('date_from'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('reason', trans("Reason"), ['class' => 'mws-form-label'])); ?>

				<div class="mws-form-item">
					 <?php echo e(Form::text('reason','', ['class' => 'form-control ','placeholder'=>'Reason','id'=>'reason'])); ?>

					<div class="error-message help-inline">
						<?php echo $errors->first('reason'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('date_till')) ? 'has-error' : ''; ?>">
				<?php echo e(Form::label('date_till', trans("Unavailable Till").' *', ['class' => 'mws-form-label'])); ?>

				<div class="input-group date">
				  <div class="input-group-addon">
				    <i class="fa fa-calendar"></i>
				  </div>
				 <?php echo e(Form::text('date_till','', ['class' => 'form-control ','placeholder'=>'Date Till','id'=>'date_till'])); ?>

				</div>
				<div class="error-message help-inline date_till">
					<?php echo $errors->first('date_till'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="button" value="<?php echo e(trans('Save')); ?>" class="btn btn-danger" onclick="save_availability();">
			<a href="<?php echo e(URL::to('admin/availability/add-availability/')); ?>" class="btn btn-primary"><i class=\"icon-refresh\"></i> <?php echo e(trans("Reset")); ?></a>
			<a href="<?php echo e(URL::to('admin/availability/')); ?>" class="btn btn-info"><i class=\"icon-refresh\"></i> <?php echo e(trans("Cancel")); ?></a>
		</div>
	</div>
	<?php echo e(Form::close()); ?>

	
</section>
<script type="text/javascript">
$(document).ready(function() {
	 $( "#date_from" ).datepicker({
		format 	: 'yyyy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		autoclose   :true,
		onSelect	: function( selectedDate ){ $("#date_till").datepicker("option","minDate",selectedDate); }
	});
	$( "#date_till" ).datepicker({
		format 	: 'yyyy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		autoclose   :true,
		onSelect	: function( selectedDate ){ $("#date_from").datepicker("option","maxDate",selectedDate); }
	});
});
$("#mode").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"<?php echo e(url('admin/get-club-mode-listing')); ?>",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_club_html').html(response);
			$('#loader_img').hide();
		}
	});
});
$(document).on("change",'#get_club_name',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"<?php echo e(url('admin/get-club-playerlist')); ?>",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});


function save_availability(){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
	var form = $('#add_availability')[0];
	var formData = new FormData(form);
    $.ajax({
        url: '<?php echo e(route("Availability.saveAvailability")); ?>',
        type:'post',
        data: formData,
		processData: false,
		contentType: false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
				//show_message(data['message'],"success");
                window.location.href   =  "<?php echo e(url('admin/availability')); ?>";
            }else if(data['success'] == 2){
            	//show_message(data['message'],"error");
                window.location.href   =  "<?php echo e(url('admin/availability')); ?>";
            }else {
                $.each(data['errors'],function(index,html){
                	if(index == "date_from"){
                	 	$(".date_from").addClass('error');
                	 	$(".date_from").html(html);
                	}if(index == "date_till"){
                		$(".date_till").addClass('error');
                		$(".date_till").html(html);
                	}
                    $("#"+index).next().addClass('error');
                    $("#"+index).next().html(html);
                });
                $('#loader_img').hide();
            }
            $('#loader_img').hide();
        }
    });
}

$('#add_availability').each(function() {
	$(this).find('input').keypress(function(e) {
       if(e.which == 10 || e.which == 13) {
			save_availability();
			return false;
        }
    });
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\myclubtap\resources\views/admin/availabilities/add.blade.php ENDPATH**/ ?>
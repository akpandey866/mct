<?php $__env->startSection('content'); ?>
<script>
	jQuery(document).ready(function(){
		$(".choosen_selct").chosen();
	});
</script>
<style>
.chosen-container-single .chosen-single{
	height:34px !important;
	padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>Pay & Activate</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo e(URL::to('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active"><?php echo e(trans("Pay & Activate")); ?></li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
		<?php echo e(Form::open(['method' => 'get','role' => 'form','url' => 'admin/admin-pay-activate','class' => 'mws-form'])); ?>

		<?php echo e(Form::hidden('display')); ?>

			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<div class="form-group ">  
					<?php echo e(Form::select('game_mode',[null => 'Please Select Mode'] + Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''), ['class' => 'form-control game_mode choosen_selct'])); ?>

					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<div class="form-group get_game_name">  
					<?php echo e(Form::select('game_name',array(''=>trans('Select Game')),((isset($searchVariable['game_name'])) ? $searchVariable['game_name'] : ''), ['class' => 'form-control choosen_selct'])); ?>

					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<div class="form-group ">  
					<?php echo e(Form::select('is_active',array(''=>trans('Select Status'),0=>'Inactive',1=>'Active'),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control choosen_selct'])); ?>

				</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href="<?php echo e(URL::to('admin/admin-pay-activate')); ?>"  class="btn btn-primary"><i class='fa fa-refresh '></i> Reset</a>
			</div>
		<?php echo e(Form::close()); ?>

	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="14%">
							<?php echo e(link_to_route(
								"Common.adminGameFee",
								trans("Game Name"),
								array(
									'sortBy' => 'game_name',
									'order' => ($sortBy == 'game_name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'game_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'game_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="14%">Platform Fees</th>
						<th width="14%">Player Fees</th>
						<th width="14%">
							<?php echo e(link_to_route(
								"Common.adminGameFee",
								 trans("Branding Fees"),
								array(
									'sortBy' => 'branding_price',
									'order' => ($sortBy == 'branding_price' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'branding_price' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'branding_price' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th>
						<th width="14%">
							Total Fee
						</th>
						<th>
							<?php echo e(link_to_route(
								"Common.adminGameFee",
								trans("Activated On"),
								array(
									'sortBy' => 'branding_activated_on',
									'order' => ($sortBy == 'branding_activated_on' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'branding_activated_on' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'branding_activated_on' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)); ?>

						</th> 
						<th><?php echo e(trans("Action")); ?></th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					<?php if(!$result->isEmpty()): ?>
					<?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr class="items-inner">
						<td data-th='<?php echo e(trans("game name")); ?>'><?php echo e($record->game_name); ?></td>
						<td data-th='<?php echo e(trans("Platform Fee")); ?>'>
							<?php   
								$price = 0;
								if($record->game_mode == 1){
							      $price = SENIORFEE;
							    }elseif ($record->game_mode == 2) {
							      $price = JUNIORFEE;
							    }elseif ($record->game_mode == 3) {
							      $price = LEAGUEFEE;
							    }
							    echo !empty($price) ? $price : ''; 
							 ?>
						</td>
						<td data-th='<?php echo e(trans("Player Fee")); ?>'>
							<?php  
								$playerFee = 0;
								if(!empty(get_player_fee($record->id))){
									$playerFee =  get_player_fee($record->id); 
								}
								echo !empty($playerFee) ? $playerFee :'-'; 
								?>
							
							</td>
						<td data-th='<?php echo e(trans("Branding fee")); ?>'>
							<?php 
							$brandingPrice = 0;
							if(!empty($record->branding_price)){
								$brandingPrice = $record->branding_price;
							} ?>
							<?php echo e(!empty($brandingPrice) ? $brandingPrice :'-'); ?></td>
						<td data-th='<?php echo e(trans("Total Fee")); ?>'>
							<?php $totalFee = $price + $playerFee + $brandingPrice;  echo $totalFee;?>	
						</td> 
						 <td data-th='<?php echo e(trans("activated on")); ?>'>
						<?php if(!empty($record->branding_activated_on) && $record->is_paid==1): ?>
							<?php echo e(date(config::get("Reading.date_format"),strtotime($record->branding_activated_on))); ?>

						<?php else: ?>
							-
						<?php endif; ?>
						</td> 
						<td data-th='<?php echo e(trans("action")); ?>'>	
							<a title="Free Platform Fees" href="<?php echo e(URL::to('admin/admin-pay-activate/admin-player-fee/'.$record->id)); ?>" class="btn btn-success">Player Fee</a>
							<?php if($record->is_game_activate == 1): ?>
								<a  title="Click To Deactivate Game"  href="<?php echo e(URL::to('admin/admin-activate-game/'.$record->id.'/0')); ?>"  class=" btn btn-success btn-small status_any_item"><span class="fa fa-check"></span> </a>
							<?php else: ?>
								<a title="Click To Activate Game" href="<?php echo e(URL::to('admin/admin-activate-game/'.$record->id.'/1')); ?>"   class=" btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span></a>
							<?php endif; ?>	
						</td>
					</tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					 <?php else: ?>
						<tr>
							<td class="alignCenterClass" colspan="6" ><?php echo e(trans("No record is yet available.")); ?></td>
						</tr>
					<?php endif; ?> 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right "><?php echo $__env->make('pagination.default', ['paginator' => $result], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
		</div>
	</div>
</section> 
<script type="text/javascript">
	var game_name = "<?php echo !empty($searchVariable['game_name']) ? $searchVariable['game_name'] :'' ?>";
	var game_mode = "<?php echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode'] :'' ?>";
	if(game_name !="" || game_mode !=""){
		get_game_name();

	}
	function get_game_name(){
		$.ajax({
		 	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:' <?php echo e("admin-get-game-name"); ?> ',
			'type':'post',
			data:{'mode':game_mode,'game_name':game_name},
			success:function(response){ 
				$(".get_game_name").html(response);
			}
		});
	}
	$(".game_mode").on('change',function(){
		var mode = $(this).val();
		$('#loader_img').show();
		 $.ajax({
		 	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:' <?php echo e("admin-get-game-name"); ?> ',
			'type':'post',
			data:{'mode':mode,'game_name':game_name},
			success:function(response){ 
				$(".get_game_name").html(response);
			}
		}); 
		$('#loader_img').hide();
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\myclubtap\resources\views/admin/common/admin_game_fee.blade.php ENDPATH**/ ?>
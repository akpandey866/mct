@extends('front.layouts.default')
@section('content')
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('css/datepicker-custom-theme.css') }}" rel="stylesheet">
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end" style="background-image: url({{asset('img/inner_banner.png')}});">
    </section>
    <section class="signin_signup_block py-5">
        <div class="container main_signup_contenar">
            <h2 class="form_title mb-3">Sign Up as User?</h2>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                    {{ Form::open(['role' => 'form','route' => "Home.signup",'class' => 'form_block p-4','id'=>'signup','files' => true]) }}
                        <h3 class="form_heading mb-4">Hello!<small>Create an account to continue</small></h3>
                        <div class="club_detail_sec mt-4">
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="First Name*" name="first_name" />
                                <span class="help-inline first_name"></span>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Last Name*" name="last_name" />
                                <span class="help-inline last_name"></span>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Email*" name="email" />
                                <span class="help-inline email"></span>
                            </div>
                            <div class="form-group">
                                 {{ Form::password('password', ['class' => 'form-control', 'placeholder' => trans('Password*')]) }}
                                <span class="help-inline password"></span>
                            </div>
                            
                            <div class="form-group">
                                 {{ Form::password('confirm_password', ['class' => 'form-control', 'placeholder' => trans('Confirm Password*')]) }}
                                <span class="help-inline confirm_password"></span>
                            </div>

                            <div class="erro_msg" style="margin-left: 2px;"><em>Must be a minimum of 8 characters in length, contain at least a number and a special character.</em></div>

                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Referral Code (optional)" name="referral_code" value="{{$refCode}}" />
                                <span class="help-inline referral_code"></span>
                            </div>
                        </div>
                        <div class="game_rep_detail_sec py-3">
                            <h4 class="form_sec_heading text-uppercase mb-2">Authorize</h4>
                            <ul class="authorize_list">
                                <li class="py-2">
                                    <label class="join_our_trems d-flex position-absolute p-0">
                                        <span class="custom_check">
                                            <input type="checkbox" name="terms">
                                            <span class="check_indicator">&nbsp;</span>
                                        </span>

                                    </label>

                                    Agree to <a href="{{url('pages/terms')}}" target="_blank">Terms</a> &amp; <a href="{{url('pages/conditions')}}" target="_blank">Conditions</a>.*<br>
                                    <span class="help-inline terms"></span>
                                </li>
                                <li class="py-2">
                                    <label class="join_our_trems d-flex position-absolute p-0">
                                        <span class="custom_check">
                                            <input type="checkbox" name="privacy">
                                            <span class="check_indicator">&nbsp;</span>
                                        </span>
                                    </label>
                                    Agree To MyClubtap <a href="{{url('pages/refund')}}" target="_blank">Refunds</a> & <a href="{{url('pages/privacy')}}" target="_blank">Privacy Policies</a>.*<br>
                                    <span class="help-inline privacy"></span>
                                </li>
                                <li class="py-2">
                                    <label class="join_our_trems d-flex position-absolute p-0">
                                        <span class="custom_check">
                                            <input type="checkbox" name="newslater">
                                            <span class="check_indicator">&nbsp;</span>
                                        </span>
                                    </label>
                                    Subscribe To Newsletter.<br>
                                    <span class="help-inline authorize"></span>
                                </li>
                            </ul>
                        </div>
                        <input class="w-100 btn text-uppercase" type="button" value="Sign Up" onclick='signup();'>
                        <div class="already_logon_text text-center mt-3">Already have an account yet? <a href="{{route('Home.login')}}">Login</a></div>
                    {{ Form::close() }}
                </div>
                <div class="col-12 col-md-6 offset-lg-1">
                    <div class="joins_btns_block  user_signup_btn">
                      <!--   <div class="mb-3">Don't have h an account yet?</div> -->
                        <div class="mb-2"><a class="btn" href="{{route('Home.signup')}}">Sign Up as Club or a League?</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>    
</div>
<script type="text/javascript">

$(document).ready(function(){

  var sc_width = screen.width;
  if(sc_width < 750){
    // alert(sc_width); 
    $(".user_signup_btn").prependTo(".main_signup_contenar"); 

  }

}); 



function signup() { 
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    var form = $('#signup')[0];
    var formData = new FormData(form);
    $.ajax({ 
        url: '{{ route("Home.userSignup") }}',
        type:'post',
        data: formData,
        processData: false,
        contentType: false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
                window.location.href     =  "{{ route('Home.userSignup') }}";
            }else { console.log(data['errors']);
                $.each(data['errors'],function(index,html){
                    if(index=="first_name"){ 
                        $(".first_name").addClass('error');
                        $(".first_name").html(html);
                    }if(index=="email"){ 
                        $(".email").addClass('error');
                        $(".email").html(html);
                    }if(index=="last_name"){ 
                        $(".last_name").addClass('error');
                        $(".last_name").html(html);
                    }if(index=="password"){ 
                        $(".password").addClass('error');
                        $(".password").html(html);
                    }if(index=="confirm_password"){ 
                        $(".confirm_password").addClass('error');
                        $(".confirm_password").html(html);
                    }if(index=="terms"){ 
                        $(".terms").addClass('error');
                        $(".terms").html(html);
                    }
                    if(index=="privacy"){ 
                        $(".privacy").addClass('error');
                        $(".privacy").html(html);
                    }
                    if(index=="referral_code"){ 
                        $(".referral_code").addClass('error');
                        $(".referral_code").html(html);
                    }
                   

                });
                
                $('#loader_img').hide();
            }
            $('#loader_img').hide();
        }
    });
}
    
$('#signup').each(function() {
    $(this).find('input').keypress(function(e) {
        if(e.which == 10 || e.which == 13) {
        signup();
        return false;
       }
    });
});

$(function(){
    $('.dob').datepicker({
        format  : 'yyyy-mm-dd',
        endDate: "today",
        autoclose:true,
    }); 
});

$('#country_id').on('change',function(){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    var country_id = $('#country_id').val();
    $.ajax({ 
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ URL('get-state') }}",
        type:'post',
        data:{'country_id':country_id},
        success: function(r){
            $('.get_state_class').html(r);
            $('#loader_img').hide();
        }
    });
});
</script>
@stop
@extends('front.layouts.default')
@section('content')
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('css/datepicker-custom-theme.css') }}" rel="stylesheet">
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end" style="background-image: url({{asset('img/inner_banner.png')}});">
    </section>
  <!-- <button type="button" class="btn btn-info btn-lg show-modal" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
    <section class="signin_signup_block py-5">
        <div class="container main_signup_contenar">
            <h2 class="form_title mb-3">Sign Up</h2>
            <div class="d-block d-md-flex align-items-center mx-n2">
              <a class="btn m-2" href="{{route('Home.userSignup')}}">Sign up as User</a>
              <a class="btn m-2" href="{{route('Home.signup')}}">Sign Up as Club or a League?</a>
            </div>
        </div>
    </section>    
</div>


<style type="text/css">
    .user_signup_btn{ margin-bottom: 15px;  }
</style>

@stop
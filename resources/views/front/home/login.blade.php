@extends('front.layouts.default')
@section('content')
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end" style="background-image: url(img/inner_banner.png);">
    </section>
    <section class="signin_signup_block py-5">
        <div class="container">
            <h2 class="form_title mb-3">Login</h2>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                    <form action="{{url('login')}}" class="form_block p-4" id="login_form">
                        <h3 class="form_heading mb-4">Welcome Back! <small>Login to continue</small></h3>
                        <div class="form-group">
                            {{ Form::text("login_username",'', ['placeholder'=>'Email*','class'=>'form-control']) }}
                            <span class="help-inline"></span>
                        </div>
                        <div class="form-group">
                            <div class="text_iconview_block d-flex align-items-center p-0">
                                {{Form::password("login_password",['id'=>'password','placeholder'=>'Password*','class'=>'form-control']) }}
                                <span class="help-inline"></span>
                                <a class="text_ivon d-flex align-items-center justify-content-center"  onclick="togglePassword();" href="javascript:void(0);" id="showPass">Show</a>
                            </div>
                        </div>
                        <div class="form-group forgot_remember_sec d-flex align-items-center py-3">
                            <div class="my-2 my-md-0">
                                <label class="join_our_trems d-flex">
                                    <span class="custom_check">
                                        <input type="checkbox" name="remember_me">
                                        <span class="check_indicator">&nbsp;</span>
                                    </span>
                                    Remember me
                                </label>
                            </div>
                            <a class="forgot_links p-0 my-2 my-md-0 ml-auto" href="{{route('Home.forgotpassword')}}">Forgot Password?</a>
                        </div>
                        <input class="w-100 btn text-uppercase" onclick="login_user();" type="button" value="Log In">
                    {{ Form::close() }}
                </div>
                <div class="col-12 col-md-6 offset-lg-1">
                    <div class="joins_btns_block">
                        <div class="mb-3">Don't have an account yet?</div>
                        <div class="mb-2"><a class="btn" href="{{route('Home.signup')}}">Sign up as Club or League?</a></div>
                        <div class="mb-2"><a class="btn" href="{{route('Home.userSignup')}}">Sign up as a User?</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>    
</div>
<script type="text/javascript">
    function togglePassword() {
      var x = document.getElementById("password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
    function login_user(){
        $('#loader_img').show();
        $('.help-inline').html('');
        $('.help-inline').removeClass('error');
        $.ajax({
             headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ URL("login") }}',
            type:'post',
            data: $('#login_form').serialize(),
            success: function(r){
                error_array     =   JSON.stringify(r);
                data            =   JSON.parse(error_array);
                if(data['success'] == 1) { 
                    if(data['is_profile_completed'] == 0){
                        window.location.href     =  "{{ route('User.updateProfile') }}";
                    }else{
                     window.location.href     =  "{{ route('User.lobby') }}";
                    }
                    /*if('<?php //echo WEBSITE_URL ?>' == '<?php //echo URL::previous();?>'){
                        window.location.href     =  "{{ URL::previous() }}";
                    }else{
                        window.location.href     =  "{{ route('User.lobby') }}";
                    }*/
                }else if(data['success'] == 2) {
                    document.getElementById("login_form").reset();
                    showMessage(data['message'],"error");
                }else {
                    $.each(data['message'],function(index,html){
                        $("input[name = "+index+"]").next().addClass('error');
                        $("input[name = "+index+"]").next().html(html);
                    });
                }
                $('#loader_img').hide();
            }
        });
    }
     $('#login_form').each(function() {
            $(this).find('input').keypress(function(e) {
            if(e.which == 10 || e.which == 13) {
                login_user();
                return false;
            }
        });
        
    });
</script>
@stop
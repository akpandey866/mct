<?php $login_user = Auth::user(); ?>
 <footer>
    <div class="full_container">
        <div class="container">
            <div class="footer_data py-3">
                <div class="row footer_link_mview">
                    <div class="col py-3">
                        <ul class="quick_links d-flex flex-wrap">
                            <li class="w-100 footer_title text-uppercase mb-3">Links</li>
                            <li class="w-100"><a href="{{url('about')}}" title="Rules">About</a></li>
                            <li class="w-100"><a href="{{url('features')}}" title="Feature">Feature</a></li>
                            <li class="w-100"><a href="{{url('pricing')}}" title="Pricing">Pricing</a></li>
                            <li class="w-100"><a href="{{url('pages/partner')}}" title="Partner With Us">Partner With Us</a></li>
                        </ul>
                    </div>
                    <div class="col py-3">
                        <ul class="quick_links d-flex flex-wrap">
                            <li class="w-100 footer_title text-uppercase mb-3">About</li>
                            <li class="w-100"><a href="{{url('pages/terms')}}" title="Terms">Terms</a></li>
                            <li class="w-100"><a href="{{url('pages/conditions')}}" title="Conditions">Conditions</a></li>
                            <li class="w-100"><a href="{{url('pages/privacy')}}" title="Privacy">Privacy</a></li>
                            <li class="w-100"><a href="{{url('pages/refund')}}" title="Refund">Refund</a></li>
                        </ul>
                    </div>
                    <div class="col py-3">
                        <ul class="quick_links d-flex flex-wrap">
                            <li class="w-100 footer_title text-uppercase mb-3">Contact Us</li>
                            <li class="w-100"><a href="{{Config::get('Social.facebook')}}" title="facebook" target="_blank">Facebook</a></li>
                            <li class="w-100"><a href="{{Config::get('Social.twitter')}}" title="twitter" target="_blank">Twitter</a></li>
                            <li class="w-100"><a href="{{Config::get('Social.linkedin_url')}}" title="linkedin" target="_blank">LinkedIn</a></li>
                            <li class="w-100"><a href="{{Config::get('Social.instagram_url')}}" title="instagram" target="_blank">Instagram</a></li>
                        </ul>
                    </div>
                    @if(empty($login_user))
                    <div class="col py-3">
                        <ul class="quick_links d-flex flex-wrap">
                            <li class="w-100 footer_title text-uppercase mb-3">Account</li>
                           <li class="w-100"><a href="{{url('signup')}}">Sign-Up</a></li>
                           <li class="w-100"><a href="{{url('login')}}">Login</a></li>
                        </ul>
                    </div> 
                    @endif

                    <div class="col py-3">
                        <ul class="quick_links d-flex flex-wrap">
                            <li class="w-100 footer_title text-uppercase mb-3">We Serve</li>
                           <li class="w-100"><a href="https://community.myclubtap.com"  title="Community">Community</a></li>
                             <li class="w-100"><a href="https://feedback.myclubtap.com"  title="Testimonials">Share Feedback</a></li>
                             <li class="w-100"><a href="{{url('testimonials')}}" title="testimonials">Testimonials</a></li>
                             <li class="w-100"><a href="{{url('help&support')}}" title="testimonials">Help & Support</a></li>
                            <!-- <li class="w-100" >Call Vicky on <a href="tel:61425882907">{{Config::get('Site.contact_number')}}</a></li>
                            <li class="w-100">Email: <a href="mailto:help@myclubtap.com"> {{Config::get('Site.contact_email')}}</a></li> -->
                        </ul>
                    </div>
                    
                    <!-- <div class="col d-flex align-items-center py-3">
                        <a href="javascript:void(0)" class="game_rule_btn text-center">Game Rules</a>                        
                    </div> -->
                </div>
            </div>
            <div class="row no-gutters align-items-center copyright_text">
                <div class="col-12 col-md-6 py-3">
                    <ul class="social_links d-flex justify-content-center justify-content-md-start flex-wrap">
                        <!--<li class="w-100 footer_title mb-3">Join Us</li>-->
                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" href="{{Config::get('Social.facebook')}}" target="_blank" title="facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" href="{{Config::get('Social.twitter')}}" target="_blank" title="twitter"><i class="fab fa-twitter"></i></a></li>
                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" href="{{Config::get('Social.linkedin_url')}}" target="_blank" title="linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                        
                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" href="{{Config::get('Social.instagram_url')}}" target="_blank" title="instagram"><i class="fab fa-instagram"></i></a></li>

                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" href="{{Config::get('Social.youtube_url')}}" target="_blank" title="youtube"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 py-3">
                    <div class="text-center text-md-right">&copy; {{Config::get('Site.copyrights')}} {{date('Y',time())}}</div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126186397-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-126186397-1');
</script>
</div>
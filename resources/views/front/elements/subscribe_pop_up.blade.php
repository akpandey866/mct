<!-- Pop for registration -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header align-items-center">
            	<img class="modal_logo mr-3" src="{{asset('img/my_club_tap_icon.png')}}" alt="MyClubTap">
                <h4 class="modal-title">Be the First to Know When MyClubtap Launches</h4>                
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
               <div class="loginpage">
               		<p>Join the list and we’ll let you know how to get your club or league fantasy game on MyClubtap.</p>
                    <h2 class="block_title my-3"></h2>
                    {{ Form::open(['role' => 'form','route' => "Home.contact",'class' => 'mws-form px-4','id'=>'contact_form']) }}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Your Name</label> 
                        {{ Form::text("data[name]",'', ['id'=>'name','class'=>'form-control']) }}
                        <span class="help-inline"></span>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Your Email</label>
                        {{ Form::text("data[email]",'', ['id'=>'email','class'=>'form-control' ]) }}
                        <span class="help-inline"></span>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Club/League Name</label>
                        {{ Form::text("data[club]",'', ['class'=>'form-control','id'=>'subject' ]) }}
                        <span class="help-inline"></span>
                    </div>
                    <div class="text-center sendbtns">
                        <br> <br>
                        <button type="button" class="btn" onclick='contact_us();'>Subscribe</button>
                    </div>
                    {{ Form::close() }}
                     <div class="popup_bottom_text text-center">In the meantime, follow us on <a href="{{Config::get('Social.facebook')}}" target="_blank">facebook</a>,<a href="{{Config::get('Social.twitter')}}" target="_blank">&nbsp;twitter </a> or <a href="{{Config::get('Social.instagram_url')}}" target="_blank">instagram</a> to stay updated with all the latest news and information.</div>
                </div>

            </div>

            <!-- Modal footer -->
        </div>
    </div>
</div>
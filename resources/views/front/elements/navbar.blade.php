<?php $segment1 = Request::segment(1);  ?>
 <nav class="navbar navbar-expand-md p-lg-0 p-0">
 
    <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#collapsibleNavbar1">
   <div class="mobilemenus">
 
	
	<div class="page_title">
	    {{!empty($segment1) ? ucfirst(str_replace('-', ' ', $segment1)) : 'Home'}}
	</div>
	  <div>
  <!---  <span class="navbar-toggler-icon"></span>
	<span class="navbar-toggler-icon"></span>
	<span class="navbar-toggler-icon"></span>--->
	
	<div class="more_menus">
	    Game menu
	    <div>
	     <span></span>
	    <span></span>
	    <span></span>
	    </div>
	    
	</div>
	
	</div>
	</div>
  </button>
 
 
 <div class="collapse navbar-collapse" id="collapsibleNavbar1">
 

<ul class="nav nav-tabs status_tabs_link d-flex border-0 navbar-nav w-100">
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='lobby') ? 'active' : '' }}" href="{{route('User.lobby')}}">Lobby</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='my-team') ? 'active' : '' }}" href="{{route('User.myTeam')}}">My Team</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='points') ? 'active' : '' }}" href="{{url('points')}}">Points</a></li> 
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='rank') ? 'active' : '' }}" href="{{url('rank')}}">Rank</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='fixtures') ? 'active' : '' }}" href="{{route('Common.fixtures')}}">Fixtures</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='stats') ? 'active' : '' }}" href="{{url('stats')}}">Stats</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='availability') ? 'active' : '' }}" href="{{route('User.availability')}}">Availability</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='chat') ? 'active' : '' }}" href="{{route('Common.chat')}}">Chat</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='prizes') ? 'active' : '' }}" href="{{url('prizes')}}">Prizes</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='winner') ? 'active' : '' }}" href="{{url('winner')}}">Winners</a></li> 
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='history') ? 'active' : '' }}" href="{{url('history')}}">History</a></li>
	<li class="nav-item d-inline-flex"><a class="nav-link {{ ($segment1=='sponsors') ? 'active' : '' }}" href="{{url('sponsors')}}">Sponsors</a></li>
</ul>   
</div>
</nav>
@extends('front.layouts.default')
@section('content')


<div class="body_section">

      <div class="container py-3 py-lg-4">
            <h2 class="block_title"><small>MyClubtap</small> My Comps</h2>
        </div>

     {{--   @include('front.elements.common_header') --}}
            <div class="container">
            {{--	      @include('front.elements.navbar') --}}
                <div class="clubs_comps my-2 my-md-5 ">

                    <div class="games_tabs_sec mycomps_tabs">
                        <ul class="nav nav-tabs d-flex" role="tablist">
                            <li class="nav-item"><a class="nav-link active py-4" data-toggle="tab" href="#form" role="tab" aria-selected="true">Senior Club Mode</a></li>
                            <li class="nav-item  "><a class="nav-link py-4" data-toggle="tab" href="#game_log" role="tab" aria-selected="false">Junior Club Mode</a></li>
                            <li class="nav-item  "><a class="nav-link py-4" data-toggle="tab" href="#league_mode" role="tab" aria-selected="false">League Mode</a></li>
                        </ul>
                        <div class="tab-content   p-1 p-md-4  mb-3">
                            <div class="tab-pane fade show active" id="form">
                                <div class="row">
                                @if(isset($user_teams[1]))
                                	@foreach($user_teams[1] as $key => $val)

												<div class="col-sm-12 col-md-6 col-lg-6 pb-0 pb-md-5">
			                                        <div class="mycomps_block p-3">
			                                            <div class="compas_logo d-flex compssl">
 @if(!empty($club_dta[$val->club_id]->club_logo) && File::exists(CLUB_IMAGE_ROOT_PATH.$club_dta[$val->club_id]->club_logo))  
 <div class="compslogo_img">
                                                            <img style="max-width:83px;max-height:41px;" src="{{CLUB_IMAGE_URL.$club_dta[$val->club_id]->club_logo}}" alt="" />
                                                        </div>
@endif

                                                        <span class="d-inline-block pl-3"> {{$club_arr[$val->club_id]}} </span></div>
			                                            <div class="row supercoash_point pt-3">
			                                                <div class="col-4 col-sm-4 divid">
			                                                   <!--  <div class="text-center"><span class="d-block">Rank {{isset($team_ranks_arr[$val->club_id][$val->id]) ? $team_ranks_arr[$val->club_id][$val->id] : 0}}</span></div> -->

                                                                <div class="text-center"><span class="d-block">{{isset($team_ranks_arr[$val->club_id][$val->id]) ? $team_ranks_arr[$val->club_id][$val->id] : 0}}</span>Rank</div> 
                                                                
			                                                </div>

			                                                <div class="col-4 col-sm-4 divid">

                                                                <div class="text-center"><span class="d-block">{{isset($temp_points_arr[$val->club_id][$val->id]) ? $temp_points_arr[$val->club_id][$val->id] : 0}}</span>Points</div>  

			                                                </div>
			                                                <div class="col-4 col-sm-4">
			                                                    <div class="text-center"> <a href="{{url('/my-team/'.$val->club_id.'/1')}}">Enter</a> </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>

                                	@endforeach
                                @else
                                <div class=" col-12">You have not yet joined a Senior Club Mode fantasy game.</div>
                                @endif

                              

                                </div>


                            </div>
                            <div class="tab-pane fade" id="game_log">
                            	
								<div class="row">


								@if(isset($user_teams[2]))
                                	@foreach($user_teams[2] as $key => $val)

												<div class="col-sm-12 col-md-6 col-lg-6 pb-0 pb-md-5">
			                                        <div class="mycomps_block p-3">
			                                            <div class="compas_logo d-flex compssl">
 @if(!empty($club_dta[$val->club_id]->club_logo) && File::exists(CLUB_IMAGE_ROOT_PATH.$club_dta[$val->club_id]->club_logo))  
  <div class="compslogo_img">
                                                            <img style="max-width:83px;max-height:41px;" src="{{CLUB_IMAGE_URL.$club_dta[$val->club_id]->club_logo}}" alt="" /> 
    </div>
@endif
                                                            <span class="d-inline-block pl-3"> {{$club_arr[$val->club_id]}} </span></div>
			                                            <div class="row supercoash_point pt-3">
			                                                <div class="col-4 col-sm-4 divid">
			                                                    
                                                               <!--  <div class="text-center"><span class="d-block">Rank {{isset($team_ranks_arr[$val->club_id][$val->id]) ? $team_ranks_arr[$val->club_id][$val->id] : 0}}</span>Senior</div> -->

                                                                 <div class="text-center"><span class="d-block">{{isset($team_ranks_arr[$val->club_id][$val->id]) ? $team_ranks_arr[$val->club_id][$val->id] : 0}}</span>Rank</div> 

			                                                </div>
			                                                <div class="col-4 col-sm-4 divid">
	
                                                                <div class="text-center"><span class="d-block">{{isset($temp_points_arr[$val->club_id][$val->id]) ? $temp_points_arr[$val->club_id][$val->id] : 0}}</span>Points</div>                                                                
			                                                </div>
			                                                <div class="col-4 col-sm-4">
			                                                    <div class="text-center"> <a href="{{url('/my-team/'.$val->club_id.'/2')}}">Enter</a> </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>

                                	@endforeach	
                                @else
                                <div class="col-12">You have not yet joined a Junior Club Mode fantasy game.</div>                                	
                                @endif

                                    
                                </div>

                            </div>
                            <div class="tab-pane fade" id="league_mode">
                            	
								<div class="row">
								@if(isset($user_teams[3]))
                                	@foreach($user_teams[3] as $key => $val)

												<div class="col-sm-12 col-md-6 col-lg-6 pb-0 pb-md-5">
			                                        <div class="mycomps_block p-3">
			                                            <div class="compas_logo d-flex compssl">
 @if(!empty($club_dta[$val->club_id]->club_logo) && File::exists(CLUB_IMAGE_ROOT_PATH.$club_dta[$val->club_id]->club_logo))      
  <div class="compslogo_img">                                                      
                                                            <img style="max-width:83px;max-height:41px;" src="{{CLUB_IMAGE_URL.$club_dta[$val->club_id]->club_logo}}" alt="" /> 
 </div>                                                            
@endif
                                                            <span class="d-inline-block pl-3"> {{$club_arr[$val->club_id]}} </span></div>
			                                            <div class="row supercoash_point pt-3">
			                                                <div class="col-4 col-sm-4 divid">
			                                                    
                                                                <!-- <div class="text-center"><span class="d-block">Rank {{isset($team_ranks_arr[$val->club_id][$val->id]) ? $team_ranks_arr[$val->club_id][$val->id] : 0}}</span>Senior</div> -->
                                                                <div class="text-center"><span class="d-block">{{isset($team_ranks_arr[$val->club_id][$val->id]) ? $team_ranks_arr[$val->club_id][$val->id] : 0}}</span>Rank</div> 
			                                                </div>
			                                                <div class="col-4 col-sm-4 divid">

                                                                <div class="text-center"><span class="d-block">{{isset($temp_points_arr[$val->club_id][$val->id]) ? $temp_points_arr[$val->club_id][$val->id] : 0}}</span>Points</div>
			                                                </div>
			                                                <div class="col-4 col-sm-4">
			                                                    <div class="text-center"> <a href="{{url('/my-team/'.$val->club_id.'/3')}}">Enter</a> </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>

                                	@endforeach	
                                @else
                                <div class="col-12">You have not yet joined a League Mode fantasy game.</div>
                                @endif


                                    
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="container pt-4">
                <div class="favorite_players_block mb-5">
                    <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
                </div>
            </div>
        </div>


@stop
@extends('front.layouts.default')
@section('content')
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
 <script>!function(){window;var e,t=document;e=function(){var e=t.createElement("script");e.type="text/javascript",e.defer=!0,e.src="https://cdn.endorsal.io/widgets/widget.min.js";var n=t.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n),e.onload=function(){NDRSL.init("5ddc71fea8b52a6998fca494")}},"interactive"===t.readyState||"complete"===t.readyState?e():t.addEventListener("DOMContentLoaded",e())}();</script>
<div class="body_section">    
    <section class="chooseus_block pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12 pt-4 pt-md-0">
                    <!-- <div class="grid1">
                      <div class="ndrsl-widget" id='ndrsl-wol-5ddc75e8a8b52a6998fca499'>...</div>
                    </div> -->
                    <div class="ndrsl-widget" id='ndrsl-wol-5ddc75e8a8b52a6998fca499'>...</div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        setTimeout(function(){
            $('.grid1').masonry({
              // options
              itemSelector: '.ndrsl-wall-col',
              columnWidth: 200
            });
        }, 1000);
        
    });
 
</script>
@stop
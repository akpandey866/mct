@extends('front.layouts.default')
@section('content')
    <div class="body_section">
        @include('front.elements.common_header')
        <section class="status_tab_block mb-2">
            <div class="container mobile_cont">
                @include('front.elements.navbar')
                <div class="tab_content_sec border p-4">
<!--                     <h4 class="status_title">Sponsors</h4> -->
                     @if($result->isEmpty())
                        <p style="margin-top: 15px;">No sponsors have yet been added.</p>
                     @endif

                    <ul class="sponsors_list">

                        @if(!$result->isEmpty())
                            @foreach($result as $value)
                            <li class="d-block d-md-flex align-items-center p-3 p-md-4 mb-4">
                                <div class="sponsors_img">
                                    @if(File::exists(SPONSOR_IMAGE_ROOT_PATH.$value->logo))
                                    <a href="{{$value->website}}"> <img src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.SPONSOR_IMAGE_URL.'/'.$value->logo ?>" alt="Sponsors"></a>
                                    @endif
                                </div>
                                <div class="w-100 sponsors_info pl-0 pl-md-4 py-4 py-md-0">
                                    <div class="sponsors_list_heading mb-1"> <a href="{{$value->website}}"> {{$value->name}}</a></div>
                                    <div class="mb-2">{{!empty($value->about) ? $value->about :''}}</div>
                                    <ul class="social_links d-flex align-items-center">
                                        @if($value->facebook)
                                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" target="_blank" href="{{$value->facebook}}" title="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                        @endif
                                        @if($value->twitter)
                                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" target="_blank" href="{{$value->twitter}}" title="twitter"><i class="fab fa-twitter"></i></a></li>
                                         @endif
                                        @if($value->instagram)
                                        <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" target="_blank" href="{{$value->instagram}}" title="instagram"><i class="fab fa-instagram"></i></a></li>
                                        @endif
                                    </ul>
                                </div>
                                @if($value->user_id ==1)
                                <a href="{{$value->website}}" target="_blank" class="sponsors_btn ml-0 ml-md-4">
                                    @if(!empty($value->offer))
                                        {{$value->offer}}
                                    @else
                                        Offer 5% off
                                    @endif
                                </a>
                                @else
                                <a href="{{$value->website}}" target="_blank" class="sponsors_btn ml-0 ml-md-4">Visit</a>
                                @endif
                            </li>
                            @endforeach
                            <?php /*
                        @else
                             <li class="d-block d-md-flex align-items-center p-3 p-md-4 mb-4">
                                <div class="w-100 sponsors_info pl-0 pl-md-4 py-4 py-md-0">
                                    <div class="sponsors_list_heading mb-1">No sponsors have yet been added.</div>
                                </div>
                            </li>
                            */ ?>
                        @endif
                    </ul>
                </div>
            </div>
        </section>
        <div class="container pt-4">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
            </div>
        </div>
@stop
<?php 
    use Carbon\Carbon;
    ?>
@extends('front.layouts.default')
@section('content')
<div class="full_container">
<div class="body_section">
    <!-- top_header element -->
    @include('front.elements.common_header')       
    <section class="status_tab_block mb-5">
        <div class="container mobile_cont">
            <!-- navbar element -->
            @include('front.elements.navbar')
            <div class="tab_content_sec border p-2 p-md-4">
                <!-- <h4 class="status_title ">Fixtures</h4> -->
                <div class="fixtures_data_block position-relative py-4 px-1 px-md-5">
                    @if(empty($result))
                    <p style=" margin-top: 20px; ">No fixtures have yet been added in the game</p>
                    @endif
                     <div class="fixture_table_slide_toparrow_sec">
                        <a href="javascript:void(0)" class="previous_owl"><img src="{{asset('img/arrow_prev.svg')}}"></a>
                        <a class="ml-auto next_owl" href="javascript:void(0)"><img src="{{asset('img/arrow_next.svg')}}"></a>
                     </div> 

                     
                    <!--  <div class="fixture_table_slide_bottomarrow_sec">
                        <a href="javascript:void(0)" class="previous_owl"><img src="{{asset('img/arrow_prev.svg')}}"></a>
                        <a class="ml-auto next_owl" href="javascript:void(0)"><img src="{{asset('img/arrow_next.svg')}}"></a>
                     </div> -->
                    <div class="fixtures_table_slide owl-carousel fixture_table_owl_carousel">

                        <?php   $m = 1;   $icnt = 0 ;   ?>
                        @if(!empty($result))
                        @foreach($result as $key => $value)
                        @if(!empty($value))
                        <div class="item">
                            <h3 class="players_title d-flex align-items-center mb-3"> Gameweek #{{$value['gw']}}
                            </h3>
                            <div class="table-responsive table-mobile">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                    <tbody>
                                        <tr>
                                            <th>Date</th>
                                            <th>Team</th>
                                            <th>Grade</th>
                                            <th>Scorecard</th>
                                        </tr>
                                        @foreach($value['data'] as $k => $v)
                                        <?php //prd($v); ?>
                                        <?php 
                                            // if(!empty($v->start_date) && Carbon::parse($v->start_date) <= Carbon::now()){
                                            //     $icnt++;  
                                            // }
                                            ?>
                                        <tr>
                                            <td>{{ !empty($v->start_date) ? Carbon::parse($v->start_date)->format('d.m.y') : '' }}</td>
                                            <td>{{ ucwords($v->team_name)}}</td>
                                            <td>{{ !empty($v->grade_name) ? ucwords($v->grade_name) : '-'}}</td>
                                            <!-- <td>{{$v->fixture_scorecard->sum('fantasy_points')}}</td>    -->
                                            <td>
                                                <!-- <a href="{{route('Global.matchScorecard',$v->id)}}" class="more_link border ml-auto"  data-toggle="modal" data-target="#myModal{{$v->id}}">Scorecard</a> -->
                                                <a href="{{route('Global.matchScorecard',$v->id)}}" class="more_link border ml-auto">Scorecard</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
            @if(!empty($result))
            @foreach($result as $key => $value)
            @if(!empty($value))
            @foreach($value['data'] as $k => $v)
            <div class="modal fade" id="myModal{{$v->id}}">
                <div class="modal-dialog modal-xl" >
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Fixture Scorecards</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <table class="table table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>Player</th>
                                        <th>Rs</th>
                                        <th>4s</th>
                                        <th>6s</th>
                                        <th>Ovrs</th>
                                        <th>Mdns </th>
                                        <th>RG</th>
                                        <th>Wks</th>
                                        <th>cs</th>
                                        <th>CWks</th>
                                        <th>Sts</th>
                                        <th>RODs</th>
                                        <th>ROAs</th>
                                        <th>Dks</th>
                                        <th>HT</th>
                                        <th>FP</th>
                                    </tr>
                                </thead>
                                <tbody id="powerwidgets">
                                    <?php 
                                        // dump($v->fixture_scorecards);   die; 
                                        ?>
                                    @if(!empty($v->fixture_scorecards) && $v->fixture_scorecards->isNotEmpty())
                                    @foreach($v->fixture_scorecards as $key => $record)
                                    <tr>
                                        <td>
                                            {{ $record->player_name }}
                                            <?php 
                                                $positionName = "";
                                                if($record->player_position == 1){
                                                    $positionName = 'Batsman';
                                                }elseif($record->player_position == 2){
                                                    $positionName = 'Bowler';
                                                }elseif ($record->player_position == 3) {
                                                    $positionName = 'All Rounder';
                                                }elseif ($record->player_position == 4) {
                                                    $positionName = 'Wicket Keeper';
                                                } ?>
                                            <br>(<em>{{$positionName}}</em>)
                                        </td>
                                        <td>{{$record->rs}}</td>
                                        <td>{{$record->fours}}</td>
                                        <td>{{$record->sixes}}</td>
                                        <td>{{$record->overs}}</td>
                                        <td>{{$record->mdns}}</td>
                                        <td>{{$record->run}}</td>
                                        <td>{{$record->wks}}</td>
                                        <td>{{$record->cs}}</td>
                                        <td>{{$record->cwks}}</td>
                                        <td>{{$record->sts}}</td>
                                        <td>{{$record->rods}}</td>
                                        <td>{{$record->roas}}</td>
                                        <td>{{$record->dks}}</td>
                                        <td>{{$record->hattrick}}</td>
                                        <td>{{$record->fantasy_points}}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="16">No record is yet available.</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            @endforeach
            @endif
    </section>
    <div class="container">
    <div class="favorite_players_block mb-5">
    <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
    </div>
    </div>
    </div>
</div>
<script type="text/javascript">
    $('.previous_owl').on('click',function(){
        $('.owl-carousel').trigger('prev.owl.carousel', [300]);
    });
    $('.next_owl').on('click',function(){
        $('.owl-carousel').trigger('next.owl.carousel', [300]);
    });
</script>
@stop
<?php use Carbon\Carbon;?>
@extends('front.layouts.default')
@section('content')
<script src="{{ asset('css/admin/fancybox/jquery.fancybox.js') }}"></script>
<script src="{{ asset('js/dreamimage.js') }}"></script>
<link href="{{ asset('css/admin/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">

<style type="text/css">
  table.dataTable thead>tr>th.sorting_asc, table.dataTable thead>tr>th.sorting_desc, table.dataTable thead>tr>th.sorting, table.dataTable thead>tr>td.sorting_asc, table.dataTable thead>tr>td.sorting_desc, table.dataTable thead>tr>td.sorting {
    padding-right: 0px !important;
}
.position_img_style{
    width: 20px;
    height: : 20px;
}
#dreamland{
    z-index: 1000;
}
#dreamcontent+h1+span{
    right: 85px !important;
}
</style>
<div class="full_container">
	<div class="body_section">
		<!-- top_header element -->
		@include('front.elements.common_header')      
		<section class="status_tab_block mb-5">
			<div class="container mobile_cont">
				@include('front.elements.navbar')
				<div class="tab_content_sec border  p-0 p-md-4">
					<h4 class="status_title mb-3">Match Scorecard</h4>
					<div class="game_info_block mt-4">
                        <div class="row">
                            <div class="col-12 col-lg-8">
                                <div class="game_user_info_block d-flex">
                                    <!-- <div class="game_user_pic">
                                        <img class="mb-2" src="{{CLUB_IMAGE_URL.$sess_club_name->club_logo}}" alt="game user">
                                    </div> -->
                                    <div class="w-100 game_info pl-0" style="padding-left: 0rem !important">
                                        <ul class="w-100 game_info_list d-flex flex-wrap mt-2">
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Team: {{$fixtureDetails->team_name}}</li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Grade: {{$fixtureDetails->grade_name}}</li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Opponent: {{$fixtureDetails->opposition_club}}</li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Type: {{get_team_type_from_fixture($fixtureDetails->club)}}</li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Status:
                                                <?php 
                                                if(!empty($fixtureScorcardDetails->status)){ 
                                                    if($fixtureScorcardDetails->status == 1){
                                                    echo "Not yet started";
                                                    }elseif($fixtureScorcardDetails->status == 2){
                                                        echo "In-Progress";
                                                    }elseif($fixtureScorcardDetails->status == 3){
                                                        echo "Completed";
                                                    }
                                                }else{
                                                     echo "Not yet started"; 
                                                }
                                                ?>
                                            </li>
                                             <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Date: {{Carbon::parse($fixtureDetails->start_date)->isoFormat('D.M.YY')}}</li>

                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Match:
                                                @if($fixtureDetails->match_type == ONEDAYMATCH)
                                                    1-Day
                                                @elseif($fixtureDetails->match_type == TWODAYMATCH)
                                                    2-Day
                                                @endif
                                            </li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Venue: {{$fixtureDetails->vanue}}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 d-flex pt-4 pt-lg-0">
                                <div class="brought_by_img_block justify-content-start ml-auto">
                                    @if(!empty($sess_club_name) && !empty($sess_club_name->brand_image) && File::exists(BRANDING_IMAGE_ROOT_PATH.$sess_club_name->brand_image))
                                    <img style="width: 90px; height: 90px" src="{{  BRANDING_IMAGE_URL.$sess_club_name->brand_image  }}" alt="brought_by"> 
                                    @else
                                    <img style="width: 90px; height: 90px" src="{{  asset('img/brand_icon.png')  }}" alt="brought_by"> 
                                    @endif
                                    <span class="text-left mt-2" style="color:#000000;">

                                    Presented by</span></div>
                            </div>
                        </div>
                    </div>
                    <div class="games_tabs_sec mt-5">
                        <ul class="nav nav-tabs d-flex" role="tablist">
                          <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#form" role="tab" aria-selected="true">1st Innings</a></li>
                          @if($fixtureDetails->match_type == TWODAYMATCH)
                          <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#game_log" role="tab" aria-selected="false">2st Innings</a></li>
                          @endif
                        </ul>
                        <div class="tab-content border">
                          <div class="tab-pane fade show active table-responsive" id="form">
                                <table cellpadding="0" cellspacing="0" border="0" class="w-100 table table-striped table-bordered dt-responsive nowrap" id="example1">
                                    <thead>
                                        <tr>
                                            <th> Player</th>
                                            <th> R</th>
                                            <th> 4</th>
                                            <th> 6</th>
                                            <th> OVS</th>
                                            <th> MDS</th>
                                            <th> RG</th>
                                            <th> WK</th>
                                            <th> CS</th>
                                            <th> CW</th>
                                            <th> ST</th>
                                            <th> ROD</th>
                                            <th> ROA</th>
                                            <th> DK</th>
                                            <th> HT</th>
                                            <th> FPS</th>
                                        </tr>
                                    </thead>
                                    <tbody id="powerwidgetssd">
                                    @if($scorcard1->isNotEmpty())
                                        @foreach($scorcard1 as $key => $record)
                                            <tr>
                                                <td>
                                                     <?php 
                                                    $positionName = "";
                                                    if($record->player_position == 1){ ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Batsman"><img class="position_img_style" id="theImg" src="{{WEBSITE_IMG_URL}}bat-icon.png"  /></a>
                                                   <?php }elseif($record->player_position == 2){ ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Bowler"><img class="position_img_style" id="theImg" src="{{WEBSITE_IMG_URL}}ball-icon.png" /></a>
                                                  <?php  }elseif ($record->player_position == 3) { ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="All Rounder"><img class="position_img_style" id="theImg" src="{{WEBSITE_IMG_URL}}bat-ball-icon.png" /></a>
                                                  <?php  }elseif ($record->player_position == 4) { ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Wicket-Keeper"><img class="position_img_style" id="theImg" src="{{WEBSITE_IMG_URL}}gloves-icon.png" /></a>
                                                   <?php } ?>
                                               <a href="{{route('Common.playerProfile',$record->player_id)}}" style="padding-right: 30px;">
                                                {{ $record->player_name }}</a>
                                                   
                                                </td>
                                                <td>{{$record->rs}}</td>
                                                <td>{{$record->fours}}</td>
                                                <td>{{$record->sixes}}</td>
                                                <td>{{$record->overs}}</td>
                                                <td>{{$record->mdns}}</td>
                                                <td>{{$record->run}}</td>
                                                <td>{{$record->wks}}</td>
                                                <td>{{$record->cs}}</td>
                                                <td>{{$record->cwks}}</td>
                                                <td>{{$record->sts}}</td>
                                                <td>{{$record->rods}}</td>
                                                <td>{{$record->roas}}</td>
                                                <td>{{$record->dks}}</td>
                                                <td>{{$record->hattrick}}</td>
                                                <td>{{$record->fantasy_points}}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="16">No record is yet available.</td></tr>
                                    @endif


                                    </tbody>                    
                                </table>
                          </div>
                          @if($fixtureDetails->match_type == TWODAYMATCH)
                          <div class="tab-pane fade" id="game_log">
                               <div class="tab-pane fade show active table-responsive" id="form-group">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0" id="example2">
                                    <thead>
                                        <tr>
                                            <tr>
                                            <th>Player</th>
                                            <th>R</th>
                                            <th>4</th>
                                            <th>6</th>
                                            <th>OVS</th>
                                            <th>MDS</th>
                                            <th>RG</th>
                                            <th>WK</th>
                                            <th>CS</th>
                                            <th>CW</th>
                                            <th>ST</th>
                                            <th>ROD</th>
                                            <th>ROA</th>
                                            <th>DK</th>
                                            <th>HT</th>
                                            <th>FPS</th>
                                        </tr>
                                        </tr>
                                    </thead>
                                    <tbody id="powerwidgetsas">
                                    @if($scorcard2->isNotEmpty())
                                        @foreach($scorcard2 as $key => $record)
                                            <?php //prd($record); ?>
                                            <tr>
                                                <td>
                                                <?php 
                                                    $positionName = "";
                                                    if($record->player_position == 1){ ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Batsman"><img class="position_img_style" id="theImg" src="{{WEBSITE_IMG_URL}}bat-icon.png" /></a>
                                                   <?php }elseif($record->player_position == 2){ ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Bowler"><img class="position_img_style" id="theImg" src="{{WEBSITE_IMG_URL}}ball-icon.png" /></a>
                                                  <?php  }elseif ($record->player_position == 3) { ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="All Rounder"><img class="position_img_style" id="theImg" src="{{WEBSITE_IMG_URL}}bat-ball-icon.png" /></a>
                                                  <?php  }elseif ($record->player_position == 4) { ?>
                                                        <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Wicket-Keeper"><img class="position_img_style" id="theImg" src="{{WEBSITE_IMG_URL}}gloves-icon.png" /></a>
                                                   <?php } ?>
                                               <a href="{{route('Common.playerProfile',$record->player_id)}}" style="padding-right: 30px;">
                                                {{ $record->player_name }}</a>
                                                </td>
                                                <td>{{$record->rs}}</td>
                                                <td>{{$record->fours}}</td>
                                                <td>{{$record->sixes}}</td>
                                                <td>{{$record->overs}}</td>
                                                <td>{{$record->mdns}}</td>
                                                <td>{{$record->run}}</td>
                                                <td>{{$record->wks}}</td>
                                                <td>{{$record->cs}}</td>
                                                <td>{{$record->cwks}}</td>
                                                <td>{{$record->sts}}</td>
                                                <td>{{$record->rods}}</td>
                                                <td>{{$record->roas}}</td>
                                                <td>{{$record->dks}}</td>
                                                <td>{{$record->hattrick}}</td>
                                                <td>{{$record->fantasy_points}}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr><td colspan="16">No record is yet available.</td></tr>
                                    @endif


                                    </tbody>                    
                                </table>
                          </div>
                          @endif
                      
                        </div>
                    </div>
                    <form action="" class="statistics_form my_player_form mt-5">
                        <div class="row">
                            @if(!empty($fixtureScorcardDetails->fall_of_wickets))
                            <div class="col-12">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">Fall Of Wickets</label><br>
                                    {{$fixtureScorcardDetails->fall_of_wickets }}
                                </div>
                            </div>
                            @endif
                            @if(!empty($fixtureScorcardDetails->match_report))
                                <div class="col-12">
                                    <div class="form-group mb-4">
                                        <label for="" class="feilds_name">Match Report</label><br>
                                        {{ $fixtureScorcardDetails->match_report}}
                                    </div>
                                </div>
                            @endif
                            <div class="col-12">
                                <div class="form-group mb-4">
                                    <label for="" class="feilds_name">Match Scorecard </label>
                                    <div class="uploaded_file_sec d-flex">
                                        <?php if(!empty($playerScorcards)){ $playerScorcards = explode(',', $playerScorcards); } ?>  
                                        @if(!empty($playerScorcards))
                                            @foreach($playerScorcards as $scorcards)
                                                @if(File::exists(PLAYER_CARD_IMAGE_ROOT_PATH.$scorcards))
                                                <div class="upload_img d-inline-flex align-items-center justify-content-center">
                                                    <img width="95px" height="86px"  id="delete_image_attr" class="fancyboxImg" src="<?php echo PLAYER_CARD_IMAGE_URL.'/'.$scorcards ?>" title="">
                                                    </a>
                                                </div>
                                                @endif
                                            @endforeach
                                        @else
                                        N/A
                                        @endif
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="legand_sec1 p-4 ml-n4 mr-n4 mb-n4">
                            <label class="feilds_name">Legend</label>
                            <div class="table-responsive">
                            <table class="legend_data_table" width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td><label for="" class="feilds_name">4: </label>&nbsp;Fours</td>
                                    <td><label for="" class="feilds_name">6: </label>&nbsp;Sixes</td>
                                    <td><label for="" class="feilds_name">OVS: </label>&nbsp;Overs</td>
                                    <td><label for="" class="feilds_name">MDS: </label>&nbsp;Maiden</td>
                                    <td><label for="" class="feilds_name">RG:</label>&nbsp;Runs Given</td>
                                    <td><label for="" class="feilds_name">WK:</label>&nbsp;Wickets</td>
                                    <td><label for="" class="feilds_name">CW:</label>&nbsp;Catch Wickets</td>
                                    <td><label for="" class="feilds_name">ST:</label>&nbsp;Stump</td>
                                    <td><label for="" class="feilds_name">ROD:</label>&nbsp;Run Out Direct</td>
                                    <td><label for="" class="feilds_name">ROA:</label>&nbsp;Run Out Assist</td>
                                    <td><label for="" class="feilds_name">DK:</label>&nbsp;Duck</td>
                                    <td><label for="" class="feilds_name">HT:</label>&nbsp;Hat-Trick</td>
                                    <td><label for="" class="feilds_name">FPS:</label>&nbsp;Fantasy Points</td>
                                </tr>
                            </table>
                        </div>
                        </div>
                    </form>
				</div>
			</div>
		</section>
	</div>
</div> 
<style type="text/css">
        .dataTables_info { display: none; }

</style>
<script type="text/javascript">
    $('body').tooltip({
    selector: '[data-toggle="tooltip"]'
}); 
table =   $('#example1').DataTable({"lengthChange": false,"scrollX": false, "paging": false, "bLengthChange": false,language: { search: '', searchPlaceholder: "Search"         }, "order": [[ 3, "desc" ]]});

table =   $('#example2').DataTable({"lengthChange": false,"paging": false,"bLengthChange": false, language: { search: '', searchPlaceholder: "Search" }, "order": [[ 3, "desc" ]]});
    $('.fancybox').fancybox();
    $('.fancybox-buttons').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        prevEffect : 'none',
        nextEffect : 'none'
    });

    $(document).ready(function(){ 
        $(".fancyboxImg").dreamimage();
    });

</script>
@stop
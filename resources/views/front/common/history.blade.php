@extends('front.layouts.default')
@section('content')

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

<?php 
    use Carbon\Carbon; 
    ?>
<div class="full_container">
    <div class="body_section">
        <!-- top_header element -->
        @include('front.elements.common_header')      
        <section class="status_tab_block mb-5">
            <div class="container mobile_cont">
                @include('front.elements.navbar')
                <div class="tab_content_sec border  p-0  p-md-4">
                    <div class="next_prev_link_sec d-flex align-items-center mb-5">
                        {{-- <span class="d-inline-flex mx-5">GW #{{$cur_gameweek_no}}</span>  --}}
                        <span>Gameweek history</span>
                    </div>

                    <div class="game_info_block mt-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="game_user_info_block d-flex">
                                    <div class="game_user_pic">
                                        @if(!empty(Auth::user()->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.Auth::user()->image))
                                        <img class="mb-2" src="{{asset('uploads/club/'.Auth::user()->image)}}" alt="game user">
                                        @elseif(!empty(Auth::user()->club_logo) && File::exists(CLUB_IMAGE_ROOT_PATH.Auth::user()->club_logo))
                                        <img class="cricketclub_logo float-left mr-4 club_logo" src="{{CLUB_IMAGE_URL.Auth::user()->club_logo}}" alt="cricketclub" >
                                        @else                                
                                        <img class="mb-2" src="{{asset('img/user_pic.jpg')}}" alt="game user">
                                        @endif
                                    </div>
                                    <div class="w-100 game_info pl-4">
                                        <div class="game_user_name d-flex align-items-center flex-wrap mb-0">{{Auth::user()->full_name}}                     
                                        </div>
                                        <div class="game_user_name d-flex align-items-center flex-wrap mb-0">
                                            <small class="mt-2">{{Auth::user()->my_team_name}}</small> 
                                        </div>
                                        <ul class="w-100 game_info_list py-2 mt-2">
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a  href="{{url('points')}}">Gameweek</a></li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a  href="{{url('history')}}">Gameweek History</a></li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> <a  href="{{url('trade-history')}}">Trade History</a></li>
                                           
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="game_info_block mt-4">
                        <div class="row">
                            <div class="col-12 col-lg-12">
                                <div class="table-responsive table-mobile">
                                    <table id="example" width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table  mb-0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="12.5%">GW</th>
                                            <th width="12.5%">GWP</th>
                                            <th width="12.5%">GWR</th>
                                            <th width="12.5%">TM</th>
                                            <th width="12.5%">OVP</th>
                                            <th width="12.5%">OVR</th>
                                            <th width="12.5%">VALUE</th>
                                            <th width="12.5%">MOVE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   
                                        @if(!empty($top_player_by_gwk_point))
                                        @foreach($top_player_by_gwk_point as $key => $value)

                                        <tr>
                                            <td data-order="{{$value['gw']}}">GW{{$value['gw']}}</td>
                                            <td>{{$value['gw_pts']}}</td>
                                            <td>{{$value['gw_rank']}}</td> 
                                            <td>{{$value['trades_made']}}</td>
                                            <td>{{$value['overall_pts']}}</td>
                                            <td>{{!empty($value['overall_rank']) ? $value['overall_rank'] : '--'}}</td>
                                            <td>${{$value['team_value']}}m</td>
                                            <td>
                                                @if(empty($prev_val))
                                                    --
                                                @elseif($prev_val > $value['team_value'])
                                                    DOWN
                                                @elseif($prev_val < $value['team_value'])
                                                    UP
                                                @else
                                                    NO CHANGE
                                                @endif
                                            </td>
                                        </tr>
                                        <?php $prev_val = $value['team_value'];  ?>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td  colspan="7">No record yet available.</td>
                                        </tr>
                                        @endif
                                        
                                    </tbody>     
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="legand_sec1 p-4 ml-n4 mr-n4 mb-n4">
                        <label class="feilds_name">Legend</label>
                        <div class="table-responsive">
                        <table class="legend_data_table" width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td><label for="" class="feilds_name">GW: </label>&nbsp;Gameweek</td>
                                <td><label for="" class="feilds_name">GWP: </label>&nbsp;Gameweek Point</td>
                                <td><label for="" class="feilds_name">GWR: </label>&nbsp;Gameweek Rank</td>
                                <td><label for="" class="feilds_name">TM:</label>&nbsp;Trades Made</td>
                                <td><label for="" class="feilds_name">OVP:</label>&nbsp;Overall Points</td>
                                <td><label for="" class="feilds_name">OVR:</label>&nbsp;Overall Rank</td>
                            </tr>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container pt-4">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
   


// $('a.page-link').click(function(){


$(document).ready(function() {


 var table =   $('#example').DataTable(); 

/*
  var table =   $('#example').DataTable( {
        "pageLength": 2,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "ajax": "{{route('Common.loadHistoryData')}}"

    } );
  */ 
  // console.log(table.page.info()); 

  // pgno = table.page.info().page; 
} );



/*

$( document ).ready(function() {
    // $('#loader_img_1').show(); 
   setTimeout(function () {
       $('#loader_img').show(); 
    }, 2000);


    $.ajax({
      type: "GET",
      url: "{{route('Common.loadHistoryData')}}",
      data: {},
      // cache: false,
      async: true,
      success: function(data){
        // console.log(data); 
        if(data && data.top_player_by_gwk_point){
            
            var tmp_html = ''; 
            prev_val = 0; 
            $.each(data.top_player_by_gwk_point, function(index,obj){
                // console.log(index);
                // console.log(obj); 
                tmp_html += '<tr><td>'+obj.gw+'</td>'; 
                tmp_html += '<td>'+obj.gw_pts+'</td>'; 
                tmp_html += '<td>'+(data['temp_last_wk_pt'][obj.gw] && data['temp_last_wk_pt'][obj.gw][data['team_id']] ? data['temp_last_wk_pt'][obj.gw][data['team_id']] : '--')+'</td>'; 
                tmp_html += '<td>'+obj.trades_made+'</td>'; 
                tmp_html += '<td>'+(obj.overall_pts ? obj.overall_pts : '--')+'</td>'; 
                tmp_html += '<td>'+(obj.overall_rank ? obj.overall_rank : '--')+'</td>'; 
                tmp_html += '<td>'+obj.team_value+'</td>'; 
                tmp_html += '<td>'+(prev_val ? (prev_val > obj.team_value ? 'DOWN' : (prev_val < obj.team_value ? 'UP' : 'NO CHANGE') ) : 'NO CHANGE')+'</td></tr>'; 
                prev_val = obj.team_value; 
            });

            $('#example tbody').append(tmp_html); 

            // $('#loader_img').hide(); 
        }
        var oTable = $('#example').dataTable();
         $('#loader_img').hide(); 

   setTimeout(function () {
       $('#loader_img').hide(); 
    }, 1500);

      }
    });
}); 

*/ 

</script>

<style type="text/css">
     #example_wrapper .dataTables_info , #example_length, #example_filter { display: none; }

</style>

@stop
@extends('front.layouts.default')
@section('content')
<div class="body_section">

	  <div class="container py-3 py-lg-4">
            <h2 class="block_title"><small>MyClubtap</small> Partners</h2>
        </div>

        <section class="chooseus_block pt-0 pt-lg-5 pb-5">
            <div class="container">
           
                <div class="tab_content_sec border p-0  p-md-4">
               
                    <ul class="sponsors_list partners_list d-block d-lg-flex flex-wrap my-n3">
                        @if(!empty($result))
                            @foreach ($result as $value)
                                <li class="d-flex d-md-flex p-3 p-md-3 my-4">
                                    <div class="sponsors_img">
                                        @if(File::exists(SPONSOR_IMAGE_ROOT_PATH.$value['logo']))
                                            <a href="{{$value['website']}}"> <img src="<?php echo WEBSITE_URL.'image.php?width=77px&height=90px&image='.SPONSOR_IMAGE_URL.'/'.$value['logo'] ?>" alt="Partners"></a>
                                        @endif
                                    </div>
                                    <div class="w-100 pl-0 pl-md-4 pt-3 pt-md-0">
                                        <div class="sponsors_list_heading d-block d-sm-flex align-items-center mb-2">
                                            <div class="mr-auto"> <a href="{{$value['website']}}"> {{$value['name']}}</a></div>
                                            <ul class="social_links d-inline-flex align-items-center ml-auto mt-2 mt-sm-0">
                                                 @if($value['facebook'])
                                                <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" target="_blank" href="{{$value['facebook']}}" title="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                                @endif
                                                @if($value['twitter'])
                                                <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" target="_blank" href="{{$value['twitter']}}" title="twitter"><i class="fab fa-twitter"></i></a></li>
                                                 @endif
                                                @if($value['instagram'])
                                                <li class="d-inline-flex mr-2"><a class="d-flex align-items-center justify-content-center" target="_blank" href="{{$value['instagram']}}" title="instagram"><i class="fab fa-instagram"></i></a></li>
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="mb-2">{{!empty($value['about']) ? $value['about'] :''}}</div>
                                        <?php 


                $url = $value['website']; 
                if (!empty($url) && !preg_match("~^(?:f|ht)tps?://~i", $url)) { $url = "http://" . $url; ?>
                                        <a target="_blank" href="{{$url}}" class="sponsors_btn">
                    <?php  } else { ?>
                         <a target="_blank" href="{{$value['website']}}" class="sponsors_btn">
                    <?php } ?>

                                       
                                            @if(!empty($value['offer']))
                                                {{$value['offer']}}
                                            @else
                                                Offer 5% off
                                            @endif
                                        </a>                                
                                    </div>
                                </li>
                         @endforeach
                        @else
                            <li class="d-block d-md-flex p-3 p-md-3 my-4">
                                <div class="sponsors_list_heading d-block d-sm-flex align-items-center mb-2">
                                    <div class="sponsors_list_heading mb-1">No record yet available.</div>
                                </div>
                            </li>
                        @endif

                    </ul>
                </div>
            </div>
        </section>
        <div class="container pt-4">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
            </div>
        </div>
</div>
@stop
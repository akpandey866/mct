@extends('front.layouts.default')
@section('content')
<style type="text/css">
    .aud_class{
        display: block;
    }
</style>
<div class="body_section">
   <section class="top_banner_block inner_top_banner d-flex align-items-end pb-3" style="background-image: url({{asset('img/inner_banner.png')}});">
      <div class="container py-3 py-lg-4">
        <h2 class="block_title"><small>Get your own</small>Fantasy Game</h2>
         <!--<a href="" class="btn py-2 px-5">Join Now</a>-->
      </div>
   </section>
   <section class="price_table_block py-5">
      <div class="container">
        <h3 class="price_info_title mb-4">For Clubs &amp; Leagues</h3>
        <div class="pricing_info_block pb-5">
            <div class="faq_collapse mt-0 mb-5" id="accordion">
              <div class="card border mb-3">
                <div class="card-header" id="headingOne">
                    <a class="collapse_point" data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">We want a fantasy game for our Senior grade players (mens, women &amp; veterans) only.</a>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body border-top p-4">
                    <div class="row my-n3">
                        <div class="col-12 col-md-6 col-lg-4 py-3">
                            <div class="h-100 pricing_info_sec border">
                                <h5 class="pricing_info_title">Features</h5>
                                <div class="pricing_info_data px-4 py-3">
                                    <ul class="pricing_info_list pl-3">
                                        <li class="my-2">Create and run your fantasy game</li>
                                        <li class="my-2">30+ admin and platform <a href="{{url('features')}}">features</a></li>
                                        <li class="my-2">Get your personalized admin portal</li>
                                        <li class="my-2">Get your personalized game lobby</li>
                                        <li class="my-2">Activate Branding for $50 (optional)</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 py-3">
                            <div class="h-100 pricing_info_sec border">
                                <h5 class="pricing_info_title">Add-Ons</h5>
                                <div class="pricing_info_data px-4 py-3">
                                    <ul class="pricing_info_list pl-3">
									  <li class="my-2">Fundraiser <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a paid add-on (for free in 2019) for Senior Club mode fantasy games which allows clubs to do fundraising through their fantasy game (available in AUS only)."><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                        <li class="my-2">Availability <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to create a list of players who are unavailable during the course of your game."><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                        <li class="my-2">Scorer <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to turn End Users of your game into 'Scorers', empowering those end users with the ability to do match score entries for assigned teams."><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                        <li class="my-2">Verify <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to map, connect and verify an End User of your game, with his/her 'Player' record within your game.
"><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                        <li class="my-2">Sponsor <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to add and advertise unlimited sponsors of your own club or league.

"><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 py-3">
                            <div class="h-100 pricing_info_sec border">
                                <h5 class="pricing_info_title">Price</h5>
                                <div class="pricing_info_data text-center px-4 py-3">
                                    <div class="priceing_info_rate my-3"><strong>$19 + $0.50 </strong><span class="aud_class">(AUD)</span>per player*</div>
                                    *first 20 club players included free
                                    <div class="mt-4"><a href="{{route('Home.signup')}}" class="btn">Sign Up</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card border mb-3">
                <div class="card-header" id="headingTwo">
                    <a class="collapse_point" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">We want a fantasy game for our Junior grade players (boys &amp; girls) only.</a>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                  <div class="card-body border-top p-4">
                    <div class="row my-n3">
                        <div class="col-12 col-md-6 col-lg-4 py-3">
                            <div class="h-100 pricing_info_sec border">
                                <h5 class="pricing_info_title">Features</h5>
                                <div class="pricing_info_data px-4 py-3">
                                    <ul class="pricing_info_list pl-3">
                                        <li class="my-2">Create and run your fantasy game</li>
                                        <li class="my-2">30+ admin and platform features</li>
                                        <li class="my-2">Get your personalized admin portal</li>
                                        <li class="my-2">Get your personalized game lobby</li>
                                        <li class="my-2">Activate Branding for $50 (optional)</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 py-3">
                            <div class="h-100 pricing_info_sec border">
                                <h5 class="pricing_info_title">Add-Ons</h5>
                                <div class="pricing_info_data px-4 py-3">
                                    <ul class="pricing_info_list pl-3">
                                       <li class="my-2">Availability <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to create a list of players who are unavailable during the course of your game."><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                        <li class="my-2">Scorer <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to turn End Users of your game into 'Scorers', empowering those end users with the ability to do match score entries for assigned teams."><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                        <li class="my-2">Verify <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to map, connect and verify an End User of your game, with his/her 'Player' record within your game.
"><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                        <li class="my-2">Sponsor <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to add and advertise unlimited sponsors of your own club or league.

"><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 py-3">
                            <div class="h-100 pricing_info_sec border">
                                <h5 class="pricing_info_title">Price</h5>
                                <div class="pricing_info_data text-center px-4 py-3">
                                    <div class="priceing_info_rate my-4"><strong>$29 <small>(AUD)</small></strong></div>
                                   <div class="mt-0"><a href="{{route('Home.signup')}}" class="btn">Sign Up</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card border mb-3">
                <div class="card-header" id="headingThree">
                    <a class="collapse_point" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">We want a combined fantasy game based on our Senior and Junior grade players only.</a>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                  <div class="card-body border-top p-4">
                    <div class="row my-n3">
                        <div class="col-12 py-3">
                            <div class="h-100 pricing_info_sec border">
                                <h5 class="pricing_info_title">Features</h5>
                                <div class="pricing_info_data px-4 py-3">
                                    <ul class="pricing_info_list pl-3">
                                        <li class="my-2">It is not recommended to combine and include Junior grades into the Senior fantasy team and vice-versa. MyClubtap enables you to create and have separate fantasy games based on Senior grade players and Junior grade players. See the above sections for more on respective game modes.</li>
                                        <li class="my-2">NOTE: MyClubtap runs routine checks on the right use of Junior club mode and can ask your club for further age-proof on players added in the junior game, to ensure no Senior players (19+) are added in the junior game. Any game found in breach of using Junior Club mode to run a fantasy game based on Senior players will be immediately blocked from further use on MyClubtap without notice and is non-refundable.</li>
                                        <li class="my-2">Read more here on separate game modes for Senior &amp; Juniors on MyClubtap. Please do reach out to us for any further questions on this.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card border mb-3">
                <div class="card-header" id="headingFour">
                    <a class="collapse_point" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">We want a fantasy game for a grade or competition of an association or league only.</a>
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                  <div class="card-body border-top p-4">
                    <div class="row my-n3">
                        <div class="col-12 col-md-6 col-lg-4 py-3">
                            <div class="h-100 pricing_info_sec border">
                                <h5 class="pricing_info_title">Features</h5>
                                <div class="pricing_info_data px-4 py-3">
                                    <ul class="pricing_info_list pl-3">
                                        <li class="my-2">Create and run your fantasy game</li>
                                        <li class="my-2">30+ admin and platform features</li>
                                        <li class="my-2">Get your personalized admin portal</li>
                                        <li class="my-2">Get your personalized game lobby</li>
                                        <li class="my-2">Activate Branding for $50 (optional)</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 py-3">
                            <div class="h-100 pricing_info_sec border">
                                <h5 class="pricing_info_title">Add-Ons</h5>
                                <div class="pricing_info_data px-4 py-3">
                                    <ul class="pricing_info_list pl-3">
                                       <li class="my-2">Availability <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to create a list of players who are unavailable during the course of your game."><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                        <li class="my-2">Scorer <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to turn End Users of your game into 'Scorers', empowering those end users with the ability to do match score entries for assigned teams."><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                        <li class="my-2">Verify <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to map, connect and verify an End User of your game, with his/her 'Player' record within your game.
"><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                        <li class="my-2">Sponsor <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="This is a free add-on which allows you to add and advertise unlimited sponsors of your own club or league.

"><i style="color: #0f70b7; " class="fa fa-question-circle" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 py-3">
                            <div class="h-100 pricing_info_sec border">
                                <h5 class="pricing_info_title">Price</h5>
                                <div class="pricing_info_data text-center px-4 py-3">
                                    <div class="priceing_info_rate my-4"><strong>$99 <!-- <small>(AUD)</small> --></strong> (in AUD)</div>
                                    <div class="mt-0"><a href="{{route('Home.signup')}}" class="btn">Sign Up</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <h3 class="price_info_title mb-4">For Users</h3>
            <div class="price_users_sec">
                <p>It is 100% free for all users to join, enter a fantasy team to play any fantasy game of their choice on MyClubtap!</p>
            <!--   <a href="{{route('Home.userSignup')}}" class="btn">Sign Up</a>
                 <a href="{{route('Home.login')}}" class="btn">Login as User</a> -->
            </div>
       </div>
      </div>
   </section>
   @include('front.elements.subscribe')
</div>
@include('front.elements.subscribe_pop_up') 
<script type="text/javascript">
    function contact_us() { 
    	$('#loader_img').show();
    	$('.help-inline').html('');
    	$('.help-inline').removeClass('error');
    	$.ajax({ 
    		url: '{{ URL("subscribe") }}',
    		type:'post',
    		data: $('#contact_form').serialize(),
    		success: function(r){
    			error_array 	= 	JSON.stringify(r);
    			data			=	JSON.parse(error_array);
    			if(data['success'] == 1) {
    				window.location.href	 =	"{{ URL('/') }}";
    			}else { 
    				$.each(data['errors'],function(index,html){
    					$("#"+index).next().addClass('error');
    					$("#"+index).next().html(html);
    				});
    				$('#loader_img').hide();
    			}
    			$('#loader_img').hide();
    		}
    	});
    }
	$('#contact_form').each(function() {
		$(this).find('input').keypress(function(e) {
	      if(e.which == 10 || e.which == 13) {
			contact_us();
			return false;
	       }
	   });
	     
});

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   

});
</script>

<style type="text/css">
    div.tooltip-inner {
    max-width: 250px;
    text-align: left;
    }
    div.tooltip-inner h6{ font-weight: bold;  }
    div.tooltip-inner li { list-style: disc; }
    #myModal li{ list-style: none; }
</style>
@stop
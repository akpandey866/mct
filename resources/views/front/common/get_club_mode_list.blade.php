 {{ Form::select('club',[null => 'Please Select Club'] + $getClubMode,((isset($club_id)) ? $club_id : ''),['id' => 'get_club_name','class'=>'form-control'])}}
 <div class="error-message help-inline club_error"></div>
 <script type="text/javascript">
 	$("#get_club_name").on('change',function(){ 
	var selectValue   =  $(this).val();
    $('#loader_img').show(); 
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"{{  url('get-club-game-name') }}",
        'type':'post',
        data:{'id':selectValue},
        success:function(response){
            $('.put_game_name_html').html(response);
            $('#loader_img').hide();
        }
    });
});


 </script>
@extends('front.layouts.default')
@section('content')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<div class="full_container">
<div class="body_section">
    <!-- top_header element -->
   <?php // @include('front.elements.common_header')  ?>       
    <section class="status_tab_block mb-5 py-3">
        <div class="container mobile_cont">
            <?php //@include('front.elements.navbar') ?>
            <div class="tab_content_sec border  p-0  p-md-4">
                <h4 class="status_title mb-3">My Fundraisers <a href="javascript:void();" data-html="true" data-toggle="tooltip" title="List of senior mode fantasy games you have supported on MyClubtap."><i style="color: #0f70b7; " class="fa fa-info-circle" aria-hidden="true"></i></a></h4>
                <!-- <div class="alert alert-success player_verified_message mb-4" role="alert"></div> -->
                <div class="table-responsive table-mobile">
                    <table id="example" width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table states_table mb-0">
                        <thead>
                            <tr>
                                <!-- <th width="70px">Rank</th> -->
                                <th class="colmn1">Club Name</th>
                                <th class="colmn2">Amount</th>
                                <th class="colmn3">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($fundraiserData))
                            @foreach($fundraiserData as $val)
                            <tr>
                                <td  data-search="{{$val->club_name}}">{{!empty($val->club_name) ? $val->club_name :'' }} </td>
                                <td>${{!empty($val->amount) ? $val->amount :'' }} </td>
                                <td>{{ date('d.m.y' ,strtotime($val->created_at)) }}</td>
                            </tr>
                            @endforeach
                            @else 
                            <tr>
                                <td colspan="5">No players have yet been added to the game.</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
    </section>
    <div class="container">
    <div class="favorite_players_block mb-5">
    <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
    </div>
    </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   

    });
    table =   $('#example').DataTable({"pageLength": 25,"lengthChange": false,  language: { search: '', searchPlaceholder: "Search" }});
    var sc_width = screen.width;
    $( document ).ready(function() {
     if(sc_width < 640){ 
      $('.colmn1').text('Club Name');
      $('.colmn2').text('Amount');
      $('.colmn3').text('Date');
     }
    });
</script>
<style type="text/css">
    #example_wrapper .dataTables_info { display: none; }
    #example_filter { text-align: right; }
</style>
@stop
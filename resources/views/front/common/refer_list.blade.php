@extends('front.layouts.default')
@section('content')
<?php 
use Carbon\Carbon;
?>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

<div class="full_container">
<div class="body_section">
    <!-- top_header element -->
    @include('front.elements.common_header')       
    <section class="status_tab_block mb-5">
        <div class="container mobile_cont">
            <!-- navbar element -->
            @include('front.elements.navbar')
            <div class="tab_content_sec border  p-0  p-md-4">
                    <h4 class="status_title ">Refer list</h4>
                    <div class="table-responsive table-mobile">
                        <table id="example" width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table states_table mb-0">
                            <thead>
                                <tr>
                                    <th class="colmn1">Image</th>
                                    <th class="colmn2" width="30%">User</th>
                                    <th class="colmn3">Created</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$result->isEmpty())
                                    @foreach($result as $val)
                                        <tr>                                
                                           <td>
                                            @if(!empty($val->image) && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.$val->image))
                                                <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo USER_PROFILE_IMAGE_URL.auth()->guard('web')->user()->image; ?>"> <div class="usermgmt_image">
                                                        <img  src="<?php echo WEBSITE_URL.'image.php?width=50px&height=50px&cropratio=1:1&image='.USER_PROFILE_IMAGE_URL.auth()->guard('web')->user()->image ?>">
                                                    </div>
                                                </a>
                                            @else
                                            <img width="50px" height="50px" src="{{asset('img/user_pic.jpg')}}" alt="User">
                                            @endif
                                           </td> 
                                            <td>{{$val->full_name}}</td>
                                            <td>{{Carbon::parse($val->created_at)->isoFormat('ddd').' - '.Carbon::parse($val->created_at)->isoFormat('D.M.YY')}}</td>
                                        </tr>
                                    @endforeach
                                @else 
                                <tr><td colspan="3">No users have yet been added.</td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
    </section>
        <div class="container">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
table =   $('#example').DataTable({"pageLength": 2,"lengthChange": false,  language: { search: '', searchPlaceholder: "Search" }, "order": [[ 1, "asc" ]]});
</script>
@stop
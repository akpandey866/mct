@extends('front.layouts.default')
@section('content')
<?php 
use Carbon\Carbon; 
?>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<style type="text/css">
.highcharts-credits {
    display: none;
}
.tooltip_class{
  font-size: 20px;
  margin-top: 5px;
  margin-left: 5px;
}  
</style>
<div class="full_container">
	<div class="body_section">
		<!-- top_header element -->
		@include('front.elements.common_header')      
		<section class="status_tab_block mb-5">
			<div class="container mobile_cont">
				@include('front.elements.navbar')
				<div class="tab_content_sec border  p-0  p-md-4">
					<!-- <h4 class="status_title mb-3">Profile</h4> -->
					<div class="game_info_block mt-4">
                        <div class="row">
                            <div class="col-12 col-lg-8">
                                <div class="game_user_info_block d-block d-sm-flex">
                                    <div class="d-flex">
                                        <div>
                                            <div class="game_user_pic">
                                                <!-- <img class="mb-2" src="{{asset('img/game_user_pic.jpg')}}" alt="game user"> -->
                                                @if(!empty($player_data->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.$player_data->image))
                                                <img  class="mb-2"  src="<?php echo WEBSITE_URL.'image.php?width=174px&height=174px&cropratio=1:1&image='.PLAYER_IMAGE_URL.$player_data->image ?>" alt="top players">
                                                @else
                                                    @if($player_data->position == 1)
                                                        <a href="{{url('player-profile/'.$player_data->id)}}">
                                                        <img class="player_profile_d_image"  src="{{asset('img/bm.png')}}" alt="top players"></a>
                                                    @endif
                                                    @if($player_data->position == 2)
                                                        <a href="{{url('player-profile/'.$player_data->id)}}">
                                                        <img class="player_profile_d_image"  src="{{asset('img/bow.png')}}" alt="top players"></a>
                                                    @endif
                                                    @if($player_data->position == 3)
                                                        <a href="{{url('player-profile/'.$player_data->id)}}">
                                                        <img class="player_profile_d_image"  src="{{asset('img/ar.png')}}" alt="top players"></a>
                                                    @endif
                                                    @if($player_data->position == 4)
                                                        <a href="{{url('player-profile/'.$player_data->id)}}">
                                                        <img class="player_profile_d_image"  src="{{asset('img/wc.png')}}" alt="top players"></a>
                                                    @endif
                                                @endif 
                                            </div>
                                        </div>
                                        <div class="d-inline-block d-sm-none ml-auto">
                                            <div>
                                                @if(!empty($sess_club_name) && !empty($sess_club_name->brand_image) && File::exists(BRANDING_IMAGE_ROOT_PATH.$sess_club_name->brand_image))
                                                <img style="width: 90px; height: 90px" src="{{  BRANDING_IMAGE_URL.$sess_club_name->brand_image  }}" alt="brought_by"> 
                                                @else
                                                <img style="width: 90px; height: 90px" src="{{  asset('img/brand_icon.png')  }}" alt="brought_by"> 
                                                @endif
                                            </div>
                                            <span class="text-left mt-2" style="color:#000000;">Presented by</span>                                            
                                        </div>
                                    </div>
                                    <div class="w-100 game_info pl-4 mt-4 mt-sm-0">
                                        <div class="game_user_name d-flex align-items-center flex-wrap mb-2">{{!empty($player_data->full_name) ? $player_data->full_name : ''}}  
                                            <!-- <img class="ml-2" src="{{asset('img/right.png')}}" alt="right"> -->
                                            @if(!empty($playerAvailability) && ( !empty(Carbon::parse($playerAvailability->date_from)) && ( ( Carbon::parse($playerAvailability->date_from) >= Carbon::now() && Carbon::now()->diffInDays(Carbon::parse($playerAvailability->date_from)) <= 10 ) || Carbon::parse($playerAvailability->date_from) <= Carbon::now() ) ) && !empty(Carbon::parse($playerAvailability->date_till)) && Carbon::now() <= Carbon::parse($playerAvailability->date_till) )


                                            <a  data-html="true" data-toggle="tooltip" title="{{$player_data->full_name}} will not available from {{!empty($playerAvailability->date_from) ? Carbon::parse($playerAvailability->date_from)->isoFormat('D/M/Y') : '--' }} to {{!empty($playerAvailability->date_till) ? Carbon::parse($playerAvailability->date_till)->isoFormat('D/M/Y') : '--' }}" class="tooltip_class"><i style="color: #FF0000; " class="fa fa-info-circle" aria-hidden="true"></i></a>
                                            @endif


                                    <?php /* <small class="w-100 mt-2">{{!empty($player_data->my_team_name) ? $player_data->my_team_name : ''}}</small> */ ?>

                                        </div>
                                        <ul class="w-100 game_info_list py-2 mt-2">
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Position: {{$position[$player_data->position]}}</li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Batting style: {{!empty($player_data->player_bat_style) ? $player_data->player_bat_style : '-'}}</li>
                                            <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Bowling style: {{!empty($player_data->player_bowl_style) ? $player_data->player_bowl_style : '-'}}</li>
                                             <li class="px-3 py-1"><i class="fas fa-angle-right"></i> Primary Team: {{!empty($player_data->team_name) ? $player_data->team_name : '-'}}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                           <div class="col-12 col-lg-4 d-flex pt-4 pt-lg-0">
                                <!-- <div class="brought_by_img_block justify-content-center text-center ml-auto"> -->
                                <div class="brought_by_img_block justify-content-start d-none d-sm-block ml-auto">
                                    @if(!empty($sess_club_name) && !empty($sess_club_name->brand_image) && File::exists(BRANDING_IMAGE_ROOT_PATH.$sess_club_name->brand_image))
                                    <img style="width: 90px; height: 90px" src="{{  BRANDING_IMAGE_URL.$sess_club_name->brand_image  }}" alt="brought_by"> 
                                    @else
                                    <img style="width: 90px; height: 90px" src="{{  asset('img/brand_icon.png')  }}" alt="brought_by"> 
                                    @endif
                                    <span class="text-left mt-2" style="color:#000000;">Presented by</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about_info_block p-3 mt-4">
                        <div class="about_info_block_title">About:</div>
                        {{ !empty($player_data->description) ? $player_data->description : 'No player description yet added.'}}
                    </div>
                    <div class="player_profile_list_block mt-5">
                        <ul class="game_week_info_list d-flex my-n2 mb-4 mobile_teamdeta liborderblock">
                         <li class="d-inline-flex align-items-center p-3 my-2">
                            <div class="w-100 game_week_info d-flex align-items-center">
                               <div class="game_week_info_icon text-center"><img src="{{asset('img/game_week_info_icon7.png')}}" alt="game_week_info"></div>
                               <div class="game_week_info_status pl-2">{{$player_fp}} <small class="mt-1">Fantasy Points</small></div>
                            </div>
                         </li>
                         <li class="d-inline-flex align-items-center p-3 my-2">
                            <div class="w-100 game_week_info d-flex align-items-center">
                               <div class="game_week_info_icon text-center"><img src="{{asset('img/game_week_info_icon11.png')}}" alt="game_week_info"></div>
                               <div class="game_week_info_status pl-2">${{$player_data->svalue}}m <small class="mt-1">Fantasy Value</small></div>
                            </div>
                         </li>
                         <li class="d-inline-flex align-items-center p-3 my-2">
                            <div class="w-100 game_week_info d-flex align-items-center">
                               <div class="game_week_info_icon text-center"><img src="{{asset('img/game_week_info_icon9.png')}}" alt="game_week_info"></div>
                               <div class="game_week_info_status pl-2">{{round($player_count)}}% <small class="mt-1">Picked In Teams</small></div>
                            </div>
                         </li>
                         <li class="d-inline-flex align-items-center p-3 my-2">
                            <div class="w-100 game_week_info d-flex align-items-center">
                               <div class="game_week_info_icon text-center"><img src="{{asset('img/game_week_info_icon12.png')}}" alt="game_week_info"></div>
                               <div class="game_week_info_status pl-2">{{round($cap_count)}}% <small class="mt-1">Picked As Cap</small></div>
                            </div>
                         </li>
                         <li class="d-inline-flex align-items-center p-3 my-2">
                            <div class="w-100 game_week_info d-flex align-items-center">
                               <div class="game_week_info_icon text-center"><img src="{{asset('img/game_week_info_icon12.png')}}" alt="game_week_info"></div>
                               <div class="game_week_info_status pl-2">{{round($vc_count)}}% <small class="mt-1">Picked As V-Cap</small></div>
                            </div>
                         </li>
                      </ul>
                    </div>
                    <div class="row pt-5">
                        <div class="col-12 col-md-12">
                            <h4 class="status_title" style="color: #1b6cb5;">Batting</h4>
                            <ul class="batting_bowling_fielding_info d-flex align-items-center">
                                <li> {{ !empty($totalMatches) ? $totalMatches :0 }} <small class="mt-1">Matches</small></li>
                                <li> {{ !empty($totalInnings) ? $totalInnings :0 }} <small class="mt-1">Innings</small></li>
                                <li>{{ !empty($playerScorecardData->sum_rs) ? $playerScorecardData->sum_rs :0 }} <small class="mt-1">Runs</small></li>
                                <li>{{ !empty($playerScorecardData->sum_fours) ? $playerScorecardData->sum_fours :0 }} <small class="mt-1">Fours</small></li>
                                <li>{{ !empty($playerScorecardData->sum_sixes) ? $playerScorecardData->sum_sixes :0 }}<small class="mt-1">Sixes</small></li>
                                <li> {{ !empty($playerScorecardData->sum_dks) ? $playerScorecardData->sum_dks :0 }} <small class="mt-1">Ducks</small></li>
                                <li class="batting_bowling_fielding_icon ml-auto"><img src="{{asset('img/batting_icon.png')}}" alt="batting"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row pt-5">
                        <div class="col-12 col-md-12">
                            <h4 class="status_title" style="color: #1b6cb5;">Bowling</h4>
                            <ul class="batting_bowling_fielding_info d-flex align-items-center">
                                <li>{{ !empty($playerScorecardData->sum_overs) ? $playerScorecardData->sum_overs :0 }}<small class="mt-1">Overs</small></li>
                                <li>{{ !empty($playerScorecardData->sum_mdns) ? $playerScorecardData->sum_mdns :0 }} <small class="mt-1">Maidens</small></li>
                                <li>{{ !empty($playerScorecardData->sum_rg) ? $playerScorecardData->sum_rg :0 }} <small class="mt-1">Runs Given</small></li>
                                <li>{{ !empty($playerScorecardData->sum_wks) ? $playerScorecardData->sum_wks :0 }} <small class="mt-1">Wickets</small></li>
                                <li>{{ !empty($playerScorecardData->sum_hattrick) ? $playerScorecardData->sum_hattrick :0 }} <small class="mt-1">Hat-trick</small></li>
                                <li class="batting_bowling_fielding_icon ml-auto"><img src="{{asset('img/bowling_icon.png')}}" alt="bowling"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row pt-5">
                        <div class="col-12  pt-2 col-md-12">
                            <h4 class="status_title" style="color: #1b6cb5;">Fielding</h4>
                            <ul class="batting_bowling_fielding_info d-flex align-items-center">
                                <li>{{ !empty($playerScorecardData->sum_cs) ? $playerScorecardData->sum_cs :0 }} <small class="mt-1">Catches</small></li>
                                <li>{{ !empty($playerScorecardData->sum_cwks) ? $playerScorecardData->sum_cwks :0 }} <small class="mt-1">Catches WK</small></li>
                        
                            
                                <li>{{ !empty($playerScorecardData->sum_sts) ? $playerScorecardData->sum_sts :0 }}  <small class="mt-1">Stumpings</small></li>
                          
                  
                                <li>{{ !empty($playerScorecardData->sum_rods) ? $playerScorecardData->sum_rods :0 }} <small class="mt-1">RO Direct</small></li>
                   
                  
                                <li>{{ !empty($playerScorecardData->sum_roas) ? $playerScorecardData->sum_roas :0 }} <small class="mt-1">RO Assist</small></li>
                     
                                <li class="batting_bowling_fielding_icon ml-auto"><img src="{{asset('img/fielding_icon.png')}}" alt="fielding"></li>
                            </ul>
                        </div>
                    </div>
                     
                    <?php /*
                    <div class="fixtures_block mt-5">
                        <h4 class="status_title mb-3">Gameweeks</h4>
                        <div class="table-responsive table-mobile">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table states_table mb-0">
                                <tr>
                                    <th>GW</th>
                                    <!-- <th>PIT</th> -->
                                    <th>PTS</th>
                                    <th>R</th>
                                    <th>4</th>
                                    <th>6</th>
                                    <th>MDS</th>
                                    <th>WK</th>
                                    <th>CS</th>
                                    <th>HT</th>
                                </tr>
                    @if(!empty($top_player_by_gwk_point))
                        @foreach($top_player_by_gwk_point as $key => $value)
                            <tr>
                                <td>GW{{$value['gwk']}}</td>
                              <!--   <td>674</td> -->
                                <td>{{$value['fp']}}</td>
                                <td>{{$value['sum_rs']}}</td>
                                <td>{{$value['sum_fours']}}</td>
                                <td>{{$value['sum_sixes']}}</td>
                                <!-- <td></td> -->
                                <td>{{$value['sum_mdns']}}</td>
                                <!-- <td>6</td> -->
                                <td>{{$value['sum_wks']}}</td>
                                <td>{{$value['sum_cs']}}</td>
                                <td>{{$value['sum_hattrick']}}</td>
                            </tr>
                        @endforeach
                    @else

                        <tr>
                            <td colspan="10">No record is yet available.</td>
                        </tr>
                    @endif

                            </table>
                        </div>
                    </div>


                    */ ?>


<div class="fixtures_block mt-5">
                  <ul class="nav nav-tabs d-flex" role="tablist">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#form" role="tab" aria-selected="true">Gameweeks</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#game_log" role="tab" aria-selected="false">Matches</a></li>
                  </ul>
                  <div class="tab-content border p-sm-4 p-2  mb-3">
                    <div class="tab-pane fade show active" id="form">



                        <div class="table-responsive table-mobile">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table states_table mb-0">
                                <tr>
                                    <th>GW</th>
                                    <!-- <th>PIT</th> -->
                                    <th>PTS</th>
                                    <th>R</th>
                                    <th>4</th>
                                    <th>6</th>
                                    <th>MDS</th>
                                    <th>WK</th>
                                    <th>CS</th>
                                    <th>HT</th>
                                </tr>
                    @if(!empty($top_player_by_gwk_point))
                        @foreach($top_player_by_gwk_point as $key => $value)
                            <tr>
                                <td>GW{{$value['gwk']}}</td>
                              <!--   <td>674</td> -->
                                <td>{{$value['fp']}}</td>
                                <td>{{$value['sum_rs']}}</td>
                                <td>{{$value['sum_fours']}}</td>
                                <td>{{$value['sum_sixes']}}</td>
                                <!-- <td></td> -->
                                <td>{{$value['sum_mdns']}}</td>
                                <!-- <td>6</td> -->
                                <td>{{$value['sum_wks']}}</td>
                                <td>{{$value['sum_cs']}}</td>
                                <td>{{$value['sum_hattrick']}}</td>
                            </tr>
                        @endforeach
                    @else

                        <tr>
                            <td colspan="10">No record is yet available.</td>
                        </tr>
                    @endif

                            </table>
                        </div>
              



                    </div>
                    <div class="tab-pane fade" id="game_log">
                      



<div class="table-responsive table-mobile">
                        <table  id="example_matches"  width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                          <thead>
                            <tr>
                              <th scope="col">Team</th>
                              <th scope="col" class="check_hide">Grade</th>
                              <th scope="col">Date</th>
                             <!--  <th scope="col">Type</th> -->
                              <th scope="col">Pts</th>
                              <th scope="col">Stats</th>
                              <th scope="col">Match</th>
                            </tr>
                          </thead>
                          <tbody>

                        @foreach($temp_fs as $key => $value)
                        @if(!empty($value->fixture->teamdata) && !empty($value->fixture->start_date))
                        <tr>
                            <td>{{  $value->fixture->teamdata->name }}</td>
                            <td class="check_hide">{{  $value->fixture->teamdata->get_grade->grade }}</td>
                            
                            <td>{{ Carbon::parse($value->fixture->start_date)->format('d.m.y') }}</td>
                         
                            <td>{{$value->fantasy_points}}</td>
                            <td><a href="javascript:void(0)"  class="more_link border ml-auto"  data-toggle="modal" data-target="#myModal{{$value->id}}">View</a></td>
                            <td><a href="{{url('match-scorecards/'.$value->fixture->id)}}"    class="more_link border ml-auto">Scorecard</a></td>
                        </tr>
                        @endif
                        @endforeach
                          </tbody>
                        </table>
                      </div>




                    </div>
                  </div>
                </div>


                    
                    

    <!-- start match wise table and graph -->



<div class="fixtures_block mt-5">
    <h4 class="status_title mb-3">Recent Performances </h4>

<div class="table-responsive table-mobile">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                          <thead>
                            <tr>
                              <th scope="col">Team</th>
                              <th scope="col">R</th>
                              <th scope="col">4</th>
                              <th scope="col">6</th>
                              <th scope="col">MDS</th>
                              <th scope="col">WK</th>
                              <th scope="col">FP</th>
                              <th scope="col">FV</th>
                            </tr>
                          </thead>
                          <tbody class="player_last_5">
                          </tbody>
                        </table>
                      </div>


</div>




<div class="fixtures_block mt-5">
    <h4 class="status_title mb-3">Recent Performances Graph</h4>
 <div id="container" style="min-width: 260px; height: 400px; margin: 0 auto"></div>
</div>
<div class="legand_sec1 p-4 ml-n4 mr-n4 mb-n4">
    <label class="feilds_name">Legend</label>
    <div class="table-responsive">
    <table class="legend_data_table" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td><label for="" class="feilds_name">GW: </label>&nbsp;Gameweeks</td>
            <td><label for="" class="feilds_name">PTS: </label>&nbsp;Points</td>
            <td><label for="" class="feilds_name">R: </label>&nbsp;Runs</td>
            <td><label for="" class="feilds_name">4: </label>&nbsp;Fours</td>
            <td><label for="" class="feilds_name">6: </label>&nbsp;Sixes</td>
            <td><label for="" class="feilds_name">MDS: </label>&nbsp;Maidens</td>
            <td><label for="" class="feilds_name">WK:</label>&nbsp;Wickets</td>
            <td><label for="" class="feilds_name">CS:</label>&nbsp;Catches </td>
            <td><label for="" class="feilds_name">HT:</label>&nbsp;Hat-Trick</td>
        </tr>
    </table>
</div>
</div>












    <!-- end match wise table and graph -->


				</div>
			</div>
		</section>
	</div>
</div>



@foreach($temp_fs as $key => $value)
@if(!empty($value->fixture->teamdata) && !empty($value->fixture->start_date))
  <!-- The Modal -->
  <div class="modal fade" id="myModal{{$value->id}}">
    <div class="modal-dialog modal-lg">
      <div class="modal-content match_view_popup">      
        <!-- Modal Header -->
        <div class="modal-header border-bottom-0">
          <div>
              <b>Name: </b>{{!empty($player_data->full_name) ? $player_data->full_name : ''}}<br>
             <b>Team: </b>{{$value->fixture->teamdata->name}}<br>
              <b>Date: </b>{{ Carbon::parse($value->fixture->start_date)->format('d.m.y') }}
             <!--  <h4 class="modal-title">{{!empty($player_data->full_name) ? $player_data->full_name : ''}}</h4> -->
          </div>
          <div class="ml-auto">
              <img class="match_view_popup_logo mt-n3" width="200" src="{{asset('img/playercardlogo.png')}}" alt="MyClubTap">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>          
        </div>
        
        <!-- Modal body -->
        <div class="modal-body p-0">
            <div class="table-responsive">
                <table class="table table-hover mb-0">
                    <tbody>
                      <tr>
                        <th>Runs</th>
                        <td>{{$value->rs}}</td>
                      </tr>
                      <tr>
                        <th>4s</th>
                        <td>{{$value->fours}}</td>
                      </tr>
                      <tr>
                        <th>6s</th>
                        <td>{{$value->sixes}}</td>
                      </tr>
                      <tr>
                        <th>Overs</th>
                        <td>{{$value->overs}}</td>
                      </tr>
                      <tr>
                        <th>Maidens</th>
                        <td>{{$value->mdns}}</td>
                      </tr>
                        <tr>
                        <th>Runs Given</th>
                        <td>{{$value->run}}</td>
                      </tr>
                      <tr>
                        <th>Wickets</th>
                        <td>{{$value->wks}}</td>
                      </tr>

                      <tr>
                        <th>Catches</th>
                        <td>{{$value->cs}}</td>
                      </tr>
                      <tr>
                        <th>Catches WK</th>
                        <td>{{$value->cwks}}</td>
                      </tr>
                      <tr>
                        <th>Stumpings</th>
                        <td>{{$value->sts}}</td>
                      </tr>
                      <tr>
                        <th>RO Direct</th>
                        <td>{{$value->rods}}</td>
                      </tr>
                      <tr>
                        <th>RO Assist</th>
                        <td>{{$value->roas}}</td>
                      </tr>
                      <tr>
                        <th>Duck</th>
                        <td>{{$value->dks}}</td>
                      </tr>
                      <tr>
                        <th>Hat-Trick</th>
                        <td>{{$value->hattrick}}</td>
                      </tr>

                      <tr>
                        <th>Fantasy Points</th>
                        <td>{{$value->fantasy_points}}</td>
                      </tr>
                      <tr>
                        <th>Value Change</th>
                        <td>{{!empty($player_data->svalue) ? '$'.$player_data->svalue.'m' : '$0.00m'}}</td>
                      </tr>

                    </tbody>
                  </table>
            </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
@endif
@endforeach






<script type="text/javascript">


 $(document).ready(function() {

  var chartt = Highcharts.chart('container', {
      chart: {
          type: 'column'
      },
      title: {
          text: ''
      },
      subtitle: {
          text: ''
      },
      xAxis: {
          categories: [
      
          ],
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Fantasy Points'
          }
      },
      // tooltip: {
      //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      //         '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
      //     footerFormat: '</table>',
      //     shared: true,
      //     useHTML: true
      // },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [{
          name: '',
          data: []
  
      }, 
      // {
      //     name: 'New York',
      //     data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
  
      // }, {
      //     name: 'London',
      //     data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
  
      // }, {
      //     name: 'Berlin',
      //     data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
  
      // }
      ]
  });
  
  
  

  

  
     var player_id =  '{{$player_data->id}}';
  
      $.ajax({
        url: '{{url("get-player-data")}}',
        type: 'POST',
        data: {_token: '{{ csrf_token() }}', player_id: player_id},
        dataType: 'HTML',
        success: function (data) { 
          var obj = $.parseJSON(data);
          // console.log(obj); 
  
          // console.log(obj.player_name); 
          // $('#exampleModal .player_name').text(obj.player_name);
          // $('#exampleModal .playr_club_name').text(obj.playr_club_name);
          // $('#exampleModal .playr_game_name').text(obj.playr_game_name);
          // $('#exampleModal .playr_bat_style').text(obj.playr_bat_style);
          // $('#exampleModal .playr_bowl_style').text(obj.playr_bowl_style);
  
          var imgSource = "{{WEBSITE_IMG_URL}}";
  
          // if(obj.playerPosition == "BAT"){ 
          //   $('#exampleModal .img_position').html('<a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Batsman"><img width="25px" height="25px" id="theImg" src='+imgSource+"bat-icon.png"+' /></a>');
          // }else if(obj.playerPosition == "BOWL"){ 
          //    $('#exampleModal .img_position').html('<a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Bowler"><img width="25px" height="25px" id="theImg" src='+imgSource+"ball-icon.png"+' /></a>');
          // }else if(obj.playerPosition == "AR"){ 
          //    $('#exampleModal .img_position').html('<a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="All-Rounder"><img width="25px" height="25px" id="theImg" src='+imgSource+"bat-ball-icon.png"+' /></a>');
          // }else if(obj.playerPosition == "WK"){ 
          //    $('#exampleModal .img_position').html('<a href="javascript:void();" data-html="true" data-toggle="tooltip" title="" data-original-title="Wicket-Keeper"><img width="25px" height="25px" id="theImg" src='+imgSource+"gloves-icon.png"+' /></a>');
          // }
          // $('#exampleModal .playerPosition').text(obj.playerPosition);
          // $('#exampleModal .player_profile_url').attr('href', '{{url("player-profile/")}}/'+player_id);
          // $('.club_mode').text(obj.club_mode);
          // // $('#exampleModal .club_name').text('Western Australia');
          // if(obj.player_image)
          //   $('#exampleModal img.player_image').attr('src', '{{asset("uploads/player")}}/'+obj.player_image);
          // $('#exampleModal .player_fps').text(obj.fantasy_points);
          // $('#exampleModal .player_value').text('$'+obj.player_price+'m');
  
          // console.log(obj); 
          var table_str = ''; 
          obj.last_5_record.forEach(function (item, index) {
            table_str += '<tr>'
            
            table_str += '<td>'+(item.team_name ? item.team_name : '--')+'</td>'; 
            table_str += '<td>'+(item.runs ? item.runs : 0)+'</td>'; 
            table_str += '<td>'+(item.four ? item.four : 0)+'</td>'; 
            table_str += '<td>'+(item.six ? item.six : 0)+'</td>'; 
            table_str += '<td>'+(item.maiden ? item.maiden : 0)+'</td>'; 
            table_str += '<td>'+(item.wks ? item.wks : 0)+'</td>'; 
            table_str += '<td>'+(item.fp ? item.fp : 0)+'</td>'; 
            table_str += '<td>$'+(item.fv ? item.fv : 0)+'m</td>'; 
            table_str += '</tr>'
          });
          if(!table_str)  table_str +='<tr><td align="center" colspan="8">No record is yet available.</td></tr>' 
          // $('#exampleModal .selected_by').text(obj.selected_by); 
          $('.player_last_5').html(table_str); 
  // player_last_5
  obj.last_10_record.fx_month_fp = obj.last_10_record.fx_month_fp.map( Number );
  chartt.series[0].setName(obj.player_name,true);
   chartt.series[0].setData(obj.last_10_record.fx_month_fp,true);
    chartt.xAxis[0].setCategories(obj.last_10_record.fx_month);
  
  
  
        }
     }); 
  

 $('#example_matches').DataTable();

  
}); 


$('body').tooltip({
                  selector: '[data-toggle="tooltip"]'
                  });
</script>



<style type="text/css">
   #example_matches_wrapper #example_matches_filter,#example_matches_wrapper  #example_matches_length,#example_matches_wrapper  #example_matches_info { display: none; }
   #example_matches_filter { text-align: right; }
 
   .d_none{ display: none; }
   table.dataTable thead .sorting:before, table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_desc:after { display: none; }
</style>
@stop
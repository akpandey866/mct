@extends('front.layouts.default')
@section('content')
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end pb-3" style="background-image: url({{asset('img/inner_banner.png')}});">
        <div class="container py-3 py-lg-4">
            <h2 class="block_title"><small>
                {{ !empty($result->name) ?  $result->name :'' }}
            </small>MyClubtap</h2>
            <!--<a href="" class="btn py-2 px-5">Join Now</a>-->
        </div>
    </section>
    
    <section class="chooseus_block pb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12 pt-4 pt-md-0">
                    <h2 class="block_title my-3"></h2>
                    @if(!empty($result->body))
                        {!! $result->body !!}
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>
@include('front.elements.subscribe_pop_up')
@stop
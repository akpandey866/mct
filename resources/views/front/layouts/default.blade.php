<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Required meta tags -->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{{Config::get("Site.title")}}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <?php 
        $segment2   =   Request::segment(2); //admin
        $referQueryString = Request::query('source');
       // echo !empty($referQueryString) ? $referQueryString :'';
        ?>
    <meta property="fb:app_id" content="2302000619822671" />
    <meta property="og:url" content="https://myclubtap.com/user-sharer/{{$segment2}}" />
    <meta property="og:type" content="website" />
    @if(isset($referQueryString))
        <meta property="og:title" content="Here is my referral code.For use please visit https://myclubtap.com/signupasauser?refercode={{$segment2}}" />
    @else
        <meta property="og:title" content="MyClubtap" />
    @endif

    <meta property="og:description" content="Here is my referral code.For use please visit https://myclubtap.com/signupasauser?refercode={{$segment2}}" />
    <meta property="og:image" content="https://myclubtap.com/img/my_club_tap_icon.png" />
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.ico')}}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/developer.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media.css') }}">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
    <script type="text/javascript" src="{{ asset('js/admin/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/owl.carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/toastr.min.js') }}"></script>
</head>
<body>
@include('front.elements.header')

 <!-- Main Container Start -->
<aside class="right-side"> 
    @if(Session::has('error'))
        <script type="text/javascript"> 
            $(document).ready(function(e){
                showMessage("{{{ Session::get('error') }}}",'error');
            });
        </script>
    @endif
    
    @if(Session::has('success'))
        <script type="text/javascript"> 
            $(document).ready(function(e){
                showMessage("{{{ Session::get('success') }}}",'success');
            });
        </script>
    @endif

    @if(Session::has('flash_notice'))
        <script type="text/javascript"> 
            $(document).ready(function(e){
                showMessage("{{{ Session::get('flash_notice') }}}",'success');
            });
        </script>
    @endif

	@yield('content')
  </aside>
@include('front.elements.footer')
<div id="loader_img"><center><img src="{{WEBSITE_IMG_URL}}loading.gif"></center></div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="{{ asset('css/admin/fancybox/jquery.fancybox.js') }}"></script>
<link href="{{ asset('css/admin/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<script type="text/javascript">
function showMessage(message,type) { 
    toastr.remove()
    if (type == 'success') {
        toastr.success(message);
    } else if (type == 'error') {
        toastr.error(message);
    } else if (type == 'warning') {
        toastr.warning(message);
    } else if (type == 'info') {
        toastr.info(message);
    }
}
/*  */
window.helpShelfSettings = {
"siteKey": "2mLDeV4h",
"userId": "",
"userEmail": "",
"userFullName": "",
"userAttributes": {
// Add custom user attributes here
},
"companyAttributes": {
// Add custom company attributes here
}
};
(function() {var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
po.src = "https://s3.amazonaws.com/helpshelf-production/gen/loader/2mLDeV4h.min.js";
po.onload = po.onreadystatechange = function() {var rs = this.readyState; if (rs && rs != "complete" && rs != "loaded") return;HelpShelfLoader = new HelpShelfLoaderClass(); HelpShelfLoader.identify(window.helpShelfSettings);};
var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);})();
</script>
<script type="text/javascript" src="{{ asset('js/custom.js')}}"></script>
</body>

</html>

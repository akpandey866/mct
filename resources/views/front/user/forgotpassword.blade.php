@extends('front.layouts.default')
@section('content')
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end" style="background-image: url(img/inner_banner.png);">
    </section>
    <section class="signin_signup_block py-5">
        <div class="container">
            <h2 class="form_title mb-3">Forgot Password</h2>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                    {{ Form::open(['role' => 'form','url' =>  route("Home.forgotpassword"),'id'=> 'forget_form','class' => 'login_register_block pb-4 max-480', 'files' => true]) }} 
                        <div class="form-group">
                            {{ Form::email("forgot_email",'', ['class' => 'form-control small', 'placeholder' => 'Email']) }}
                            <span class="help-inline"></span>
                        </div>
                        <input class="btn btn-primary" type="button" onclick="forget_password();" value="{{trans('Submit')}}">
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>    
</div>
<script type="text/javascript">
function forget_password() {
    var formData  = $('#forget_form')[0];
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    $.ajax({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        url: '{{ route("Home.forgotpassword") }}',
        type:'post',
        //data: $('#signup_form').serialize(),
        data: new FormData(formData), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,
        success: function(r){  
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
                showMessage(data['message'],"success");
                window.location.href     =  "{{ URL('/') }}";
            }else if(data['success'] == 2) {
                    document.getElementById("forget_form").reset();
                    showMessage(data['message'],"error");
            }else { 
                $.each(data['errors'],function(index,html){ 
                    $("input[name = "+index+"]").next().addClass('error');
                    $("input[name = "+index+"]").next().html(html);
                });
            }
            $('#loader_img').hide();
        }
    });
}

 $('#forget_form').each(function() {
    $(this).find('input').keypress(function(e) {
       if(e.which == 10 || e.which == 13) {
            forget_password();
            return false;
        }
    });
});
</script>
@stop
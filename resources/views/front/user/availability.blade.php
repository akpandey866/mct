@extends('front.layouts.default')
@section('content')
<div class="full_container">
    <div class="body_section">
        <!-- top_header element -->
        @include('front.elements.common_header')
        <section class="status_tab_block mb-5">
            <div class="container mobile_cont">
                <!-- navbar element -->
                @include('front.elements.navbar')
                <div class="tab_content_sec border p-0 p-md-4">
                    <div class="d-block d-md-flex align-items-center">
                       <!--  <h4 class="status_title mr-auto">Player Availability </h4> -->
                        {{ Form::open(['role' => 'form','url' => Route('User.availability'),'class' => 'top_search_block statistics_form position-relative mws-form ml-0 ml-md-auto',"method"=>"get"]) }}
                        <div class="row">
                            <div class="col-12 col-sm-6 py-2 ml-auto">
                                <button class="input_search" type="submit"><img src="{{asset('img/search.svg')}}" alt="search"></button>
                                {{ Form::text('search',((isset($search)) ? $search : ''), ['class' => 'form-control','placeholder'=>'Search']) }}
                            </div>
                        </div>
                        {{ Form::close() }}

                    </div>
                    <div class="top_trades_block mt-4">
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="TradesIn" role="tabpanel">
                                <div class="table-responsive table-mobile">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                        <tr>
                                            <th>Unavailable
                                            </th>
                                            <!--  <th>Position <span class="float-right"><img style="width: .71rem;" src="img/sort_icon.svg" alt="sort"></span></th> -->
                                            <th> {{
                                                link_to_route(
                                                    "User.availability",
                                                    trans('Reason'),
                                                    array(
                                                        'sortBy' => 'reason',
                                                        'order' => ($sortBy == 'reason' && $order == 'desc') ? 'asc' : 'desc',
                                                        $query_string
                                                    ),
                                                   array('class' => (($sortBy == 'reason' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'reason' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                                )
                                            }}
                                            </th>
                                            <th>{{
                                                link_to_route(
                                                    "User.availability",
                                                    trans('From'),
                                                    array(
                                                        'sortBy' => 'date_from',
                                                        'order' => ($sortBy == 'date_from' && $order == 'desc') ? 'asc' : 'desc',
                                                        $query_string
                                                    ),
                                                   array('class' => (($sortBy == 'date_from' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'date_from' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                                )
                                            }}
                                            </th>
                                            <th>{{
                                                link_to_route(
                                                    "User.availability",
                                                    trans('Till'),
                                                    array(
                                                        'sortBy' => 'date_till',
                                                        'order' => ($sortBy == 'date_till' && $order == 'desc') ? 'asc' : 'desc',
                                                        $query_string
                                                    ),
                                                   array('class' => (($sortBy == 'date_till' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'date_till' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                                )
                                            }}
                                            </th>
                                        </tr>
                                        <?php   
                                    if(count($data) > 0){
                                     foreach($data as $key => $val){  ?>
                                        <tr>
                                            <!-- <td>{{$val->player_data->full_name}}</td> -->
                                            <td>
                                <a href="{{url('player-profile/'.$val->player_data->id)}}">
                                {{!empty($val->player_data->first_name) ? ucfirst(substr($val->player_data->first_name, 0, 2).'. ') : ''}}
                                {{!empty($val->player_data->last_name) ? $val->player_data->last_name : ''}}
                                </a>             
                                                
                                            </td>
                                            <td>{{$val->reason}}</td>
                                            <td>{{ date('d.m.y' ,strtotime($val->date_from)) }}</td>
                                            <td>{{ date('d.m.y' ,strtotime($val->date_till)) }}</td>
                                        </tr>

                                        <?php }
                                     }else{ ?>
                                        <tr>
                                            <td colspan="4">{{ trans("No record is yet available.") }}</td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="TradesOut" role="tabpanel">2</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="favorite_players_block mb-5">
                <a class="my_team_banner mt-1" href=""><img class="img-fluid" src="img/play_now_banner.jpg" alt="my_team_banner"></a>
            </div>
        </div>
    </div>
</div>
@stop
@extends('front.layouts.default')
@section('content')
<div class="body_section">
    <section class="top_banner_block inner_top_banner d-flex align-items-end" style="background-image: url(img/inner_banner.png);">
    </section>
    <section class="signin_signup_block py-5">
        <div class="container">
            <h2 class="form_title mb-3">Reset Password</h2>
            {{ Form::open(['role' => 'form','url' =>  route("Home.save_reset_password"),'id'=>'reset_password','class' => 'max', 'files' => true]) }} 
            {{ Form::hidden('validate_string', $validateString) }}
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                        <div class="form-group">
                            {{ Form::password('password', ['class' => 'form-control', 'placeholder'=>trans('New Password')]) }}
                            <span class="help-inline"></span>
                        </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                        <div class="form-group">
                            {{ Form::password('confirm_password', ['class' => 'form-control', 'placeholder'=>trans('Confirm Password')]) }}
                            <span class="help-inline"></span>
                        </div>
                </div>
            </div>
            <input class="btn btn-primary" type="button" onclick="reset_password();" value="{{trans('Submit')}}">
            {{ Form::close() }}
        </div>
    </section>    
</div>
<script type="text/javascript">

function reset_password() {
    var formData  = $('#reset_password')[0];
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
    $.ajax({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        url: '{{ route("Home.save_reset_password") }}',
        type:'post',
        //data: $('#signup_form').serialize(),
        data: new FormData(formData), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
                window.location.href     =  "{{ URL('login') }}";
                showMessage(data['message'],"success");
            }else if(data['success'] == 2) {
                    document.getElementById("reset_password").reset();
                    showMessage(data['message'],"error");
              }else {
                $.each(data['errors'],function(index,html){
                    if($.trim(index) == "country") {
                        $("#country").addClass('error');
                        $("#country").html(html);
                    }else {
                        $("input[name = "+index+"]").next().addClass('error');
                        $("input[name = "+index+"]").next().html(html);
                    }
                });
            }
            $('#loader_img').hide();
        }
    });
}

 $('#reset_password').each(function() {
    $(this).find('input').keypress(function(e) {
       if(e.which == 10 || e.which == 13) {
            reset_password();
            return false;
        }
    });
});
</script>
@stop
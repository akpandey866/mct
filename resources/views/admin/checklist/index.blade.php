@extends('admin.layouts.default')
@section('content')

<section class="content-header">
	<h1>
	  {{ trans("Checklist") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Checklist") }}</li>
	</ol>
</section>
<section class="content"> 
	
	<div class="row">
		{{ Form::open(['role' => 'form','url' => 'admin/checklist','class' => 'mws-form',"method"=>"get"]) }}
		{{ Form::hidden('display') }}	
		<div class="col-md-2 col-sm-2">
			<div class="form-group ">  
				{{ Form::text('title',((isset($searchVariable['title'])) ? $searchVariable['title'] : ''), ['class' => 'form-control','placeholder'=>'Title']) }}
			</div>
		</div>	
		<div class="col-md-2 col-sm-2">
			<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
			<a href="{{URL::to('admin/checklist')}}"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> {{ trans("Reset") }}</a>
		</div>
		{{ Form::close() }}
		<div class="col-md-8 col-sm-8">
			<div class="form-group">  
				<a href="{{URL::to('admin/checklist/add-checklist')}}" class="btn btn-success btn-small  pull-right" style="margin:0;">{{ trans("Add New Checklist") }} </a>
			</div>
		</div>
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">
							{{
								link_to_route(
									"checklist.index",
									trans("Title"),
									array(
										'sortBy' => 'title',
										'order' => ($sortBy == 'title' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'title' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'title' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							Description
						</th>
						<th width="20%">
							{{
								link_to_route(
									"checklist.index",
									trans("Status"),
									array(
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
									"player.index",
									trans("Created On"),
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							Action
						</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)  
						<tr class="items-inner">
							<td> 
								{{ $record->title }} 
							</td>
							<td>
								{{ strip_tags(Str::limit($record->description,100))}}
							</td>
							<td>
								@if($record->is_active	==1)
									<span class="label label-success" >{{ trans("Activated") }}</span>
								@else
									<span class="label label-warning" >{{ trans("Deactivated") }}</span>
								@endif
							</td>
							<td>
								{{ date(config::get("Reading.date_format"),strtotime($record->created_at))}}
							</td>
							<td data-th='Action'>
								<a title="{{ trans('Edit') }}" href="{{URL::to('admin/checklist/edit-checklist/'.$record->id)}}" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>
								@if($record->is_active == 1)
									<a  title="Click To Deactivate" href="{{URL::to('admin/checklist/update-status/'.$record->id.'/0')}}" class="btn btn-success btn-small status_any_item"><span class="fa fa-check"></span>
									</a>
								@else
									<a title="Click To Activate" href="{{URL::to('admin/checklist/update-status/'.$record->id.'/1')}}" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
									</a> 
								@endif							
								<!-- <a title="{{ trans('Delete') }}" href="{{ URL::to('admin/checklist/delete-checklist/'.$record->id) }}"  class="delete_any_item btn btn-danger">
									<i class="fa fa-trash-o"></i>
								</a> -->
								<!-- <a title="{{ trans('View') }}" href="{{URL::to('admin/checklist/view-checklist/'.$record->id)}}" class="btn btn-primary">
									<i class="fa fa-eye"></i>
								</a> -->
							</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="5" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
@stop
@extends('admin.layouts.default')
@section('content')
<script type="text/javascript" src="{{asset('js/admin/ckeditor/ckeditor.js')}}"></script>
<section class="content-header">
	<h1>
		{{ trans("Edit Checklist") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/checklist')}}">{{ trans("Checklist") }}</a></li>
		<li class="active">{{ trans("Edit Checklist") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/checklist/edit-checklist','class' => 'mws-form','files'=>'true']) }}
	{{ Form::hidden('id',$details->id) }}
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('title')) ? 'has-error' : ''; ?>">
				{{ Form::label('title', trans("Title"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('title',isset($details->title) ? $details->title :'', ['class' => 'form-control ','placeholder'=>'Title','id'=>'title']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('title'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('description')) ? 'has-error' : ''; ?>">
				{{ Form::label('description', trans("Description"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::textarea('description',isset($details->description) ? $details->description :'', ['class' => 'form-control ','placeholder'=>'Description','id'=>'description']) }}
					<script type="text/javascript">
					/* For CKEDITOR */
						CKEDITOR.replace( <?php echo 'description'; ?>,
						{
							height: 250,
							width: 507,
							filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
							filebrowserImageWindowWidth : '640',
							filebrowserImageWindowHeight : '480',
							enterMode : CKEDITOR.ENTER_BR
						});
							
					</script>
					<div class="error-message help-inline">
						<?php echo $errors->first('description'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Update') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/checklist/edit-checklist/'.$details->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/checklist')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
@stop

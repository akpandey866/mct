@extends('admin.layouts.default')
@section('content') 
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<script src="{{ asset('js/admin/bootstrap-timepicker.min.js') }}"></script> 
 <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-timepicker.min.css') }}">
<section class="content-header">
	<h1>
		{{ trans("Add New Fixture") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/fixture')}}">{{ trans("Fixture") }}</a></li>
		<li class="active">{{ trans("Add New Fixture") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/fixture/add-fixture','class' => 'mws-form','files'=>'true','id'=>'add_fixture']) }}
	
	@if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
		<div class="row">
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('mode')) ? 'has-error' : ''; ?>">
					{{ Form::label('mode', trans("Mode").' *', ['class' => 'mws-form-label'])}}
					<div class="mws-form-item">
						{{ Form::select('mode', [null => 'Please Select Mode']+Config::get('home_club'),'',['id' => 'mode','class'=>'form-control']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('mode'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
					{{ Form::label('club', trans("Club").' *', ['class' => 'mws-form-label'])}}
					<div class="mws-form-item put_club_html">
					 {{ Form::select('club', [null => 'Please Select Club'],'',['id' => 'get_club_name','class'=>'form-control']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif

	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('grade')) ? 'has-error' : ''; ?>">
				{{ Form::label('grade', trans("Grade").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item put_grade_html">
					{{ Form::select('grade',[null => 'Please Select Grade'] +$gradeName,'',['id' => 'grade','class'=>'form-control'])}}
					<div class="error-message help-inline">
						<?php echo $errors->first('grade'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			@if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
				<div class="form-group <?php echo ($errors->first('team')) ? 'has-error' : ''; ?>">
					{{ Form::label('team', trans("Team").' *', ['class' => 'mws-form-label'])}}
					<div class="mws-form-item put_team_html">
						{{ Form::select('team',[null => 'Please Select Team'],'',['id' => 'team','class'=>'form-control'])}}
						<div class="error-message help-inline">
							<?php echo $errors->first('team'); ?>
						</div>
					</div>
				</div>
			@else
				<div class="form-group <?php echo ($errors->first('team')) ? 'has-error' : ''; ?>">
					{{ Form::label('team', trans("Team").' *', ['class' => 'mws-form-label'])}}
					<div class="mws-form-item put_team_html">
						{{ Form::select('team',[null => 'Please Select Team'],'',['id' => 'team','class'=>'form-control'])}}
						<div class="error-message help-inline">
							<?php echo $errors->first('team'); ?>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('match_type')) ? 'has-error' : ''; ?>">
				{{ Form::label('match_type', trans("Match Type").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select('match_type', [null => 'Please Select Match']+$matchTypeList,'',['id' => 'match_type','class'=>'form-control']) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('match_type'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('start_date')) ? 'has-error' : ''; ?>">
				{{ Form::label('start_date', trans("Match Start Date").' *', ['class' => 'mws-form-label'])}}
				<div class="input-group date">
				  <div class="input-group-addon">
				    <i class="fa fa-calendar"></i>
				  </div>
				 {{ Form::text('start_date','', ['class' => 'form-control ','placeholder'=>'Match Start Date','id'=>'start_date']) }}
				</div>
				<div class="error-message help-inline start_date_error">
					<?php echo $errors->first('start_date'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('position')) ? 'has-error' : ''; ?>">
				{{ Form::label('end_date', trans("Match End Date").' *', ['class' => 'mws-form-label'])}}
				<div class="input-group date">
				  <div class="input-group-addon">
				    <i class="fa fa-calendar"></i>
				  </div>
				 {{ Form::text('end_date','', ['class' => 'form-control ','placeholder'=>'Match End Date','id'=>'end_date']) }}
				</div>
				<div class="error-message help-inline end_date_error">
					<?php echo $errors->first('end_date'); ?>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('start_time')) ? 'has-error' : ''; ?>">
				{{ Form::label('start_time', trans("Match Start Time").' *', ['class' => 'mws-form-label'])}}
				<div class="input-group">
					<div class="input-group-addon">
			          <i class="fa fa-clock-o"></i>
			        </div>
			        {{ Form::text('start_time','', ['class' => 'form-control timepicker','placeholder'=>'Match Start Time']) }}
			    </div>
			    <div class="error-message help-inline start_time_error">
					<?php echo $errors->first('start_time'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
	    	<div class="form-group <?php echo ($errors->first('end_time')) ? 'has-error' : ''; ?>">
				{{ Form::label('end_time', trans("Match End Time").' *', ['class' => 'mws-form-label'])}}
				<div class="input-group">
					<div class="input-group-addon">
			          <i class="fa fa-clock-o"></i>
			        </div>
			        {{ Form::text('end_time','', ['class' => 'form-control timepicker','placeholder'=>'Match End Time']) }}
			    </div>
			    <div class="error-message help-inline end_time_error">
					<?php echo $errors->first('end_time'); ?>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('opposition_club')) ? 'has-error' : ''; ?>">
				{{ Form::label('opposition_club', trans("Oppostion Club"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					<!-- {{ Form::select( 'opposition_club',[null => 'Please Select Oppostion Club']+$cludDetails,'',
						 ['id' => 'opposition_club','class'=>'form-control']) }} -->


{{ Form::text('opposition_club',isset($userDetails->opposition_club) ? $userDetails->opposition_club :'', ['class' => 'form-control ','placeholder'=>'Opposition Club','id'=>'opposition_club']) }}

						 
					<div class="error-message help-inline">
						<?php echo $errors->first('opposition_club'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		@if(Auth::guard('admin')->user()->user_role_id != SUPER_ADMIN_ROLE_ID)
			{{ Form::hidden('club',Auth::guard('admin')->user()->id)}}
		@endif
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('vanue')) ? 'has-error' : ''; ?>">
				{{ Form::label('vanue', trans("Venue"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('vanue','', ['class' => 'form-control ','placeholder'=>'Venue','id'=>'Venue']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('vanue'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="button" value="{{ trans('Save') }}" class="btn btn-danger" onclick="save_fixture();">
			<a href="{{URL::to('admin/fixture/add-fixture/')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/fixture/')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script type="text/javascript">
$("#grade").on('change',function(){
	var gradeVal = $(this).val();
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-grade-club-mode-listing') }}",
		'type':'post',
		data:{'id':gradeVal},
		success:function(response){
			$('.put_team_html').html(response);
			$('#loader_img').hide();
		}
	});
});
$("#mode").on('change',function(){
	$('#loader_img').show(); 
	var selectValue   =  $(this).val();
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-listing') }}",
		'type':'post',
		data:{'id':selectValue},
		success:function(response){
			$('.put_club_html').html(response);
			$('#loader_img').hide();
		}
	});
});

$(document).on('change',"#get_club_name",function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-team') }}",
		'type':'post',
		data:{'id':selectValue},
		success:function(response){
			$('.put_team_html').html(response);
			$('#loader_img').hide();
		}
	});
});

$(document).on('change',"#get_club_name",function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-grade') }}",
		'type':'post',
		data:{'id':selectValue},
		success:function(response){
			$('.put_grade_html').html(response);
			$('#loader_img').hide();
		}
	});
});


function save_fixture(){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
	var form = $('#add_fixture')[0];
	var formData = new FormData(form);
    $.ajax({
        url: '{{ route("Fixture.saveFixture") }}',
        type:'post',
        data: formData,
		processData: false,
		contentType: false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
				show_message(data['message'],"success");
                window.location.href   =  "{{ route('fixture.index') }}";
            }else {
                $.each(data['errors'],function(index,html){
                	if(index == "start_date"){
                	 	$(".start_date_error").addClass('error');
                	 	$(".start_date_error").html(html);
                	}if(index == "end_date"){
                		$(".end_date_error").addClass('error');
                		$(".end_date_error").html(html);
                	}if(index == "start_time"){
                		$(".start_time_error").addClass('error');
                		$(".start_time_error").html(html);
                	}if(index == "end_time"){
                		$(".end_time_error").addClass('error');
                		$(".end_time_error").html(html);
                	}
                    $("#"+index).next().addClass('error');
                    $("#"+index).next().html(html);
                });
                $('#loader_img').hide();
            }
            $('#loader_img').hide();
        }
    });
}

$('#add_fixture').each(function() {
	$(this).find('input').keypress(function(e) {
       if(e.which == 10 || e.which == 13) {
			save_fixture();
			return false;
        }
    });
});
$('#start_date').on('changeDate', function (ev) {
	var currentDate = $(this).val();
	var matchType = $("#match_type").val();
	if(matchType == 27){
		$("#end_date").val(currentDate);
	}
});
$(document).ready(function() {
	 $( "#start_date" ).datepicker({
		format 	: 'dd/mm/yyyy',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		autoclose   :true,
		onSelect	: function( selectedDate ){ $("#end_date").datepicker("option","minDate",selectedDate); }
	});
	$( "#end_date" ).datepicker({
		format 	: 'dd/mm/yyyy',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		autoclose   :true,
		onSelect	: function( selectedDate ){ $("#start_date").datepicker("option","maxDate",selectedDate); }
	});
	$('.timepicker').timepicker({
      showInputs: false,
      //minuteStep: 5,
    })
});
</script>
@stop

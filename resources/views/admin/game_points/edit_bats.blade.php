{{ Form::open(['role' => 'form','url' => 'admin/game-points/edit-bats-game-points/'.$id,'class' => 'mws-form','onsubmit'=>'return false;']) }}
	<div class="mws-form-inline">
		<div class="mws-form-row">
			<div class="mws-form-item">
			<div class="controls" style="margin-left:0px;margin-bottom:0px;">
				{{ Form::text('bats_wk_ar',!empty($result->bats_wk_ar) ? $result->bats_wk_ar :'', ['class' => 'small','style'=>"height:30px;width:200px;font-size:9pt;",'id'=>'edit_bats_msgstr']) }}
				<?php echo $errors->first('word'); ?><br/>
				<input type="submit" value="{{ trans('Save') }}" id="editgroupbats" class="btn btn-primary">
				<a id="cancel1" class="btn btn-primary" href="javascript:void(0);">{{ trans('Cancel') }}</a>
			</div>	
			</div>
		</div>
	</div>
{{ Form::close() }} 	

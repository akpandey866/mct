<select name="user_id" class="form-control verify_user">
	<option value="" selected="">Please Select Users</option>
	<?php foreach ($userList as $key => $value) {  ?>
		<option value="{{$key}}">{{$value}}</option>
	<?php } ?>
</select>
<div class="error-message help-inline">
	<?php echo $errors->first('user_id'); ?>
</div>
@extends('admin.layouts.default')
@section('content')

<section class="content-header">
	<h1>
	  {{ trans("Branding") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Branding") }}</li>
	</ol>
</section>
<section class="content"> 
	
	<div class="row">
		@if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
		{{ Form::open(['role' => 'form','url' => 'admin/admin-branding','class' => 'mws-form',"method"=>"get"]) }}
		{{ Form::hidden('display') }}	
			<div class="col-md-2 col-sm-12">
				<div class="form-group ">  
					{{ Form::select('mode',[null => 'Please Select mode'] +Config::get('home_club'),((isset($searchVariable['mode'])) ? $searchVariable['mode'] : ''),['id' => 'mode','class'=>'form-control choosen_selct'])}}
				</div>
			</div>	
			<div class="col-md-2 col-sm-12">
				<div class="form-group put_html">  
					{{ Form::select('club',[null => 'Please Select Club'],((isset($searchVariable['club'])) ? $searchVariable['club'] : ''),['id' => 'club','class'=>'form-control choosen_selct'])}}
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
				<a href="{{URL::to('admin/admin-branding')}}"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> {{ trans("Reset") }}</a>
			</div>
		{{ Form::close() }}
		<div class="col-md-5 col-sm-12">
			<div class="form-group">  
				<a href="{{URL::to('admin/admin-add-branding')}}" class="btn btn-success btn-small  pull-right" style="margin:0;">{{ trans("Add New Branding") }} </a>
			</div>
		</div>
		@endif
		
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">
							Logo
						</th>
						<th width="20%">
							{{
								link_to_route(
									"Common.adminbranding",
									trans("Name"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
									"Common.adminbranding",
									trans("url"),
									array(
										'sortBy' => 'webstie',
										'order' => ($sortBy == 'webstie' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'webstie' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'webstie' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
									"Common.adminbranding",
									trans("Created On"),
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)  
						<tr class="items-inner">
							<td>
								@if(File::exists(BRANDING_IMAGE_ROOT_PATH.$record->logo))
									<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo BRANDING_IMAGE_URL.$record->logo; ?>">
										<div class="usermgmt_image">
											<img  src="<?php echo WEBSITE_URL.'image.php?width=50x&height=50px&cropratio=1:1&image='.BRANDING_IMAGE_URL.'/'.$record->logo ?>">
										</div>
									</a>
								@endif
							</td>
							<td> {{ $record->name }} </td>
							<td> {{ $record->url }} </td>
							<td>
								{{ date(config::get("Reading.date_format"),strtotime($record->created_at))}}
							</td>
							<td data-th='Action'>
								<a title="{{ trans('Edit') }}" href="{{URL::to('admin/admin-edit-branding/'.$record->id)}}" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>						
								<!-- <a title="{{ trans('Delete') }}" href="{{ URL::to('admin/delete-branding/'.$record->id) }}"  class="delete_any_item btn btn-danger">
									<i class="fa fa-trash-o"></i>
								</a> -->
							</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="9" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
<script> 
$("#mode").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-listing') }}",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});
/*adter search club searhcing*/
var mode = "<?php  echo !empty($searchVariable['mode']) ? $searchVariable['mode']:'';  ?>";
var searchClubVar = "<?php  echo !empty($searchVariable['club']) ? $searchVariable['club']:'';  ?>";
if(mode !=''){ 
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-listing') }}",
		'type':'post',
		data:{'id':mode,'club_id':searchClubVar},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
}

</script>
@stop
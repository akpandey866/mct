@extends('admin.layouts.default')
@section('content')

<section class="content-header">
	<h1>
	  {{ trans("Lockout") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Lockout") }}</li>
	</ol>
</section>
<section class="content"> 
	
	<div class="row">
		{{ Form::open(['role' => 'form','url' => 'admin/admin-lockout','class' => 'mws-form',"method"=>"get"]) }}
		{{ Form::hidden('display') }}	
			<div class="col-md-2 col-sm-12">
				<div class="form-group ">  
					{{ Form::select('game_mode',[null => 'Please Select mode'] +Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''),['id' => 'mode','class'=>'form-control choosen_selct'])}}
				</div>
			</div>	
			<div class="col-md-2 col-sm-12">
				<div class="form-group put_html">  
					{{ Form::select('club',[null => 'Please Select Club'],((isset($searchVariable['club'])) ? $searchVariable['club'] : ''),['id' => 'club','class'=>'form-control choosen_selct'])}}
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
				<a href="{{URL::to('admin/admin-lockout')}}"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> {{ trans("Reset") }}</a>
			</div>
		{{ Form::close() }}
		<!-- <div class="col-md-5 col-sm-12">
			<div class="form-group">  
				<a href="{{URL::to('admin/add-admin-lockout')}}" class="btn btn-success btn-small  pull-right" style="margin:0;">{{ trans("Add New lockout") }} </a>
			</div>
		</div>	 -->	
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">
							{{
								link_to_route(
									"lockout.adminIndex",
									trans("Start Day"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
									"lockout.adminIndex",
									trans("End Day"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
									"lockout.adminIndex",
									trans("Start Time"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
									"lockout.adminIndex",
									trans("End Time"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)  
						<tr class="items-inner">
							<td> {{ $record->lockout_start_day }} </td>
							<td> {{ $record->lockout_end_day }} </td>
							<td> {{ $record->lockout_start_time }} </td>
							<td> {{ $record->lockout_end_time }} </td>
							<td data-th='Action'>
								<a title="{{ trans('Edit') }}" href="{{URL::to('admin/admin-edit-lockout/'.$record->id)}}" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>								
							</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="5" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
<script> 
$("#mode").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-listing') }}",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});
/*adter search club searhcing*/
var mode = "<?php  echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode']:'';  ?>";
var searchClubVar = "<?php  echo !empty($searchVariable['club']) ? $searchVariable['club']:'';  ?>";
if(mode !=''){ 
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-listing') }}",
		'type':'post',
		data:{'id':mode,'club_id':searchClubVar},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
}

</script>
@stop
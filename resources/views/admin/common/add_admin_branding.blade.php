@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
	  {{ trans("Branding") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Branding") }}</li>
	</ol>
</section>
<section class="content"> 
         <div class="activateBranding">
            {{ Form::open(['role' => 'form','url' => 'admin/admin-save-branding','class' => 'mws-form','files'=>'true','id' => 'add_branding']) }}

            <input type="hidden" name="brand_id" value="{{!empty($userDetails->id) ? $userDetails->id : 0}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group <?php echo ($errors->first('mode')) ? 'has-error' : ''; ?>">
                        {{ Form::label('mode', trans("Game Mode"), ['class' => 'mws-form-label'])}}
                        <div class="mws-form-item">
                              {{ Form::select('mode',[null => 'Please Select Mode'] +Config::get('home_club'),'',['id' => 'mode','class'=>'form-control'])}}
                            <div class="error-message help-inline">
                                <?php echo $errors->first('club'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group <?php echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
                        {{ Form::label('club', trans("Club"), ['class' => 'mws-form-label'])}}
                        <div class="mws-form-item put_club_html">
                              {{ Form::select('club',[null => 'Please Select Club'],'',['id' => 'get_player','class'=>'form-control'])}}
                            <div class="error-message help-inline">
                                <?php echo $errors->first('club'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
                                {{ Form::label('name',trans("Name").' *', ['class' => 'mws-form-label name']) }}
                                <div class="mws-form-item">
                                    {{ Form::text('name',isset($userDetails->name) ? $userDetails->name :'',['class' => 'form-control','required']) }}
                                    <div class="error-message help-inline">
                                        <?php echo $errors->first('name'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <?php echo ($errors->first('url')) ? 'has-error' : ''; ?>">
                                {{ Form::label('url',trans("Website URL").' *', ['class' => 'mws-form-label url']) }}
                                <div class="mws-form-item">
                                    {{ Form::text('url',isset($userDetails->url) ? $userDetails->url :'',['class' => 'form-control']) }}
                                    <div class="error-message help-inline">
                                        <?php echo $errors->first('url'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
                                <label for="image" class="mws-form-label image">Brand Logo *</label>
                                <div class="mws-form-item">
                                    {{ Form::file('logo', array('accept' => 'image/*')) }}<br>
                                    @if(!empty($userDetails->logo) && (File::exists(BRANDING_IMAGE_ROOT_PATH.$userDetails->logo))) 
                                        <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo BRANDING_IMAGE_URL.$userDetails->logo; ?>">
                                            <div class="usermgmt_image">
                                                <img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.BRANDING_IMAGE_URL.'/'.$userDetails->logo ?>">
                                            </div>
                                        </a>
                                    @endif
                                    <div class="error-message help-inline">
                                        <?php echo $errors->first('logo'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mws-button-row">
                        <div class="input" >
                           <button type="submit" class="btn btn-info">Save</button>
                        </div>
                    </div>
            {{ Form::close() }}
     </div> 
</section> 
<script type="text/javascript">
    $("#mode").on('change',function(){
        var selectValue   =  $(this).val();
        $('#loader_img').show(); 
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:"{{  url('admin/get-club-mode-listing') }}",
            'type':'post',
            data:{'id':selectValue},
            async : false,
            success:function(response){
                $('.put_club_html').html(response);
                $('#loader_img').hide();
            }
        });
    });
    $(document).on("change",'#get_club_name',function(){
        var selectValue   =  $(this).val();
        $('#loader_img').show(); 
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:"{{  url('admin/get-club-playerlist') }}",
            'type':'post',
            data:{'id':selectValue},
            async : false,
            success:function(response){
                $('.put_html').html(response);
                $('#loader_img').hide();
            }
        });
    });

</script>
@stop
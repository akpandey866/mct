@extends('admin.layouts.default')
@section('content')
<script>
	jQuery(document).ready(function(){
		$(".choosen_selct").chosen();
	});
</script>
<style>
.chosen-container-single .chosen-single{
	height:34px !important;
	padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>Pay & Activate</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Pay & Activate") }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
		{{ Form::open(['method' => 'get','role' => 'form','url' => 'admin/admin-pay-activate','class' => 'mws-form']) }}
		{{ Form::hidden('display') }}
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<div class="form-group ">  
					{{ Form::select('game_mode',[null => 'Please Select Mode'] + Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''), ['class' => 'form-control game_mode choosen_selct']) }}
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<div class="form-group get_game_name">  
					{{ Form::select('game_name',array(''=>trans('Select Game')),((isset($searchVariable['game_name'])) ? $searchVariable['game_name'] : ''), ['class' => 'form-control choosen_selct']) }}
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<div class="form-group ">  
					{{ Form::select('is_active',array(''=>trans('Select Status'),0=>'Inactive',1=>'Active'),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control choosen_selct']) }}
				</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href="{{URL::to('admin/admin-pay-activate')}}"  class="btn btn-primary"><i class='fa fa-refresh '></i> Reset</a>
			</div>
		{{ Form::close() }}
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="14%">
							{{
								link_to_route(
								"Common.adminGameFee",
								trans("Game Name"),
								array(
									'sortBy' => 'game_name',
									'order' => ($sortBy == 'game_name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'game_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'game_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">Platform Fees</th>
						<th width="14%">Player Fees</th>
						<th width="14%">
							{{
								link_to_route(
								"Common.adminGameFee",
								 trans("Branding Fees"),
								array(
									'sortBy' => 'branding_price',
									'order' => ($sortBy == 'branding_price' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'branding_price' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'branding_price' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							Total Fee
						</th>
						<th>
							{{
								link_to_route(
								"Common.adminGameFee",
								trans("Activated On"),
								array(
									'sortBy' => 'branding_activated_on',
									'order' => ($sortBy == 'branding_activated_on' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'branding_activated_on' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'branding_activated_on' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th> 
						<th>{{ trans("Action") }}</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					@if(!$result->isEmpty())
					@foreach($result as $record)
					<tr class="items-inner">
						<td data-th='{{ trans("game name") }}'>{{ $record->game_name }}</td>
						<td data-th='{{ trans("Platform Fee") }}'>
							<?php   
								$price = 0;
								if($record->game_mode == 1){
							      $price = SENIORFEE;
							    }elseif ($record->game_mode == 2) {
							      $price = JUNIORFEE;
							    }elseif ($record->game_mode == 3) {
							      $price = LEAGUEFEE;
							    }
							    echo !empty($price) ? $price : ''; 
							 ?>
						</td>
						<td data-th='{{ trans("Player Fee") }}'>
							<?php  
								$playerFee = 0;
								if(!empty(get_player_fee($record->id))){
									$playerFee =  get_player_fee($record->id); 
								}
								echo !empty($playerFee) ? $playerFee :'-'; 
								?>
							
							</td>
						<td data-th='{{ trans("Branding fee") }}'>
							<?php 
							$brandingPrice = 0;
							if(!empty($record->branding_price)){
								$brandingPrice = $record->branding_price;
							} ?>
							{{ !empty($brandingPrice) ? $brandingPrice :'-' }}</td>
						<td data-th='{{ trans("Total Fee") }}'>
							<?php $totalFee = $price + $playerFee + $brandingPrice;  echo $totalFee;?>	
						</td> 
						 <td data-th='{{ trans("activated on")}}'>
						@if(!empty($record->branding_activated_on) && $record->is_paid==1)
							{{ date(config::get("Reading.date_format"),strtotime($record->branding_activated_on))}}
						@else
							-
						@endif
						</td> 
						<td data-th='{{ trans("action") }}'>	
							<a title="Free Platform Fees" href="{{URL::to('admin/admin-pay-activate/admin-player-fee/'.$record->id)}}" class="btn btn-success">Player Fee</a>
							@if($record->is_game_activate == 1)
								<a  title="Click To Deactivate Game"  href="{{URL::to('admin/admin-activate-game/'.$record->id.'/0')}}"  class=" btn btn-success btn-small status_any_item"><span class="fa fa-check"></span> </a>
							@else
								<a title="Click To Activate Game" href="{{URL::to('admin/admin-activate-game/'.$record->id.'/1')}}"   class=" btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span></a>
							@endif	
						</td>
					</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="6" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div>
</section> 
<script type="text/javascript">
	var game_name = "<?php echo !empty($searchVariable['game_name']) ? $searchVariable['game_name'] :'' ?>";
	var game_mode = "<?php echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode'] :'' ?>";
	if(game_name !="" || game_mode !=""){
		get_game_name();

	}
	function get_game_name(){
		$.ajax({
		 	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:' {{{ "admin-get-game-name" }}} ',
			'type':'post',
			data:{'mode':game_mode,'game_name':game_name},
			success:function(response){ 
				$(".get_game_name").html(response);
			}
		});
	}
	$(".game_mode").on('change',function(){
		var mode = $(this).val();
		$('#loader_img').show();
		 $.ajax({
		 	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:' {{{ "admin-get-game-name" }}} ',
			'type':'post',
			data:{'mode':mode,'game_name':game_name},
			success:function(response){ 
				$(".get_game_name").html(response);
			}
		}); 
		$('#loader_img').hide();
	});
</script>
@stop

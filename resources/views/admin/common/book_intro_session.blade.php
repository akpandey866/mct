@extends('admin.layouts.default')
@section('content')
<style type="text/css">
.info_box_custom	{
	max-height: 200px;
	min-height: 200px;
    width: 100%;
    overflow-y: auto;
}
</style>
<div class="row pad" >
	<div class="col-md-12 col-sm-12 col-xs-12">
		<center>
			<div class="info-box">
				<div class="info-box-content">
					<!-- Calendly inline widget begin -->
					<div class="calendly-inline-widget" data-url="https://calendly.com/vickyatmyclubtap/myclubtapintroforgameadminsin30mins" style="min-width:320px;height:750px;"></div>
					<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
					<!-- Calendly inline widget end -->
				</div>
			</div>
		</center>
	</div>
</div>
@stop
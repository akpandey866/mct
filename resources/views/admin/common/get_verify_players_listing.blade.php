<select name="player_id" class="form-control">
	<option value="" selected="" >Please Select Player</option>
	<?php foreach ($playerlist as $key => $value) {  ?>
		<option value="{{$key}}" <?php echo (in_array($key, $playerIds) ? "disabled":"") ?> >{{$value}}</option>
	<?php } ?>
</select>
<div class="error-message help-inline">
	<?php echo $errors->first('player_id'); ?>
</div>
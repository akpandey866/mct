@extends('admin.layouts.default')
@section('content')

<section class="content-header">
	<h1>
		{{ trans("Purchase Additional Players") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Purchase Packs") }} </li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
		<div class="col-md-12 col-sm-8">
			<div class="form-group">  
				<a href="{{URL::to('admin/additional-player-logs')}}" class="btn btn-success btn-small  pull-right" style="margin:0;">{{ trans("Additional Player Logs") }} </a>
			</div>
		</div>
	</div>
	{{ Form::open(['role' => 'form','url' => 'admin/purchase-player-packs','class' => 'mws-form','files'=>'true']) }}

	<div class="box">
		<div class="box-body ">
	<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-6">	
			<div class="form-group ">  
				{{ Form::select('plan',$playerData,(isset($searchVariable['position'])) ? $searchVariable['position'] : '',['id' => 'position','class'=>'form-control']) 
				}}
				<div class="error-message help-inline">
					<?php echo $errors->first('plan'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input">
			<!-- <input type="submit" value="{{ trans('Pay Now') }}" class="btn btn-danger"> -->
			<button type="button" class="btn btn-info" onclick="payAmount()">Pay Now</button>
			<a href="{{URL::to('admin/purchase-player-packs')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	</div>
	</div>
	{{ Form::close() }}
</section>
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="panel-heading display-table" >
						<div class="row display-tr" >
							<h3 class="panel-title display-td" >Payment Details</h3>
							<div class="display-td" >                            
								<img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
							</div>
						</div>
					</div>
			</div>
			<div class="modal-body append_data"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
function payAmount(){
	$('#loader_img').show();
	var id = $("#position").val();
	var club = "<?php echo auth()->guard('admin')->user()->id; ?>";
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:' {{ route("Common.stripePayment") }} ',
		'type':'post',
		data:{'id':id,'club':club},
		success:function(response){ 
			$('.append_data').html(response);
			$('#myModal').modal('show');
			$('#loader_img').hide();
		}
	});
}
</script>
@stop

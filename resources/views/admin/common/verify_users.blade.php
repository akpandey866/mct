@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
	  {{ trans("Verify Users") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Verified Users") }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
			{{ Form::open(['role' => 'form','url' => 'admin/save-verify-users','class' => 'mws-form',"method"=>"post"]) }}			
			<div class="col-md-4 col-sm-3">
				<div class="form-group ">  
					<select name="user_id" class="form-control">
						<option value="" selected="" >Please Select Users</option>
						<?php foreach ($userList as $key => $value) {  ?>
							 <!-- <option value="{{$key}}" <?php echo (in_array($key, $playerIds) ? "disabled":"") ?> >{{$value}}</option> -->
							<option value="{{$key}}">{{$value}}</option>
						<?php } ?>
					</select>
					<div class="error-message help-inline">
						<?php echo $errors->first('user_id'); ?>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-3">
				<div class="form-group ">  
					<select name="player_id" class="form-control">
						<option value="" selected="" >Please Select Player</option>
						<?php foreach ($playerlist as $key => $value) {  ?>
							<option value="{{$key}}" <?php echo (in_array($key, $playerIds) ? "disabled":"") ?> >{{$value}}</option>
						<?php } ?>
					</select>
					<div class="error-message help-inline">
						<?php echo $errors->first('player_id'); ?>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-5">
				<div class="form-group">  
					<button class="btn btn-success">{{trans("Verify") }}</button>
				</div>
			</div>
			{{ Form::close() }}
	</div> 
	<div class="box">
		<div class="box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="25%">User Account</th>
						<th width="25%">Player Account</th>
						<th width="25%">On Date</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				@if(!$result->isEmpty())
				<?php $n=1; ?>
					@foreach($result as $key => $record)  
						<tr class="items-inner">
							<td>
								{{ $record->username  }}
							</td>
							<td>
								{{ $record->player_name }}
							</td>
							<td>
								{{ date(config::get("Reading.date_format"),strtotime($record->created_at))}}
							</td>
							<td data-th='Action'>

								<a title="{{ trans('Delete') }}" href="{{ URL::to('admin/delete-verify-users/'.$record->id) }}"  class="delete_any_item btn btn-danger"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<?php $n++; ?>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="4" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
@stop
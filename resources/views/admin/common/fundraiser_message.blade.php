@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
	  {{ trans("Fundraiser Message") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Fundraiser Message") }}</li>
	</ol>
</section>
<section class="content"> 
{{ Form::open(['role' => 'form','url' => 'admin/fundraiser-message','class' => 'mws-form','files'=>'true','id' => 'add_branding']) }}
<div class="row">
    <div class="col-md-6">
        <div class="form-group <?php echo ($errors->first('message')) ? 'has-error' : ''; ?>">
            {{ Form::label('message',trans("Message").' *', ['class' => 'mws-form-label message']) }}
            <div class="mws-form-item">
                {{ Form::text('message',isset($result->message) ? $result->message :'',['class' => 'form-control']) }}
                <div class="error-message help-inline">
                    <?php echo $errors->first('message'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mws-button-row">
    <div class="input" >
       <!-- <button type="button" class="btn btn-info" onclick="payAmount()">Pay Now</button> -->
      <input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
    </div>
</div>
{{ Form::close() }}
</section> 
@stop
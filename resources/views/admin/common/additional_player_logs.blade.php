@extends('admin.layouts.default')
@section('content')

<section class="content-header">
	<h1>
	  {{ trans("Purchased Player Logs") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Additional Player Logs") }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
					@if(Auth::guard('admin')->user()->user_role_id == 1)
						<th width="25%">Club Name</th>
					@endif
					<th width="25%">Players Added</th>
					<th width="25%">Price</th>
					<th width="25%">Purchase Date</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)  
						<?php if(!empty($record->player_number) || !empty($record->player_price)){ ?>
						<tr class="items-inner">
							@if(Auth::guard('admin')->user()->user_role_id == 1)
								<td>
									{{ $record->club_name }} 
								</td>
							@endif
							<td>
								{{ $record->player_number }} 
							</td>
							<td>
								{{ !empty($record->player_price) ? "$".$record->player_price :'' }}
							</td>
							<td>
								{{ date('d-M-Y h:i',strtotime($record->created_at))}}
							</td>
						</tr>
						<?php }else{ ?>
						<?php } ?>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="9" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
<script>

</script>
@stop
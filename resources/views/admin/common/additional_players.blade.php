@extends('admin.layouts.default')
@section('content')

<section class="content-header">
	<h1>
		{{ trans("Manage Player Packs") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Manage Player Packs") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/player-packs','class' => 'mws-form','files'=>'true']) }}
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-12">
			@if ($errors->any())
			       <div class="bg-red"> {!! implode('', $errors->all('<div style="margin-left: 10px;">:message</div>')) !!}</div>
			@endif
			<table class="table table-responsive table-bordered lastrow_feature_detail" width="100%">
				@if($packsDetails->isEmpty())
					<tr>
						<th>Players</th>
						<th>Price</th>
						<th></th>
					</tr>
					<tr class="add_brand0 add_more_feature_detail add_feature_detail" id="0">
						<td>
							<div class="mws-form-item add_feature_input">
								{{ Form::text('name[]','',['id' => 'name0','class'=>'form-control'])}}
								<span class="help-inline"></span>
							</div>
						</td>
						<td>
							<div class="mws-form-item add_feature_input">
								{{ Form::text('price[]','',['id' => 'price0','class'=>'form-control'])}}
								<span class="help-inline"></span>
							</div>
						</td>
						<td><input  type="button" value="Add More" class="btn btn-primary" id="add_more_feature_detail"/></td>
					</tr>
				@else
					<tr>
						<th>Players</th>
						<th>Price</th>
						<th><input  type="button" value="Add More" class="btn btn-primary" id="add_more_feature_detail"/></th>
					</tr>
					@foreach($packsDetails as $key=>$value)
						<tr class="add_brand{{$key}} add_more_feature_detail add_feature_detail" id="{{$value->id}}">
							<td>
								<div class="mws-form-item add_feature_input">
									{{ Form::text("name[]",$value->name,['id' => 'name'.$value->id,'class'=>'form-control'])}}
									<span class="help-inline"></span>
								</div>
							</td>
							<td>
								<div class="mws-form-item add_feature_input">
									{{ Form::text("price[]",$value->price,['id' => 'price'.$value->id,'class'=>'form-control'])}}
									<span class="help-inline"></span>
								</div>
							</td>
							<td>
								<!-- <input type="hidden" name="id[]" value="{{$value->id}}">
								<a href="javascript:void(0);" onclick="del_feature($(this),'{{$value->id}}');" id="{{$value->id}}" class="btn btn-info btn-small align_button add_more " style="margin-left:15px;" data-id="{{$value->id}}">
									<i class="fa fa-trash-o"></i>
								</a> -->
							</td>
						</tr>
					@endforeach
				@endif
			</table>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input">
			<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/player-packs')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
</section>
<script type="text/javascript">
$('#add_more_feature_detail').on('click', function() {
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	var get_last_id			=	$('.lastrow_feature_detail:last tr:last').attr('rel');
	if(typeof get_last_id === "undefined") {
		get_last_id			=	0;
	}
	var finalValNumber = $("#name"+get_last_id).val();
	var counter  = parseInt(get_last_id) + 1;
	if((finalValNumber != '')){ 
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url:'{{ url("admin/add-more-player-pack") }}',
			'type':'post',
			data:{'counter':counter},
			success:function(response){
				$('#loader_img').hide();
				$('.add_feature_detail').last().after(response);
			}
		});
	}else{    
		$(".add_feature_input").each(function(){ 
			var dataVal = $(this).find("input,text,file,select").val();
			if(dataVal == "" || dataVal == "undefined"){
				$(this).find("input,text,select").next().addClass('error');
				$(this).find("input,text,select").next().html("This field is required.");
			}
		});
		$('#loader_img').hide();
		return false;
	}
});
function del_feature(_this,id){ 
	if(id>0){
		bootbox.confirm("Are you sure want to remove this ?",
		function(result){
			if(result){
				$('#loader_img').show();
				$.ajax({
					headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					url:'{{ url("admin/delete-add-more-player-pack") }}',
					'type':'post',
					data:{'id':id},
					success:function(response){
						_this.closest('tr').remove();
						$('#loader_img').hide();

					}
				});
			}
		});
	}else{
		bootbox.confirm("Are you sure want to remove this ?",
		function(result){
			if(result){
				_this.closest('tr').remove();
			}
		});
	}
	
}
</script>
@stop

@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
	  {{ trans("Gameweeks") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Lockout Log") }}</li>
	</ol>
</section>
<section class="content">  
    <div class="row">
        {{ Form::open(['role' => 'form','url' => 'admin/admin-lockout-log','class' => 'mws-form',"method"=>"get"]) }}
        {{ Form::hidden('display') }}   
            <div class="col-md-2 col-sm-12">
                <div class="form-group ">  
                    {{ Form::select('mode',[null => 'Please Select mode'] +Config::get('home_club'),((isset($searchVariable['mode'])) ? $searchVariable['mode'] : ''),['id' => 'mode','class'=>'form-control choosen_selct'])}}
                </div>
            </div>  
            <div class="col-md-2 col-sm-12">
                <div class="form-group put_html">  
                    {{ Form::select('club',[null => 'Please Select Club'],((isset($searchVariable['club'])) ? $searchVariable['club'] : ''),['id' => 'club','class'=>'form-control choosen_selct'])}}
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
                <a href="{{URL::to('admin/admin-lockout-log')}}"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> {{ trans("Reset") }}</a>
            </div>
        {{ Form::close() }}
    </div>
	<div class="box">
		<div class="box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">GW</th>
						<th width="20%">Start Day</th>
						<th width="20%">End Day</th>
						<th width="20%">Start Time</th>
						<th width="20%">End Time</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				@if(!empty($result))
					@foreach($result as $key => $record)  
						<tr class="items-inner " style="{{ $record['current'] == 1 ? 'background: yellow;' : '' }}">
							<td>GW{{$key}}</td>
							<td>{{ $record['gw_start']->format('d.m.y')  }}</td>
							<td>{{ $record['gw_end']->format('d.m.y') }}</td>
							<td>{{ $record['gw_start']->format('g:i A') }}</td>
							<td>{{ $record['gw_end']->format('g:i A') }}</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="4" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif
		</tbody>					
			</table>
		</div>
	</div> 
</section> 
<script type="text/javascript">
$("#mode").on('change',function(){
    var selectValue   =  $(this).val();
    $('#loader_img').show(); 
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"{{  url('admin/get-club-mode-listing') }}",
        'type':'post',
        data:{'id':selectValue},
        async : false,
        success:function(response){
            $('.put_html').html(response);
            $('#loader_img').hide();
        }
    });
});
/*adter search club searhcing*/
var mode = "<?php  echo !empty($searchVariable['mode']) ? $searchVariable['mode']:'';  ?>";
var searchClubVar = "<?php  echo !empty($searchVariable['club']) ? $searchVariable['club']:'';  ?>";
if(mode !=''){ 
    $('#loader_img').show(); 
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"{{  url('admin/get-club-mode-listing') }}",
        'type':'post',
        data:{'id':mode,'club_id':searchClubVar},
        async : false,
        success:function(response){
            $('.put_html').html(response);
            $('#loader_img').hide();
        }
    });
}

</script>
@stop
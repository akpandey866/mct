<select name="team_id" class="form-control chosen_select">
	<option value="" selected="" >Please Select Team</option>
	<?php foreach ($teams as $key => $value) {  ?>
		<option value="{{$key}}">{{$value}}</option>
	<?php } ?>
</select>
<div class="error-message help-inline">
	<?php echo $errors->first('team_id'); ?>
</div>
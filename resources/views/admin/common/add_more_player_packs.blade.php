<tr class="add_feature{{$counter}} add_feature_detail" rel="{{$counter}}">
	<td>
		<div class="mws-form-item add_feature_input">
			{{ Form::text('name[]','',['id' => "name$counter",'class'=>'form-control'])}}
			<span class="help-inline"></span>
		</div>
	</td>
	<td>
		<div class="mws-form-item add_feature_input">
			{{ Form::text('price[]','',['id' => "price$counter",'class'=>'form-control'])}}
			<span class="help-inline"></span>
		</div>
	</td>
	<td>
		<a href="javascript:void(0);" onclick="del_feature($(this),0);" id="{{$counter}}" class="btn btn-info btn-small align_button add_more " style="margin-left:15px;">
			<i class="fa fa-trash-o"></i>
		</a>
	</td>
</tr>
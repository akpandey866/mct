@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
	  {{ trans("Scorers") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Scorer Access") }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
		{{ Form::open(['method' => 'get','role' => 'form','url' => 'admin/admin-scorer-access','class' => 'mws-form']) }}
       	 {{ Form::hidden('display') }}
            <div class="col-md-2 col-sm-1">
				<div class="form-group ">  
					{{ Form::select('mode',[null => 'Please Select mode'] +Config::get('home_club'),((isset($requestData['mode'])) ? $requestData['mode'] : ''),['class'=>'form-control mode choosen_selct'])}}
				</div>
			</div>	
			<div class="col-md-2 col-sm-1">
				<div class="form-group put_html">  
					{{ Form::select('club',[null => 'Please Select Club'],((isset($requestData['club'])) ? $requestData['club'] : ''),['class'=>'form-control club choosen_selct'])}}
				</div>
			</div>	
			<div class="col-md-2 col-sm-2">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
				<a href="{{URL::to('admin/admin-scorer-access')}}"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> {{ trans("Reset") }}</a>
			</div>
        {{ Form::close() }}
       </div>
	<div class="row">
			{{ Form::open(['role' => 'form','url' => 'admin/admin-save-scorer-access','class' => 'mws-form',"method"=>"post"]) }}		
			<div class="col-md-2 col-sm-3">
				{{ Form::select('mode',[null => 'Please Select mode'] +Config::get('home_club'),((isset($requestData['mode'])) ? $requestData['mode'] : ''),['class'=>'form-control mode choosen_selct'])}}
			</div>	
			<div class="col-md-2 col-sm-1">
				<div class="form-group put_html">  
					{{ Form::select('club',[null => 'Please Select Club'],((isset($requestData['club'])) ? $requestData['club'] : ''),['class'=>'form-control club choosen_selct'])}}
				</div>
			</div>		
			<div class="col-md-2 col-sm-3">
				<div class="form-group put_users_html">  
					{{ Form::select('user_id',[null => 'Please Select Users'],'',['class'=>'form-control users choosen_selct'])}}
					<div class="error-message help-inline">
						<?php echo $errors->first('user_id'); ?>
					</div>
				</div>
			</div>

			<div class="col-md-2 col-sm-3">
				<div class="form-group put_team_list_list">  
					{{ Form::select('team_id',[null => 'Please Select Players'],'',['class'=>'form-control choosen_selct'])}}
					<div class="error-message help-inline">
						<?php echo $errors->first('team_id'); ?>
					</div>
				</div>
			</div>
			<input type="hidden" name="club_id" class="club_id">
			<div class="col-md-2 col-sm-5">
				<div class="form-group">  
					<button class="btn btn-success">{{trans("Add Scorer") }}</button>
				</div>
			</div>
			{{ Form::close() }}
	</div> 
	<div class="box">
		<div class="box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">SN</th>
						<th width="20%">User Name</th>
						<th width="20%">Team</th>
						<th width="20%">Email</th>
						<th width="20%">Created On</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				@if(!$result->isEmpty())
				<?php $n=1; ?>
					@foreach($result as $key => $record)  
						<tr class="items-inner">
							<td>
								{{ $n }}
							</td>
							<td>
								{{ $record->username }}
							</td>
							<td>
								{{ isset($teams[$record->team_id]) ? $teams[$record->team_id] : '-' }}
							</td>
							<td>
								{{ $record->email }}
							</td>
							<td>
								{{ date(config::get("Reading.date_format"),strtotime($record->created_at))}}
							</td>
							<td data-th='Action'>
								<a title="{{ trans('Delete') }}" href="{{ URL::to('admin/delete-scorer-access/'.$record->id) }}"  class="delete_any_item btn btn-danger"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<?php $n++; ?>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="6" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
<script type="text/javascript">
$(".mode").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-verify-users') }}",
		'type':'post',
		data:{'id':selectValue},
		async : true,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});
var mode = "<?php  echo !empty($requestData['mode']) ? $requestData['mode']:'';  ?>";
var searchClubVar = "<?php  echo !empty($requestData['club']) ? $requestData['club']:'';  ?>";
if(mode !=''){  
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-verify-users') }}",
		'type':'post',
		data:{'id':mode,'club_id':searchClubVar},
		async : true,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
}
if(searchClubVar !=''){ 
	$(".club_id").val(searchClubVar);
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-scorer-users') }}",
		'type':'post',
		data:{'club_id':searchClubVar},
		async : true,
		success:function(response){
			$('.put_users_html').html(response);
			$('#loader_img').hide();
		}
	});
}
$(document).on("change",'.club',function(){
	var club_id = $(this).val();
	$(".club_id").val(club_id);

	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-scorer-users') }}",
		'type':'post',
		data:{'club_id':club_id},
		async : true,
		success:function(response){
			$('.put_users_html').html(response);
			$('#loader_img').hide();
		}
	});
});
$(document).on("change",'.team_listing',function(){ 
	var user_id = $(this).val();
	var club_id = $(".club_id").val();
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-scorer-team-lists') }}",
		'type':'post',
		data:{'club_id':club_id,'user_id':user_id},
		async : true,
		success:function(response){
			$('.put_team_list_list').html(response);
			$('#loader_img').hide();
		}
	});
});
</script>
@stop
@extends('admin.layouts.default')

@section('content')
<section class="content-header">
	<h1>
		{{ trans("View Contact") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href='{{ route("$modelName.index")}}'>{{ trans("Contact Management") }}</a></li>
		<li class="active">{{ trans("View Contact") }}</li>
	</ol>
</section>
<div class="box box-warning "> 
	<div class="row pad">
		<div class="col-md-12 col-sm-12">
			<div class="mws-panel-body no-padding dataTables_wrapper">
				<table class="table table-striped">
					<thead>
					<tr class="bgcss">
						<th  width="30%" height="50%" class="txtFotnSize" colspan="2">CONTACT DETAIL</th>
					</tr>
					</thead>
					<tbody>
						<tr>
							<th width="30%" class="details text-right txtFotnSize txtFotnSize" >Name</th>
							<td data-th='name'>{{ $model->name }}</td>
						</tr>
						<tr>
							<th width="30%" class="details text-right txtFotnSize txtFotnSize" >Email</th>
							<td data-th='email'>{{ $model->email }}</td>
						</tr>
						<tr>
							<th width="30%" class="details text-right txtFotnSize txtFotnSize" >Subject</th>
							<td data-th='subject'>{{ $model->subject }}</td>
						</tr>
						<tr>
							<th width="30%" valign="top" class="details text-right txtFotnSize txtFotnSize" >Message</th>
							<td data-th='Message'>{{ $model->message }}</td>
						</tr>
						<tr>
							<th width="30%" class="details text-right txtFotnSize txtFotnSize" >Created</th>
							<td data-th='{{ trans("Created at") }}'>{{ date(Config::get("Reading.date_format") , strtotime($model->created_at)) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- View contact detail end here -->
<!-- reply section start here -->
<div class="box box-warning "> 
	<div class="row pad">
		<div class="col-md-9 col-sm-8" id="reply">	
			<div class="mws-panel-header">
				<span>
					<i class="fa fa-exclamation-circle "></i>
					{{ trans("Reply")}}
				</span>
			</div>
			<div class="mws-panel-body no-padding dataTables_wrapper">
				<span class="contactMsg"></span>
				{{ Form::open(['role' => 'form','url'=>route("$modelName.reply","$modelId"),'class' => 'mws-form']) }}
				<div class="mws-form-inline">
					<div class="mws-form-row">
						<div class="mws-form-message info">Message will be attached in email.</div>
					</div>
					<div class="mws-from-row contactMessageBox">
						{{  Form::label('body', trans("Message"), ['class' => 'mws-form-label']) }}
						{{ Form::textarea("message",'', ['id' => 'body','rows' => 5,'cols'=>80,'class'=>'form-control contactHeight','required']) }}
					</div>
					<div class="error-message help-inline">
						{{ $errors->first('message') }}
					</div>
				</div>
			</div>
			<div class="clearfix">
				<br />
				<div class="mws-button-row">
					<input type="submit" value='Reply' class="btn btn-danger">
					<a href="{{Request::url()}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }} </a>
					
					<a href="{{URL::to('admin/contact-manager')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
				</div>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
<style>
	
</style>
@stop

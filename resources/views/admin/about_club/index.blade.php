@extends('admin.layouts.default')
@section('content')

<section class="content-header">
	<h1>
	  {{ trans("About Club") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("About Club") }}</li>
	</ol>
</section>
<section class="content"> 
	
	<div class="row">
		{{ Form::open(['role' => 'form','url' => 'admin/about-club','class' => 'mws-form',"method"=>"get"]) }}
		{{ Form::hidden('display') }}	
		<div class="col-md-2 col-sm-2">
			<div class="form-group ">  
				{{ Form::text('text1',((isset($searchVariable['text1'])) ? $searchVariable['text1'] : ''), ['class' => 'form-control','placeholder'=>'First text']) }}
			</div>
		</div>	
		<div class="col-md-2 col-sm-2">
			<div class="form-group ">  
				{{ Form::text('text2',((isset($searchVariable['text2'])) ? $searchVariable['text2'] : ''), ['class' => 'form-control','placeholder'=>'Second text']) }}
			</div>
		</div>
		<div class="col-md-2 col-sm-2">
			<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
			<a href="{{URL::to('admin/about-club')}}"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> {{ trans("Reset") }}</a>
		</div>
		{{ Form::close() }}
		<div class="col-md-8 col-sm-8">
			<div class="form-group">  
				<a href="{{URL::to('admin/about-club/add-about-club')}}" class="btn btn-success btn-small  pull-right" style="margin:0;">{{ trans("Add New About Club") }} </a>
			</div>
		</div>
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="15%">
							{{
								link_to_route(
									"AboutClub.index",
									trans("First Text"),
									array(
										'sortBy' => 'text1',
										'order' => ($sortBy == 'text1' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'text1' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'text1' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="25%">
							{{
								link_to_route(
									"AboutClub.index",
									trans("Second Text"),
									array(
										'sortBy' => 'text2',
										'order' => ($sortBy == 'text2' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'text2' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'text2' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
									"serve.index",
									trans("Status"),
									array(
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
									"AboutClub.index",
									trans("Created On"),
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							Action
						</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)  
						<tr class="items-inner">
							<td> 
								{{ $record->text1 }} 
							</td>
							<td> 
								{{ $record->text2 }} 
							</td>
							<td>
								@if($record->is_active	==1)
									<span class="label label-success" >{{ trans("Activated") }}</span>
								@else
									<span class="label label-warning" >{{ trans("Deactivated") }}</span>
								@endif
							</td>
							<td>
								{{ date(config::get("Reading.date_format"),strtotime($record->created_at))}}
							</td>
							<td data-th='Action'>
								<a title="{{ trans('Edit') }}" href="{{URL::to('admin/about-club/edit-about-club/'.$record->id)}}" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>
								@if($record->is_active == 1)
									<a  title="Click To Deactivate" href="{{URL::to('admin/about-club/update-status/'.$record->id.'/0')}}" class="btn btn-success btn-small status_any_item"><span class="fa fa-check"></span>
									</a>
								@else
									<a title="Click To Activate" href="{{URL::to('admin/about-club/update-status/'.$record->id.'/1')}}" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
									</a> 
								@endif							
								<a title="{{ trans('Delete') }}" href="{{ URL::to('admin/about-club/delete-about-club/'.$record->id) }}"  class="delete_any_item btn btn-danger">
									<i class="fa fa-trash-o"></i>
								</a>
							</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="5" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
@stop
@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		{{ trans("Edit About Club") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/about-club')}}">{{ trans("About Club") }}</a></li>
		<li class="active">{{ trans("Edit About Club") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/about-club/edit-about-club','class' => 'mws-form','files'=>'true']) }}
	{{ Form::hidden('id',$details->id) }}
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('text1')) ? 'has-error' : ''; ?>">
				{{ Form::label('text1', trans("First Text"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('text1',isset($details->text1) ? $details->text1 :'', ['class' => 'form-control ','id'=>'text1']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('text1'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('text2')) ? 'has-error' : ''; ?>">
				{{ Form::label('text2', trans("Second Text"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('text2',isset($details->text2) ? $details->text2 :'', ['class' => 'form-control ','id'=>'text2']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('text2'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Update') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/about-club/edit-about-club/'.$details->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/about-club')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
@stop

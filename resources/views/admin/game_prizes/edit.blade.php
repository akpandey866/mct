@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
	  {{ trans("Game Prizes") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Game Prizes") }}</li>
	</ol>
</section>
<section class="content"> 
            	{{ Form::open(['role' => 'form','url' => 'admin/game-prizes/edit-game-prize','class' => 'mws-form','files'=>'true']) }}
            	{{ Form::hidden('game_prize_id',$userDetails->id) }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group <?php echo ($errors->first('club_type')) ? 'has-error' : ''; ?>">
                            <label for="address" class="mws-form-label">Overall Prizes</label>

                            <div class="mws-form-item">
                                <?php  echo  Config::get('prize_type')[$userDetails->category_id]; ?>
                                {{ Form::hidden('category_id',isset($userDetails->category_id) ? $userDetails->category_id :'') }}

                                <!-- {{ Form::select(
                                'category_id',
                                [null => 'Please Select Prize Type'] + Config::get('prize_type'),
                                isset($userDetails->category_id) ? $userDetails->category_id :'',
                                ['id' => 'userDetails','class'=>'form-control']
                                ) 
                                }} -->
                                <div class="error-message help-inline">
                                    <?php echo $errors->first('club_type'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?php echo ($errors->first('title')) ? 'has-error' : ''; ?>">
                            {{ Form::label('title',trans("Title").' *', ['class' => 'mws-form-label title']) }}
                            <div class="mws-form-item">
                                {{ Form::text('title',isset($userDetails->title) ? $userDetails->title :'',['class' => 'form-control']) }}
                                <div class="error-message help-inline">
                                    <?php echo $errors->first('title'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group <?php echo ($errors->first('description')) ? 'has-error' : ''; ?>">
                            {{ Form::label('description',trans("Description").' *', ['class' => 'mws-form-label description']) }}
                            <div class="mws-form-item">
                                {{ Form::textarea('description',isset($userDetails->description) ? $userDetails->description :'',['class' => 'form-control']) }}
                                <div class="error-message help-inline">
                                    <?php echo $errors->first('description'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
                            <label for="image" class="mws-form-label image">Prize Image *</label>
                            <div class="mws-form-item">
                                {{ Form::file('image', array('accept' => 'image/*')) }}<br>
                                @if(File::exists(PRIZE_IMAGE_ROOT_PATH.$userDetails->image))
									<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo PRIZE_IMAGE_URL.$userDetails->image; ?>">
										<div class="usermgmt_image">
											<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.PRIZE_IMAGE_URL.'/'.$userDetails->image ?>">
										</div>
									</a>
								@endif
                                <div class="error-message help-inline">
                                    <?php echo $errors->first('image'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 @if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
                            <label for="address" class="mws-form-label">Club *</label>
                            <div class="mws-form-item">
                                {{ Form::select(
                                'club',
                                [null => 'Please Select Club'] + $clubList,
                                isset($userDetails->club) ? $userDetails->club :'',
                                ['id' => 'clubList','class'=>'form-control']
                                ) 
                                }}
                                <div class="error-message help-inline">
                                    <?php echo $errors->first('club'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                {{ Form::hidden('club',Auth::guard('admin')->user()->id)}}
                @endif
                <div class="mws-button-row">
					<div class="input" >
						<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
						<a href="{{URL::to('admin/game-prizes/edit-game-prize/'.$userDetails->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
						<a href="{{URL::to('admin/game-prizes')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
					</div>
				</div>
	    {{ Form::close() }}
	</div>	 
</section> 
<script>
function savePoints() { 
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	$.ajax({
		url: '{{ URL("admin/game-points/edit-game-points") }}',
		type:'post',
		data: $('#updateGamePoints').serialize(),
		success: function(r){
			window.location.href	 =	"{{ URL('admin/game-prizes') }}";
			$('#loader_img').hide();
		}
	});
}
    
$('#updateGamePoints').each(function() {
    $(this).find('input').keypress(function(e) {
        if(e.which == 10 || e.which == 13) {
        savePoints();
        return false;
       }
    });
});
</script>
@stop
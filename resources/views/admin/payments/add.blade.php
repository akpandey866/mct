@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
	  {{ trans("Add Game Prize") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Add Game Prize") }}</li>
	</ol>
</section>
<section class="content"> 
            	{{ Form::open(['role' => 'form','url' => 'admin/game-prizes/add-game-prize','class' => 'mws-form','files'=>'true']) }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group <?php echo ($errors->first('category_id')) ? 'has-error' : ''; ?>">
                            <label for="address" class="mws-form-label">Overall Prizes *</label>
                            <div class="mws-form-item">
                                {{ Form::select(
                                'category_id',
                                [null => 'Please Select Prize Type'] + Config::get('prize_type'),
                                '',
                                ['id' => 'category_id','class'=>'form-control']
                                ) 
                                }}
                                <div class="error-message help-inline">
                                    <?php echo $errors->first('category_id'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?php echo ($errors->first('title')) ? 'has-error' : ''; ?>">
                            {{ Form::label('title',trans("Title").' *', ['class' => 'mws-form-label title']) }}
                            <div class="mws-form-item">
                                {{ Form::text('title','',['class' => 'form-control']) }}
                                <div class="error-message help-inline">
                                    <?php echo $errors->first('title'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group <?php echo ($errors->first('description')) ? 'has-error' : ''; ?>">
                            {{ Form::label('description',trans("Description").' *', ['class' => 'mws-form-label description']) }}
                            <div class="mws-form-item">
                                {{ Form::textarea('description','',['class' => 'form-control']) }}
                                <div class="error-message help-inline">
                                    <?php echo $errors->first('description'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
                            <label for="image" class="mws-form-label image">Prize Image *</label>
                            <div class="mws-form-item">
                                {{ Form::file('image', array('accept' => 'image/*')) }}
                                <div class="error-message help-inline">
                                    <?php echo $errors->first('image'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
                            <label for="address" class="mws-form-label">Club *</label>
                            <div class="mws-form-item">
                                {{ Form::select(
                                'club',
                                [null => 'Please Select Club'] + $clubList,
                                '',
                                ['id' => 'clubList','class'=>'form-control']
                                ) 
                                }}
                                <div class="error-message help-inline">
                                    <?php echo $errors->first('club'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                {{ Form::hidden('club',Auth::guard('admin')->user()->id)}}
                @endif
                <div class="mws-button-row">
                    <div class="input" >
                        <input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
                        <a href="{{URL::to('admin/team/add-game-prize/')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
                        <a href="{{URL::to('admin/game-prizes/')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
                    </div>
                </div>
	    {{ Form::close() }}
</section>
@stop
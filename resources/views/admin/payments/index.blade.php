@extends('admin.layouts.default')
@section('content')

<section class="content-header">
    <h1>
      {{ trans("Game Prizes") }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">{{ trans("Game Prizes") }}</li>
    </ol>
</section>
<section class="content"> 
    
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="form-group">  
                <a href="{{URL::to('admin/game-prizes/add-game-prize')}}" class="btn btn-success btn-small  pull-right" style="margin:0;">{{ trans("Add New Game Prize") }} </a>
            </div>
        </div>
    </div> 
    <div class="box">
        <div class="box-body ">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="11%">
                            Image
                        </th>
                        <th width="11%">
                            {{
                                link_to_route(
                                    "GamePrize.index",
                                    trans("Category"),
                                    array(
                                        'sortBy' => 'category_id',
                                        'order' => ($sortBy == 'category_id' && $order == 'desc') ? 'asc' : 'desc',
                                        $query_string
                                    ),
                                   array('class' => (($sortBy == 'category_id' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'category_id' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                )
                            }}
                        </th>
                        <th width="11%">
                            {{
                                link_to_route(
                                    "GamePrize.index",
                                    trans("Title"),
                                    array(
                                        'sortBy' => 'title',
                                        'order' => ($sortBy == 'title' && $order == 'desc') ? 'asc' : 'desc',
                                        $query_string
                                    ),
                                   array('class' => (($sortBy == 'title' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'title' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                )
                            }}
                        </th>
                        <th width="11%">
                            {{
                                link_to_route(
                                    "GamePrize.index",
                                    trans("Status"),
                                    array(
                                        'sortBy' => 'is_active',
                                        'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
                                        $query_string
                                    ),
                                   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                )
                            }}
                        </th>
                        <th width="20%">
                            {{
                                link_to_route(
                                    "GamePrize.index",
                                    trans("Created On"),
                                    array(
                                        'sortBy' => 'created_at',
                                        'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
                                        $query_string
                                    ),
                                   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
                                )
                            }}
                        </th>
                        <th width="20%">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody id="powerwidgets">
                @if(!$result->isEmpty())
                    @foreach($result as $key => $record)  
                        <tr class="items-inner">
                            <td>
                                @if(File::exists(PRIZE_IMAGE_ROOT_PATH.$record->image))
                                    <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo PRIZE_IMAGE_URL.$record->image; ?>">
                                        <div class="usermgmt_image">
                                            <img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.PRIZE_IMAGE_URL.'/'.$record->image ?>">
                                        </div>
                                    </a>
                                @endif
                            </td>
                            <td> 
                                {{ config('prize_type')[$record->category_id] }} 
                            </td>
                            <td>
                                {{ $record->title }}
                            </td>
                            <td>
                                @if($record->is_active  ==1)
                                    <span class="label label-success" >{{ trans("Activated") }}</span>
                                @else
                                    <span class="label label-warning" >{{ trans("Deactivated") }}</span>
                                @endif
                            </td>
                            <td>
                                {{ date(config::get("Reading.date_format"),strtotime($record->created_at))}}
                            </td>
                            <td data-th='Action'>
                                <a title="{{ trans('Edit') }}" href="{{URL::to('admin/game-prizes/edit-game-prize/'.$record->id)}}" class="btn btn-primary">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @if($record->is_active == 1)
                                    <a  title="Click To Deactivate" href="{{URL::to('admin/game-prizes/update-status/'.$record->id.'/0')}}" class="btn btn-success btn-small status_any_item"><span class="fa fa-check"></span>
                                    </a>
                                @else
                                    <a title="Click To Activate" href="{{URL::to('admin/game-prizes/update-status/'.$record->id.'/1')}}" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
                                    </a> 
                                @endif                          
                                <a title="{{ trans('Delete') }}" href="{{ URL::to('admin/game-prizes/delete-sponsor/'.$record->id) }}"  class="delete_any_item btn btn-danger">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                                
                            </td>
                        </tr>
                     @endforeach
                     @else
                        <tr>
                            <td class="alignCenterClass" colspan="9" >{{ trans("No record is yet available.") }}</td>
                        </tr>
                    @endif
        </tbody>                    
            </table>
        </div>
        <div class="box-footer clearfix">   
            <div class="col-md-3 col-sm-4 "></div>
            <div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
        </div>
    </div> 
</section> 
<script>

</script>
@stop
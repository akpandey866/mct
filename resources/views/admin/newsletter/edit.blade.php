@extends('admin.layouts.default')
@section('content')

<!-- chosen select box css and js start here-->
{{ HTML::style('css/admin/chosen.css') }}
{{ HTML::script('js/admin/chosen.jquery.js') }}
<!-- chosen select box css and js end here-->

<!-- ckeditor js start here-->
{{ HTML::script('js/admin/ckeditor/ckeditor.js') }}
<!-- ckeditor js end here-->

<!-- datetime picker js and css start here-->
{{ HTML::script('js/admin/jui/js/jquery-ui-1.9.2.min.js') }}
{{ HTML::script('js/admin/jui/js/timepicker/jquery-ui-timepicker.min.js') }}
{{ HTML::script('js/admin/prettyCheckable.js') }}
{{ HTML::style('css/admin/jui/css/jquery.ui.all.css') }}
{{ HTML::style('css/admin/prettyCheckable.css') }}
{{ HTML::style('css/admin/timepicker.css') }}
<!-- date time picker js and css and here-->

<script type="text/javascript">

/* For datetimepicker */
	$(function(){
		$(".chzn-select").chosen();
		
		$('#scheduled_time').datetimepicker({ 
			timeFormat: "hh:mm:ss tt",
			dateFormat: 'yy-mm-dd',
			ampm: true,
			minDate: new Date(<?php echo date('Y,m-1,d,H,i');  ?>),
		});	
	});
</script>
<section class="content-header">
	<h1>
		{{ trans("messages.system_management.edit_newsletter") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/news-letter')}}">Newsletter</a></li>
		<li class="active">Edit Newsletter</li>
	</ol>
</section>	

<section="content">
	<div class="row pad">
		{{ Form::open(['role' => 'form','url' => 'admin/news-letter/edit-template/'.$result->id,'class' => 'mws-form']) }}
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('scheduled_time')) ? 'has-error' : ''; ?>">
					<div class="mws-form-row">
						{{  Form::label('scheduled_time', trans("messages.system_management.scheduled_time"), ['class' => 'mws-form-label']) }}
						<div class="mws-form-item">
							{{ Form::text('scheduled_time', $result->scheduled_time, ['class' => 'form-control','readonly']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('scheduled_time'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group  <?php echo ($errors->first('subject')) ? 'has-error' : ''; ?>">
					<div class="mws-form-row">
						{{  Form::label('subject', trans("messages.system_management.subject"), ['class' => 'mws-form-label']) }}
						<div class="mws-form-item">
							{{ Form::text('subject', $result->subject, ['class' => 'form-control']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('subject'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="mws-form-inline">
				<div class="form-group">
					{{  Form::label('newsletter_subscriber_id', 'Subscribers', ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						{{ Form::radio('subscribers',MANUAL,true,array('id'=>'manual_id')) }}
						{{ Form::label('manual_id',trans("Manual")) }} &nbsp
						
						{{ Form::radio('subscribers',FITNESS_ENTHUSIAST,false,array('id'=>'enthusiast_id','class'=>'fitness_enthusiast_trainer')) }}
						{{ Form::label('enthusiast_id',trans("Fitness Enthusiast")) }}&nbsp 
						
						{{ Form::radio('subscribers',FITNESS_TRAINER_DIETICIAN,false,array('id'=>'trainer_id','class'=>'fitness_enthusiast_trainer')) }}
						{{ Form::label('trainer_id',trans("Fitness Trainer/Dietician")) }}
					</div>
				</div>
			</div>
				<div class="mws-form-inline newsletter_subscriber">
					<div class="form-group <?php echo ($errors->first('newsletter_subscriber_id')) ? 'has-error' : ''; ?>">					{{Form::label('newsletter_subscriber_id',trans("messages.system_management.select_subscriber"), ['class' => 'mws-form-label']) }}
						<div class="mws-form-item">
							{{ Form::select('newsletter_subscriber_id[]',$subscriberArray, $allReadySubscriberArray,['class' => 'chzn-select','form-control', 'style' => 'width:100%', 'data-placeholder'=>'Select Subscribers','multiple'=>'multiple']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('newsletter_subscriber_id'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="mws-form-inline">
					<div class="form-group  <?php echo ($errors->first('constant')) ? 'has-error' : ''; ?>">
						{{  Form::label('constant', trans("messages.system_management.constants"), ['class' => 'mws-form-label']) }}
						<div class="mws-form-item">
							<div class="col-md-6">
								<?php $constantArray = Config::get('newsletter_template_constant'); ?>
								{{ Form::select('constant', $constantArray,'', ['id' => 'constants','empty' => 'Select one','class' => 'form-control']) }}
								<div class="error-message help-inline">
									<?php echo $errors->first('constants'); ?>
								</div>
							</div>
						</div>
					<div class="col-md-6">
						<span >
							<a onclick = "return InsertHTML()" href="javascript:void(0)" class="btn btn-success no-ajax"><i class="icon-white "></i>{{ trans("messages.system_management.insert_variable") }} </a>
						</span>
					</div>
					</div>	
				</div>
				<div class="mws-form-row ">
					{{  Form::label('body', trans("messages.system_management.email_body"), ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						{{ Form::textarea("body",$result->body, ['class' => 'small','id' => 'body']) }}
						<span class="error-message help-inline">
							<?php echo $errors->first('body'); ?>
						</span>
					</div>
					<script type="text/javascript">
						// <![CDATA[
						CKEDITOR.replace( 'body',
						{
							height: 350,
							width: 507,
							filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
							filebrowserImageWindowWidth : '640',
							filebrowserImageWindowHeight : '480',
							enterMode : CKEDITOR.ENTER_BR
						});
						//]]>		
					</script>
				</div>
			<div class="mws-button-row">
				<div class="input" >
					<input type="submit" value="{{ trans('messages.system_management.save') }}" class="btn btn-danger">
					
					<a href="{{URL::to('admin/news-letter/edit-template/'.$result->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i>{{ trans('messages.system_management.reset') }}</a>
					
					<a href="{{URL::to('admin/news-letter')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
				</div>
			</div>
		{{ Form::close() }}
		</div>    	
	</div>
</section>
<script type='text/javascript'>
	<?php if(!empty(Input::old('subscribers')) && (Input::old('subscribers') == FITNESS_ENTHUSIAST ||  Input::old('subscribers') == FITNESS_TRAINER_DIETICIAN)){?>
	$('.newsletter_subscriber').hide();
	<?php }else{
	?>
	$('.newsletter_subscriber').show();
	<?php } ?>
	$('#manual_id').click(function(){
	 
		$('.newsletter_subscriber').show();
	});
	$('.fitness_enthusiast_trainer').click(function(){
		$('.newsletter_subscriber').hide();
	});
	/* this function insert defined onstant on button click */
	function InsertHTML() {
		var strUser = document.getElementById("constants").value;
		if(strUser != ''){
			var newStr = '{'+strUser+'}';
			var oEditor = CKEDITOR.instances["body"] ;
			oEditor.insertHtml(newStr) ;	
		}
    } 
</script>
<style>
	.chosen-container-multi .chosen-choices li.search-field input[type="text"] {
		padding:1px;
	}
	.chosen-choices{
		height: 38px;
	}
</style>
@stop

@extends('admin.layouts.default')
@section('content')
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<script src="{{ asset('js/admin/bootstrap-timepicker.min.js') }}"></script> 
 <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-timepicker.min.css') }}">
<section class="content-header">
	<h1>
		{{ trans("Edit Availability") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/availability/')}}">{{ trans("Availability") }}</a></li>
		<li class="active">{{ trans("Edit Availability") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/availability/edit-availability','class' => 'mws-form','files'=>'true','id'=>'add_availability']) }}
	{{ Form::hidden('availability_id',$availabilityDetails->id) }}
	@if(Auth::guard('admin')->user()->user_role_id == 1)
		<div class="row">
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('mode')) ? 'has-error' : ''; ?>">
					{{ Form::label('mode', trans("Game Mode"), ['class' => 'mws-form-label'])}}
					<div class="mws-form-item">
						  {{ Form::select('mode',[null => 'Please Select Mode'] +Config::get('home_club'),isset($availabilityDetails->mode) ? $availabilityDetails->mode :'',['id' => 'mode','class'=>'form-control'])}}
						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
					{{ Form::label('club', trans("Club"), ['class' => 'mws-form-label'])}}
					<div class="mws-form-item put_club_html">
						  {{ Form::select('club',[null => 'Please Select Club'],isset($availabilityDetails->club) ? $availabilityDetails->club :'',['id' => 'club','class'=>'form-control'])}}
						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('player')) ? 'has-error' : ''; ?>">
				{{ Form::label('player', trans("Player").' *', ['class' => 'mws-form-label'])}}
					@if(Auth::guard('admin')->user()->user_role_id == 1)
					<div class="mws-form-item put_player_html">
					  {{ Form::select('player',[null => 'Please Select Player'] + $playerList,isset($availabilityDetails->player) ? $availabilityDetails->player :'',['id' => 'player','class'=>'form-control'])}}
					  	<div class="error-message help-inline">
							<?php echo $errors->first('player'); ?>
						</div>
					</div>
					@else
						<div class="mws-form-item put_html">
						 {{ Form::select('player',[null => 'Please Select Player']+$playerList,isset($availabilityDetails->player) ? $availabilityDetails->player :'',['id' => 'player','class'=>'form-control'])}} 
							<div class="error-message help-inline">
							<?php echo $errors->first('player'); ?>
						</div>
						</div>
					@endif
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('date_from')) ? 'has-error' : ''; ?>">
				{{ Form::label('date_from', trans("Unavailable From").' *', ['class' => 'mws-form-label'])}}
				<div class="input-group date">
				  <div class="input-group-addon">
				    <i class="fa fa-calendar"></i>
				  </div>
				 {{ Form::text('date_from',isset($availabilityDetails->date_from) ? $availabilityDetails->date_from :'', ['class' => 'form-control ','placeholder'=>'Date From','id'=>'date_from']) }}
				
				</div>
				<div class="error-message help-inline">
					<?php echo $errors->first('date_from'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
				{{ Form::label('reason', trans("Reason"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					 {{ Form::text('reason',isset($availabilityDetails->reason) ? $availabilityDetails->reason :'', ['class' => 'form-control ','placeholder'=>'Reason','id'=>'reason']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('reason'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('date_till')) ? 'has-error' : ''; ?>">
				{{ Form::label('date_till', trans("Unavailable Till").' *', ['class' => 'mws-form-label'])}}
				<div class="input-group date">
				  <div class="input-group-addon">
				    <i class="fa fa-calendar"></i>
				  </div>
				 {{ Form::text('date_till',isset($availabilityDetails->date_till) ? $availabilityDetails->date_till :'', ['class' => 'form-control ','placeholder'=>'Date Till','id'=>'date_till']) }}
				</div>
				<div class="error-message help-inline">
					<?php echo $errors->first('date_till'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="button" value="{{ trans('Save') }}" class="btn btn-danger" onclick="save_availability();">
			<a href="{{URL::to('admin/availability/edit-availability/'.$availabilityDetails->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/availability')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script type="text/javascript">
$(document).ready(function() {
	 $( "#date_from" ).datepicker({
		format 	: 'yyyy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		autoclose   :true,
		onSelect	: function( selectedDate ){ $("#date_till").datepicker("option","minDate",selectedDate); }
	});
	$( "#date_till" ).datepicker({
		format 	: 'yyyy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		autoclose   :true,
		onSelect	: function( selectedDate ){ $("#date_from").datepicker("option","maxDate",selectedDate); }
	});
});

$("#mode").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-listing') }}",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_club_html').html(response);
			$('#loader_img').hide();
		}
	});
});

var mode = '<?php echo !empty($availabilityDetails->mode) ? $availabilityDetails->mode : 0  ?>';
var club = '<?php echo !empty($availabilityDetails->club) ? $availabilityDetails->club : 0  ?>';
if(mode !=''){ 
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-listing') }}",
		'type':'post',
		data:{'id':mode,'club_id':club},
		async : false,
		success:function(response){
			$('.put_club_html').html(response);
			$('#loader_img').hide();
		}
	});
}

$("#club").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-playerlist') }}",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});

function save_availability(){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
	var form = $('#add_availability')[0];
	var formData = new FormData(form);
    $.ajax({
        url: '{{ route("Availability.updateAvailability") }}',
        type:'post',
        data: formData,
		processData: false,
		contentType: false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
				//show_message(data['message'],"success");
                window.location.href   =  "{{ url('admin/availability') }}";
            }else {
                $.each(data['errors'],function(index,html){
                	if(index == "date_from"){
                	 	$(".date_from").addClass('error');
                	 	$(".date_from").html(html);
                	}if(index == "date_till"){
                		$(".date_till").addClass('error');
                		$(".date_till").html(html);
                	}
                    $("#"+index).next().addClass('error');
                    $("#"+index).next().html(html);
                });
                $('#loader_img').hide();
            }
            $('#loader_img').hide();
        }
    });
}
$('#add_availability').each(function() {
	$(this).find('input').keypress(function(e) {
       if(e.which == 10 || e.which == 13) {
			save_availability();
			return false;
        }
    });
});
</script>
@stop

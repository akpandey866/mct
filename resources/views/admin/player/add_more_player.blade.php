<tr class="add_feature_detail" rel="{{$counter}}">
	<td>
		<div class="mws-form-item add_feature_input">
			@if(auth()->guard('admin')->user()->user_role_id != SUPER_ADMIN_ROLE_ID)
				{{ Form::hidden("data[$counter][club]",auth()->guard('admin')->user()->id,['id' => "club$counter"])}}
			@endif
			{{ Form::text("data[$counter][first_name]",'',['id' => "first_name$counter",'class'=>'form-control'])}}
			<span class="help-inline"></span>
		</div>
	</td>
	<td>
		<div class="mws-form-item add_feature_input">
			{{ Form::text("data[$counter][last_name]",'',['id' => "last_name$counter",'class'=>'form-control'])}}
			<span class="help-inline"></span>
		</div>
	</td>
	<td>
		<div class="mws-form-item add_feature_input">
			{{ Form::select("data[$counter][position]",[null => 'Select Position'] + $position,'',['id' => "position$counter",'class'=>'form-control ']) }}
			<span class="help-inline"></span>
		</div>
	</td>
<!-- 	<td>
		<div class="mws-form-item add_feature_input">
			{{ Form::select("data[$counter][category]",[null => 'Select Category'] + $category,'',['id' => "category$counter",'class'=>'form-control ']) }}
			<span class="help-inline"></span>
		</div>
	</td> -->
	<td>
		<div class="mws-form-item add_feature_input">
			{{ Form::select("data[$counter][value]",[null => 'Select Value'] + $svalue,'',['id' => "svalue$counter",'class'=>'form-control ']) }}
			<span class="help-inline" id="svalueerror$counter"></span>
		</div>
	</td>
	<td>
					        	
	{{ Form::select("data[$counter][bat_style]",[null => 'Please Select Bat Style'] + $batStyle, isset($userDetails->batStyle) ? $userDetails->batStyle :'',['id' => 'batStyle','class'=>'form-control ']) }}
	</td>
	<td>
	{{ Form::select("data[$counter][bowl_style]",[null => 'Please Select Bowl Style'] + $bowlStyle,isset($userDetails->bowl_style) ? $userDetails->bowl_style :'',['id' => 'bowl_style','class'=>'form-control '])}}
	</td>
	@if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
	<td>
		<div class="mws-form-item add_feature_input">
			{{ Form::select("data[$counter][club]",[null => 'Select Club'] + $cludDetails,'',['id' => "club$counter",'class'=>'form-control ']) }}
			<span class="help-inline"></span>
		</div>
	</td>
	@endif
	<td>
		<div class="mws-form-item">
			{{ Form::file("data[$counter][image]", array('accept' => 'image/*','id' => "image$counter")) }}
			<span class="help-inline"></span>
		</div>
	</td>
	<td>
		<a href="javascript:void(0);" onclick="del_feature($(this),0);" id="{{$counter}}" class="btn btn-info btn-small align_button add_more " style="margin-left:15px;">
			<i class="fa fa-trash-o"></i>
		</a>
	</td>
</tr>
@extends('admin.layouts.default')
@section('content')
<style type="text/css">
	.chosen-container .chosen-results {
	    max-height:100px;
	}
</style>
<script>
jQuery(document).ready(function(){
$(".change_status").chosen();
});
</script>
<section class="content-header">
	<h1>
		{{ trans("Edit Player") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/player/')}}">{{ trans("Player") }}</a></li>
		<li class="active">{{ trans("Edit Player") }} </li>
	</ol>
</section>
<?php $checkPlayerId = playerIdInTeam($userDetails->id); ?>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/player/edit-player','class' => 'mws-form','files'=>'true','id'=>'edit_player']) }}
	{{ Form::hidden('player_id',$userDetails->id,['class' => '']) }}
<div class="row">

      <div class="col-md-6">
       <div class="form-group <?php echo ($errors->first('first_name')) ? 'has-error' : ''; ?>">
		{{ Form::label('first_name',trans("First Name").' *', ['class' => 'mws-form-label']) }}
		<div class="mws-form-item">
			@if($checkPlayerId == 1)
				{{ $userDetails->first_name	}}
				{{ Form::hidden('first_name',isset($userDetails->first_name) ? $userDetails->first_name :'') }}
			@else
				{{ Form::text('first_name',isset($userDetails->first_name) ? $userDetails->first_name :'',['class' => 'form-control']) }}
			@endif
			<div class="error-message help-inline">
				<?php echo $errors->first('first_name'); ?>
			</div>
		</div></div>
  </div>
  <div class="col-md-6">
	<div class="form-group <?php echo ($errors->first('last_name')) ? 'has-error' : ''; ?>">
		{{ Form::label('last_name',trans("Last Name").' *', ['class' => 'mws-form-label']) }}
		<div class="mws-form-item">
			@if($checkPlayerId == 1)
				{{ $userDetails->last_name	}}
				{{ Form::hidden('last_name',isset($userDetails->last_name) ? $userDetails->last_name :'') }}
			@else
				{{ Form::text('last_name',isset($userDetails->last_name) ? $userDetails->last_name :'',['class' => 'form-control']) }}
			@endif
			<div class="error-message help-inline">
				<?php echo $errors->first('last_name'); ?>
			</div>
		</div>
	</div>
</div>
  
</div>	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('svalue')) ? 'has-error' : ''; ?>">
				{{ Form::label('svalue', trans("Value($)").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					@if($checkPlayerId == 1)
						{{ $userDetails->svalue	}}
						{{ Form::hidden('svalue',isset($userDetails->svalue) ? $userDetails->svalue :'') }}
					@else
						{{ Form::select(
							 'svalue',
							 [null => 'Please Select Value'] + $svalue,
							 isset($userDetails->svalue) ? $userDetails->svalue :'',
							 ['id' => 'svalue','class'=>'form-control chosen_select']
							) 
						}}
					@endif
					<div class="error-message help-inline">
						<?php echo $errors->first('svalue'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('position')) ? 'has-error' : ''; ?>">
				{{ Form::label('position', trans("Position").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					@if($checkPlayerId == 1)
						{{ $userDetails->player_position	}}
						{{ Form::hidden('position',isset($userDetails->position) ? $userDetails->position :'') }}
					@else
						{{ Form::select(
							 'position',
							 [null => 'Please Select Position'] + $position,
							 isset($userDetails->position) ? $userDetails->position :'',
							 ['id' => 'position','class'=>'form-control chosen_select']
							) 
						}}
					@endif					
					<div class="error-message help-inline">
						<?php echo $errors->first('position'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('bat_style')) ? 'has-error' : ''; ?>">
				{{ Form::label('bat_style', trans("Bat Style"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					<?php /*
					@if($checkPlayerId == 1)
						{{ $userDetails->player_bat_style }}
						{{ Form::hidden('bat_style',isset($userDetails->bat_style) ? $userDetails->bat_style :'') }}
					@else
					*/ ?>
						{{ Form::select(
							 'bat_style',
							 [null => 'Please Select Bat Style'] + $batStyle,
							 isset($userDetails->bat_style) ? $userDetails->bat_style :'',
							 ['id' => 'bat_style','class'=>'form-control chosen_select']
							) 
						}}
						<?php /*
					@endif
					*/ ?>
					<div class="error-message help-inline">
						<?php echo $errors->first('bat_style'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('bowl_style')) ? 'has-error' : ''; ?>">
				{{ Form::label('bowl_style', trans("Bowl Style"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					<?php /*
					@if($checkPlayerId == 1)
						{{ $userDetails->player_bowl_style }}
						{{ Form::hidden('bowl_style',isset($userDetails->bowl_style) ? $userDetails->bowl_style :'') }}
					@else
					*/ ?>
						{{ Form::select(
						 'bowl_style',
						 [null => 'Please Select Bowl Style'] + $bowlStyle,
						 isset($userDetails->bowl_style) ? $userDetails->bowl_style :'',
						 ['id' => 'bowl_style','class'=>'form-control chosen_select']
						) 
					}}
					<?php /*
					@endif
					*/ ?>
					
					<div class="error-message help-inline">
						<?php echo $errors->first('bowl_style'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('team')) ? 'has-error' : ''; ?>">
				{{ Form::label('team', trans("Team"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'team',
						 [null => 'Please Select Team'] + $teamList,
						 isset($userDetails->team_id) ? $userDetails->team_id :'',
						 ['id' => 'team','class'=>'form-control chosen_select']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('team'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Image</label>
				<div class="mws-form-item">
					{{ Form::file('image', array('accept' => 'image/*')) }}
					<br />
					@if(File::exists(PLAYER_IMAGE_ROOT_PATH.$userDetails->image) && !empty($userDetails->image))
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo PLAYER_IMAGE_URL.$userDetails->image; ?>">
							<div class="usermgmt_image">
								<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.PLAYER_IMAGE_URL.'/'.$userDetails->image ?>">
							</div>
						</a>
					@endif
					<div class="error-message help-inline">
						<?php echo $errors->first('image'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-6">
		    <div class="form-group <?php echo ($errors->first('description')) ? 'has-error' : ''; ?>">
				{{ Form::label('description',trans("Description"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::textarea('description',isset($userDetails->description) ? $userDetails->description :'',['class' => 'form-control','rows'=>'3','cols'=>'3']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('description'); ?>
					</div>
				</div>
			</div>
	  	</div>
	</div>
	<div class="row">
		@if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
				{{ Form::label('club', trans("Club").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'club',
						 [null => 'Please Select Club'] + $cludDetails,
						 isset($userDetails->club) ? $userDetails->club :'',
						 ['id' => '','class'=>'form-control chosen_select']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('club'); ?>
					</div>
				</div>
			</div>
		</div>
		@else
		{{ Form::hidden('club',Auth::guard('admin')->user()->id)}}
		@endif

	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="button" value="{{ trans('Update') }}" class="btn btn-danger" onclick="save_player();">
			<a href="{{URL::to('admin/edit-player/'.$userId)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/player/')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script type="text/javascript">
$('#club').on('change', function() {  
	$('#loader_img').show(); 
	var selectValue  		= $(this).val();
	var selectValueInput    = '<?php echo old('club');?>';
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:"{{  URL::route('Team.getTeamList') }}",
		'type':'post',
		data:{'club_id':selectValue,'old_club_id':selectValueInput},
		success:function(response){
			$('.put_team_dropdown').html(response);
			$('#loader_img').hide();
		}
	});
});
function save_player(){
$('#loader_img').show();
$('.help-inline').html('');
$('.help-inline').removeClass('error');
var form = $('#edit_player')[0];
var formData = new FormData(form);
$.ajax({
    url: '{{ route("Player.updatePlayer") }}',
    type:'post',
    data: formData,
	processData: false,
	contentType: false,
    success: function(r){
        error_array     =   JSON.stringify(r);
        data            =   JSON.parse(error_array);
        if(data['success'] == 1) {
			window.location.href   =  "{{ route('player.index') }}";
        }else {
            $.each(data['errors'],function(index,html){
                $("#"+index).next().addClass('error');
                $("#"+index).next().html(html);
            });
            $('#loader_img').hide();
        }
        $('#loader_img').hide();
    }
});
}
	
$('#edit_player').each(function() {
	$(this).find('input').keypress(function(e) {
	   if(e.which == 10 || e.which == 13) {
			save_player();
			return false;
	    }
	});
});
</script>
@stop

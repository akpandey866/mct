@extends('admin.layouts.default')
@section('content')
<style type="text/css">
#response {
    padding: 10px;
    margin-bottom: 10px;
    border-radius: 2px;
    display:none;
}
div#response.display-block {
    display: block;
}
.success {
    background: #c7efd9;
    border: #bbe2cd 1px solid;
}

.error {
    background: #fbcfcf;
    border: #f3c6c7 1px solid;
}
</style>
<section class="content-header">
	<h1>
		{{ trans("Import Player") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/player/')}}">{{ trans("Player") }}</a></li>
		<li class="active">{{ trans("Import Player") }} </li>
	</ol>
</section>
<section class="content"> 
	<div id="response"></div>
	<!-- <form class="form-horizontal" action="" method="post" name="uploadCSV" enctype="multipart/form-data"> -->
	{{ Form::open(['role' => 'form','url' => 'admin/player/import-player','class' => 'mws-form','files'=>'true','name'=>'frmCSVImport','id'=>'frmCSVImport']) }}
	    <div class="row">
	    	<div class="col-md-6">
	    		<div class="form-group">
	    			{{ Form::label('choose_csv_file',trans("Choose CSV File"), ['class' => 'mws-form-label']) }}
		      		<input type="file" name="file" id="file" accept=".csv">
	       		</div>
	        </div>

	    </div>
	    <div class="mws-button-row">
	    	<div class="input">
	    		<button type="submit" id="submit" name="import" class="btn btn-primary">Import</button>
	    	</div>
	    </div>
	    <div id="labelError"></div>
	{{ Form::close() }}
</section>
<script type="text/javascript">
$(document).ready(function() {
    $("#frmCSVImport").on("submit", function () {
	    $("#response").attr("class", "");
        $("#response").html("");
        var fileType = ".csv";
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
        if (!regex.test($("#file").val().toLowerCase())) {
        	    $("#response").addClass("error");
        	    $("#response").addClass("display-block");
            $("#response").html("Invalid File. Upload : <b>" + fileType + "</b> Files.");
            return false;
        }
        return true;
    });
});
</script>
@stop

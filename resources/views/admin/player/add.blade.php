@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		{{ trans("Add New Player") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/player/')}}">{{ trans("Player") }}</a></li>
		<li class="active">{{ trans("Add New Player") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/player/add-player','class' => 'mws-form','files'=>'true','id'=>'add_player']) }}
	<div class="row">
	
		      <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('first_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('first_name',trans("First Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('first_name','',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('first_name'); ?>
					</div>
				</div></div>
		  </div>
		  <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('last_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('last_name',trans("Last Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('last_name','',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('last_name'); ?>
					</div>
				</div>
			</div>
		</div>
		  
		</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('nickname')) ? 'has-error' : ''; ?>">
				{{ Form::label('nickname',trans("Nick Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('nickname','',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('nickname'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('position')) ? 'has-error' : ''; ?>">
				{{ Form::label('position', trans("Position").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'position',
						 [null => 'Please Select Position'] + $position,
						 '',
						 ['id' => 'position','class'=>'form-control chosen_select']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('position'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('category')) ? 'has-error' : ''; ?>">
				{{ Form::label('category', trans("Category").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'category',
						 [null => 'Please Select Category'] + $category,
						 '',
						 ['id' => 'category','class'=>'form-control chosen_select']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('category'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('svalue')) ? 'has-error' : ''; ?>">
				{{ Form::label('svalue', trans("Value").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'svalue',
						 [null => 'Please Select Value'] + $svalue,
						 '',
						 ['id' => 'svalue','class'=>'form-control chosen_select']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('svalue'); ?>
					</div>
				</div>
			</div>
		
		</div>
	</div>
	<div class="row">
		@if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
					{{ Form::label('club', trans("Club").' *', ['class' => 'mws-form-label'])}}
					<div class="mws-form-item">
						{{ Form::select(
							 'club',
							 [null => 'Please Select Club'] + $cludDetails,
							 '',
							 ['id' => '','class'=>'form-control chosen_select']
							) 
						}}
						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
		@else
		{{ Form::hidden('club',Auth::guard('admin')->user()->id)}}
		@endif
	</div>
	<div class="row">
		<div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Image *</label>
				<div class="mws-form-item">
					{{ Form::file('image', array('accept' => 'image/*','id' => 'image')) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('image'); ?>
					</div>
				</div>
			</div>	
		  </div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="button" value="{{ trans('Save') }}" class="btn btn-danger" onclick="save_player();">
			<a href="{{URL::to('admin/add-player/')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/player/')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
</section>
<script type="text/javascript">
	$('#club').on('change', function() {  
		$('#loader_img').show(); 
		var selectValue  		= $(this).val();
		var selectValueInput    = '<?php echo old('club');?>';
		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:"{{  URL::route('Team.getTeamList') }}",
			'type':'post',
			data:{'club_id':selectValue,'old_club_id':selectValueInput},
			success:function(response){
				$('.put_team_dropdown').html(response);
				$('#loader_img').hide();
			}
		});
	});

	function save_player(){
        $('#loader_img').show();
        $('.help-inline').html('');
        $('.help-inline').removeClass('error');
		var form = $('#add_player')[0];
		var formData = new FormData(form);
        $.ajax({
            url: '{{ route("Player.savePlayer") }}',
            type:'post',
            data: formData,
			processData: false,
			contentType: false,
            success: function(r){
                error_array     =   JSON.stringify(r);
                data            =   JSON.parse(error_array);
                if(data['success'] == 1) {
					show_message(data['message'],"success");
                    window.location.href   =  "{{ route('player.index') }}";
                }else {
                    $.each(data['errors'],function(index,html){
                        $("#"+index).next().addClass('error');
                        $("#"+index).next().html(html);
                    });
                    $('#loader_img').hide();
                }
                $('#loader_img').hide();
            }
        });
    }
	
	$('#add_player').each(function() {
		$(this).find('input').keypress(function(e) {
	       if(e.which == 10 || e.which == 13) {
				save_club();
				return false;
	        }
	    });
	});
</script>
@stop

<table class="table table-hover">
    <tbody>
        <tr>
            <th>Players</th>
            <th>RS</th>
            <th>4S</th>
            <th>6S</th>
            <th>OVRS</th>
            <th>MDNS</th>
            <th>WKS</th>
            <th>CS</th>
            <th>CWKS</th>
            <th>STS</th>
            <th>RODS</th>
            <th>ROAS</th>
            <th>DKS</th>
            <th>HT</th>
            <th>FP</th>
        </tr>
        <tr>
            <?php if(!empty($playerScoreSum)){ ?>
                <td>{{!empty($playerScoreSum->player_name) ? $playerScoreSum->player_name :'-'}}</td>
                <td>{{!empty($playerScoreSum->runs) ? $playerScoreSum->runs :'-'}}</td>
                <td>{{!empty($playerScoreSum->fours) ? $playerScoreSum->fours :'-'}}</td>
                <td>{{!empty($playerScoreSum->sixes) ? $playerScoreSum->sixes :'-'}}</td>
                <td>{{!empty($playerScoreSum->overs) ? $playerScoreSum->overs :'-'}}</td>
                <td>{{!empty($playerScoreSum->mdns) ? $playerScoreSum->mdns :'-'}}</td>
                <td>{{!empty($playerScoreSum->wks) ? $playerScoreSum->wks :'-'}}</td>
                <td>{{!empty($playerScoreSum->cs) ? $playerScoreSum->cs :'-'}}</td>
                <td>{{!empty($playerScoreSum->cwks) ? $playerScoreSum->cwks :'-'}}</td>
                <td>{{!empty($playerScoreSum->sts) ? $playerScoreSum->sts :'-'}}</td>
                <td>{{!empty($playerScoreSum->rods) ? $playerScoreSum->rods :'-'}}</td>
                <td>{{!empty($playerScoreSum->roas) ? $playerScoreSum->roas :'-'}}</td>
                <td>{{!empty($playerScoreSum->dks) ? $playerScoreSum->dks :'-'}}</td>
                <td>{{!empty($playerScoreSum->hattrick) ? $playerScoreSum->hattrick :'-'}}</td>
                <td>{{!empty($playerScoreSum->fantasy_points) ? $playerScoreSum->fantasy_points :'-'}}</td>
            <?php } ?>
        </tr>
    </tbody>
</table>
@extends('admin.layouts.default')
@section('content')
<script type="text/javascript" src="{{asset('js/admin/ckeditor/ckeditor.js')}}"></script>
<section class="content-header">
	<h1>
		{{ trans("Edit Team Slider") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/team-slider')}}">{{ trans("Team Slider") }}</a></li>
		<li class="active">{{ trans("Edit Team Slider") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/team-slider/edit-team-slider','class' => 'mws-form','files'=>'true']) }}
	{{ Form::hidden('id',$details->id) }}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Image</label>
				<div class="mws-form-item">
					{{ Form::file('image', array('accept' => 'image/*')) }}
					<br />
					
					@if(File::exists(TEAM_SLIDER_IMAGE_ROOT_PATH.$details->image))
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo USER_SLIDER_IMAGE_URL.$details->image; ?>">
							<div class="usermgmt_image">
								<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.TEAM_SLIDER_IMAGE_URL.'/'.$details->image ?>">
							</div>
						</a>
					@endif
					<div class="error-message help-inline">
						<?php echo $errors->first('image'); ?>
					</div>
				</div>
			</div>
	 	</div>
	 	<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				{{ Form::label('name', trans("Name"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('name',isset($details->name) ? $details->name :'', ['class' => 'form-control ','id'=>'name']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('position')) ? 'has-error' : ''; ?>">
				{{ Form::label('position', trans("Position"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('position',isset($details->position) ? $details->position :'', ['class' => 'form-control ','id'=>'position']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('position'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('description')) ? 'has-error' : ''; ?>">
				{{ Form::label('description', trans("Description"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::textarea('description',isset($details->description) ? $details->description :'', ['class' => 'form-control ','id'=>'description',"rows"=>3,"cols"=>3]) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('description'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				{{ Form::label('facebook', trans("Facebook URL"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('facebook',isset($details->facebook) ? $details->facebook :'', ['class' => 'form-control ','id'=>'facebook']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('facebook'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				{{ Form::label('twitter', trans("Twitter URL"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('twitter',isset($details->twitter) ? $details->twitter :'', ['class' => 'form-control','id'=>'twitter']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('twitter'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('instagram')) ? 'has-error' : ''; ?>">
				{{ Form::label('instagram', trans("Instagram URL"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('instagram',isset($details->instagram) ? $details->instagram :'', ['class' => 'form-control ','id'=>'instagram']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('instagram'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('linkedin')) ? 'has-error' : ''; ?>">
				{{ Form::label('linkedin', trans("Linkedin URL"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('linkedin',isset($details->linkedin) ? $details->linkedin :'', ['class' => 'form-control','id'=>'linkedin']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('linkedin'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Update') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/team-slider/edit-team-slider/'.$details->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/team-slider')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
@stop

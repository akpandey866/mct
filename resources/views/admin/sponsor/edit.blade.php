@extends('admin.layouts.default')
@section('content')
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<script src="{{ asset('js/admin/bootstrap-timepicker.min.js') }}"></script> 
 <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-timepicker.min.css') }}">
<section class="content-header">
	<h1>
		{{ trans("Edit Sponsor") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/sponsor')}}">{{ trans("Sponsor") }}</a></li>
		<li class="active">{{ trans("Edit Sponsor") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/sponsor/edit-sponsor','class' => 'mws-form','files'=>'true']) }}
	{{ Form::hidden('id',$details->id) }}
	@if(Auth::guard('admin')->user()->user_role_id == 1)
		<div class="row">
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('mode')) ? 'has-error' : ''; ?>">
					{{ Form::label('mode', trans("Game Mode"), ['class' => 'mws-form-label'])}}
					<div class="mws-form-item">
						  {{ Form::select('mode',[null => 'Please Select Mode'] +Config::get('home_club'),isset($gameMode) ? $gameMode :'',['id' => 'mode','class'=>'form-control'])}}
						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
					{{ Form::label('club', trans("Club"), ['class' => 'mws-form-label'])}}
					<div class="mws-form-item put_club_html">
						  {{ Form::select('club',[null => 'Please Select Club'],isset($details->user_id) ? $details->user_id :'',['id' => 'club','class'=>'form-control'])}}
						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Sponsor Logo</label>
				<div class="mws-form-item">
					{{ Form::file('logo', array('accept' => 'image/*')) }}
					<br />
					
					@if(File::exists(SPONSOR_IMAGE_ROOT_PATH.$details->logo))
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo SPONSOR_IMAGE_URL.$details->logo; ?>">
							<div class="usermgmt_image">
								<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.SPONSOR_IMAGE_URL.'/'.$details->logo ?>">
							</div>
						</a>
					@endif
					<div class="error-message help-inline">
						<?php echo $errors->first('club_logo'); ?>
					</div>
				</div>
			</div>
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				{{ Form::label('name', trans("Sponsor Name"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('name',isset($details->name) ? $details->name :'', ['class' => 'form-control ','placeholder'=>'Sponsor Name','id'=>'name']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('website')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Website</label>
				<div class="mws-form-item">
					{{ Form::text('website',isset($details->website) ? $details->website :'', ['class' => 'form-control ','id'=>'website']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('website'); ?>
					</div>
				</div>
			</div>	
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('facebook')) ? 'has-error' : ''; ?>">
				{{ Form::label('facebook', trans("Facebook URL"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('facebook',isset($details->facebook) ? $details->facebook :'', ['class' => 'form-control ','id'=>'facebook']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('facebook'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('twitter')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Twitter URL</label>
				<div class="mws-form-item">
					{{ Form::text('twitter',isset($details->twitter) ? $details->twitter :'', ['class' => 'form-control ','id'=>'twitter']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('twitter'); ?>
					</div>
				</div>
			</div>	
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('instagram')) ? 'has-error' : ''; ?>">
				{{ Form::label('instagram', trans("Instagram URL"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('instagram',isset($details->instagram) ? $details->instagram :'', ['class' => 'form-control ','id'=>'instagram']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('instagram'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('offer')) ? 'has-error' : ''; ?>">
				{{ Form::label('offer', trans("Offer"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('offer',isset($details->offer) ? $details->offer :'', ['class' => 'form-control ','id'=>'offer']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('offer'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('about')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">About</label>
				<div class="mws-form-item">
					{{ Form::textarea('about',isset($details->about) ? $details->about :'', ['class' => 'form-control ','id'=>'about','rows'=>2]) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('about'); ?>
					</div>
				</div>
			</div>	
		</div>
	</div>







	<!-- <div class="row">
		<div class="col-md-6">
			<div class="form-group <?php //echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
				{{ Form::label('club_type', trans("Club Type"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'club_type',
						 [null => 'Please Select Club'] +$clubLists,
						 isset($details->club_type) ? $details->club_type :'',
						 ['id' => 'club_type','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php //echo $errors->first('club_type'); ?>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Update') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/sponsor/edit-sponsor/'.$details->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/sponsor')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script type="text/javascript">

$("#mode").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-listing') }}",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_club_html').html(response);
			$('#loader_img').hide();
		}
	});
});

var mode = '<?php echo !empty($gameMode) ? $gameMode : 0  ?>';
var club = '<?php echo !empty($details->user_id) ? $details->user_id : 0  ?>';
if(mode !=''){ 
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-listing') }}",
		'type':'post',
		data:{'id':mode,'club_id':club},
		async : false,
		success:function(response){
			$('.put_club_html').html(response);
			$('#loader_img').hide();
		}
	});
}

$("#club").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-playerlist') }}",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});
</script>
@stop

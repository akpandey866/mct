@extends('admin.layouts.default')
@section('content') 
<section class="content-header">
	<h1>
		{{ trans("Add New Sponsor") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/sponsor')}}">{{ trans("Sponsor") }}</a></li>
		<li class="active">{{ trans("Add New Sponsor") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/sponsor/add-sponsor','class' => 'mws-form','files'=>'true']) }}

	@if(Auth::guard('admin')->user()->user_role_id == 1)
		<div class="row">
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('mode')) ? 'has-error' : ''; ?>">
					{{ Form::label('mode', trans("Game Mode"), ['class' => 'mws-form-label'])}}
					<div class="mws-form-item">
						  {{ Form::select('mode',[null => 'Please Select Mode'] +Config::get('home_club'),'',['id' => 'mode','class'=>'form-control'])}}
						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
					{{ Form::label('club', trans("Club"), ['class' => 'mws-form-label'])}}
					<div class="mws-form-item put_club_html">
						  {{ Form::select('club',[null => 'Please Select Club'],'',['id' => 'get_player','class'=>'form-control'])}}
						<div class="error-message help-inline">
							<?php echo $errors->first('club'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif




	<div class="row">
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('logo')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Sponsor logo *</label>
				<div class="mws-form-item">
					{{ Form::file('logo', array('accept' => 'image/*')) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('logo'); ?>
					</div>
				</div>
			</div>	
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				{{ Form::label('name', trans("Name").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('name','', ['class' => 'form-control ','id'=>'name']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('website')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Website</label>
				<div class="mws-form-item">
					{{ Form::text('website','', ['class' => 'form-control ','id'=>'website']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('website'); ?>
					</div>
				</div>
			</div>	
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('facebook')) ? 'has-error' : ''; ?>">
				{{ Form::label('facebook', trans("Facebook URL"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('facebook','', ['class' => 'form-control ','id'=>'facebook']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('facebook'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('twitter')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Twitter URL</label>
				<div class="mws-form-item">
					{{ Form::text('twitter','', ['class' => 'form-control ','id'=>'twitter']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('twitter'); ?>
					</div>
				</div>
			</div>	
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('instagram')) ? 'has-error' : ''; ?>">
				{{ Form::label('instagram', trans("Instagram URL"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('instagram','', ['class' => 'form-control ','id'=>'instagram']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('instagram'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		@if(Auth::guard('admin')->user()->user_role_id == ADMIN_ID)
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('offer')) ? 'has-error' : ''; ?>">
				{{ Form::label('offer', trans("Offer"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('offer','', ['class' => 'form-control ','id'=>'offer']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('offer'); ?>
					</div>
				</div>
			</div>
		</div>
        @endif
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('about')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">About</label>
				<div class="mws-form-item">
					{{ Form::textarea('about','', ['class' => 'form-control ','id'=>'about','rows'=>2]) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('about'); ?>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<!-- <div class="row">
		<div class="col-md-6">
			<div class="form-group <?php //echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
				{{ Form::label('club_type', trans("Club Type"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'club_type',
						 [null => 'Please Select Club'] +$clubLists,
						 '',
						 ['id' => 'club_type','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php //echo $errors->first('club_type'); ?>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/sponsor/add-sponsor/')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/sponsor/')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script type="text/javascript">
$("#mode").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-listing') }}",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_club_html').html(response);
			$('#loader_img').hide();
		}
	});
});
$(document).on("change",'#get_club_name',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-playerlist') }}",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});

</script>
@stop

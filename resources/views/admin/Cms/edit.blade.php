@extends('admin.layouts.default')
@section('content')
<!-- CKeditor start here-->
{{ Html::script('js/admin/ckeditor/ckeditor.js') }}
<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		{{ trans("Edit Cms") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/cms-manager')}}">Cms Pages</a></li>
		<li class="active">Edit Cms</li>
	</ol>
</section>

<section class="content"> 
{{ Form::open(['role' => 'form','url' => 'admin/cms-manager/edit-cms/'.$result->id,'class' => 'mws-form']) }}
	<div class="row">
		<div class="col-md-6">	
			<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
				<div class="mws-form-row">
					{{ Form::label('name',trans("Page Name").' *', ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						{{ Form::text('name',$result->name, ['class' => 'form-control']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('name'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">	
			<div class="form-group <?php echo ($errors->first('title')?'has-error':''); ?>">
				<div class="mws-form-row">
					{{ Form::label('title',trans("Page title").' *', ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						{{ Form::text('title',$result->title, ['class' => 'form-control']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('title'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
		<div class="form-group <?php echo ($errors->first('title')?'has-error':''); ?>">
			<div class="mws-form-row">
				{{ Form::label('meta_title',trans("Meta title").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('meta_title',$result->meta_title, ['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('meta_title'); ?>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('meta_description')?'has-error':''); ?>">
				<div class="mws-form-row">
					{{ Form::label('meta_description',trans("Meta Description").' *', ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						{{ Form::text('meta_description',$result->meta_description, ['class' => 'form-control']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('meta_description'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('meta_keywords')?'has-error':''); ?>">
				<div class="mws-form-row">
					{{ Form::label('meta_keywords',trans("Meta Keywords").' *', ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						{{ Form::text('meta_keywords',$result->meta_keywords, ['class' => 'form-control']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('meta_keywords'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<div class="form-group <?php echo ($errors->first('body')?'has-error':''); ?>">
			<div class="mws-form-row">
				{{ Form::label('body',trans("Description").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
				{{ Form::textarea("body",$result->body, ['class' => 'form-control textarea_resize','id' => 'body' ,"rows"=>3,"cols"=>3]) }}
					<script type="text/javascript">
					/* For CKEDITOR */
						
						CKEDITOR.replace( <?php echo 'body'; ?>,
						{
							height: 250,
							width: 507,
							filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
							filebrowserImageWindowWidth : '640',
							filebrowserImageWindowHeight : '480',
							enterMode : CKEDITOR.ENTER_BR
						});
							
					</script>
					<div class="error-message help-inline">
						<?php echo $errors->first('body'); ?>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="mws-button-row">
				<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
				
				<a href="{{URL::to('admin/cms-manager/edit-cms',$result->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans('Reset')  }}</a>
				
				<a href="{{URL::to('admin/cms-manager')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
			</div>
		</div>
	</div>
{{ Form::close() }} 
</section>
<style>
	.textarea_resize {
		resize: vertical;
	}
</style>
@stop

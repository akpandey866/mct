<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{{Config::get("Site.title")}}}</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.ico')}}">
        <!-- Script -->
        <script src="{{ asset('js/admin/jquery.min.js') }}"></script>
        <link href="{{ asset('css/admin/bootstrap.min.css') }}" rel="stylesheet">
        <script src="{{ asset('js/admin/bootstrap.min.js') }}"></script>    
        <link href="{{ asset('css/admin/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/ionicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/morris.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/themify-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/AdminLTE.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/custom_admin.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/bootmodel.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin/notification/jquery.toastmessage.css') }}" rel="stylesheet">
        <script src="{{ asset('css/admin/notification/jquery.toastmessage.js') }}"></script>
        <script src="{{ asset('js/admin/jquery.equalheights.js') }}"></script>
        <link href="{{ asset('css/admin/chosen.min.css') }}" rel="stylesheet">
        <script src="{{ asset('js/admin/chosen.jquery.min.js') }}"></script>
    </head>
    <body class="skin-black">
        <header class="header">
            @if(Auth::guard('admin')->user()->user_role_id == 1)
                <a href="{{URL::to('admin/dashboard')}}" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining --> 
            @else
                <a href="{{URL::to('admin/club-dashboard')}}" class="logo">
            @endif
                    <img src="{{ asset('img/logo.png')}}">
                    {{-- Config::get("Site.title") --}}
                </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button--> 
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
                <div class="navbar-right">

                    <ul class="nav navbar-nav">
                        <?php $authuser = Auth::guard('admin')->user(); ?>
                        @if($authuser->user_role_id !=1)
                        <li><span class="label label-info" style="margin: 3px 3px;"><b>{{$authuser->game_name}}</b></span></li>
                        @if(!empty($authuser->game_mode))
                          
                             <li><span class="label label-success" style="margin: 3px 3px;"><b>{{Config::get('home_club')[$authuser->game_mode]}}</b></span></li>
                        @endif
                       
                         <li>
                            @if($authuser->user_role_id ==1)
                                @if($authuser->is_game_activate == 1)
                                    <span class="label label-success" style="margin: 3px 3px;"><b>Active</b></span>
                                @else
                                    <span class="label label-danger" style="margin: 3px 3px;"><b>Inactive</b></span>
                                @endif

                            @endif
                             @if($authuser->user_role_id ==CLUBUSER)
                                @if($authuser->is_game_activate == 1)
                                    <span class="label label-success" style="margin: 3px 3px;"><b>Active</b></span>
                                @else
                                    <span class="label label-danger" style="margin: 3px 3px;"><b>Inactive</b></span>
                                @endif

                            @endif
                            @if($authuser->user_role_id == SCORER)
                                @if(get_admin_role_id() == 1)
                                    <span class="label label-success" style="margin: 3px 3px;"><b>Active</b></span>
                                @else
                                    <span class="label label-danger" style="margin: 3px 3px;"><b>Inactive</b></span>
                                @endif
                            @endif
                            </li>
                            <li>
                            @if($authuser->user_role_id == 4)
                            <span class="label label-info" style="margin: 3px 3px;"><a style="color:#ffffff;" target="_blank" href="https://myclubtap.freshdesk.com/a/solutions/articles/43000534774" > Help For Scorer</a></span>
                            @else
                            <span class="label label-info" style="margin: 3px 3px;"><a style="color:#ffffff;" target="_blank" href="https://myclubtap.freshdesk.com/support/solutions/43000364012" > Help For Admins</a></span>
                            @endif
                            
                            </li>
                        @endif
                         
         
                       
                        <li class="dropdown user user-menu">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i> <span>{{{ auth()->guard('admin')->user()->full_name}}} <i class="caret"></i></span> </a>
                            <ul class="dropdown-menu">
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left"><a class="btn btn-default btn-flat" href="{{URL::to('admin/myaccount')}}">
                                        {{ trans("Edit Profile") }} </a> 
                                    </div>
                                    <div class="pull-right"> <a class="btn btn-default btn-flat" href="{{URL::to('admin/logout')}}">
                                        {{ trans("Logout") }} </a>
                                    </div>
                                </li>
								
                            </ul>
						
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Start Main Wrapper -->
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <?php 
                $segment1   =   Request::segment(1); //admin
                $segment2   =   Request::segment(2); //url
                $segment3   =   Request::segment(3); //parameters
                $segment4   =   Request::segment(4); 
                $segment5   =   Request::segment(5); 
                 
                ?>
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <ul class='sidebar-menu'>
                        <!-- Super Admin Menu listing start here -->
                        @if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
                        <li class="{{ ($segment2 == 'dashboard') ? 'active' : '' }} "><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-home  {{ ($segment3 == 'dashboard') ? '' : '' }}"></i>{{ trans("Dashboard") }} </a></li>
                        <li class="treeview {{ in_array($segment2 ,array('club','list-grade','team','fixture','game-prizes','game-points')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('club','list-grade','team','fixture','game-prizes','game-points')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Game") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('club','list-grade','team','fixture','game-prizes','game-points')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('club','player-packs','purchase-player-packs','additional-player-logs')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='club') class="active" @endif>
                                <a href="{{URL::to('admin/club')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Basics") }} </a>
                                </li>
                                <li  @if($segment2 =='list-grade') class="active" @endif>
                                <a href="{{URL::to('admin/list-grade')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Grades") }} </a>
                                </li>
                                <li  @if($segment2 =='team') class="active" @endif>
                                <a href="{{URL::to('admin/team')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Teams") }} </a>
                                </li>
                                <li  @if($segment2 =='fixture') class="active" @endif>
                                <a href="{{URL::to('admin/fixture')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Fixtures") }} </a>
                                </li>
                                <li  @if($segment2 =='game-prizes') class="active" @endif>
                                <a href="{{URL::to('admin/game-prizes')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Prizes") }} </a>
                                </li>
                                @if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
                                <li  @if($segment2 =='game-points') class="active" @endif>
                                <a href="{{URL::to('admin/game-points')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Point System") }} </a>
                                </li>
                                @endif
                            </ul>
                        </li>
                        <li class="treeview {{ in_array($segment2 ,array('admin-verify-users','availability','admin-scorer-access','admin-edit-branding','admin-add-branding','admin-branding')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('admin-verify-users','availability','admin-scorer-access','admin-edit-branding','admin-add-branding','admin-branding')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Add-Ons") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('admin-verify-users','availability','admin-scorer-access','admin-edit-branding','admin-add-branding','admin-branding')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('club','player-packs','additional-player-logs','admin-add-branding','admin-branding')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='availability') class="active" @endif> <a href="{{URL::to('admin/availability')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Availability") }} </a>
                                </li>
                                <li  @if($segment2 =='admin-verify-users') class="active" @endif>
                                <a href="{{URL::to('admin/admin-verify-users')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Verify Users") }} </a>
                                </li>
                                <li  @if($segment2 =='admin-scorer-access') class="active" @endif>
                                <a href="{{URL::to('admin/admin-scorer-access')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Scorers") }} </a>
                                </li>
                                <li  @if($segment2 =='admin-branding' || $segment2 =='admin-edit-branding' || $segment2 =='admin-add-branding') class="active" @endif>
                                <a href="{{URL::to('admin/admin-branding')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Branding") }} </a>
                                </li>
                            </ul>
                        </li>
                         <li class="treeview {{ in_array($segment2 ,array('admin-lockout','lockout-log','admin-edit-lockout','admin-lockout-log')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('admin-lockout','lockout-log','admin-edit-lockout','admin-lockout-log')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Gameweek") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('admin-lockout','admin-lockout-log')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('admin-lockout','admin-lockout-log','admin-edit-lockout')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='admin-lockout') class="active" @endif>
                                <a href="{{URL::to('admin/admin-lockout')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Lockout") }} </a>
                                </li>
                                <li  @if($segment2 =='admin-lockout-log') class="active" @endif>
                                <a href="{{URL::to('admin/admin-lockout-log')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Gameweeks") }} </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview {{ in_array($segment2 ,array('player','additional-player-logs','player-packs')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('player','additional-player-logs','player-packs')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Players") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('player','additional-player-logs')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('club','player-packs','additional-player-logs')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='player') class="active" @endif>
                                <a href="{{URL::to('admin/player')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Players") }} </a>
                                </li>
                                <li  @if($segment2 =='player-packs') class="active" @endif>
                                <a href="{{URL::to('admin/player-packs')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Purchase Players") }} </a>
                                </li>
                                <li  @if($segment2 =='additional-player-logs') class="active" @endif>
                                <a href="{{URL::to('admin/additional-player-logs')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Purchase Log") }} </a>
                                </li>
                                <!-- <li  @if($segment2 =='availability') class="active" @endif>
                                    <a href="{{URL::to('admin/availability')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Availability") }} </a>
                                    </li> -->
                            </ul>
                        </li>
                        <li class="{{ ($segment2 == 'admin-pay-activate') ? 'active in' : '' }}">
                             <a href="{{URL::to('admin/admin-pay-activate')}}">
                            <i class="fa fa-dollar  {{ $segment2 == 'admin-pay-activate' ? '' : '' }}"></i>
                            {{'Pay & Activate'}} 
                            </a>
                        </li>
                        <li class="{{ ($segment2 == 'admin-fundraiser') ? 'active in' : '' }}">
                             <a href="{{URL::to('admin/admin-fundraiser')}}">
                            <i class="fa fa-dollar  {{ $segment2 == 'admin-fundraiser' ? '' : '' }}"></i>
                            {{'Fundraiser'}} 
                            </a>
                        </li>
                        <li class="{{ ($segment2 == 'users' && $segment3 == USER) ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/users/'.USER)}}">
                            <i class="fa fa-users {{ $segment2 == 'users' ? '' : '' }}"></i>
                            {{'Users'}} 
                            </a>
                        </li>

                        <li class="{{ ($segment2 == 'sponsor') ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/sponsor')}}">
                            <i class="fa fa-cc-diners-club {{ $segment2 == 'sponsor' ? '' : '' }}"></i> {{'MCT Partners '}} 
                            </a>
                        </li>
                        <li class="{{ ($segment2 == 'about-club') ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/about-club')}}">
                            <i class="fa fa-cc-diners-club {{ $segment2 == 'about-club' ? '' : '' }}"></i>
                            {{'MCT Features'}} 
                            </a>
                        </li>
                        <!-- About US -->
                        <li class="treeview {{ in_array($segment2 ,array('platform','serve','partner','user-slider','team-slider')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('platform','serve','team-slider')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("About MyClubtap") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('platform','serve','partner','user-slider')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('platform','partner','serve','user-slider','team-slider')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='platform' || $segment3 =='platform') class="active" @endif>
                                <a href="{{URL::to('admin/platform')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Feature Box") }} </a>
                                </li>
                                <li  @if($segment2 =='serve') class="active" @endif>
                                <a href="{{URL::to('admin/serve')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Partner Games") }} </a>
                                </li>
                                <li  @if($segment2 =='partner') class="active" @endif>
                                <a href="{{URL::to('admin/partner')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Community Partners") }} </a>
                                </li>
                                <li  @if($segment2 =='user-slider') class="active" @endif>
                                <a href="{{URL::to('admin/user-slider')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Feedback Slider") }} </a>
                                </li>
                                <li  @if($segment2 =='team-slider') class="active" @endif>
                                <a href="{{URL::to('admin/team-slider')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Our Team") }} </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview {{ in_array($segment2 ,array('slider-manager','clubs-slider')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment2 ,array('slider-manager','clubs-slider')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Sliders") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('slider-manager','clubs-slider')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment2 ,array('slider-manager','clubs-slider')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='slider-manager' && $segment3 == 'clubs-slider') class="active" @endif>
                                <a href="{{URL::to('admin/slider-manager/clubs-slider')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Signup") }} </a>
                                </li>
                                <li  @if($segment2 =='slider-manager' && $segment3 == 'fantasy-slider') class="active" @endif>
                                <a href="{{URL::to('admin/slider-manager/fantasy-slider')}}"><i class='fa fa-angle-double-right'></i>{{ trans("How to Play") }} </a>
                                </li>
                                <li  @if($segment2 =='slider-manager' && $segment3 == 'platfrom-feature') class="active" @endif>
                                <a href="{{URL::to('admin/slider-manager/platfrom-feature')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Features") }} </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview {{ in_array($segment2 ,array('dropdown-manager','food-category')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment2 ,array('dropdown-manager','bowl-style','bat-style')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("MCT Masters") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('dropdown-manager','bowl-style','bat-style')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment2 ,array('dropdown-manager','bowl-style','bat-style')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='dropdown-manager' && $segment3 == 'position') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/position')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Position") }} </a>
                                </li>
                                <li  @if($segment2 =='dropdown-manager' && $segment3 == 'club-position') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/club-position')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Admin Position") }} </a>
                                </li>

                                <li  @if($segment2 =='dropdown-manager' && $segment3 == 'club-position') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/bat-style')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Batting Style") }} </a>
                                </li>

                                 <li  @if($segment2 =='dropdown-manager' && $segment3 == 'club-position') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/bowl-style')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Bowling Style") }} </a>
                                </li>
                       
                                <li  @if($segment2 =='dropdown-manager' && $segment3 == 'svalue') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/svalue')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Fantasy Values") }} </a>
                                </li>
                                <!-- <li  @if($segment2 =='dropdown-manager' && $segment3 == 'jvalue') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/jvalue')}}"><i class='fa fa-angle-double-right'></i>{{ trans("JValue") }} </a>
                                </li> -->
                                <!-- <li  @if($segment2 =='dropdown-manager' && $segment3 == 'teamtype') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/teamtype')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Team Type") }} </a>
                                </li> -->
                                <li  @if($segment2 =='dropdown-manager' && $segment3 == 'teamcategory') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/teamcategory')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Team Category") }} </a>
                                </li>
                                <li  @if($segment2 =='dropdown-manager' && $segment3 == 'matchtype') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/matchtype')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Match Type") }} </a>
                                </li>
                                <li  @if($segment2 =='dropdown-manager' && $segment3 == 'vanue') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/vanue')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Match Venue") }} </a>
                                </li>
                                <li  @if($segment2 =='dropdown-manager' && $segment3 == 'about-us') class="active" @endif>
                                <a href="{{URL::to('admin/dropdown-manager/about-us')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Hear About Us") }} </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview {{ in_array($segment2 ,array('cms-manager','no-cms-manager','faqs-manager','categories','email-manager','email-logs','block-manager','subscriber','user-notification','news-letter','checklist')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-desktop  {{ in_array($segment2 ,array('cms-manager','email-manager','email-logs','block-manager','faqs-manager','subscriber','user-notification','news-letter','checklist')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("System Management") }} </a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('cms-manager','email-manager','faqs-manager','email-logs','block-manager','subscriber','user-notification','news-letter','checklist')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment2 ,array('cms-manager')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='cms-manager') class="active" @endif>
                                <a href="{{URL::to('admin/cms-manager')}}"><i class='fa fa-angle-double-right'></i>{{ trans("CMS") }} </a>
                                </li>
                                <li @if($segment2=='subscriber') class="active" @endif><a href="{{URL::to('admin/subscriber')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Subscriber") }} </a></li>
                                <li @if($segment2 =='email-manager') class="active" @endif ><a href="{{URL::to('admin/email-manager')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Email Templete") }} </a></li>
                                <li @if($segment2=='email-logs') class="active" @endif><a href="{{URL::to('admin/email-logs')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Email Logs") }} </a></li>

                                <li @if($segment2=='user-notification') class="active" @endif><a href="{{URL::to('admin/user-notification')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Notifications") }} </a></li>

                                <li @if($segment2=='news-letter') class="active" @endif><a href="{{URL::to('admin/news-letter/newsletter-templates')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Newsletter") }} </a></li>

                                <li @if($segment2=='checklist') class="active" @endif><a href="{{URL::to('admin/checklist')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Set-up checklist") }} </a></li>

                            </ul>
                        </li>
                        <li class="treeview {{ in_array($segment2 ,array('settings')) ? 'active in' : 'offer-reports' }} {{ in_array($segment2 ,array('language-settings')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-cogs  {{ in_array($segment2 ,array('settings')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Manage Settings")  }} </a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('settings')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment2 ,array('settings')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2=='settings' && Request::segment(4)=='Site') class="active" @endif>
                                <a href="{{URL::to('admin/settings/prefix/Site')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Site") }} </a>
                                </li>

                                <li  @if($segment2=='settings' && Request::segment(4)=='Reading') class="active" @endif>
                                <a href="{{URL::to('admin/settings/prefix/Reading')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Reading") }} </a>
                                
                                </li>
                                <li  @if($segment2=='settings' && Request::segment(4)=='Social') class="active" @endif>
                                <a href="{{URL::to('admin/settings/prefix/Social')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Social") }} </a>
                                </li> 
                            </ul>
                        </li>
                        <li class="{{ ($segment2 == 'help&support') ? 'active in' : '' }}">
                             <a href="{{URL::to('admin/help&support')}}">
                            <i class="fa fa-question  {{ $segment2 == 'help&support' ? '' : '' }}"></i>
                            {{'Help & Support'}} 
                            </a>
                        </li>
                         <li class="{{ ($segment2 == '') ? 'active in' : '' }}">
                             <a href="https://community.myclubtap.com">
                            <i class="fa fa-compass  {{ $segment2 == 'help&support' ? '' : '' }}"></i>
                            {{'MyClubtap Community'}} 
                            </a>
                        </li>
                        <li class="{{ ($segment2 == 'book-intro-sesssion') ? 'active in' : '' }}">
                             <a href="{{URL::to('admin/book-intro-sesssion')}}">
                            <i class="fa fa-book {{ $segment2 == 'book-intro-sesssion' ? '' : '' }}"></i>
                            {{'Book Intro Session'}} 
                            </a>
                        </li>
                        
                        @endif
                        <!-- Super Admin Menu listing finish here -->
                        @if(Auth::guard('admin')->user()->user_role_id == SCORER)
                        <li class="{{ ($segment2 == 'fixture' || $segment2=='team-player') ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/fixture')}}">
                            <i class="fa fa-cc-diners-club {{ $segment2 == 'fixture' ? '' : '' }}"></i>
                            {{'Manage Fixture'}} 
                            </a>
                        </li>
                         <li class="{{ ($segment2 == 'book-intro-sesssion') ? 'active in' : '' }}">
                             <a href="{{URL::to('admin/book-intro-sesssion')}}">
                            <i class="fa fa-book {{ $segment2 == 'book-intro-sesssion' ? '' : '' }}"></i>
                            {{'Book Intro Session'}} 
                            </a>
                        </li>
                        <li class="{{ ($segment2 == 'help&support') ? 'active in' : '' }}">
                             <a href="{{URL::to('admin/help&support')}}">
                            <i class="fa fa-question  {{ $segment2 == 'help&support' ? '' : '' }}"></i>
                            {{'Help & Support'}} 
                            </a>
                        </li>
                        @endif
                        <!-- Club User menu List Start Here -->
                        @if(Auth::guard('admin')->user()->user_role_id == CLUBUSER)
                        <li class="{{ ($segment2 == 'club-dashboard') ? 'active' : '' }} "><a href="{{URL::to('admin/club-dashboard')}}"><i class="fa fa-home  {{ ($segment3 == 'club-dashboard') ? '' : '' }}"></i>{{ trans("Dashboard") }} </a></li>
                        <li class="{{ ($segment2 == 'set-up-checklist') ? 'active' : '' }} "><a href="{{URL::to('admin/set-up-checklist')}}"><i class="fa fa-list  {{ ($segment3 == 'set-up-checklist') ? '' : '' }}"></i>{{ trans("Set-Up Checklist") }} </a></li>
                        <li class="treeview {{ in_array($segment2 ,array('club','list-grade','add-grade','edit-grade','team','fixture','game-prizes','game-points','game-prizes-message')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('club','list-grade','add-grade','edit-grade','team','fixture','game-prizes','game-points','game-prizes-message')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Games") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('club','list-grade','team','fixture','game-prizes','game-points')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('club','player-packs','purchase-player-packs','additional-player-logs')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='club') class="active" @endif>
                                <a href="{{URL::to('admin/club/edit-club/'.auth()->guard('admin')->user()->id)}}"><i class='fa fa-angle-double-right'></i>{{ trans("Basics") }} </a>
                                </li>
                                <li  @if($segment2 =='list-grade' || $segment2 =='add-grade'|| $segment2 =='edit-grade') class="active" @endif>
                                <a href="{{URL::to('admin/list-grade')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Grades") }} </a>
                                </li>
                                <li  @if($segment2 =='team') class="active" @endif>
                                <a href="{{URL::to('admin/team')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Teams") }} </a>
                                </li>
                                <li  @if($segment2 =='fixture') class="active" @endif>
                                <a href="{{URL::to('admin/fixture')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Fixtures") }} </a>
                                </li>
                                <li  @if($segment2 =='game-prizes') class="active" @endif>
                                <a href="{{URL::to('admin/game-prizes')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Prizes") }} </a>
                                </li>
                                <li  @if($segment2 =='game-prizes-message') class="active" @endif>
                                <a href="{{URL::to('admin/game-prizes/game-prizes-message')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Prizes Message") }} </a>
                                </li>
                                <li  @if($segment2 =='game-points') class="active" @endif>
                                <a href="{{URL::to('admin/game-points')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Point System") }} </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview {{ in_array($segment2 ,array('lockout','lockout-log')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('lockout','lockout-log')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Gameweek") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('lockout','lockout-log')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('lockout','lockout-log')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='lockout') class="active" @endif>
                                <a href="{{URL::to('admin/lockout')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Lockout") }} </a>
                                </li>
                                <li  @if($segment2 =='lockout-log') class="active" @endif>
                                <a href="{{URL::to('admin/lockout-log')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Gameweeks") }} </a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview {{ in_array($segment2 ,array('player','additional-player-logs','purchase-player-packs','player-packs')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('player','additional-player-logs','player-packs')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Players") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('player','additional-player-logs','purchase-player-packs','player-packs')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('club','player-packs','purchase-player-packs','additional-player-logs','purchase-player-packs','player-packs')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='player') class="active" @endif>
                                <a href="{{URL::to('admin/player')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Players") }} </a>
                                </li>
                                @if(Auth::guard('admin')->user()->game_mode == 1 && Auth::guard('admin')->user()->is_game_activate == 1)

                                <li  @if($segment2 =='purchase-player-packs') class="active" @endif>
                                <a href="{{URL::to('admin/purchase-player-packs')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Purchase Players") }} </a>
                                </li>
                                <li  @if($segment2 =='additional-player-logs') class="active" @endif>
                                <a href="{{URL::to('admin/additional-player-logs')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Purchase Log") }} </a>
                                </li>

                                @endif
                            </ul>
                        </li>
                        @if(auth()->guard('admin')->user()->game_mode == 1)
                        <li class="treeview {{ in_array($segment2 ,array('game-setting','fundraiser-message','fundraiser-history')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('game-setting','fundraiser-message','fundraiser-history')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Fundraiser") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('game-setting')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('game-setting','fundraiser-message','fundraiser-history')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='game-setting') class="active" @endif>
                                <a href="{{URL::to('admin/game-setting')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Settings") }} </a>
                                </li>
                                <li  @if($segment2 =='fundraiser-message') class="active" @endif>
                                    <a href="{{URL::to('admin/fundraiser-message')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Message") }} </a>
                                </li>
                                <li  @if($segment2 =='fundraiser-history') class="active" @endif>
                                    <a href="{{URL::to('admin/fundraiser-history')}}"><i class='fa fa-angle-double-right'></i>{{ trans("History") }} </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        
                        <li class="treeview {{ in_array($segment2 ,array('verify-users','availability','scorer-access', 'sponsor', 'branding')) ? 'active in' : 'offer-reports' }}">
                            <a href="javascript::void(0)"><i class="fa fa-th  {{ in_array($segment3 ,array('verify-users','availability','scorer-access', 'sponsor', 'branding')) ? '' : '' }}"></i><i class="fa pull-right fa-angle-left"></i>{{ trans("Add-Ons") }}</a>
                            <ul class="treeview-menu {{ in_array($segment2 ,array('verify-users','availability','scorer-access', 'sponsor', 'branding')) ? 'open' : 'closed' }}" style="treeview-menu {{ in_array($segment3 ,array('club','player-packs','additional-player-logs')) ? 'display:block;' : 'display:none;' }}">
                                <li  @if($segment2 =='availability') class="active" @endif>
                                <a href="{{URL::to('admin/availability')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Availability") }} </a>
                                </li>
                                <li  @if($segment2 =='verify-users') class="active" @endif>
                                <a href="{{URL::to('admin/verify-users')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Verify Users") }} </a>
                                </li>
                                <li  @if($segment2 =='scorer-access') class="active" @endif>
                                <a href="{{URL::to('admin/scorer-access')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Scorers") }} </a>
                                </li>
                                <li  @if($segment2 =='sponsor') class="active" @endif>
                                <a href="{{URL::to('admin/sponsor')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Sponsors") }} </a>
                                </li>
                                <li  @if($segment2 =='branding') class="active" @endif>
                                <a href="{{URL::to('admin/branding')}}"><i class='fa fa-angle-double-right'></i>{{ trans("Branding") }} </a>
                                </li>
                            </ul>
                        </li>

                         <li class="{{ ($segment2 == 'show-activate-game') ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/show-activate-game')}}">
                            <i class="fa fa-lock {{ $segment2 == 'show-activate-game' ? '' : '' }}"></i>
                            Pay & Activate
                            </a>
                        </li>

                        <li class="{{ ($segment2 == 'users' && $segment3 == USER) ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/users/'.USER)}}">
                            <i class="fa fa-users {{ $segment2 == 'users' ? '' : '' }}"></i>
                            {{'Users'}} 
                            </a>
                        </li>

                        <!--  <li class="{{ ($segment2 == 'scorer-access') ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/scorer-access')}}">
                                <i class="fa fa-cc-diners-club {{ $segment2 == 'about-club' ? '' : '' }}"></i>
                                {{'Scorer Access'}} 
                            </a>
                            </li>
                            <li class="{{ ($segment2 == 'verify-users') ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/verify-users')}}">
                                <i class="fa fa-cc-diners-club {{ $segment2 == 'verify-users' ? '' : '' }}"></i>
                                {{'Verify Users'}} 
                            </a>
                            </li> -->
                        <!-- <li class="{{ ($segment2 == 'sponsor') ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/sponsor')}}">
                                <i class="fa fa-cc-diners-club {{ $segment2 == 'sponsor' ? '' : '' }}"></i>{{'Manage Sponsor'}} 
                            </a>
                            </li> -->
                        @if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
                        <li class="{{ ($segment2 == 'game-points') ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/game-points')}}">
                            <i class="fa fa-cc-diners-club {{ $segment2 == 'game-points' ? '' : '' }}"></i>
                            {{'Manage Game Points'}} 
                            </a>
                        </li>
                        @endif
                        <!-- <li class="{{ ($segment2 == 'branding') ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/branding')}}">
                                <i class="fa fa-cc-diners-club {{ $segment2 == 'branding' ? '' : '' }}"></i>
                                {{'Branding'}} 
                            </a>
                            </li> -->
                        <?php /*
                            @if(auth()->guard('admin')->user()->game_mode == 1)
                            <li class="{{ ($segment2 == 'game-setting') ? 'active in' : '' }}">
                                <a href="{{URL::to('admin/game-setting')}}">
                                    <i class="fa fa-cc-diners-club {{ $segment2 == 'game-setting' ? '' : '' }}"></i>
                                    {{'Package'}} 
                                </a>
                            </li>
                            @endif
                            */ ?>
                        <!--     <li class="{{ ($segment2 == 'notifications') ? 'active in' : '' }}">
                            <a href="javascript:void();">
                                <i class="fa fa-bell {{ $segment2 == 'users' ? '' : '' }}"></i>
                                {{'Notifications'}} 
                            </a>
                            </li>-->
                        <li class="{{ ($segment2 == 'book-intro-sesssion') ? 'active in' : '' }}">
                             <a href="{{URL::to('admin/book-intro-sesssion')}}">
                            <i class="fa fa-book {{ $segment2 == 'book-intro-sesssion' ? '' : '' }}"></i>
                            {{'Book Intro Session'}} 
                            </a>
                        </li>
                        <li class="{{ ($segment2 == 'help&support') ? 'active in' : '' }}">
                             <a href="{{URL::to('admin/help&support')}}">
                            <i class="fa fa-question  {{ $segment2 == 'help&support' ? '' : '' }}"></i>
                            {{'Help & Support'}} 
                            </a>
                        </li>
                        <li class="{{ ($segment2 == 'reports') ? 'active in' : '' }}">
                            <a href="javascript:void();" onclick="alert('Reports Coming Soon. Please check back later.');">
                            <i class="fa fa-file {{ $segment2 == 'reports' ? '' : '' }}"></i>
                            {{'Reports'}} 
                            </a>
                        </li>
                        <li class="{{ ($segment2 == 'help&support') ? 'active in' : '' }}">
                             <a href="https://community.myclubtap.com">
                            <i class="fa fa-compass  {{ $segment2 == 'help&support' ? '' : '' }}"></i>
                            {{'MyClubtap Community'}} 
                            </a>
                        </li>
                        <li class="{{ ($segment2 == 'help&support') ? 'active in' : '' }}">
                             <a href="https://feedback.myclubtap.com/" target="_blank">
                            <i class="fa fa-share-alt  {{ $segment2 == 'help&support' ? '' : '' }}"></i>
                            {{'Share Feedback'}} 
                            </a>
                        </li>
                        <!-- <li class="{{ ($segment2 == 'help&support') ? 'active in' : '' }}">
                             <a href="javascript:void(0)">
                            <i class="fa fa-bell  {{ $segment2 == 'help&support' ? '' : '' }}"></i>
                            {{'Notifications'}} 
                            </a>
                        </li> -->
                       <!--  <li class="{{ ($segment4 == 'Reading') ? 'active in' : '' }}">
                            <a href="{{URL::to('admin/settings/prefix/Reading')}}">
                            <i class="fa fa-book {{ $segment2 == 'reports' ? '' : '' }}"></i>
                            {{'Manage Reading Settings'}} 
                            </a>
                        </li> -->
                        <!-- Club admin section end here -->
                        @endif     

                                        
                    </ul>

                </section>
                @if(Auth::guard('admin')->user()->user_role_id != 1)
                <div class="lobby_link">
                <span><a class="btn btn-info" href="{{ URL::to('/lobby')}}">Go to Lobby</a></span><br>
                <span>&copy;{{Config::get('Site.copyrights')}}{{date('Y',time())}}</span>
                </div>
                @endif
            </aside>
            <!-- Main Container Start -->
            <aside class="right-side">
                @if(Session::has('error'))
                <script type="text/javascript"> 
                    $(document).ready(function(e){
                        
                        show_message("{{{ Session::get('error') }}}",'error');
                    });
                </script>
                @endif
                @if(Session::has('success'))
                <script type="text/javascript"> 
                    $(document).ready(function(e){
                        show_message("{{{ Session::get('success') }}}",'success');
                    });
                </script>
                @endif
                @if(Session::has('flash_notice'))
                <script type="text/javascript"> 
                    $(document).ready(function(e){
                        show_message("{{{ Session::get('flash_notice') }}}",'success');
                    });
                </script>
                @endif
                <div id="loader_img" style="display:none">
                    <center><img src="{{WEBSITE_IMG_URL}}spin.gif" style="height: 87px; margin-top: 24%; width: 87px;"></center>
                </div>
                @yield('content')
            </aside>
        </div>
        <?php echo Config::get("Site.copyright_text"); ?>
    </body>
</html>
<script src="{{ asset('js/admin/bootbox.js') }}"></script>
<script src="{{ asset('js/admin/core/mws.js') }}"></script>
<script src="{{ asset('js/admin/core/themer.js') }}"></script>
<script src="{{ asset('js/admin/app.js') }}"></script>
<script src="{{ asset('css/admin/fancybox/jquery.fancybox.js') }}"></script>
<link href="{{ asset('css/admin/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
<link href="{{ asset('css/admin/bootmodel.css') }}" rel="stylesheet">
<script type="text/javascript">
    $(".chosen_select").chosen();
    function show_message(message,message_type) {
        $().toastmessage('showToast', { 
            text: message,
            sticky: false,
             hideAfter: 500000,
    
            position: 'top-right',
            type: message_type,
        });
    }
            
    $(function(){
        $('.fancybox').fancybox();
        $('.fancybox-buttons').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',
            prevEffect : 'none',
            nextEffect : 'none',
        });
        
        $(document).on('click', '.delete_any_item', function(e){
            e.stopImmediatePropagation();
            url = $(this).attr('href');
            bootbox.confirm("Are you sure you want to delete this ?",
            function(result){
                if(result){
                    window.location.replace(url);
                }
            });
            e.preventDefault();
        });
        
        /**
         * Function to change status
         *
         * @param null
         *
         * @return void
         */
        $(document).on('click', '.status_any_item', function(e){  
            e.stopImmediatePropagation();
            url = $(this).attr('href');
            bootbox.confirm("Are you sure you want to change status ?",
            function(result){
                if(result){
                    window.location.replace(url);
                }
            });
            e.preventDefault();
        });
        
        $('.open').parent().addClass('active');
           
    
        $('.skin-black .sidebar > .sidebar-menu > li > a').click(function(e) {
            if(!($(this).next().hasClass("open"))) { 
                $(".treeview-menu").addClass("closed");
                $(".treeview-menu").removeClass("open");
                $(".treeview-menu.open").slideUp();
                $('.skin-black .sidebar > .sidebar-menu > li').removeClass("active");
              
                $(this).next().slideDown();
                $(this).next().addClass("open");  
                $(this).parent().addClass("active"); 
                 
            }else {  
                e.stopPropagation(); 
                return false;  
            }
        }); 
        /**
         * For match height of div 
         */
        $('.items-inner').equalHeights();
    });
    
    
    $(document).ready(function () { 
        $( "select[name='country']" ).change(function () {
            var countary_id = $(this).val();
      
                $.ajax({
                    url: "{{url('admin/getstate') }}" + '/'+countary_id,
                    success: function(data) {
                        //alert(data);
                        $('#state').prop('disabled', false);
                        //console.log(data);
                        $('#state').html('');
                        $('#state').html(data);     
                    }
                });
        });
     $( "select[name='state']" ).change(function () {
            var state_id = $(this).val();
                $.ajax({
                    url: "{{url('admin/getcity') }}" + '/'+state_id,
                    success: function(data) {
                        //alert(data);
                        $('#city').prop('disabled', false);
                        //console.log(data);
                        $('#city').html('');
                        $('#city').html(data);      
                    }
                });
        });
        
        
    
    });
    
</script>
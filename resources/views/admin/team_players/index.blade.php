@extends('admin.layouts.default')
@section('content')

<section class="content-header">
	<h1>
	  {{ trans("Squad") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/fixture')}}">{{ trans("Fixture") }}</a></li>
		<li class="active">{{ trans("Squad") }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
		<div class="col-md-3 col-sm-3"><div class="form-group">  Team: {{!empty($fixtureDetails->team_name) ? $fixtureDetails->team_name:''}}</div></div>
		<div class="col-md-3 col-sm-3"><div class="form-group">  Grade: {{!empty($fixtureDetails->grade) ? $fixtureDetails->grade:'-'}}</div></div>
		<div class="col-md-3 col-sm-3"><div class="form-group">  Start Date: {{!empty($fixtureDetails->start_date) ? Date('d/m/Y',strtotime($fixtureDetails->start_date)):'-'}}</div></div>
		<div class="col-md-3 col-sm-3"><div class="form-group">  End Date: {{!empty($fixtureDetails->end_date) ? Date('d/m/Y',strtotime($fixtureDetails->end_date)):'-'}}</div></div>
	</div>
	<div class="row">
			@if($is_lock == 0)
			{{ Form::open(['role' => 'form','url' => 'admin/add-team-player','class' => 'mws-form',"method"=>"post"]) }}
			<input type="hidden" name="player_hidden_ids">
			
			<?php /*	@if($playerCount == 11)
				<div class="col-md-4 col-sm-4">
					<div class="form-group">  
						<a title="{{ trans('Lock Player') }}" href="{{ route('lockGame',$fixture_id) }}"  class="lock_game btn btn-warning">Lock Player</a>
					</div>
				</div>
				@endif */ ?>
			
			<div class="col-md-4 col-sm-3">
				<div class="form-group ">  

					<select name="player_id[]" class="form-control chosen_select"  data-placeholder="Choose Player..." multiple class="chosen-select">
						<!-- <option value="" selected=""  >Please Select Player</option> -->
						<?php  $i=0; ?>
						<?php foreach ($playerlist as $key => $value) {  ?>
							<option value="{{$key}}" <?php echo (in_array($key, $playerIds) ? "disabled":"") ?> >{{$value}}</option>
						<?php $i++; } ?>
					</select>
					<div class="error-message help-inline">
						<?php echo $errors->first('player_id'); ?>
					</div>
					<input type="hidden" name="fixture_id" value="{{$fixture_id}}">
				</div>
			</div>
			<div class="col-md-4 col-sm-5">
				<div class="form-group">  
					<button class="btn btn-success">{{trans("Add Player") }}</button>
				</div>
			</div>
			{{ Form::close() }}
			@endif
			<!-- Player locked -->

	</div> 
	<div class="box">
		<div class="box-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="5%">SN</th>
						<th width="25%">Player Name</th>
						<th width="25%">Created On</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				@if(!$result->isEmpty())
				<?php $n=1; ?>
					@foreach($result as $key => $record)  
						<tr class="items-inner">
							<td>
								{{ $n }}
							</td>
							<td>
								{{ $record->player_name }}
							</td>
							<td>
								{{ date(config::get("Reading.date_format"),strtotime($record->created_at))}}
							</td>
							<td data-th='Action'>
								@if($record->is_lock == 0)
								<a title="{{ trans('Delete') }}" href="{{ URL::to('admin/delete-team-player/'.$record->id.'/'.$record->fixture_id) }}"  class="delete_any_item btn btn-danger"><i class="fa fa-trash-o"></i></a>
								@else
								 <button type="button" class="btn btn-primary">Locked Player</button>
								@endif

							</td>
						</tr>
						<?php $n++; ?>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="4" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
<script type="text/javascript">
$(document).on('click', '.lock_game', function(e){
    e.stopImmediatePropagation();
    url = $(this).attr('href');
    bootbox.confirm("<em>Are you sure you want to lock the players.After this you can not add or delete players ?<em>",
    function(result){
        if(result){
            window.location.replace(url);
        }
    });
    e.preventDefault();
});

$(document).on('click', '.btn-success', function(e){
var x = 'tt'; 
$( ".chosen-choices li.search-choice .search-choice-close" ).each(function( index ) {
  if(x=='tt'){
   x = $(this).data('option-array-index');
  }else{
   x = x+','+$(this).data('option-array-index');
  }
  $('[name="player_hidden_ids"]').val(x);
});
});
</script>
<style type="text/css">
	.chosen-choices .search-field input{ height: 30px !important; }
	.btn.btn-success{ margin-top: 0px;  }

</style>
@stop
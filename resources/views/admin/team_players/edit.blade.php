@extends('admin.layouts.default')
@section('content')

<section class="content-header">
	<h1>
		{{ trans("Edit Team") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/player/')}}">{{ trans("Team") }}</a></li>
		<li class="active">{{ trans("Edit Team") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/team/edit-team','class' => 'mws-form','files'=>'true']) }}
	{{ Form::hidden('team_id',$userDetails->id,['class' => '']) }}
	<div class="row">
	
		      <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				{{ Form::label('name',trans("First Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('name',isset($userDetails->name) ? $userDetails->name :'',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div></div>
		  </div>
		  <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('grade_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('grade_name', trans("Grade Name").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'grade_name',
						 [null => 'Please Select Grade Name'] + $gradeName,
							isset($userDetails->grade_name) ? $userDetails->grade_name :'',
						 ['id' => 'grade_name','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('grade_name'); ?>
					</div>
				</div>
			</div>
		</div>
		  
		</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('team_category')) ? 'has-error' : ''; ?>">
				{{ Form::label('team_category', trans("Team Category").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'team_category',
						 [null => 'Please Select Team Category'] + $teamCategory,
						 isset($userDetails->team_category) ? $userDetails->team_category :'',
						 ['id' => 'team_category','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('team_category'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('type')) ? 'has-error' : ''; ?>">
				{{ Form::label('type', trans("Team Type").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'type',
						 [null => 'Please Select Team Type'] + $teamType,
						  isset($userDetails->type) ? $userDetails->type :'',
						 ['id' => 'type','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('type'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	@if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
				{{ Form::label('svalue', trans("Club").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'club',
						 [null => 'Please Select Club'] + $cludDetails,
						 isset($userDetails->club) ? $userDetails->club :'',
						 ['id' => 'club','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('club'); ?>
					</div>
				</div>
			</div>
		
		</div>
	</div>
	@else
		{{ Form::hidden('club',Auth::guard('admin')->user()->id)}}
	@endif
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Update') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/team/edit-team/'.$userId)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/team/')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
@stop

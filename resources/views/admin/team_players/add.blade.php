@extends('admin.layouts.default')
@section('content')

<section class="content-header">
	<h1>
		{{ trans("Add New Team Player") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/team/')}}">{{ trans("Team") }}</a></li>
		<li><a href="{{URL::to('admin/team-player/'.$team_id)}}">{{ trans("Team Player") }}</a></li>
		<li class="active">{{ trans("Add New Team Player") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/add-team-player','class' => 'mws-form','files'=>'true']) }}
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6">
			<div class="form-group">
				<div class="mws-form-row">
					<table class="table table-responsive table-bordered lastrow_feature_detail" width="100%">
						<tr class="add_brand0 add_more_feature_detail add_feature_detail" id="0">
							<td>
								<div class="mws-form-item add_feature_input">
									{{ Form::select('club[0]',[null => 'Please Select Club'] + $cludDetails,'',['id' => 'name0','class'=>'form-control'])}}
									<span class="help-inline"></span>
								</div>
							</td>
							<td>
								<div class="mws-form-item add_feature_input">
									{{ Form::select('player_name[0]',[null => 'Please Select Player Name'] + $playerlist,'',['id' => 'name0','class'=>'form-control'])}}
									<span class="help-inline"></span>
								</div>
							</td>
							<td><input  type="button" value="Add More" class="btn btn-primary" id="add_more_feature_detail"/></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input">
			<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/add-team-player/'.$team_id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/team-player/'.$team_id)}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
</section>
<script type="text/javascript">
$('#add_more_feature_detail').on('click', function() {
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	var get_last_id			=	$('.lastrow_feature_detail:last tr:last').attr('rel');
	if(typeof get_last_id === "undefined") {
		get_last_id			=	0;
	}
	var finalValNumber = $("#name"+get_last_id).val();
	var counter  = parseInt(get_last_id) + 1;
	if((finalValNumber != '')){ 
		$.ajax({
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url:'{{ url("admin/add-more-team-player") }}',
			'type':'post',
			data:{'counter':counter},
			success:function(response){
				$('#loader_img').hide();
				$('.add_feature_detail').last().after(response);
			}
		});
	}else{   
		$(".add_feature_input").each(function(){ 
			var dataVal = $(this).find("input,text,file,select").val();
			if(dataVal == "" || dataVal == "undefined"){
				$(this).find("input,text,select").next().addClass('error');
				$(this).find("input,text,select").next().html(" {{{ trans('This field is required.') }}} ");
			}
		});
		$('#loader_img').hide();
		return false;
	}
});
function del_feature(id){
	url = $(this).attr('href');
	bootbox.confirm("Are you sure want to remove this ?",
	function(result){
		if(result){
			$(".add_feature"+id).remove();
		}
	});
}
</script>
@stop

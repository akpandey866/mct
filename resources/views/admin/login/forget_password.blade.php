@extends('admin.layouts.login_layout')

@section('content')

<div class="form-box" id="login-box">
	<div class="header"><img style="margin-bottom: 10px; " src="{{asset('img/logo_white.png')}}"></div>
	{{ Form::open(['role' => 'form','url' => 'admin/send_password']) }}
	<div class="body bg-gray">
	<b> Forgot Password</b>
		<div class="form-group">
			{{ Form::text('email', null, ['placeholder' => 'Email','class'=>'form-control']) }}
			<div class="error-message help-inline">
				<?php echo $errors->first('email'); ?>
			</div>
		</div>
	</div>
	<div class="footer">                                                               
		<button type="submit" class="btn bg-olive btn-block">Submit</button> 
		<a class="btn bg-olive btn-block"  href="{{ URL::to('/admin')}}">Cancel</a>
	</div>
	{{ Form::close() }}
</div>
@extends('admin.layouts.login_layout')

@section('content')

<div class="form-box" id="login-box">
	
	<div class="header"><img style="margin-bottom: 10px; " src="{{asset('img/logo_white.png')}}"></div>
	{{ Form::open(['role' => 'form','url' => 'admin/login']) }}    
	<div class="body bg-gray">
	<b>Login</b>
		<div class="form-group">
			{{ Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) }}
			<div class="error-message help-inline">
				<?php echo $errors->first('email'); ?>
			</div>
		</div>
		<div class="form-group">
		   {{ Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control','autocomplete'=>false]) }}
		   <div class="error-message help-inline">
				<?php echo $errors->first('password'); ?>
			</div>
		</div>
		@if(Session::get('failed_attampt_login') >= 11)
			<div class="form-group">
				{{ Form::text('captcha', null, ['placeholder' => 'Captcha Code', 'class' => 'form-control']) }}
				<div class="error-message help-inline">
					<?php echo $errors->first('captcha'); ?>
				</div>
				{{captcha_img('flat')}}
			</div>
		@endif
		
	</div>
	<div class="footer">                                                               
		<button type="submit" class="btn bg-olive btn-block">Login</button> 
		<a href="{{ URL::to('admin/forget_password')}}">Forgot your password?</a>
		
	</div>
	<br /><a class="btn bg-olive btn-block" href="{{ URL::to('/')}}">Go to myclubtap.com</a>
	{{ Form::close() }}
</div>

@stop

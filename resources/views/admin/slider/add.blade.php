@extends('admin.layouts.default')

@section('content')
<section class="content-header">
	<h1>
		{{ 'Add New '.studly_case($type) }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{URL::to('admin/slider-manager/'.$type)}}">{{{$type}}}</a></li>
		<li class="active">{{ 'Add New '.studly_case($type) }}</li>
	</ol>
</section>

<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/slider-manager/add-slider/'.$type,'class' => 'mws-form','files' => true]) }}	
	<div class="row pad">
		<div class="col-md-6">	
			<div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
					{{  Form::label('image',trans("Image").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::file('image', array('accept' => 'image/*')) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('image'); ?>
					</div>
				</div>
			</div>
			<div class="mws-button-row">
				<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
				
				<a href="{{URL::to('admin/slider-manager/add-slider/'.$type)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans('Reset')  }}</a>
				
				<a href="{{URL::to('admin/slider-manager/'.$type)}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
			</div>
		</div>
	</div>
	{{ Form::close() }} 
</section>
<style>
	.textarea_resize {
		resize: vertical;
	}
</style>
@stop

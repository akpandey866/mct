@extends('admin.layouts.default')

@section('content')
<section class="content-header">
	<h1>
		{{ studly_case($type) }} Management
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ studly_case($type) }} Management</li>
	</ol>
</section>
	
<section class="content"> 
	<div class="row">
		<div class="col-md-12 col-sm-12 ">
			<div class="form-group pull-right">  
				<a href="{{URL::to('admin/slider-manager/add-slider/'.$type)}}" class="btn btn-success btn-small align">{{ 'Add New '.studly_case($type) }} </a>
			</div>
		</div>
	</div> 
		
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
			<thead>
				<tr>
					<th width="40%">
						Image
					</th>
					
					<th width="30%">
						{{
							link_to_route(
								'slider.listslider',
								'Created ',
								array(
									$type,
									'sortBy' => 'created_at',
									'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc'
								),
							   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)
						}}
					</th>
					<th>{{ 'Action' }}</th>
				</tr>
			</thead>
			<tbody id="powerwidgets">
				@if(!$result->isEmpty())
				@foreach($result as $record)
				<tr class="items-inner">
					<td data-th='Name'>
						@if(File::exists(SLIDER_IMAGE_ROOT_PATH.$record->image))
							<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo SLIDER_IMAGE_URL.$record->image; ?>">
								<div class="usermgmt_image">
									<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.SLIDER_IMAGE_URL.$record->image ?>">
								</div>
							</a>
						@endif
					</td>
					<td data-th='Created At'>{{ date(Config::get("Reading.date_format") , strtotime($record->created_at)) }}</td>
					<td data-th='Action'>						
						<a title="Edit" href="{{URL::to('admin/slider-manager/edit-slider/'.$record->id.'/'.$type)}}" class="btn btn-info btn-small"><span class="ti-pencil"></span></a>
						<a title="Delete" href="{{URL::to('admin/slider-manager/delete-slider/'.$record->id.'/'.$type)}}"  class="delete_any_item btn btn-danger btn-small"><span class="ti-trash"></span> </a>
					</td>
				</tr>
				 @endforeach  
			</tbody>
		</table>
		@else
			<tr>
			<td align="center" style="text-align:center;" colspan="6" > No Result Found</td>
		  </tr>			
		@endif 
	</div>
	@include('pagination.default', ['paginator' => $result])
</div>
@stop

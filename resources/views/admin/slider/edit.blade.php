@extends('admin.layouts.default')

@section('content')

<section class="content-header">
	<h1>
		{{ 'Edit '.studly_case($type) }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/slider-manager',$type)}}">Masters</a></li>
		<li class="active">{{ 'Edit '.studly_case($type) }}</li>
	</ol>
</section>
<section class="content">
	{{ Form::open(['role' => 'form','url' => 'admin/slider-manager/edit-slider/'.$slider->id.'/'.$type,'class' => 'mws-form','files' => true]) }}
	<div class="row pad">
		<div class="col-md-6">
			<div class="form-group">
				{{  Form::label('image',trans("Image").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::file('image', array('accept' => 'image/*')) }}
					@if(File::exists(SLIDER_IMAGE_ROOT_PATH.$slider->image))
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo SLIDER_IMAGE_URL.$slider->image; ?>">
							<div class="usermgmt_image">
								<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.SLIDER_IMAGE_URL.'/'.$slider->image ?>">
							</div>
						</a>
					@endif
					<div class="error-message help-inline">
						<?php echo $errors->first('image'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<input type="submit" value="{{ trans('Save') }}" class="btn btn-primary">
		<a href="{{URL::to('slider-manager/edit-slider/'.$slider->id.'/'.$type)}}" class="btn btn-danger"><i class=\"icon-refresh\"></i> Reset</a>
	</div>
	{{ Form::close() }} 
</section>
@stop

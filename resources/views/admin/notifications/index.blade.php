@extends('admin.layouts.default')

@section('content')
<section class="content-header">
	<h1>Notification Management</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">Notification Management</li>
	</ol>
</section>
	
<section class="content"> 
	<div class="row">
		<div class="col-md-12 col-sm-12 ">
			<div class="form-group pull-right">  
				<a href="{{route('notification.addNotificaiton')}}" class="btn btn-success btn-small align">{{ 'Add New notification' }} </a>
			</div>
		</div>
	</div> 
		
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
			<thead>
				<tr>
					<th>Body</th>
					<th>
						{{
							link_to_route(
								"notification.listing",
								trans("Status"),
								array(
									'id'=>isset($user_role_id)?$user_role_id:'',
									'sortBy' => 'is_active',
									'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
							   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)
						}}
					</th>
					<th>
						{{
							link_to_route(
								'notification.listing',
								'Created ',
								array(
									'sortBy' => 'created_at',
									'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc'
								),
							   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)
						}}
					</th>
					<th>{{ 'Action' }}</th>
				</tr>
			</thead>
			<tbody id="powerwidgets">
				@if(!$result->isEmpty())
				@foreach($result as $record)
				<tr class="items-inner">
					<td data-th='Body'>
						{!! Str::limit($record->body,20) !!}
					</td>
					<td>
						@if($record->is_active	==1)
							<span class="label label-success" >{{ trans("Activated") }}</span>
						@else
							<span class="label label-warning" >{{ trans("Deactivated") }}</span>
						@endif
						@if($record->status==1)
							<span class="label label-success" >{{ trans("Draft") }}</span>
						@endif
					</td>
					<td data-th='Created At'>{{ date(Config::get("Reading.date_format") , strtotime($record->created_at)) }}</td>
					<td data-th='Action'>						
						<a title="Edit" href="{{route('notification.editNotification',$record->id)}}" class="btn btn-info btn-small"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						<a title="Delete" href="{{route('notification.deleteNotification',$record->id)}}"  class="delete_any_item btn btn-danger btn-small"><i class="fa fa-trash" aria-hidden="true"></i></a>
						<a title="Send as  email" href="{{route('notification.sendEmail',$record->id)}}" class="btn btn-info btn-small"><i class="fa fa-envelope" aria-hidden="true"></i></a>
						<a title="Push as Notification" href="{{route('notification.editNotification',$record->id)}}" class="btn btn-info btn-small"><i class="fa fa-bell" aria-hidden="true"></i></a>
					</td>
				</tr>
				 @endforeach  
			</tbody>
		</table>
		@else
			<tr>
			<td align="center" style="text-align:center;" colspan="6" > No Result Found</td>
		  </tr>			
		@endif 
	</div>
	@include('pagination.default', ['paginator' => $result])
</div>
@stop

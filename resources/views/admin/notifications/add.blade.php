@extends('admin.layouts.default')
@section('content')
{{ Html::script('js/admin/ckeditor/ckeditor.js') }}
<section class="content-header">
	<h1>Add New Notification</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{route('notification.listing')}}">Notification</a></li>
		<li class="active">Add New Notification</li>
	</ol>
</section>

<section class="content"> 
	{{ Form::open(['role' => 'form','route' => 'notification.addNotificaiton','class' => 'mws-form','files' => true]) }}	
	<div class="row pad">
		<div class="col-md-6">	
			<div class="form-group <?php echo ($errors->first('body')) ? 'has-error' : ''; ?>">
					{{  Form::label('body',trans("Body").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::textarea('body','',['clas'=>'form-control','id' => 'body']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('body'); ?>
					</div>
				</div>
			</div>
			<div class="mws-button-row">
				<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
				<a href="{{route('notification.addNotificaiton')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans('Reset')  }}</a>
				<a href="{{route('notification.addNotificaiton')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
			</div>
		</div>
	</div>
	{{ Form::close() }} 
</section>
<script type="text/javascript">
/* For CKEDITOR */
	CKEDITOR.replace( 'body',
	{
		height: 350,
		width: 507,
		filebrowserUploadUrl : '<?php echo URL::to('admin/base/uploder'); ?>',
		filebrowserImageWindowWidth : '640',
		filebrowserImageWindowHeight : '480',
		enterMode : CKEDITOR.ENTER_BR
	});
</script>
@stop

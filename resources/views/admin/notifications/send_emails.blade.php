@extends('admin.layouts.default')
@section('content')
{{ Html::script('js/admin/ckeditor/ckeditor.js') }}
<style type="text/css">
	.hideCheckbox{
		display: none;
	}
	.chosen-choices .search-field input{ height: 30px !important; }
	.btn.btn-success{ margin-top: 0px;  }
</style>

<section class="content-header">
	<h1>Send New Email</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
		<li><a href="{{route('notification.listing')}}">Notification</a></li>
		<li class="active">Send New Email</li>
	</ol>
</section>

<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/user-notification/send-email/'.$notificationId,'class' => 'mws-form','files' => true]) }}	
	<input type="hidden" name="notification_id" value="{{!empty($notificationId) ? $notificationId :''}}">
	<div class="row box">
		<div class="col-md-12 box-footer">
				{{  Form::label('body',trans("Body"), ['class' => 'mws-form-label']) }}<br>
			{!! $notificationsDescription !!}
			
		</div>
	</div>
	<div class="row pad">
		<div class="col-md-6" style="margin-bottom: 100px;">
			<span class="custom_check"><label for="btnCheck1">Send single user</label> &nbsp; <input type="checkbox" name="single_user" id="btnCheck1" value="1" /><span class="check_indicator">&nbsp;</span></span>
			<div class="form-group ">  
				<select name="player_id[]" class="form-control chosen_select"  data-placeholder="Choose User..." multiple class="chosen-select">
					<!-- <option value="" selected=""  >Please Select Player</option> -->
					<?php foreach ($userList as $key => $value) {  ?>
					 <option value="{{$key}}">{{$value}}</option>
					<?php } ?>
				</select>
				<div class="error-message help-inline">
					<?php echo $errors->first('player_id'); ?>
				</div>
				
			</div>
			
		</div>
		<div class="col-md-6">
			<span class="custom_check"><label for="btnCheck2">Send all game admin</label> &nbsp; <input type="checkbox" name="game_admin" id="btnCheck2" value="2" /><span class="check_indicator">&nbsp;</span></span>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">	
			<div class="mws-button-row">
				<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
				<a href="{{route('notification.addNotificaiton')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans('Reset')  }}</a>
				<a href="{{route('notification.addNotificaiton')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
			</div>
		</div>
	</div>
	{{ Form::close() }} 
</section>
<script type="text/javascript">
	 $('#btnCheck1').on('click',function(){
        if($(this). prop("checked") == true){
            $("#btnCheck2"). prop("checked", false);
            //$(".uploadDiv").removeClass("hideCheckbox");
            //$(".imageDiv").addClass("hideCheckbox");
        }
    });
    $('#btnCheck2').on('click',function(){
        if($(this). prop("checked") == true){
            $("#btnCheck1"). prop("checked", false);
           // $(".imageDiv").removeClass("hideCheckbox");
           // $(".uploadDiv").addClass("hideCheckbox");
        }
    });
</script>
@stop

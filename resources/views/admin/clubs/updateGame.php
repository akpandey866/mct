@extends('admin.layouts.default')
@section('content')
<script src="{{ asset('js/bootstrap-dropdown.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<section class="content-header">
	<h1>
		{{ trans("Edit Club") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/club')}}">{{ trans("Clubs") }}</a></li>
		<li class="active">{{ trans("Edit Club") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/club/edit-club/','class' => 'mws-form','files'=>'true', 'id' => 'edit_club']) }}
	{{ Form::hidden('user_id',$userDetails->id,['class' => '']) }}
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">CLUB USER</h3>
            </div>
           <div class="box-body">
		   <div class="row">
		      <div class="col-md-6">
			  <div class="form-group <?php echo ($errors->first('club_type')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">Club Type *</label>
				<div class="mws-form-item">
					{{ Form::select(
						 'club_type',
						 [null => 'Please Select Club Type'] + Config::get('home_club'),
						 isset($userDetails->club_type) ? $userDetails->club_type :'',
						 ['id' => 'club_type','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('club_type'); ?>
					</div>
				</div>
			</div>
			
                
			 </div>	
			  <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('club_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('club_name',trans("Club Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('club_name',isset($userDetails->club_name) ? $userDetails->club_name :'',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('club_name'); ?>
					</div>
				</div>
			</div>
			</div>
          </div>
		  <div class="row">
		      <div class="col-md-6">
                
			<div class="form-group <?php echo ($errors->first('game_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('game_name',trans("Game Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('game_name',isset($userDetails->game_name) ? $userDetails->game_name :'',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('game_name'); ?>
					</div>
				</div></div>
		  </div>
		  <div class="col-md-6">
                
			<div class="form-group <?php echo ($errors->first('club_logo')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Club logo</label>
				<!--<span class='tooltipHelp' title="" data-html="true" data-toggle="tooltip"  data-original-title="<?php echo "The attachment must be a file of type:".IMAGE_EXTENSION; ?>" style="cursor:pointer;">
					<i class="fa fa-question-circle fa-2x"> </i>
				</span>-->
				<div class="mws-form-item">
					{{ Form::file('club_logo', array('accept' => 'image/*')) }}
					<br />
					
					@if(File::exists(CLUB_IMAGE_ROOT_PATH.$userDetails->club_logo))
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo CLUB_IMAGE_URL.$userDetails->club_logo; ?>">
							<div class="usermgmt_image">
								<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.CLUB_IMAGE_URL.'/'.$userDetails->club_logo ?>">
							</div>
						</a>
					@endif
					<div class="error-message help-inline">
						<?php echo $errors->first('club_logo'); ?>
						<div class="error-message help-inline">
						<?php echo $errors->first('club_logo'); ?>
					</div>
					</div>
				</div>
			</div>
		  </div>
		</div>
	</div>
	</div>
	</div>
	</div>
	
	
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Club Details</h3>
            </div>
           <div class="box-body">
		   <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('sport_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('sport_name',trans("Sport Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('sport_name',isset($userDetails->sport_name) ? $userDetails->sport_name :'',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('sport_name'); ?>
					</div>
				</div>
			</div>
			 </div>	
			  <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('country')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">Country</label>
				<div class="mws-form-item">
					{{ Form::select(
						 'country',
						 [null => 'Please Select Country'] + $countryList,
						isset($userDetails->country) ? $userDetails->country :'',
						 ['id' => 'country','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('country'); ?>
					</div>
				</div>
			</div>
			</div>
          </div>
		  <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('state')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">State</label>
				<div class="mws-form-item">
					{{ Form::select(
						 'state',
						 [null => 'Please Select State'],
						 isset($userDetails->state) ? $userDetails->state :'',
						 ['id' => 'state','class'=>'form-control','disabled'=>'true']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('state'); ?>
					</div>
				</div>
			</div>
		  </div>
		  <div class="col-md-6">
		     <div class="form-group <?php echo ($errors->first('city')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">City</label>
				<div class="mws-form-item">
					{{ Form::select(
						 'city',
						 [null => 'Please Select city'],
						isset($userDetails->city) ? $userDetails->city :'',
						 ['id' => 'city','class'=>'form-control','disabled'=>'true']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('city'); ?>
					</div>
				</div>
			</div>
		  </div>
		</div>
		<div class="row">
		      <div class="col-md-6">
			     <div class="form-group <?php echo ($errors->first('post_code')) ? 'has-error' : ''; ?>">
				{{ Form::label('post_code',trans("Post Code"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('post_code',isset($userDetails->post_code) ? $userDetails->post_code :'',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('post_code'); ?>
					</div>
				</div>
			</div>
			  </div>
		</div>	  
			  
	</div>
	</div>
	</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Club User Details</h3>
            </div>
           <div class="box-body">
		   <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('first_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('first_name',trans("First Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('first_name',isset($userDetails->first_name) ? $userDetails->first_name :'',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('first_name'); ?>
					</div>
				</div>
			</div>
			 </div>	
			  <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('last_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('last_name',trans("Last Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('last_name',isset($userDetails->last_name) ? $userDetails->last_name :'',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('last_name'); ?>
					</div>
				</div>
			</div>
			</div>
          </div>
		  <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('email')) ? 'has-error' : ''; ?>">
				{{ Form::label('email',trans("Email").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('email',isset($userDetails->email) ? $userDetails->email :'',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('email'); ?>
					</div>
				</div>
			</div>
		  </div>
		  <div class="col-md-6">
		     <div class="form-group <?php echo ($errors->first('phone')) ? 'has-error' : ''; ?>">
				{{ Form::label('phone',trans("Phone").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('phone',isset($userDetails->phone) ? $userDetails->phone :'',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('phone'); ?>
					</div>
				</div>
			</div>
		  </div>
		</div>
		<div class="row">
		      <div class="col-md-6">
			     <div class="form-group <?php echo ($errors->first('dob')) ? 'has-error' : ''; ?>">
				<label for="dob" class="mws-form-label">Date of Birth </label>
				<div class="mws-form-item">
					{{ Form::text('dob',isset($userDetails->dob) ? $userDetails->dob :'',['class' => 'form-control','id'=>'dob',"autocomplete"=>"off"]) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('dob'); ?>
					</div>
				</div>
			</div>
			  </div>
			  <div class="col-md-6">
			  <div class="form-group <?php echo ($errors->first('gender')) ? 'has-error' : ''; ?>">
				<label for="gender" class="mws-form-label">Gender</label>
				<div class="mws-form-item">
					{{ Form::radio('gender','male',( isset($userDetails->gender) && $userDetails->gender =='male') ? 'true' : '',array('id'=>'male_id')) }}
					{{ Form::label('male_id',trans("Male")) }}
					{{ Form::radio('gender','female',( isset($userDetails->gender) && $userDetails->gender =='female') ? 'true' : '',array('id'=>'female_id')) }}
					{{ Form::label('female_id',trans("Female")) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('gender'); ?>
					</div>
				</div>
			</div>
			  </div>
		</div>	  
			  
	</div>
	</div>
	</div>
	</div>
	
	<div class="mws-button-row">
		<div class="input" >
			<input type="button" value="{{ trans('Update') }}" class="btn btn-danger" onclick="save_club();">
			<a href="{{URL::to('admin/club/edit-club')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/club')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script>

	$(function(){
		$('#dob').datepicker({
			format 	: 'yyyy-mm-dd',
			orientation: "bottom",
			todayHighlight: true,
			autoclose: true,
			endDate: '+0d',
		});	
	});

$(document).ready(function () { 
		$( "select[name='country']" ).change(function () {
			var countary_id = $(this).val();
      
				$.ajax({
					url: "{{url('admin/getstate') }}" + '/'+countary_id,
					success: function(data) {
					    //alert(data);
						$('#state').prop('disabled', false);
						//console.log(data);
						$('#state').html('');
						$('#state').html(data);		
					}
				});
		});
     $( "select[name='state']" ).change(function () {
			var state_id = $(this).val();
				$.ajax({
					url: "{{url('admin/getcity') }}" + '/'+state_id,
					success: function(data) {
					    //alert(data);
						$('#city').prop('disabled', false);
						//console.log(data);
						$('#city').html('');
						$('#city').html(data);		
					}
				});
        });
        
		

	});
	
	function save_club(){
        $('#loader_img').show();
        $('.help-inline').html('');
        $('.help-inline').removeClass('error');
		var form = $('#edit_club')[0];
		var formData = new FormData(form);
        $.ajax({
            url: '{{ route("Club.updateClub") }}',
            type:'post',
            data: formData,
			processData: false,
			contentType: false,
            success: function(r){
                error_array     =   JSON.stringify(r);
                data            =   JSON.parse(error_array);
                if(data['success'] == 1) {
					show_message(data['message'],"success");
                    window.location.href   =  "{{ route('club.index') }}";
                }else {
                    $.each(data['errors'],function(index,html){
                        $("#"+index).next().addClass('error');
                        $("#"+index).next().html(html);
                    });
                    $('#loader_img').hide();
                }
                $('#loader_img').hide();
            }
        });
    }
	
	$('#edit_club').each(function() {
	$(this).find('input').keypress(function(e) {
       if(e.which == 10 || e.which == 13) {
			save_club();
			return false;
        }
    });
});
</script>

@stop

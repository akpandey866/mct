@extends('admin.layouts.default')
@section('content')
{{ HTML::script('js/admin/jquery-ui-1.9.2.min.js') }}
{{ HTML::script('js/admin/jquery-ui-timepicker.min.js') }}
{{ HTML::style('css/admin/jui/css/jquery.ui.all.css') }}
<!-- date time picker js and css and here-->
<script>
	jQuery(document).ready(function(){
		$(".choosen_selct").chosen();
	});
</script>
<style>
.chosen-container-single .chosen-single{
	height:34px !important;
	padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>
	  {{ trans("Submitted Items") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Submitted Items") }}</li>
	</ol>
</section>
<section class="content"> 

	<div class="row">
		{{ Form::open(['role' => 'form','route' => 'userSubmittedItems.UserItemListings','class' => 'mws-form',"method"=>"get"]) }}
		{{ Form::hidden('display') }}	
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::select('item_type',array(''=>trans('Select Item'))+config::get('item_types'),((isset($searchVariable['item_type'])) ? $searchVariable['item_type'] : ''), ['class' => 'form-control change_status choosen_selct']) }}
				</div>
			</div>		
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => 'form-control','placeholder'=>'Name']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('email',((isset($searchVariable['email'])) ? $searchVariable['email'] : ''), ['class' => 'form-control','placeholder'=>'Email']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('location',((isset($searchVariable['location'])) ? $searchVariable['location'] : ''), ['class' => 'form-control','placeholder'=>'Location']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('start_from',((isset($dateS)) ? $dateS : ''), ['class' => 'form-control ','placeholder'=>'Date From','id'=>'start_from']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('start_to',((isset($dateE)) ? $dateE : ''), ['class' => 'form-control ','placeholder'=>'Date To','id'=>'start_to']) }}
				</div>
			</div>
			
			<div class="col-md-3 col-sm-2">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> {{ trans('messages.search.text') }}</button>
				<a href="{{route('userSubmittedItems.UserItemListings')}}"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> {{ trans("messages.reset.text") }}</a>
			</div>
			{{ Form::close() }}
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="14%">
							{{
								link_to_route(
									"userSubmittedItems.UserItemListings",
									trans("Item Type"),
									array(
										'sortBy' => 'item_type',
										'order' => ($sortBy == 'item_type' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'item_type' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'item_type' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							{{
								link_to_route(
									"userSubmittedItems.UserItemListings",
									trans("Name"),
									array(
										'sortBy' => 'name',
										'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							{{
								link_to_route(
									"userSubmittedItems.UserItemListings",
									trans("Email"),
									array(
										'sortBy' => 'email',
										'order' => ($sortBy == 'email' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'email' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'email' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							{{
								link_to_route(
									"userSubmittedItems.UserItemListings",
									trans("Location"),
									array(
										'sortBy' => 'location',
										'order' => ($sortBy == 'location' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'location' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'location' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							{{ trans("Description") }}
						</th>
						<th width="14%">
							{{ trans("The Story") }}
						</th>
						<th>
							{{
								link_to_route(
									"userSubmittedItems.UserItemListings",
									trans("Created On"),
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
					</tr>
				</thead>
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)  
						<tr>
							<td>
							{{	!empty($record->item_type) ? config::get('item_types')[$record->item_type] : '' }}
							</td>
							<td>
								{{ $record->name }}
							</td>
							<td>
								<a href="mailto:{{ $record->email }}" class="redicon">
									{{ $record->email }}
								</a>
							</td>
							<td>{{ $record->location }}</td>
							<td>{{ $record->description }}</td>
							<td>{{ $record->story }}</td>
							<td>
								{{ date(config::get("Reading.date_format"),strtotime($record->created_at))}}
							</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="9" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif 
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
<script>
$(document).ready(function() {
	 $( "#start_from" ).datepicker({
		dateFormat 	: 'yy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		onSelect	: function( selectedDate ){ $("#start_to").datepicker("option","minDate",selectedDate); }
	});
	$( "#start_to" ).datepicker({
		dateFormat 	: 'yy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		onSelect	: function( selectedDate ){ $("#start_from").datepicker("option","maxDate",selectedDate); }
	});
})
$(function(){
		$('.date_of_birth').datepicker({
			dateFormat 	: 'yy-mm-dd',
			changeMonth : true,
			changeYear 	: true,
			yearRange	: '-100y:c+nn',
			maxDate		: '-1'
		});	
	});
</script>
@stop
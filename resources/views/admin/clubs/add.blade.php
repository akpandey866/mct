@extends('admin.layouts.default')
@section('content')
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<script src="{{ asset('js/timezones.full.js') }}"></script>
<section class="content-header">
	<h1>
		{{ trans("Add New Game") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/club')}}">{{ trans("Games") }}</a></li>
		<li class="active">{{ trans("Add New Game") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/club/add-club','class' => 'mws-form','files'=>'true', 'id' => 'add_club']) }}
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">GAME BASIC</h3>
            </div>
           <div class="box-body">
		   <div class="row">
		      <div class="col-md-6">
			  <div class="form-group <?php echo ($errors->first('club_type')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">Mode *</label>
				<div class="mws-form-item">
					{{ Form::select(
						 'game_mode',
						 [null => 'Please Select Mode'] + Config::get('home_club'),
						 '',
						 ['id' => 'club_type','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('club_type'); ?>
					</div>
				</div>
			</div>
               
			 </div>	
			  <div class="col-md-6">
                 <div class="form-group <?php echo ($errors->first('club_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('club_name',trans("Club Name").' *', ['class' => 'mws-form-label club_name']) }}
				<div class="mws-form-item">
					{{ Form::text('club_name','',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('club_name'); ?>
					</div>
				</div>
			</div>
			</div>
          </div>
		  <div class="row">
		      <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('game_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('game_name',trans("Club Game Name").' *', ['class' => 'mws-form-label game_name']) }}
				<div class="mws-form-item">
					{{ Form::text('game_name','',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('game_name'); ?>
					</div>
				</div></div>
		  </div>
		  <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('club_logo')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label club_logo">Club logo</label>
				<!--<span class='tooltipHelp' title="" data-Html="true" data-toggle="tooltip"  data-original-title="<?php //echo "The attachment must be a file of type:".IMAGE_EXTENSION; ?>" style="cursor:pointer;">
					<i class="fa fa-question-circle fa-2x"> </i>
				</span>-->
				<div class="mws-form-item">
					{{ Form::file('club_logo', array('accept' => 'image/*')) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('club_logo'); ?>
					</div>
				</div>
			</div>	
		  </div>
		</div>
		<div class="row">
			  <div class="col-md-6">
			     <div class="form-group <?php echo ($errors->first('username')) ? 'has-error' : ''; ?>">
					<label for="username" class="mws-form-label">Username *</label>
					<div class="mws-form-item">
						{{ Form::text('username',isset($userDetails->username) ? $userDetails->username :'',['class' => 'form-control','id'=>'username',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('username'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
</div>
	
	
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Game Details</h3>
            </div>
           <div class="box-body">
		   <div class="row">
		      <div class="col-md-6">
			  
                <div class="form-group <?php echo ($errors->first('sport_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('sport_name',trans("Sport Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('sport_name','Cricket',['class' => 'form-control','readonly']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('sport_name'); ?>
					</div>
				</div>
			</div>
			 </div>	
			  <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('country')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">Country</label>
				<div class="mws-form-item">
					{{ Form::select(
						 'country',
						 [null => 'Please Select Country'] + $countryList,
						 '',
						 ['id' => 'country','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('country'); ?>
					</div>
				</div>
			</div>
			</div>
          </div>
		  <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('state')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">State</label>
				<div class="mws-form-item">
					{{ Form::select(
						 'state',
						 [null => 'Please Select State'],
						 !empty(old('state')) ? old('state') :'',
						 ['id' => 'state','class'=>'form-control','disabled'=>'true']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('state'); ?>
					</div>
				</div>
			</div>
		  </div>
		  <div class="col-md-6">
		     <div class="form-group <?php echo ($errors->first('city')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">City</label>
				<div class="mws-form-item">
					{{ Form::text('city','',['id' => 'city','class'=>'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('city'); ?>
					</div>
				</div>
			</div>
		  </div>
		</div>
		<div class="row">
		      <div class="col-md-6">
			     <div class="form-group <?php echo ($errors->first('post_code')) ? 'has-error' : ''; ?>">
				{{ Form::label('post_code',trans("Post Code"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('post_code','',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('post_code'); ?>
					</div>
				</div>
				</div>
			  </div>
			  <div class="col-md-6">
                <div class="form-group">
					{{ Form::label('timezone',trans("Timezone"), ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						<select class="form-control" id="timezoneSelect" name="timezone" ></select>	
						<div class="error-message help-inline">
							<?php echo $errors->first('sport_name'); ?>
						</div>
					</div>
				</div>
			 </div>
		</div>	  
			  
	</div>
	</div>
	</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Game User Details</h3>
            </div>
           <div class="box-body">
		   <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('first_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('first_name',trans("First Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('first_name','',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('first_name'); ?>
					</div>
				</div>
			</div>
			 </div>	
			  <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('last_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('last_name',trans("Last Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('last_name','',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('last_name'); ?>
					</div>
				</div>
			</div>
			</div>
          </div>
		  <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('email')) ? 'has-error' : ''; ?>">
				{{ Form::label('email',trans("Email").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('email','',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('email'); ?>
					</div>
				</div>
			</div>
		  </div>
		  <div class="col-md-6">
		     <div class="form-group <?php echo ($errors->first('phone')) ? 'has-error' : ''; ?>">
				{{ Form::label('phone',trans("Phone").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('phone','',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('phone'); ?>
					</div>
				</div>
			</div>
		  </div>
		</div>
		<div class="row">
		      <div class="col-md-6">
			     <div class="form-group <?php echo ($errors->first('dob')) ? 'has-error' : ''; ?>">
				<label for="dob" class="mws-form-label">Date of Birth * </label>
				<div class="mws-form-item">
					{{ Form::text('dob','',['class' => 'form-control','id'=>'dob',"autocomplete"=>"off"]) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('dob'); ?>
					</div>
				</div>
			</div>
			  </div>
			  <div class="col-md-6">
			  <div class="form-group fitnessTrainer<?php echo ($errors->first('gender')) ? 'has-error' : ''; ?>">
				<label for="gender" class="mws-form-label">Gender *</label>
				<div class="mws-form-item">
					{{ Form::radio('gender','male',true,array('id'=>'male_id')) }}
					{{ Form::label('male_id','Male') }}
				
					{{ Form::radio('gender','female',false,array('id'=>'female_id')) }}
					{{ Form::label('female_id','Female') }}

					{{ Form::radio('gender',DONOTSPECIFY,false,array('id'=>'do_not_specify')) }}
					{{ Form::label('do_not_specify',DONOTSPECIFY) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('gender'); ?>
					</div>
				</div>
			</div>
			  </div>
		</div>
	</div>
</div>
</div>
</div>
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Club Social</h3>
            </div>
           <div class="box-body">
		<div class="row">
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('facebook')) ? 'has-error' : ''; ?>">
					<label for="facebook" class="mws-form-label">Facebook </label>
					<div class="mws-form-item">
						{{ Form::text('facebook','',['class' => 'form-control','id'=>'facebook',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('facebook'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('twitter')) ? 'has-error' : ''; ?>">
					<label for="twitter" class="mws-form-label">Twitter </label>
					<div class="mws-form-item">
						{{ Form::text('twitter','',['class' => 'form-control','id'=>'twitter',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('twitter'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('instagram')) ? 'has-error' : ''; ?>">
					<label for="instagram" class="mws-form-label">Instagram </label>
					<div class="mws-form-item">
						{{ Form::text('instagram','',['class' => 'form-control','id'=>'instagram',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('instagram'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('website')) ? 'has-error' : ''; ?>">
					<label for="website" class="mws-form-label">Website </label>
					<div class="mws-form-item">
						{{ Form::text('website','',['class' => 'form-control','id'=>'website',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('website'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

			  
	</div>
	</div>
	</div>
	</div>
		<!-- Game basics -->
	<?php
	$startDate = "";
	$endDate = "";
	if(!empty($userDetails->lockout_start_date)){
		$startDate = date('d/m/Y',strtotime($userDetails->lockout_start_date));
	}if(!empty($userDetails->lockout_end_date)){
		$endDate = date('d/m/Y',strtotime($userDetails->lockout_end_date));
	}
	?>
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Season Information</h3>
            </div>
           <div class="box-body">
		<div class="row">
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('start_date')) ? 'has-error' : ''; ?>">
					<label for="facebook" class="mws-form-label">Start Date </label>
					<div class="mws-form-item">
						{{ Form::text('start_date',$startDate,['class' => 'form-control from_date','id'=>'start_date',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('start_date'); ?>
						</div> 
					</div>
				</div>
			</div>
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('end_date')) ? 'has-error' : ''; ?>">
					<label for="end_date" class="mws-form-label">End Date </label>
					<div class="mws-form-item">
						{{ Form::text('end_date',$endDate,['class' => 'form-control to_date','id'=>'end_date',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('end_date'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
			  
	</div>
	</div>
	</div>
	</div>

	<!-- Game basics -->
	<div class="mws-button-row">
		<div class="input" >
			<input type="button" value="{{ trans('Save') }}" class="btn btn-danger" onclick="save_club();">
			<a href="{{URL::to('admin/club/add-club')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/club')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script>
$('#timezoneSelect').timezones();
	$(function(){
		$('#dob').datepicker({
			format 	: 'yyyy-mm-dd',
			orientation: "bottom",
			todayHighlight: true,
			autoclose: true,
			endDate: '+0d',
		});	
	});
$(".from_date").datepicker({
  format: 'dd/mm/yyyy',
	  autoclose: true,
	  todayHighlight: true,
	  startDate: '-0m',
	}).on('changeDate', function (selected) {
	    var startDate = new Date(selected.date.valueOf());
	    $('.to_date').datepicker('setStartDate', startDate);
	}).on('clearDate', function (selected) {
	    $('.to_date').datepicker('setStartDate', null);
	});

	$(".to_date").datepicker({
	   format: 'dd/mm/yyyy',
	   autoclose: true,
	   todayHighlight: true,
	}).on('changeDate', function (selected) {
	   var endDate = new Date(selected.date.valueOf());
	   $('.from_date').datepicker('setEndDate', endDate);
	}).on('clearDate', function (selected) {
	   $('.from_date').datepicker('setEndDate', null);
	});
$(document).ready(function () { 
		$( "select[name='country']" ).change(function () {
			var countary_id = $(this).val();
      
				$.ajax({
					url: "{{url('admin/getstate') }}" + '/'+countary_id,
					success: function(data) {
					    //alert(data);
						$('#state').prop('disabled', false);
						//console.log(data);
						$('#state').html('');
						$('#state').html(data);		
					}
				});

				$.ajax({
					url: "{{url('admin/getCountaryTimezone') }}" + '/'+countary_id,
					success: function(data) {
					    
						$('#timezone').val(data);		
					}
				});
		});
  /*   $( "select[name='state']" ).change(function () {
			var state_id = $(this).val();
				$.ajax({
					url: "{{url('admin/getcity') }}" + '/'+state_id,
					success: function(data) {
					    //alert(data);
						$('#city').prop('disabled', false);
						//console.log(data);
						$('#city').html('');
						$('#city').html(data);		
					}
				});
        });*/
        
		

	});
	
	function save_club(){
        $('#loader_img').show();
        $('.help-inline').html('');
        $('.help-inline').removeClass('error');
		var form = $('#add_club')[0];
		var formData = new FormData(form);
        $.ajax({
            url: '{{ route("Club.saveClub") }}',
            type:'post',
            data: formData,
			processData: false,
			contentType: false,
            success: function(r){
                error_array     =   JSON.stringify(r);
                data            =   JSON.parse(error_array);
                if(data['success'] == 1) {
					show_message(data['message'],"success");
                    window.location.href   =  "{{ route('club.index') }}";
                }else {
                    $.each(data['errors'],function(index,html){
                        $("#"+index).next().addClass('error');
                        $("#"+index).next().html(html);
                    });
                    $('#loader_img').hide();
                }
                $('#loader_img').hide();
            }
        });
    }
	
	$('#add_club').each(function() {
		$(this).find('input').keypress(function(e) {
	       if(e.which == 10 || e.which == 13) {
				save_club();
				return false;
	        }
	    });
	});

$('#club_type').on('change',function(){
	var club_name=$(this).val();
	var modeName = '';
	if(club_name == 1){
		modeName = "Senior Club";
	}else if(club_name == 2){
		modeName = "Junior Club";
	}else if(club_name == 3){
		modeName = "League Club";
	}
	var club_name_lable = $('.club_name').text(modeName+' Name');
	var game_name_lable = $('.game_name').text(modeName+' Game Name');
	var club_logo_lable = $('.club_logo').text(modeName+'  Logo');
})
</script>

@stop

@extends('admin.layouts.default')
@section('content')
<?php $timezoneVal = !empty($userDetails->timezone) ? $userDetails->timezone :''; ?>
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/timezones.full.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<section class="content-header">
	<h1>
		{{ trans("BASICS") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		@if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID)
		<li><a href="{{URL::to('admin/club')}}">{{ trans("Game") }}</a></li>
		@endif
		<li class="active">{{ trans("Basics") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/club/edit-club/','class' => 'mws-form','files'=>'true', 'id' => 'edit_club']) }}
	{{ Form::hidden('user_id',$userDetails->id,['class' => '']) }}
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Game</h3>
            </div>
           <div class="box-body">
		   <div class="row">
		      <div class="col-md-6">
			  <div class="form-group <?php echo ($errors->first('club_type')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">Mode*</label>
				<div class="mws-form-item">
					<b><?php echo Config::get('home_club')[$userDetails->game_mode]; ?></b>
				</div>
			</div>
			
                
			 </div>	
			 <div class="col-md-6">
			     <div class="form-group <?php echo ($errors->first('username')) ? 'has-error' : ''; ?>">
					<label for="username" class="mws-form-label">Username *</label>
					<div class="mws-form-item">
						<b><?php echo isset($userDetails->username) ? $userDetails->username :''; ?></b>
						<!-- {{ Form::text('username',isset($userDetails->username) ? $userDetails->username :'',['class' => 'form-control','id'=>'username',"autocomplete"=>"off"]) }} -->
						<div class="error-message help-inline">
							<?php echo $errors->first('username'); ?>
						</div>
					</div>
				</div>
			</div>
			  
          </div>
		  <div class="row">
		  	<div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('club_name')) ? 'has-error' : ''; ?>">
	                @if($userDetails->game_mode == 1 || $userDetails->game_mode == 2)
						{{ Form::label('club_name',trans("Club Name").' *', ['class' => 'mws-form-label']) }}
					@else
						{{ Form::label('club_name',trans("League name").' *', ['class' => 'mws-form-label']) }}
					@endif
					<div class="mws-form-item">
						{{ Form::text('club_name',isset($userDetails->club_name) ? $userDetails->club_name :'',['class' => 'form-control club_name']) }}

						<div class="error-message help-inline">
							<?php echo $errors->first('club_name'); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('game_name')) ? 'has-error' : ''; ?>">
					{{ Form::label('game_name',trans("Game Name").' *', ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						{{ Form::text('game_name',isset($userDetails->game_name) ? $userDetails->game_name :'',['class' => 'form-control game_name']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('game_name'); ?>
						</div>
					</div>
				</div>
			</div>
			  
		</div>
		<div class="row">
  		  <div class="col-md-6">
    
			<div class="form-group <?php echo ($errors->first('club_logo')) ? 'has-error' : ''; ?>">
				@if($userDetails->game_mode == 1 || $userDetails->game_mode == 2)
					<label for="image" class="mws-form-label">Club</label>
				@else
					<label for="image" class="mws-form-label">League logo</label>
				@endif
				<div class="mws-form-item">
					{{ Form::file('club_logo', array('accept' => 'image/*','class'=>'club_logo')) }}
					<br />
					
					@if(File::exists(CLUB_IMAGE_ROOT_PATH.$userDetails->club_logo))
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo CLUB_IMAGE_URL.$userDetails->club_logo; ?>">
							<div class="usermgmt_image">
								<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.CLUB_IMAGE_URL.'/'.$userDetails->club_logo ?>">
							</div>
						</a>
					@endif
					<div class="error-message help-inline">
						<?php echo $errors->first('club_logo'); ?>
						<div class="error-message help-inline">
						<?php echo $errors->first('club_logo'); ?>
					</div>
					</div>
				</div>
			</div>
		  </div>
		</div>
	</div>
	</div>
	</div>
	</div>
	
	
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
            	@if($userDetails->game_mode == 1 || $userDetails->game_mode == 2)
              		<h3 class="box-title">About Club</h3>
              	@else
              		<h3 class="box-title">About League</h3>
             	@endif
            </div>
           <div class="box-body">
		   <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('sport_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('sport_name',trans("Sport").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('sport_name',isset($userDetails->sport_name) ? $userDetails->sport_name :'',['class' => 'form-control', 'readOnly' => true]) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('sport_name'); ?>
					</div>
				</div>
			</div>
			 </div>	
			  <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('country')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">Country</label>
				<div class="mws-form-item">
					{{ Form::select(
						 'country',
						 [null => 'Please Select Country'] + $countryList,
						isset($userDetails->country) ? $userDetails->country :'',
						 ['id' => 'country','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('country'); ?>
					</div>
				</div>
			</div>
			</div>
          </div>
		  <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('state')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">State</label>
				<div class="mws-form-item">
					{{ Form::select(
						 'state',
						 [null => 'Please Select State'],
						 isset($userDetails->state) ? $userDetails->state :'',
						 ['id' => 'state','class'=>'form-control','disabled'=>'true']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('state'); ?>
					</div>
				</div>
			</div>
		  </div>
		  <div class="col-md-6">
		     <div class="form-group <?php echo ($errors->first('city')) ? 'has-error' : ''; ?>">
				<label for="address" class="mws-form-label">City</label>
				<div class="mws-form-item">
					{{ Form::text('city',isset($userDetails->city) ? $userDetails->city :'',['id' => 'city','class'=>'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('city'); ?>
					</div>
				</div>
			</div>
		  </div>
		</div>
		<div class="row">
		      <div class="col-md-6">
			     <div class="form-group <?php echo ($errors->first('post_code')) ? 'has-error' : ''; ?>">
				{{ Form::label('post_code',trans("Post Code"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('post_code',isset($userDetails->post_code) ? $userDetails->post_code :'',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('post_code'); ?>
					</div>
				</div>
			</div>
			  </div>
			  <div class="col-md-6">
                <div class="form-group">
					{{ Form::label('timezone',trans("Timezone").' *', ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						<select class="form-control" id="timezoneSelect" name="timezone" ></select>
						<div class="error-message help-inline">
							<?php echo $errors->first('sport_name'); ?>
						</div>
					</div>
				</div>
			 </div>
		</div>	  
			  
	</div>
	</div>
	</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Game Admin</h3>
            </div>
           <div class="box-body">
		   <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('first_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('first_name',trans("First Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('first_name',isset($userDetails->first_name) ? $userDetails->first_name :'',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('first_name'); ?>
					</div>
				</div>
			</div>
			 </div>	
			  <div class="col-md-6">
               <div class="form-group <?php echo ($errors->first('last_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('last_name',trans("Last Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('last_name',isset($userDetails->last_name) ? $userDetails->last_name :'',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('last_name'); ?>
					</div>
				</div>
			</div>
			</div>
          </div>
		  <div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('email')) ? 'has-error' : ''; ?>">
				{{ Form::label('email',trans("Email").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('email',isset($userDetails->email) ? $userDetails->email :'',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('email'); ?>
					</div>
				</div>
			</div>
		  </div>
		  <div class="col-md-6">
		     <div class="form-group <?php echo ($errors->first('phone')) ? 'has-error' : ''; ?>">
				{{ Form::label('phone',trans("Phone").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('phone',isset($userDetails->phone) ? $userDetails->phone :'',['class' => 'form-control']) }}

					<div class="error-message help-inline">
						<?php echo $errors->first('phone'); ?>
					</div>
				</div>
			</div>
		  </div>
		</div>
		<!-- Password confirm password -->
		<div class="row">
			<div class="col-md-6">
				<div class="mws-form-row">
					<div class="mws-form-message info">Please leave blank if you do not want to change password.</div>
				</div>
			</div>
		</div>
		<div class="row">
		      <div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('password')) ? 'has-error' : ''; ?>">
				{{ Form::label('password',trans("New Password"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::password('password',['class'=>'userPassword form-control','autocomplete'=>"false"]) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('password'); ?>
					</div>
				</div>
			</div>
		  </div>
		  <div class="col-md-6">
		     <div class="form-group <?php echo ($errors->first('confirm_password')) ? 'has-error' : ''; ?>">
				{{ Form::label('confirm_password',trans("Confirm Password"), ['class' => 'mws-form-label','autocomplete'=>"false"]) }}
				<div class="mws-form-item">
					{{ Form::password('confirm_password',['class'=>'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('confirm_password'); ?>
					</div>
				</div>
			</div>
		  </div>
		</div>

		<div class="row">
		      <div class="col-md-6">
			     <div class="form-group <?php echo ($errors->first('dob')) ? 'has-error' : ''; ?>">
					<label for="dob" class="mws-form-label">Date of Birth * </label>
					<div class="mws-form-item">
						{{ Form::text('dob',isset($userDetails->dob) ? \Carbon\Carbon::parse($userDetails->dob)->format('d/m/Y') :'',['class' => 'form-control','id'=>'dob',"autocomplete"=>"off"]) }}

						<div class="error-message help-inline">
							<?php echo $errors->first('dob'); ?>
						</div>
					</div>
				</div>
			  </div>
			  <div class="col-md-6">
			  <div class="form-group <?php echo ($errors->first('gender')) ? 'has-error' : ''; ?>">
				<label for="gender" class="mws-form-label">Gender *</label>
				<div class="mws-form-item">
					{{ Form::radio('gender','male',( isset($userDetails->gender) && $userDetails->gender =='male') ? 'true' : '',array('id'=>'male_id')) }}
					{{ Form::label('male_id',trans("Male")) }}
					{{ Form::radio('gender','female',( isset($userDetails->gender) && $userDetails->gender =='female') ? 'true' : '',array('id'=>'female_id')) }}
					{{ Form::label('female_id',trans("Female")) }}

					{{ Form::radio('gender',DONOTSPECIFY,( isset($userDetails->gender) && $userDetails->gender == DONOTSPECIFY) ? 'true' : '',array('id'=>'do_not_specify')) }}
					{{ Form::label('do_not_specify',DONOTSPECIFY) }}
		
					<div class="error-message help-inline" id="gender">
						<?php echo $errors->first('gender'); ?>
					</div>
				</div>
			</div>
			  </div>
		</div>	
		<div class="row">
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
					<label for="image" class="mws-form-label">Profile Image</label>
					<!--<span class='tooltipHelp' title="" data-html="true" data-toggle="tooltip"  data-original-title="<?php echo "The attachment must be a file of type:".IMAGE_EXTENSION; ?>" style="cursor:pointer;">
						<i class="fa fa-question-circle fa-2x"> </i>
					</span>-->
					<div class="mws-form-item">
						{{ Form::file('image', array('accept' => 'image/*','class'=>'image')) }}
						<br />
						@if(!empty($userDetails->image) && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.$userDetails->image))
							<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo USER_PROFILE_IMAGE_URL.$userDetails->image; ?>">
								<div class="usermgmt_image">
									<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.USER_PROFILE_IMAGE_URL.'/'.$userDetails->image ?>">
								</div>
							</a>
						@endif
						<div class="error-message help-inline">
							<?php echo $errors->first('image'); ?>
						</div>
					</div>
				</div>
			  </div>
		</div>		
	</div>
</div>
</div>
</div>
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
            	@if($userDetails->game_mode == 1 || $userDetails->game_mode == 2)
              		<h3 class="box-title">Club Social</h3>
              	@else
              		<h3 class="box-title">League Social</h3>
             	@endif
              
            </div>
           <div class="box-body">
		<div class="row">
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('facebook')) ? 'has-error' : ''; ?>">
					<label for="facebook" class="mws-form-label">Facebook </label>
					<div class="mws-form-item">
						{{ Form::text('facebook',isset($userDetails->facebook) ? $userDetails->facebook :'',['class' => 'form-control','id'=>'facebook',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('facebook'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('twitter')) ? 'has-error' : ''; ?>">
					<label for="twitter" class="mws-form-label">Twitter </label>
					<div class="mws-form-item">
						{{ Form::text('twitter',isset($userDetails->twitter) ? $userDetails->twitter :'',['class' => 'form-control','id'=>'twitter',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('twitter'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('instagram')) ? 'has-error' : ''; ?>">
					<label for="instagram" class="mws-form-label">Instagram </label>
					<div class="mws-form-item">
						{{ Form::text('instagram',isset($userDetails->instagram) ? $userDetails->instagram :'',['class' => 'form-control','id'=>'instagram',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('instagram'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('website')) ? 'has-error' : ''; ?>">
					<label for="website" class="mws-form-label">Website </label>
					<div class="mws-form-item">
						{{ Form::text('website',isset($userDetails->website) ? $userDetails->website :'',['class' => 'form-control','id'=>'website',"autocomplete"=>"off"]) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('website'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>  
			  
	</div>
	</div>
	</div>
	</div>
	<!-- Game basics -->
	<?php
	$startDate = "";
	$endDate = "";
	if(!empty($userDetails->lockout_start_date)){
		$startDate = date('d/m/Y',strtotime($userDetails->lockout_start_date));
	}if(!empty($userDetails->lockout_end_date)){
		$endDate = date('d/m/Y',strtotime($userDetails->lockout_end_date));
	}
	?>
	<div class="row">
		<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Season</h3>
            </div>
           <div class="box-body">
		<div class="row">
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('start_date')) ? 'has-error' : ''; ?>">
					<label for="facebook" class="mws-form-label">Season Start Date * </label>
					<div class="mws-form-item">
						<?php if(empty($userDetails->lockout_start_date)){ ?>
						{{ Form::text('start_date',$startDate,['class' => 'form-control from_date','id'=>'start_date',"autocomplete"=>"off"]) }}
						<?php }else{ echo $startDate; ?>
							{{ Form::hidden('start_date',$startDate) }}
						<?php 
						}?>
						<div class="error-message help-inline">
							<?php echo $errors->first('start_date'); ?>
						</div> 
					</div>
				</div>
			</div>
			<div class="col-md-6">
			    <div class="form-group <?php echo ($errors->first('end_date')) ? 'has-error' : ''; ?>">
					<label for="end_date" class="mws-form-label">Season End Date *</label>
					<div class="mws-form-item">
						<?php if(empty($userDetails->lockout_start_date)){ ?>
						{{ Form::text('end_date',$endDate,['class' => 'form-control to_date','id'=>'end_date',"autocomplete"=>"off"]) }}
						<?php }else{ echo $endDate; ?>
							{{ Form::hidden('end_date',$endDate) }}
						<?php }?>
						<div class="error-message help-inline">
							<?php echo $errors->first('end_date'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
			  
	</div>
	</div>
	</div>
	</div>

	<!-- Game basics -->
	
	<div class="mws-button-row">
		<div class="input" >
			<input type="button" value="{{ trans('Update') }}" class="btn btn-danger" onclick="save_club();">
			<a href="{{URL::to('admin/club/edit-club')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/club')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script>
 $('#timezoneSelect').timezones();
var timezoneVal = "{{$timezoneVal}}";
if(timezoneVal !=""){
 	$('#timezoneSelect').val(timezoneVal);
}
$(function(){
	$('#dob').datepicker({
		format 	: 'dd/mm/yyyy',
		orientation: "bottom",
		todayHighlight: true,
		autoclose: true,
		endDate: '+0d',
	});	
});
$(".from_date").datepicker({
  format: 'dd/mm/yyyy',
	  autoclose: true,
	  todayHighlight: true,
	  startDate: '-0m',
	}).on('changeDate', function (selected) {
	    var startDate = new Date(selected.date.valueOf());
	    $('.to_date').datepicker('setStartDate', startDate);
	}).on('clearDate', function (selected) {
	    $('.to_date').datepicker('setStartDate', null);
	});

	$(".to_date").datepicker({
	   format: 'dd/mm/yyyy',
	   autoclose: true,
	   todayHighlight: true,
	}).on('changeDate', function (selected) {
	   var endDate = new Date(selected.date.valueOf());
	   $('.from_date').datepicker('setEndDate', endDate);
	}).on('clearDate', function (selected) {
	   $('.from_date').datepicker('setEndDate', null);
	});

var country = "<?php echo $country; ?>";
var state = "<?php echo $state; ?>";
var city = "<?php echo $city; ?>";
if(state != 0){
	get_state();
}
/*if(city != 0){
	get_city();
}*/
function get_state(){
	$.ajax({
		url: "{{url('admin/getstateedit') }}" + '/'+country+ '/'+state,
		success: function(data) {
			$('#state').prop('disabled', false);
			$('#state').html('');
			$('#state').html(data);		
		}
	});
}
/*function get_city(){
	$.ajax({
		url: "{{url('admin/getcityedit') }}" + '/'+state+ '/'+city,
		success: function(data) {
			$('#city').prop('disabled', false);
			$('#city').html('');
			$('#city').html(data);		
		}
	});
}*/
$(document).ready(function () { 
		$( "#country" ).change(function () {
			 $('#loader_img').show();
			var countary_id = $(this).val();
				$.ajax({
					url: "{{url('admin/getstate') }}" + '/'+countary_id,
					success: function(data) {
					    //alert(data);
						$('#state').prop('disabled', false);
						//console.log(data);
						$('#state').html('');
						$('#state').html(data);	
						 $('#loader_img').hide();	
					}
				});

				$.ajax({
				url: "{{url('admin/getCountaryTimezone') }}" + '/'+countary_id,
				success: function(data) {
				    
					$('#timezone').val(data);		
				}
			});
		});
     $( "select[name='state']" ).change(function () {
     	 	$('#loader_img').show();
			var state_id = $(this).val();
				$.ajax({
					url: "{{url('admin/getcity') }}" + '/'+state_id,
					success: function(data) {
						$('#city').prop('disabled', false);
						$('#city').html('');
						$('#city').html(data);	
						 $('#loader_img').hide();	
					}
				});
        });
        
		

	});
	
	function save_club(){
        $('#loader_img').show();
        $('.help-inline').html('');
        $('.help-inline').removeClass('error');
		var form = $('#edit_club')[0];
		var user_role_id = "{{Auth::guard('admin')->user()->user_role_id}}";
		var user_id = "{{Auth::guard('admin')->user()->id}}";
		var formData = new FormData(form);
        $.ajax({
            url: '{{ route("Club.updateClub") }}',
            type:'post',
            data: formData,
			processData: false,
			contentType: false,
            success: function(r){
            	// alert('hello'); 
                error_array     =   JSON.stringify(r);
                data            =   JSON.parse(error_array);
                if(data['success'] == 1) {
					if(user_role_id == 1){
						show_message(data['message'],"success");
                   		window.location.href   =  "{{ route('club.index') }}";
					}else{
						show_message(data['message'],"success");
						window.location.href   =  "{{ url('admin/club/edit-club/') }}"+'/'+user_id;
					}
                }else {
                    $.each(data['errors'],function(index,html){
                    	if(index == "gender"){
                    		 $("#gender").addClass('error');
                        	$("#gender").html(html);
                    	}
                        $("#"+index).next().addClass('error');
                        $("#"+index).next().html(html);
                    });
                    $('#loader_img').hide();
                }
                $('#loader_img').hide();
            }
        });
    }
	
	$('#edit_club').each(function() {
		$(this).find('input').keypress(function(e) {
	       if(e.which == 10 || e.which == 13) {
				save_club();
				return false;
	        }
	    });
	});

$('#club_type').on('change',function(){ 
	var club_name=$(this).val();
	var modeName = '';
	if(club_name == 1){
		modeName = "Senior Club";
	}else if(club_name == 2){
		modeName = "Junior Club";
	}else if(club_name == 3){
		modeName = "League Club";
	}
	var club_name_lable = $('.club_name').text(modeName+' Name');
	var game_name_lable = $('.game_name').text(modeName+' Game Name');
	var club_logo_lable = $('.club_logo').text(modeName+'  Logo');
});
</script>

@stop

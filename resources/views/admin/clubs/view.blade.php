@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		 {{ trans("Club Detail") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/club')}}">{{ trans("Clubs") }}</a></li>
		<li class="active"> {{ trans("Club Detail") }}  </li>
	</ol>
</section>
<div class="box box-warning "> 
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">CLUB USER</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Mode:</th>
						<td data-th='Full Name' class="txtFntSze"><?php echo Config::get('home_club')[$userDetails->game_mode]; ?></td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Club Name:</th>
						<td data-th='Full Name' class="txtFntSze">{{ isset($userDetails->club_name) ? $userDetails->club_name:'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Game Name:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($userDetails->game_name) ? $userDetails->game_name :'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Username:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($userDetails->username) ? $userDetails->username :'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Club Log:</th>
						<td data-th='Phone Number' class="txtFntSze">@if(File::exists(CLUB_IMAGE_ROOT_PATH.$userDetails->club_logo))
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo CLUB_IMAGE_URL.$userDetails->club_logo; ?>">
							<div class="usermgmt_image">
								<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.CLUB_IMAGE_URL.'/'.$userDetails->club_logo ?>">
							</div>
						</a>
					@endif</td>
					</tr>
					
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">About Club</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Sport Name:</th>
						<td data-th='Full Name' class="txtFntSze">{{ isset($userDetails->sport_name) ? $userDetails->sport_name:'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Country:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($countryName->name) ? $countryName->name :'' }}</td>
					</tr>
					
					<tr>
						<th  width="30%" class="text-right txtFntSze">State:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($stateName->name) ? $userDetails->name :'' }}</td>
					</tr>
					
					<tr>
						<th  width="30%" class="text-right txtFntSze">City:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($cityName->name) ? $cityName->name :'' }}</td>
					</tr>
					
				</tbody>
			</table>
		</div>
	</div>
	
	
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">Club User Details</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Full Name:</th>
						<td data-th='Full Name' class="txtFntSze">{{ isset($userDetails->full_name) ? $userDetails->full_name:'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Email  :</th>
						<td data-th='Email' class="txtFntSze">{{ isset($userDetails->email) ? $userDetails->email :'' }}</td>
					</tr>
					
					<tr>
						<th  width="30%" class="text-right txtFntSze">Phone:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($userDetails->mobile) ? $userDetails->mobile :'' }}</td>
					</tr>
					
					<tr>
						<th  width="30%" class="text-right txtFntSze">Date of Birth:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($userDetails->dob) ? $userDetails->dob :'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Gender:</th>
						<td data-th='Email' class="txtFntSze">
							<?php 
							if($userDetails->gender == MALE){
									echo "Male";
							}if($userDetails->gender == FEMALE){
								echo "Female";
							}if($userDetails->gender == DONOTSPECIFY){
								echo "Do not wish to specify";
							}

							?>
							</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">Club Social</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Facebook:</th>
						<td data-th='Full Name' class="txtFntSze">{{ isset($userDetails->facebook) ? $userDetails->facebook:'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Twitter:</th>
						<td data-th='Full Name' class="txtFntSze">{{ isset($userDetails->twitter) ? $userDetails->twitter:'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Instagram:</th>
						<td data-th='Full Name' class="txtFntSze">{{ isset($userDetails->instagram) ? $userDetails->instagram:'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Website:</th>
						<td data-th='Full Name' class="txtFntSze">{{ isset($userDetails->website) ? $userDetails->website:'' }}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<style>
.about_me {
    word-wrap:break-word;
	word-break: break-all;
}
</style>
@stop

@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		{{ trans("Edit Serve Slider") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/serve')}}">{{ trans("Serve Slider") }}</a></li>
		<li class="active">{{ trans("Edit Serve Slider") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/serve/edit-serve','class' => 'mws-form','files'=>'true']) }}
	{{ Form::hidden('id',$details->id) }}
		<div class="row">
			<div class="col-md-6">
				<div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
					<label for="image" class="mws-form-label">Image</label>
					<div class="mws-form-item">
						{{ Form::file('image', array('accept' => 'image/*')) }}
						<br />
						
						@if(File::exists(SERVE_IMAGE_ROOT_PATH.$details->image))
							<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo SERVE_IMAGE_URL.$details->image; ?>">
								<div class="usermgmt_image">
									<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.SERVE_IMAGE_URL.'/'.$details->image ?>">
								</div>
							</a>
						@endif
						<div class="error-message help-inline">
							<?php echo $errors->first('image'); ?>
						</div>
					</div>
				</div>
		 	</div>
		</div>
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('title')) ? 'has-error' : ''; ?>">
				{{ Form::label('title', trans("Title"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('title',isset($details->title) ? $details->title :'', ['class' => 'form-control ','placeholder'=>'Title','id'=>'title']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('title'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Update') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/serve/edit-serve/'.$details->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/serve')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
@stop

@extends('admin.layouts.default')

@section('content')

<section class="content-header">
	<h1>
		{{ 'Edit '.studly_case($type) }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/dropdown-manager',$type)}}">Masters</a></li>
		<li class="active">{{ 'Edit '.studly_case($type) }}</li>
	</ol>
</section>
<section class="content">
	{{ Form::open(['role' => 'form','url' => 'admin/dropdown-manager/edit-dropdown/'.$dropdown->id.'/'.$type,'class' => 'mws-form','files' => true]) }}
	<div class="row pad">
		<div class="col-md-6">
			<div class="form-group">
				{{  Form::label('name',trans("Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text("name",isset($dropdown->name)?$dropdown->name:'', ['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<input type="submit" value="{{ trans('Save') }}" class="btn btn-primary">
		<a href="{{URL::to('dropdown-manager/edit-dropdown/'.$dropdown->id.'/'.$type)}}" class="btn btn-danger"><i class=\"icon-refresh\"></i> Reset</a>
	</div>
	{{ Form::close() }} 
</section>
@stop

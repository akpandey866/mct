@extends('admin.layouts.default')

@section('content')
<section class="content-header">
	<h1>
		{{ studly_case($type) }} Management
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ studly_case($type) }} Management</li>
	</ol>
</section>
	
<section class="content"> 
	<div class="row">
		{{ Form::open(['method' => 'get','role' => 'form','url' => 'admin/dropdown-manager/'.$type,'class' => 'mws-form']) }}
		{{ Form::hidden('display') }}
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					{{ Form::text('name',((isset($searchVariable['name'])) ? $searchVariable['name'] : ''), ['class' => 'form-control','placeholder'=>studly_case($type)]) }}
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href="{{URL::to('dropdown-manager/'.$type)}}"  class="btn btn-primary"><i class='fa fa-refresh '></i> Reset</a>
			</div>
		{{ Form::close() }}
		<div class="col-md-5 col-sm-3 ">
			<div class="form-group pull-right">  
				<a href="{{URL::to('admin/dropdown-manager/add-dropdown/'.$type)}}" class="btn btn-success btn-small align">{{ 'Add New '.studly_case($type) }} </a>
			</div>
		</div>
	</div> 
		
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
			<thead>
				<tr>
					<th width="40%">
						{{
							link_to_route(
								'DropDown.listDropDown',
								'Name',
								array(
									$type,
									'sortBy' => 'name',
									'order' => ($sortBy == 'name' && $order == 'desc') ? 'asc' : 'desc'
								),
							   array('class' => (($sortBy == 'name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)
						}}
					</th>
					
					<th width="30%">
						{{
							link_to_route(
								'DropDown.listDropDown',
								'Created ',
								array(
									$type,
									'sortBy' => 'created_at',
									'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc'
								),
							   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
							)
						}}
					</th>
					<th>{{ 'Action' }}</th>
				</tr>
			</thead>
			<tbody id="powerwidgets">
				@if(!$result->isEmpty())
				@foreach($result as $record)
				<tr class="items-inner">
					<td data-th='Name'>{{ $record->name }}</td>
					<td data-th='Created At'>{{ date(Config::get("Reading.date_format") , strtotime($record->created_at)) }}</td>
					<td data-th='Action'>						
						<a title="Edit" href="{{URL::to('admin/dropdown-manager/edit-dropdown/'.$record->id.'/'.$type)}}" class="btn btn-info btn-small"><span class="ti-pencil"></span></a>

						@if($type !="position")
							<a title="Delete" href="{{URL::to('admin/dropdown-manager/delete-dropdown/'.$record->id.'/'.$type)}}"  class="delete_any_item btn btn-danger btn-small"><span class="ti-trash"></span> </a>
						@endif
						
					</td>
				</tr>
				 @endforeach  
			</tbody>
		</table>
		@else
			<tr>
			<td align="center" style="text-align:center;" colspan="6" > No Result Found</td>
		  </tr>			
		@endif 
	</div>
	@include('pagination.default', ['paginator' => $result])
</div>
@stop

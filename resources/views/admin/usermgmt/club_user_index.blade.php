@extends('admin.layouts.default')
@section('content')
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<!-- date time picker js and css and here-->
<script>
	jQuery(document).ready(function(){
		$(".choosen_selct").chosen();
	});
</script>
<style>
.chosen-container-single .chosen-single{
	height:34px !important;
	padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>
	
		@if($user_role_id == USER)
			<?php $user_role = trans("Users"); ?>
		@else
			<?php $user_role = trans("Club Users"); ?>
		@endif
		{{ $user_role }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Users") }}</li>
	</ol>
</section>
<section class="content"> 
	@if( Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID )
	<div class="row">
		<div class="col-md-3 col-sm-3 col-xs-12">
		  <div class="info-box">
			<span class="info-box-icon bg-orange"><i class="fa fa-users"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text"><b>Users</b> <br/>(Till Now)</span>
			  <span class="info-box-number">{{ $totalUsers }}</span>
			</div>
		  </div>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
		  <div class="info-box">
			<span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text"><b>Users</b>  <br/>(In this month)</span>
			  <span class="info-box-number">{{ $thisMonthServiceSeeker }}</span>
			</div>
		  </div>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
		  <div class="info-box">
			<span class="info-box-icon bg-purple"><i class="fa fa-users"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text"><b>Users</b> <br/>(In last month)</span>
			  <span class="info-box-number">{{ $lastMonthServiceSeeker }}</span>
			</div>
		  </div>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
		  <div class="info-box">
			<span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text"><b>Users</b> <br/>(In this year)</span>
			  <span class="info-box-number">{{ $thisYearServiceSeeker }}</span>
			</div>
		  </div>
		</div>
	</div>
	@endif
	<div class="row">
		{{ Form::open(['role' => 'form','url' => 'admin/users/'.$user_role_id,'class' => 'mws-form',"method"=>"get"]) }}
		{{ Form::hidden('display') }}
			<!--<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::select('is_active',array(''=>trans('Select Status'),0=>'Inactive',1=>'Active'),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control choosen_selct']) }}
				</div>
			</div>-->
			
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('full_name',((isset($searchVariable['full_name'])) ? $searchVariable['full_name'] : ''), ['class' => 'form-control','placeholder'=>'Full Name']) }}
				</div>
			</div>
<!-- 			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('last_name',((isset($searchVariable['last_name'])) ? $searchVariable['last_name'] : ''), ['class' => 'form-control','placeholder'=>'Last Name']) }}
				</div>
			</div> -->
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('my_team_name',((isset($searchVariable['my_team_name'])) ? $searchVariable['my_team_name'] : ''), ['class' => 'form-control','placeholder'=>'Team Name']) }}
				</div>
			</div>
			<?php /*
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('start_from',((isset($dateS)) ? $dateS : ''), ['class' => 'form-control ','placeholder'=>'Date From','id'=>'start_from']) }}
				</div>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('start_to',((isset($dateE)) ? $dateE : ''), ['class' => 'form-control ','placeholder'=>'Date To','id'=>'start_to']) }}
				</div>
			</div>
			*/ ?>
			<div class="col-md-3 col-sm-2">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
				<a href="{{URL::to('admin/users/'.$user_role_id)}}"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> {{ trans("Reset") }}</a>
			</div>
			{{ Form::close() }}
		@if( Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID )
		<div class="col-md-2 col-sm-2">
			<div class="form-group">  
				<a href="{{URL::to('admin/users/add-user/'.$user_role_id)}}" class="btn btn-success btn-small  pull-right" style="margin:0;">{{ trans("Add New User") }} </a>
			</div>
		</div>
		@endif
	</div> 

	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="15%">
							{{
								link_to_route(
									"Users.index",
									trans("Full Name"),
									array(
										'id'=>isset($user_role_id)?$user_role_id:'',
										'sortBy' => 'full_name',
										'order' => ($sortBy == 'full_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'full_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'full_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							{{
								link_to_route(
									"Users.index",
									trans("Username"),
									array(
										'id'=>isset($user_role_id)?$user_role_id:'',
										'sortBy' => 'username',
										'order' => ($sortBy == 'username' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'username' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'username' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							{{
								link_to_route(
									"Users.index",
									trans("Email"),
									array(
										'id'=>isset($user_role_id)?$user_role_id:'',
										'sortBy' => 'email',
										'order' => ($sortBy == 'email' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'email' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'email' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							{{
								link_to_route(
									"Users.index",
									trans("Team Name"),
									array(
										'id'=>isset($user_role_id)?$user_role_id:'',
										'sortBy' => 'my_team_name',
										'order' => ($sortBy == 'my_team_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'my_team_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'my_team_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							{{
								link_to_route(
									"Users.index",
									trans("Status"),
									array(
										'id'=>isset($user_role_id)?$user_role_id:'',
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th>
							<a href="javascript:void();" style="cursor: auto;" class="sorting">Action</a>
						</th>
					</tr>
				</thead>
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)  
						<tr>
							<td>
								{{ $record->full_name }}
							</td>
							<td>
								{{ $record->username }}
							</td>
							<td>
								{{ $record->email }}
							</td>
							<td>
								{{ $record->team_name }}
							</td>
							<td>
								@if($record->is_active	==1)
									<span class="label label-success" >{{ trans("Activated") }}</span>
								@else
									<span class="label label-warning" >{{ trans("Deactivated") }}</span>
								@endif
							</td>
							<td>
								<a href="{{URL::to('admin/users/view-user/'.$record->id)}}" title="{{ trans('View') }}" class="btn btn-info">
									<i class="fa fa-eye"></i>
								</a>
								<a href="{{URL::to('admin/users/user-refer/'.$record->id)}}" title="{{ trans('Refer List') }}" class="btn btn-primary">
									<i class="fa fa-user-plus"></i>
								</a>
								<a title="{{ trans('Send Login Credentials') }}" href="{{ URL::to('admin/users/send-credential/'.$record->id) }}" class="btn btn-success">
									<i class="fa fa-share"></i>
								</a>
								@if($record->user_role_id !=CLUBUSER)
									@if($record->is_active == 1)
										<a  title="Click To Deactivate" href="{{URL::to('admin/users/update-status/'.$record->user_role_id.'/'.$record->id.'/0')}}" class="btn btn-success btn-small status_any_item"><span class="fa fa-check"></span>
										</a>
									@else
										<a title="Click To Activate" href="{{URL::to('admin/users/update-status/'.$record->user_role_id.'/'.$record->id.'/1')}}" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
										</a> 
									@endif 
								@endif
							</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="9" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif 
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
<script>
$(document).ready(function() {
	 $( "#start_from" ).datepicker({
		dateFormat 	: 'yy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		onSelect	: function( selectedDate ){ $("#start_to").datepicker("option","minDate",selectedDate); }
	});
	$( "#start_to" ).datepicker({
		dateFormat 	: 'yy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		onSelect	: function( selectedDate ){ $("#start_from").datepicker("option","maxDate",selectedDate); }
	});
})
$(function(){
	$('.date_of_birth').datepicker({
		dateFormat 	: 'yy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		maxDate		: '-1'
	});	
});

 $(document).on('click', '.show_profile', function(e){ 
			e.stopImmediatePropagation();
			url = $(this).attr('href');
			var full_name = $(this).attr('data-rel');
			bootbox.confirm("Are you sure want to show "+full_name+ '?',
			function(result){
				if(result){
					window.location.replace(url);
				}
			});
			e.preventDefault();
		});
		
		$(document).on('click', '.hide_profile', function(e){ 
			e.stopImmediatePropagation();
			url = $(this).attr('href');
			var full_name = $(this).attr('data-rel');
			bootbox.confirm("Are you sure want to hide "+full_name+ '?',
			function(result){
				if(result){
					window.location.replace(url);
				}
			});
			e.preventDefault();
		});
	$(".findUsers").change(function(){
		$("#search_users").submit();
	});
</script>
@stop
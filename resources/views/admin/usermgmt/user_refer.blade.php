@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>Refer List</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Refer List") }}</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		{{ Form::open(['role' => 'form','url' => 'admin/users/user-refer/'.$user_id,'class' => 'mws-form',"method"=>"get"]) }}
		{{ Form::hidden('display') }}			
			<div class="col-md-2 col-sm-2">
				<div class="form-group ">  
					{{ Form::text('full_name',((isset($searchVariable['full_name'])) ? $searchVariable['full_name'] : ''), ['class' => 'form-control','placeholder'=>'Full Name']) }}
				</div>
			</div>			
			<div class="col-md-3 col-sm-2">
				<button class="btn btn-primary" style="margin:0;"><i class='fa fa-search '></i> {{ trans('Search') }}</button>
				<a href="{{URL::to('admin/users/user-refer/'.$user_id)}}"  class="btn btn-primary" style="margin:0;"><i class='fa fa-refresh '></i> {{ trans("Reset") }}</a>
			</div>
			{{ Form::close() }}
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="15%">Image</th>
						<th width="14%">
							{{
								link_to_route(
									"Users.referList",
									trans("Full Name"),
									array(
										'id'=>isset($user_id)?$user_id:'',
										'sortBy' => 'full_name',
										'order' => ($sortBy == 'full_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'full_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'full_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							{{
								link_to_route(
									"Users.referList",
									trans("Email"),
									array(
										'id'=>isset($user_id)?$user_id:'',
										'sortBy' => 'full_name',
										'order' => ($sortBy == 'full_name' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'full_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'full_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="14%">
							{{
								link_to_route(
									"Users.referList",
									trans("Status"),
									array(
										'id'=>isset($user_role_id)?$user_role_id:'',
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th>
							{{
								link_to_route(
									"Users.referList",
									trans("Created On"),
									array(
										'id'=>isset($user_id)?$user_id:'',
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
					</tr>
				</thead>
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)  
						<tr>
							<td>
							@if(!empty($record->image) && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.$record->image))
                                <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo USER_PROFILE_IMAGE_URL.$record->image; ?>"> <div class="usermgmt_image">
                                        <img  src="<?php echo WEBSITE_URL.'image.php?width=50px&height=50px&cropratio=1:1&image='.USER_PROFILE_IMAGE_URL.$record->image ?>">
                                    </div>
                                </a>
                            @else
                            	<img width="50px" height="50px" src="{{asset('img/user_pic.jpg')}}" alt="User">
                            @endif
							</td>
							<td>{{$record->full_name}}</td>
							<td>{{$record->email}}</td>
							<td>
								@if($record->is_active	==1)
									<span class="label label-success" >{{ trans("Activated") }}</span>
								@else
									<span class="label label-warning" >{{ trans("Deactivated") }}</span>
								@endif
							</td>
							<td>
								{{ date(config::get("Reading.date_format"),strtotime($record->created_at))}}
							</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="5" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif 
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
@stop
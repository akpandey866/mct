@extends('admin.layouts.default')
@section('content')
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">

<section class="content-header">
	<h1>
		{{ trans("Add New User") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/users/'.USER)}}">{{ trans("Users") }}</a></li>
		<li class="active">{{ trans("Add New User") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/users/add-user/'.$user_role_id,'class' => 'mws-form','files'=>'true']) }}
	{{ Form::hidden('user_role_id',$user_role_id) }}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('first_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('first_name',trans("First Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('first_name','',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('first_name'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('last_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('last_name',trans("Last Name").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('last_name','',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('last_name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('username')) ? 'has-error' : ''; ?>">
				{{  Form::label('username', trans("Username").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('username','',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('username'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('email')) ? 'has-error' : ''; ?>">
				{{ Form::label('email', trans("Email").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('email','',['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('email'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('username')) ? 'has-error' : ''; ?>">
				{{  Form::label('dob', trans("Date Of Birth"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('dob','', ['class' => 'form-control dob']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('username'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group fitnessTrainer<?php echo ($errors->first('gender')) ? 'has-error' : ''; ?>">
				{{  Form::label('gender', trans("Gender"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::radio('gender','male',true,array('id'=>'male_id')) }}
					{{ Form::label('male_id',trans("Male")) }}
				
					{{ Form::radio('gender','female',false,array('id'=>'female_id')) }}
					{{ Form::label('female_id',trans("Female")) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('gender'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('password')) ? 'has-error' : ''; ?>">
				{{ Form::label('password', trans("password").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::password('password',['class'=>'userPassword form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('password'); ?>
					</div>
				</div>
			</div>
		
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('confirm_password')) ? 'has-error' : ''; ?>">
			{{ Form::label('confirm_password', trans("Confirm Password").' *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::password('confirm_password',['class'=>'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('confirm_password'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('phone',trans("Phone"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text("phone",'', ['class'=>'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('phone'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('country')) ? 'has-error' : ''; ?>">
				{{  Form::label('country', trans("Country"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item country">
					{{ Form::select(
						 'country',
						 [null => 'Please Select Country'] + $countryList,
						 '',
						 ['id' => 'country_id','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('country'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="get_state_class">
				<div class="form-group state">
					{{ Form::label('state',trans("State"), ['class' => 'mws-form-label']) }}
					<div class="mws-form-item ">
						{{ Form::select(
							 'state',
							 [null => 'Please Select Stae'],
							 '',
							 ['id' => 'state_id','class'=>'form-control']
							) 
						}}
						<div class="error-message help-inline">
							<?php echo $errors->first('state'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="get_city_class">
				<div class="form-group">
					{{ Form::label('city',trans("City"), ['class' => 'mws-form-label']) }}
					<div class="mws-form-item ">
						{{ Form::select("city",[null=>trans("Please select city")],'', ['class'=>'form-control']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('city'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('address',trans("Address"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text("address",'', ['class'=>'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('address'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('zipcode',trans("Zip Code"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text("zipcode",'', ['class'=>'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('city'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('home_club',trans("Home Club"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::select("home_club",[null=>trans("Please select home club")]+Config::get('home_club'),'', ['class'=>'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('home_club'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('about_us',trans("How did you hear about us?"), ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::select("about_us",[null=>trans("Please select ")]+$aboutUsList,'', ['class'=>'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('about_us'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/users/add-user/'.$user_role_id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/users/',$user_role_id)}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script type="text/javascript">
	var state_old_id = "<?php echo old('state'); ?>";
	var city_old_id = "<?php echo old('city'); ?>";
$(function(){
	get_states();
	get_cities();
	$('.dob').datepicker({
		format 	: 'yyyy-mm-dd',
		endDate: "today",
		autoclose:true,
	});	
});
function get_states(){
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	var country_id = $('#country_id').val();
	$.ajax({ 
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: "{{ URL('admin/get-state') }}",
		type:'post',
		data:{'country_id':country_id,'state_old_id':state_old_id},
		success: function(r){ 
			$('.get_state_class').html(r);
			$('#loader_img').hide();
		}
	});
}
function get_cities(){
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	var state_id = "<?php echo old('state') ?>";
	$.ajax({ 
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: "{{ URL('admin/get-city') }}",
		type:'post',
		data:{'state_id':state_id,'city_old_id':city_old_id},
		success: function(r){ 
			$('.get_city_class').html(r);
			$('#loader_img').hide();
		}
	});
}
$('#country_id').on('change',function(){
	$('#loader_img').show();
	$('.help-inline').html('');
	$('.help-inline').removeClass('error');
	var country_id = $('#country_id').val();
	$.ajax({ 
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url: "{{ URL('admin/get-state') }}",
		type:'post',
		data:{'country_id':country_id},
		success: function(r){
			$('.get_state_class').html(r);
			$('#loader_img').hide();
		}
	});
});
</script>
@stop

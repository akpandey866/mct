@if(!empty($profile_pictures))
		@foreach($profile_pictures as $image)
			<li>
				{{ Form::radio('picture_radio',$image->id,'',['class'=>'profile_image_radio']); }}
				@if($image->profile_image != '' && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.$image->profile_image))
					<figure>
						<div class="usermgmt_image gtvnder_img">
							@if(file_exists(USER_PROFILE_IMAGE_ROOT_PATH.$image->profile_image)) 
								<img rel="{{$image->profile_image}}" class="img-circle vndr_imgs" src="<?php echo WEBSITE_URL.'image.php?width=120px&height=120px&image='.USER_PROFILE_IMAGE_URL.'/'.$image->profile_image ?>">
							@endif
						</div>
					</figure>
				@endif
			</li> 
		@endforeach
@else 
		{{trans('No profile image')}}
@endif

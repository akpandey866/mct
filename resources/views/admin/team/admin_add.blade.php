@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		{{ trans("Add New Team") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/team/')}}">{{ trans("Team") }}</a></li>
		<li class="active">{{ trans("Add New Team") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/team/admin-add-team','class' => 'mws-form','files'=>'true', 'id' => 'add_team']) }}
	<div class="row">
	
		<div class="col-md-6">
		   <div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
			{{ Form::label('name',trans("Team Name").' *', ['class' => 'mws-form-label']) }}
			<div class="mws-form-item">
				{{ Form::text('name','',['class' => 'form-control']) }}
				<div class="error-message help-inline">
					<?php echo $errors->first('name'); ?>
				</div>
			</div></div>
		</div>
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('club')) ? 'has-error' : ''; ?>">
				{{ Form::label('club', trans("Club").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select('club',[null => 'Please Select Club'] + $cludDetails,'',['id' => 'club','class'=>'form-control']) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('club'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('team_category')) ? 'has-error' : ''; ?>">
				{{ Form::label('team_category', trans("Team Category").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'team_category',
						 [null => 'Please Select Team Category'] + $teamCategory,
						 '',
						 ['id' => 'team_category','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('team_category'); ?>
					</div>
				</div>
			</div>
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('grade_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('grade_name', trans("Grade This Team Plays In").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item put_html">
					{{ Form::select('grade_name',[null => 'Please Select Grade Name'],'',['id' => 'grade_name','class'=>'form-control'])}}
					<div class="error-message help-inline">
						<?php echo $errors->first('grade_name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('type')) ? 'has-error' : ''; ?>">
				{{ Form::label('type', trans("Team Type").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item put_team_type">
					{{ Form::select(
						 'type',
						 [null => 'Please Select Team Type'],
						 '',
						 ['id' => 'type','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('type'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="button" value="{{ trans('Save') }}" class="btn btn-danger" onclick="save_team();">
			<a href="{{URL::to('admin/team/add-team/')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/team/')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script type="text/javascript">
$('#club').on('change',function(){
	$('#loader_img').show();
	var id = $(this).val();
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:' {{ route("Team.getGrade") }} ',
		'type':'post',
		data:{'id':id},
		async : true,
		success:function(response){ 
			$('.put_html').html(response);
		}
	});
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:' {{ route("Team.getTeamType") }} ',
		'type':'post',
		data:{'id':id},
		async : true,
		success:function(response){ 
			$('.put_team_type').html(response);
		}
	});
	$('#loader_img').hide();
});
function save_team(){
    $('#loader_img').show();
    $('.help-inline').html('');
    $('.help-inline').removeClass('error');
	var form = $('#add_team')[0];
	var formData = new FormData(form);
    $.ajax({
        url: '{{ route("Team.saveTeam") }}',
        type:'post',
        data: formData,
		processData: false,
		contentType: false,
        success: function(r){
            error_array     =   JSON.stringify(r);
            data            =   JSON.parse(error_array);
            if(data['success'] == 1) {
				show_message(data['message'],"success");
                window.location.href   =  "{{ route('team.index') }}";
            }else {
                $.each(data['errors'],function(index,html){
                    $("#"+index).next().addClass('error');
                    $("#"+index).next().html(html);
                });
                $('#loader_img').hide();
            }
            $('#loader_img').hide();
        }
    });
}
$('#add_team').each(function() {
		$(this).find('input').keypress(function(e) {
	       if(e.which == 10 || e.which == 13) {
				save_team();
				return false;
	        }
	    });
	});
</script>
@stop

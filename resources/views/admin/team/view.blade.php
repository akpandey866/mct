@extends('admin.layouts.default')
@section('content')
<section class="content-header">
	<h1>
		 {{ trans("Player Detail") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/player/')}}">{{ trans("Player") }}</a></li>
		<li class="active"> {{ trans("Player Detail") }}  </li>
	</ol>
</section>
<div class="box box-warning "> 
	<div class="row pad">
		<div class="col-md-12 col-sm-6">		
			<table class="table table-striped table-responsive">
				
				<thead>
					<tr style="background-color:#3c3f44; color:white;">
						<th  width="30%" height="50%" class="" colspan="2" style="font-size:14px;">Player INFORMATION</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Nick Name:</th>
						<td data-th='Full Name' class="txtFntSze">{{ isset($userDetails->nickname) ? $userDetails->nickname:'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Position:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($positionName) ? $positionName :'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Club Name:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($clubName) ? $clubName :'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Category:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($categoryName) ? $categoryName :'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">SValue:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($svalueName) ? $svalueName :'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">JValue:</th>
						<td data-th='Email' class="txtFntSze">{{ isset($jvalueName) ? $jvalueName :'' }}</td>
					</tr>
					<tr>
						<th  width="30%" class="text-right txtFntSze">Image:</th>
						<td data-th='Email' class="txtFntSze">@if(File::exists(PLAYER_IMAGE_ROOT_PATH.$userDetails->image))
									<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo PLAYER_IMAGE_URL.$userDetails->image; ?>">
										<div class="usermgmt_image">
											<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.PLAYER_IMAGE_URL.'/'.$userDetails->image ?>">
										</div>
									</a>
								@endif</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<style>
.about_me {
    word-wrap:break-word;
	word-break: break-all;
}
</style>
@stop

@extends('admin.layouts.default')
@section('content')
<!-- CKeditor start here-->
{{ Html::script('js/admin/ckeditor/ckeditor.js') }}
<!-- CKeditor ends-->
<section class="content-header">
	<h1>
		{{ trans("Edit Amount") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/admin-fundraiser')}}">Fundraiser</a></li>
		<li class="active">Edit Fundraiser Amount</li>
	</ol>
</section>

<section class="content"> 
{{ Form::open(['role' => 'form','url' => 'admin/admin-fundraiser/edit-amount/'.$result->id,'class' => 'mws-form']) }}
	<div class="row">
		<div class="col-md-6">	
			<div class="form-group <?php echo ($errors->first('entry_price')?'has-error':''); ?>">
				<div class="mws-form-row">
					{{ Form::label('entry_price',trans("Fundraising Amount").' *', ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						{{ Form::text('entry_price',$result->entry_price, ['class' => 'form-control']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('entry_price'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">	
			<div class="form-group <?php echo ($errors->first('fundraising_target')?'has-error':''); ?>">
				<div class="mws-form-row">
					{{ Form::label('fundraising_target',trans("Fundraising Target").' *', ['class' => 'mws-form-label']) }}
					<div class="mws-form-item">
						{{ Form::text('fundraising_target',$result->fundraising_target, ['class' => 'form-control']) }}
						<div class="error-message help-inline">
							<?php echo $errors->first('fundraising_target'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="mws-button-row">
				<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
				
				<a href="{{URL::to('admin/admin-fundraiser/edit-amount',$result->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans('Reset')  }}</a>
				
				<a href="{{URL::to('admin/admin-fundraiser')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
			</div>
		</div>
	</div>
{{ Form::close() }} 
</section>
<style>
	.textarea_resize {
		resize: vertical;
	}
</style>
@stop

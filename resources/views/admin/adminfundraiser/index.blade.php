@extends('admin.layouts.default')
@section('content')
<script>
	jQuery(document).ready(function(){
		$(".choosen_selct").chosen();
	});
</script>
<style>
.chosen-container-single .chosen-single{
	height:34px !important;
	padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>
	  {{ trans("Fundraiser") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Fundraiser") }}</li>
	</ol>
</section>
<section class="content"> 
	<div class="row">
		{{ Form::open(['method' => 'get','role' => 'form','url' => 'admin/admin-fundraiser','class' => 'mws-form']) }}
		{{ Form::hidden('display') }}
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<div class="form-group ">  
					{{ Form::select('game_mode',[null => 'Please Select Mode'] + Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''), ['class' => 'form-control game_mode choosen_selct']) }}
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<div class="form-group get_game_name">  
					{{ Form::select('game_name',array(''=>trans('Select Game')),((isset($searchVariable['game_name'])) ? $searchVariable['game_name'] : ''), ['class' => 'form-control choosen_selct']) }}
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="form-group ">  
					<div class="form-group ">  
					{{ Form::select('is_active',array(''=>trans('Select Status'),0=>'Inactive',1=>'Active'),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control choosen_selct']) }}
				</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-4">
				<button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
				<a href="{{URL::to('admin/admin-fundraiser')}}"  class="btn btn-primary"><i class='fa fa-refresh '></i> Reset</a>
			</div>
		{{ Form::close() }}
	</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="20%">
							{{
								link_to_route(
								"AdminFundraiser.index",
								trans("Game Name"),
								array(
									'sortBy' => 'game_name',
									'order' => ($sortBy == 'game_name' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'game_name' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'game_name' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
								"AdminFundraiser.index",
								 trans("Amount"),
								array(
									'sortBy' => 'entry_price',
									'order' => ($sortBy == 'entry_price' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'entry_price' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'entry_price' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
								"AdminFundraiser.index",
								 trans("Target"),
								array(
									'sortBy' => 'fundraising_target',
									'order' => ($sortBy == 'fundraising_target' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'fundraising_target' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'fundraising_target' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
								"AdminFundraiser.index",
								 trans("Raised"),
								array(
									'sortBy' => 'raised',
									'order' => ($sortBy == 'raised' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'raised' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'raised' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th>
							{{
								link_to_route(
								"AdminFundraiser.index",
								trans("Status"),
								array(
									'sortBy' => 'is_active',
									'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
									$query_string
								),
								array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th> 
						<th>{{ trans("Action") }}</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
					@if(!$result->isEmpty())
					@foreach($result as $record)
					<tr class="items-inner">
						<td data-th='{{ trans("game name") }}'>{{ $record->game_name }}</td>
						<td data-th='{{ trans("amont") }}'>{{ !empty($record->amount) ? $record->amount :'-' }}</td>
						<td data-th='{{ trans("target") }}'>{{ !empty($record->target) ? $record->target :'-' }}</td>
						<td data-th='{{ trans("raised") }}'>{{ !empty($record->raised) ? $record->raised :'-' }}</td>
						 <td data-th='{{ trans("status") }}'>
						@if($record->is_active	== 1)
							<span class="label label-success" >{{ trans("Activated") }}</span>
						@else
							<span class="label label-warning" >{{ trans("Deactivated") }}</span>
						@endif
						</td> 
						<td data-th='{{ trans("action") }}'>
							<a title="Edit Amount" href="{{URL::to('admin/admin-fundraiser/edit-amount/'.$record->id)}}" class="btn btn-primary"><span class="fa fa-pencil"></span>
							</a>
							<a title="Edit Message" href="{{URL::to('admin/admin-fundraiser/edit-message/'.$record->id)}}" class="btn btn-primary"><span class="fa fa-envelope"></span>
							</a>
						</td>
					</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="6" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif 
				</tbody>
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div>
</section> 
<script type="text/javascript">
	var game_name = "<?php echo !empty($searchVariable['game_name']) ? $searchVariable['game_name'] :'' ?>";
	var game_mode = "<?php echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode'] :'' ?>";
	if(game_name !="" || game_mode !=""){
		get_game_name();

	}
	function get_game_name(){
		$.ajax({
		 	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:' {{{ "admin-get-game-name" }}} ',
			'type':'post',
			data:{'mode':game_mode,'game_name':game_name},
			success:function(response){ 
				$(".get_game_name").html(response);
			}
		});
	}
	$(".game_mode").on('change',function(){
		var mode = $(this).val();
		$('#loader_img').show();
		 $.ajax({
		 	headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url:' {{{ "admin-get-game-name" }}} ',
			'type':'post',
			data:{'mode':mode,'game_name':game_name},
			success:function(response){ 
				$(".get_game_name").html(response);
			}
		}); 
		$('#loader_img').hide();
	});
</script>
@stop

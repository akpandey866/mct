{{ Html::style('css/admin/button.css') }}
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<a data-dismiss="modal" class="close" href="javascript:void(0)">
			<span style="float:right" class="no-ajax" aria-hidden="true">x</span>
			<span class="sr-only no-ajax"></span></a>	
			<h4 class="modal-title" id="myModalLabel">
				Email Details
			</h4>
		</div>
		<div class="modal-body">
			<div class="mws-panel-body no-padding dataTables_wrapper">
				<table class="table table-bordered table-responsive"  >
					<tbody>
						<?php 
						if(!empty($result)){  
							foreach($result as $value){ ?>
							<tr>
								<th><?php echo trans("Email To"); ?></th>
								<td data-th='Email To'> <?php echo $value->email_to;  ?></td>
							</tr>
							<tr>
								<th><?php echo trans("Email From"); ?></th>
								<td data-th='Email From'><?php  echo $value->email_from; ?></td>
							</tr>
							<tr>
								<th><?php echo trans("Subject"); ?></th>
								<td data-th='Subject'><?php echo  $value->subject; ?></td>
							</tr>
							<tr>
								<th valign='top'><?php echo trans("Message"); ?></th>
								<td data-th='Message'><?php  echo  $value->message; ?></td>
							</tr>
						<?php }	} ?>
					</tbody>		
				</table>
			</div>
			<div class="clearfix">&nbsp;</div>
		</div>
	</div>
</div>

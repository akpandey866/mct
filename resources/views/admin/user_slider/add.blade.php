@extends('admin.layouts.default')
@section('content') 
<script type="text/javascript" src="{{asset('js/admin/ckeditor/ckeditor.js')}}"></script>
<section class="content-header">
	<h1>
		{{ trans("Add New User Slider") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/user-slider')}}">{{ trans("User Slider") }}</a></li>
		<li class="active">{{ trans("Add New User Slider") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/user-slider/add-user-slider','class' => 'mws-form','files'=>'true']) }}
	<div class="row">
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Image</label>
				<div class="mws-form-item">
					{{ Form::file('image', array('accept' => 'image/*')) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('image'); ?>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				{{ Form::label('name', trans("User Name").' *', ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					<div class="mws-form-item">
					{{ Form::text("name",'', ['class' => 'form-control ','id' => 'name']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('club_name')) ? 'has-error' : ''; ?>">
				{{ Form::label('club_name', trans("Club Name"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text("club_name",'', ['class' => 'form-control ','id' => 'club_name']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('club_name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('description')) ? 'has-error' : ''; ?>">
				{{ Form::label('description', trans("Description"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::textarea("description",'', ['class' => 'form-control ','id' => 'description' ,"rows"=>3,"cols"=>3]) }}
					<script type="text/javascript">
						/* For CKEDITOR */
						CKEDITOR.replace( 'description',
						{
							height: 350,
							width: 507,
							filebrowserUploadUrl : '<?php echo URL::to('base/uploder'); ?>',
							filebrowserImageWindowWidth : '640',
							enterMode : CKEDITOR.ENTER_BR
						});
					</script>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/user-slider/add-user-slider/')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/user-slider/')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
@stop

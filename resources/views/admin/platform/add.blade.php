@extends('admin.layouts.default')
@section('content') 
<script type="text/javascript" src="{{asset('js/admin/ckeditor/ckeditor.js')}}"></script>
<section class="content-header">
	<h1>
		{{ trans("Add New Platfrom") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/platform')}}">{{ trans("Platfrom") }}</a></li>
		<li class="active">{{ trans("Add New Platfrom") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/platform/add-platform','class' => 'mws-form','files'=>'true']) }}
	<div class="row">
		<div class="col-md-6">
            <div class="form-group <?php echo ($errors->first('logo')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">logo</label>
				<div class="mws-form-item">
					{{ Form::file('logo', array('accept' => 'image/*')) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('logo'); ?>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('title')) ? 'has-error' : ''; ?>">
				{{ Form::label('title', trans("Title"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('title','', ['class' => 'form-control ','placeholder'=>'Title','id'=>'title']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('title'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
				{{ Form::label('description', trans("Description"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::textarea("description",'', ['class' => 'form-control ','id' => 'description' ,"rows"=>3,"cols"=>3]) }}
				</div>
			</div>
		</div>
	</div>

	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/platform/add-checklist')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/platform/')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
@stop

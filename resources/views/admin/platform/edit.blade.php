@extends('admin.layouts.default')
@section('content')
<script type="text/javascript" src="{{asset('js/admin/ckeditor/ckeditor.js')}}"></script>
<section class="content-header">
	<h1>
		{{ trans("Edit Platform") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/platform')}}">{{ trans("Platform") }}</a></li>
		<li class="active">{{ trans("Edit Platform") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/platform/edit-platform','class' => 'mws-form','files'=>'true']) }}
	{{ Form::hidden('id',$details->id) }}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Sponsor Logo</label>
				<div class="mws-form-item">
					{{ Form::file('logo', array('accept' => 'image/*')) }}
					<br />
					
					@if(File::exists(PLATFORM_IMAGE_ROOT_PATH.$details->logo))
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo PLATFORM_IMAGE_URL.$details->logo; ?>">
							<div class="usermgmt_image">
								<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.PLATFORM_IMAGE_URL.'/'.$details->logo ?>">
							</div>
						</a>
					@endif
					<div class="error-message help-inline">
						<?php echo $errors->first('club_logo'); ?>
						<div class="error-message help-inline">
						<?php echo $errors->first('club_logo'); ?>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('title')) ? 'has-error' : ''; ?>">
				{{ Form::label('title', trans("Title"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('title',isset($details->title) ? $details->title :'', ['class' => 'form-control ','placeholder'=>'Title','id'=>'title']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('title'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('description')) ? 'has-error' : ''; ?>">
				{{ Form::label('description', trans("Description"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::textarea('description',isset($details->description) ? $details->description :'', ['class' => 'form-control ','placeholder'=>'Description','id'=>'description']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('description'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Update') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/platform/edit-platform/'.$details->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/platform')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
@stop

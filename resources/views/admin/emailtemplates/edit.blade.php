@extends('admin.layouts.default')

@section('content')
<script src="{{ asset('js/bootstrap-dropdown.js') }}"></script>
<!--- ckeditor js start  here -->
<script type="text/javascript" src="{{asset('js/admin/ckeditor/ckeditor.js')}}"></script>
<!--- ckeditor js end  here -->

<section class="content-header">
	 <h1>
		Edit Email Template
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/email-manager')}}">Email Templates</a></li>
		<li class="active">Edit Email Template</li>
	</ol>
</section>

<section class="content"> 
	<div class="row pad">
		<div class="col-md-6">	
		{{ Form::open(['role' => 'form','url' => 'admin/email-manager/edit-template/'.$emailTemplate->id,'class' => 'mws-form']) }}
			<div class="form-group <?php echo ($errors->first('name')?'has-error':''); ?>">
					{{  Form::label('name','Name *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('name', $emailTemplate->name, ['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div>
			</div>
			<div class="form-group <?php echo ($errors->first('subject')?'has-error':''); ?>">
					{{  Form::label('subject','Subject *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::text('subject', $emailTemplate->subject, ['class' => 'form-control']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('subject'); ?>
					</div>
				</div>
			</div>
			<div class="form-group" style="display:none;">
				{{  Form::label('action', 'Action', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::select('action',$Action_options,$emailTemplate->action, ['class' => 'form-control','onchange'=>'constant()']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('action'); ?>
					</div>
				</div>
			</div>
			<div class="form-group <?php echo ($errors->first('constants')?'has-error':''); ?>">
				<table class="table table-bordered table-responsive">
					<tr>
						<td colspan="2" >
							{{  Form::label('constants','Constants', ['class' => 'mws-form-label']) }}
						</td>
					</tr>
					<tr>
						<td>
							{{ Form::select('constants', array(),'', ['empty' => 'Select one','class' => 'form-control','id'=>'constants']) }}
							<div class="error-message help-inline">
								<?php echo $errors->first('constants'); ?>
							</div>
						</td>
						<td>
							<a onclick = "return InsertHTML()" href="javascript:void(0)" class="btn  btn-success no-ajax pull-right"><i class="icon-white "></i>Insert Variable </a>
						</td>
					</tr>
					
				</table>
			</div>
			<div class="form-group <?php echo ($errors->first('body')?'has-error':''); ?>">
					{{  Form::label('body','Email Body *', ['class' => 'mws-form-label']) }}
				<div class="mws-form-item">
					{{ Form::textarea("body",$emailTemplate->body, ['class' => 'form-control','id' => 'body']) }}
					<span class="error-message help-inline">
						<?php echo $errors->first('body'); ?>
					</span>
				</div>
				<script type="text/javascript">
				/* For CKEDITOR */
					CKEDITOR.replace( 'body',
					{
						height: 350,
						width: 507,
						filebrowserUploadUrl : '<?php echo URL::to('admin/base/uploder'); ?>',
						filebrowserImageWindowWidth : '640',
						filebrowserImageWindowHeight : '480',
						enterMode : CKEDITOR.ENTER_BR
					});
				</script>
			</div>
			<div class="mws-button-row">
				<input type="submit" value="Save" class="btn btn-danger">
				
				<a href="{{URL::to('admin/email-manager/edit-template/'.$emailTemplate->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> Reset</a>
				
				<a href="{{URL::to('admin/email-manager')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans('Cancel')  }}</a>
			</div>
		{{ Form::close() }} 	
	</div>
</div>
</section>
<?php  $constant = ''; ?>
<script type='text/javascript'>
var myText = '<?php  echo $constant; ?>';
	$(function(){
		constant();
	});
	/* this function used for  insert contant, when we click on  insert variable button */
    function InsertHTML() {
		
		var strUser = document.getElementById("constants").value;
		
		if(strUser != ''){
			var newStr = '{'+strUser+'}';
			var oEditor = CKEDITOR.instances["body"] ;
			oEditor.insertHtml(newStr) ;	
		}
    }
	/* this function used for get constant,define in email template*/
	function constant() {
		var constant = document.getElementById("action").value;
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "<?php echo URL::to('admin/email-manager/get-constant')?>",
				type: "POST",
				data: { constant: constant},
				dataType: 'json',
				success: function(r){
					$('#constants').empty();
					$('#constants').append( '<option value="">-- Select One --</option>' );
					$.each(r, function(val, text) {
						var sel ='';
						if(myText == text)
						 {
						   sel ='selected="selected"';
						 }
						 
						$('#constants').append( '<option value="'+text+'" '+sel+'>'+text+'</option>');
					});	
			   }
			});
		return false; 
	}	
</script>
<style>
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		font-size: 14px !important;
		padding: 0px !important;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		vertical-align: top !important;
	}
	.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
		border: 0px !important;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
		border-top: 0px !important;
		padding: 0px !important;
	}
	.table-bordered {
		border: 0px !important;
	}
</style>
@stop

@extends('admin.layouts.default')
@section('content')
<section class="content">
    <div class="row">
        <!-- accordian ***************** -->
        <div class="col-sm-12">
            <h3>Set-up Checklist</h3>
            <div class="panel-group" id="accordion">

                @if(!$checklistDetails->isEmpty())
                    @foreach($checklistDetails as $key => $record) 
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">{{$record->title}}</a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse ">
                                <div class="panel-body">
                                    {!!$record->description !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

                <?php /*
                @if(Auth::guard('admin')->user()->is_game_activate == 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">Step 8: Minimum requirements to activate the game</a>
                        </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li>Create at least 12 player records.</li>
                                <li>Have at least 1 WK/3 BAT/2 AR/ 3 BWL as players.</li>
                                <li>Create at least one grade in game.</li>
                                <li>Create at least one team in game.</li>
                                <li>Create at least one fixture in game.</li>
                                <li>Create at least one prize in game.</li>
                                <li>Set the weekly Lockout Period for the game.</li>
                                <li>Pay the fees to activate the game.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endif
                */ ?>
                
            </div>
        </div>
    </div>
</section>
@stop
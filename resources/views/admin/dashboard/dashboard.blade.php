@extends('admin.layouts.default')
@section('content')
<div class="row pad" >
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
			<span class="info-box-icon bg-orange"><i class="ion-person"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">Game User</span>
			  <span class="info-box-number"> <?php echo $totalClubUser; ?> </span>
				
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="ion-person"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">User</span>
			  <span class="info-box-number"> <?php echo $totalFrontUser; ?> </span>
				
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
			<span class="info-box-icon bg-blue"><i class="ion-shuffle"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">Total Activated Game</span> 
			  <span class="info-box-number"> <?php echo $totalActivatedGame; ?> </span>
				
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
			<span class="info-box-icon bg-red"><i class="ion-shuffle"></i></span>
			<div class="info-box-content">
			  <span class="info-box-text">Total Inactive Game</span>
			  <span class="info-box-number"><?php echo $totalInActivatedGame; ?> </span>
			
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-green"><i class="fa fa-money "></i></span>

		<div class="info-box-content">
		  <span class="info-box-text">Game Income</span>
		  <span class="info-box-number"> $<?php echo $clubIncome; ?> </span>
			
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-purple"><i class="fa fa-money "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Players Income</span>
		  <span class="info-box-number"> $<?php echo $playersIncome; ?> </span>
			
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-purple"><i class="fa fa-flag "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Grades</span>
		  <span class="info-box-number"><?php echo $totalGrades; ?> </span>
			
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-purple"><i class="fa fa-group "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Teams</span>
		  <span class="info-box-number"><?php echo $totalTeams; ?> </span>
			
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-money "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Branding Fees</span>
		  <span class="info-box-number">$<?php echo $brandingFee; ?> </span>
			
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-money "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Games</span>
		  <span class="info-box-number"><?php echo $totalGame; ?> </span>
			
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-tint "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Active Games</span>
		  <span class="info-box-number"><?php echo $activeGame; ?> </span>
			
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-sitemap "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">In-Active Games</span>
		  <span class="info-box-number"><?php echo $inActiveGame; ?> </span>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-star "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Fixtures</span>
		  <span class="info-box-number"><?php echo $totalFixtures; ?> </span>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-tty "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Players</span>
		  <span class="info-box-number"><?php echo $totalPlayers; ?> </span>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-support "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Availability</span>
		  <span class="info-box-number"><?php echo $totalAvailability; ?> </span>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-id-badge "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Verify Users</span>
		  <span class="info-box-number"><?php echo $totalVerifyUsers; ?> </span>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-id-card "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Scorers</span>
		  <span class="info-box-number"><?php echo $totalScorers; ?> </span>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-map "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Sponsors</span>
		  <span class="info-box-number"><?php echo $totalSponsors; ?> </span>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-podcast "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Branding</span>
		  <span class="info-box-number"><?php echo $totalBranding; ?> </span>
		</div>
	  </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
	  <div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-inbox "></i></span>
		<div class="info-box-content">
		  <span class="info-box-text">Total Prizes</span>
		  <span class="info-box-number"><?php echo $totalPrize; ?> </span>
		</div>
	  </div>
	</div>
</div>
<div class="row pad">
     <div class="col-md-6">
        <div class="box box-danger box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Match Category</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <canvas id="pieChartMatch" style="height:250px"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-danger box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Match Types</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <canvas id="pieChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
<div class="row pad">
	<div class="col-lg-6 col-xs-12 col-sm-12">
	        <div class="box box-info">
	        	<div class="box-header with-border">
	                <h3 class="box-title">Fixtures</h3>

	                <div class="box-tools pull-right">
	                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	                    </button>
	                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	                </div>
	            </div>
	            <div class="box-body">
	                <div class="progress-group">
	                    <span class="progress-text">Completed </span>
	                    <span class="progress-number"><b>{{ admin_fixture_status_details(3) }}</b>/{{$fixtureCount}}</span>

	                    <div class="progress">
	                        <?php 
	                    	$completedP = 0;
	                    	if(!empty($fixtureCount) && !empty(admin_fixture_status_details(3))){
	                    		$completedP = admin_fixture_status_details(3) / $fixtureCount; 
	                    		$completedP = ceil($completedP*100);
	                    	}
	                    	?>
	                            <div class="progress-bar progress-bar-green" style="width: {{$completedP}}%"></div>
	                    </div>
	                </div>
	                <!-- /.progress-group -->
	                <div class="progress-group">
	                    <span class="progress-text">In-Progress</span>
	                    <span class="progress-number"><b>{{ admin_fixture_status_details(2) }}</b>/{{$fixtureCount}}</span>

	                    <div class="progress">
	                        <?php 
	                    	$inProgressP = 0;
	                    	if(!empty($fixtureCount) && !empty(admin_fixture_status_details(2))){ 
	                    		$inProgressP = admin_fixture_status_details(2) / $fixtureCount; 
	                    		$inProgressP = ceil($inProgressP*100); 
	                    	} 
	                    	?>
	                            <div class="progress-bar progress-bar-aqa" style="width: {{$inProgressP}}%"></div>
	                    </div>
	                </div>
	                <!-- /.progress-group -->
	                <div class="progress-group">
	                    <span class="progress-text">Not Yet Started</span>
	                    <span class="progress-number"><b>{{admin_fixture_status_details_not_started() }}</b>/{{$fixtureCount}}</span>

	                    <div class="progress">
	                        <?php 

	                    	$notStartedP = 0;
	                    	if(!empty($fixtureCount)){ 
	                    		$notStartedP = admin_fixture_status_details_not_started() / $fixtureCount; 
	                    		$notStartedP = ceil($notStartedP*100);
	                    	}
	                    	?>
	                            <div class="progress-bar progress-bar-red" style="width: {{$notStartedP}}%"></div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	<!-- Product List -->
	<div class="col-lg-6 col-xs-12 col-sm-12">
		<div class="portlet light portlet-fit bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-microphone font-dark hide"></i>
					<span class="caption-subject bold font-dark uppercase"> Recent Games</span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn green-haze btn-outline btn-circle btn-sm" href="{{{ URL::to('admin/club') }}}"> View All
						</a>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row pad">
					@if(!empty($recentGame))
						@foreach($recentGame as $key=>$record)
							<div class="col-md-4">
								<div class="mt-widget-4">
									<div class="mt-img-container">
										@if($record->club_logo != '' && File::exists(CLUB_IMAGE_ROOT_PATH.$record->club_logo))
											<img class="img-circle" src="<?php echo WEBSITE_URL.'image.php?width=183px&height=250px&cropratio=183:250&image='.CLUB_IMAGE_URL.'/'.$record->club_logo ?>">
										@else 
											<img class="img-circle" src="<?php echo WEBSITE_URL.'image.php?width=183px&height=250px&cropratio=183:250&image='.asset('img/admin/no_image.jpg') ?>">
									@endif
										 
									</div>
									@if($key	==	0)
										<div class="mt-container bg-purple-opacity">
											<div class="mt-head-title"> {{{ $record->game_name }}} </div>
											<div class="mt-body-icons">
												<a href="{{{ URL::to('admin/club/edit-club/'.$record->id) }}}">
													<i class=" fa fa-pencil"></i>
												</a>
												<a href="{{{ URL::to('admin/club/view-club/'.$record->id) }}}">
													<i class=" fa fa-eye"></i>
												</a>
												
											</div>
											<div class="mt-footer-button">
												<button type="button" class="btn btn-circle btn-danger btn-sm">{{{ $record->club_name }}}</button>
											</div>
										</div>
									@elseif($key	==	1)
										<div class="mt-container bg-green-opacity">
											<div class="mt-head-title"> {{{ $record->game_name }}} </div>
											<div class="mt-body-icons">
												<a href="{{{ URL::to('admin/club/edit-club/'.$record->id) }}}">
													<i class=" fa fa-pencil"></i>
												</a>
												<a href="{{{ URL::to('admin/club/view-club/'.$record->id) }}}">
													<i class=" fa fa-eye"></i>
												</a>
											</div>
											<div class="mt-footer-button">
												<button type="button" class="btn btn-circle blue-ebonyclay btn-sm">{{{ $record->club_name }}}</button>
											</div>
										</div>
									@else
										<div class="mt-container bg-dark-opacity">
											<div class="mt-head-title"> {{{ $record->game_name }}} </div>
											<div class="mt-body-icons">
												<a href="{{{ URL::to('admin/club/edit-club/'.$record->id) }}}">
													<i class=" fa fa-pencil"></i>
												</a>
												<a href="{{{ URL::to('admin/club/view-club/'.$record->id) }}}">
													<i class=" fa fa-eye"></i>
												</a>
											</div>
											<div class="mt-footer-button">
												<button type="button" class="btn btn-circle btn-success btn-sm">{{{ $record->club_name }}}</button>
											</div>
										</div>
									@endif
									
								</div>
							</div>
						@endforeach
					@else
						<center><label>No Record found</label></center>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row pad">
	<!-- club and front users tables -->
	<div class="col-lg-6 col-xs-12 col-sm-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-bubble font-dark hide"></i>
					<span class="caption-subject font-hide bold uppercase">Recent Game Users</span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn green-haze btn-outline btn-circle btn-sm" href=" {{{ URL::to('admin/users/'.CLUBUSER) }}} "> View All
						</a>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row pad">
					@if(isset($clubUser) && !empty($clubUser))
						@foreach($clubUser as $record)
								<div class="col-md-4">
									<!--begin: widget 1-1 -->
									<div class="mt-widget-1 dash_user">
										<div class="mt-icon">
											<a href="#">
												<i class="icon-plus"></i>
											</a>
										</div>
										<div class="mt-img">
											@if($record->image != '' && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.$record->image))
												<img class="" src="<?php echo WEBSITE_URL.'image.php?width=80px&height=80px&cropratio=1:1&image='.USER_PROFILE_IMAGE_URL.'/'.$record->image ?>">
											@else
												<img width="80px" height="80px" src="<?php echo WEBSITE_IMG_URL ?>admin/no_image.jpg">
											@endif
										</div>
										<div class="mt-body">
											<h3 class="mt-username"> <a href=" {{{ URL::to('admin/club/view-club/'.$record->id) }}} " >{{{ $record->full_name }}}</a> </h3>
											<p class="mt-user-title"> {{{ $record->created_at }}} </p>
										</div>
									</div>
									<!--end: widget 1-1 -->
								</div>
						@endforeach
					@else
						<center><label>No Record found</label></center>
					@endif
				</div>
			</div>
		</div>
	</div>
	<!-- Front Users -->
	<div class="col-lg-6 col-xs-12 col-sm-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-bubble font-dark hide"></i>
					<span class="caption-subject font-hide bold uppercase">Recent Users</span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn green-haze btn-outline btn-circle btn-sm" href=" {{{ URL::to('admin/users/'.USER) }}} "> View All
						</a>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row pad">
					@if(isset($frontUser) && !empty($frontUser))
						@foreach($frontUser as $record)
								<div class="col-md-4">
									<!--begin: widget 1-1 -->
									<div class="mt-widget-1 dash_user">
										<div class="mt-icon">
											<a href="#">
												<i class="icon-plus"></i>
											</a>
										</div>
										<div class="mt-img">
											@if($record->image != '' && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.$record->image))
												<img class="" src="<?php echo WEBSITE_URL.'image.php?width=80px&height=80px&cropratio=1:1&image='.USER_PROFILE_IMAGE_URL.'/'.$record->image ?>">
											@else
												<img width="80px" height="80px" src="<?php echo WEBSITE_IMG_URL ?>admin/no_image.jpg">
											@endif
										</div>
										<div class="mt-body">
											<h3 class="mt-username"> <a href=" {{{ URL::to('admin/users/view-user/'.$record->id) }}} " >{{{ $record->full_name }}}</a> </h3>
											<p class="mt-user-title"> {{{ $record->created_at }}} </p>
										</div>
									</div>
									<!--end: widget 1-1 -->
								</div>
						@endforeach
					@else
						<center><label>No Record found</label></center>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row pad">
	<div class="col-md-6">
        <div class="box box-danger box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Hear About Us</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-striped">
              <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Task</th>
                  <th>Progress</th>
                  <th style="width: 40px">Label</th>
                </tr>
                
                @if(!empty($aboutUsList))
                <?php $n=1; ?>
                @foreach($aboutUsList as $key=>$value)
                <tr>
                  <td>{{$n}}.</td>
                  <td>{{$value}}</td>
                  <td>
                    <div class="progress progress-xs">
                        <?php $hereAboutUsCount = get_here_about_us($key); ?>
                      <div class="progress-bar progress-bar-danger" style="width: {{$hereAboutUsCount}}%"></div>
                    </div>
                  </td>
                  <td><span class="badge bg-red">{{ round($hereAboutUsCount)}}%</span></td>
                </tr>
                 <?php $n++; ?>
                @endforeach
                @else
                <tr><td colspan="4">No data available in table</td></tr>
                @endif

              </tbody>
            </table>
          </div>
        </div>
    </div>
    <div class="col-md-6">
            <div class="box box-danger box-info">
              <div class="box-header with-border">
                    <h3 class="box-title">Countries</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Task</th>
                      <th>No. of Country</th>
                    </tr>
                  
                    @if(!$usersCountries->isEmpty())
                    <?php $n=1; ?>
                    @foreach($usersCountries as $key=>$value)
                    <tr>
                      <td>{{$n}}.</td>
                      <td>{{$value->country_name}}</td>
                      <td><span class="badge bg-red">{{$value->country_count}}</span></td>
                    </tr>
                     <?php $n++; ?>
                    @endforeach
                    @else
                        <tr><td colspan="4">No data available in table</td></tr>
                    @endif

                  </tbody>
                </table>
              </div>
            </div>
        </div>
</div>
<div class="row">
	<div class="col-md-6">
        <div class="box box-danger box-info">
            <div class="box-header with-border">
                <h3 class="box-title">States</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-striped">
              <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Task</th>
                  <th>No. of States</th>
                </tr>
                 @if(!$usersStates->isEmpty())
                <?php $n=1; ?>
                @foreach($usersStates as $key=>$value)
                <tr>
                  <td>{{$n}}.</td>
                  <td>{{$value->states_name}}</td>
                  <td><span class="badge bg-red">{{$value->states_count}}</span></td>
                </tr>
                 <?php $n++; ?>
                @endforeach
                @else
                <tr><td colspan="4">No data available in table</td></tr>
                @endif

              </tbody>
            </table>
          </div>
        </div>
    </div>
</div>
  <div class="row pad">
    	<div class="col-md-12">
    		<div class="box box-danger box-info">
	    		<div class="box-header with-border">
	                <h3 class="box-title">Players</h3>

	                <div class="box-tools pull-right">
	                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	                    </button>
	                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	                </div>
	            </div>
	            <div class="box-body">
		            <div class="panel with-nav-tabs panel-default">
		                <div class="panel-heading">
		                        <ul class="nav nav-tabs">
		                            <li class="active"><a href="#tab1default" data-toggle="tab">All Player</a></li>
                                 <!--    <li class=""><a href="#tab3default" data-toggle="tab">Player Trade</a></li> -->
		                        </ul>
		                </div>
		                <div class="panel-body">
		                    <div class="tab-content">
		                        <div class="tab-pane fade active in" id="tab1default">
		                        	<div class="box">
							            <!-- <div class="box-header">
							              <h3 class="box-title">Data Table With Full Features</h3>
							            </div> -->
							            <!-- /.box-header -->
							            <div class="box-body">
							              <table id="example1" class="table table-bordered table-striped">
							                <thead>
							                <tr>
							                  <th>Players</th>
							                  <th>RS</th>
							                  <th>4S</th>
							                  <th>6S</th>
							                  <th>OVRS</th>
							                  <th>MDNS</th>
							                  <th>WKS</th>
							                  <th>CS</th>
							                  <th>CWKS</th>
							                  <th>STS</th>
							                  <th>RODS</th>
							                  <th>ROAS</th>
							                  <th>DKS</th>
							                  <th>HT</th>
							                  <th>FP</th>
							                </tr>
							                </thead>
							                <tbody>
							                	@if(!$playerScoreSum->isEmpty())
												@foreach($playerScoreSum as $key => $record)  
									                <tr>
									                  <td>{{!empty($record->player_name) ? $record->player_name :'-'}}</td>
									                  <td>{{!empty($record->runs) ? $record->runs :'-'}}</td>
									                  <td>{{!empty($record->fours) ? $record->fours :'-'}}</td>
									                  <td>{{!empty($record->sixes) ? $record->sixes :'-'}}</td>
									                  <td>{{!empty($record->overs) ? $record->overs :'-'}}</td>
									                  <td>{{!empty($record->mdns) ? $record->mdns :'-'}}</td>
									                  <td>{{!empty($record->wks) ? $record->wks :'-'}}</td>
									                  <td>{{!empty($record->cs) ? $record->cs :'-'}}</td>
									                  <td>{{!empty($record->cwks) ? $record->cwks :'-'}}</td>
									                  <td>{{!empty($record->sts) ? $record->sts :'-'}}</td>
									                  <td>{{!empty($record->rods) ? $record->rods :'-'}}</td>
									                  <td>{{!empty($record->roas) ? $record->roas :'-'}}</td>
									                  <td>{{!empty($record->dks) ? $record->dks :'-'}}</td>
									                  <td>{{!empty($record->hattrick) ? $record->hattrick :'-'}}</td>
									                  <td>{{!empty($record->fantasy_points) ? $record->fantasy_points :'-'}}</td>
									                </tr>
									            @endforeach
												@else
													<tr>
														<td class="alignCenterClass" colspan="15" >{{ trans("No record is yet available.") }}</td>
													</tr>
												@endif 
							                </tbody>
							              </table>
							            </div>
							            <div class="box-footer clearfix">	
											<div class="col-md-3 col-sm-4 "></div>
											<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $playerScoreSum])</div>
										</div>
							          </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
	            </div>
            </div>
        </div>
    </div>
<!-- club and front users tables -->
<div class="row pad">
	<!-- Fitness Enthusiast Users Graph-->
	<div class="col-md-12 col-sm-12">
		<div class="box box-info">
			<div id="info1"></div>
			<div class="portlet-title">	
				<div class="caption">
					<span class="caption-subject font-hide bold uppercase">Game Users</span>
					<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse" type="button">
						<i class="fa fa-minus"></i>
					</button>
					<button class="btn btn-box-tool" data-widget="remove" type="button">
						<i class="fa fa-times"></i>
					</button>
					</div>
				</div>
				<?php /**/?>
			</div>
			<div class="box-body" style="display: block;padding:15px;">  
				<div id="newBarChart">
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row pad">
	<div class="col-md-12 col-sm-12">	
		<div class="box box-info">
			<div id="info1"></div>
			<div class="portlet-title">	
				<div class="caption">
					<span class="caption-subject font-hide bold uppercase">Users</span>
					<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse" type="button">
						<i class="fa fa-minus"></i>
					</button>
					<button class="btn btn-box-tool" data-widget="remove" type="button">
						<i class="fa fa-times"></i>
					</button>
					</div>
				</div>
			</div>
			<div class="box-body" style="display: block;padding:15px;">  
				<div id="newBarChart1">
				</div>
			</div>
		</div>
	</div>
</div>
{{Html::script('js/admin/d3/d3.v3.min.js') }}
{{Html::script('js/admin/d3/d3.tip.v0.6.3.js') }}
{{ Html::style('css/admin/d3/d3.css') }}
{{ Html::style('css/admin/dashboard.css') }}
<script src="{{ asset('js/admin/Chart.js') }}"></script>
<script type="text/javascript">
//Graph For Fitness Enthusiast User
//----------------------------------------------------------------
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 1050 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

var formatPercent = d3.format("1.0");

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .55);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
	.orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
	.tickFormat(d3.format("d"));
    //.tickFormat(formatPercent);

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>Total Game Users:</strong> <span style='color:red'>" + d.frequency + "</span>";
  });

 var tip1 = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>Total Users:</strong> <span style='color:red'>" + d.frequency + "</span>";
  })

var svg = d3.select("#newBarChart").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.call(tip);

// The new data variable.
var data = [
  <?php foreach($allClubUsers as $user){?>
  {letter: "<?php echo  $user['month']; ?>", frequency: <?php echo  $user['users']; ?>},
  <?php }  ?>
];

// The following code was contained in the callback function.
x.domain(data.map(function(d) { return d.letter; }));
y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
  .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("Game Users");

svg.selectAll(".bar")
    .data(data)
  .enter().append("rect")
    .attr("class", "bar")
    .attr("x", function(d) { return x(d.letter); })
    .attr("width", x.rangeBand())
    .attr("y", function(d) { return y(d.frequency); })
    .attr("height", function(d) { return height - y(d.frequency); })
    .on('mouseover', tip.show)
    .on('mouseout', tip.hide)

/*Front Users Graph*/
	// Graph For Front User
//----------------------------------------------------------------
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 1050 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

var formatPercent = d3.format("1.0");

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .55);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
	.orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
	.tickFormat(d3.format("d"));
    //.tickFormat(formatPercent);

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<strong>Total Users:</strong> <span style='color:red'>" + d.frequency + "</span>";
  })

var svg = d3.select("#newBarChart1").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.call(tip);

// The new data variable.
var data = [
  <?php foreach($frontUsers as $user){ ?>
  {letter: "<?php echo  $user['month']; ?>", frequency: <?php echo  $user['users']; ?>},
  <?php } ?>
];

// The following code was contained in the callback function.
x.domain(data.map(function(d) { return d.letter; }));
y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
  .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("Users");

svg.selectAll(".bar")
    .data(data)
  .enter().append("rect")
    .attr("class", "bar")
    .attr("x", function(d) { return x(d.letter); })
    .attr("width", x.rangeBand())
    .attr("y", function(d) { return y(d.frequency); })
    .attr("height", function(d) { return height - y(d.frequency); })
    .on('mouseover', tip.show)
    .on('mouseout', tip.hide)


/*Match type for senior game section start here*/
var pieChartCanvasMatch = $('#pieChartMatch').get(0).getContext('2d');
var pieChartMatch = new Chart(pieChartCanvasMatch);
var PieDataMatch = [{
        value: "{{$seniormens}}",
        color: '#f56954',
        highlight: '#f56954',
        label: 'Senior Mens'
    }, {
        value: "{{$seniorWomens}}",
        color: '#00a65a',
        highlight: '#00a65a',
        label: 'Senior Womens'
    }, {
        value: "{{$veterans}}",
        color: '#f39c12',
        highlight: '#f39c12',
        label: 'Veterans'
    },
    {
        value: "{{$juniorBoys}}",
        color: '#00c0ef',
        highlight: '#00c0ef',
        label: 'Junior Boys'
    },
    {
        value: "{{$juniorGirls}}",
        color: '#932ab6',
        highlight: '#932ab6',
        label: 'Junior Girls'
    }

    ];
var pieOptionsMatch = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //String - A legend template
        legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
pieChartMatch.Doughnut(PieDataMatch, pieOptionsMatch);	
/*Match category for senior game section finish here*/
/*Match Type section start here*/
 var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
        var pieChart = new Chart(pieChartCanvas);
        var PieData = [{
            value: "{{$oneDayMatch}}",
            color: '#00a65a',
            highlight: '#00a65a',
            label: '1-Day'
        }, {
            value: "{{$twoDayMatch}}",
            color: '#00c0ef',
            highlight: '#00c0ef',
            label: '2-Day'
        }];
        var pieOptions = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth: 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //String - A legend template
                legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions); 
/*Match Type section finish here*/
</script>
@stop
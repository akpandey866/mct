@extends('admin.layouts.default') 
@section('content')
<style type="text/css">
    .info_box_custom {
        max-height: 200px;
        min-height: 200px;
        width: 100%;
        overflow-y: auto;
    }
    
    .box.box-info {
        background: white;
        padding: 0 10px 10px 10px;
    }
    .chosen-container
	{
	    width: 100% !important;
	}
</style>
{{ Html::style('datatables.net-bs/css/dataTables.bootstrap.min.css') }}
<!-- <div class="row pad" >
	<div class="col-md-12 col-sm-12 col-xs-12"> -->
<!-- <center> -->
<!-- <div class="info-box">
				<center>
				<div class="bg-red error_class"> {!! implode('', $errors->all('<div style="margin-left: 10px;">:message</div>')) !!}</div>
				<div class="info-box-content">
					{{ Form::open(['role' => 'form','url' => 'admin/activate-game','class' => 'mws-form','files'=>'true', 'id' => 'activate_game']) }}
					<input type="button" value="{{ trans('Activate Game') }}" class="btn btn-success btn-lg flot-left" onclick="activateGame();">
					{{ Form::close() }}
				</div>
				</center>
			</div> -->
<!-- </center> -->
<!-- 	</div>
</div> -->

<section class="content">
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-orange"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Users</b> <br/>(Total Users)</span>
                    <span class="info-box-number">{{$total_user_cnt}}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Fixtures</b>  <br/>(Total Fixtures)</span>
                    <span class="info-box-number">{{$total_fixture_cnt}}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Teams</b> <br/>(Total Teams)</span>
                    <span class="info-box-number">{{$total_team_cnt}}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Players</b> <br/>(Total Players)</span>
                    <span class="info-box-number">{{$total_player_cnt}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Grades</b> <br/>(Total Grades)</span>
                    <span class="info-box-number">{{$totalGrades}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Availability </b> <br/>(Total Availability)</span>
                    <span class="info-box-number">{{$totalAvailability}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Verify Users  </b> <br/>(Total Verify Users)</span>
                    <span class="info-box-number">{{$totalVerifyUsers}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Scorers</b> <br/>(Total Scorers)</span>
                    <span class="info-box-number">{{$totalScorers}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-orange"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Sponsors </b> <br/>(Total Sponsors)</span>
                    <span class="info-box-number">{{$totalSponsors}}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Branding  </b></span>
                    <span class="info-box-number">{{!empty($totalBrandingActive) ? 'Activated' :'In-Active'}}</span>
                </div>
            </div>
        </div>
@if(!empty($cur_gw_dta))
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Current GW {{$cur_gw_dta['current']}}</b></span>
                    <!-- GW#5> Mon 00:00am, Dec 1 2019– Sun 11:59pm, Dec 8 2019 -->
                    
    <span>{{($cur_gw_dta['gw_start'])->isoFormat('ddd h:mma, MMM D YYYY')}}-{{($cur_gw_dta['gw_end'])->isoFormat('ddd h:mma, MMM D YYYY')}}</span>
    
                </div>
            </div>
        </div>
@endif

   <!--      <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><b>Branding  </b> <br/>(Total In-Active Branding)</span>
                    <span class="info-box-number">{{$totalBrandingInActive}}</span>
                </div>
            </div>
        </div> -->
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
            	<div class="box-header with-border">
                    <h3 class="box-title">Fixtures</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
	                <div class="progress-group">
	                    <span class="progress-text">Completed </span>
	                    <span class="progress-number"><b>{{ fixture_status_details(3) }}</b>/{{$fixtureCount}}</span>

	                    <div class="progress">
	                        <?php 
	                    	$completedP = 0;
	                    	if(!empty($fixtureCount) && !empty(fixture_status_details(3))){
	                    		$completedP = fixture_status_details(3) / $fixtureCount; 
	                    		$completedP = round($completedP*100);
	                    	}
	                    	?>
	                            <div class="progress-bar progress-bar-green" style="width: {{$completedP}}%"></div>
	                    </div>
	                </div>
	                <!-- /.progress-group -->
	                <div class="progress-group">
	                    <span class="progress-text">In-Progress</span>
	                    <span class="progress-number"><b>{{ fixture_status_details(2) }}</b>/{{$fixtureCount}}</span>

	                    <div class="progress">
	                        <?php 
	                    	$inProgressP = 0;
	                    	if(!empty($fixtureCount) && !empty(fixture_status_details(2))){
	                    		$inProgressP = fixture_status_details(2) / $fixtureCount; 
	                    		$inProgressP = round($inProgressP*100);
	                    	}
	                    	?>
	                            <div class="progress-bar progress-bar-aqa" style="width: {{$inProgressP}}%"></div>
	                    </div>
	                </div>
	                <!-- /.progress-group -->
	                <div class="progress-group">
	                    <span class="progress-text">Not Yet Started</span>
	                    <span class="progress-number"><b>{{fixture_status_details_not_started() }}</b>/{{$fixtureCount}}</span>

	                    <div class="progress">
	                        <?php 

	                    	$notStartedP = 0;
	                    	if(!empty($fixtureCount)){ 
	                    		$notStartedP = fixture_status_details_not_started() / $fixtureCount; 
	                    		$notStartedP = round($notStartedP*100);
	                    	}
	                    	?>
	                            <div class="progress-bar progress-bar-red" style="width: {{$notStartedP}}%"></div>
	                    </div>
	                </div>
                </div>
            </div>
        </div>
     
         <div class="col-md-6">
            <div class="box box-danger box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Fundraiser </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
               <!--  <div class="box-body">
                    <canvas id="pieChartFundraiser" style="height:250px"></canvas>

                </div> -->
                <div class="box-body">
                    <span class="progress-text">Amount(${{$fundraiserAmount}}) </span>
                  <div class="progress">
                    <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{$fundraiserAmountPercentage}}%">
                      <span class="sr-only">40% Complete (success)</span>
                    </div>
                  </div>
                  <span class="progress-text">Target(${{$fundraiserTarget}}) </span>
                  <div class="progress">
                    <div class="progress-bar progress-bar-aqua" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                      <span class="sr-only">20% Complete</span>
                    </div>
                  </div>
                  <span class="progress-text">Raised(${{$fundraiserRaised}}) </span>
                  <div class="progress">
                    <div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{$fundraiserRaisedPercentage}}%">
                      <span class="sr-only">60% Complete (warning)</span>
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-danger box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Match Category</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <canvas id="pieChartMatch" style="height:250px"></canvas>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-danger box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Match Types</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <canvas id="pieChart" style="height:250px"></canvas>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
       
    </div>
    <div class="row pad">
    	<div class="col-md-12">
    		<div class="box box-danger box-info">
	    		<div class="box-header with-border">
	                <h3 class="box-title">Players</h3>

	                <div class="box-tools pull-right">
	                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
	                    </button>
	                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	                </div>
	            </div>
	            <div class="box-body">
		            <div class="panel with-nav-tabs panel-default">
		                <div class="panel-heading">
		                        <ul class="nav nav-tabs">
		                            <li class="active"><a href="#tab1default" data-toggle="tab">All Player</a></li>
                                 <!--    <li class=""><a href="#tab3default" data-toggle="tab">Player Trade</a></li> -->
		                        </ul>
		                </div>
		                <div class="panel-body">
		                    <div class="tab-content">
		                        <div class="tab-pane fade active in" id="tab1default">
		                        	<div class="box">
							            <!-- <div class="box-header">
							              <h3 class="box-title">Data Table With Full Features</h3>
							            </div> -->
							            <!-- /.box-header -->
							            <div class="box-body">
							              <table id="example1" class="table table-bordered table-striped">
							                <thead>
							                <tr>
							                  <th>Players</th>
							                  <th>RS</th>
							                  <th>4S</th>
							                  <th>6S</th>
							                  <th>OVRS</th>
							                  <th>MDNS</th>
							                  <th>WKS</th>
							                  <th>CS</th>
							                  <th>CWKS</th>
							                  <th>STS</th>
							                  <th>RODS</th>
							                  <th>ROAS</th>
							                  <th>DKS</th>
							                  <th>HT</th>
							                  <th>FP</th>
							                </tr>
							                </thead>
							                <tbody>
							                	@if(!$playerScoreSum->isEmpty())
												@foreach($playerScoreSum as $key => $record)  
									                <tr>
									                  <td>{{!empty($record->player_name) ? $record->player_name :'-'}}</td>
									                  <td>{{!empty($record->runs) ? $record->runs :'-'}}</td>
									                  <td>{{!empty($record->fours) ? $record->fours :'-'}}</td>
									                  <td>{{!empty($record->sixes) ? $record->sixes :'-'}}</td>
									                  <td>{{!empty($record->overs) ? $record->overs :'-'}}</td>
									                  <td>{{!empty($record->mdns) ? $record->mdns :'-'}}</td>
									                  <td>{{!empty($record->wks) ? $record->wks :'-'}}</td>
									                  <td>{{!empty($record->cs) ? $record->cs :'-'}}</td>
									                  <td>{{!empty($record->cwks) ? $record->cwks :'-'}}</td>
									                  <td>{{!empty($record->sts) ? $record->sts :'-'}}</td>
									                  <td>{{!empty($record->rods) ? $record->rods :'-'}}</td>
									                  <td>{{!empty($record->roas) ? $record->roas :'-'}}</td>
									                  <td>{{!empty($record->dks) ? $record->dks :'-'}}</td>
									                  <td>{{!empty($record->hattrick) ? $record->hattrick :'-'}}</td>
									                  <td>{{!empty($record->fantasy_points) ? $record->fantasy_points :'-'}}</td>
									                </tr>
									            @endforeach
												@else
													<tr>
														<td class="alignCenterClass" colspan="15" >{{ trans("No record is yet available.") }}</td>
													</tr>
												@endif 
							                </tbody>
							              </table>
							            </div>
							            <!-- /.box-body -->
							          </div>
		                        </div>
		                    <div class="tab-pane fade" id="tab2default">
		                    	<div class="form-group "> 
                                    <div class="row"><div class="col-md-6">{{ Form::select('player_id',[null => 'Please Select Player'] +$playerList,((isset($searchVariable['player_id'])) ? $searchVariable['player_id'] : ''), ['class' => 'form-control chosen_select','id'=>'player_id']) }}</div></div> 
									
								</div>
		                    	<div class="box">
						            <div class="box-body addPlayerLists">

						            </div>
					       		</div>
		                    </div>





		                    </div>
		                </div>
		            </div>
	            </div>
            </div>
        </div>
    </div>



<div class="row">
      <div class="col-md-6">
        <div class="box box-danger box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Top Players (GWK)</h3>
            <div class="box-tools pull-right">
              <!-- <span class="label label-danger">8 New Members</span> -->
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">
              
                 
                        {{ Form::open(array('name' => 'myFormName' )) }}
                        <div class="row">
                            <div class="col-sm-12">
                                <select class="custom-select gw_no" name="gw_no" style="width: 10rem; " >
                                    <!-- <option value="" >Choose GW</option> -->
                                    @for ($i = 0; $i < $most_last_gw; $i++)
                                        <option {{isset($gw_no) && $gw_no == $i ? 'selected' : ''}} value="{{$i}}">GW{{$i+1}}</option>
                                    @endfor
                                </select>
                                <div class="table-responsive table-mobile">
                                    <table  id="example_player_gwpt"  width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Player Name</th>
                                                <th>GW Pts</th>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            
                                                <?php $i = 1;  // dump($temp_gw_data); die;  ?>
                                            @if(!empty($temp_gw_data))
                                                @foreach($temp_gw_data as $key=>$val)
                                                    @if(!empty($val) && !empty($val['player_data']))

                                                       
                                                    <tr>
                                                        <td>{{$i++}}</td>
                                                        <td>{{!empty($val['player_data']) ? $val['player_data']->full_name : ''}}</td>
                                                        <td>{{$val['player_point']}}</td>
                                                    </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                            
                                    
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            

                            
                        </div>
                    {{ Form::close() }}




          </div>
          
        </div>
      </div>
      <div class="col-md-6">
        <div class="box box-danger box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Powers</h3>
            <div class="box-tools pull-right">
              <!-- <span class="label label-danger">8 New Members</span> -->
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="table-responsive table-mobile">
                <table id="examplexyz" width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table states_table mb-0">
                    <thead>
                    <tr>
                        <!-- <th width="70px">Rank</th> -->
                        <th class="colmn1">Player</th>
                        <th class="colmn2">Trades</th>
                        <th class="colmn2">Triple Cap</th>
                        <th class="colmn2">12th Man</th>
                        <th class="colmn2">Dealer</th>
                        <th class="colmn2">Flipper</th>
                        <th class="colmn2">Shield Steal</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @if(!empty($all_users) && $all_users->isNotEmpty())
                            @foreach($all_users as $val)
                            <tr>
                                <td>{{$val->full_name}}</td>
                                <td>{{ !empty($trades_count[$val->id]) ? $trades_count[$val->id] : 0 }}</td>
                                <td>{{ !empty($capton_card[$val->id]) ? $capton_card[$val->id] : 0 }}</td>
                                <td>{{ !empty($twelve_man_card[$val->id]) ? $twelve_man_card[$val->id] : 0 }}</td>
                                <td>{{ !empty($dealer_card[$val->id]) ? $dealer_card[$val->id] : 0 }}</td>
                                <td>{{ !empty($flipper_card[$val->id]) ? $flipper_card[$val->id] : 0 }}</td>
                                <td>{{ !empty($shield_steal[$val->id]) ? $shield_steal[$val->id] : 0 }}</td>
                            </tr>
                            @endforeach
                       @else 
                    <tr><td colspan="7">No players have yet been added to the game.</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-danger box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Trades</h3>
                <div class="box-tools pull-right">
                  <!-- <span class="label label-danger">8 New Members</span> -->
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="table-responsive table-mobile">
                    <table width="100%" id="exampleTrades" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                        <thead>
                            <tr>
                                <th>Player</th>
                                <th>Trade In</th>
                                <th>Trade Out</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1;  ?>
                        @if(!empty($top_trade_in))
                        @foreach($top_trade_in as $key => $value)
                        <tr>
                            <!-- <td>{{$i++}}</td> -->
                            <td>
                            {{!empty($value->full_name) ? $value->full_name : ''}}</a>
                            </td>
                            <td>{{!empty($top_trade_in_count[$value->id]) ? $top_trade_in_count[$value->id] : 0}}</td>
                            <td>{{!empty($top_trade_out[$value->id]) ? $top_trade_out[$value->id] : 0}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">No record is yet available.</td>
                        </tr>
                        @endif
                    </tbody>
                    </table>
                </div>
              </div>
            </div>
      </div>
       <div class="col-md-6">
            <div class="box box-danger box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Captain</h3>
                <div class="box-tools pull-right">
                  <!-- <span class="label label-danger">8 New Members</span> -->
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="table-responsive table-mobile">
                    <table width="100%" id="exampleCaptains" cellpadding="0" cellspacing="0" border="0" class="table table-striped top_trades_table mb-0">
                        <thead>
                            <tr>
                                <th>Player</th>
                                <th>Captain</th>
                                <th>Vice Captain</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(!empty($top_captain_in))
                            @foreach($top_captain_in as $key => $value)
                                <tr>
                                    <td>
                                    {{!empty($value->full_name) ? $value->full_name : ''}}</a>
                                    </td>
                                    <td>{{!empty($top_captain_in_count[$value->id]) ? $top_captain_in_count[$value->id] : 0}}</td>
                                    <td>{{!empty($top_vice_captain_out[$value->id]) ? $top_vice_captain_out[$value->id] : 0}}</td>
                                </tr>
                            @endforeach
                        @else
                        <tr>
                            <td colspan="3">No record is yet available.</td>
                        </tr>
                        @endif
                    </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="box box-danger box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Recent Users</h3>
            <div class="box-tools pull-right">
              <!-- <span class="label label-danger">8 New Members</span> -->
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <ul class="users-list clearfix">
              @if(!empty($recentUsers))
                @foreach($recentUsers as $list)
                    <li>
                        @if(!empty($list->image) && File::exists(USER_PROFILE_IMAGE_ROOT_PATH.$list->image))
                        <img src="<?php echo WEBSITE_URL.'image.php?width=85.25px&height=85.25px&cropratio=1:1&image='.USER_PROFILE_IMAGE_URL.$list->image ?>" alt="User Image">
                        @else
                        <img src="<?php echo WEBSITE_URL.'image.php?width=85.25px&height=85.25px&cropratio=1:1&image='.WEBSITE_IMG_URL.'admin/no_image.jpg' ?>" alt="User Image">
                        @endif
                        <a class="users-list-name" href="{{url('admin/users/view-user/'.$list->id)}}">{{$list->full_name}}</a>
                        <span class="users-list-date">{{ Date('d M Y',strtotime($list->created_at))}}</span>
                    </li>
                @endforeach
              @endif
            </ul>
          </div>
          <div class="box-footer text-center">
            <a href="{{url('admin/users/2')}}" class="uppercase">View All Users</a>
          </div>
        </div>
       
      </div>
      <div class="col-md-6">
          <div class="box box-danger box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Users</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Task</th>
                      <th>Progress</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td>Total Active Users</td>
                      <td>
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-danger" style="width: {{$totalActiveUsersPercentage}}%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-red">{{round($totalActiveUsersPercentage)}}%</span></td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>Total Inactive Users</td>
                      <td>
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-yellow" style="width: {{$totalInActiveUsersPercentage}}%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-yellow">{{round($totalInActiveUsersPercentage)}}%</span></td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>Subscribed/Notifications</td>
                      <td>
                        <div class="progress progress-xs progress-striped active">
                          <div class="progress-bar progress-bar-primary" style="width: {{$totalSubscribedPercentage}}%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-light-blue">{{round($totalSubscribedPercentage)}}%</span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-danger box-info">
               <div class="box-header with-border">
                    <h3 class="box-title">Users</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Task</th>
                      <th>Progress</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                    <tr>
                      <td>1.</td>
                      <td>Male</td>
                      <td>
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-danger" style="width: {{$mPercentageUsers}}%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-red">{{round($mPercentageUsers)}}%</span></td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>Female</td>
                      <td>
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-yellow" style="width: {{$fPercentageUsers}}%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-yellow">{{round($fPercentageUsers)}}%</span></td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>Do not wish to specify</td>
                      <td>
                        <div class="progress progress-xs progress-striped active">
                          <div class="progress-bar progress-bar-primary" style="width: {{$dnwtsPercentageUsers}}%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-light-blue">{{round($dnwtsPercentageUsers)}}%</span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-danger box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Hear About Us</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Task</th>
                      <th>Progress</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                    
                    @if(!empty($aboutUsList))
                    <?php $n=1; ?>
                    @foreach($aboutUsList as $key=>$value)
                    <tr>
                      <td>{{$n}}.</td>
                      <td>{{$value}}</td>
                      <td>
                        <div class="progress progress-xs">
                            <?php $hereAboutUsCount = get_here_about_us($key); ?>
                          <div class="progress-bar progress-bar-danger" style="width: {{$hereAboutUsCount}}%"></div>
                        </div>
                      </td>
                      <td><span class="badge bg-red">{{ round($hereAboutUsCount)}}%</span></td>
                    </tr>
                     <?php $n++; ?>
                    @endforeach
                    @else
                    <tr><td colspan="4">No data available in table</td></tr>
                    @endif

                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
    <div class="row pad">
         <div class="col-md-6">
            <div class="box box-danger box-info">
              <div class="box-header with-border">
                    <h3 class="box-title">Countries</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Task</th>
                      <th>No. of Country</th>
                    </tr>
                  
                    @if(!$usersCountries->isEmpty())
                    <?php $n=1; ?>
                    @foreach($usersCountries as $key=>$value)
                    <tr>
                      <td>{{$n}}.</td>
                      <td>{{$value->country_name}}</td>
                      <td><span class="badge bg-red">{{$value->country_count}}</span></td>
                    </tr>
                     <?php $n++; ?>
                    @endforeach
                    @else
                        <tr><td colspan="4">No data available in table</td></tr>
                    @endif

                  </tbody>
                </table>
              </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-danger box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">States</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Task</th>
                      <th>No. of States</th>
                    </tr>
                     @if(!$usersStates->isEmpty())
                    <?php $n=1; ?>
                    @foreach($usersStates as $key=>$value)
                    <tr>
                      <td>{{$n}}.</td>
                      <td>{{$value->states_name}}</td>
                      <td><span class="badge bg-red">{{$value->states_count}}</span></td>
                    </tr>
                     <?php $n++; ?>
                    @endforeach
                    @else
                    <tr><td colspan="4">No data available in table</td></tr>
                    @endif

                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="panel-heading display-table">
                    <div class="row display-tr">
                        <h3 class="panel-title display-td">Payment Details</h3>
                        <div class="display-td">
                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body append_data"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/admin/Chart.js') }}"></script>
<script type="text/javascript">

$("#player_id").on('change',function(){
	var player_id = $(this).val();
	$('#loader_img').show();
	 $.ajax({
	 	headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:' {{{ url("admin/player/get-player-list") }}} ',
		'type':'post',
		data:{'player_id':player_id},
		async : true,
		success:function(response){ 
			$('.addPlayerLists').html(response);
			$('#loader_img').hide();
		}
	}); 
});







$('#example1').DataTable();

$('#examplexyz').DataTable();
$('#exampleTrades,#exampleCaptains').DataTable();

$('#example2').DataTable({
  'paging'      : true,
  'lengthChange': false,
  'searching'   : false,
  'ordering'    : true,
  'info'        : true,
  'autoWidth'   : false
})
    var club = "<?php echo auth()->guard('admin')->user()->id; ?>";
    var premiumMember = "<?php echo $premiumMember ?>";

    function callStripeModal() {
        $('#loader_img').show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: ' {{ url("admin/activate-game") }} ',
            'type': 'post',
            data: {
                'club': club
            },
            success: function(response) {
                error_array = JSON.stringify(response);
                data = JSON.parse(error_array);
                $('#loader_img').hide();
                if (data['success'] == 0) {
                    $('.error_class').html(data['data']);
                } else {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: ' {{ url("admin/save-activate-game") }} ',
                        'type': 'post',
                        data: {
                            'id': 4,
                            'club': 9
                        },
                        success: function(response) {
                            $('.append_data').html(response);
                            $('#myModal').modal('show');
                            $('#loader_img').hide();
                        }
                    });
                }
            }
        });
    }

    function activateGame() {
        var seniorGameMode = "{{auth()->guard('admin')->user()->game_mode}}";
        callStripeModal();
    }

    /*Pie chart start here*/
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    $(document).ready(function() {
    	/*For Match type donut chart*/
        var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
        var pieChart = new Chart(pieChartCanvas);
        var PieData = [{
            value: "{{$oneDayMatch}}",
            color: '#00a65a',
            highlight: '#00a65a',
            label: '1-Day'
        }, {
            value: "{{$twoDayMatch}}",
            color: '#00c0ef',
            highlight: '#00c0ef',
            label: '2-Day'
        }];
        var pieOptions = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth: 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //String - A legend template
                legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions);  

        /*Fundraiser*/
        /* var pieChartCanvasFundraiser = $('#pieChartFundraiser').get(0).getContext('2d');
        var pieChartFundraiser = new Chart(pieChartCanvasFundraiser);
        var PieDataFundraiser = [{
                value: "{{$fundraiserAmount}}",
                color: '#f56954',
                highlight: '#f56954',
                label: 'Fundraiser Amount'
            }, {
                value: "{{$fundraiserTarget}}",
                color: '#00a65a',
                highlight: '#00a65a',
                label: 'Target'
            }, {
                value: "{{$fundraiserRaised}}",
                color: '#f39c12',
                highlight: '#f39c12',
                label: 'Raised'
            }];
        var pieOptionsFundraiser = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //String - A legend template
        legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChartFundraiser.Doughnut(PieDataFundraiser, pieOptionsFundraiser);  */     
    });
</script>
<?php if(Auth::guard('admin')->user()->game_mode == 1){ ?>
<script type="text/javascript">
var pieChartCanvasMatch = $('#pieChartMatch').get(0).getContext('2d');
var pieChartMatch = new Chart(pieChartCanvasMatch);
var PieDataMatch = [{
        value: "{{$seniormens}}",
        color: '#f56954',
        highlight: '#f56954',
        label: 'Senior Mens'
    }, {
        value: "{{$seniorWomens}}",
        color: '#00a65a',
        highlight: '#00a65a',
        label: 'Senior Womens'
    }, {
        value: "{{$veterans}}",
        color: '#f39c12',
        highlight: '#f39c12',
        label: 'Veterans'
    }];
var pieOptionsMatch = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //String - A legend template
        legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
pieChartMatch.Doughnut(PieDataMatch, pieOptionsMatch);	
</script>
<?php }elseif(Auth::guard('admin')->user()->game_mode==2){ ?>
<script type="text/javascript">
	 var pieChartCanvasMatch = $('#pieChart').get(0).getContext('2d');
        var pieChartMatch = new Chart(pieChartCanvasMatch);
        var PieDataMatch = [{
	            value: "{{$juniorBoyes}}",
	            color: '#f56954',
	            highlight: '#f56954',
	            label: 'Junior Boys'
	        }, {
	            value: "{{$juniorGirls}}",
	            color: '#00a65a',
	            highlight: '#00a65a',
	            label: 'Junior Girls'
	        }];
        
        var pieOptionsMatch = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth: 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //String - A legend template
                legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
        pieChartMatch.Doughnut(PieDataMatch, pieOptionsMatch);
	
</script>
<?php }elseif (Auth::guard('admin')->user()->game_mode==3) { ?>
<script type="text/javascript">
	
	 var pieChartCanvasMatch = $('#pieChart').get(0).getContext('2d');
        var pieChartMatch = new Chart(pieChartCanvasMatch);
        var PieDataMatch = [{
	            value: "{{$seniormens}}",
	            color: '#f56954',
	            highlight: '#f56954',
	            label: 'Senior Mens'
	        }, {
	            value: "{{$seniorWomens}}",
	            color: '#00a65a',
	            highlight: '#00a65a',
	            label: 'Senior Womens'
	        }, {
	            value: "{{$veterans}}",
	            color: '#f39c12',
	            highlight: '#f39c12',
	            label: 'Veterans'
	        },{
	            value: "{{$juniorBoyes}}",
	            color: '#00c0ef',
	            highlight: '#00c0ef',
	            label: 'Junior Boys'
	        }, {
	            value: "{{$juniorGirls}}",
	            color: '#3c8dbc',
	            highlight: '#3c8dbc',
	            label: 'Junior Girls'
	        }];        
        var pieOptionsMatch = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth: 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //String - A legend template
                legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
        pieChartMatch.Doughnut(PieDataMatch, pieOptionsMatch);
</script>
<?php } ?>


<script type="text/javascript">

    $( document ).ready(function() {
        
        var table =   $('#example_player_gwpt').DataTable( );

        


      $('.gw_no').on('change', function() {
         document.forms['myFormName'].submit();
      });


    }); 




</script>


<style type="text/css">
    
    #example_player_gwpt_length, #example_player_gwpt_filter, #example_player_gwpt_info { display: none; }
</style>

@stop
@extends('admin.layouts.default')
@section('content')
<script>
    jQuery(document).ready(function(){
        $(".choosen_selct").chosen();
    });
</script>
<style>
.chosen-container-single .chosen-single{
    height:34px !important;
    padding:3px 6px;
}
</style>
<section class="content-header">
	<h1>
	  {{ trans("Grade") }}
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li class="active">{{ trans("Grade") }}</li>
	</ol>
</section>
<section class="content"> 
	    <div class="row">
@if(auth()->guard('admin')->user()->id == 1)

        {{ Form::open(['role' => 'form','url' => 'admin/list-grade','class' => 'mws-form',"method"=>"get"]) }}
        {{ Form::hidden('display') }}
            <div class="col-md-2 col-sm-3">
                <div class="form-group ">  
                    <div class="form-group ">  
                    {{ Form::select('game_mode',[null => 'Please Select Mode'] + Config::get('home_club'),((isset($searchVariable['game_mode'])) ? $searchVariable['game_mode'] : ''), ['class' => 'form-control choosen_selct game_mode']) }}
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3">
                <div class="form-group ">  
                    <div class="form-group get_game_name">  
                    {{ Form::select('game_name',array(''=>trans('Select Game')),((isset($searchVariable['game_name'])) ? $searchVariable['game_name'] : ''), ['class' => 'form-control choosen_selct']) }}
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-3">
                <div class="form-group ">  
                    <div class="form-group ">  
                    {{ Form::select('is_active',array(''=>trans('Select Status'),0=>'Inactive',1=>'Active'),((isset($searchVariable['is_active'])) ? $searchVariable['is_active'] : ''), ['class' => 'form-control choosen_selct']) }}
                </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <button class="btn btn-primary"><i class='fa fa-search '></i> Search</button>
                <a href="{{URL::to('admin/list-grade')}}"  class="btn btn-primary"><i class='fa fa-refresh '></i> Reset</a>
            </div>
        {{ Form::close() }}
        <div class="col-md-2 col-sm-12">
			<div class="form-group">  
				<a href="{{URL::to('admin/add-grade')}}" class="btn btn-success btn-small  pull-right" style="margin:0;">{{ trans("Add New Grade") }} </a>
			</div>
		</div>
    @else
    <div class="col-md-12 col-sm-12">
		<div class="form-group">  
			<a href="{{URL::to('admin/add-grade')}}" class="btn btn-success btn-small  pull-right" style="margin:0;">{{ trans("Add New Grade") }} </a>
		</div>
	</div>
@endif 
</div> 
	<div class="box">
		<div class="box-body ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th width="11%">
							{{
								link_to_route(
									"Grade.listGrade",
									trans("Grade"),
									array(
										'sortBy' => 'grade',
										'order' => ($sortBy == 'grade' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'grade' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'grade' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
					@if(auth()->guard('admin')->user()->id == 1)						
						<th width="11%">
							{{
								link_to_route(
									"Grade.listGrade",
									trans("Club"),
									array(
										'sortBy' => 'club',
										'order' => ($sortBy == 'club' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'club' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'club' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
					@endif
						
						<th width="11%">
							{{
								link_to_route(
									"Grade.listGrade",
									trans("Status"),
									array(
										'sortBy' => 'is_active',
										'order' => ($sortBy == 'is_active' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'is_active' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'is_active' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							{{
								link_to_route(
									"Grade.listGrade",
									trans("Created On"),
									array(
										'sortBy' => 'created_at',
										'order' => ($sortBy == 'created_at' && $order == 'desc') ? 'asc' : 'desc',
										$query_string
									),
								   array('class' => (($sortBy == 'created_at' && $order == 'desc') ? 'sorting desc' : (($sortBy == 'created_at' && $order == 'asc') ? 'sorting asc' : 'sorting')) )
								)
							}}
						</th>
						<th width="20%">
							Action
						</th>
					</tr>
				</thead>
				<tbody id="powerwidgets">
				@if(!$result->isEmpty())
					@foreach($result as $key => $record)  
						<tr class="items-inner">
							<td>
								{{ $record->grade }} 
							</td>
							@if(auth()->guard('admin')->user()->id == 1)
							<td>
								{{ $record->club_name->club_name }} 
							</td>
							@endif
							<td>
								@if($record->is_active	==1)
									<span class="label label-success" >{{ trans("Activated") }}</span>
								@else
									<span class="label label-warning" >{{ trans("Deactivated") }}</span>
								@endif
							</td>
							<td>
								{{ date(config::get("Reading.date_format"),strtotime($record->created_at))}}
							</td>
							<td data-th='Action'>
								<a title="{{ trans('Edit') }}" href="{{URL::to('admin/edit-grade/'.$record->id)}}" class="btn btn-primary">
									<i class="fa fa-pencil"></i>
								</a>
								@if($record->is_active == 1)
									<a  title="Click To Deactivate" href="{{URL::to('admin/grade/update-status/'.$record->id.'/0')}}" class="btn btn-success btn-small status_any_item"><span class="fa fa-check"></span>
									</a>
								@else
									<a title="Click To Activate" href="{{URL::to('admin/grade/update-status/'.$record->id.'/1')}}" class="btn btn-warning btn-small status_any_item"><span class="fa fa-ban"></span>
									</a> 
								@endif							
								<a title="{{ trans('Delete') }}" href="{{ URL::to('admin/grade/delete-grade/'.$record->id) }}"  class="delete_any_item btn btn-danger">
									<i class="fa fa-trash-o"></i>
								</a>
								
							</td>
						</tr>
					 @endforeach
					 @else
						<tr>
							<td class="alignCenterClass" colspan="9" >{{ trans("No record is yet available.") }}</td>
						</tr>
					@endif
		</tbody>					
			</table>
		</div>
		<div class="box-footer clearfix">	
			<div class="col-md-3 col-sm-4 "></div>
			<div class="col-md-9 col-sm-8 text-right ">@include('pagination.default', ['paginator' => $result])</div>
		</div>
	</div> 
</section> 
<script> 
var game_name = "<?php echo !empty($searchVariable['game_name']) ? $searchVariable['game_name'] :'' ?>";
var game_mode = "<?php echo !empty($searchVariable['game_mode']) ? $searchVariable['game_mode'] :'' ?>";
if(game_name !="" || game_mode !=""){
    get_game_name();

}
function get_game_name(){
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:' {{{ "admin-get-game-name" }}} ',
        'type':'post',
        data:{'mode':game_mode,'game_name':game_name},
        success:function(response){ 
            $(".get_game_name").html(response);
        }
    });
}
$(".game_mode").on('change',function(){
    var mode = $(this).val();
    $('#loader_img').show();
     $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:' {{{ route("GamePrize.adminGetGameName") }}} ',
        'type':'post',
        data:{'mode':mode,'game_name':game_name},
        success:function(response){ 
            $(".get_game_name").html(response);
        }
    }); 
    $('#loader_img').hide();
});
$('#loader_img').show();
$("#mode").on('change',function(){
	var selectValue   =  $(this).val();
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-grade-club-mode-listing') }}",
		'type':'post',
		data:{'id':selectValue},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
});
/*adter search club searhcing*/
var mode = "<?php  echo !empty($searchVariable['mode']) ? $searchVariable['mode']:0;  ?>";
var searchClubVar = "<?php  echo !empty($searchVariable['club']) ? $searchVariable['club']:0;  ?>";
if(mode !=''){ 
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-grade-club-mode-listing') }}",
		'type':'post',
		data:{'id':mode,'club_id':searchClubVar},
		async : false,
		success:function(response){
			$('.put_html').html(response);
			$('#loader_img').hide();
		}
	});
}

/*After search player searching*/
var club_id = "<?php  echo !empty($searchVariable['club']) ? $searchVariable['club']:0;  ?>";
var searchPlayerVar = "<?php  echo !empty($searchVariable['player']) ? $searchVariable['player']:0;  ?>";
if(club_id !=''){  
	$('#loader_img').show(); 
	$.ajax({
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		url:"{{  url('admin/get-club-mode-player') }}",
		'type':'post',
		data:{'id':club_id,'player_id':searchPlayerVar},
		async : false,
		success:function(response){
			$('.put_player_html').html(response);
			$('#loader_img').hide();
		}
	});
}
</script>
@stop
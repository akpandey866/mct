@extends('admin.layouts.default')
@section('content') 
<section class="content-header">
	<h1>
		{{ trans("Add New Sponsor") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/sponsor')}}">{{ trans("Sponsor") }}</a></li>
		<li class="active">{{ trans("Add New Sponsor") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/sponsor/add-sponsor','class' => 'mws-form','files'=>'true']) }}
	<div class="row">
	<div class="col-md-6">
			<div class="col-md-6">
                <div class="form-group <?php echo ($errors->first('logo')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Sponsor logo</label>
				<div class="mws-form-item">
					{{ Form::file('logo', array('accept' => 'image/*')) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('logo'); ?>
					</div>
				</div>
			</div>	
		  </div>
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				{{ Form::label('name', trans("Sponsor Name"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('name','', ['class' => 'form-control ','placeholder'=>'Sponsor Name','id'=>'name']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
				{{ Form::label('club_type', trans("Club Type"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'club_type',
						 [null => 'Please Select Club'] +$clubLists,
						 '',
						 ['id' => 'club_type','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('club_type'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Save') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/sponsor/add-sponsor/')}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/sponsor/')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
@stop

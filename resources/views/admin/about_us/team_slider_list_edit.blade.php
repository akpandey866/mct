@extends('admin.layouts.default')
@section('content')
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<script src="{{ asset('js/admin/bootstrap-timepicker.min.js') }}"></script> 
 <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-timepicker.min.css') }}">
<section class="content-header">
	<h1>
		{{ trans("Edit Sponsor") }} 
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="{{URL::to('admin/sponsor')}}">{{ trans("Sponsor") }}</a></li>
		<li class="active">{{ trans("Edit Sponsor") }} </li>
	</ol>
</section>
<section class="content"> 
	{{ Form::open(['role' => 'form','url' => 'admin/sponsor/edit-sponsor','class' => 'mws-form','files'=>'true']) }}
	{{ Form::hidden('id',$details->id) }}
	<div class="row">
	<div class="col-md-6">
			<div class="col-md-6">
                
			<div class="form-group <?php echo ($errors->first('image')) ? 'has-error' : ''; ?>">
				<label for="image" class="mws-form-label">Sponsor Logo</label>
				<div class="mws-form-item">
					{{ Form::file('logo', array('accept' => 'image/*')) }}
					<br />
					
					@if(File::exists(SPONSOR_IMAGE_ROOT_PATH.$details->logo))
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo SPONSOR_IMAGE_URL.$details->logo; ?>">
							<div class="usermgmt_image">
								<img  src="<?php echo WEBSITE_URL.'image.php?width=100px&height=100px&image='.SPONSOR_IMAGE_URL.'/'.$details->logo ?>">
							</div>
						</a>
					@endif
					<div class="error-message help-inline">
						<?php echo $errors->first('club_logo'); ?>
						<div class="error-message help-inline">
						<?php echo $errors->first('club_logo'); ?>
					</div>
					</div>
				</div>
			</div>
		  </div>
		</div>
	    <div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('name')) ? 'has-error' : ''; ?>">
				{{ Form::label('name', trans("Sponsor Name"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::text('name',isset($details->name) ? $details->name :'', ['class' => 'form-control ','placeholder'=>'Sponsor Name','id'=>'name']) }}
					<div class="error-message help-inline">
						<?php echo $errors->first('name'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group <?php echo ($errors->first('reason')) ? 'has-error' : ''; ?>">
				{{ Form::label('club_type', trans("Club Type"), ['class' => 'mws-form-label'])}}
				<div class="mws-form-item">
					{{ Form::select(
						 'club_type',
						 [null => 'Please Select Club'] +$clubLists,
						 isset($details->club_type) ? $details->club_type :'',
						 ['id' => 'club_type','class'=>'form-control']
						) 
					}}
					<div class="error-message help-inline">
						<?php echo $errors->first('club_type'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="mws-button-row">
		<div class="input" >
			<input type="submit" value="{{ trans('Update') }}" class="btn btn-danger">
			<a href="{{URL::to('admin/availability/edit-availability/'.$details->id)}}" class="btn btn-primary"><i class=\"icon-refresh\"></i> {{ trans("Reset") }}</a>
			<a href="{{URL::to('admin/availability')}}" class="btn btn-info"><i class=\"icon-refresh\"></i> {{ trans("Cancel") }}</a>
		</div>
	</div>
	{{ Form::close() }}
	
</section>
<script type="text/javascript">
$(document).ready(function() {
	 $( "#date_from" ).datepicker({
		format 	: 'yyyy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		autoclose   :true,
		onSelect	: function( selectedDate ){ $("#date_till").datepicker("option","minDate",selectedDate); }
	});
	$( "#date_till" ).datepicker({
		format 	: 'yyyy-mm-dd',
		changeMonth : true,
		changeYear 	: true,
		yearRange	: '-100y:c+nn',
		autoclose   :true,
		onSelect	: function( selectedDate ){ $("#date_from").datepicker("option","maxDate",selectedDate); }
	});
});
</script>
@stop

<?php 
$app->make("config")->set("Contact.address", "577 N. Meeker Ave.Boise ID
99090 USA");
$app->make("config")->set("Contact.map", "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.2889612081294!2d-0.08991633479969738!3d51.50791446848737!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876035159bb13c5%3A0xa61e28267c3563ac!2sLondon+Bridge!5e0!3m2!1sen!2sin!4v1484053031612");
$app->make("config")->set("Debug.debug", "1");
$app->make("config")->set("default_language.folder_code", "eng");
$app->make("config")->set("default_language.language_code", "1");
$app->make("config")->set("default_language.name", "English");
$app->make("config")->set("Email.client", "gmail.com");
$app->make("config")->set("Email.port", "465");
$app->make("config")->set("Email.timeout", "30");
$app->make("config")->set("Fatsecret.live_consumer_key", "15f2c86b3009483dbf49344d9834b1c6");
$app->make("config")->set("Fatsecret.sandbox_consumer_secret", "740595bab85d4e7d8598127d71b53e08");
$app->make("config")->set("Reading.date_format", "d-M-Y");
$app->make("config")->set("Reading.record_front_per_page", "12");
$app->make("config")->set("Reading.record_game_per_page", "15");
$app->make("config")->set("Reading.records_per_page", 10);
$app->make("config")->set("Site.address", "16 Talara Close, Springvale");
$app->make("config")->set("Site.contact_email", "info@myclubtap.com");
$app->make("config")->set("Site.contact_number", "+61425882907");
$app->make("config")->set("Site.copyrights", "MyClubtap");
$app->make("config")->set("Site.email", "info@myclubtap.com");
$app->make("config")->set("Site.google_api_key", "AIzaSyD1oeEXsc8yF2VX_gWCDid2Hht3Y1zLm9M");
$app->make("config")->set("Site.junior_game_fee", "29");
$app->make("config")->set("Site.league_game_fee", "99");
$app->make("config")->set("Site.senior_game_fee", "19");
$app->make("config")->set("Site.title", "MyClubtap");
$app->make("config")->set("Social.facebook", "https://www.facebook.com/myclubtap");
$app->make("config")->set("Social.instagram_url", "https://instagram.com/myclubtap");
$app->make("config")->set("Social.linkedin_url", "https://www.linkedin.com/company/myclubtap/");
$app->make("config")->set("Social.twitter", "https://twitter.com/myclubtap");
$app->make("config")->set("Social.youtube_url", "https://www.youtube.com/channel/UCxkEHoFm634u8L3-tHkMMgQ");

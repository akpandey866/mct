<?php
/* Global constants for site */
Session::put("appCurrency","USD");
define('FFMPEG_CONVERT_COMMAND', '');

define("ADMIN_FOLDER", "admin/");
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', public_path());
define('APP_PATH', app_path());

define("IMAGE_CONVERT_COMMAND", "");
define('WEBSITE_URL', url('/').'/');
define('WEBSITE_JS_URL', WEBSITE_URL . 'js/');
define('WEBSITE_CSS_URL', WEBSITE_URL . 'css/');
define('WEBSITE_IMG_URL', WEBSITE_URL . 'img/');

define('WEBSITE_UPLOADS_ROOT_PATH', ROOT . DS . 'uploads' .DS );
define('WEBSITE_UPLOADS_URL', WEBSITE_URL . 'uploads/');

define('WEBSITE_ADMIN_URL', WEBSITE_URL.ADMIN_FOLDER );
define('WEBSITE_ADMIN_IMG_URL', WEBSITE_ADMIN_URL . 'img/');
define('WEBSITE_ADMIN_JS_URL', WEBSITE_ADMIN_URL . 'js/');
define('WEBSITE_ADMIN_FONT_URL', WEBSITE_ADMIN_URL . 'fonts/');
define('WEBSITE_ADMIN_CSS_URL', WEBSITE_ADMIN_URL . 'css/');

define('SETTING_FILE_PATH', APP_PATH . DS . 'settings.php');
define('MENU_FILE_PATH', APP_PATH . DS . 'menus.php');

define('CK_EDITOR_URL', WEBSITE_UPLOADS_URL . 'ckeditor_images/');
define('CK_EDITOR_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH . 'ckeditor_images' . DS);

define('USER_PROFILE_IMAGE_URL', WEBSITE_UPLOADS_URL . 'user_profile/');
define('USER_PROFILE_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'user_profile' . DS); 

define('CLUB_IMAGE_URL', WEBSITE_UPLOADS_URL . 'club/');
define('CLUB_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'club' . DS); 

define('PLAYER_IMAGE_URL', WEBSITE_UPLOADS_URL . 'player/');
define('PLAYER_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'player' . DS);

define('MASTERS_IMAGE_URL', WEBSITE_UPLOADS_URL . 'masters/');
define('MASTERS_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'masters' . DS); 

define('SPONSOR_IMAGE_URL', WEBSITE_UPLOADS_URL . 'sponsor/');
define('SPONSOR_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'sponsor' . DS); 

define('SERVE_IMAGE_URL', WEBSITE_UPLOADS_URL . 'serve/');
define('SERVE_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'serve' . DS); 

define('PARTNER_IMAGE_URL', WEBSITE_UPLOADS_URL . 'partner/');
define('PARTNER_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'partner' . DS); 

define('USER_SLIDER_IMAGE_URL', WEBSITE_UPLOADS_URL . 'user_slider/');
define('USER_SLIDER_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'user_slider' . DS); 

define('TEAM_SLIDER_IMAGE_URL', WEBSITE_UPLOADS_URL . 'team_slider/');
define('TEAM_SLIDER_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'team_slider' . DS); 

define('PLATFORM_IMAGE_URL', WEBSITE_UPLOADS_URL . 'platform/');
define('PLATFORM_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'platform' . DS); 

define('SLIDER_IMAGE_URL', WEBSITE_UPLOADS_URL . 'slider/');
define('SLIDER_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'slider' . DS); 

define('PRIZE_IMAGE_URL', WEBSITE_UPLOADS_URL . 'game_prizes/');
define('PRIZE_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'game_prizes' . DS);

define('BRANDING_IMAGE_URL', WEBSITE_UPLOADS_URL . 'branding/');
define('BRANDING_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'branding' . DS);

define('PLAYER_CARD_IMAGE_URL', WEBSITE_UPLOADS_URL . 'player_scorecard/');
define('PLAYER_CARD_IMAGE_ROOT_PATH', WEBSITE_UPLOADS_ROOT_PATH .  'player_scorecard' . DS); 

define('ALLOWED_TAGS_XSS', '<a><strong><b><p><br><i><font><img><h1><h2><h3><h4><h5><h6><span><div></div><em><table><ul><li><section><thead><tbody><tr><td><meta><style><title>');

define('ADMIN_ID', 1);
define('SUPER_ADMIN_ROLE_ID', 1);
define('USER',2);
define('CLUBUSER',3);
define('SCORER',4);
define('MANUAL',5);

define('MAX_NO_OF_TRADE', 20);
define('MAX_NO_OF_CAPTON_CARDS', 2);
define('MAX_NO_OF_12_MAN_CARDS', 1);
define('MAX_NO_OF_DEALER_CARDS', 3);
define('MAX_NO_OF_FLIPPER_CARDS', 1);
define('MAX_NO_OF_SHIELD_STEAL_CARDS', 3); 
//////////////// extension 
define('IMAGE_EXTENSION','jpeg,jpg,png,gif');
define('PDF_EXTENSION','pdf');
define('DOC_EXTENSION','doc,xls,docx');
define('VIDEO_EXTENSION','mpeg,avi,mp4,webm,flv,3gp,m4v,mkv,mov,moov');
define('USER_PROFILE_IMG_FOLDER','user_profile_images');

define('TEXT_ADMIN_ID',1);
define('TEXT_FRONT_USER_ID',2);
define('FRONT_USER',2);

/**  Active Inactive global constant **/
define('ACTIVE',1);
define('INACTIVE',0);

define("MALE",'male');
define("FEMALE",'female');
define("DONOTSPECIFY",'Do not wish to specify');
Config::set('gender_type', array(
	MALE		=>	'Male',
	FEMALE		=>	'Female',
	DONOTSPECIFY =>	'Do not wish to specify'
));
Config::set('club_type', array(
	'Club'		=>	'Club',
	'Junior'	=>	'Junior',
	'League'	=>	'League'
));
Config::set('home_club', array(
	'1' =>	'Senior Mode',
	'2' =>	'Junior Mode',
	'3'=>	'League Mode'
));
Config::set('prize_type', array(
	'1' =>	'Overall Prize',
	'2' =>	'Monthly Prize',
	'3'=>	'Weekly Prize'
));
Config::set('inning_type', array(
	'1' =>	'First Inning',
	'2' =>	'Second Inning',
));

Config::set('game_status', array(
	//'1' =>	'Up-Comming',
	'2' =>	'In-Progress',
	'3' =>	'Completed',
));

Config::set('weekdays', array(
	'Sunday' => 'Sunday',
	'Monday' => 'Monday',
	'Tuesday' => 'Tuesday',
	'Wednesday' => 'Wednesday',
	'Thursday' => 'Thursday',
	'Friday' => 'Friday',
	'Saturday' => 'Saturday',
));


Config::set('player_price_list', array(
	'10.50' => '$10.50m',
	'10.00' => '$10.00m',
	'9.50' => '$9.50m',
	'9.00' => '$9.00m',
	'8.50' => '$8.50m',
	'8.00' 	=> '$8.00m',
	'7.50' 	=> '$7.50m',
));

Config::set('senior_team_type',array(
	1 => 'Senior Mens',
	2 => 'Senior Womens',
	3 => 'Veterans',
));

Config::set('junior_team_type',array(
	1 => 'Junior Boys',
	2 => 'Junior Girls'
));

Config::set('league_team_type',array(
	1 => 'Senior Mens',
	2 => 'Senior Womens',
	3 => 'Veterans',
	4 => 'Junior Boys',
	5 => 'Junior Girls'
));

define('SUBADMIN',4);

define('MAXIMUMPLAYER',20);

define('SENIORFEE',19);
define('JUNIORFEE',29);
define('LEAGUEFEE',99);

define('AUTHORIZE_URI', 'https://connect.stripe.com/oauth/authorize');
define('TOKEN_URI', 'https://connect.stripe.com/oauth/token');

define('BIRDDISCOUNT',20);
define('PLAYERFEE',0.5);

//Branding Activation fees
define('SENIORBRANDING',100);
define('JUNIORBRANDING',100);
define('LEAGUEBRANDING',150);
define('QUERYFILEPATH', APP_PATH . DS . 'query.sql');

define('ONEDAYMATCH',27);
define('TWODAYMATCH',28);

Config::set('newsletter_template_constant',array('USER_NAME'=>'USER_NAME','TO_EMAIL'=>'TO_EMAIL','WEBSITE_URL'=>'WEBSITE_URL','UNSUBSCRIBE_LINK'=>'UNSUBSCRIBE_LINK'));
Config::set('notification_email_constant',array('USER_NAME'=>'USER_NAME','TO_EMAIL'=>'TO_EMAIL'));

//position 1=>BATS;2=>Bowler3=>AR;4=>WK;;
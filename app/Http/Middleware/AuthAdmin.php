<?php

namespace App\Http\Middleware;

use Closure;
Use Auth;
Use Redirect;

class AuthAdmin 
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
		if (Auth::guard('admin')->guest())
		{ 
			return Redirect::to('/admin/login');
		}
        
        $adminIDs = [SUPER_ADMIN_ROLE_ID,SUBADMIN,CLUBUSER];
        if(!in_array(Auth::guard('admin')->user()->user_role_id, $adminIDs)){  
			return Redirect::to('/'); 
		}
        return $next($request);
    }
}

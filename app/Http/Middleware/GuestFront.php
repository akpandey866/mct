<?php

namespace App\Http\Middleware;

use Closure;
Use Auth;
Use Redirect;
Use Session;
Use App;
use Illuminate\Support\Facades\Config;

class GuestFront 
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (Session::has('applocale')) {
            App::setLocale(Session::get('applocale'));
        }else {
            App::setLocale(Config::get('app.fallback_locale'));
        }

        /*$ip = $_SERVER["REMOTE_ADDR"];
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if($ipdat->geoplugin_countryName == "India"){
          return response('Unauthorized.', 401);
        }*/
		
		/*if (!empty(Auth::user())){
			if(Auth::user()->user_role_id  == SUPER_ADMIN_ROLE_ID){
				return Redirect::to('/admin');
			}
		}*/
        return $next($request);
    }
}

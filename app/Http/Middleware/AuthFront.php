<?php

namespace App\Http\Middleware;

use Closure;
Use Auth;
Use Redirect;
Use Session;
Use App\Model\User;
Use App\Model\Branding; 
Use App;
Use Illuminate\Support\Facades\Config;

class AuthFront 
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (Auth::guard('web')->guest()){
			return Redirect::to('login');	
		}
    /*$ip = $_SERVER["REMOTE_ADDR"];
    $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
    if($ipdat->geoplugin_countryName == "India"){
      return response('Unauthorized.', 401);
    }*/

//     if($request->isMethod('post')){ 

// print_r($request->input('my_game_mode')); 
//       print_r($request->input('my_club_name')); 
//       print_r($request->input('junior_club_name')); 
//       print_r($request->input('league_club_name')); 
//       die; 

//     }


// die; 


        // print_r(auth()->guard('web')->user()); 
      // dump(\Request::input('game_mode')); die; 
		
        if(!empty(\Request::input('club_name'))){
         \Session::put('sess_club_name', \Request::input('club_name'));
         $request->request->remove('club_name');
        }
// dump(\Session::get('sess_club_name')); 
// dump(Session::get('sess_game_mode')); 
// dump(\Request::input('game_mode'));         
        if(!empty(\Request::input('game_mode'))){

          if(\Session::get('sess_game_mode') != \Request::input('game_mode')){
// echo 'hi'; die; 
              $clubLists = User::where(['game_mode'=>\Request::input('game_mode'),'is_active'=>1,'is_deleted'=>0, 'user_role_id' => 3, 'is_game_activate' => 1])->orderBy('club_name', 'ASC')->pluck('club_name','id')->all();
              // dump($clubLists); die; 
              // dump(array_key_first($clubLists)); die; 
              if(\Request::input('game_mode') == 1 && !empty(auth()->guard('web')->user()->senior_club_name)){
                $cur_club = auth()->guard('web')->user()->senior_club_name; 
              }elseif(\Request::input('game_mode') == 2 && !empty(auth()->guard('web')->user()->junior_club_name)){
                $cur_club = auth()->guard('web')->user()->junior_club_name; 
              }elseif(\Request::input('game_mode') == 3 && !empty(auth()->guard('web')->user()->league_club_name)){
                $cur_club = auth()->guard('web')->user()->league_club_name; 
              }

              if(empty($cur_club)){
                $cur_club = array_key_first($clubLists); 
              }
              
              if($cur_club){
                    \Session::put('sess_club_name', $cur_club);
              }else{
                \Session::forget('sess_club_name'); 
              }
           \Session::put('sess_game_mode', \Request::input('game_mode'));
           $request->request->remove('game_mode');

          }else{

           \Session::put('sess_game_mode', \Request::input('game_mode'));
           $request->request->remove('game_mode');

          }

        }else if(empty(\Session::get('sess_game_mode')) && !empty(auth()->guard('web')->user()->game_mode)){
           \Session::put('sess_game_mode', auth()->guard('web')->user()->game_mode);

        }else if(empty(\Session::get('sess_game_mode'))){
           \Session::put('sess_game_mode', 1);
         
        }



// dump(\Session::get('sess_club_name')); 


    if($request->isMethod('post')){ // from update page 
      // echo 'hi'; die; 
      if($request->input('my_game_mode') == 1 && !empty($request->input('my_club_name'))){
        \Session::forget('sess_game_mode'); 
        \Session::forget('sess_club_name'); 
        \Session::put('sess_game_mode', 1);
        \Session::put('sess_club_name', $request->input('my_club_name'));
      }elseif($request->input('my_game_mode') == 2 && !empty($request->input('junior_club_name')) ){
        \Session::forget('sess_game_mode'); 
        \Session::forget('sess_club_name'); 
        \Session::put('sess_game_mode', 2);
        \Session::put('sess_club_name', $request->input('junior_club_name'));
      }elseif($request->input('my_game_mode') == 3 && !empty($request->input('league_club_name')) ){
        \Session::forget('sess_game_mode'); 
        \Session::forget('sess_club_name'); 
        \Session::put('sess_game_mode', 3);
        \Session::put('sess_club_name', $request->input('league_club_name'));
      }
   

    }



  if(!\Session::has('sess_club_name') && !empty(auth()->guard('web')->user()) && (\Session::get('sess_game_mode') == 1 && !empty(auth()->guard('web')->user()->senior_club_name) && is_numeric(auth()->guard('web')->user()->senior_club_name))){
                // echo 'hi1'; die; 
                \Session::put('sess_club_name', auth()->guard('web')->user()->senior_club_name);
              }else if(!\Session::has('sess_club_name') && !empty(auth()->guard('web')->user()) && (\Session::get('sess_game_mode') == 2 && !empty(auth()->guard('web')->user()->junior_club_name) && is_numeric(auth()->guard('web')->user()->junior_club_name))){
                  // echo 'hi2'; die; 
                \Session::put('sess_club_name', auth()->guard('web')->user()->junior_club_name);
              }else if(!\Session::has('sess_club_name') && !empty(auth()->guard('web')->user()) && (\Session::get('sess_game_mode') == 3 && !empty(auth()->guard('web')->user()->league_club_name) && is_numeric(auth()->guard('web')->user()->league_club_name))){
                  // echo 'hi3'; die; 
                \Session::put('sess_club_name', auth()->guard('web')->user()->league_club_name);
              }


// dump(\Session::get('sess_club_name')); 


          view()->composer('*', function ($view) use ($request)
            {
                $sess_club_name = ''; 


                // for game mode start

// dump(\Session::get('sess_club_name')); die; 
      

              $clubLists = User::where(['game_mode'=>\Session::get('sess_game_mode'),'is_active'=>1,'is_deleted'=>0, 'user_role_id' => 3, 'is_game_activate' => 1])->orderBy('club_name', 'ASC')->pluck('club_name','id')->all();
              // dump($clubLists); die; 
              if(empty($clubLists)){
                // \Session::put('sess_club_name', '');
                // echo 'hi'; die; 
                session()->forget('sess_club_name');
                $sess_club_name = null ; 
              }else if(!empty($clubLists) && !array_key_exists(\Session::get('sess_club_name'), $clubLists)){
                // dump(\Session::get('sess_club_name')); die; 
                  // echo 'hi4'; die; 
                 \Session::put('sess_club_name', array_key_first($clubLists));
                 if(\Session::get('sess_game_mode') == 1){
                  session()->forget('sess_club_name');
                   \Session::put('sess_club_name', 42);
                 }
                
                 
              }

// dump(\Session::get('sess_game_mode')); 
// dump($clubLists); 
// die; 
              // die; 



// dump(\Session::get('sess_club_name')); 
// die; 



                if(\Session::has('sess_club_name')){

                        $sess_club_name = User::where('id', \Session::get('sess_club_name'))->first();

                }elseif(!empty(auth()->guard('web')->user())){
                    if(auth()->guard('web')->user()->user_role_id == 3){

                      if(!empty(\Session::get('sess_game_mode'))){
                        $sess_club_name = User::where('id', auth()->guard('web')->user()->id)->where('game_mode', \Session::get('sess_game_mode'))->first();
                      }else{
                        $sess_club_name = User::where('id', auth()->guard('web')->user()->id)->where('game_mode', 1)->first();
                      }

                        if(!empty($sess_club_name))
                        \Session::put('sess_club_name', $sess_club_name->id);
                    }else if(is_numeric(auth()->guard('web')->user()->club_name)){
                      // if(!empty(\Session::get('sess_game_mode'))){
                      //   $sess_club_name = User::where('id', auth()->guard('web')->user()->club_name)->where('game_mode', \Session::get('sess_game_mode'))->first();
                      // }else{
                      //   $sess_club_name = User::where('id', auth()->guard('web')->user()->club_name)->where('game_mode', 1)->first();
                      // }
                      //   if(!empty($sess_club_name))
                      //   \Session::put('sess_club_name', $sess_club_name->id);
                    }
                     $sess_club_name = User::where('id', 42)->first(); // according to client requirement, he need to auto select club id 42 if no club selected

                }   





                if(!empty($sess_club_name) && !empty($sess_club_name->id)){
                    $brand = Branding::where('club_id', $sess_club_name->id )->first(); 
                    if(!empty($brand) && !empty($brand->logo) && $brand->is_paid == 1){
                        $sess_club_name->brand_image = $brand->logo; 
                    }
                    if(!empty($brand) && !empty($brand->url)  && $brand->is_paid == 1){
                      $brand->url = strpos($brand->url, 'http') !== 0 ? "http://".$brand->url : $brand->url;
                      $sess_club_name->brand_url = $brand->url; 
                    }                   
                }

                $view->with('sess_club_name', $sess_club_name );  
              $view->with('sessClubLists', $clubLists ); 
            }); 

 
		/*if(Auth::user()->user_role_id  == SUPER_ADMIN_ROLE_ID){
			return Redirect::to('/admin');
		}*/
		
        return $next($request);
    }
}

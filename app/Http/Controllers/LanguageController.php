<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use App\Model\User;
use App\Model\Language;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;


/**
 * Language Controller
 *
 * Add your methods in the class below
 */

class LanguageController extends BaseController{
/** 
 * Function to switchLang
 *
 * @param $lang
 * 
 * @return view page
 */
    public function switchLang($lang){
	    if (array_key_exists($lang, Config::get('languages'))){
            Session::put('applocale', $lang);
			App::setLocale($lang);
			if(Auth::user()){
				DB::table("users")->where("id",Auth::user()->id)->update(array("default_language"=>Session::get('applocale')));
			}
		}
		return Redirect::back();
    }// end switchLang()
	
	public function switchCurrency($curr){
		$response	=	DB::table('currencies')->where('status',1)->orderBy('id','DESC')->pluck('currency','currency')->all();
        if(array_key_exists($curr, $response)){
            Session::put('appCurrency', $curr);
			if(Auth::user()){
				DB::table("users")->where("id",Auth::user()->id)->update(array("default_currency"=>Session::get('appCurrency')));
			}
        }
        return Redirect::back();
    }// end switchCurrency()

}// end LanguageController

<?php
/** 
 * Users Controller
 */
namespace App\Http\Controllers\front;
use App\Http\Controllers\BaseController;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator,Stripe;
use Carbon\Carbon;
use App\Model\UserSlider;
use App\Model\DropDown; 
use App\Model\AboutClub;
use App\Model\UserTeamPlayers; 
use App\Model\UserTeamTradeLog; 
use App\Model\UserTeamValueLog; 
use App\Model\GamePoint; 
use App\Model\Player; 
use App\Model\State;
use App\Model\City;
use App\Model\User;
use App\Model\Fixture;
use App\Model\Sponsor;
use App\Model\GamePrize;
use App\Model\UserTeams;
use App\Model\FixtureScorcard;
use App\Model\Club;
use App\Model\GameSetting;
use App\Model\PlayerTradeInOut; 
use App\Model\Fundraiser; 
use Illuminate\Http\Request;
use App\Model\UserTeamsPlayerTrack; 
use App\Model\UserTeamsCVCTrack; 
use App\Model\UserTeamsGWExtraPTTrack; 
use App\Model\GamePrizeMessage;
use App\Model\UserTeamLogs; 
use App\Model\Grade;
use App\Model\Availability;
use App\Model\UserTeamsMonthPTTrack; 

class CommonController extends BaseController {

/** 
 * Function to display website home page
 *
 * @param null
 * 
 * @return view page
 */
	public function pricing(){ 
		return View::make('front.common.pricing');
	} //end index()

/** 
 * Function to display about club
 *
 * @param null
 * 
 * @return view page
 */
	public function aboutClub(){
		$userSliders = UserSlider::leftJoin('users' , 'user_slider.club_name' , '=' , 'users.id')
							->where('user_slider.is_active',1)
							->select('user_slider.*','users.club_name')
							->get();
		$data = AboutClub::where('is_active',1)->get();
		return View::make('front.common.about_club',compact('data','userSliders'));
	}

	public function getState(){
		$country_id = Input::get('country_id');
		$state_id = !empty(Input::get('state_id')) ? Input::get('state_id') :'';
		$stateList = State::get_states($country_id);
		$state_old_id = !empty(Input::get('state_old_id')) ? Input::get('state_old_id') : '';
		return View::make('front.common.state',compact('stateList','state_old_id','country_id','state_id'));
	}
	public function getCity(){
		$state_id = Input::get('state_id');
		$cityList = City::get_cities($state_id);
		$city_old_id = !empty(Input::get('city_old_id')) ? Input::get('city_old_id') : '';
		return View::make('front.common.city',compact('cityList','city_old_id','state_id'));
	}

	public function getClubList(){
		$clubId = Input::get('club_id');
		$clubLists = User::where(['club_type'=>$clubId,'is_active'=>1,'is_deleted'=>0])->pluck('club_name','id')->all();
		return View::make('front.common.club_list_dropdown',compact('clubLists'));
	}

	public function getGameName(){
		$id = Input::get('id');
		$gameName = User::leftjoin('cities','cities.id','=','users.city')
					->where('users.id',$id)
					->select('club_logo','game_name','cities.name as city_name')
					->first();
		$response	=	array(
			'success' 	=> true,
			'name' 	=> $gameName->game_name,
			'city_name' => $gameName->city_name,
			'image' =>$gameName->club_logo
		);
		return Response::json($response); 
	}

	public function logoutAdmin(){ 
		Auth::guard('admin')->logout();
		if(!empty(Session::get('user_password'))){
			$userdata = array('email'=> Auth::guard('web')->user()->email,'password'=> Session::get('user_password'));
			if (Auth::guard('admin')->attempt($userdata)){	
				return Redirect::intended('admin/club-dashboard')->with('message','You are now logged in!');
			}
		}
		return Redirect::to('/admin/login');
	}

	public function fixtures(){

		$query_string = null; 
		
		$club_id = \Session::get('sess_club_name');
 	// print_r($club_id); 

		$DB 					= 	Fixture::query();
		if(!empty($club_id))
			$DB->where('fixtures.club', $club_id); 
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		/*
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			// foreach($searchData as $fieldName => $fieldValue){
			// 	if(!empty($fieldValue) || $fieldValue==0){
			// 		$DB->where("$fieldName",'like','%'.$fieldValue.'%');
			// 	}
			// 	$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			// } 
		}

		*/



		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'fixtures.created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';

	   if(!empty($club_id)){
	    $temp = array(); 
// whereDate('created_at', '>', Carbon::now()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->endOfWeek())
	    // Carbon::now()->subDays(30)

	    $DB->leftJoin('teams','teams.id','=','fixtures.team')
						->leftJoin('dropdown_managers','dropdown_managers.id','=','fixtures.grade')
						->leftJoin('grades','grades.id','=','fixtures.grade')
						->where('fixtures.is_active',1)
						->orderBy($sortBy, $order); 




		// dump($club_id); die; 

		$curclub = User::find($club_id);

		// dump($curclub); die; 

		$lockout_start_date  = Carbon::parse($curclub->lockout_start_date); 

		$lockout_end_date	= Carbon::parse($curclub->lockout_end_date); 
		$cnt = 1; 
		$past_temp = array(); 
		$future_temp = array(); 
		while ($lockout_start_date <= $lockout_end_date) {
			$result = clone $DB; 

			$result = 	$result
						   ->whereBetween('fixtures.start_date',  [$lockout_start_date->copy()->startOfWeek() , $lockout_start_date->copy()->endOfWeek()] )
						   // ->whereDate('fixtures.created_at', '<=', Carbon::now()->subDays(7 * $i)->endOfWeek() )
						   ->select('teams.name as team_name','grades.grade as grade_name', 'fixtures.*')->get(); 

			// dump($result); 
						// ->paginate(Config::get("Reading.records_per_page"));
			// if(!empty($result) && $result->isNotEmpty())
				// $temp[Carbon::now()->addDays(7 * $i)->startOfWeek()->isoFormat('ddd').' - '.Carbon::now()->addDays(7 * $i)->startOfWeek()->isoFormat('D MMMM YYYY')] = $result;
			if(!empty($result) && $result->isNotEmpty()){
				if(Carbon::now() < $lockout_start_date->copy()->endOfWeek()){
					$future_temp[$cnt]['gw'] = $cnt; 
					$future_temp[$cnt]['data'] = $result; 
				}else{
					$past_temp[$cnt]['gw'] = $cnt; 
					$past_temp[$cnt]['data'] = $result; 
				}
			}

			// $temp[$cnt] = $result; 

			$lockout_start_date->addWeek(); 
			$cnt++; 
					
		}

		// dump($past_temp); 
		// dump(array_reverse($past_temp));
		// dump($future_temp);  die; 

		if(!empty($future_temp)){


			$temp = array_merge($future_temp, $past_temp); 
		}elseif(!empty($past_temp)){
			

			$past_temp = array_reverse($past_temp); 

			$flg_val = false; 
			$tmp_past_temp = array();
			foreach ($past_temp as $key => $value) {

				if($flg_val){
					$tmp_past_temp[$key] = $value; 
				}
				if(!empty($value) && !$flg_val){
					$temp[$key] = $value; 
					$flg_val = true; 
				}				
			}

			$temp = array_merge($temp, array_reverse($tmp_past_temp)); 

		}
		// dump($temp); die; 

		// die; 

		/*

		// for future
	    for ($i = 0; $i <= 10; $i++) {

			$result = clone $DB; 
			$result = 	$result
						   ->whereBetween('fixtures.start_date',  [Carbon::now()->addDays(7 * $i)->startOfWeek() , Carbon::now()->addDays(7 * $i)->endOfWeek()] )
						   // ->whereDate('fixtures.created_at', '<=', Carbon::now()->subDays(7 * $i)->endOfWeek() )
						   ->select('teams.name as team_name','grades.grade as grade_name', 'fixtures.*')->get(); 

			
						// ->paginate(Config::get("Reading.records_per_page"));
		if(!empty($result) && $result->isNotEmpty())
			$temp[Carbon::now()->addDays(7 * $i)->startOfWeek()->isoFormat('ddd').' - '.Carbon::now()->addDays(7 * $i)->startOfWeek()->isoFormat('D MMMM YYYY')] = $result; 
	
		
			
		}
		// dump($temp); die; 
		// $temp = array_reverse($temp); 

		// dump($temp); 
		// for past
		// echo 'hi'; die; 
		$temp_temp = array(); 
	    for ($i = 0; $i <= 10; $i++) {

			$result = clone $DB; 
			$result = 	$result
						   ->whereBetween('fixtures.start_date',  [Carbon::now()->subDays(7 * $i)->startOfWeek() , Carbon::now()->subDays(7 * $i)->endOfWeek()] )
						   // ->whereDate('fixtures.created_at', '<=', Carbon::now()->subDays(7 * $i)->endOfWeek() )
						   ->select('teams.name as team_name','grades.grade as grade_name', 'fixtures.*')->get(); 

			
						// ->paginate(Config::get("Reading.records_per_page"));
		if(!empty($result) && $result->isNotEmpty())
			$temp_temp[Carbon::now()->subDays(7 * $i)->startOfWeek()->isoFormat('ddd').' - '.Carbon::now()->subDays(7 * $i)->startOfWeek()->isoFormat('D MMMM YYYY')] = $result; 
		
			// dump($result); die; 
			
		}

		$temp_temp = array_reverse($temp_temp); 

		dump($temp_temp); die; 

		// dump(array_merge($temp, $temp_temp)); 
		// die; 
		$temp = array_merge($temp, $temp_temp);
*/ 
		// dump($temp); die; 
		// dump($result); 
		// $complete_string		=	Input::query();
		// unset($complete_string["sortBy"]);
		// unset($complete_string["order"]);
		// $query_string			=	http_build_query($complete_string);
		// $result->appends(Input::all())->render();
		$club = new User();
		$seniorClub = $club->getClubList();
		$gameName = User::where(['club_type'=>1])
						// ->orderBy('club_id','ASC')
						->orderBy('created_at','DESC')
						->value('game_name');

		foreach ($temp as $key => $value) {
			if(!empty($value['data'])){
				foreach ($value['data'] as $k => $val) {
					// dump($val); die; 
					$val->tot_fp = $val->fixture_scorecard->sum('fantasy_points'); 

				}
			
				// $result = $result->sortByDesc('tot_fp');
			}
		}

		// dump($temp); die; 
		$result = $temp; 

		// dump($result ); die; 

	}else{
		$result = null; 
	}



	// dump($result); die; 
	
if(!empty($result)){
	foreach ($result as $key => $value) { 
		foreach ($value['data'] as $k1 => $v1) {
			// dump($v1->id); 
	$DB 					= 	FixtureScorcard::query();
	$resulttt = 	$DB->leftjoin('players','fixture_scorecards.player_id','=','players.id')
						//->leftjoin('fixture','fixture.id','=','fixture_scorecards.fixture_id')
						->select('fixture_scorecards.*','players.full_name as player_name','players.position as player_position')

						->where('fixture_scorecards.fixture_id',$v1->id)->get();
				$v1->fixture_scorecards = $resulttt ; 
		}
	}
}

	// dd($result); 
	// die; 


			// $DB 					= 	FixtureScorcard::query();


			// $result = 	$DB->leftjoin('players','fixture_scorecards.player_id','=','players.id')
			// 			//->leftjoin('fixture','fixture.id','=','fixture_scorecards.fixture_id')
			// 			->select('fixture_scorecards.*','players.full_name as player_name','players.position as player_position')

			// 			->where('fixture_scorecards.fixture_id',$fixture_id)

			// 			// ->orderBy($sortBy, $order)

			// 			->paginate(40);


// echo 'hi'; die; 

		// dump($result ); die; 
		// print_r($result); die; 
		return  View::make('front.common.fixtures', compact('result','searchVariable','sortBy','order','query_string','gameName','seniorClub'));
	}
	public function gameSponsor(){
		$club_id = \Session::get('sess_club_name');
		$result = Sponsor::where('user_id', $club_id)->where('is_active',1)->get();
		// print_r($result); die; 
		return  View::make('front.common.game_sponsors', compact('result'));
	}

	public function partners(){
		$club_id = \Session::get('sess_club_name');
		$result = Sponsor::where('user_id', 1)->get();
		$result = $result ? $result->toArray() : array();
		return  View::make('front.common.partners', compact('result'));
	}
	public function prizes(){
		$club_id = \Session::get('sess_club_name');
		$clubMessage = GamePrizeMessage::where('club_id',$club_id)->value('message');
		if(!empty($club_id)){
			$result = GamePrize::where('club', $club_id)->where('is_active',1)->get()->toArray();
		}else{
			$result = GamePrize::where('is_active',1)->get()->toArray();
		}
		// print_r($result); die; 
		return  View::make('front.common.prizes', compact('result','clubMessage'));
	}


	public function stats(){
		// dump(Input::all()); die; 
	$input_data = Input::all(); 
	$search = ''; 
	$drop_down =	new DropDown();
	$position =	$drop_down->get_master_list("position");
	// print_r($position); die; 
	$club_id = \Session::get('sess_club_name');
	if(!empty($club_id)){

		if(isset($input_data['search'])){
			$search = $input_data['search']; 
			$result = Player::where('club', $club_id)->where('is_active', 1)->where('full_name', 'LIKE', '%'.$input_data['search'].'%')->withCount(['get_scorecard as score_count' => function($query) {
				    $query->select(DB::raw('sum(fantasy_points)'));
				}])->orderBy('score_count', 'DESC')->get(); //->paginate(Config::get("Reading.records_per_page")); // 
		}else{
			$result = Player::where('club', $club_id)->where('is_active', 1)->withCount(['get_scorecard as score_count' => function($query) {
				    $query->select(DB::raw('sum(fantasy_points)'));
				}])->orderBy('score_count', 'DESC')->get(); //->paginate(Config::get("Reading.records_per_page"));  // 
		}





	// }else{
	// 	$result = Player::withCount(['get_scorecard as score_count' => function($query) {
	// 			    $query->select(DB::raw('sum(fantasy_points)'));
	// 			}])->orderBy('score_count', 'DESC')->get(); 
	// }
	// $result = Player::get(); 

	    $tot_team = UserTeams::where('club_id', $club_id)->count(); 
	    // dump($tot_team); 
	    // dump($result); die; 

	 	foreach ($result as $key => $value) {
		 	// $team_selected = UserTeamPlayers::where('player_id',  $value->id)
				// 			->whereHas('teams', function($q) use ($club_id) {
				// 									    $q->where('club_id', $club_id);
				// 									})
		 	// 				->pluck('team_id')->toArray(); 


		 	$team_selected = UserTeams::where('club_id', $club_id)->whereHas('getTeamPlayer', function($q) use ($value) {
                                    $q->where('player_id', $value->id);
                                })->count();


		    // $team_selected = array_unique($team_selected);
		    $cap_count = 0; // UserTeams::where('club_id', $club_id)->where('capton_id', $value->id)->count(); 
		    $vice_cap_count = 0; // UserTeams::where('club_id', $club_id)->where('vice_capton_id', $value->id)->count(); 
		    // dump($team_selected); 
		    // $team_selected = count($team_selected); 
		    $selected_by = 0;
		    if(!empty($team_selected) && !empty($tot_team) && ($team_selected > ($cap_count + $vice_cap_count))){
		 		$selected_by = round((($team_selected-($cap_count + $vice_cap_count))/$tot_team)*100); 
		 		// dump($selected_by); 
		    }
		 	$value->selected_by = $selected_by; 






		}
	}else{
		$result = null; 
	}

// die; 
		// dump($result); die; 



        $all_player = Player::where('club', $club_id)->pluck('id')->toArray(); 
        $all_player_stat_data = FixtureScorcard::with('player')->whereIn('player_id', $all_player)->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum_fp, sum(rs) as sum_rs,  sum(fours) as sum_fours, sum(sixes) as sum_sixes, sum(wks) as sum_wks, sum(mdns) as sum_mdns, sum(hattrick) as sum_hattrick, sum(cs) as sum_cs, sum(sts) as sum_sts, sum(overs) as sum_overs, player_id')->get(); // ->pluck('sum_fp', 'sum_rs', 'sum_fours', 'sum_sixes', 'sum_wks', 'sum_mdns', 'sum_hattrick', 'sum_cs', 'sum_sts', 'sum_overs', 'player_id')->toArray(); 

        $other_data = array(); 
        foreach ($all_player_stat_data as $key2 => $value2) {
        	$other_data[$value2->player->id] = $value2->toArray(); 
        }
        // dump($result); 
        // dump($other_data); die; 


        $total_team = UserTeams::where('club_id', $club_id)->count(); 
        // dump($other_data); 

        foreach ($other_data as $k2 => $v2) {
	      
	        // dump($total_team); 
	        $cap_count = 0; // UserTeams::where('club_id', $club_id)->where('capton_id', $player_id)->count(); 
	        $vc_count = 0; // UserTeams::where('club_id', $club_id)->where('vice_capton_id', $player_id)->count(); 

	        /*
		        $player_count = UserTeams::where('club_id', $club_id)->whereHas('getTeamPlayer', function($q) use ($player_id) {
		                                    $q->where('player_id', $player_id);
		                                })->count();
		        // dump($player_id);
		        // dump($cap_count);
		        // dump($vc_count); 
		        // dump($player_count); die; 



		        if($player_count > 0 && ($player_count - ($cap_count + $vc_count) > 0) && $total_team > 0){
		            $player_count = (100 * ($player_count - ($cap_count + $vc_count))) / $total_team  ; 
		        }

	        */ 

	        $cap_count = UserTeams::where('club_id', $club_id)->where('capton_id', $k2)->count(); 
	        $vc_count = UserTeams::where('club_id', $club_id)->where('vice_capton_id', $k2)->count(); 

	        if($cap_count > 0 && $total_team > 0){
	            $cap_count = (100 * $cap_count) / $total_team  ; 
	        }
	        if($vc_count > 0 && $total_team > 0 ){
	            $vc_count = (100 * $vc_count) / $total_team  ; 
	        }

	        $other_data[$k2]['pcap'] = round($cap_count); 
	        $other_data[$k2]['pvcap'] = round($vc_count); 

        }


        // dump($other_data); die; 


		return  View::make('front.common.stats', compact('result', 'position', 'search', 'other_data'));

	}

	public function points($cur_gameweek_no = null){

		// dump($gw_cnt); die; 

// $tmp_gpp = null;
// $v = 5; 
// $tmp_gpp[$v] = !empty($tmp_gpp[$v]) ? $tmp_gpp[$v] + 20 : 20; 

// dump($tmp_gpp); die; 


$drop_down =	new DropDown();
	$position =	$drop_down->get_master_list("position");
	// print_r($position); die; 
	$club_id = \Session::get('sess_club_name');

/*  // commented on 24/02/20 

	if(!empty($club_id)){
		$result = Player::where('club', $club_id)->withCount(['get_scorecard as score_count' => function($query) {
				    $query->select(DB::raw('sum(fantasy_points)'));
				}])->orderBy('score_count', 'DESC')->get(); 
	}else{
		$result = Player::withCount(['get_scorecard as score_count' => function($query) {
				    $query->select(DB::raw('sum(fantasy_points)'));
				}])->orderBy('score_count', 'DESC')->get(); 
	}

	*/ 
	// $result = Player::get(); 

// dump($result); die; 

	// start ************************************ start 

	if(!empty($club_id)){
		$cur_club = User::where('id', $club_id)->first(); 

		// dump($club_id); 
		if(empty($cur_gameweek_no) && !empty($cur_club->lockout_start_date)){
			$cur_gameweek_no = Carbon::parse($cur_club->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now());	
			// $cur_gameweek_no++;  // on points page we need previous week data that's why commented 
		}
	}


		if(empty($cur_gameweek_no) || $cur_gameweek_no < 1 || empty($cur_club->lockout_start_date)){
			$cur_gameweek_date= Carbon::now()->startOfWeek();
			$cur_gameweek_no = 1; 
		}else{

			$cur_gameweek_date= Carbon::parse($cur_club->lockout_start_date)->startOfWeek()->addWeeks($cur_gameweek_no)->subDay()->startOfWeek();   // subday for come in the last week range
		}



	
		// dump($cur_gameweek_no); 
		// dump($cur_gameweek_date);  die; 


		$drop_down =	new DropDown();
	    $position =	$drop_down->get_master_list("position");
	    // dump($position); die; 
	    $position = array_replace(array_flip(array('4', '1', '3', '2')), $position);
	    // dump($position); die;
	    $category =	$drop_down->get_master_list("category");
	    $svalue	=	$drop_down->get_master_list("svalue");
	    $svalue = Config::get('player_price_list'); 
	    $jvalue =	$drop_down->get_master_list("jvalue");
	    $capton_id = 0; 
	    $vice_capton_id = 0; 
	    $my_team_name = ''; 
	    $userteam_fantasy_points = array() ; 
	    $gameweek_fantasy_points = array() ; 
	    $club_name = Session::get('sess_club_name'); 
	    // print_r($club_name); 
	    $club_data = User::where('id', $club_name)->first(); 
	    $game_point_data = GamePoint::where('club', $club_name)->get(); 
	    // dump($club_data); die; 
	    // dump($club_name); 
	    // dump($game_point_data);  

	    $players =  Player::where('club', $club_name)->where('full_name','!=','')->where('is_active', 1)->get(); 
	    $chosen_player = []; 
		$user_teams = UserTeams::where('user_id', auth()->guard('web')->user()->id)->where('club_id', $club_name)->first();
		// dump($user_teams); die; 
		$user_teams_data = $user_teams ; 
		// dump($user_teams_data); die; 
		$no_of_trade = 0; 
		if ($user_teams) {
			$capton_id = $user_teams->capton_id; 
			$vice_capton_id = $user_teams->vice_capton_id; 
			$my_team_name = $user_teams->my_team_name;

			if($cur_gameweek_date->diffInWeeks(Carbon::now()) == 0 && Carbon::now()->startOfDay() >= $cur_gameweek_date){
				// dump(Carbon::now()); 
				// dump($cur_gameweek_date); 
				// echo 'asldfk'; die; 
				$no_of_trade = $user_teams->no_of_trade; 
			}else{
				$no_of_trade = UserTeamTradeLog::where('team_id', $user_teams->id)->whereDate('created_at', '<=', $cur_gameweek_date->copy()->endOfWeek())->sum('no_of_trade'); 
				// dump($user_teams->id); 
				// dump( $cur_gameweek_date->copy()->endOfWeek()->endOfDay()); die; 
				// $gameweek_team_value = UserTeamsGWExtraPTTrack::where(['team_id' => $user_teams->id, 'gw_end_date' => $cur_gameweek_date->copy()->endOfWeek()->endOfDay() ])->first(); 

				// dump($gameweek_team_value); die; 




				// get the game week data if exists, if not get the previous gameweek data 
				// dump($cur_gameweek_date); 

	            if(UserTeamsGWExtraPTTrack::where(['team_id' => $user_teams->id, 'gw_end_date' => $cur_gameweek_date->copy()->endOfWeek()->endOfDay() ])->exists()){

	              $gameweek_team_value = UserTeamsGWExtraPTTrack::where('team_id', $user_teams->id)->where('gw_end_date', $cur_gameweek_date->copy()->endOfWeek()->endOfDay())->first(); 


	            }else{
	              $gameweek_team_value = UserTeamsGWExtraPTTrack::where('team_id', $user_teams->id)->where('gw_end_date', '<', $cur_gameweek_date->copy()->endOfWeek()->endOfDay())->orderBy('id', 'DESC')->first(); 


	              // set the gw_pt and gw_player_points to null, because
	              $gameweek_team_value->gw_pt = 0; 
	              $gameweek_team_value->gw_player_points = null; 
	            }

			}



			// whereDate('created_at', '<=', $cur_gameweek_date->copy()->endOfWeek())

			// $club_name =  $user_teams->club_id;
		    $user_teams = $user_teams->toArray();
		}else{
			$user_teams = [];
		}
		if(!empty($user_teams) && !empty($gameweek_team_value)){


			$gameweek_team_value_utvl = UserTeamValueLog::where('team_id', $gameweek_team_value->team_id)->whereNotNull('team_value')->where('created_at', '<', $cur_gameweek_date->copy()->endOfWeek()->addHour())->orderBy('id', 'DESC')->first(); 

			// dump($gameweek_team_value_utvl); die; 

			$team_creation_date = $user_teams['created_at'] ; 
			// dump($user_teams);  
			$capton_id = !empty($gameweek_team_value_utvl->capton_id) ? $gameweek_team_value_utvl->capton_id : $user_teams['capton_id']; 
			$vice_capton_id = !empty($gameweek_team_value_utvl->v_capton_id) ? $gameweek_team_value_utvl->v_capton_id : $user_teams['vice_capton_id']; 
			$twelveth_man_id = $user_teams['twelveth_man_id'] ; 
			$user_teams = UserTeamPlayers::where('team_id', $user_teams['id'])->pluck('player_id')->toArray();
			// dump($user_teams);

            if(!empty($user_teams['twelve_man_card_last_date']) && (Carbon::parse($user_teams['twelve_man_card_last_date'])->startOfWeek() < Carbon::now() && Carbon::now() < Carbon::parse($user_teams['twelve_man_card_last_date'])->endOfWeek()  ) && !empty($twelveth_man_id)){
                // $user_teams[] = $twelveth_man_id; 
                array_push($user_teams, $twelveth_man_id);
            }
			// dump($user_teams); die; 


			// game week calculation start 




			// dump($user_teams_data->id); 

			$user_team_player = UserTeamPlayers::where('team_id', $user_teams_data->id)->get(); 
			// dump($user_team_player); 
			$tmp_top_pnt = 0 ; 

		


            // $temp_last_wk_pt[$value->id] = $tmp_top_pnt; 

                    // $gameweek_fantasy_points =  $tmp_top_pnt + $gw_extra_pt; 

                    // $gameweek_fantasy_points = $gameweek_fantasy_points > 0 ? $gameweek_fantasy_points : 0 ; 

                    $gameweek_fantasy_points = !empty($gameweek_team_value->gw_pt) ? $gameweek_team_value->gw_pt : 0 ;

			// game week calculation end 


			// get end of week date

			// $user_teams = unserialize($user_teams['player_ids']); 
			// $chosen_player = Player::whereIn('id', $user_teams)->get();

		}


$tot_club_users = 0; 

// rank calc

	$club_id = \Session::get('sess_club_name');
		if(!empty($club_id)){
		// $result = UserTeams::leftJoin('users','users.id','=','user_teams.user_id')->where('club_id', $club_id)
		// 			//->where('user_teams.club_id', $club_id)
		// 			//->leftJoin('')
		// 			->with('getTeamPlayer')
		// 			->select('user_teams.*','users.first_name','users.club_logo')
		// 			->get();


		// check the previous link active on points page start 

		// dump($cur_gameweek_date); 

		$prev_page_active = true; 

	if(!empty($gameweek_team_value)){

		// set "prev_page_active" variable to false if previous record does not exist in UserTeamsGWExtraPTTrack table 
		if(UserTeamsGWExtraPTTrack::where('team_id', $gameweek_team_value->team_id)->where('gw_end_date', '<' , Carbon::parse($gameweek_team_value->gw_end_date)->subWeek()->subDay()->endOfWeek()->endOfDay())->doesntExist()){
			$prev_page_active = false; 
		}


	}else {
		$prev_page_active = false; 
	}


	/* // commented on 24/02/20 

	if(!empty($user_teams_data) && !empty($gameweek_team_value)){

		// $ppa1 = UserTeamValueLog::where('team_id', $user_teams_data->id)->whereNotNull('team_player')->whereBetween('created_at', [$cur_gameweek_date, $cur_gameweek_date->copy()->endOfWeek()])->orderBy('id', 'ASC')->first(); 
		// $ppa2 = UserTeamValueLog::where('team_id', $user_teams_data->id)->whereNotNull('team_player')->whereBetween('created_at', [$cur_gameweek_date->copy()->subWeek()->startOfWeek(), $cur_gameweek_date->copy()->subWeek()->endOfWeek()])->orderBy('id', 'ASC')->first(); 
		// $ppa3 = UserTeamValueLog::where('team_id', $user_teams_data->id)->whereNotNull('team_player')->whereBetween('created_at', [$cur_gameweek_date->copy()->subWeeks(2)->startOfWeek(), $cur_gameweek_date->copy()->subWeeks(2)->endOfWeek()])->orderBy('id', 'ASC')->first(); 


		// dump($gameweek_team_value); die; 

		// if(empty($ppa1) && empty($ppa2) && empty($ppa3)){
		if(UserTeamsGWExtraPTTrack::where('team_id', $gameweek_team_value->team_id)->where('gw_end_date', Carbon::parse($gameweek_team_value->gw_end_date)->subWeek()->subDay()->endOfWeek()->endOfDay())->doesntExist()){
			$prev_page_active = false; 
		}
	}else{
		$prev_page_active = false; 
	}

	*/ 

		// $user_teams_data
		// check the previous link active on points page end



		// foreach ($user_teams_data as $k4 => $v4) {
		// 	dump($k4);
		// 	dump($v4);  
		// }

		// die; 

		// dump($user_teams_data); 
		// dump($user_teams_dt); 
		// dump($temp_points_arr); 
		// dump($team_ranks_arr); 
		// die; 
		$tot_club_users = UserTeams::where('club_id', $club_id)->whereDate('created_at', '<=', $cur_gameweek_date->copy()->startOfWeek())->count(); 

		// dump($tot_club_users); die; 

		}


		
		/*
		$booster_active = array( 'name' => false, 'status' => false );  
		if(!empty($user_teams_data) && !empty(UserTeams::where('id', $user_teams_data->id)->first()->capton_card_last_date)){
			UserTeams::where('id', $user_teams_data->id)->update(['twelve_man_card_last_date' => null]);
			
			$booster_active['name'] = '3x'; 
			$booster_active['status'] = true; 
		}
		// if(!empty($user_teams_data->twelve_man_card_last_date)){
		if(!empty($user_teams_data) && !empty(UserTeams::where('id', $user_teams_data->id)->first()->twelve_man_card_last_date)){
			UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
			$booster_active['name'] = '12th'; 
			$booster_active['status'] = true; 
		}

		*/ 

// dump($user_teams_data); 
// dump($cur_gameweek_date); 

			if(!empty($user_teams_data)){
				if(!empty($user_teams_data->capton_card_last_date) &&  Carbon::parse($user_teams_data->capton_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
				if(!empty($user_teams_data->twelve_man_card_last_date) &&  Carbon::parse($user_teams_data->twelve_man_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['twelve_man_card_last_date' => null]);
				if(!empty($user_teams_data->dealer_card_last_date) &&  Carbon::parse($user_teams_data->dealer_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['dealer_card_last_date' => null]);
				if(!empty($user_teams_data->flipper_card_last_date) &&  Carbon::parse($user_teams_data->flipper_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['flipper_card_last_date' => null]);
				
			}



		$booster_active = array( 'name' => false, 'status' => false );  
		if(!empty($user_teams_data) && UserTeamLogs::where('team_id', $user_teams_data->id)->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('log_type', 2)->exists()){
			// UserTeams::where('id', $user_teams_data->id)->update(['twelve_man_card_last_date' => null]);
			
			$booster_active['name'] = '3x'; 
			$booster_active['status'] = true; 
		}
		// if(!empty($user_teams_data->twelve_man_card_last_date)){
		if(!empty($user_teams_data) && UserTeamLogs::where('team_id', $user_teams_data->id)->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('log_type', 3)->exists()){
			// UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
			$booster_active['name'] = '12th'; 
			$booster_active['status'] = true; 
		}

		if(!empty($user_teams_data) && UserTeamLogs::where('team_id', $user_teams_data->id)->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('log_type', 4)->exists()){
			// UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
			$booster_active['name'] = 'Dealer'; 
			$booster_active['status'] = true; 
		}

		if(!empty($user_teams_data) && UserTeamLogs::where('team_id', $user_teams_data->id)->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('log_type', 5)->exists()){
			// UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
			$booster_active['name'] = 'Flipper'; 
			$booster_active['status'] = true; 
		}

		/* 
		$availability = Availability::where('is_active',1)->where('club', $club_id)->where('date_till', '>=', Carbon::now())->get(); 

		$temp = array(); 
		foreach ($availability as $key => $value) {
			// dump(Carbon::parse($value->date_from)); die; 
			$temp[$value->player] = $value; 
		}
		$availability = $temp; 

		*/ 

		// dump($availability); die; 
		$club_points = null; 
		if(!empty($club_id)){
			$club_points = GamePoint::where('club', $club_id)->get(); 
			// dump($club_points); die; 
		}


$lockout_club = true; 




		$club_id = Session::get('sess_club_name'); 
		// dump($club_id); die; 

		
	

	
		// $gameName = User::where(['club_type'=>1])
		// 				->orderBy('club_name','ASC')
		// 				->orderBy('created_at','DESC')
		// 				->value('game_name');
		// $upcomminFixtures = Fixture::where('start_date','>=',date("Y-m-d"))->get();
		$top_trade_in = array(); 
		/*
		$top_trade_in = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('in_out', 1)
						->whereHas('player', function($q) use ($club_id) {
						    $q->where('club', $club_id);
						})
						->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();
		*/
// dump($user_teams_data); 

	if(!empty($user_teams_data))
		$top_trade_in = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('in_out', 1)->where('team_id', $user_teams_data->id)->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();

		// dump($top_trade_in);  

		$top_trade_out = array(); 
		/*
		$top_trade_out = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->whereBetween('created_at', [$cur_gameweek_date, $cur_gameweek_date->copy()->endOfWeek()])->where('in_out', 0)
						->whereHas('player', function($q) use ($club_id) {
						    $q->where('club', $club_id);
						})
						->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();

		*/ 
	if(!empty($user_teams_data))
		$top_trade_out = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->whereBetween('created_at', [$cur_gameweek_date, $cur_gameweek_date->copy()->endOfWeek()])->where('in_out', 0)->where('team_id', $user_teams_data->id)->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();


		// dump($top_trade_out); die; 
		$top_trade_in = array_keys($top_trade_in); 
		$top_trade_out = array_keys($top_trade_out); 
		$temp_top_trade_in = array(); 
		$temp_top_trade_out = array(); 
		// dump($top_trade_in);  
		foreach ($top_trade_in as $key => $value) {
			$tp = Player::where('id', $value)->first(); 
				if(!empty($tp))
					$temp_top_trade_in[]=$tp; 
		}
		foreach ($top_trade_out as $key => $value) {
			$tp = Player::where('id', $value)->first(); 
				if(!empty($tp))
					$temp_top_trade_out[]=$tp; 
		}
		$top_trade_in = $temp_top_trade_in ; 
		$top_trade_out = $temp_top_trade_out ; 


		// dump($top_trade_in); 
		// dump($top_trade_out); die; 




	// end ***************************************** end 
		// dump($club_id); 
	  $club_players =  Player::where('club', $club_id)->where('is_active', 1)->pluck('id')->toArray(); 
	  // dump($club_players); 
// dump($user_teams_data); die; 

/*  // commented on 25/02/20 as not required 

	  $club_players_fantasy_points = array(); 
	  if(!empty($user_teams_data->created_at)){
		  // $club_players_fantasy_points = FixtureScorcard::whereIn('player_id', $club_players)->whereDate('created_at', '>=', Carbon::parse($user_teams_data->created_at)->startOfDay())->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray();  // ->where('status', 3)

	  	$club_players_fantasy_points = FixtureScorcard::whereIn('player_id', $club_players)->where('created_at', '>=', $cur_gameweek_date->copy()->startOfWeek())->where('created_at', '<=', $cur_gameweek_date->copy()->endOfWeek())->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray(); 




	  }

*/

	  // dump($cur_gameweek_date); die; 

// dump($club_players_fantasy_points); die; 
// dump($cur_gameweek_date); 

		// dump($cur_gameweek_date); 
		// dump($cur_gameweek_no); 
		// dump($temp_points_arr); 

	  // dump($cur_gameweek_date); 
	  // dump($cur_gameweek_date->copy()->addWeek()->startOfDay()); 
	  // dump(Carbon::now()->startOfDay()); 
	  // die; 

	  // dump($gameweek_team_value); die; 

	  // dump($chosen_player); 

	  // dump($gameweek_team_value); 
	  // dump($gameweek_team_value_utvl); 


	  	$chosen_player = !empty($gameweek_team_value->gw_player_points) ? @unserialize($gameweek_team_value->gw_player_points) : array();
	  	// dump($chosen_player); 


	  // dump($gameweek_team_value); 
	  // dump($gameweek_team_value_utvl); 

	  	if(!empty($gameweek_team_value_utvl->team_player)){
	  		$chosen_player_utvl = @unserialize($gameweek_team_value_utvl->team_player);

	  		// dump($chosen_player);  
	  		// dump($chosen_player_utvl); die; 
	  		if(!empty($chosen_player_utvl)){
		  		$chosen_player_utvl = array_flip($chosen_player_utvl); 
		  		$chosen_player_utvl = array_fill_keys(array_keys($chosen_player_utvl), 0);
		  	}else{
		  		$chosen_player_utvl = array(); 
		  	}
	  		// dump($chosen_player_utvl); 

	  		$chosen_player = array_replace($chosen_player_utvl, $chosen_player);
	  		// $chosen_player = $chosen_player  + $chosen_player_utvl ; 
	  		// dump($chosen_player);  
	  		// die; 

	  	}	




	  	$chosen_player_points_arr = $chosen_player; 

	  	// dump($chosen_player_points_arr); die; 

		if ($chosen_player !== false){
			$chosen_player = array_keys($chosen_player); 
			$chosen_player = Player::whereIn('id', $chosen_player)->get();
		}else{
			$chosen_player = array(); 
		} 




	  if(!empty($gameweek_team_value_utvl->players_price)){

	  	$chosen_player_price_arr = @unserialize($gameweek_team_value_utvl->players_price); 

	  }


	  // dump($chosen_player); 

	  // dump(unserialize($gameweek_team_value_utvl->team_player)); 
	  // die; 

  	  $gtv_players_points = array(); 
  	  if(!empty($gameweek_team_value->gw_player_points)){

  	  	$gtv_players_points = @unserialize($gameweek_team_value->gw_player_points);
  	 	if ($gtv_players_points === false){
  	 		$gtv_players_points = array(); 
  	 	}
  	  }
	 $gtv_players_price = array(); 
  	 if(!empty($gameweek_team_value->players_price)){
  	 	$gtv_players_price = @unserialize($gameweek_team_value->players_price);
  	 	if ($gtv_players_price === false){
  	 		$gtv_players_price = array(); 
  	 	}
	 }



	 // get fixture detail of this gameweek start ******




        $club_id = \Session::get('sess_club_name');
    // print_r($club_id); 
        $DB                     =   Fixture::query();
        if(!empty($club_id))
            $DB->where('fixtures.club', $club_id); 

       if(!empty($club_id)){
        $temp = array(); 
// whereDate('created_at', '>', Carbon::now()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->endOfWeek())
        // Carbon::now()->subDays(30)

        $DB->leftJoin('teams','teams.id','=','fixtures.team')
                        ->leftJoin('dropdown_managers','dropdown_managers.id','=','fixtures.grade')
                        ->leftJoin('grades','grades.id','=','fixtures.grade')
                        ->where('fixtures.is_active',1); 

        $result_fix =   $DB
                       ->whereBetween('fixtures.start_date',  [$cur_gameweek_date, $cur_gameweek_date->copy()->endOfWeek()] )

                       // ->whereDate('fixtures.created_at', '<=', Carbon::now()->subDays(7 * $i)->endOfWeek() )
                       ->select('teams.name as team_name','grades.grade as grade_name', 'fixtures.*')->get(); 


            if(!empty($result_fix)){
                foreach ($result_fix as $k => $val) {
                    // dump($val); die; 
                    $val->tot_fp = $val->fixture_scorecard->sum('fantasy_points'); 

                }
            
                // $result_fix = $result_fix->sortByDesc('tot_fp');
            }

    }else{
        $result_fix = null; 
    }



    // dump($result_fix); die; 
    
if(!empty($result_fix)){
        foreach ($result_fix as $k1 => $v1) {
            // dump($v1->id); 
    $DB                     =   FixtureScorcard::query();
    $result_fixtt =     $DB->leftjoin('players','fixture_scorecards.player_id','=','players.id')
                        //->leftjoin('fixture','fixture.id','=','fixture_scorecards.fixture_id')
                        ->select('fixture_scorecards.*','players.full_name as player_name','players.position as player_position')

                        ->where('fixture_scorecards.fixture_id',$v1->id)->get();
                $v1->fixture_scorecards = $result_fixtt ; 
        }
}







// dump($prev_page_active); die; 





		return  View::make('front.common.points', compact('result', 'position', 'players','position','svalue','user_teams','chosen_player','capton_id','vice_capton_id', 'my_team_name', 'club_name', 'userteam_fantasy_points', 'club_data', 'no_of_trade', 'gameweek_fantasy_points', 'user_teams_data', 'lockout_club', 'twelve_player_select', 'remain_lock_out_time', 'team_ranks_arr', 'tot_club_users', 'temp_points_arr', 'lockout_time_remaining_seconds', 'booster_active', 'availability', 'club_points', 'top_trade_in', 'top_trade_out', 'cur_gameweek_date', 'cur_gameweek_no', 'gameweek_team_value', 'club_players_fantasy_points', 'gtv_players_points', 'gtv_players_price', 'prev_page_active', 'result_fix', 'gameweek_team_value_utvl', 'chosen_player_points_arr', 'chosen_player_price_arr'));













/*

$drop_down =	new DropDown();
	$position =	$drop_down->get_master_list("position");
	// print_r($position); die; 
	$club_id = \Session::get('sess_club_name');
	if(!empty($club_id)){
		$result = Player::where('club', $club_id)->withCount(['get_scorecard as score_count' => function($query) {
				    $query->select(DB::raw('sum(fantasy_points)'));
				}])->orderBy('score_count', 'DESC')->get(); 
	}else{
		$result = Player::withCount(['get_scorecard as score_count' => function($query) {
				    $query->select(DB::raw('sum(fantasy_points)'));
				}])->orderBy('score_count', 'DESC')->get(); 
	}
	// $result = Player::get(); 



	// start ************************************ start 

	if(!empty($club_id)){
		$cur_club = User::where('id', $club_id)->first(); 

		// dump($club_id); 
		if(empty($cur_gameweek_no) && !empty($cur_club->lockout_start_date)){
			$cur_gameweek_no = Carbon::parse($cur_club->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now());	
			// $cur_gameweek_no++;  // on points page we need previous week data that's why commented 
		}
	}


		if(empty($cur_gameweek_no) || $cur_gameweek_no < 1 || empty($cur_club->lockout_start_date)){
			$cur_gameweek_date= Carbon::now()->startOfWeek();
			$cur_gameweek_no = 1; 
		}else{

			$cur_gameweek_date= Carbon::parse($cur_club->lockout_start_date)->startOfWeek()->addWeeks($cur_gameweek_no)->subDay()->startOfWeek();   // subday for come in the last week range
		}



	
		// dump($cur_gameweek_no); 
		// dump($cur_gameweek_date);  die; 


		$drop_down =	new DropDown();
	    $position =	$drop_down->get_master_list("position");
	    // dump($position); die; 
	    $position = array_replace(array_flip(array('4', '1', '3', '2')), $position);
	    // dump($position); die;
	    $category =	$drop_down->get_master_list("category");
	    $svalue	=	$drop_down->get_master_list("svalue");
	    $svalue = Config::get('player_price_list'); 
	    $jvalue =	$drop_down->get_master_list("jvalue");
	    $capton_id = 0; 
	    $vice_capton_id = 0; 
	    $my_team_name = ''; 
	    $userteam_fantasy_points = array() ; 
	    $gameweek_fantasy_points = array() ; 
	    $club_name = Session::get('sess_club_name'); 
	    // print_r($club_name); 
	    $club_data = User::where('id', $club_name)->first(); 
	    $game_point_data = GamePoint::where('club', $club_name)->get(); 
	    // dump($club_data); die; 
	    // dump($club_name); 
	    // dump($game_point_data);  

	    $players =  Player::where('club', $club_name)->where('full_name','!=','')->where('is_active', 1)->get(); 
	    $chosen_player = []; 
		$user_teams = UserTeams::where('user_id', auth()->guard('web')->user()->id)->where('club_id', $club_name)->first();
		// dump($user_teams); die; 
		$user_teams_data = $user_teams ; 
		// dump($user_teams_data); die; 
		$no_of_trade = 0; 
		if ($user_teams) {
			$capton_id = $user_teams->capton_id; 
			$vice_capton_id = $user_teams->vice_capton_id; 
			$my_team_name = $user_teams->my_team_name;

			if($cur_gameweek_date->diffInWeeks(Carbon::now()) == 0 && Carbon::now()->startOfDay() >= $cur_gameweek_date){
				// dump(Carbon::now()); 
				// dump($cur_gameweek_date); 
				// echo 'asldfk'; die; 
				$no_of_trade = $user_teams->no_of_trade; 
			}else{
				$no_of_trade = UserTeamTradeLog::where('team_id', $user_teams->id)->whereDate('created_at', '<=', $cur_gameweek_date->copy()->endOfWeek())->sum('no_of_trade'); 
				// dump($user_teams->id); 
				// dump( $cur_gameweek_date->copy()->endOfWeek()->endOfDay()); die; 
				$gameweek_team_value = UserTeamsGWExtraPTTrack::where(['team_id' => $user_teams->id, 'gw_end_date' => $cur_gameweek_date->copy()->endOfWeek()->endOfDay() ])->first(); 

				// dump($gameweek_team_value); die; 




			}



			// whereDate('created_at', '<=', $cur_gameweek_date->copy()->endOfWeek())

			// $club_name =  $user_teams->club_id;
		    $user_teams = $user_teams->toArray();
		}else{
			$user_teams = [];
		}
		if(!empty($user_teams) && !empty($gameweek_team_value)){


			$gameweek_team_value_utvl = UserTeamValueLog::where('team_id', $gameweek_team_value->team_id)->whereNotNull('team_value')->where('created_at', '<', Carbon::parse($gameweek_team_value->gw_end_date)->endOfWeek()->addHour())->orderBy('id', 'DESC')->first(); 

			$team_creation_date = $user_teams['created_at'] ; 
			// dump($user_teams);  
			$capton_id = $user_teams['capton_id']; 
			$vice_capton_id = $user_teams['vice_capton_id']; 
			$twelveth_man_id = $user_teams['twelveth_man_id'] ; 
			$user_teams = UserTeamPlayers::where('team_id', $user_teams['id'])->pluck('player_id')->toArray();
			// dump($user_teams);

            if(!empty($user_teams['twelve_man_card_last_date']) && (Carbon::parse($user_teams['twelve_man_card_last_date'])->startOfWeek() < Carbon::now() && Carbon::now() < Carbon::parse($user_teams['twelve_man_card_last_date'])->endOfWeek()  ) && !empty($twelveth_man_id)){
                // $user_teams[] = $twelveth_man_id; 
                array_push($user_teams, $twelveth_man_id);
            }
			// dump($user_teams); die; 


			// game week calculation start 




			// dump($user_teams_data->id); 

			$user_team_player = UserTeamPlayers::where('team_id', $user_teams_data->id)->get(); 
			// dump($user_team_player); 
			$tmp_top_pnt = 0 ; 

		


            // $temp_last_wk_pt[$value->id] = $tmp_top_pnt; 

                    // $gameweek_fantasy_points =  $tmp_top_pnt + $gw_extra_pt; 

                    // $gameweek_fantasy_points = $gameweek_fantasy_points > 0 ? $gameweek_fantasy_points : 0 ; 

                    $gameweek_fantasy_points = !empty($gameweek_team_value->gw_pt) ? $gameweek_team_value->gw_pt : 0 ;

			// game week calculation end 


			// get end of week date

			// $user_teams = unserialize($user_teams['player_ids']); 
			// $chosen_player = Player::whereIn('id', $user_teams)->get();

		}


$tot_club_users = 0; 

// rank calc

	$club_id = \Session::get('sess_club_name');
		if(!empty($club_id)){
		// $result = UserTeams::leftJoin('users','users.id','=','user_teams.user_id')->where('club_id', $club_id)
		// 			//->where('user_teams.club_id', $club_id)
		// 			//->leftJoin('')
		// 			->with('getTeamPlayer')
		// 			->select('user_teams.*','users.first_name','users.club_logo')
		// 			->get();


		// check the previous link active on points page start 

		// dump($cur_gameweek_date); 

		$prev_page_active = true; 
	if(!empty($user_teams_data) && !empty($gameweek_team_value)){
		// $ppa1 = UserTeamValueLog::where('team_id', $user_teams_data->id)->whereNotNull('team_player')->whereBetween('created_at', [$cur_gameweek_date, $cur_gameweek_date->copy()->endOfWeek()])->orderBy('id', 'ASC')->first(); 
		// $ppa2 = UserTeamValueLog::where('team_id', $user_teams_data->id)->whereNotNull('team_player')->whereBetween('created_at', [$cur_gameweek_date->copy()->subWeek()->startOfWeek(), $cur_gameweek_date->copy()->subWeek()->endOfWeek()])->orderBy('id', 'ASC')->first(); 
		// $ppa3 = UserTeamValueLog::where('team_id', $user_teams_data->id)->whereNotNull('team_player')->whereBetween('created_at', [$cur_gameweek_date->copy()->subWeeks(2)->startOfWeek(), $cur_gameweek_date->copy()->subWeeks(2)->endOfWeek()])->orderBy('id', 'ASC')->first(); 

		// if(empty($ppa1) && empty($ppa2) && empty($ppa3)){
		if(UserTeamsGWExtraPTTrack::where('team_id', $gameweek_team_value)->where('gw_end_date', Carbon::parse($gameweek_team_value->gw_end_date)->subWeek()->subDay()->endOfWeek()->endOfDay())->doesntExist()){
			$prev_page_active = false; 
		}
	}else{
		$prev_page_active = false; 
	}

		// $user_teams_data
		// check the previous link active on points page end



		// foreach ($user_teams_data as $k4 => $v4) {
		// 	dump($k4);
		// 	dump($v4);  
		// }

		// die; 

		// dump($user_teams_data); 
		// dump($user_teams_dt); 
		// dump($temp_points_arr); 
		// dump($team_ranks_arr); 
		// die; 
		$tot_club_users = UserTeams::where('club_id', $club_id)->whereDate('created_at', '<=', $cur_gameweek_date->copy()->startOfWeek())->count(); 

		// dump($tot_club_users); die; 

		}


		
		/*
		$booster_active = array( 'name' => false, 'status' => false );  
		if(!empty($user_teams_data) && !empty(UserTeams::where('id', $user_teams_data->id)->first()->capton_card_last_date)){
			UserTeams::where('id', $user_teams_data->id)->update(['twelve_man_card_last_date' => null]);
			
			$booster_active['name'] = '3x'; 
			$booster_active['status'] = true; 
		}
		// if(!empty($user_teams_data->twelve_man_card_last_date)){
		if(!empty($user_teams_data) && !empty(UserTeams::where('id', $user_teams_data->id)->first()->twelve_man_card_last_date)){
			UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
			$booster_active['name'] = '12th'; 
			$booster_active['status'] = true; 
		}

		*/ 

// dump($user_teams_data); 
// dump($cur_gameweek_date); 
/*
			if(!empty($user_teams_data)){
				if(!empty($user_teams_data->capton_card_last_date) &&  Carbon::parse($user_teams_data->capton_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
				if(!empty($user_teams_data->twelve_man_card_last_date) &&  Carbon::parse($user_teams_data->twelve_man_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['twelve_man_card_last_date' => null]);
				if(!empty($user_teams_data->dealer_card_last_date) &&  Carbon::parse($user_teams_data->dealer_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['dealer_card_last_date' => null]);
				if(!empty($user_teams_data->flipper_card_last_date) &&  Carbon::parse($user_teams_data->flipper_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['flipper_card_last_date' => null]);
				
			}



		$booster_active = array( 'name' => false, 'status' => false );  
		if(!empty($user_teams_data) && UserTeamLogs::where('team_id', $user_teams_data->id)->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('log_type', 2)->exists()){
			// UserTeams::where('id', $user_teams_data->id)->update(['twelve_man_card_last_date' => null]);
			
			$booster_active['name'] = '3x'; 
			$booster_active['status'] = true; 
		}
		// if(!empty($user_teams_data->twelve_man_card_last_date)){
		if(!empty($user_teams_data) && UserTeamLogs::where('team_id', $user_teams_data->id)->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('log_type', 3)->exists()){
			// UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
			$booster_active['name'] = '12th'; 
			$booster_active['status'] = true; 
		}

		if(!empty($user_teams_data) && UserTeamLogs::where('team_id', $user_teams_data->id)->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('log_type', 4)->exists()){
			// UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
			$booster_active['name'] = 'Dealer'; 
			$booster_active['status'] = true; 
		}

		if(!empty($user_teams_data) && UserTeamLogs::where('team_id', $user_teams_data->id)->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('log_type', 5)->exists()){
			// UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
			$booster_active['name'] = 'Flipper'; 
			$booster_active['status'] = true; 
		}

		/* 
		$availability = Availability::where('is_active',1)->where('club', $club_id)->where('date_till', '>=', Carbon::now())->get(); 

		$temp = array(); 
		foreach ($availability as $key => $value) {
			// dump(Carbon::parse($value->date_from)); die; 
			$temp[$value->player] = $value; 
		}
		$availability = $temp; 

		*/ 
/*
		// dump($availability); die; 
		$club_points = null; 
		if(!empty($club_id)){
			$club_points = GamePoint::where('club', $club_id)->get(); 
			// dump($club_points); die; 
		}


$lockout_club = true; 




		$club_id = Session::get('sess_club_name'); 
		// dump($club_id); die; 

		
	

	
		// $gameName = User::where(['club_type'=>1])
		// 				->orderBy('club_name','ASC')
		// 				->orderBy('created_at','DESC')
		// 				->value('game_name');
		// $upcomminFixtures = Fixture::where('start_date','>=',date("Y-m-d"))->get();
		$top_trade_in = array(); 
		/*
		$top_trade_in = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('in_out', 1)
						->whereHas('player', function($q) use ($club_id) {
						    $q->where('club', $club_id);
						})
						->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();
		*/
// dump($user_teams_data); 
/*
	if(!empty($user_teams_data))
		$top_trade_in = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->whereBetween('created_at', [$cur_gameweek_date , $cur_gameweek_date->copy()->endOfWeek()])->where('in_out', 1)->where('team_id', $user_teams_data->id)->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();

		// dump($top_trade_in);  

		$top_trade_out = array(); 
		/*
		$top_trade_out = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->whereBetween('created_at', [$cur_gameweek_date, $cur_gameweek_date->copy()->endOfWeek()])->where('in_out', 0)
						->whereHas('player', function($q) use ($club_id) {
						    $q->where('club', $club_id);
						})
						->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();

		*/ 
	/*
	if(!empty($user_teams_data))
		$top_trade_out = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->whereBetween('created_at', [$cur_gameweek_date, $cur_gameweek_date->copy()->endOfWeek()])->where('in_out', 0)->where('team_id', $user_teams_data->id)->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();


		// dump($top_trade_out); die; 
		$top_trade_in = array_keys($top_trade_in); 
		$top_trade_out = array_keys($top_trade_out); 
		$temp_top_trade_in = array(); 
		$temp_top_trade_out = array(); 
		// dump($top_trade_in);  
		foreach ($top_trade_in as $key => $value) {
			$tp = Player::where('id', $value)->first(); 
				if(!empty($tp))
					$temp_top_trade_in[]=$tp; 
		}
		foreach ($top_trade_out as $key => $value) {
			$tp = Player::where('id', $value)->first(); 
				if(!empty($tp))
					$temp_top_trade_out[]=$tp; 
		}
		$top_trade_in = $temp_top_trade_in ; 
		$top_trade_out = $temp_top_trade_out ; 


		// dump($top_trade_in); 
		// dump($top_trade_out); die; 




	// end ***************************************** end 
		// dump($club_id); 
	  $club_players =  Player::where('club', $club_id)->where('is_active', 1)->pluck('id')->toArray(); 
	  // dump($club_players); 
// dump($user_teams_data); die; 
	  $club_players_fantasy_points = array(); 
	  if(!empty($user_teams_data->created_at)){
		  // $club_players_fantasy_points = FixtureScorcard::whereIn('player_id', $club_players)->whereDate('created_at', '>=', Carbon::parse($user_teams_data->created_at)->startOfDay())->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray();  // ->where('status', 3)

	  	$club_players_fantasy_points = FixtureScorcard::whereIn('player_id', $club_players)->where('created_at', '>=', $cur_gameweek_date->copy()->startOfWeek())->where('created_at', '<=', $cur_gameweek_date->copy()->endOfWeek())->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray(); 




	  }
	  // dump($cur_gameweek_date); die; 

// dump($club_players_fantasy_points); die; 
// dump($cur_gameweek_date); 

		// dump($cur_gameweek_date); 
		// dump($cur_gameweek_no); 
		// dump($temp_points_arr); 

	  // dump($cur_gameweek_date); 
	  // dump($cur_gameweek_date->copy()->addWeek()->startOfDay()); 
	  // dump(Carbon::now()->startOfDay()); 
	  // die; 

	  // dump($gameweek_team_value); die; 

	  // dump($chosen_player); 

	  // dump($gameweek_team_value); 
	  if(!empty($gameweek_team_value->gw_player_points)){

	  	$chosen_player = @unserialize($gameweek_team_value->gw_player_points);
	  	// dump($chosen_player); 
		if ($chosen_player !== false){
			$chosen_player = array_keys($chosen_player); 
			$chosen_player = Player::whereIn('id', $chosen_player)->get();
		}else{
			$chosen_player = array(); 
		} 
	  }else{
	  	$chosen_player = array(); 
	  }

  	  $gtv_players_points = array(); 
  	  if(!empty($gameweek_team_value->gw_player_points)){

  	  	$gtv_players_points = @unserialize($gameweek_team_value->gw_player_points);
  	 	if ($gtv_players_points === false){
  	 		$gtv_players_points = array(); 
  	 	}
  	  }
	 $gtv_players_price = array(); 
  	 if(!empty($gameweek_team_value->players_price)){
  	 	$gtv_players_price = @unserialize($gameweek_team_value->players_price);
  	 	if ($gtv_players_price === false){
  	 		$gtv_players_price = array(); 
  	 	}
	 }



	 // get fixture detail of this gameweek start ******




        $club_id = \Session::get('sess_club_name');
    // print_r($club_id); 
        $DB                     =   Fixture::query();
        if(!empty($club_id))
            $DB->where('fixtures.club', $club_id); 

       if(!empty($club_id)){
        $temp = array(); 
// whereDate('created_at', '>', Carbon::now()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->endOfWeek())
        // Carbon::now()->subDays(30)

        $DB->leftJoin('teams','teams.id','=','fixtures.team')
                        ->leftJoin('dropdown_managers','dropdown_managers.id','=','fixtures.grade')
                        ->leftJoin('grades','grades.id','=','fixtures.grade')
                        ->where('fixtures.is_active',1); 

        $result_fix =   $DB
                       ->whereBetween('fixtures.start_date',  [$cur_gameweek_date, $cur_gameweek_date->copy()->endOfWeek()] )

                       // ->whereDate('fixtures.created_at', '<=', Carbon::now()->subDays(7 * $i)->endOfWeek() )
                       ->select('teams.name as team_name','grades.grade as grade_name', 'fixtures.*')->get(); 


            if(!empty($result_fix)){
                foreach ($result_fix as $k => $val) {
                    // dump($val); die; 
                    $val->tot_fp = $val->fixture_scorecard->sum('fantasy_points'); 

                }
            
                // $result_fix = $result_fix->sortByDesc('tot_fp');
            }

    }else{
        $result_fix = null; 
    }



    // dump($result_fix); die; 
    
if(!empty($result_fix)){
        foreach ($result_fix as $k1 => $v1) {
            // dump($v1->id); 
    $DB                     =   FixtureScorcard::query();
    $result_fixtt =     $DB->leftjoin('players','fixture_scorecards.player_id','=','players.id')
                        //->leftjoin('fixture','fixture.id','=','fixture_scorecards.fixture_id')
                        ->select('fixture_scorecards.*','players.full_name as player_name','players.position as player_position')

                        ->where('fixture_scorecards.fixture_id',$v1->id)->get();
                $v1->fixture_scorecards = $result_fixtt ; 
        }
}




// dump($result_fix); 


	 // get fixture detail of this gameweek end *****





// dump($gtv_players_points); die; 





		return  View::make('front.common.points', compact('result', 'position', 'players','position','svalue','user_teams','chosen_player','capton_id','vice_capton_id', 'my_team_name', 'club_name', 'userteam_fantasy_points', 'club_data', 'no_of_trade', 'gameweek_fantasy_points', 'user_teams_data', 'lockout_club', 'twelve_player_select', 'remain_lock_out_time', 'team_ranks_arr', 'tot_club_users', 'temp_points_arr', 'lockout_time_remaining_seconds', 'booster_active', 'availability', 'club_points', 'top_trade_in', 'top_trade_out', 'cur_gameweek_date', 'cur_gameweek_no', 'gameweek_team_value', 'club_players_fantasy_points', 'gtv_players_points', 'gtv_players_price', 'prev_page_active', 'result_fix', 'gameweek_team_value_utvl'));

*/ 
	}

	public function history(){


ini_set('memory_limit', '-1');
		
		// $temp_lockout_start_date = Carbon::now()->subMonths(7);
		// while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->endOfWeek()) {

		// 	$all_teams = UserTeams::all(); 

		// 	foreach ($all_teams as $key => $value) {
		// 		if(UserTeamsGWExtraPTTrack::where(['team_id' => $value->id, 'gw_end_date' => $temp_lockout_start_date->copy()->endOfWeek()->endOfDay() ])->count()  >= 2){
		// 			dump(UserTeamsGWExtraPTTrack::where(['team_id' => $value->id, 'gw_end_date' => $temp_lockout_start_date->copy()->endOfWeek()->endOfDay() ])->count()); 
		// 		}
		// 	}


		// 	$temp_lockout_start_date->addWeek(); 
		// }

		// die; 

// dump(Carbon::now()->subDays(2)->endOfWeek()->endOfDay()); die; 


		$club_id = \Session::get('sess_club_name');
	$top_player_by_gwk_point = array(); 
		$temp_club_data = User::where('id', $club_id)->first(); 
		
		$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date)->startOfWeek(); 
// echo 'hi'; die; 
			$uteam = UserTeams::where('user_id', Auth::user()->id)->where('club_id', $club_id)->first(); 	
			

		$i = 1; 

		if(!empty($uteam)){
		while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->endOfWeek()) {

// dump($temp_lockout_start_date); 



            if(UserTeamsGWExtraPTTrack::where(['team_id' => $uteam->id, 'gw_end_date' => $temp_lockout_start_date->copy()->endOfWeek()->endOfDay() ])->exists()){

            	$tmp = UserTeamsGWExtraPTTrack::where('team_id', $uteam->id)->where('gw_end_date', $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->first(); 


            }else{
            	$tmp = UserTeamsGWExtraPTTrack::where('team_id', $uteam->id)->where('gw_end_date', '<', $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->orderBy('id', 'DESC')->first(); 
            }


			$no_of_trade = UserTeamTradeLog::where('team_id', $uteam->id)->where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->sum('no_of_trade') ; 

			$utvl = UserTeamValueLog::where('team_id', $uteam->id)->whereNotNull('team_value')->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek()->addHour())->orderBy('id', 'DESC')->first(); 

			$top_player_by_gwk_point[$i]['gw'] = $i; 
			$top_player_by_gwk_point[$i]['gw_pts'] = !empty($tmp->gw_pt) && $tmp->gw_end_date == $temp_lockout_start_date->copy()->endOfWeek()->endOfDay() ? round($tmp->gw_pt) : 0; 
			$top_player_by_gwk_point[$i]['gw_rank'] = !empty($tmp->gw_rank) && $tmp->gw_end_date == $temp_lockout_start_date->copy()->endOfWeek()->endOfDay() ? $tmp->gw_rank : '--'; 
			$top_player_by_gwk_point[$i]['trades_made'] = $no_of_trade; 
			$top_player_by_gwk_point[$i]['overall_pts'] = !empty($tmp->overall_pt) ? round($tmp->overall_pt) : 0; 
			$top_player_by_gwk_point[$i]['overall_rank'] = !empty($tmp->overall_rank) ? $tmp->overall_rank : 0; 
			$top_player_by_gwk_point[$i]['team_value'] =  !empty($utvl->team_value) ? $utvl->team_value : 0; 
// echo 'hi'; die; 
// dump($top_player_by_gwk_point); die; 
			$temp_lockout_start_date->addWeek(); 
			$i++; 

		}
	}

// dump($top_player_by_gwk_point); die; 

		// dump($top_player_by_gwk_point); die; 

		// GW - GWP - GWR - Trades Made - Overall Pts - Overall Rank - Team Value - More

		return  View::make('front.common.history', compact('top_player_by_gwk_point')); 

	}


	public function loadHistoryData(Request $request){


		// print_r($_REQUEST); die; 

		// print_r($request->pgno); die; 



	$club_id = \Session::get('sess_club_name');

$temp_last_wk_pt = array(); 
	// end ***************************************** end 
	$data = array('draw' => $request->draw, 'recordsTotal' => 0 , 'recordsFiltered' => 0 , 'data' => []); 
	$top_player_by_gwk_point = array(); 
	$user_teams = UserTeams::where('club_id', $club_id)->where('user_id', Auth::user()->id)->first(); 

		if(!empty($club_id) && !empty($user_teams)){


$all_club_team = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 

			$all_temp_utp = array(); 
			// $temp_utp = UserTeamPlayers::where('team_id', $user_teams->id)->pluck('player_id')->toArray(); 

			// foreach ($temp_utp as $key => $value) {
			// 	$all_temp_utp[$value] = UserTeamsPlayerTrack::where('player_id', $value)->where('team_id', $user_teams->id)->whereNull('remove_date')->first(); 
			// 	if(empty($all_temp_utp[$value])){
			// 		$all_temp_utp[$value] = UserTeamPlayers::where('player_id', $value)->where('team_id', $user_teams->id)->first(); 
			// 	}
			// }

			// $temp_utp = $all_temp_utp ; 
			// dump($all_temp_utp); die; 





			$temp_club_data = User::where('id', $club_id)->first(); 
			
			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date)->startOfWeek(); 
			// dump($temp_lockout_start_date->startOfWeek()); 
			// dump($temp_lockout_start_date->endOfWeek()); 
			// dump($temp_lockout_start_date->addWeek()); 
			// dump($temp_lockout_start_date->startOfWeek()); 
			// dump($temp_lockout_start_date->endOfWeek()); 
			// die; 
			// dump(Auth::user()->id); die; 

	// GW - GWP - GWR - Trades Made - Overall Pts - Overall Rank - Team Value - More


			$i = 1; 
			$page_cnt = 1; 
			$skip_rec = $request->start; 
			$lenght_rec = $request->length; 
			while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->endOfWeek()) {

				// if($i > 1) break; 



				$gameweek_team_value =  UserTeamValueLog::where('team_id', $user_teams->id)->whereNotNull('team_player')->whereDate('created_at', '>=', $temp_lockout_start_date)->whereDate('created_at', '<=', $temp_lockout_start_date->copy()->endOfWeek())->orderBy('id', 'DESC')->first(); 


				if(empty($gameweek_team_value)){  // if gameweek is empty search previous old record
					// $gameweek_team_value = UserTeamValueLog::where('team_id', $user_teams->id)->whereNotNull('team_player')->where('created_at', '<', $temp_lockout_start_date)->orderBy('id', 'DESC')->first(); 

$gameweek_team_value = UserTeamValueLog::where('team_id', $user_teams->id)->whereNotNull('team_player')->whereBetween('created_at', [$temp_lockout_start_date->copy()->subWeek()->startOfWeek(), $temp_lockout_start_date->copy()->subWeek()->endOfWeek()])->orderBy('id', 'DESC')->first(); 


				}
				if(empty($gameweek_team_value)){  // if empty search next  future record
					// $gameweek_team_value = UserTeamValueLog::where('team_id', $user_teams->id)->whereNotNull('team_player')->where('created_at', '>', $temp_lockout_start_date)->orderBy('id', 'ASC')->first(); 

$gameweek_team_value = UserTeamValueLog::where('team_id', $user_teams->id)->whereNotNull('team_player')->whereBetween('created_at', [$temp_lockout_start_date->copy()->addWeek()->startOfWeek(), $temp_lockout_start_date->copy()->addWeek()->endOfWeek()])->orderBy('id', 'ASC')->first(); 


				}	



// dump($gameweek_team_value); die; 



		

if(!empty($gameweek_team_value) && ($skip_rec < $page_cnt && $page_cnt <= $skip_rec+$lenght_rec) ){ // get the fixed no of record 



				$no_of_trade = UserTeamTradeLog::where('team_id', $user_teams->id)->where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->sum('no_of_trade') ; 


				// $overall_pts = FixtureScorcard::whereIn('player_id', $team_players)
				// 								->whereDate('created_at', '<', $temp_lockout_start_date->endOfWeek())->sum('fantasy_points'); 
								// ->groupBy('player_id')
								//  ->selectRaw('sum(fantasy_points) as sum, sum(rs) as sum_rs, player_id')->pluck();

		
// dump($temp_lockout_start_date); 
				// dump($temp_var); die; 
		


		$temp_utp = unserialize($gameweek_team_value->team_player); 
// print_r($temp_utp); 
				$tot_sum_fp = 0; 
				$top_sum_rs = 0; 
				// dump($temp_lockout_start_date); 
				foreach ($temp_utp as $key3 => $value3) {
					// $sum_fp = FixtureScorcard::where('player_id', $value3)
					// 				->where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->sum('fantasy_points'); 


$sum_fp = FixtureScorcard::where('player_id', $value3)

                        ->whereHas('fixture', function($q) use ($temp_lockout_start_date){
   							$q->where('start_date', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('start_date', '<=', $temp_lockout_start_date->copy()->endOfWeek()); 


                                 })

                        // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())

                        ->sum('fantasy_points'); 




					// $sum_rs = FixtureScorcard::where('player_id', $value3)
					// 				->where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->sum('rs'); 
									// ->groupBy('player_id')
									//  ->selectRaw('sum(fantasy_points) as sum, sum(rs) as sum_rs, player_id')->get(); 
								   // ->selectRaw('sum(fantasy_points) as sum, sum(rs) as sum_rs, sum(fours) as sum_fours, sum(sixes) as sum_sixes, sum(wks) as sum_wks, sum(mdns) as sum_mdns, sum(cs) as sum_cs, sum(hattrick) as sum_hattrick, player_id')->get();  // ->orderBy('sum', 'DESC')
								   // ->pluck('sum','player_id')->toArray();
					if($value3 == $user_teams->capton_id){
						$sum_fp = $sum_fp * 2; 

					}
					if($value3 == $user_teams->v_capton_id){
						$sum_fp = round($sum_fp * 1.5);
					}
					$tot_sum_fp += $sum_fp; 
					// $top_sum_rs += $sum_rs; 
				}

                if(UserTeamsGWExtraPTTrack::where(['team_id' => $user_teams->id, 'gw_end_date' => $temp_lockout_start_date->copy()->endOfWeek()->endOfDay() ])->exists()){

                	$tmp = UserTeamsGWExtraPTTrack::where('team_id', $user_teams->id)->where('gw_end_date', $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->first(); 
			// dump($tmp); 
// echo 'hi'; die; 
                		$tot_sum_fp += round($tmp->gw_extra_pt)  ; 



                }


                    // if(UserTeamLogs::where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->where('team_id', $user_teams->id)->where('log_type', 4)->exists()){

                    // 	$tot_sum_fp = $tot_sum_fp-50; // subtract 50 points if dealer card is active in this game week 

                    // }












				$overall_pts = (!empty($gameweek_team_value)) ? $gameweek_team_value->tot_team_point : 0 ; 
				$team_value= (!empty($gameweek_team_value)) ? $gameweek_team_value->team_value : 0 ; 
				$team_rank= (!empty($gameweek_team_value)) ? $gameweek_team_value->team_rank : 0 ; 



						$top_player_by_gwk_point[$i]['gw'] = $i; 
						$top_player_by_gwk_point[$i]['gw_pts'] = $tot_sum_fp; 
						// $top_player_by_gwk_point[$i]['gw_run'] = $top_sum_rs; 
						$top_player_by_gwk_point[$i]['trades_made'] = $no_of_trade; 
						$top_player_by_gwk_point[$i]['overall_pts'] = $overall_pts; 
						$top_player_by_gwk_point[$i]['overall_rank'] = $team_rank; 
						$top_player_by_gwk_point[$i]['team_value'] = $team_value; 




// dump(unserialize($gameweek_team_value->team_player)); die; 

	// gameweek rank calculation start ********************************************************************************************************************
		// dump($club_id); 
		// dump($all_club_team); die; 
	// gameweek point calucation for each team of this club 
		foreach ($all_club_team as $key => $value) {



		$temp = array(); 
		// $temp_last_wk_pt = array(); 




				$gameweek_team_value_for_clubteam =  UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->whereDate('created_at', '>=', $temp_lockout_start_date)->whereDate('created_at', '<=', $temp_lockout_start_date->copy()->endOfWeek())->orderBy('id', 'DESC')->first(); 


				if(empty($gameweek_team_value_for_clubteam)){  // if gameweek is empty search previous old record
					// $gameweek_team_value_for_clubteam = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->where('created_at', '<', $temp_lockout_start_date)->orderBy('id', 'DESC')->first(); 

$gameweek_team_value_for_clubteam = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->whereBetween('created_at', [$temp_lockout_start_date->copy()->subWeek()->startOfWeek(), $temp_lockout_start_date->copy()->subWeek()->endOfWeek()])->orderBy('id', 'DESC')->first(); 


				}
				if(empty($gameweek_team_value_for_clubteam)){  // if empty search next  future record
					// $gameweek_team_value_for_clubteam = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->where('created_at', '>', $temp_lockout_start_date)->orderBy('id', 'ASC')->first(); 

$gameweek_team_value_for_clubteam = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->whereBetween('created_at', [$temp_lockout_start_date->copy()->addWeek()->startOfWeek(), $temp_lockout_start_date->copy()->addWeek()->endOfWeek()])->orderBy('id', 'ASC')->first(); 


				}	




				// dump($gameweek_team_value_for_clubteam); 
		// foreach ($user_teams as $key => $value) {
		if(!empty($gameweek_team_value_for_clubteam) && !empty($gameweek_team_value_for_clubteam->team_player)){


		
			
			// $user_teams_data = $value ; 
			$user_team_player = unserialize($gameweek_team_value_for_clubteam->team_player); 

			$tmp_top_pnt = 0 ; 
			foreach ($user_team_player as $k1 => $v1) {




                    // dump($v1->player_id); 
				if($v1 == $gameweek_team_value_for_clubteam->capton_id){



                        $tmp_pnt = FixtureScorcard::where('player_id', $v1)

                        ->whereHas('fixture', function($q) use ($temp_lockout_start_date){
   							$q->where('start_date', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('start_date', '<=', $temp_lockout_start_date->copy()->endOfWeek()); 


                                 })

                        // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())

                        ->sum('fantasy_points'); // ->where('status', 3)



    // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)
    //                    ->whereHas('fixture', function($q) use ($tmp_strt_dt){
    //                                  $q->where('start_date', '>', Carbon::parse($tmp_strt_dt)->endOfDay());
    //                              })
    // ->sum('fantasy_points'); // ->where('status', 3)








                        // dump($tmp_pnt); 
                        // echo 'hello'; 
                        $tmp_top_pnt += round($tmp_pnt) ; 
                    }


                    if($v1 == $gameweek_team_value_for_clubteam->v_capton_id){



                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)


    	             $tmp_pnt = FixtureScorcard::where('player_id', $v1)

    	             ->whereHas('fixture', function($q) use ($temp_lockout_start_date){
							$q->where('start_date', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('start_date', '<=', $temp_lockout_start_date->copy()->endOfWeek()); 

                                 })

    	             // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())


    	             ->sum('fantasy_points'); // ->where('status', 3)



 











                        $tmp_top_pnt += round($tmp_pnt * 0.5) ; 
                    }



                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)



                        $tmp_pnt = FixtureScorcard::where('player_id', $v1)


					->whereHas('fixture', function($q) use ($temp_lockout_start_date){

						$q->where('start_date', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('start_date', '<=', $temp_lockout_start_date->copy()->endOfWeek()); 


					                                 })


                        // ->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())


                        ->sum('fantasy_points'); // ->where('status', 3)



                        $tmp_top_pnt += $tmp_pnt ; 





			}



                    $gw_extra_pt = 0 ; 

                    if(UserTeamsGWExtraPTTrack::where(['team_id' => $value, 'gw_end_date' => $temp_lockout_start_date->copy()->endOfWeek()->endOfDay() ])->exists()){

                    	$tmp = UserTeamsGWExtraPTTrack::where('team_id', $value)->where('gw_end_date', $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->first(); 
				// dump($tmp); 

                    		$gw_extra_pt = round($tmp->gw_extra_pt)  ; 




                    }



                    if(UserTeamLogs::where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->where('team_id', $value)->where('log_type', 4)->exists()){

                    	$tmp_top_pnt = $tmp_top_pnt-50; // subtract 50 points if dealer card is active in this game week 

                    }                    












			$temp_last_wk_pt[$i][$value] = $tmp_top_pnt + $gw_extra_pt; 
			// echo 'hi'; die; 

			
		}





		}

		// dump($temp_last_wk_pt);  
	// gameweek rank calculation ends *****************************************************************************************************************

// break; 

}

if(!empty($gameweek_team_value) ){
	$page_cnt++; // increase the page data 		
}

						// $top_player_by_gwk_point[$i]['sum_fours'] = $value->sum_fours; 
						// $top_player_by_gwk_point[$i]['sum_sixes'] = $value->sum_sixes; 
						// $top_player_by_gwk_point[$i]['sum_wks'] = $value->sum_wks; 
						// $top_player_by_gwk_point[$i]['sum_mdns'] = $value->sum_mdns; 
						// $top_player_by_gwk_point[$i]['sum_cs'] = $value->sum_cs; 
						// $top_player_by_gwk_point[$i]['sum_hattrick'] = $value->sum_hattrick; 
					
						// gw gw_pts gw_run trades_made overall_pts overall_rank team_value 
// dump($temp_last_wk_pt);  


		$temp_lockout_start_date->addWeek(); 

				$i++; 
				 
			}



		// dump($temp_last_wk_pt); 
		if(!empty($temp_last_wk_pt)){
			foreach ($temp_last_wk_pt as $key => $value) {
				// dump($value); die; 
				$value = array_filter($value); 
				arsort($value); 
				$i = 1; 
				foreach ($value as $k => $v) {
					$value[$k] = $i++; 
				}
				$temp_last_wk_pt[$key] = $value; 
				
			}


		}

		// print_r($temp_last_wk_pt); print_r($top_player_by_gwk_point); die; 

     $temp_last_wk_pt = array_filter($temp_last_wk_pt); 
        $data = array();
        $i = 0; 
     	foreach ($top_player_by_gwk_point as $key => $value) {
     		$tmp = array(); 
     
     	$i++; 
     		$tmp[] =  $value['gw']; 
     		$tmp[] =  $value['gw_pts']; 
     		$tmp[] =  !empty($temp_last_wk_pt[$value['gw']][$user_teams->id])  ? $temp_last_wk_pt[$value['gw']][$user_teams->id] : '--'; 
     		$tmp[] =  $value['trades_made']; 
     		$tmp[] =  !empty($value['overall_pts']) ? $value['overall_pts'] : '--'; 
     		$tmp[] =  !empty($value['overall_rank']) ? $value['overall_rank'] : '--'; 
     		$tmp[] =  $value['team_value']; 
     		$tmp[] =  !empty($prev_val) ? ($prev_val>  $value['team_value'] ? 'DOWN' : ($prev_val < $value['team_value'] ? 'UP' : 'NO CHANGE') ) : 'NO CHANGE'; 

     		$prev_val = $value['team_value']; 
     		$data[] = $tmp; 

     	}

     	$data = array('draw' => $request->draw, 'recordsTotal' => $page_cnt-1 , 'recordsFiltered' => $page_cnt-1 , 'data' => $data); 

     	// print_r($data); die; 
	// $data = array('team_id' => $user_teams->id, 'temp_last_wk_pt' => $temp_last_wk_pt, 'top_player_by_gwk_point' => $top_player_by_gwk_point ); 
		






		}


// dump($data); die; 
		// dump($top_player_by_gwk_point); die; 

		// GW - GWP - GWR - Trades Made - Overall Pts - Overall Rank - Team Value - More

		// return  View::make('front.common.history', compact('result', 'position', 'players','position','svalue','user_teams','chosen_player','capton_id','vice_capton_id', 'my_team_name', 'club_name', 'userteam_fantasy_points', 'club_data', 'no_of_trade', 'gameweek_fantasy_points', 'user_teams_data', 'lockout_club', 'twelve_player_select', 'remain_lock_out_time', 'team_ranks_arr', 'tot_club_users', 'temp_points_arr', 'lockout_time_remaining_seconds', 'booster_active', 'availability', 'club_points', 'top_trade_in', 'top_trade_out', 'cur_gameweek_no', 'top_player_by_gwk_point')); 

		return response()->json($data); 
		// return  View::make('front.common.history', compact('top_player_by_gwk_point')); 





	}



	public function tradeHistory(Request $request){

	$club_id = \Session::get('sess_club_name');

	$top_player_by_gwk_point = array(); 
	$user_teams = UserTeams::where('club_id', $club_id)->where('user_id', Auth::user()->id)->first(); 

		if(!empty($club_id) && !empty($user_teams)){


			$temp_club_data = User::where('id', $club_id)->first(); 
			
			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 

			$i = 1; 
			$gw_trade_in = array(); 
			$gw_trade_out = array(); 
			while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->endOfWeek()) {

				$temp_ptio = PlayerTradeInOut::with('player')->where('team_id', $user_teams->id)->where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->orderBy('id', 'DESC')->get(); 
				// dump($temp_ptio); 


				foreach ($temp_ptio as $key => $value) {
					if($value->in_out == 0){
						$gw_trade_out[$i][] = $value; 
					}elseif($value->in_out == 1){
						$gw_trade_in[$i][] = $value; 
					}
				}


				$temp_lockout_start_date->addWeek(); 
				$i++; 
			}

			// dump($gw_trade_in); 
			// dump(array_combine( array_reverse(array_keys( $gw_trade_in )), array_reverse( array_values( $gw_trade_in ) ) )); 

			// dump($gw_trade_out); 
			// die; 

			$gw_trade_in = array_combine( array_reverse(array_keys( $gw_trade_in )), array_reverse( array_values( $gw_trade_in ) ) ); 
			$gw_trade_out = array_combine( array_reverse(array_keys( $gw_trade_out )), array_reverse( array_values( $gw_trade_out ) ) ); 



		}

			return  View::make('front.common.trade_history', compact('gw_trade_in', 'gw_trade_out')); 


	}



	public function winner(Request $request){


		 // dump(Carbon::now()->startOfYear()->addMonth(0)->endOfMonth());  

		 // dump(Carbon::now()->month);

		// dump(Input::all()); die; 

	$club_id = \Session::get('sess_club_name');

	$year =  Carbon::now()->year ; 

		if(!empty($club_id)){
			$club_data = User::where('id', $club_id)->first(); 
			if(!empty($club_data->lockout_start_date)){
				$cur_gameweek_no = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now());
				// $cur_gameweek_no++;
				// $cur_gameweek_no--; 
			}else{
				$cur_gameweek_no = 0; 
			}
		}else{
			$cur_gameweek_no = 0; 
		}

		$most_last_gw = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now()) + 1;  // +1 for most last gameweek

		 // dump(Input::get('gw_no')); 
		 // dump(Input::get('month_no')); 

		 $gw_no = !empty(Input::get('gw_no')) && is_numeric(Input::get('gw_no')) &&  Input::get('gw_no') >= 0 ? Input::get('gw_no') : $cur_gameweek_no; 
		 $month_no = !empty(Input::get('month_no')) ? Input::get('month_no') : Carbon::now()->month-1; 

		 // dump($gw_no); 
		 // dump($month_no);

		 // dump($cur_gameweek_no); 
		 // dump($gw_no); 


	// end ***************************************** end 

	

		return  View::make('front.common.winner', compact('gw_no', 'month_no',  'most_last_gw', 'year')); 

	}



	public function getWinnerData(Request $request){



	parse_str($request->form_data, $res); 
	// print_r($res); die; 

	// 	print_r(Input::all()); die;

	$club_id = \Session::get('sess_club_name');

$club_data = User::where('id', $club_id)->first(); 
/*
Array
(
    [_token] => V9Dg0FOqOSlvZw2Uxxi12cEX5fftV5ZKzb4ZTikB
    [gw_no] => 19
    [example_length] => 10
    [month_no] => 1
    [year] => 2020
    [example1_length] => 10
)

*/ 



		$most_last_gw = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now()); 

		 // dump(Input::get('gw_no')); 
		 // dump(Input::get('month_no')); 

		 $gw_no = $res['gw_no']; // !empty($res['gw_no']) && is_numeric($res['gw_no']) &&  $res['gw_no'] >= 0 ? $res['gw_no'] : $cur_gameweek_no; 
		 // echo $res['month_no']; 
		 $month_no =  $res['month_no']  ; // !empty($res['month_no']) ? $res['month_no'] : Carbon::now()->month-1; 
		 $year =  !empty($res['year']) ? $res['year'] : Carbon::now()->year; 

		 // echo $gw_no; die; 
		 // echo $month_no; 
		 // echo $year; print_r(Carbon::now()->setYear($year)->addMonth($month_no)); die; 
		 // dump($gw_no); 
		 // dump($month_no);

		 // dump($cur_gameweek_no); 
		 // dump($gw_no); 


	// end ***************************************** end 

	$top_player_by_gwk_point = array(); 
	$user_teams = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 

	// dump($user_teams); die; 
// $user_teams = null; 
		if(!empty($club_id) && !empty($user_teams)){



			$temp_club_data = User::where('id', $club_id)->first(); 
			
		if($request->select_name == 'all' || $request->select_name == 'gw_no'){

			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 
			$temp_lockout_start_date = $temp_lockout_start_date->addWeek($gw_no); 
			// print_r($temp_lockout_start_date);  
		}

		if($request->select_name == 'all' || $request->select_name == 'month_no' || $request->select_name == 'year'){

			// dump($temp_lockout_start_date); 
			
			// $temp_lockout_start_date_month = Carbon::now()->startOfYear()->addMonth($month_no); 
				$temp_lockout_start_date_month = Carbon::now()->setYear($year)->startOfYear()->addMonth($month_no); 
				// print_r($temp_lockout_start_date_month);  

		}
			

		// print_r($temp_lockout_start_date); die; 
		// print_r($temp_lockout_start_date_month); die; 


			// dump($temp_lockout_start_date); 

			// dump(Carbon::parse($temp_lockout_start_date_month)->endOfMonth()); 

			$temp_gw_data = array(); 
			$temp_gw_data_month = array();



			if($request->select_name == 'all' || $request->select_name == 'gw_no'){

			
// UserTeamsGWExtraPTTrack::whereIn('team_id', $user_teams)->where('gw_end_date', $temp_lockout_start_date->copy()->endOfWeek())
// print_r($user_teams); die; 
	// print_r( $temp_lockout_start_date->copy()->endOfWeek()->endOfDay()); 
$temp = UserTeamsGWExtraPTTrack::whereIn('team_id', $user_teams)->where('gw_end_date' , $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->where('gw_pt', '>', 0)->orderBy('gw_pt', 'DESC')->orderBy('id', 'ASC')->pluck('gw_pt', 'team_id')->toArray(); 

// print_r($temp); 

foreach ($temp as $tk => $tv) {
	 $temp_gw_data[$tk]['team_point'] = round($tv) ; // $gameweek_team_value->tot_team_point ; 
}
 // $temp_gw_data[$value]['team_point'] = $tot_sum_fp ; // $gameweek_team_value->tot_team_point ; 


// print_r($temp_gw_data); 


			}


			if($request->select_name == 'all' || $request->select_name == 'month_no' || $request->select_name == 'year'){
// print_r($temp_lockout_start_date_month->copy()->subWeeks(2)->endOfMonth()->endOfDay()); die; 
			$temp = UserTeamsMonthPTTrack::whereIn('team_id', $user_teams)->where('month_end_date' , $temp_lockout_start_date_month->copy()->endOfMonth()->endOfDay())->where('month_pt', '>', 0)->orderBy('month_pt', 'DESC')->orderBy('id', 'ASC')->pluck('month_pt', 'team_id')->toArray(); 

foreach ($temp as $tk => $tv) {
	 $temp_gw_data_month[$tk]['team_point'] = round($tv) ; // $gameweek_team_value->tot_team_point ; 
}


 			// $temp_gw_data_month[$value]['team_point'] = $tot_sum_fp ; // $gameweek_team_value->tot_team_point ; 





			}


	

				// dump($temp_gw_data_month); 
			if($request->select_name == 'all' || $request->select_name == 'gw_no'){
				// dump($temp_gw_data); 
				// print_r($temp_gw_data); 
				arsort($temp_gw_data);
				// print_r($temp_gw_data);  die; 
				$i = 1; 
				foreach ($temp_gw_data as $key => $value) {
					$tttmp =  UserTeams::with('userdata')->find($key);
					$temp_gw_data[$key]['rank'] = $i++; 
					$temp_gw_data[$key]['manager'] = !empty($tttmp->userdata->full_name) ? $tttmp->userdata->full_name : 'Anonymous User' ; 
					$temp_gw_data[$key]['team_name'] = !empty($tttmp->userdata->my_team_name) ? $tttmp->userdata->my_team_name :  (!empty($tttmp->my_team_name) ? $tttmp->my_team_name : 'Anonymous'); 



				}
				$i++; 

				$gw_winner_data = $temp_gw_data; 
			}else{
				$gw_winner_data = array(); 
			}


			if($request->select_name == 'all' || $request->select_name == 'month_no' || $request->select_name == 'year'){

				arsort($temp_gw_data_month); 
				$i = 1; 
				foreach ($temp_gw_data_month as $key => $value) {
					$tttmp = UserTeams::with('userdata')->find($key); 
					$temp_gw_data_month[$key]['rank'] = $i++; 
					$temp_gw_data_month[$key]['manager'] =  !empty($tttmp->userdata->full_name) ? $tttmp->userdata->full_name : 'Anonymous User' ; 
					$temp_gw_data_month[$key]['team_name'] =  !empty($tttmp->userdata->my_team_name) ? $tttmp->userdata->my_team_name :  (!empty($tttmp->my_team_name) ? $tttmp->my_team_name : 'Anonymous'); 
				}
				$i++; 

				$gw_winner_data_month = $temp_gw_data_month; 

			}else{
				$gw_winner_data_month = array(); 
			}


			// print_r($gw_winner_data); die; 



		}

	    $data = array('select_name' => $request->select_name, 'gw_winner_data' => $gw_winner_data, 'gw_winner_data_month' => $gw_winner_data_month); 


	    // print_r($data); die; 
		return response()->json($data); 
		// return  View::make('front.common.winner', compact('gw_no', 'month_no', 'gw_winner_data', 'gw_winner_data_month', 'most_last_gw')); 



	    // $data = array('select_name' => $request->select_name, 'gw_winner_data' => $gw_winner_data, 'gw_winner_data_month' => $gw_winner_data_month); 


	    // print_r($data); die; 
		// return response()->json($data); 


/* commented on 07/02/20 working code (it calculate points dynamically)

		if(!empty($club_id)){
			$club_data = User::where('id', $club_id)->first(); 
			if(!empty($club_data->lockout_start_date)){
				$cur_gameweek_no = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now());
				// $cur_gameweek_no++;
				$cur_gameweek_no--; 
			}else{
				$cur_gameweek_no = 0; 
			}
		}else{
			$cur_gameweek_no = 0; 
		}

		$most_last_gw = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now()); 

		 // dump(Input::get('gw_no')); 
		 // dump(Input::get('month_no')); 

		 $gw_no = $res['gw_no']; // !empty($res['gw_no']) && is_numeric($res['gw_no']) &&  $res['gw_no'] >= 0 ? $res['gw_no'] : $cur_gameweek_no; 
		 $month_no = $res['month_no'] ; // !empty($res['month_no']) ? $res['month_no'] : Carbon::now()->month-1; 
		 $year =  !empty($res['year']) ? $res['year'] : Carbon::now()->year; 

		 // echo $gw_no; die; 
		 // echo $month_no; 
		 // echo $year; print_r(Carbon::now()->setYear($year)->addMonth($month_no)); die; 
		 // dump($gw_no); 
		 // dump($month_no);

		 // dump($cur_gameweek_no); 
		 // dump($gw_no); 


	// end ***************************************** end 

	$top_player_by_gwk_point = array(); 
	$user_teams = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 

	// dump($user_teams); die; 
// $user_teams = null; 
		if(!empty($club_id) && !empty($user_teams)){



			$temp_club_data = User::where('id', $club_id)->first(); 
			
		if($request->select_name == 'all' || $request->select_name == 'gw_no'){

			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 
			$temp_lockout_start_date = $temp_lockout_start_date->addWeek($gw_no); 
			// print_r($temp_lockout_start_date);  
		}

		if($request->select_name == 'all' || $request->select_name == 'month_no' || $request->select_name == 'year'){

			// dump($temp_lockout_start_date); 
			
			// $temp_lockout_start_date_month = Carbon::now()->startOfYear()->addMonth($month_no); 
				$temp_lockout_start_date_month = Carbon::now()->setYear($year)->addMonth($month_no); 
				// print_r($temp_lockout_start_date_month); die; 

		}
			

			// dump($temp_lockout_start_date); 

			// dump(Carbon::parse($temp_lockout_start_date_month)->endOfMonth()); 

			$temp_gw_data = array(); 
			$temp_gw_data_month = array();

			foreach ($user_teams as $key => $value) {


			if($request->select_name == 'all' || $request->select_name == 'gw_no'){

			
				// print_r($temp_lockout_start_date->copy()->startOfWeek()); die; 
				// gameweek calculation 

				$gameweek_team_value =  UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->whereDate('created_at', '>=', $temp_lockout_start_date->copy()->startOfWeek())->whereDate('created_at', '<=', $temp_lockout_start_date->copy()->endOfWeek())->orderBy('id', 'DESC')->first(); 

				

				if(empty($gameweek_team_value)){  // if gameweek is empty search previous old record
					$gameweek_team_value = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->where('created_at', '<=', $temp_lockout_start_date)->orderBy('id', 'DESC')->first(); 

					// $gameweek_team_value = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->whereBetween('created_at', [$temp_lockout_start_date->copy()->subWeek()->startOfWeek(), $temp_lockout_start_date->copy()->subWeek()->endOfWeek()])->orderBy('id', 'DESC')->first(); 


				}
				if(empty($gameweek_team_value)){  // if empty search next  future record
					$gameweek_team_value = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->where('created_at', '>=', $temp_lockout_start_date)->orderBy('id', 'ASC')->first(); 

					// $gameweek_team_value = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->whereBetween('created_at', [$temp_lockout_start_date->copy()->addWeek()->startOfWeek(), $temp_lockout_start_date->copy()->addWeek()->endOfWeek()])->orderBy('id', 'ASC')->first(); 


				}	

				
				// if(!empty($gameweek_team_value))
				// 	$temp_gw_data[$value]['team_point'] = $gameweek_team_value->tot_team_point ; 


				if(!empty($gameweek_team_value)){   // calculate the gameweek team value for each team 
					// $temp_gw_data[$value]['team_point'] = $gameweek_team_value->tot_team_point ; 
			// echo 'hello'; die; 

		$temp_utp = unserialize($gameweek_team_value->team_player); 
// 		echo '##'.$value.'##'; 
// print_r($temp_utp); 
				$tot_sum_fp = 0; 
				$top_sum_rs = 0; 
				// dump($temp_lockout_start_date); 
				foreach ($temp_utp as $key3 => $value3) {
					// $sum_fp = FixtureScorcard::where('player_id', $value3)
					// 				->where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->sum('fantasy_points'); 


$sum_fp = FixtureScorcard::where('player_id', $value3)

                        ->whereHas('fixture', function($q) use ($temp_lockout_start_date){
   							$q->where('start_date', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('start_date', '<=', $temp_lockout_start_date->copy()->endOfWeek()); 


                                 })

                        // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())

                        ->sum('fantasy_points'); 




					// $sum_rs = FixtureScorcard::where('player_id', $value3)
					// 				->where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->sum('rs'); 
									// ->groupBy('player_id')
									//  ->selectRaw('sum(fantasy_points) as sum, sum(rs) as sum_rs, player_id')->get(); 
								   // ->selectRaw('sum(fantasy_points) as sum, sum(rs) as sum_rs, sum(fours) as sum_fours, sum(sixes) as sum_sixes, sum(wks) as sum_wks, sum(mdns) as sum_mdns, sum(cs) as sum_cs, sum(hattrick) as sum_hattrick, player_id')->get();  // ->orderBy('sum', 'DESC')
								   // ->pluck('sum','player_id')->toArray();
					if($value3 == $gameweek_team_value->capton_id){
						$sum_fp = $sum_fp * 2; 

					}
					if($value3 == $gameweek_team_value->v_capton_id){
						$sum_fp = round($sum_fp * 1.5);
					}
					$tot_sum_fp += $sum_fp; 
					// $top_sum_rs += $sum_rs; 
// echo $sum_fp.'**'; 					
				}

                if(UserTeamsGWExtraPTTrack::where(['team_id' => $value, 'gw_end_date' => $temp_lockout_start_date->copy()->endOfWeek()->endOfDay() ])->exists()){

                	$tmp = UserTeamsGWExtraPTTrack::where('team_id', $value)->where('gw_end_date', $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->first(); 
			// dump($tmp); 
// echo 'hi'; die; 
                		$tot_sum_fp += round($tmp->gw_extra_pt)  ; 



                }



 $temp_gw_data[$value]['team_point'] = $tot_sum_fp ; // $gameweek_team_value->tot_team_point ; 


				}




			}


			if($request->select_name == 'all' || $request->select_name == 'month_no' || $request->select_name == 'year'){

			

				// monthly calculation 

				// print_r($temp_lockout_start_date_month->copy()->startOfMonth()); die; 


				$gameweek_team_value_month =  UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->whereDate('created_at', '>=', $temp_lockout_start_date_month->copy()->startOfMonth())->whereDate('created_at', '<=', $temp_lockout_start_date_month->copy()->endOfMonth())->orderBy('id', 'DESC')->first(); 

				

				if(empty($gameweek_team_value_month)){  // if gameweek is empty search previous old record
					$gameweek_team_value_month = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->where('created_at', '<=', $temp_lockout_start_date_month)->orderBy('id', 'DESC')->first(); 

					// $gameweek_team_value_month = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->whereBetween('created_at', [$temp_lockout_start_date_month->copy()->subMonth()->startOfMonth(), $temp_lockout_start_date_month->copy()->subMonth()->endOfMonth()])->orderBy('id', 'DESC')->first(); 


				}
				if(empty($gameweek_team_value_month)){  // if empty search next  future record
					$gameweek_team_value_month = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->where('created_at', '>=', $temp_lockout_start_date_month)->orderBy('id', 'ASC')->first(); 

					// $gameweek_team_value_month = UserTeamValueLog::where('team_id', $value)->whereNotNull('team_player')->whereBetween('created_at', [$temp_lockout_start_date_month->copy()->addMonth()->startOfMonth(), $temp_lockout_start_date_month->copy()->addMonth()->endOfMonth()])->orderBy('id', 'ASC')->first(); 


				}	


				

				if(!empty($gameweek_team_value_month)){
					// $temp_gw_data_month[$value]['team_point'] = $gameweek_team_value_month->tot_team_point ; 

// echo 'hi'; die; 


		$temp_utp = unserialize($gameweek_team_value_month->team_player); 
// print_r($temp_utp); 
				$tot_sum_fp = 0; 
				$top_sum_rs = 0; 
				// dump($temp_lockout_start_date); 
				foreach ($temp_utp as $key3 => $value3) {
					// $sum_fp = FixtureScorcard::where('player_id', $value3)
					// 				->where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->sum('fantasy_points'); 


$sum_fp = FixtureScorcard::where('player_id', $value3)

                        ->whereHas('fixture', function($q) use ($temp_lockout_start_date_month){
   							$q->where('start_date', '>=', $temp_lockout_start_date_month->copy()->startOfMonth())->where('start_date', '<=', $temp_lockout_start_date_month->copy()->endOfMonth()); 


                                 })

                        // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())

                        ->sum('fantasy_points'); 




					// $sum_rs = FixtureScorcard::where('player_id', $value3)
					// 				->where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->sum('rs'); 
									// ->groupBy('player_id')
									//  ->selectRaw('sum(fantasy_points) as sum, sum(rs) as sum_rs, player_id')->get(); 
								   // ->selectRaw('sum(fantasy_points) as sum, sum(rs) as sum_rs, sum(fours) as sum_fours, sum(sixes) as sum_sixes, sum(wks) as sum_wks, sum(mdns) as sum_mdns, sum(cs) as sum_cs, sum(hattrick) as sum_hattrick, player_id')->get();  // ->orderBy('sum', 'DESC')
								   // ->pluck('sum','player_id')->toArray();
					if($value3 == $gameweek_team_value_month->capton_id){
						$sum_fp = $sum_fp * 2; 

					}
					if($value3 == $gameweek_team_value_month->v_capton_id){
						$sum_fp = round($sum_fp * 1.5);
					}
					$tot_sum_fp += $sum_fp; 
					// $top_sum_rs += $sum_rs; 
				}

				/* 

                if(UserTeamsGWExtraPTTrack::where(['team_id' => $user_teams->id, 'gw_end_date' => $temp_lockout_start_date->copy()->endOfWeek()->endOfDay() ])->exists()){

                	$tmp = UserTeamsGWExtraPTTrack::where('team_id', $user_teams->id)->where('gw_end_date', $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->first(); 
			// dump($tmp); 
// echo 'hi'; die; 
                		$tot_sum_fp += round($tmp->gw_extra_pt)  ; 



                }

                */ 

/*


 $temp_gw_data_month[$value]['team_point'] = $tot_sum_fp ; // $gameweek_team_value->tot_team_point ; 








				}



			}


			}


				// dump($temp_gw_data_month); 
			if($request->select_name == 'all' || $request->select_name == 'gw_no'){
				// dump($temp_gw_data); 
				// print_r($temp_gw_data); 
				arsort($temp_gw_data);
				// print_r($temp_gw_data);  die; 
				$i = 1; 
				foreach ($temp_gw_data as $key => $value) {
					$tttmp =  UserTeams::with('userdata')->find($key);
					$temp_gw_data[$key]['rank'] = $i++; 
					$temp_gw_data[$key]['manager'] = !empty($tttmp->userdata->full_name) ? $tttmp->userdata->full_name : '' ; 
					$temp_gw_data[$key]['team_name'] = !empty($tttmp->userdata->my_team_name) ? $tttmp->userdata->my_team_name :  $tttmp->my_team_name; 


				}
				$i++; 

				$gw_winner_data = $temp_gw_data; 
			}else{
				$gw_winner_data = array(); 
			}


			if($request->select_name == 'all' || $request->select_name == 'month_no' || $request->select_name == 'year'){

				arsort($temp_gw_data_month); 
				$i = 1; 
				foreach ($temp_gw_data_month as $key => $value) {
					$tttmp = UserTeams::with('userdata')->find($key); 
					$temp_gw_data_month[$key]['rank'] = $i++; 
					$temp_gw_data_month[$key]['manager'] =  !empty($tttmp->userdata->full_name) ? $tttmp->userdata->full_name : '' ; 
					$temp_gw_data_month[$key]['team_name'] =  !empty($tttmp->userdata->my_team_name) ? $tttmp->userdata->my_team_name :  $tttmp->my_team_name; 
				}
				$i++; 

				$gw_winner_data_month = $temp_gw_data_month; 

			}else{
				$gw_winner_data_month = array(); 
			}


			// print_r($gw_winner_data); die; 

		/*			
			$i = 1; 
			while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->endOfWeek()) {






				$temp_lockout_start_date->addWeek(); 

			}


		*/
/*

		}

	    $data = array('select_name' => $request->select_name, 'gw_winner_data' => $gw_winner_data, 'gw_winner_data_month' => $gw_winner_data_month); 


	    // print_r($data); die; 
		return response()->json($data); 
		// return  View::make('front.common.winner', compact('gw_no', 'month_no', 'gw_winner_data', 'gw_winner_data_month', 'most_last_gw')); 


*/ 
// code comment end on 07/02/20



	}


	public function teamRank(Request $request){ 

	

		$club_id = \Session::get('sess_club_name');
			$item_per_page = 10; 
		if(!empty($club_id)){


			// update all teams total point  start ********************************* uncomment this and run if you want to update the recent totoal points

/* 
			$temp_all_ut_id_arr = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 

			foreach ($temp_all_ut_id_arr as $key => $value) {






                $user_terms = UserTeams::find($value); 


                $user_team_player = UserTeamPlayers::where('team_id', $user_terms->id)->get(); 

                // dump($value->id); 
                // dump($user_team_player); 
                // dump($value->capton_id); 
                // dump($user_team_player); die; 
                $tmp_top_pnt = 0 ; 
                foreach ($user_team_player as $k1 => $v1) {



                    $vtemp = UserTeamsPlayerTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->whereNull('remove_date')->first(); 

                    if(!empty($vtemp)){
                        $v1 = $vtemp ; 
                    }else{
                        $utp_vtemp = UserTeamPlayers::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->first(); 
                        if(!empty($utp_vtemp)){
                            $v1 = $utp_vtemp; 
                        }

                    }







                // echo 'hi'; 
                // dump($v1); die; 

                // $tmp_top_pnt += $tmp_pnt ; 

                    // dump($v1->player_id); 
                    if($v1->player_id == $user_terms->capton_id){

                        $utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 1)->whereNull('remove_date')->first(); 
                        if(!empty($utcvct)){
                            $tmp_strt_dt = $utcvct->created_at ; 
                        }else{
                            // echo 'hi1'; die; 
                            $tmp_strt_dt = $v1->created_at ; 
                        }
                        // echo 'hi'; 
                        // dump(Carbon::parse($tmp_strt_dt)->endOfDay()); 

                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)


					// if 12th man then change the date accordingly

					if($v1->player_id == $user_terms->twelveth_man_id && !empty($user_terms->twelve_man_card_last_date)){
						$tmp_strt_dt = $user_terms->twelve_man_card_last_date ; 
					}



                    $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

                            ->whereHas('fixture', function($q) use ($tmp_strt_dt){
                                                $q->where('start_date', '>', Carbon::parse($tmp_strt_dt));
                                            })

                    // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

                    ->sum('fantasy_points'); // ->where('status', 3)







                        // dump($tmp_pnt); 
                        // echo 'hello'; 
                        $tmp_top_pnt += round($tmp_pnt) ; 
                    }

                    if($v1->player_id == $user_terms->vice_capton_id){


                        $utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
                        if(!empty($utcvct)){
                            $tmp_strt_dt = $utcvct->created_at ; 
                        }else{
                            // echo 'hi2'; die; 
                            $tmp_strt_dt = $v1->created_at ; 
                        }




					// if 12th man then change the date accordingly
					if($v1->player_id == $user_terms->twelveth_man_id && !empty($user_terms->twelve_man_card_last_date)){
						$tmp_strt_dt = $user_terms->twelve_man_card_last_date ; 
					}



                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)

                    $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

                        ->whereHas('fixture', function($q) use ($tmp_strt_dt){
                                                                        $q->where('start_date', '>', Carbon::parse($tmp_strt_dt));
                                                                    })
                    // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

                    ->sum('fantasy_points'); // ->where('status', 3)
















                        $tmp_top_pnt += round($tmp_pnt * 0.5) ; 
                    }


                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)




					$tmp_strt_dt = $v1->created_at; 

					// if 12th man then change the date accordingly
					if($v1->player_id == $user_terms->twelveth_man_id && !empty($user_terms->twelve_man_card_last_date)){
						$tmp_strt_dt = $user_terms->twelve_man_card_last_date ; 
					}




                    $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

                        ->whereHas('fixture', function($q) use ($tmp_strt_dt){
                                    $q->where('start_date', '>', Carbon::parse($tmp_strt_dt));
                                })
                    // ->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())

                    ->sum('fantasy_points'); // ->where('status', 3)


                    // print_r($v1->player_id); 
                    // print_r($v1->created_at); 
                    // print_r($tmp_pnt); 




                        $tmp_top_pnt += $tmp_pnt ; 



                    // dump($tmp_top_pnt); 



                    }


                    $user_terms->total_team_point = round($user_terms->extra_team_point + $tmp_top_pnt + $user_terms->three_cap_point) - $user_terms->dealer_card_minus_points;  // save extra team point + player 11 point + captain cap booster extra point - dealer card booster negative point

                    $user_terms->save(); 





				
			}


*/ 

/*
 // code to save old data of teams 

	$all_team = UserTeams::pluck('id')->toArray(); 
	// dump($all_team); die; 
	foreach ($all_team as $key => $value) {
		$temp = UserTeamValueLog::where('team_id', $value)->where('created_at', '<=', Carbon::now()->subWeek())->orderBy('id', 'DESC')->select('tot_team_point', 'team_id')->first(); 
		if(!empty($temp)){

			// echo $temp->team_id.' => '.$temp->tot_team_point.'<br>'; 
			$tmp_team = UserTeams::find($value); 

			echo $temp->team_id.' => '.$temp->tot_team_point.'<br>'; 
			// dump($tmp_team); 
			if($temp->tot_team_point > $tmp_team->total_team_point	){
				// $tmp_team->extra_team_point += ($temp->tot_team_point - $tmp_team->total_team_point) ; 
				$tmp_team->total_team_point = $temp->tot_team_point; 
				$tmp_team->save(); 
			}

			// total_team_point

		}

	}
	echo 'hello'; 

	die; 
*/ 


			// update all teams total point end ***************************



		// $result = UserTeams::leftJoin('users','users.id','=','user_teams.user_id')->where('club_id', $club_id)
		// 			//->where('user_teams.club_id', $club_id)
		// 			//->leftJoin('')
		// 			->with('getTeamPlayer')
		// 			->select('user_teams.*','users.first_name','users.club_logo')
		// 			->get();
	 /*  // commented on 14-02-2020 as not required because all data will be fetechec in single go
			$skip_rec = !empty(Input::get('page')) ? Input::get('page') * $item_per_page : 0; 
			
			// $skip_rec = 2; 

		$user_teams = UserTeams::where('club_id', $club_id)->orderBy('total_team_point', 'DESC')->orderBy('id', 'ASC')->skip($skip_rec)->take($item_per_page)->get(); 
		// $user_teams = UserTeams::where('club_id', $club_id)->paginate(1); 

		// dump($user_teams); die; 
// $result = $user_teams; 


		$temp = array(); 
		$temp_last_wk_pt = array(); 
		foreach ($user_teams as $key => $value) {


		

		// dump($value); die; 
			/*   // commented as now the rank is not calculated on the fly
			$user_team_player = UserTeamPlayers::where('team_id', $value->id)->get(); 
			$tmp_top_pnt = 0 ; 
			foreach ($user_team_player as $k1 => $v1) {



					$vtemp = UserTeamsPlayerTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->whereNull('remove_date')->first(); 

					if(!empty($vtemp)){
						$v1 = $vtemp ; 
					}else{
						$utp_vtemp = UserTeamPlayers::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->first(); 
						if(!empty($utp_vtemp)){
							$v1 = $utp_vtemp; 
						}

					}







				// echo 'hi'; 
				// dump($v1); die; 

				// $tmp_top_pnt += $tmp_pnt ; 

					// dump($v1->player_id); 
					if($v1->player_id == $value->capton_id){

						$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 1)->whereNull('remove_date')->first(); 
						if(!empty($utcvct)){
							$tmp_strt_dt = $utcvct->created_at ; 
						}else{
							// echo 'hi1'; die; 
							$tmp_strt_dt = $v1->created_at ; 
						}
						// echo 'hi'; 
						// dump(Carbon::parse($tmp_strt_dt)->endOfDay()); 

						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)



					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

							->whereHas('fixture', function($q) use ($tmp_strt_dt){
												$q->where('start_date', '>', Carbon::parse($tmp_strt_dt)->endOfDay());
											})

					// ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)







						// dump($tmp_pnt); 
						// echo 'hello'; 
						$tmp_top_pnt += round($tmp_pnt) ; 
					}

					if($v1->player_id == $value->vice_capton_id){


						$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
						if(!empty($utcvct)){
							$tmp_strt_dt = $utcvct->created_at ; 
						}else{
							// echo 'hi2'; die; 
							$tmp_strt_dt = $v1->created_at ; 
						}




						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)

					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

						->whereHas('fixture', function($q) use ($tmp_strt_dt){
																		$q->where('start_date', '>', Carbon::parse($tmp_strt_dt)->endOfDay());
																	})
					// ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)
















						$tmp_top_pnt += round($tmp_pnt * 0.5) ; 
					}


						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)


					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

						->whereHas('fixture', function($q) use ($v1){
									$q->where('start_date', '>', Carbon::parse($v1->created_at)->endOfDay());
								})
					// ->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)







						$tmp_top_pnt += $tmp_pnt ; 



					// dump($tmp_top_pnt); 




			}


			$tmp_top_pnt += $value->extra_team_point ; 

			$temp[$value->id] = $tmp_top_pnt; 


			*/ 

		 /*  // commented on 14-02-2020 as not required because all data will be fetechec in single go
			$user_teams_data = $value ; 
			$user_team_player = UserTeamPlayers::where('team_id', $value->id)->get(); 

			$tmp_top_pnt = 0 ; 
			foreach ($user_team_player as $k1 => $v1) {




				$vtemp = UserTeamsPlayerTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->whereNull('remove_date')->first(); 

				if(!empty($vtemp)){
					$v1 = $vtemp ; 
				}else{
					$utp_vtemp = UserTeamPlayers::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->first(); 
					if(!empty($utp_vtemp)){
						$v1 = $utp_vtemp; 
					}

				}


                    // dump($v1->player_id); 
				if($v1->player_id == $user_teams_data->capton_id){

					$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 1)->whereNull('remove_date')->first(); 
					if(!empty($utcvct)){
						$tmp_strt_dt = $utcvct->created_at ; 
					}else{
                            // echo 'hi1'; die; 
						$tmp_strt_dt = $v1->created_at ; 
					}
                        // echo 'hi'; 
                        // dump(Carbon::parse($tmp_strt_dt)->endOfDay()); 

                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)



                        $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

                        ->whereHas('fixture', function($q) use ($tmp_strt_dt){
   							$q->where('start_date', '>=', Carbon::parse($tmp_strt_dt))->where('start_date', '>=', Carbon::now()->subWeek()->startOfWeek())->where('start_date', '<=', Carbon::now()->subWeek()->endOfWeek()); 


                                 })

                        // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())

                        ->sum('fantasy_points'); // ->where('status', 3)



    // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)
    //                    ->whereHas('fixture', function($q) use ($tmp_strt_dt){
    //                                  $q->where('start_date', '>', Carbon::parse($tmp_strt_dt)->endOfDay());
    //                              })
    // ->sum('fantasy_points'); // ->where('status', 3)








                        // dump($tmp_pnt); 
                        // echo 'hello'; 
                        $tmp_top_pnt += round($tmp_pnt) ; 
                    }


                    if($v1->player_id == $user_teams_data->vice_capton_id){


                    	$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
                    	if(!empty($utcvct)){
                    		$tmp_strt_dt = $utcvct->created_at ; 
                    	}else{
                            // echo 'hi2'; die; 
                    		$tmp_strt_dt = $v1->created_at ; 
                    	}




                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)


    	             $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

    	             ->whereHas('fixture', function($q) use ($tmp_strt_dt){
							$q->where('start_date', '>=', Carbon::parse($tmp_strt_dt))->where('start_date', '>=', Carbon::now()->subWeek()->startOfWeek())->where('start_date', '<=', Carbon::now()->subWeek()->endOfWeek()); 

                                 })

    	             // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())


    	             ->sum('fantasy_points'); // ->where('status', 3)



 











                        $tmp_top_pnt += round($tmp_pnt * 0.5) ; 
                    }



                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)



                        $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)


					->whereHas('fixture', function($q) use ($v1){

						$q->where('start_date', '>=', Carbon::parse($v1->created_at))->where('start_date', '>=', Carbon::now()->subWeek()->startOfWeek())->where('start_date', '<=', Carbon::now()->subWeek()->endOfWeek()); 


					                                 })


                        // ->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())


                        ->sum('fantasy_points'); // ->where('status', 3)



                        $tmp_top_pnt += $tmp_pnt ; 





			}



                    $gw_extra_pt = 0 ; 

                    if(UserTeamsGWExtraPTTrack::where(['team_id' => $user_teams_data->id, 'gw_end_date' => Carbon::now()->subWeek()->endOfWeek()->endOfDay() ])->exists()){

                    	$tmp = UserTeamsGWExtraPTTrack::where('team_id', $user_teams_data->id)->where('gw_end_date', Carbon::now()->subWeek()->endOfWeek()->endOfDay())->first(); 
				// dump($tmp); 

                    		$gw_extra_pt = round($tmp->gw_extra_pt)  ; 




                    }



                    if(UserTeamLogs::where('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<', Carbon::now()->subWeek()->endOfWeek())->where('team_id', $user_teams_data->id)->where('log_type', 4)->exists()){

                    	$tmp_top_pnt = $tmp_top_pnt-50; // subtract 50 points if dealer card is active in this game week 

                    }                    












			$temp_last_wk_pt[$value->id] = $tmp_top_pnt + $gw_extra_pt; 
			

			
		}


		*/ 

	// die;

		// dump($temp); 
		// dump($temp_last_wk_pt); 
		// die; 

	// $temp = UserTeams::where('club_id', $club_id)->orderBy('total_team_point', 'DESC')->orderBy('id', 'ASC')->skip($skip_rec)->take($item_per_page)->pluck('total_team_point', 'id')->toArray();  // for overall


$club_team_ids = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 
			$temp =  UserTeamsGWExtraPTTrack::select(DB::raw('max(id) as gw_id, team_id'))  // get the id of most last gameweek table record 
			                     ->whereIn('team_id', $club_team_ids)
			                     ->groupBy('team_id')
			                     ->pluck('gw_id', 'team_id')->toArray();




			$temp_gw =  UserTeamsGWExtraPTTrack::select(DB::raw('max(id) as gw_id, team_id'))  // get the id of most last gameweek table record 
			                     ->whereIn('team_id', $club_team_ids)
			                     ->where('gw_end_date', Carbon::now()->subWeek()->endOfWeek())
			                     ->groupBy('team_id')
			                     ->pluck('gw_id', 'team_id')->toArray();

			$temp_last_wk_pt = UserTeamsGWExtraPTTrack::whereIn('id', $temp_gw)->pluck('gw_pt', 'team_id')->toArray();  // get the over all point 

			$temp = UserTeamsGWExtraPTTrack::whereIn('id', $temp)->pluck('overall_pt', 'team_id')->toArray();  // get the over all point 

				// dump($temp); die; 



// dump($user_teams_data); 
// dump(Auth::user());
// 		dump($temp); die; 
				arsort($temp); 


// dump($temp); 
// 		arsort($temp); 
// 	dump($temp); 
		$temp_points_arr = $temp; 
		$temp_points_arr_past_wk = $temp_last_wk_pt; 
		$i = 1; 
		foreach ($temp as $key => $value) {
			$temp[$key] = $i++ ; 
		}
	
		$team_ranks_arr = $temp; 


		$key_team_ranks_arr = array_keys($team_ranks_arr); 
		// $ids = collect([11,10,1]);
		// $order = $list->item_order;
		$temp = array(); 

		foreach ($key_team_ranks_arr as $key => $value) {
			$temp[] = UserTeams::find($value); 
		}
		$user_teams = $temp; 

// dump($temp_points_arr); 

		$temp_points_arr=array_map("round",$temp_points_arr); // remove decimal points 
		$temp_points_arr_past_wk=array_map("round",$temp_points_arr_past_wk); // remove decimal points 

// dump($temp_points_arr); die; 

		// dump($user_teams);  die; 
		// dump($temp); 
		// dump($key_team_ranks_arr); 
		// dump($user_teams);
		// dump($temp_points_arr); 
		// dump($team_ranks_arr); 
		// die; 

		// all rank calculatin  start 
		/*
	$temp = UserTeams::where('club_id', $club_id)->orderBy('total_team_point', 'DESC')->orderBy('id', 'ASC')->pluck('total_team_point', 'id')->toArray();  // for overall

// dump($temp); 
// 		arsort($temp); 
// 		dump($temp); 
		$temp_points_arr = $temp; 
		// $temp_points_arr_past_wk = $temp_last_wk_pt; 
		$i = 1; 
		foreach ($temp as $key => $value) {
			$temp[$key] = $i++ ; 
		}
	
		$team_ranks_arr = $temp; 


		*/ 

// dump($temp); 
// all rank calculatin  end



		}

		// dump($user_teams); die; 
		/*
		else{
		$result = UserTeams::leftJoin('users','users.id','=','user_teams.user_id')
					//->where('user_teams.club_id', $club_id)
					//->leftJoin('')
					->with('getTeamPlayer')
					->select('user_teams.*','users.first_name','users.club_logo')
					->get();

		}

		
		// print_r($result); die; 


		$fantasyPointsData = [];
		if(!$result->isEmpty()){
			foreach ($result as $key => $value) {
				$fantasyPointsData[$key] ['first_name'] = $value->first_name;
				$fantasyPointsData[$key]['team_name'] = $value->my_team_name;
				$fantasyPointsData[$key]['club_logo'] = $value->image;
				$fantasyPointsData[$key]['sum'] = $this->getTeamFantasyPoints($value['getTeamPlayer'],$value->created_at);
			}
		}

		*/
		// dump($user_teams); dump($temp_points_arr); dump($team_ranks_arr); dump($temp_points_arr_past_wk); die; 
		// $result = $this->array_sort($fantasyPointsData, 'sum', SORT_DESC);

		$tot_team_count = UserTeams::where('club_id', $club_id)->where('is_active', 1)->count(); 


		if($request->ajax()){
           return  View::make('front.common.load_more_rank', compact('user_teams', 'temp_points_arr', 'team_ranks_arr', 'temp_points_arr_past_wk', 'tot_team_count', 'item_per_page'));
        }else{
			return  View::make('front.common.rank', compact('user_teams', 'temp_points_arr', 'team_ranks_arr', 'temp_points_arr_past_wk', 'tot_team_count', 'item_per_page'));
        }

		
	}




	public function playerProfile($player_id = null){

		if(empty($player_id)){
			return Redirect::to('lobby');
		}

		$club_id = Session::get('sess_club_name'); 
		$drop_down =	new DropDown();
		$position =	$drop_down->get_master_list("position");
		$player_data = Player::leftJoin('dropdown_managers','dropdown_managers.id','=','players.bat_style')
								->leftJoin('dropdown_managers as dm','dm.id','=','players.bowl_style')
								->leftJoin('teams','teams.id','=','players.team_id')
								->where('players.id', $player_id)
								->select('players.*','dropdown_managers.name as player_bat_style','dm.name as player_bowl_style','teams.name as team_name')
								->first();

		$player_fp = FixtureScorcard::where('player_id',$player_data->id)->sum('fantasy_points');
		$top_player_by_gwk_point = array(); 
		if(!empty($club_id)){
			$temp_club_data = User::where('id', $club_id)->first(); 
			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 
			// dump($temp_lockout_start_date->startOfWeek()); 
			// dump($temp_lockout_start_date->endOfWeek()); 
			// dump($temp_lockout_start_date->addWeek()); 
			// dump($temp_lockout_start_date->startOfWeek()); 
			// dump($temp_lockout_start_date->endOfWeek()); 
			// die; 
			$i = 1; 
			while ($temp_lockout_start_date <= Carbon::now()->endOfWeek()) {
				$temp_var = FixtureScorcard::where('player_id', $player_id)
								// ->where('created_at', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<=', $temp_lockout_start_date->copy()->endOfWeek())

							->whereHas('fixture', function($q) use ($temp_lockout_start_date){
	   							$q->where('start_date', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('start_date', '<=', $temp_lockout_start_date->copy()->endOfWeek()); 
	                                 })

								->groupBy('player_id')
							   ->selectRaw('sum(fantasy_points) as sum, sum(rs) as sum_rs, sum(fours) as sum_fours, sum(sixes) as sum_sixes, sum(wks) as sum_wks, sum(mdns) as sum_mdns, sum(cs) as sum_cs, sum(hattrick) as sum_hattrick, player_id')->get();  // ->orderBy('sum', 'DESC')
							   // ->pluck('sum','player_id')->toArray();



				// dump($temp_var);





				$temp_lockout_start_date->addWeek(); 

				// dump($temp_var); die; 
		
					foreach ($temp_var as $key => $value) {
						// dump($value); 
						$top_player_by_gwk_point[$i]['gwk'] = $i; 
						$top_player_by_gwk_point[$i]['fp'] = $value->sum; 
						$top_player_by_gwk_point[$i]['sum_rs'] = $value->sum_rs; 
						$top_player_by_gwk_point[$i]['sum_fours'] = $value->sum_fours; 
						$top_player_by_gwk_point[$i]['sum_sixes'] = $value->sum_sixes; 
						$top_player_by_gwk_point[$i]['sum_wks'] = $value->sum_wks; 
						$top_player_by_gwk_point[$i]['sum_mdns'] = $value->sum_mdns; 
						$top_player_by_gwk_point[$i]['sum_cs'] = $value->sum_cs; 
						$top_player_by_gwk_point[$i]['sum_hattrick'] = $value->sum_hattrick; 
					}

		

				$i++; 
				 
			}

			// dump($top_player_by_gwk_point); 
			// die; 

			// foreach ($top_player_by_gwk_point as $key => $value) {
			// 	$top_player_by_gwk_point[$key]['player_id']  = Player::where('id', $top_player_by_gwk_point[$key]['player_id'])->first(); 
			// }

			// dump($top_player_by_gwk_point); 
			// die; 


		}
		$total_team = UserTeams::where('club_id', $club_id)->count(); 
		// dump($total_team); 
		$cap_count = 0; // UserTeams::where('club_id', $club_id)->where('capton_id', $player_id)->count(); 
		$vc_count = 0; // UserTeams::where('club_id', $club_id)->where('vice_capton_id', $player_id)->count(); 

		$player_count = UserTeams::where('club_id', $club_id)->whereHas('getTeamPlayer', function($q) use ($player_id) {
								    $q->where('player_id', $player_id);
								})->count();
		// dump($player_id);
		// dump($cap_count);
		// dump($vc_count); 
		// dump($player_count); die; 



		if($player_count > 0 && ($player_count - ($cap_count + $vc_count) > 0) && $total_team > 0){
			$player_count = (100 * ($player_count - ($cap_count + $vc_count))) / $total_team  ; 
		}

		$cap_count = UserTeams::where('club_id', $club_id)->where('capton_id', $player_id)->count(); 
		$vc_count = UserTeams::where('club_id', $club_id)->where('vice_capton_id', $player_id)->count(); 

		if($cap_count > 0 && $total_team > 0){
			$cap_count = (100 * $cap_count) / $total_team  ; 
		}
		if($vc_count > 0 && $total_team > 0 ){
			$vc_count = (100 * $vc_count) / $total_team  ; 
		}

		// dump($player_data); die; 



		$temp_fs = FixtureScorcard::with('fixture.teamdata', 'fixture.teamdata.get_grade')->where('player_id', $player_id)->orderBy('id', 'desc')->get();

		$tmp_fixture_grp = array(); 
		foreach ($temp_fs as $key => $value) {
			// dump($value); die; 
			$tmp_fixture_grp[$value->fixture_id][] = $value; 
		}

		// dump($tmp_fixture_grp);  // following loop for removing duplicates
		foreach ($tmp_fixture_grp as $key => $value) {
			if(count($value)> 1){ // for 2 day match 
				$temp = null; 
				foreach ($value as $k => $v) {
				  if(!empty($temp)){
				  	$v->rs += $temp->rs ; 
				  	$v->fours += $temp->fours ; 
				  	$v->sixes += $temp->sixes ; 
				  	$v->wks += $temp->wks ; 
				  	$v->mdns += $temp->mdns ; 
				  	$v->cs += $temp->cs ; 
				  	$v->cwks += $temp->cwks ; 
				  	$v->sts += $temp->sts ; 
				  	$v->rods += $temp->rods ; 
				  	$v->roas += $temp->roas ; 
				  	$v->dks += $temp->dks ; 
				  	$v->hattrick += $temp->hattrick ; 
				  	$v->overs += $temp->overs ; 
				  	$v->fantasy_points += $temp->fantasy_points ; 
				  }
				  $temp = $v; 
				}
				if(!empty($temp)){
					$tmp_fixture_grp[$key] = $temp; 
				}

			}else{
				$tmp_fixture_grp[$key] = $value[0]; 
			}

		}

		// dump($tmp_fixture_grp); die; 




		
		$temp_fs = $tmp_fixture_grp ; 
		
		// dump($temp_fs); die; 
			// dump($teamType = Config::get('senior_team_type')); 
		
			// dump($teamType = Config::get('junior_team_type')); 
	
		$teamType = Config::get('league_team_type'); 

		$gradeName	=	Grade::pluck('grade', 'id')->toArray();  // where('club', $club_id)->

		// dump($gradeName); 
		// dump($teamType); 
		// die;  

		// dump($player_fp); die;    rods
		$playerScorecardData = FixtureScorcard::where('player_id', $player_id)
								->groupBy('player_id')
							   ->selectRaw('sum(rs) as sum_rs, sum(fours) as sum_fours, sum(sixes) as sum_sixes, sum(wks) as sum_wks, sum(mdns) as sum_mdns, sum(cs) as sum_cs, sum(hattrick) as sum_hattrick, sum(overs) as sum_overs, sum(run) as sum_rg, sum(cwks) as sum_cwks, sum(sts) as sum_sts, sum(rods) as sum_rods, sum(dks) as sum_dks, sum(roas) as sum_roas, player_id, count(player_id) as total_match')->first(); 
		//prd($playerScorecardData);
		$totalMatches = FixtureScorcard::where('player_id', $player_id)->groupBy('inning')->count('player_id');
		$totalInnings = FixtureScorcard::where('player_id', $player_id)->count('player_id');

		$playerAvailability = Availability::where('is_active',1)->where('player', $player_id)->where('date_till', '>=', Carbon::now())->first(); 

		return View::make('front.common.player_profile', compact('player_data', 'position', 'player_fp', 'top_player_by_gwk_point', 'cap_count', 'vc_count', 'player_count', 'temp_fs', 'teamType', 'gradeName','playerScorecardData','playerAvailability','totalMatches','totalInnings'));
	}




	public function getTeamFantasyPoints($teamArray=null,$created_at=null)
    {    
        $teamIds = [];    
        $sumOfScore = 0;
        if(!$teamArray->isEmpty()){
        	foreach ($teamArray as $key => $value) {
	            $teamIds[] =  $value->player_id;
	        }
	        $sumOfScore = FixtureScorcard::whereIn('player_id',$teamIds)->where('created_at','>=',$created_at)->sum('fantasy_points');
        }
       
       return $sumOfScore;
    }
    public function array_sort($array, $on, $order=SORT_ASC){
	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}
	public function getClubModeList(){ 
		$mode = Input::get('id');
		$club_id = Input::get('club_id');
		$club = new Club;
		$getClubMode = $club->get_club_mode($mode);
		return View::make('front.common.get_club_mode_list',compact('getClubMode','club_id'));
	}

	public function getClubGameName(){
		$id = Input::get('id');
		$gameName = User::where('id',$id)->value('game_name');
		return View::make('front.common.game_name',compact('gameName'));
	}

	public function matchScorecard(){
		return View::make('front.common.match_scorecards');
	}

	public function payFundraiser(){
		$club_id = Session::get('sess_club_name'); 
       
		$price = Input::get('price');
		return View::make('front.common.pay_fundraiser',compact('price'));
	}
	public function payFundraiserFee(Request $request){
		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $club_id = Session::get('sess_club_name'); 
        $stripeData = GameSetting::where('club_id',$club_id)->first();
        $entry_price = User::where('id',$club_id)->value('entry_price');
        $entry_price = round($entry_price);
        $accountId = json_decode($stripeData->stripe_account_data)->stripe_user_id;
		
	try {
		 /*$getValues = Stripe\Transfer::create ([
            "amount" => $entry_price * 100,
            "currency" => "aud",
            "destination" => $accountId,
            "description" => "Fundraiser Payment" 
    	]);*/
    	$charge = \Stripe\Charge::create([
		  "amount" => $entry_price * 100,
		  "currency" => "aud",
		  "source" => $request->stripeToken,
		  "description" => "MyClubtap Fundraiser",
		  "transfer_data" => [
		    "destination" => $accountId,
		  ],
		]);
	}catch (\Stripe\Error\Card $e) {
		prd($e);
		$error = $e->getMessage();
	}
	if(!empty($error)){
		Session::flash('error', $error);
	}

		$obj = new Fundraiser;
		$obj->user_id = Auth::guard('web')->user()->id;
		$obj->club_id = $club_id;
		$obj->amount = $entry_price;
		$obj->description = $charge->description;
		$obj->save();
		Session::flash('success', 'You successfully paid fundraiser amount.');
        return back();
	}

	public function userChat(){ 
		return View::make('front.common.user_chat');
	} 
	public function userRefer(){
		$metaArray = [];
		$metaArray['og_url'] = WEBSITE_URL.'usersignup';
		$metaArray['og_title'] = Config::get('Site.title');
		$metaArray['og_description'] = 'description';
		$metaArray['og_image'] = WEBSITE_URL.'img/my_club_tap_icon.png';
		$result = User::where('user_referral_id',Auth::guard('web')->user()->id)->where('is_verified',1)->where('id','!=',1)->select('id','image','full_name','created_at')->get();
		return View::make('front.common.user_refer',compact('metaArray','result'));
	}
} 	



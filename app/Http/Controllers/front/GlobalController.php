<?php
/**
 * Home Controller
 */ 
namespace App\Http\Controllers\front;
use App\Http\Controllers\BaseController;
use App\Model\FixtureScorcard;
use App\Model\Fixture;
use App\Model\User;
use App\Model\DropDown;
use App\Model\Player;
use App\Model\VerifyUser;
use App\Model\Availability;
use App\Model\Fundraiser;
use App\Model\GamePoint;
use App\Model\UserTeams;
use App\Model\UserTeamPlayers;
use App\Model\UserTeamTradeLog;
use App\Model\UserSlider;
use App\Model\LockoutMailSendToUserLog; 
use Illuminate\Http\Request;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator,App;
use Carbon\Carbon;
use App\Model\EmailAction;   
use App\Model\EmailTemplate; 
use App\Model\NewsLettersubscriber; 

class GlobalController extends BaseController {

	public function matchScorecard($fixture_id=null){  
		$fixtureDetails = Fixture::leftJoin('teams','teams.id','=','fixtures.team')
								->leftJoin('grades','grades.id','=','fixtures.grade')
								->leftJoin('users','users.id','=','fixtures.club')
								->leftJoin('dropdown_managers' , 'dropdown_managers.id' , '=' , 'fixtures.match_type')
								->where('fixtures.id',$fixture_id)
								->select('fixtures.*','teams.name as team_name','grades.grade as grade_name','dropdown_managers.name as match_type_name','users.game_name as game_name')
								->first();
		//1-Day = 27 ; 2-Day = 28
		if($fixtureDetails->match_type == ONEDAYMATCH){
			$scorcard1 = FixtureScorcard::leftJoin('players','players.id','=','fixture_scorecards.player_id')
							->where('fixture_scorecards.fixture_id',$fixture_id)
							->where('fixture_scorecards.inning',1)
							->select('fixture_scorecards.*','players.full_name as player_name','players.position as player_position')
							->get();
		}if($fixtureDetails->match_type == TWODAYMATCH){
			$scorcard1 = FixtureScorcard::leftJoin('players','players.id','=','fixture_scorecards.player_id')
							->where('fixture_scorecards.fixture_id',$fixture_id)
							->where('fixture_scorecards.inning',1)
							->select('fixture_scorecards.*','players.full_name as player_name','players.position as player_position')
							->get();
			$scorcard2 = FixtureScorcard::leftJoin('players','players.id','=','fixture_scorecards.player_id')
							->where('fixture_scorecards.fixture_id',$fixture_id)
							->where('fixture_scorecards.inning',2)
							->select('fixture_scorecards.*','players.full_name as player_name','players.position as player_position')
							->get();
		}
		$fixtureScorcardDetails = FixtureScorcard::where('fixture_scorecards.fixture_id',$fixture_id)->select('fall_of_wickets','match_report','status')->first();
		$playerScorcards = FixtureScorcard::where('fixture_id',$fixture_id)->where('player_card','!=','')->value('player_card');
		return View::make('front.common.match_scorecards',compact('scorcard1','scorcard2','fixtureDetails','fixtureScorcardDetails','playerScorcards'));
	}

	public function playerAccount(){ 
		$club_id = \Session::get('sess_club_name');
		$clubDetails = User::where('id',$club_id)->first();
		$drop_down =	new DropDown();
		$batStyle = $drop_down->get_master_list("bat-style");
		$bowlStyle = $drop_down->get_master_list("bowl-style");
     	$playerId = VerifyUser::where('club_id',$club_id)->where('user_id',auth()->guard('web')->user()->id)->value('player_id');
		$playerDetails = Player::where('id',$playerId)->first();
		$availabilityData = Availability::where('club',$club_id)->where('player',$playerId)->get();
		return View::make('front.common.player_account',compact('clubDetails','batStyle','bowlStyle','playerId','playerDetails','availabilityData'));
	}
	public function savePlayerAccount(Request $request){
		$allData	=	$request->all();
		Validator::extend('custom_description', function($attribute, $value, $parameters) {
			if(count(explode(' ', $value)) > 100){
				return false;
			} else {
				return true;
			}
		});
		if(!empty($allData)){
			$validator = Validator::make(
				$allData,
				array(
					'bat_style' => 'required',
					'bowl_style' => 'required',
					'description' => 'custom_description',
				),array(
					"description.custom_description"	=>	'100 words exceed.Please enter less than 100 words.'
				)
			);
			if ($validator->fails()){
				$response	=	array(
					'success' 	=> false,
					'errors' 	=> $validator->errors()
				);
				return Response::json($response); 
				die;
			}else{
				$playerId = $request->player_id;
				if(empty($playerId)){
						$response	=	array(
						'success' 	=>	'2',
						'errors' 	=>	trans("Something went wrong.")
					);
					Session::flash('error',  trans("Something went wrong.please try again later.")); 
					return  Response::json($response); 
					die;
				}
				/*Player Profile update section*/
				$obj = Player::find($playerId);
				$obj->bat_style = $request->bat_style;
				$obj->bowl_style = $request->bowl_style;
				$obj->description = !empty($request->description) ? $request->description :'';

				if(Input::hasFile('image')){
					$unlinkImage =	Player::where('id',$playerId)->value('image');
					@unlink(PLAYER_IMAGE_ROOT_PATH.$unlinkImage);
					$extension  = Input::file('image')->getClientOriginalExtension();
					$newFolder  = strtoupper(date('M') . date('Y')) . '/';
					$folderPath = PLAYER_IMAGE_ROOT_PATH . $newFolder;
					
					if (!File::exists($folderPath)) {
						File::makeDirectory($folderPath, $mode = 0777, true);
					}
					$userImages = time() . '-player.' . $extension;
					$image = $newFolder . $userImages;
					if (Input::file('image')->move($folderPath, $userImages)) {
						$obj->image = $image;
					}
				}
				$obj->save();

				/*Player availability section*/
				$club_id = \Session::get('sess_club_name');
				if(!empty($request->date_from) && !empty($request->date_till)){
					$playerAvailability = Availability::where('club',$club_id)
											->where('player',$playerId)
											->where('date_from','<=',$request->date_from)
											->where('date_till','>=',$request->date_from)
											->count();
					if(!empty($playerAvailability)){
						$response	=	array(
							'success' 	=>	'2',
							'errors' 	=>	trans("Availability date already set.Please choose different dates.")
						);
						Session::flash('error',  trans("Availability date already set.Please choose different dates.")); 
						return  Response::json($response); 
						die;
					}
					$availabilityObj = new Availability();
					$availabilityObj->club = $club_id;
					$availabilityObj->player = $playerId;
					$availabilityObj->date_from = $request->date_from;
					$availabilityObj->date_till = $request->date_till;
					$availabilityObj->reason = !empty($request->reason) ? $request->reason :'';
					$availabilityObj->save();
				}

				$response	=	array(
					'success' 	=>	'1',
					'errors' 	=>	trans("Player account has been updated successfully.")
				);
				Session::flash('flash_notice',  trans("Player account has been updated successfully.")); 
				return  Response::json($response); 
				die;	
			}
		}
	}

	public function fundraiserListing(){
		$userId = Auth::guard('web')->user()->id;
		$fundraiserData = Fundraiser::leftJoin('users','users.id','fundraisers.club_id')
							->where('fundraisers.user_id',$userId)
							->select('fundraisers.*','users.club_name as club_name')
							->get();

		return View::make('front.common.fundraiser_listing',compact('fundraiserData'));

	}

	public function history(){
        $drop_down =    new DropDown();
        $position =    $drop_down->get_master_list("position");
        $position = array_replace(array_flip(array('4', '1', '3', '2')), $position);
        $category =    $drop_down->get_master_list("category");
        $svalue    =    $drop_down->get_master_list("svalue");
        $svalue = Config::get('player_price_list'); 
        $jvalue =    $drop_down->get_master_list("jvalue");
        $capton_id = 0; 
        $vice_capton_id = 0; 
        $my_team_name = ''; 
        $userteam_fantasy_points = array() ; 
        $gameweek_fantasy_points = array() ; 
        $club_name = Session::get('sess_club_name'); 
        $club_data = User::where('id', $club_name)->first(); 
        $cur_gameweek_no = 1; 
        if(!empty($club_data)){
            if(!empty($club_data->lockout_start_date)){
                $cur_gameweek_no = Carbon::parse($club_data->lockout_start_date)->diffInWeeks(Carbon::now());    
                $cur_gameweek_no++;
            }
        }
        $game_point_data = GamePoint::where('club', $club_name)->get(); 
        $players =  Player::where('club', $club_name)->where('full_name','!=','')->where('is_active', 1)->get(); 
        $chosen_player = []; 
        $user_teams = UserTeams::where('user_id', auth()->guard('web')->user()->id)->where('club_id', $club_name)->first();
        $user_teams_data = $user_teams ; 
        $no_of_trade = 0; 
        if ($user_teams) {
            $capton_id = $user_teams->capton_id; 
            $vice_capton_id = $user_teams->vice_capton_id; 
            $my_team_name = $user_teams->my_team_name;
            $no_of_trade = $user_teams->no_of_trade; 
            $user_teams = $user_teams->toArray();
        }else{
            $user_teams = [];
        }
		$tot_club_users = 0; 
    	$club_id = \Session::get('sess_club_name');
        $club_points = null; 
        if(!empty($club_id)){
            $club_points = GamePoint::where('club', $club_id)->get(); 
        }
		$lockout_club = true; 
        $club_id = Session::get('sess_club_name'); 
        $top_player_by_gwk_point = array(); 
        if(!empty($club_id)){
            $temp_club_data = User::where('id', $club_id)->first(); 
            $temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 
            $temp_lockout_start_date->addDay(); 

            $ex_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 
            $season_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 
            $team_players = UserTeams::where('club_id', $club_id)->where('user_id', Auth::user()->id)->first();
	        if(!empty($team_players)){
	            $team_id = $team_players->id; 
	            $capton_id = $team_players->capton_id;
	            $v_capton_id = $team_players->vice_capton_id; 
	            $team_players = UserTeamPlayers::where('team_id', $team_players->id)->pluck('player_id')->toArray(); 
	            $i = 1; 
	            while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->addWeek()->endOfWeek()) {
            		/*$seasonGameWeekStartDate = strtotime($temp_lockout_start_date->startOfWeek()); 
            		$seasonGameWeekEndDate = strtotime($season_lockout_start_date);
            		$startDate = Date('Y-m-d',$seasonGameWeekStartDate);
            		$endDateDate = Date('Y-m-d',$seasonGameWeekEndDate);
            		$seasonStartDate = strtotime($temp_lockout_start_date); 

            		$sum_fp = '';
            		$sum_rs = '';
            		$capton_point = '';
            		$vc_point = '';*/

            		/*For check gameweek date should not greater than start date*/ 
            		$sum_fp = FixtureScorcard::whereIn('player_id', $team_players)
                            	->whereDate('created_at', '>', $temp_lockout_start_date->startOfWeek())
                            	->whereDate('created_at', '<', $temp_lockout_start_date->endOfWeek())
                            	->sum('fantasy_points'); 
	                $sum_rs = FixtureScorcard::whereIn('player_id', $team_players)
	                        ->whereDate('created_at', '>', $temp_lockout_start_date->startOfWeek())
	                        ->whereDate('created_at', '<', $temp_lockout_start_date->endOfWeek())
	                        ->sum('rs'); 

	                $capton_point = FixtureScorcard::whereDate('created_at', '>', $temp_lockout_start_date->startOfWeek())
	            				->whereDate('created_at', '<', $temp_lockout_start_date->endOfWeek())
	            				->where('player_id', $capton_id)
	            				->sum('fantasy_points');


	                $vc_point = FixtureScorcard::whereDate('created_at', '>', $temp_lockout_start_date->startOfWeek())
								->whereDate('created_at', '<', $temp_lockout_start_date->endOfWeek())
								->where('player_id', $v_capton_id)
								->sum('fantasy_points');
								

			       
            		$overall_pts = FixtureScorcard::whereIn('player_id', $team_players)
                                    ->whereDate('created_at', '<', $temp_lockout_start_date->endOfWeek())
                                    ->sum('fantasy_points'); 
	                $sum_fp = $sum_fp + $capton_point + round($vc_point*.5); 
	                $no_of_trade = UserTeamTradeLog::where('team_id', $team_id)->where('created_at', '<=', $temp_lockout_start_date)->sum('no_of_trade') ; 

					$temp_lockout_start_date->addWeek();

					$top_player_by_gwk_point[$i]['gwk'] = $i; 
					$top_player_by_gwk_point[$i]['fp'] = $sum_fp; 
					$top_player_by_gwk_point[$i]['sum_rs'] = $sum_rs; 
					$top_player_by_gwk_point[$i]['no_of_trade'] = $no_of_trade; 
					$top_player_by_gwk_point[$i]['overall_pts'] = $overall_pts; 
	                $i++; 
	            }

	        }
        }
        return  View::make('front.common.history', compact('result', 'position', 'players','position','svalue','user_teams','chosen_player','capton_id','vice_capton_id', 'my_team_name', 'club_name', 'userteam_fantasy_points', 'club_data', 'no_of_trade', 'gameweek_fantasy_points', 'user_teams_data', 'lockout_club', 'twelve_player_select', 'remain_lock_out_time', 'team_ranks_arr', 'tot_club_users', 'temp_points_arr', 'lockout_time_remaining_seconds', 'booster_active', 'availability', 'club_points', 'top_trade_in', 'top_trade_out', 'cur_gameweek_no', 'top_player_by_gwk_point')); 

    }

    public function userChat(){ 
		$club_id = \Session::get('sess_club_name');
		$userid = Auth::guard('web')->user()->id;
		$userName = Auth::guard('web')->user()->full_name;
		$userDetails = User::where('id',$userid)
		              ->first();
		$clubDetails = User::where('id',$club_id)->first();
		$gamename = User::where('id',$club_id)->value('game_name');
		$checkGameExists = UserTeams::where('user_id',$userDetails->id)->where('club_id',$club_id)->count();
		if($checkGameExists == 0){
			return redirect()->intended('/lobby');
		}
     // prd($club_id); 
		/*$gamename = User::where('id',$userDetails->senior_club_name)->value('game_name');
		if($userDetails->senior_club_name == $club_id && $clubDetails->game_mode == 1  ){ 
			$gamename = User::where('id',$userDetails->senior_club_name)->value('game_name');
			$checkGameExists = UserTeams::where('user_id',$userDetails->id)->count();
		}if($userDetails->junior_club_name == $club_id && $clubDetails->game_mode == 2){ 
			$gamename = User::where('id',$userDetails->junior_club_name)->value('game_name');
			$checkGameExists = UserTeams::where('user_id',$userDetails->id)->count();
		}if($userDetails->league_club_name == $club_id && $clubDetails->game_mode == 3){ 
			$gamename = User::where('id',$userDetails->league_club_name)->value('game_name');
			$checkGameExists = UserTeams::where('user_id',$userDetails->id)->count();
		}
		if(empty($gamename)){
			return redirect()->intended('/lobby');
		}*/

		$profileImage = "";
		if(!empty(Auth::guard('web')->user()->image)){
			$profileImage = Auth::guard('web')->user()->image;
		}
		return View::make('front.common.user_chat',compact('gamename','club_id','userid','userName','profileImage'));
	} 

	public function getReferUserList(){
		$result = User::where('user_referral_id',Auth::guard('web')->user()->id)->where('is_verified',1)->where('id','!=',1)->select('id','image','full_name','created_at')->get();
		//prd($result);
		return View::make('front.common.refer_list',compact('result'));
	}


	public function userSharer(Request $request){ 
	$referQuery = $request->source;
	$ogTitle = "";
	if(!empty($referQuery) && $referQuery="linkedin"){
		$ogTitle = "Here is my referral code.For use please visit https://myclubtap.com/signupasauser?refercode=";
	}
	$tot_user_count = User::count(); 
		// dump($tot_user_count); die; 
	$tot_club = User::where('user_role_id', 3)->where('is_game_activate', 1)->count(); 
	$tot_senior_club = User::where('user_role_id', 3)->where('club_type', 1)->where('is_game_activate', 1)->count(); 
	$tot_junior_club = User::where('user_role_id', 3)->where('club_type', 2)->where('is_game_activate', 1)->count(); 
	$tot_league_club = User::where('user_role_id', 3)->where('club_type', 3)->where('is_game_activate', 1)->count(); 
	$tot_user_team = UserTeams::where('is_active', 1)->count(); 
		// dump($tot_junior_club); die; 
	$userSliders = UserSlider::leftJoin('users' , 'user_slider.club_name' , '=' , 'users.id')
	->where('user_slider.is_active',1)
	->select('user_slider.*','users.club_name')
	->get();
	return View::make('front.user.index',compact('userSliders', 'tot_user_count', 'tot_club', 'tot_senior_club', 'tot_junior_club', 'tot_league_club', 'tot_user_team'));
	} //end index()



	public function sendLockoutMailByCron(){

		$temp = User::where('user_role_id', 3)->get();

		// dump($temp); die; 



		foreach ($temp as $key => $value) {

			$club_data = $value; 

            if(!empty($club_data->lockout_start_date) ||  (!empty($club_data->is_lockout) && $club_data->is_lockout == 2) ){
            	$now = Carbon::now();

            	if(!empty($club_data->is_lockout) && $club_data->is_lockout == 2){
 	// echo 'hi'; die; 
            		$emitted = Carbon::now()->subDay()->next($club_data->lockout_start_day)->startOfDay(); 

            	}elseif(Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_date)->startOfDay()){
  		// dump(Carbon::parse($club_data->lockout_start_date)->dayName); 
  		// strtolower($club_data->lockout_start_day) == strtolower(Carbon::parse($club_data->lockout_start_date)->dayName) || 
            		if(	Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_end_day) < Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)

            	){
            			$emitted = Carbon::parse($club_data->lockout_start_date)->startOfDay();
            	}else{
            		$emitted = Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)->startOfDay();
            	}
  		// dump($club_data->lockout_start_day); die; 
  		// $emitted = Carbon::now()->next($club_data->lockout_start_day)->startOfDay();
            }elseif(Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_day)->startOfDay()){
            	$emitted = Carbon::now()->next($club_data->lockout_start_day)->startOfDay();
            }else{
            	$emitted = Carbon::now()->startOfDay();
            }
	  	// $emitted = Carbon::now()->next('wednesday');

            // dump($club_data); 
            // dump($emitted);  
            // dump($emitted->next($club_data->lockout_end_day)); 
            // die; 
            // dump($emitted->addHour()); 
            // dump($club_data->lockout_start_time); 

            if(!empty($club_data->lockout_start_time)){
            	$club_data->lockout_start_time = explode(':', $club_data->lockout_start_time); 
            	// dump($club_data->lockout_start_time); 
            	!empty($club_data->lockout_start_time[0]) ? $emitted->addHours($club_data->lockout_start_time[0]) : 0; 
            	!empty($club_data->lockout_start_time[1]) ? $emitted->addMinutes($club_data->lockout_start_time[1]) : 0; 
            }

            // dump($emitted); 

            // dump(Carbon::now()->diffInHours($emitted)); 

            $all_club_team = UserTeams::where('club_id', $club_data->id)->with('userdata')->get(); 

            // dump($all_club_team); die; 

            foreach ($all_club_team as $key => $value) {

            	if(LockoutMailSendToUserLog::where('team_id', $value->id)->where('club_id', $club_data->id)->where('mail_for', 1)->whereBetween('mail_send_date',  [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()] )->doesntExist()){



            		if(!empty($value->userdata) && ($value->userdata->lockout_notification == 1) && (Carbon::now()->diffInHours($emitted) <= 24)){

            			// dump(Carbon::now()->diffInHours($emitted)); 
            			// dump($club_data); 

					try {

								$settingsEmail 	= Config::get('Site.email');
								$full_name		= $value->userdata->full_name; 
								$game_name = $club_data->club_name ; 
								$start_time = $emitted->isoFormat('MMMM Do YYYY, h:mm:ss a'); 

								if(!empty($club_data->lockout_end_day) && !empty($club_data->lockout_end_time)){
									$tmp_emitted = $emitted->copy()->next($club_data->lockout_end_day); 
					            	$temp_lockout_end_time = explode(':', $club_data->lockout_end_time); 
					            	// dump($club_data->lockout_end_time); 
					            	!empty($temp_lockout_end_time[0]) ? $tmp_emitted->addHours($temp_lockout_end_time[0]) : 0; 
					            	!empty($temp_lockout_end_time[1]) ? $tmp_emitted->addMinutes($temp_lockout_end_time[1]) : 0; 
					            	$end_time = $tmp_emitted->isoFormat('MMMM Do YYYY, h:mm a'); 
								}else{
									$end_time = $emitted->isoFormat('MMMM Do YYYY, h:mm a'); 
								}





								$email			= 'testtt123@mailinator.com'; // $value->userdata->email;

								$emailActions	= EmailAction::where('action','=','send_lockout_start_time')->get()->toArray();
								$emailTemplates	= EmailTemplate::where('action','=','send_lockout_start_time')->get(array('name','subject','action','body'))->toArray();
							
								$cons 			= explode(',',$emailActions[0]['options']);
								$constants 		= array();
								
								foreach($cons as $key => $val){
									$constants[] = '{'.$val.'}';
								}
								
								$subject 		= $emailTemplates[0]['subject'];
								$rep_Array 		= array($full_name, $game_name, $start_time, $end_time); 
								$messageBody	= str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
								$mail			= $this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);


								// notification code here  (for lockout start)

					    		$lmstul = new LockoutMailSendToUserLog; 
					    		$lmstul->user_id = $value->user_id; 
					    		$lmstul->team_id = $value->id ; 
					    		$lmstul->club_id =  $club_data->id ; 
					    		$lmstul->mail_for = 1; 
					    		$lmstul->mail_send_date = Carbon::now(); 
					    		$lmstul->text = "Lockout start time is: ".$start_time." and end time is: ".$end_time;; 
					    		$lmstul->save(); 



					} catch (Exception $e) {
					    echo 'Caught exception: ',  $e->getMessage(), "\n";
					}



            		}

            	}



            		if(LockoutMailSendToUserLog::where('team_id', $value->id)->where('club_id', $club_data->id)->where('mail_for', 2)->whereBetween('mail_send_date',  [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()] )->doesntExist()){



            	if(!empty($value->userdata) &&  ($value->userdata->trading_lockout_notification == 1) && !empty($club_data->lockout_end_day) && !empty($club_data->lockout_end_time) ){

		// dump($emitted); 
						$tmp_emitted = $emitted->copy()->previous($club_data->lockout_end_day); 

		            	$temp1_lockout_end_time = explode(':', $club_data->lockout_end_time); 
		            	// dump($club_data->lockout_end_time); 
		            	!empty($temp1_lockout_end_time[0]) ? $tmp_emitted->addHours($temp1_lockout_end_time[0]) : 0; 
		            	!empty($temp1_lockout_end_time[1]) ? $tmp_emitted->addMinutes($temp1_lockout_end_time[1]) : 0; 
// dump($club_data); 
// dump($tmp_emitted); die; 
		            	if(Carbon::now() > $tmp_emitted){



							try {

										$settingsEmail 	= Config::get('Site.email');
										$full_name		= $value->userdata->full_name; 
										$game_name = $club_data->club_name ; 
										// $start_time = $emitted->isoFormat('MMMM Do YYYY, h:mm:ss a'); 






										$email			= 'testtt123@mailinator.com'; // $value->userdata->email;

										$emailActions	= EmailAction::where('action','=','send_lockout_end_time')->get()->toArray();
										$emailTemplates	= EmailTemplate::where('action','=','send_lockout_end_time')->get(array('name','subject','action','body'))->toArray();
									
										$cons 			= explode(',',$emailActions[0]['options']);
										$constants 		= array();
										
										foreach($cons as $key => $val){
											$constants[] = '{'.$val.'}';
										}
										
										$subject 		= $emailTemplates[0]['subject'];
										$rep_Array 		= array($full_name, $game_name); 
										$messageBody	= str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
										$mail			= $this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);


										// notification code here  (for trade start)

							    		$lmstul = new LockoutMailSendToUserLog; 
							    		$lmstul->user_id = $value->user_id; 
							    		$lmstul->team_id = $value->id ; 
							    		$lmstul->club_id =  $club_data->id ; 
							    		$lmstul->mail_for = 2; 
							    		$lmstul->mail_send_date = Carbon::now(); 
							    		$lmstul->text = "Now trading window is open you can trades now."; 
							    		$lmstul->save(); 



							} catch (Exception $e) {
							    echo 'Caught exception: ',  $e->getMessage(), "\n";
							}




		            	}


            			// dump(Carbon::now()->diffInHours($emitted)); 
            			// dump($club_data); 


            		}

            	}



            	
            }




        }

		}
		return 1; 
		// echo 'end'; 
		die; 

	}

	public function notification(){
		$getNotificationCount = LockoutMailSendToUserLog::where('user_id',Auth::guard('web')->user()->id)->count();
		return View::make('front.global.notification',compact('1'));
	}

	public function checkNotification(){
		$checkReadStatus = LockoutMailSendToUserLog::where('user_id',Auth::guard('web')->user()->id)->where('is_read',0)->count();
		if($checkReadStatus > 1){
			LockoutMailSendToUserLog::where('user_id',Auth::guard('web')->user()->id)->update(['is_read'=>1]);
		}
		echo 1;die;
	}
	public function testimonials(){
		return View::make('front.global.testimonials');
	}

	public function unsubscribe_newsletter($enc_id = null){ 
		$DB			=	NewsLettersubscriber::query();
		$user		=	$DB->where('status',1)->where('enc_id',$enc_id)->first();
		$userEncId  =	User::where('is_active',1)->where('enc_id',$enc_id)->first();
		if(!empty($user)){
			$obj		=	NewsLettersubscriber::find($user->id);
			$obj->status = 0;
			$obj->enc_id = '';
			$obj->save();
			Session::flash('flash_notice',  trans("You are successfully unsubscribed for newsletter.")); 
			return Redirect::to('/');
		}elseif(!empty($userEncId)){
			User::where('enc_id',$enc_id)->update(['is_subscribe'=>0]);
			Session::flash('flash_notice',  trans("You are successfully unsubscribed for newsletter.")); 
			return Redirect::to('/');
		}else{
			Session::flash('error',  trans("Sorry you are using wrong link.")); 
			return Redirect::to('/');
		}
	}
}// end GlobalController

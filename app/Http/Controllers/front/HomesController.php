<?php
/**
 * Home Controller
 */
namespace App\Http\Controllers\front;
use App\Http\Controllers\BaseController;
use App\Model\User;
use App\Model\Country;
use App\Model\DropDown;
use App\Model\EmailAction;
use App\Model\EmailTemplate;
use Illuminate\Http\Request;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator,App;

class HomesController extends BaseController {
	
/** 
 * Function to display Game/Club signup view
 *
 * @param null
 * 
 * @return view page
 */
	public function signupView() { 
		if(Auth::user()){
			Session::flash('error',trans("Please logout first."));
			return redirect()->intended('/');
		}
		$countryObj	= new Country();
		$dropdown =	new DropDown();
		$countryList =	$countryObj->get_country();
		$positionList = $dropdown->get_master_list('club-position');
		return  View::make('front.home.signup',compact("countryList","positionList"));
	}// end signupView()

/** 
 * Function to display user signup view
 *
 * @param null
 * 
 * @return view page
 */
	public function userSignupView(Request $request) {
		$refCode = !empty($request->refercode) ? $request->refercode:'';
		if(Auth::user()){
			Session::flash('error',trans("Please logout first."));
			return redirect()->intended('/');
		}
		$countryObj	= new Country();
		$dropdown =	new DropDown();
		$countryList =	$countryObj->get_country();
		$positionList = $dropdown->get_master_list('position');
		return  View::make('front.home.user_signup',compact("countryList","positionList","refCode"));
	}// end signupView()

/** 
 * Function to save User signup information
 *
 * @param null
 * 
 * @return void
 */
	public function userSignup(){   
		Input::replace($this->arrayStripTags(Input::all()));
		$formData	=	Input::all();
		Validator::extend('custom_password', function($attribute, $value, $parameters) {
			if (preg_match('#[0-9]#', $value) && preg_match('#[a-zA-Z]#', $value) && preg_match('#[\W]#', $value)) {
				return true;
			} else {
				return false;
			}
		});
		Validator::extend('custom_referral_code', function($attribute, $value, $parameters) {
			$lowerReferCode = strtolower($value);
			$checkReferCode = User::where('referral_code',$lowerReferCode)->where('is_deleted',0)->count();
			if ($checkReferCode > 0) {
				return true;
			} else {
				return false;
			}
		});
		$validator = Validator::make(
			Input::all(),
			array(
				'first_name'=> 'required',
				'last_name'	=> 'required',
				'email' => 'required|email|unique:users',
				'password'	=> 'required|min:8|custom_password',
				'confirm_password'=> 'required|min:8|same:password', 	
				'terms'=> 'required',
				'privacy'=> 'required',
				'referral_code'	=> 'custom_referral_code',
				//'newslater'=> 'required',
			),
			array(
				"password.custom_password"		=>	trans("Password must have be a combination of numeric, alphabet and special characters."),
				"confirm_password.required"		=>	trans("Please enter confirm password."),
				"password.min"					=>	trans("Password must have a mimimum of 8 characters."),
				"email.unique"					=>	trans("Email already exists."),
				"referral_code.custom_referral_code"	=>	trans("Referral code does not exists."),
			)
		);
		if ($validator->fails()){
			$errors 				=	$validator->messages();
			 $response	=	array(
				'success' 	=> false,
				'errors' 	=> $errors
			);
			return Response::json($response); 
			die;
		}else {
			$obj 					=  new User;
			$validateString			=  md5(time() . Input::get('email'));
			$obj->validate_string	=  $validateString;					
			$obj->first_name 		=  ucfirst(Input::get('first_name'));
			$obj->last_name 		=  ucfirst(Input::get('last_name'));
			$obj->full_name 		=  $obj->first_name." ".$obj->last_name;
			$obj->email 			=  Input::get('email');
			$obj->slug	 			=  $this->getSlug(Input::get('first_name')." ".Input::get('last_name'),'full_name','User');
			$obj->password	 		=  Hash::make(Input::get('password'));
			$obj->user_role_id		=  USER;
			$obj->is_verified		=  0;
			$obj->is_active			=  1;
			if(!empty(Input::get('referral_code'))){
				$userrefererCode = strtolower(Input::get('referral_code'));
				$refererId = User::where('referral_code',$userrefererCode)->value('id');
				$obj->user_referral_id = $refererId;
			}
			$obj->referral_code		=  substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 8);
			$obj->is_subscribe		=  !empty(Input::get('newslater')) ? 1 :0;
			$obj->save();
			$userId					=	$obj->id;
			//mail email and password to new registered user
			$settingsEmail 	= Config::get('Site.email');
			$full_name		= $obj->full_name; 
			$email			= $obj->email;
			$password		= Input::get('password');
			$route_url      = URL::to('account-verification/'.$obj->validate_string);
			$select_url     = "<a href='".$route_url."'>".trans("Click here")."</a>";
			
			$emailActions	= EmailAction::where('action','=','account_verification')->get()->toArray();
			$emailTemplates	= EmailTemplate::where('action','=','account_verification')->get(array('name','subject','action','body'))->toArray();
		
			$cons 			= explode(',',$emailActions[0]['options']);
			$constants 		= array();
			
			foreach($cons as $key => $val){
				$constants[] = '{'.$val.'}';
			}
			
			$subject 		= $emailTemplates[0]['subject'];
			$rep_Array 		= array($full_name,$select_url,$route_url); 
			$messageBody	= str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
			$mail			= $this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);
			Session::flash('flash_notice',  trans("Your account has been registered successfully. Please check your email to verify your account."));
			$response	=	array(
				'success' 	=>	'1',
				'errors' 	=>	trans("Your account has been registered successfully. Please check your email to verify your account.")
			); 
			return  Response::json($response); 
			die;	
		}
	}// end signup()
/** 
 * Function to save Club/Game signup information
 *
 * @param null
 * 
 * @return void
 */
	public function signup(){   
		Input::replace($this->arrayStripTags(Input::all()));
		$formData	=	Input::all();
		Validator::extend('custom_password', function($attribute, $value, $parameters) {
			if (preg_match('#[0-9]#', $value) && preg_match('#[a-zA-Z]#', $value) && preg_match('#[\W]#', $value)) {
				return true;
			} else {
				return false;
			}
		});
		$validator = Validator::make(
			Input::all(),
			array(
				'first_name'=> 'required',
				'last_name'	=> 'required',
				'country'=> 'required',
				'email' => 'required|email|unique:users',
				'password'	=> 'required|min:8|custom_password',
				'confirm_password'=> 'required|min:8|same:password', 	
				'image' => 'mimes:'.IMAGE_EXTENSION,			
				'game_mode' => 'required',
				'username' => 'required|unique:users',
				'state'	=> 'required',
				'city'	=> 'required',
				'position'=> 'required',
				'terms'=> 'required',
				'privacy'=> 'required',
				'authorize'	=> 'required',
				'club_name'	=> 'required',
				'game_name'	=> 'required', 
			),
			array(
				"password.custom_password"		=>	trans("Password must have be a combination of numeric, alphabet and special characters."),
				"confirm_password.required"		=>	trans("Please enter confirm password."),
				"password.min"					=>	trans("Password must have a mimimum of 8 characters."),
				"email.unique"					=>	trans("Email already exists."),
			)
		);
		if ($validator->fails()){
			$errors 				=	$validator->messages();
			 $response	=	array(
				'success' 	=> false,
				'errors' 	=> $errors
			);
			return Response::json($response); 
			die;
		}else {
			$obj 					=  new User;
			$validateString			=  md5(time() . Input::get('email'));
			$obj->validate_string	=  $validateString;					
			$obj->first_name 		=  ucfirst(Input::get('first_name'));
			$obj->last_name 		=  ucfirst(Input::get('last_name'));
			$obj->full_name 		=  $obj->first_name." ".$obj->last_name;
			$obj->email 			=  Input::get('email');
			$obj->slug	 			=  $this->getSlug(Input::get('first_name')." ".Input::get('last_name'),'full_name','User');
			$obj->password	 		=  Hash::make(Input::get('password'));
			$obj->dob	 			=  !empty(Input::get('dob')) ? Input::get('dob') :date('Y-m-d',time());
			$obj->user_role_id		=  CLUBUSER;
			$obj->country			=  !empty(Input::get('country')) ? Input::get('country') : 0;
			$obj->state				=  !empty(Input::get('state')) ? Input::get('state') : 0;
			$obj->city				=  !empty(Input::get('city')) ? Input::get('city') : 0;
			/*$obj->phone				=  !empty(Input::get('phone')) ? Input::get('phone') : 0;*/
			$obj->gender			=  Input::get('gender');

			$obj->game_mode			=  !empty(Input::get('game_mode')) ? Input::get('game_mode') : '';
			$obj->club_name			=  Input::get('club_name');
			$obj->game_name			=  Input::get('game_name');
			$obj->username			=  Input::get('username');
			
			$obj->sport_name		=  'Cricket';
			$obj->position			=  Input::get('position');

			$obj->is_verified		=  0;
			$obj->is_active			=  1;
			$obj->is_approved		=  1;
			$obj->is_profile_completed =   1;
			
			if(input::hasFile('club_logo')){
				$extension 			=	Input::file('club_logo')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	CLUB_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-club.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('club_logo')->move($folderPath, $userImageName)){
					$obj->club_logo		=	$image;
				}
			}
			$obj->save();
			$userId					=	$obj->id;
			//mail email and password to new registered user
			$settingsEmail 	= Config::get('Site.email');
			$full_name		= $obj->full_name; 
			$email			= $obj->email;
			$password		= Input::get('password');
			$route_url      = URL::to('account-verification/'.$obj->validate_string);
			$select_url     = "<a href='".$route_url."'>".trans("Click here")."</a>";
			
			$emailActions	= EmailAction::where('action','=','account_verification')->get()->toArray();
			$emailTemplates	= EmailTemplate::where('action','=','account_verification')->get(array('name','subject','action','body'))->toArray();
		
			$cons 			= explode(',',$emailActions[0]['options']);
			$constants 		= array();
			
			foreach($cons as $key => $val){
				$constants[] = '{'.$val.'}';
			}
			
			$subject 		= $emailTemplates[0]['subject'];
			$rep_Array 		= array($full_name,$select_url,$route_url); 
			$messageBody	= str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
			$mail			= $this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);
			Session::flash('flash_notice',  trans("Your account has been registered successfully. Please check your email to verify your account."));
			$response	=	array(
				'success' 	=>	'1',
				'errors' 	=>	trans("Your account has been registered successfully. Please check your email to verify your account.")
			); 
			return  Response::json($response); 
			die;	
		}
	}// end signup()
	
/** 
 * Function use for accountverify
 *
 * @param null
 * 
 * @return void
 */	
	public function accountverify(){
		$login_user			=	Auth::guard('web')->user();
		if(empty($login_user)) {
			return Redirect::to('/');
		}
		return  View::make('front.user.accountverify');
	}//end accountverify()
	
/** 
 * Function to verify user account
 *
 * @param $validateString for get user validate string
 * 
 * @return void
 */
	public function Verify($validateString = '') {
		if($validateString!="" && $validateString!=null){
			$userDetail	=	User::where('is_active','1')->where('validate_string',$validateString)->first();
			if(!empty($userDetail)){
				User::where('validate_string',$validateString)->update(array('validate_string'=>'',
				'is_verified'=>1));
				return Redirect::to('/')
						->with('success', trans("Your MyClubtap account has now been verified successfully."));
			}else{
				return Redirect::to('/')
						->with('error', trans('Sorry, You are using wrong link.'));
			}
		}else{
			return Redirect::to('/')->with('error', trans('Sorry, You are using wrong link.'));
		}
	}// end Verify()
	
/** 
 * Function use for view login 
 *
 * @param null
 * 
 * @return void
 */
	public function login(){ 
		if(Auth::guard('web')->user()){
			return redirect()->intended('/');
		}
		return  View::make('front.home.login');
	}//end Login
	
/** 
 * Function use for login user
 *
 * @param null
 * 
 * @return void
 */	
	public function loginUser(Request $request){
		Input::replace($this->arrayStripTags(Input::all()));
		$formData	=	Input::all();
		if(!empty($formData)){
			$validator = Validator::make(
				Input::all(),
				array(
					'login_username' 			=> 'required',
					'login_password'			=> 'required',
				),
				array(
					'login_username.required' 		=> trans('Email field is required.'),
					'login_password.required'		=> trans('Password field is required.'),
				)
			);
		}
		if ($validator->fails()){
			$response	=	array(
				'success' 	=> 0,
				'message' 	=> $validator->errors()
			);
			return Response::json($response); 
			die;
		}else {
			$userdata = array(
				'email' 		=>	 Input::get('login_username'),
				'password' 		=> 	 Input::get('login_password'),
				'is_active' 	=>   1,
				'is_verified' 	=>   1,
				'is_deleted' 	=>   0,
			);
			$remember_me 		= !empty(Input::get('remember_me')) ? true : false; 
			if (Auth::guard('web')->attempt($userdata,$remember_me)){ 
				if($request->ajax()) {
					if(Auth::guard('web')->user()->user_role_id != SUPER_ADMIN_ROLE_ID){
						Session::put('user_password',Input::get('login_password'));
						Session::flash('success',  trans("You have successfully logged in.")); 
						$err				=	array();
						$err['success']		=	1;
						$err['is_profile_completed']=Auth::guard('web')->user()->is_profile_completed;
						$err['message']		=	'';
						return Response::json($err); 
						die;
					}else{
						$err				=	array();
						$err['success']		=	2;
						$err['message']		=	trans('Email or Password is incorrect.');
						return Response::json($err); 
						die;
					}
				}else{
					return redirect()->intended('/lobby');
				}
			}else{  
				$userDetails	=	DB::table('users')
									->where('email',Input::get('login_username'))
									->where('user_role_id', '!=' , SUPER_ADMIN_ROLE_ID )
									->where('is_deleted', '=' , INACTIVE )
									->first();
				if(!empty($userDetails)) { 
					if($userDetails->is_active == INACTIVE) {
						$err				=	array();
						$err['success']		=	2;
						$err['message']		=	trans('Your account is inactive please contact to admin');
						return Response::json($err); 
					}else if($userDetails->is_verified == INACTIVE) {
						$err				=	array();
						$err['success']		=	2;
						$err['message']		=	trans('Your account is unverified please verified your account');
						return Response::json($err); 
					}else {
						$err				=	array();
						$err['success']		=	2;
						$err['message']		=	trans('Email or Password is incorrect.');
						return Response::json($err); 
					}
				}else { 
					$err				=	array();
					$err['success']		=	2;
					$err['message']		=	trans('Email or Password is incorrect.');
					return Response::json($err); 
				}
				die;
			}
		} 
	}// end LoginUser()
	
/** 
 * Function use for logout user
 *
 * @param null
 * 
 * @return void
 */
	public function logout(){
		Session::flush();
		Auth::guard('web')->logout();
		return Redirect::to('/');
	}// end logout()
	
/** 
 * Function use view Forgotpassword
 *
 * @param null
 * 
 * @return void
 */
	public function ViewForgotPassword(){
		return  View::make('front.user.forgotpassword');
	}//end ForgetView
	
/** 
 * Function use for send a forgot password email to user
 *
 * @param null
 * 
 * @return void
 */
	public function ForgotPassword(){
		 $validator = Validator::make(
			Input::all(),
			array(
				'forgot_email' 			=> 'required|email',
			),
			array(
				'forgot_email.required' 			=> trans("Email field is required."),
				'forgot_email.email' 				=> trans("Please enter valid email address."),
			)
		);
		if ($validator->fails()){
			$errors 	=	$validator->messages();
			$response	=	array(
				'success' 	=> 0,
				'errors' 	=> $errors
			);
			return Response::json($response); 
			die;
		}else{
			$email		=	Input::get('forgot_email');   
			$userDetail	=	User::where('email',$email)
									->where('is_active','=',ACTIVE)
									->where('is_verified','=',ACTIVE)
									->where('is_deleted','=',INACTIVE)
									->first();
				
			if(!empty($userDetail)){
				$forgot_password_validate_string	= 	md5($userDetail->email);
				User::where('email',$email)->update(array('forgot_password_validate_string'=>$forgot_password_validate_string));
				$settingsEmail 		=  Config::get('Site.email');
				$email 				=  $userDetail->email;
				$username			=  $userDetail->full_name;
				$full_name			=  $userDetail->full_name;  
				$route_url      	=  URL::to('reset-password/'.$forgot_password_validate_string);
				$verify_link   		=   $route_url;
				
				$emailActions		=	EmailAction::where('action','=','forgot_password')->get()->toArray();
				$emailTemplates		=	EmailTemplate::where('action','=','forgot_password')->get(array('name','subject','action','body'))->toArray();
				$cons = explode(',',$emailActions[0]['options']);
				$constants = array();
				
				foreach($cons as $key=>$val){
					$constants[] = '{'.$val.'}';
				}
				$subject 			=  $emailTemplates[0]['subject'];
				$rep_Array 			= array($username,$verify_link,$route_url); 
				$messageBody		=  str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
				$this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);
				Session::flash('flash_notice',  trans("Password reset link has been emailed to your email account."));  
				$err				=	array();
				$err['success']		=	1;
				$err['message']		=	trans('Password reset link has been emailed to your email account.');
				return Response::json($err); 
			}else{
				$response	=	array(
					'success' 	=> 2,
					'message' 	=> trans("Your email is not registered with us.")
				);
				return Response::json($response); 
				die;
			}
		}
	} //end ForgotPassword()
	
/** 
 * Function use for reset passowrd
 *
 * @param null
 * 
 * @return void
 */	
	public function resetPassword($validateString ='' ){
		if($validateString!="" && $validateString!=null){
			$userDetail	=	User::where('is_active','1')->where('forgot_password_validate_string',$validateString)->first();
			if(!empty($userDetail)){
				return View::make('front.user.resetpassword',compact('validateString'));
			}else{
				return Redirect::to('/')
						->with('error', trans('Sorry, You are using wrong link.'));
			}
		}else{
			return Redirect::to('/')->with('error', trans('Sorry, You are using wrong link.'));
		}
	}//end resetPassword()
	
/** 
 * Function use for save password
 *
 * @param null
 * 
 * @return void
 */
	public function saveResetPassword(){
		$newPassword		=	Input::get('password');
		// print_r(Input::all()); die; 
		$validate_string	=	Input::get('validate_string');
		Validator::extend('custom_password', function($attribute, $value, $parameters) {
			if (preg_match('#[0-9]#', $value) && preg_match('#[a-zA-Z]#', $value) && preg_match('#[\W]#', $value)) {
				return true;
			} else {
				return false;
			}
		});
		$rules    = 	array(
			'password'				=>	'required|min:8|custom_password',
			'confirm_password' 	 	=> 'required|min:8|same:password',
		);
		$validator 				= 	Validator::make(Input::all(), $rules,
		array(
			"password.required"			=>	trans("Password field is required."),
			"confirm_password.required"	=>	trans("Confirm Password field is required."),
			"confirm_password.same"		=>	trans("confirm password and password must match."),
			"confirm_password.min"		=>	trans("The confirm password must be at least 8 characters."),
			"password.min"				=>	trans("The new password must be at least 8 characters."),
			"password.custom_password"	=>	trans("Password must have combination."),
		));
		
		if ($validator->fails()){	
			$response	=	array(
				'success' 	=> false,
				'errors' 	=> $validator->errors()
			);
			return Response::json($response); 
			die;
		}else{
			// echo Hash::make($newPassword); echo $newPassword; die; 
			$userInfo = User::where('forgot_password_validate_string',$validate_string)->first();
			User::where('forgot_password_validate_string',$validate_string)
				->update(array(
					'password'							=>	Hash::make($newPassword),
					'forgot_password_validate_string'	=>	''
				));
			$settingsEmail 		= Config::get('Site.email');			
			$action				= "reset_password";
			
			$emailActions		=	EmailAction::where('action','=','reset_password')->get()->toArray();
			$emailTemplates		=	EmailTemplate::where('action','=','reset_password')->get(array('name','subject','action','body'))->toArray();
			$cons 				= 	explode(',',$emailActions[0]['options']);
			$constants 			= 	array();
			foreach($cons as $key=>$val){
				$constants[] = '{'.$val.'}';
			}
			$subject 			=  $emailTemplates[0]['subject'];
			$rep_Array 			= array($userInfo->full_name); 
			$messageBody		=  str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
									 
			$this->sendMail($userInfo->email,$userInfo->full_name,$subject,$messageBody,$settingsEmail);
			$response	=	array(
				'success' 	=> true,
			);
			Session::flash('flash_notice', trans("Password has been reset successfully.")); 
			return Response::json($response); 
			die;
		}
	}//end saveResetPassword()
	

	public function frontSignup(Request $request){

		return  View::make('front.home.front_signup');

	}

	
}// end HomesController

<?php
/**
 * Users Controller
 */
namespace App\Http\Controllers\front;
use App;
use App\Http\Controllers\BaseController;
use App\Model\FixtureScorcard;
use App\Model\User;
use App\Model\UserTeams;
use App\Model\UserTeamsCVCTrack;
use App\Model\UserTeamsPlayerTrack;
use App\Model\UserTeamPlayers; 
use App\Model\UserTeamsGWExtraPTTrack; 
use Auth;
use Carbon\Carbon;
use Session;
use View, DB;

class MyCompsController extends BaseController {

	public function index() {
		$club_id = \Session::get('sess_club_name');
		$user_team_club = User::where(['is_active' => 1, 'is_deleted' => 0, 'user_role_id' => 3, 'is_game_activate' => 1])->pluck('id')->toArray();

		$user_teams = UserTeams::where('user_id', auth()->user()->id)->whereIn('club_id', $user_team_club)->get();

		$user_team_club = UserTeams::where('user_id', Auth::user()->id)->whereIn('club_id', $user_team_club)->pluck('club_id')->toArray();

		// dump($user_teams); die;
		$temp = array();
	/* // commented on  11-06-19
		foreach ($user_teams as $key => $value) {
	

			// dump($value); die;
			$user_team_player = UserTeamPlayers::where('team_id', $value->id)->get();
			$tmp_top_pnt = 0;
			foreach ($user_team_player as $k1 => $v1) {




					$vtemp = UserTeamsPlayerTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->whereNull('remove_date')->first(); 

					if(!empty($vtemp)){
						$v1 = $vtemp ; 
					}else{
						$utp_vtemp = UserTeamPlayers::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->first(); 
						if(!empty($utp_vtemp)){
							$v1 = $utp_vtemp; 
						}

					}







				// echo 'hi'; 
				// dump($v1); die; 

				// $tmp_top_pnt += $tmp_pnt ; 

					// dump($v1->player_id); 
					if($v1->player_id == $value->capton_id){

						$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 1)->whereNull('remove_date')->first(); 
						if(!empty($utcvct)){
							$tmp_strt_dt = $utcvct->created_at ; 
						}else{
							// echo 'hi1'; die; 
							$tmp_strt_dt = $v1->created_at ; 
						}
						// echo 'hi'; 
						// dump(Carbon::parse($tmp_strt_dt)->endOfDay()); 

						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)



					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

							->whereHas('fixture', function($q) use ($tmp_strt_dt){
												$q->where('start_date', '>', Carbon::parse($tmp_strt_dt)->endOfDay());
											})

					// ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)







						// dump($tmp_pnt); 
						// echo 'hello'; 
						$tmp_top_pnt += round($tmp_pnt) ; 
					}

					if($v1->player_id == $value->vice_capton_id){


						$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
						if(!empty($utcvct)){
							$tmp_strt_dt = $utcvct->created_at ; 
						}else{
							// echo 'hi2'; die; 
							$tmp_strt_dt = $v1->created_at ; 
						}




						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)

					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

						->whereHas('fixture', function($q) use ($tmp_strt_dt){
																		$q->where('start_date', '>', Carbon::parse($tmp_strt_dt)->endOfDay());
																	})
					// ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)
















						$tmp_top_pnt += round($tmp_pnt * 0.5) ; 
					}


						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)


					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

						->whereHas('fixture', function($q) use ($v1){
									$q->where('start_date', '>', Carbon::parse($v1->created_at)->endOfDay());
								})
					// ->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)







						$tmp_top_pnt += $tmp_pnt ; 



					// dump($tmp_top_pnt); 




			}
			$tmp_top_pnt += $value->extra_team_point;

			$temp[$value->id] = $tmp_top_pnt;

		}

		*/ 

		$temp_points_arr = array(); 

		// $temp = UserTeams::where('club_id', $club_id)->pluck('total_team_point', 'id')->toArray(); 

		foreach ($user_team_club as $key => $value) {

			// $temp = UserTeams::pluck('total_team_point', 'id')->toArray(); 
			// $temp = UserTeams::where('club_id', $value)->pluck('total_team_point', 'id')->toArray(); 



				$club_team_ids = UserTeams::where('club_id', $value)->pluck('id')->toArray(); 

			$temp =  UserTeamsGWExtraPTTrack::select(DB::raw('max(id) as gw_id, team_id'))  // get the id of most last gameweek table record 
			                     ->whereIn('team_id', $club_team_ids)
			                     ->groupBy('team_id')
			                     ->pluck('gw_id', 'team_id')->toArray();




			$temp = UserTeamsGWExtraPTTrack::whereIn('id', $temp)->pluck('overall_pt', 'team_id')->toArray();  // get the over all point 





			arsort($temp);
			// dump($temp); die;

			$temp=array_map("round",$temp); // remove decimal points 

			$temp_points_arr[$value] = $temp;

		}


			
		// dump($temp_points_arr);  die; 

			foreach ($temp_points_arr as $k2 => $v2) {
				$i = 1;
				foreach ($v2 as $k3 => $v3) {
					$v2[$k3] = $i++;
				}
				$team_ranks_arr[$k2] = $v2;
			}



			// $team_ranks_arr[$value] = $temp;





		// dump($team_ranks_arr); die; 

		$temp = array();

		// dump($user_teams); die;

		foreach ($user_teams as $key => $value) {
			$temp[$value->game_mode][] = $value;
		}
		$user_teams = $temp;



		// dump($user_teams); die;

		// dump($user_team_club); die;

		$club_arr = User::whereIn('id', $user_team_club)->where('user_role_id', 3)->pluck('club_name', 'id')->toArray();

		// dump($club_arr);  die;
		$temp = User::where('user_role_id', 3)->whereIn('id', $user_team_club)->get();

		// dump($temp); die;

		$ttemp = array();
		foreach ($temp as $key => $value) {
			$ttemp[$value->id] = $value;
		}
		$club_dta = $ttemp;

		// dump($user_teams); die;

		// dump($team_ranks_arr); die;

		// dump($club_dta); die;

		return View::make('front.my_comps.index', compact('user_teams', 'club_arr', 'temp_points_arr', 'team_ranks_arr', 'club_dta'));
	}

}

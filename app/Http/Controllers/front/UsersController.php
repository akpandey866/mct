<?php
/**
 * Users Controller
 */  
namespace App\Http\Controllers\front;
use App\Http\Controllers\BaseController;
use App\Model\EmailAction;
use App\Model\User;
use App\Model\Language;
use App\Model\Cms;
use App\Model\UserTeams; 
use App\Model\UserTeamPlayerLogs; 
use App\Model\UserTeamTradeLog ; 
use App\Model\EmailTemplate;
use App\Model\UserTeamValueLog; 
use App\Model\DropDown;
use App\Model\GamePoint; 
use App\Model\Player;
use App\Model\UserTeamPlayers;
use App\Model\UserTeamLogs; 
use App\Model\PlayerTradeInOut; 
use App\Model\FixtureScorcard;
use App\Model\Platform;
use App\Model\Serve;
use App\Model\Partner;
use App\Model\UserSlider;
use App\Model\TeamSlider;
use App\Model\Fixture;
use App\Model\Availability;
use App\Model\Country;
use App\Model\Club;
use App\Model\GameSetting;
use App\Model\PlayerSvalueLog; 
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use Carbon\Carbon;
use App\Model\UserTeamsPlayerTrack; 

use App\Model\Branding; 
use App\Model\UserTeamsCVCTrack; 
use App\Model\UserTeamsGWExtraPTTrack; 

class UsersController extends BaseController {

/** 
 * Function to display website home page
 *
 * @param null
 * 
 * @return view page
 */
public function index(){ 
	if(Auth::guard('web')->user()){
		return Redirect::to('/lobby'); 
	}
		// echo 'hi'; die; 
	$tot_user_count = User::count(); 
		// dump($tot_user_count); die; 
	$tot_club = User::where('user_role_id', 3)->where('is_game_activate', 1)->count(); 
	$tot_senior_club = User::where('user_role_id', 3)->where('club_type', 1)->where('is_game_activate', 1)->count(); 
	$tot_junior_club = User::where('user_role_id', 3)->where('club_type', 2)->where('is_game_activate', 1)->count(); 
	$tot_league_club = User::where('user_role_id', 3)->where('club_type', 3)->where('is_game_activate', 1)->count(); 
	$tot_user_team = UserTeams::where('is_active', 1)->count(); 
		// dump($tot_junior_club); die; 
	$userSliders = UserSlider::leftJoin('users' , 'user_slider.club_name' , '=' , 'users.id')
	->where('user_slider.is_active',1)
	->select('user_slider.*','users.club_name')
	->get();
	return View::make('front.user.index',compact('userSliders', 'tot_user_count', 'tot_club', 'tot_senior_club', 'tot_junior_club', 'tot_league_club', 'tot_user_team'));
	} //end index()

/** 
 * Function to display cms page on website
 *
 * @param slug as slug of cms page
 * 
 * @return view page
 */	
public function showCms($slug){
		// echo 'hi'; die; 
	$result		=	DB::table('cms_pages')->where(['slug'=>$slug])->first();			
		// dump($result); die; 
	return View::make('front.cms.index' , compact('result','slug'));
	}//end showCms()


/** 
 * Function to display cms page on website
 *
 * @param slug as slug of cms page
 * 
 * @return view page
 */	
public function showAboutUs(){
	$result =	Cms::where('slug','about')->first();	
	$platform = Platform::where('is_active',1)->limit(6)->orderBy('updated_at','desc')->get();
	$serve = Serve::where('is_active',1)->get();
	$partner = Partner::where('is_active',1)->get();
	$userSliders = UserSlider::leftJoin('users' , 'user_slider.club_name' , '=' , 'users.id')
	->where('user_slider.is_active',1)
	->select('user_slider.*','users.club_name')
	->get();
	$teamSliders = TeamSlider::where('is_active',1)->get();
	return View::make('front.cms.aboutus' , compact('result','platform','serve','partner','userSliders','teamSliders'));
	}//end showCms()
	
/** 
 * Function to display contact us page
 *
 * @param null
 * 
 * @return view page
 */
public function contactUs(){
	Input::replace($this->arrayStripTags(Input::all()));
	$allData	=	Input::all();
	if(!empty($allData)){
		$validator = Validator::make(
			$allData['data'],
			array(
				'name' 				=> 'required',
				'email' 			=> 'required|email',
				'subject' 			=> 'required',
				'message'  			=> 'required'
			),
			array(
				'email.email' 		=> trans('Please enter valid email address.'),
			)
		);
		if ($validator->fails()){
			$response	=	array(
				'success' 	=> false,
				'errors' 	=> $validator->errors()
			);
			return Response::json($response); 
			die;
		}else{
			$date = date("Y-m-d H:i:s");
			DB::table('contact_us')->insert(
				array(
					'name'			=> $allData['data']['name'],
					'email' 		=> $allData['data']['email'],
					'subject' 		=> $allData['data']['subject'],
					'message' 		=> $allData['data']['message'],
					'created_at'   => date('Y-m-d',time()),
					'updated_at'   => date('Y-m-d',time())
				)
			);
				//send email to site admin with user information,to inform that user wants to contact
			$emailActions		=  EmailAction::where('action','=','contact_us')->get()->toArray();
			$emailTemplates		=  EmailTemplate::where('action','=','contact_us')->get()->toArray();
			$cons 				=  explode(',',$emailActions[0]['options']);
			$constants 			=  array();

			foreach($cons as $key=>$val){
				$constants[] = '{'.$val.'}';
			}
			$name				=	 $allData['data']['name'];
			$email				=	 $allData['data']['email'];
			$message			=	 $allData['data']['message'];
			$subject_data		=	 $allData['data']['subject'];

			$subject 			=  $emailTemplates[0]['subject'];
			$rep_Array 			=  array($name,$email,$subject_data,$message); 
			$messageBody		=  str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
			$settingsEmail = Config::get("Site.contact_email");
			$this->sendMail(Config::get("Site.contact_email"),'Admin',$subject,$messageBody,$settingsEmail);
			$response	=	array(
				'success' 	=>	'1',
				'errors' 	=>	trans("Thanks for connecting with us.")
			);
			Session::flash('flash_notice',  trans("Thanks for connecting with us.")); 
			return  Response::json($response); 
			die;	
		}
	}
	return View::make('front.cms.contact_us');
	}//end contactUs()

/** 
 * Function to display Subscribe page
 *
 * @param null
 * 
 * @return view page
 */
public function subscribe(){
	Input::replace($this->arrayStripTags(Input::all()));
	$allData	=	Input::all();
	if(!empty($allData)){
		$validator = Validator::make(
			$allData['data'],
			array(
				'name' 	=> 'required',
				'email' => 'required|email',
			),
			array(
				'email.email' 		=> trans('Please enter valid email address.'),
			)
		);
		if ($validator->fails()){
			$response	=	array(
				'success' 	=> false,
				'errors' 	=> $validator->errors()
			);
			return Response::json($response); 
			die;
		}else{
			$date = date("Y-m-d H:i:s");
			DB::table('subscribes')->insert(
				array(
					'name'	=> $allData['data']['name'],
					'email' => $allData['data']['email'],
					'club' 	=> $allData['data']['club'],
					'created_at' => $date,
					'updated_at' => $date
				)
			);
				//send email to site admin with user information,to inform that user wants to contact
			$emailActions		=  EmailAction::where('action','=','contact_us')->get()->toArray();
			$emailTemplates		=  EmailTemplate::where('action','=','contact_us')->get()->toArray();
			$cons 				=  explode(',',$emailActions[0]['options']);
			$constants 			=  array();

			foreach($cons as $key=>$val){
				$constants[] = '{'.$val.'}';
			}
			$name				=	 $allData['data']['name'];
			$email				=	 $allData['data']['email'];
			$message			=	 $allData['data']['club'];
			$subject_data		=	 'Subscribe';

			$subject 			=  $emailTemplates[0]['subject'];
			$rep_Array 			=  array($name,$email,$subject_data,$message); 
			$messageBody		=  str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
			$settingsEmail = Config::get("Site.contact_email");
			$this->sendMail(Config::get("Site.contact_email"),'Admin',$subject,$messageBody,$settingsEmail);
			$response	=	array(
				'success' 	=>	'1',
				'errors' 	=>	trans("We'll keep you updated on the launch of the platform in Sep 2019!")
			);
			Session::flash('flash_notice',  trans("We'll keep you updated on the launch of the platform in Sep 2019!")); 
			return  Response::json($response); 
			die;	
		}
	}
	return View::make('front.cms.contact_us');
	}//end contactUs()
	public function lobbyView($club_id = null, $game_mode = null){



/*
		$res = array_unique(array_merge(array(4,2,5,6), array(1,2,3)), SORT_REGULAR);

		dump($res); 


		$tmp_team = UserTeamPlayers::where('player_id', 69)->pluck('team_id')->toArray();  // the team ids in which this player belong

dump($tmp_team); 


if (($key = array_search(3, $tmp_team)) !== false) {
    unset($tmp_team[$key]);
}
dump($tmp_team); 
    $tmp_team = UserTeamsPlayerTrack::where('player_id', 69)
              ->where(function($q)  {
                    $q->where(function($query) {
                            $query->where('created_at', '<', Carbon::now())->whereNull('remove_date'); 
                        })
                      ->orWhere(function($query) {
                            $query->where('created_at', '<', Carbon::now())->where('remove_date', '>', Carbon::now()); 
                        });
                    })->pluck('team_id')->toArray(); // get the team ids in which this player belong at the time of fixture


dump($tmp_team); 

die; 

*/ 





		$club = new User();

		$top_player_by_gameweek = null; $top_player_by_gameweek_points = null; $top_player_by_overall = null; $top_player_by_overall_points = null; $sess_club_name_lobby = null; $clubLists_lobby = null; 

/*
		$temp = UserTeams::all(); 

		foreach ($temp as $key => $value) {
			
			
			$t_new = new UserTeamsCVCTrack;
			$t_new->team_id = $value->id; 
			$t_new->player_id = $value->capton_id; 
			$t_new->remove_date = Carbon::now(); 
			$t_new->c_vc = 1; 
			$t_new->save(); 

			$t_new = new UserTeamsCVCTrack;
			$t_new->team_id = $value->id; 
			$t_new->player_id = $value->vice_capton_id; 
			$t_new->remove_date = Carbon::now(); 
			$t_new->c_vc = 0; 
			$t_new->save(); 

		}

		dump($temp); die; 
*/
/* // old code commented 
		if(!empty($club_id) && !empty($game_mode)){

 \Session::forget('sess_club_name'); 
 \Session::put('sess_club_name', $club_id);
  \Session::forget('sess_game_mode'); 
 \Session::put('sess_game_mode', $game_mode);
  

 $clubLists_lobby = User::where(['game_mode'=>$game_mode,'is_active'=>1,'is_deleted'=>0, 'user_role_id' => 3, 'is_game_activate' => 1])->pluck('club_name','id')->all();
  $sess_club_name_lobby = User::where('id', \Session::get('sess_club_name'))->first();



    if(!empty($sess_club_name_lobby) && !empty($sess_club_name_lobby->id)){
        $brand = Branding::where('club_id', $sess_club_name_lobby->id )->first(); 
        if(!empty($brand) && !empty($brand->logo) && $brand->is_paid == 1){
            $sess_club_name_lobby->brand_image = $brand->logo; 
        }
        if(!empty($brand) && !empty($brand->url)  && $brand->is_paid == 1){
          $brand->url = strpos($brand->url, 'http') !== 0 ? "http://".$brand->url : $brand->url;
          // $brand->url = "http://" . $brand->url; 
          $sess_club_name_lobby->brand_url = $brand->url; 
        }
        // dump($sess_club_name->brand_image); 
       
    }



		}
*/ 
		// dump(Session::get('sess_club_name')); 
		if(!empty($club_id))
			Session::put('sess_club_name', $club_id);

	// dump(Session::get('sess_club_name')); 

		$club_id = Session::get('sess_club_name'); 
		// dump($club_id); die; 
		// $club_id = 31; 

		$seniorClub = $club->getClubList();
		$gameName = User::where(['club_type'=>1])
		->orderBy('club_name','ASC')
		->orderBy('created_at','DESC')
		->value('game_name');
		$upcomminFixtures = Fixture::where('start_date','>=',date("Y-m-d"))->get();
		$top_trade_in = array(); 
		$top_trade_in = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->where('in_out', 1)
		->whereHas('player', function($q) use ($club_id) {
			$q->where('club', $club_id);
		})
		->groupBy('player_id')->orderBy('total', 'DESC')->take(10)->pluck('total','player_id')->toArray();


		$top_trade_out = array(); 
		$top_trade_out = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->where('in_out', 0)
		->whereHas('player', function($q) use ($club_id) {
			$q->where('club', $club_id);
		})
		->groupBy('player_id')->orderBy('total', 'DESC')->take(10)->pluck('total','player_id')->toArray();
		// dump($top_trade_out); die; 
		// dump($top_trade_in);  
		$top_trade_in_count = $top_trade_in ; 
		$top_trade_out_count = $top_trade_out ; 
		$top_trade_in = array_keys($top_trade_in); 
		$top_trade_out = array_keys($top_trade_out); 
		$temp_top_trade_in = array(); 
		$temp_top_trade_out = array(); 
		// dump($top_trade_in_count); 
		// dump($top_trade_in);
		foreach ($top_trade_in as $key => $value) {
			$temp_top_trade_in[]=Player::where('id', $value)->first(); 
		}
		foreach ($top_trade_out as $key => $value) {
			$temp_top_trade_out[]=Player::where('id', $value)->first(); 
		}
		$top_trade_in = $temp_top_trade_in ; 
		$top_trade_out = $temp_top_trade_out ; 

		// dump($top_trade_in); die; 
		// dump($top_trade_in_count); 
		//  die; 
		// for over all start 

		$player_id_arr = Player::where('club', $club_id)->pluck('id')->toArray(); 
		// dump($player_id_arr); die; 

		$heighest_player_pts = FixtureScorcard::whereIn('player_id', $player_id_arr)->groupBy('player_id')
		->selectRaw('sum(fantasy_points) as sum, player_id')->orderBy('sum', 'DESC')
		->pluck('sum','player_id')->toArray();
		if(count($heighest_player_pts) > 0){
			$average_player_pts = array_sum($heighest_player_pts) / count($heighest_player_pts);
		}else{
			$average_player_pts = 0; 
		}
		
		$average_player_pts = round($average_player_pts) ; 
		$heighest_player_pts = !empty($heighest_player_pts) ? max($heighest_player_pts) : 0 ; 

		$total_no_of_trade = UserTeams::where('club_id', $club_id)->sum('no_of_trade'); 

		// dump($total_no_of_trade); die; 

		/* // commented as not used 11-08-19

		$booster_used = UserTeams::where('club_id', $club_id)
		->selectRaw('sum(no_of_trade) as sum_trade, sum(capton_card_count) as sum_capton, sum(twelve_man_card_count) as sum_twelve_man ')->first()->toArray();

		// dump($booster_used); die; 
		$trades_used = !empty($booster_used['sum_trade']) ? $booster_used['sum_trade'] : 0; 
		if(!empty($booster_used)){
			$booster_used = $booster_used['sum_trade'] + $booster_used['sum_capton'] + $booster_used['sum_twelve_man'] ; 
		}else{
			$booster_used = 0; 
		}
		*/ 


        // $trades_used = UserTeamLogs::where('log_type', 1)->where('club_id', $club_id)->where('user_id', auth()->guard('web')->user()->id)->count(); 
$temp_all_club_team = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 
  $trades_used = UserTeamTradeLog::whereIn('team_id', $temp_all_club_team)->sum('no_of_trade'); 


        // $booster_used = UserTeamLogs::where('log_type', '!=', 1)->where('club_id', $club_id)->where('user_id', auth()->guard('web')->user()->id)->count(); 


        $booster_used = UserTeamLogs::where('log_type', '!=', 1)->where('club_id', $club_id)->count(); 



		// dump($booster_used); die; 
		$club_team_id = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 
		
		// $most_picked_players = UserTeamPlayerLogs::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $club_team_id)
		// ->groupBy('player_id')->orderBy('total', 'DESC')
		// ->pluck('total','player_id')->toArray();

		$most_picked_players = UserTeamPlayers::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $club_team_id)->groupBy('player_id')->orderBy('total', 'DESC')
		->pluck('total','player_id')->toArray();

		/*
		// dump($most_picked_players); die; 
		$twelveth_man_arr =  UserTeams::select('twelveth_man_id', DB::raw('count(*) as total'))->where('club_id', $club_id)
		->whereNotNull('twelveth_man_id')
		->groupBy('twelveth_man_id')->orderBy('total', 'DESC')
		->pluck('total','twelveth_man_id')->toArray();
		// dump($twelveth_man_arr); 
  //       dump($most_picked_players); 
		foreach ($twelveth_man_arr as $key => $value) {
			if(isset($most_picked_players[$key])){
				$most_picked_players[$key] += $value; 
			}else{
				$most_picked_players[$key] = $value; 
			}
		}

		*/
		arsort($most_picked_players);

		// dump($most_picked_players); die; 



		$most_picked_players = array_key_first($most_picked_players) ; 
		$most_picked_players = Player::where('id', $most_picked_players)->first(); 

        // dump($most_picked_players);   die; 
		// $most_picked_capton =  UserTeams::select('capton_id', DB::raw('count(*) as total'))->where('club_id', $club_id)->whereNotNull('capton_id')
		// ->groupBy('capton_id')->orderBy('total', 'DESC')
		// ->pluck('total','capton_id')->toArray();

	// $most_picked_capton =  UserTeamPlayerLogs::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $club_team_id)
	// 			        ->where('player_type', 1)
	// 			        ->groupBy('player_id')->orderBy('total', 'DESC')
	// 			        ->pluck('total','player_id')->toArray();





		$most_picked_capton = UserTeamsCVCTrack::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $club_team_id)
		// ->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())
		->where('c_vc', 1)
		->groupBy('player_id')->orderBy('total', 'DESC')
		->pluck('total','player_id')->toArray();

		// dump($most_picked_capton); die; 

		// dump($most_picked_capton); die; 
		$most_picked_capton = array_key_first($most_picked_capton); 
		if(!empty($most_picked_capton))
			$most_picked_capton = Player::where('id', $most_picked_capton)->first(); 
		// dump($most_picked_capton); die; 
		// dump($top_trade_out); die; 

		// for over all end 


		// for game week start ***********

// !empty($user_teams_data->capton_card_last_date) && Carbon::now()->startOfWeek() <= Carbon::parse($user_teams_data->capton_card_last_date)->startOfDay() && Carbon::parse($user_teams_data->capton_card_last_date)->startOfDay() <= Carbon::now()->endOfWeek()

		$player_id_arr = Player::where('club', $club_id)->pluck('id')->toArray(); 
		// dump($player_id_arr); die; 
		
		$gameweek_heighest_player_pts = FixtureScorcard::whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())->whereIn('player_id', $player_id_arr)->groupBy('player_id')
		->selectRaw('sum(fantasy_points) as sum, player_id')->orderBy('sum', 'DESC')
		->pluck('sum','player_id')->toArray();
		// dump($heighest_player_pts); die; 
		if(count($gameweek_heighest_player_pts) > 0){
			$gameweek_average_player_pts = array_sum($gameweek_heighest_player_pts) / count($gameweek_heighest_player_pts);
		}else{
			$gameweek_average_player_pts = 0; 
		}
		
		$gameweek_average_player_pts = round($gameweek_average_player_pts) ; 
		$gameweek_heighest_player_pts = !empty($gameweek_heighest_player_pts) ? max($gameweek_heighest_player_pts) : 0 ; 

/* // commented on 11-08-19
		$gameweek_total_no_of_trade = UserTeamLogs::where('log_type', 1)->where('club_id', $club_id)->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())->count(); 



		$gameweek_booster_used = UserTeamLogs::whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->where('club_id', $club_id)->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())->count(); 

*/ 


        // $gameweek_total_no_of_trade = UserTeamLogs::where('log_type', 1)->where('club_id', $club_id)->where('user_id', auth()->guard('web')->user()->id)->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())->count(); 


		$temp_all_club_team = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 

  $gameweek_total_no_of_trade = UserTeamTradeLog::whereIn('team_id', $temp_all_club_team)->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())->sum('no_of_trade'); 

// dump($gameweek_total_no_of_trade); die; 

        // $gameweek_booster_used = UserTeamLogs::where('log_type', '!=', 1)->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->where('club_id', $club_id)->where('user_id', auth()->guard('web')->user()->id)->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())->count(); 




        $gameweek_booster_used = UserTeamLogs::where('log_type', '!=', 1)->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->where('club_id', $club_id)->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())->count(); 







		$club_team_id = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 

		// commented on 21-11-19
		// $gameweek_most_picked_players = UserTeamPlayerLogs::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $club_team_id)
		// ->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())
		// ->groupBy('player_id')->orderBy('total', 'DESC')
		// ->pluck('total','player_id')->toArray();

		$gameweek_most_picked_players = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $club_team_id)->where('in_out', 1)
		->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())
		->groupBy('player_id')->orderBy('total', 'DESC')
		->pluck('total','player_id')->toArray();

		// $gameweek_most_picked_players = UserTeamPlayers::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $club_team_id)
		// ->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())
		// ->groupBy('player_id')->orderBy('total', 'DESC')
		// ->pluck('total','player_id')->toArray();

		// dump($gameweek_most_picked_players); 



		arsort($gameweek_most_picked_players);

		$gameweek_most_picked_players = array_key_first($gameweek_most_picked_players) ; 
		 // dump($gameweek_most_picked_players); 
		$gameweek_most_picked_players = Player::where('id', $gameweek_most_picked_players)->first(); 

        // dump($gameweek_most_picked_players);   die; 


		// $gameweek_most_picked_capton = UserTeamPlayerLogs::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $club_team_id)
		// ->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())
		// ->where('player_type', 1)
		// ->groupBy('player_id')->orderBy('total', 'DESC')
		// ->pluck('total','player_id')->toArray();



		$gameweek_most_picked_capton = UserTeamsCVCTrack::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $club_team_id)
		->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())
		->where('c_vc', 1)
		->groupBy('player_id')->orderBy('total', 'DESC')
		->pluck('total','player_id')->toArray();

		// dump($gameweek_most_picked_capton); die; 

		// dump($most_picked_capton); die; 
		$gameweek_most_picked_capton = array_key_first($gameweek_most_picked_capton); 
		if(!empty($gameweek_most_picked_capton)){
			$gameweek_most_picked_capton = Player::where('id', $gameweek_most_picked_capton)->first(); 
		}else{
			$gameweek_most_picked_capton = null;
		}
		// dump($gameweek_most_picked_capton); die; 
		// dump($top_trade_out); die; 



		// for game week end  *****************


/* 
	// commented as not needed
		//whereDate('created_at', '>', Carbon::now()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->endOfWeek())->

		$player_id_arr = Player::where('club', $club_id)->pluck('id')->toArray(); 

		$top_player_by_overall = FixtureScorcard::whereIn('player_id', $player_id_arr)->groupBy('player_id')
						   ->selectRaw('sum(fantasy_points) as sum, player_id')->orderBy('sum', 'DESC')
						   ->pluck('sum','player_id')->toArray();

		$top_player_by_overall_points = $top_player_by_overall ; 

		// dump($top_player_by_gameweek); die; 

		$top_player_by_overall = array_keys($top_player_by_overall); 
		$temp = array(); 
		foreach ($top_player_by_overall as $key => $value) {
			$temp[] = Player::where('id', $value)->first(); 
		}
		// dump($top_player_by_gameweek); die; 
		// dump($temp); die; 
		$top_player_by_overall = $temp; 
		// dump(array_keys($top_player_by_gameweek)); 

*/ 


		// top player gameweek 

		$player_id_arr = Player::where('club', $club_id)->pluck('id')->toArray(); 

/*
		$top_player_by_gameweek = FixtureScorcard::whereIn('player_id', $player_id_arr)
							->whereDate('created_at', '>', Carbon::now()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->endOfWeek())
							->groupBy('player_id')
						   ->selectRaw('sum(fantasy_points) as sum, player_id')->orderBy('sum', 'DESC')
						   ->pluck('sum','player_id')->toArray();

		$top_player_by_gameweek_points = $top_player_by_gameweek ; 

		// dump($top_player_by_gameweek); die; 

		$top_player_by_gameweek = array_keys($top_player_by_gameweek); 
		$temp = array(); 
		foreach ($top_player_by_gameweek as $key => $value) {
			$temp[] = Player::where('id', $value)->first(); 
		}
		// dump($top_player_by_gameweek); die; 
		// dump($temp); die; 
		$top_player_by_gameweek = $temp; 		
*/ 

		$top_player_by_gwk_point = array(); 
		if(!empty($club_id)){
			$temp_club_data = User::where('id', $club_id)->first(); 
		}

		if(!empty($temp_club_data)){


			
			// $temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 
			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 

			// dump($temp_club_data->lockout_start_date); 
			// dump($temp_lockout_start_date) ; die; 

			
			// dump($temp_lockout_start_date->startOfWeek()); 
			// dump($temp_lockout_start_date->endOfWeek()); 
			// dump($temp_lockout_start_date->addWeek()); 
			// dump($temp_lockout_start_date->startOfWeek()); 
			// dump($temp_lockout_start_date->endOfWeek()); 
			// die; 
			$i = 1; 
			while ($temp_lockout_start_date <= Carbon::now()->endOfWeek()) {
				// $temp_var = FixtureScorcard::whereIn('player_id', $player_id_arr)
				// ->whereDate('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->whereDate('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())
				// ->groupBy('player_id')
				// 			   ->selectRaw('sum(fantasy_points) as sum, player_id')->orderBy('sum', 'DESC')
				// 			   ->pluck('sum','player_id')->toArray();




				$temp_var = FixtureScorcard::whereIn('player_id', $player_id_arr)
                        ->whereHas('fixture', function($q) use ($temp_lockout_start_date){
   							$q->where('start_date', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('start_date', '<=', $temp_lockout_start_date->copy()->endOfWeek()); 
                                 })
				               ->groupBy('player_id')
							   ->selectRaw('sum(fantasy_points) as sum, player_id')->orderBy('sum', 'DESC')
							   ->pluck('sum','player_id')->toArray();








							   $temp_lockout_start_date->addWeek(); 
// dump($temp_var); 
							   if(!empty($temp_var)){
							   	// $v2 = max($temp_var);
							   	// $k2 = array_search($v2, $temp_var);
							   	$k2 = array_key_first($temp_var) ; 
							   	$v2 = $temp_var[$k2]; 
							   	// dump($temp_var); 
							   	// dump($k2); 
							   	// dump($v2); 
							   		if(!empty($v2)){
									   	$top_player_by_gwk_point[$i]['gwk'] = $i; 
									   	$top_player_by_gwk_point[$i]['player_id'] = $k2; 
									   	$top_player_by_gwk_point[$i]['player_point'] = $v2; 	
							   		}

							   }

							   $i++; 

							}

			// dump($top_player_by_gwk_point); 
			// die; 

							foreach ($top_player_by_gwk_point as $key => $value) {
								
								$top_player_by_gwk_point[$key]['player_id']  = Player::where('id', $top_player_by_gwk_point[$key]['player_id'])->first(); 
							}

			// dump($top_player_by_gwk_point); 
			// die; 


						}

		// Player::where()
		// !empty($user_teams_data->capton_card_last_date) && Carbon::now()->startOfWeek() <= Carbon::parse($user_teams_data->capton_card_last_date)->startOfDay() && Carbon::parse($user_teams_data->capton_card_last_date)->startOfDay() <= Carbon::now()->endOfWeek() 
						$not_available_player_arr = Availability::where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())->pluck('player')->toArray(); 

		// dump($not_available_player_arr); die; 

						$available_players_in_club = Player::where('club', $club_id)->whereNotIn('id', $not_available_player_arr)->get(); 

		// dump($available_players_in_club); die; 





    // ************************** 


						$club_id = \Session::get('sess_club_name');
						$club = new User();
						$seniorClub = $club->getClubList();
						$gameName = User::where(['club_type'=>1])
						->orderBy('club_name','ASC')
						->orderBy('created_at','DESC')
						->value('game_name');

						$sortBy=$query_string ='';

						$sort_by_data = Input::get("sortBy");
						$search       = Input::get("search");
						$sortBy       = (Input::get('sortBy')) ? Input::get('sortBy') : 'id';
						$order        = (Input::get('order')) ? Input::get('order')   : 'DESC'; 

						if(!empty($club_id)){
							$avail_data = Availability::where('is_active',1)->where('date_till', '>=', Carbon::now())->where('club', $club_id)
				// ->where("availabilities.player_name",'like','%'.$search.'%')
				// ->whereHas('player_data', function($q) use ($search){
				//     $q->where('first_name', 'like', '%'.$search.'%');
				// })
							->with('player_data')
							->orderBy($sortBy,$order)
							->get();

						}else{
		// $avail_data = Availability::where('is_active',1)
		// 		// ->where("availabilities.player_name",'like','%'.$search.'%')
		// 		// ->whereHas('player_data', function($q) use ($search){
		// 		//     $q->where('first_name', 'like', '%'.$search.'%');
		// 		// })
		// 		->with('player_data')
		// 		->orderBy($sortBy,$order)
		// 		->get();
							$avail_data = [] ; 

						}


	// *************** fixtures by game week start *****************


		// $DB 					= 	FixtureScorcard::query();
		// if(!empty($club_id))
		// 	$DB->where('fixtures.club', $club_id); 

// whereDate('created_at', '>', Carbon::now()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->endOfWeek())
	    // Carbon::now()->subDays(30)
	    // $club_id = null; 
						if(!empty($club_id)){

			// $fixture_of_week = Fixture::whereBetween('created_at',  [Carbon::now()->subDays(7)->startOfWeek() , Carbon::now()->subDays(7)->endOfWeek()] )->where('club', $club_id)->with('fixture_scorecard')->with('teamdata')->get(); 

							$fixture_of_week = Fixture::whereDate('end_date',  '>=', Carbon::now()->startOfDay() )->whereDate('start_date', '<=', Carbon::now()->endOfWeek())->where('club', $club_id)->with('fixture_scorecard')->with('teamdata')->get(); 


			// dump($fixture_of_week); die; 
						}else{
			// $fixture_of_week = Fixture::whereBetween('created_at',  [Carbon::now()->subDays(7)->startOfWeek() , Carbon::now()->subDays(7)->endOfWeek()] )->with('fixture_scorecard')->with('teamdata')->get(); 
			// dump($fixture_of_week); die; 
							$fixture_of_week = null ; 
						}



	    // dump($fixture_of_week); die; 


	// ******************* fixtures by game week end ******************
						$club_data  = User::where('id', $club_id)->first(); 

	    // dump($club_data); die; 
	    // dump($club_id); die; 
						$all_teams = UserTeams::with(['getTeamPlayer', 'getTeamPlayer.players' ])->where('club_id', $club_id)->where('is_active', 1)->get(); 

	    // dump($all_teams); die; 
						$all_team_price = array(); 
						$all_team_name  = array(); 
						foreach ($all_teams as $key => $value) { 
							// dump($value); die; 
	    	// dump($value->getTeamPlayer); die;
	    	// $all_team_price[$value] = 
							$all_team_price[$value->id] = 0 ; 
	    	// $all_team_price[$value->id]['name'] = $value-> ; 
	    	// echo $value->my_team_name; die; 
							$all_team_name[$value->id] = 'Anonymous Team'; 
							if(!empty($value->my_team_name)){
								$all_team_name[$value->id] = $value->my_team_name ; 
							}else if(User::where('id', $value->user_id)->exists() && !empty(User::where('id', $value->user_id)->first()->my_team_name)){
								$all_team_name[$value->id] = User::where('id', $value->user_id)->first()->my_team_name ; 
							}else{
								$all_team_name[$value->id] = 'Anonymous Team'; 
							}

							// dump($value->getTeamPlayer); die; 

							if($value->getTeamPlayer->isNotEmpty()){
								foreach ($value->getTeamPlayer as $k => $v) {
	    			// dump($v->players->svalue); die; 
	    			// dump($v->players);  
									// dump($v->players); 
									$all_team_price[$value->id] = $all_team_price[$value->id] + (!empty($v->players->svalue) ? $v->players->svalue : 0); 
								}

								// dump($all_team_price); die; 

							}
						}
// die; 
	    // dump($all_team_name);  
	    // dump($all_team_price); 

						arsort($all_team_price); 
	    // dump($all_team_price); 

		$temp_atp = array(); 
	    foreach ($all_team_price as $key => $value) {
	    	// code commented as it gets the initial value of team
	    	// $utvl = UserTeamValueLog::where('team_id', $key)->orderBy('id', 'DESC')->pluck('team_value', 'id')->toArray(); 




	    	// dump($utvl);

	    	// dump(array_pop($utvl)); 
	    	 // die; 
	    	// echo $key; die; 
	    	// $temp_atp[$key]['old'] = !empty($utvl) ? array_pop($utvl) : $value ; 

	    	 $utvl = UserTeamValueLog::where('team_id', $key)->max('team_value'); 
	    	 $temp_atp[$key]['old'] = !empty($utvl) && ($utvl > $value) ? $utvl : $value ; 
	    	$temp_atp[$key]['current'] = $value; 

	    }
	    $all_team_price = $temp_atp ; 
	    // dump($all_team_name); 
	    // die ;
	    // dump($all_team_price); die; 

						$is_fundraiser_activate = GameSetting::where('club_id',$club_id)->count();

	    // $most_run $most_wicket $most_points $most_four $most_sixes

						$all_player = Player::where('club', $club_id)->pluck('id')->toArray(); 
	    $all_player_stat_data = FixtureScorcard::with('player')->whereIn('player_id', $all_player)->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum_fp, sum(rs) as sum_rs,  sum(fours) as sum_fours, sum(sixes) as sum_sixes, sum(wks) as sum_wks, sum(mdns) as sum_mdns, sum(hattrick) as sum_hattrick, sum(cs) as sum_cs, sum(sts) as sum_sts, sum(overs) as sum_overs, player_id')->get(); // ->pluck('sum_fp', 'sum_rs', 'sum_fours', 'sum_sixes', 'sum_wks', 'sum_mdns', 'sum_hattrick', 'sum_cs', 'sum_sts', 'sum_overs', 'player_id')->toArray(); 
	    $temp = array('sum_fp' => 0, 'sum_rs' => 0, 'sum_fours' => 0, 'sum_sixes' => 0, 'sum_wks' => 0, 'sum_mdns' => 0, 'sum_hattrick' => 0, 'sum_cs' => '0', 'sum_sts' => 0, 'sum_overs' => 0); 
	    // dump($all_player_stat_data); die; 
	    // dump($all_player_stat_data->max('sum_fp')); 
	    $sum_fp = $all_player_stat_data->max('sum_fp'); 
	    $sum_rs = $all_player_stat_data->max('sum_rs'); 
	    $sum_fours = $all_player_stat_data->max('sum_fours'); 
	    $sum_sixes = $all_player_stat_data->max('sum_sixes'); 
	    $sum_wks = $all_player_stat_data->max('sum_wks'); 
	    $sum_mdns = $all_player_stat_data->max('sum_mdns'); 
	    $sum_hattrick = $all_player_stat_data->max('sum_hattrick'); 
	    $sum_cs = $all_player_stat_data->max('sum_cs'); 
	    $sum_sts = $all_player_stat_data->max('sum_sts'); 
	    $sum_overs = $all_player_stat_data->max('sum_overs'); 

	    // dump($sum_fp); die; 
	    // dump($sum_fp); dump($sum_rs);  dump($sum_fours);  dump($sum_sixes);  dump($sum_wks);  dump($sum_mdns);  dump($sum_hattrick);  dump($sum_cs);  dump($sum_sts);  dump($sum_overs); 


	    $filtered_collection = $all_player_stat_data->filter(function ($item) use(&$temp, $sum_fp, $sum_rs, $sum_fours, $sum_sixes, $sum_wks, $sum_mdns, $sum_hattrick, $sum_cs, $sum_sts, $sum_overs) {

		// dump($item->player); die; 

	    	if($item->sum_fp == $sum_fp){
	    		$temp['sum_fp'] = $item; 
	    	}
	    	if($item->sum_rs == $sum_rs){
	    		$temp['sum_rs'] = $item; 

	    	}
	    	if($item->sum_fours == $sum_fours){
	    		$temp['sum_fours'] = $item; 

	    	}
	    	if($item->sum_sixes == $sum_sixes){
	    		$temp['sum_sixes'] = $item; 

	    	}
	    	if($item->sum_wks == $sum_wks){
	    		$temp['sum_wks'] = $item; 

	    	}
	    	if($item->sum_mdns == $sum_mdns){
	    		$temp['sum_mdns'] = $item; 

	    	}
	    	if($item->sum_hattrick == $sum_hattrick){
	    		$temp['sum_hattrick'] = $item; 

	    	}
	    	if($item->sum_cs == $sum_cs){
	    		$temp['sum_cs'] = $item; 

	    	}
	    	if($item->sum_sts == $sum_sts){
	    		$temp['sum_sts'] = $item; 

	    	}
	    	if($item->sum_overs == $sum_overs){
	    		$temp['sum_overs'] = $item; 

	    	}

	    	return true;
	    })->values();

		// dump($temp); die;
	    $all_player_stat_data = $temp ; 

	    $i = 0; 
	    $j = 0; 
	    foreach ($all_player_stat_data as $key => $value) {
	    	$i++; 
	    	if(empty($value)){
	    		$j++; 
	    	}

	    }

	    if($i == $j){
	    	$all_player_stat_data = array(); 
	    }


		// dump(array_sum($all_player_stat_data)); die; 

	    // sum(rs) as sum_rs      sum(fours) as sum_fours        sum(sixes) as sum_sixes       sum(wks) as sum_wks      sum(mdns) as sum_mdns         sum(hattrick) as sum_hattrick      sum(cs) as sum_cs     sum(sts) as sum_sts   
	    
// RS -> Runs, 4S -> Fours, 6S -> Sixes, Ovrs -> Overs, MDNS -> Maiden, WKS -> Wickets, CS -> Catches, STS -> Stump, HT -> Hat-Trick

	    if(!empty($club_data->lockout_start_date) && Carbon::parse($club_data->lockout_start_date) > Carbon::now()){
				$cur_gameweek_no = ''; 
		}elseif(!empty($club_data->lockout_start_date)){
	    	$cur_gameweek_no = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now());
	    	$cur_gameweek_no++;
	    }else{
	    	$cur_gameweek_no = 1; 
	    }
			// dump($club_data); die; 



        $match_type = DropDown::where('dropdown_type', 'matchtype')->pluck('name', 'id')->toArray(); 

        // dump($match_type); die; 


	    return View::make('front.user.lobby',compact('seniorClub','gameName','upcomminFixtures', 'top_trade_in', 'top_trade_out', 'heighest_player_pts', 'average_player_pts', 'trades_used', 'booster_used', 'most_picked_players', 'most_picked_capton', 'top_player_by_gameweek', 'top_player_by_gameweek_points', 'top_player_by_overall', 'top_player_by_overall_points', 'available_players_in_club', 'gameweek_heighest_player_pts',  'gameweek_average_player_pts',  'gameweek_total_no_of_trade',   'gameweek_booster_used',  'gameweek_most_picked_players', 'gameweek_most_picked_capton', 'avail_data', 'fixture_of_week', 'club_data', 'all_team_name', 'all_team_price','is_fundraiser_activate', 'all_player_stat_data', 'cur_gameweek_no', 'top_player_by_gwk_point', 'top_trade_in_count', 'top_trade_out_count', 'sess_club_name_lobby', 'clubLists_lobby', 'match_type'));




	}

 

	public function myTeam(Request $request, $club_id = null, $game_mode = null ){



// $utl_d_d = UserTeamLogs::where(['team_id' => 3, 'log_type' => 1, 'dealer_card_status' => 0])->whereBetween('created_at',  [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()] )->first();
// $utl_d_d->shield_steal_status = 1; 
// $utl_d_d->save(); 
// dump($utl_d_d); 
// die; 

		// UserTeamLogs::where('id', 811)->update(['steal_team_id'=> -50]);
		// $tmp = UserTeamLogs::find(811);
		// $tmp->steal_team_id += -10; 
		// $tmp->save(); 

		// die; 


$remain_lock_out_time = null; $sess_club_name_lobby = null; $clubLists_lobby = null; 

// date_default_timezone_set('Asia/Kolkata');
		// dump(Auth::user()); die; 
		// dump(auth()->guard('web')->user()); die; 	
		// dump(Carbon::now()->isoFormat('dddd')); die; 
		// dump(Carbon::now()->isoFormat('H'));  
		// dump(Carbon::now()->isoFormat('m')); die; 



/*
$all_team = UserTeams::get(); 
// dump($all_team); die; 
foreach ($all_team as $key => $value) {
    $all_player = UserTeamPlayers::where('team_id', $value->id)->get();
    // dump($all_player); die; 
    foreach ($all_player as $k1 => $v1) {
        $temp = new UserTeamsPlayerTrack; 
        $temp->player_id = $v1->player_id; 
        $temp->team_id = $v1->team_id; 
        $temp->created_at = Carbon::parse($v1->created_at); 
        $temp->save();
    }

}
die; 

*/ 



if(!empty($club_id) && !empty($game_mode)){

	\Session::forget('sess_club_name'); 
	\Session::put('sess_club_name', $club_id);
	\Session::forget('sess_game_mode'); 
	\Session::put('sess_game_mode', $game_mode);


	$clubLists_lobby = User::where(['game_mode'=>$game_mode,'is_active'=>1,'is_deleted'=>0, 'user_role_id' => 3, 'is_game_activate' => 1])->orderBy('club_name', 'ASC')->pluck('club_name','id')->all();
	$sess_club_name_lobby = User::where('id', \Session::get('sess_club_name'))->first();



	if(!empty($sess_club_name_lobby) && !empty($sess_club_name_lobby->id)){
		$brand = Branding::where('club_id', $sess_club_name_lobby->id )->first(); 
		if(!empty($brand) && !empty($brand->logo) && $brand->is_paid == 1){
			$sess_club_name_lobby->brand_image = $brand->logo; 
		}
		if(!empty($brand) && !empty($brand->url)  && $brand->is_paid == 1){
			$brand->url = strpos($brand->url, 'http') !== 0 ? "http://".$brand->url : $brand->url;
          // $brand->url = "http://" . $brand->url; 
			$sess_club_name_lobby->brand_url = $brand->url; 
		}
        // dump($sess_club_name->brand_image); 

	}



}

	if(empty($club_id))
     	$club_id = \Session::get('sess_club_name');

$drop_down =	new DropDown();
$position =	$drop_down->get_master_list("position");
	    // dump($position);
$position = array_replace(array_flip(array('4', '1', '3', '2')), $position);
	    // dump($position); die;
$category =	$drop_down->get_master_list("category");
$svalue	=	$drop_down->get_master_list("svalue");
$svalue = Config::get('player_price_list'); 
$jvalue =	$drop_down->get_master_list("jvalue");
$capton_id = 0; 
$vice_capton_id = 0; 
$my_team_name = ''; 
$userteam_fantasy_points = array() ; 
$gameweek_fantasy_points = array() ; 
$club_name = Session::get('sess_club_name'); 
	    // print_r($club_name); 
$club_data = User::where('id', $club_name)->first(); 
$game_point_data = GamePoint::where('club', $club_name)->get(); 
	    // dump($club_data); die; 
	    // dump($club_name); 
	    // dump($game_point_data);  

$players =  Player::where('club', $club_name)->where('full_name','!=','')->where('is_active', 1)->get(); 
$chosen_player = []; 
$user_teams = UserTeams::where('user_id', auth()->guard('web')->user()->id)->where('club_id', $club_name)->first();
		// dump($user_teams); die; 

// dump(Carbon::now()->startOfWeek()); 
    	// dump($user_teams); 
if(!empty($user_teams->twelve_man_card_last_date) && Carbon::parse($user_teams->twelve_man_card_last_date)->endOfWeek() < Carbon::now()->startOfWeek()){

	// get the point of 12th man and store in extra point 

	$ut_tmp = UserTeams::where('id', $user_teams->id)->first(); 

	if(!empty($ut_tmp->twelveth_man_id)){

        // Delete 12th man player from User team player table after the 12th man power expires

         UserTeamPlayers::where('team_id', $user_teams->id)->where('player_id', $ut_tmp->twelveth_man_id)->delete(); 
         UserTeamsPlayerTrack::where('team_id', $user_teams->id)->where('player_id', $ut_tmp->twelveth_man_id)->update(['remove_date' => Carbon::now()]); 

// dump($ut_tmp->twelveth_man_id); 

                        $fspnt = FixtureScorcard::where('player_id', $ut_tmp->twelveth_man_id)

                        ->whereHas('fixture', function($q) use ($user_teams){
   							$q->where('start_date', '>=', Carbon::parse($user_teams->twelve_man_card_last_date))->where('start_date', '>=', Carbon::parse($user_teams->twelve_man_card_last_date)->startOfWeek())->where('start_date', '<=', Carbon::parse($user_teams->twelve_man_card_last_date)->endOfWeek()); 


                                 })

                        // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())

                        ->sum('fantasy_points'); // ->where('status', 3)

                      $ut_tmp->extra_team_point += $fspnt ; 

                      $ut_tmp->twelveth_man_id = null; 
                      $ut_tmp->twelve_man_card_last_date = null; 

                      $ut_tmp->save(); 
     // dump($fspnt); die; 


	}

	
	UserTeams::where('id', $user_teams->id)->update(['twelveth_man_id' => NULL, 'twelve_man_card_last_date' => NULL]);

}
if(!empty($user_teams->capton_card_last_date) && Carbon::parse($user_teams->capton_card_last_date)->endOfWeek() < Carbon::now()->startOfWeek()){
	
	UserTeams::where('id', $user_teams->id)->update([ 'capton_card_last_date' => NULL]);

}
		// dump($user_teams); die; 

$user_teams_data = $user_teams ; 

// **********************************************************

/* 
	//  	->whereHas('fixture', function($q) use ($user_terms){
	// 							$q->where('start_date', '>', Carbon::parse($user_terms->created_at)->endOfDay());
	// 						})
	dump($user_teams_data->id); 
	$a = 111; 
	$b = 'hello'; 
	$ttemp = 	UserTeamPlayers::whereHas('teams', function($q) use ($user_teams_data, $a, $b){
		dump($user_teams_data); dump($a); dump($b);  die; 
								$q->where('team_id',  $user_teams_data->id);
							})
						->get(); 
	dump($ttemp); die; 
*/ 
// *****************************************************






		// dump($user_teams_data); die; 
	$no_of_trade = 0; 
	if ($user_teams) {
		$capton_id = $user_teams->capton_id; 
		$vice_capton_id = $user_teams->vice_capton_id; 
		$my_team_name = $user_teams->my_team_name;
		$no_of_trade = $user_teams->no_of_trade; 
			// $club_name =  $user_teams->club_id;
		$user_teams = $user_teams->toArray();
	}else{
		$user_teams = [];
	}
	if(!empty($user_teams)){

		// dump($user_teams); die; 
		$team_creation_date = $user_teams['created_at'] ; 
			// dump($user_teams);  die; 
		$capton_id = $user_teams['capton_id']; 
		$vice_capton_id = $user_teams['vice_capton_id']; 
		$twelveth_man_id = $user_teams['twelveth_man_id'] ; 
		// dump($twelveth_man_id); 
		$user_teams = UserTeamPlayers::where('team_id', $user_teams['id'])->pluck('player_id')->toArray();
			// dump($user_teams);
		// if(!empty($twelveth_man_id)){
		// 		// $user_teams[] = $twelveth_man_id; 
		// 	array_push($user_teams, $twelveth_man_id);
		// }


		// dump($user_teams);
		// dump($user_teams_data->twelveth_man_id); 
        if(!empty($user_teams_data->twelve_man_card_last_date) && Carbon::parse($user_teams_data->twelve_man_card_last_date) >= Carbon::now()->startOfWeek() && Carbon::parse($user_teams_data->twelve_man_card_last_date) <= Carbon::now()->endOfWeek() &&  !empty($twelveth_man_id)){

        	// $user_teams[] =  $user_teams_data->twelveth_man_id; 
        	array_push($user_teams, $twelveth_man_id);
        }

			// dump($user_teams); die; 
/*
			$userteam_fantasy_points = FixtureScorcard::whereIn('player_id', $user_teams)->whereDate('created_at', '>=', $team_creation_date)->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray(); 

			// dump($userteam_fantasy_points); die; 

			if(isset($userteam_fantasy_points[$capton_id])){

				 if(!empty($user_teams_data->capton_card_last_date) && Carbon::now()->startOfWeek() <= Carbon::parse($user_teams_data->capton_card_last_date)->startOfDay() && Carbon::parse($user_teams_data->capton_card_last_date)->startOfDay() <= Carbon::now()->endOfWeek() ){
			       
				 	$userteam_fantasy_points[$capton_id] = $userteam_fantasy_points[$capton_id] * 3; 

			      }else{
			      	$userteam_fantasy_points[$capton_id] = $userteam_fantasy_points[$capton_id] * 2; 
			      }

				
			}
			if(isset($userteam_fantasy_points[$vice_capton_id])){
				$userteam_fantasy_points[$vice_capton_id] = round($userteam_fantasy_points[$vice_capton_id] * 1.5); 
			}

			$userteam_fantasy_points = array_sum($userteam_fantasy_points); 

*/ 

			// get start of week date 

			// print_r(Carbon::now()->startOfWeek()); print_r(Carbon::now()->endOfWeek()); die; 

		/*  // old code not in use according to new requirement 

			$gameweek_fantasy_points = FixtureScorcard::whereIn('player_id', $user_teams)->whereDate('created_at', '>=', $team_creation_date)->whereDate('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray(); 
			// dump($gameweek_fantasy_points); die; 
			if(isset($gameweek_fantasy_points[$capton_id])){

				 if(!empty($user_teams_data->capton_card_last_date) && Carbon::now()->startOfWeek() <= Carbon::parse($user_teams_data->capton_card_last_date)->startOfDay() && Carbon::parse($user_teams_data->capton_card_last_date)->startOfDay() <= Carbon::now()->endOfWeek() ){
			       
				 	$gameweek_fantasy_points[$capton_id] = $gameweek_fantasy_points[$capton_id] * 3; 

			      }else{

						$gameweek_fantasy_points[$capton_id] = $gameweek_fantasy_points[$capton_id] * 2; 
				}
			}
			if(isset($gameweek_fantasy_points[$vice_capton_id])){
				$gameweek_fantasy_points[$vice_capton_id] = round($gameweek_fantasy_points[$vice_capton_id] * 1.5); 
			}
			
			$gameweek_fantasy_points = array_sum($gameweek_fantasy_points); 


		*/ 


			// dump($user_teams_data->id); 
/* // commented on 14/02/20 , now we are not calculating gameweek on the fly 
			$user_team_player = UserTeamPlayers::where('team_id', $user_teams_data->id)->get(); 
			// dump($user_team_player); 
			$tmp_top_pnt = 0 ; 
			foreach ($user_team_player as $k1 => $v1) {

				$vtemp = UserTeamsPlayerTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->whereNull('remove_date')->first(); 

				if(!empty($vtemp)){
					$v1 = $vtemp ; 
				}else{
					$utp_vtemp = UserTeamPlayers::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->first(); 
					if(!empty($utp_vtemp)){
						$v1 = $utp_vtemp; 
					}

				}


                    // dump($v1->player_id); 
				if($v1->player_id == $user_teams_data->capton_id){

					$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 1)->whereNull('remove_date')->first(); 
					if(!empty($utcvct)){
						$tmp_strt_dt = $utcvct->created_at ; 
					}else{
                            // echo 'hi1'; die; 
						$tmp_strt_dt = $v1->created_at ; 
					}
                        // echo 'hi'; 
                        // dump(Carbon::parse($tmp_strt_dt)->endOfDay()); 

                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)



                        $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

                        ->whereHas('fixture', function($q) use ($tmp_strt_dt){
   							$q->where('start_date', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('start_date', '>=', Carbon::now()->subWeek()->startOfWeek())->where('start_date', '<=', Carbon::now()->subWeek()->endOfWeek()); 


                                 })

                        // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())

                        ->sum('fantasy_points'); // ->where('status', 3)



    // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)
    //                    ->whereHas('fixture', function($q) use ($tmp_strt_dt){
    //                                  $q->where('start_date', '>', Carbon::parse($tmp_strt_dt)->endOfDay());
    //                              })
    // ->sum('fantasy_points'); // ->where('status', 3)








                        // dump($tmp_pnt); 
                        // echo 'hello'; 
                        $tmp_top_pnt += round($tmp_pnt) ; 
                    }


                    if($v1->player_id == $user_teams_data->vice_capton_id){


                    	$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
                    	if(!empty($utcvct)){
                    		$tmp_strt_dt = $utcvct->created_at ; 
                    	}else{
                            // echo 'hi2'; die; 
                    		$tmp_strt_dt = $v1->created_at ; 
                    	}




                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)


    	             $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

    	             ->whereHas('fixture', function($q) use ($tmp_strt_dt){
							$q->where('start_date', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('start_date', '>=', Carbon::now()->subWeek()->startOfWeek())->where('start_date', '<=', Carbon::now()->subWeek()->endOfWeek()); 

                                 })

    	             // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())


    	             ->sum('fantasy_points'); // ->where('status', 3)



 











                        $tmp_top_pnt += round($tmp_pnt * 0.5) ; 
                    }



                        // $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)



                        $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)


					->whereHas('fixture', function($q) use ($v1){

						$q->where('start_date', '>=', Carbon::parse($v1->created_at)->endOfDay())->where('start_date', '>=', Carbon::now()->subWeek()->startOfWeek())->where('start_date', '<=', Carbon::now()->subWeek()->endOfWeek()); 


					                                 })


                        // ->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())


                        ->sum('fantasy_points'); // ->where('status', 3)



                        $tmp_top_pnt += $tmp_pnt ; 



                    }

			// die; 

                    $gw_extra_pt = 0 ; 

                    if(UserTeamsGWExtraPTTrack::where(['team_id' => $user_teams_data->id, 'gw_end_date' => Carbon::now()->subWeek()->endOfWeek()->endOfDay() ])->exists()){

                    	$tmp = UserTeamsGWExtraPTTrack::where('team_id', $user_teams_data->id)->where('gw_end_date', Carbon::now()->subWeek()->endOfWeek()->endOfDay())->first(); 
				// dump($tmp); 

                    		$gw_extra_pt = round($tmp->gw_extra_pt)  ; 

                    		// dump($gw_extra_pt); 


                    }


                    if(UserTeamLogs::where('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<', Carbon::now()->subWeek()->endOfWeek())->where('team_id', $user_teams_data->id)->where('log_type', 4)->exists()){

                    	$tmp_top_pnt = $tmp_top_pnt - $user_teams_data->dealer_card_minus_points; // subtract 50 points if dealer card is active in this game week 

                    	// dump($tmp_top_pnt); die; 

                    }


*/ 

            // $temp_last_wk_pt[$value->id] = $tmp_top_pnt; 

                    // $gameweek_fantasy_points =  $tmp_top_pnt + $gw_extra_pt; 

                    // $gameweek_fantasy_points = $gameweek_fantasy_points > 0 ? $gameweek_fantasy_points : 0 ; 





			// dump($userteam_fantasy_points); 
			// dump($gameweek_fantasy_points); 
			// die; 


			// get end of week date


        if(!empty($club_id)){	// add dummy player to existing chosen player 
        	$dummy_player = Player::where('club', $club_id)->where('dummy_player', 1)->where('is_active', 1)->pluck('id')->toArray(); 
        	// dump($dummy_player);  die; 
        	// $dummy_player = [999, 991]; 
        	if(!empty($dummy_player) && is_array($dummy_player) && is_array($user_teams)){
        		$user_teams = array_merge($user_teams, $dummy_player); 
        	}

        }

        // dump($user_teams); 


        // $user_teams[] = 1111; 
        // dump($user_teams); die; 
                    

			// $user_teams = unserialize($user_teams['player_ids']); 
                    $chosen_player = Player::whereIn('id', $user_teams)->get();

                }


		// dump(Carbon::now()->isoFormat('dddd')); die; 
		// dump(Carbon::now()->isoFormat('H'));  
		// dump(Carbon::now()->isoFormat('m')); die; 
		// dump($club_data); die; 
		// dump($user_teams); die; 

    // "lockout_end_date" => "2019-09-11 00:00:00"
    // "lockout_start_day" => "wednusday"
    // "lockout_end_day" => "monday"
    // "lockout_start_time" => "21:15"
    // "lockout_end_time" => "18:15"
                $temp_day_arr = array(	'sunday' => 0,
                	'monday' => 1,
                	'tuesday' => 2,
                	'wednesday' => 3,
                	'thursday' => 4,
                	'friday' => 5,
                	'saturday' => 6);



                $flg = false; 
                $i = 0; 
		// dump($club_data); 
                if(!empty($club_data)){


                	foreach ($temp_day_arr as $key => $value) {
                		if(strtolower($club_data->lockout_start_day) == $key) $flg = true; 
                		if($flg){
                			$temp_day_arr[$key] = $i++; 
                		}
                	}
                	foreach ($temp_day_arr as $key => $value) {
                		if(strtolower($club_data->lockout_start_day) == $key) break; 
                		$temp_day_arr[$key] = $i++; 
                	}


                }

                $lockout_club = false; 


                if(!empty($club_data->lockout_start_day) && !empty($club_data->lockout_end_day) && !empty($club_data->lockout_start_time) && !empty($club_data->lockout_end_time)){


                	$club_data->lockout_start_time = explode(':', $club_data->lockout_start_time);
                	$club_data->lockout_end_time = explode(':', $club_data->lockout_end_time);

		// dump($club_data->lockout_start_time); 
		// dump(Carbon::now()->isoFormat('H'));
// dump($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))]);

// dump($temp_day_arr[strtolower($club_data->lockout_start_day)]); 
//  die; 

                	if($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] > $temp_day_arr[strtolower($club_data->lockout_start_day)] && 
                		$temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] < $temp_day_arr[strtolower($club_data->lockout_end_day)]  
                	){
			// echo 'hi'; die; 
                		$lockout_club = true; 	
                }else if(
                	($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] == $temp_day_arr[strtolower($club_data->lockout_start_day)]  ) && 
                	(Carbon::now()->isoFormat('H') > $club_data->lockout_start_time[0] || (Carbon::now()->isoFormat('H') == $club_data->lockout_start_time[0] && Carbon::now()->isoFormat('m') > $club_data->lockout_start_time[1])) 
                ){
			// echo 'hello'; die; 
                	$lockout_club = true; 
                }else if(
                	($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] == $temp_day_arr[strtolower($club_data->lockout_end_day)] ) && 
                	(Carbon::now()->isoFormat('H') < $club_data->lockout_end_time[0] || (Carbon::now()->isoFormat('H') == $club_data->lockout_end_time[0] && Carbon::now()->isoFormat('m') < $club_data->lockout_end_time[1]))
                ){
			// echo 'hihihihh'; die; 
                	$lockout_club = true; 
                }

            }


// dump($lockout_club); 

            if(!empty($club_data->lockout_start_date) && !(Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay() &&   Carbon::now()->startOfDay() <= Carbon::parse($club_data->lockout_end_date)->startOfDay()) && $lockout_club == true ){
            	$lockout_club = false; 
            }

            if(!empty($club_data->lockout_start_date) && (Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_date)->startOfDay())  && $lockout_club == true ){
            	$lockout_club = false; 
            }


  // dump($club_data->lockout_start_date); 
  // dump(Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay()); 
  // dump(Carbon::now()->startOfDay() <= Carbon::parse($club_data->lockout_end_date)->startOfDay()); 
  // dump($lockout_club); 
  // die; 


            $lockout_time_remaining_seconds = 0; 
// dump($club_data->lockout_start_date); die; 
  	// dump(Carbon::now()->next('Sunday')); die; 


            if(!$lockout_club && !empty($club_data->lockout_start_date) ||  (!empty($club_data->is_lockout) && $club_data->is_lockout == 2) ){
            	$now = Carbon::now();

            	if(!empty($club_data->is_lockout) && $club_data->is_lockout == 2){
 	// echo 'hi'; die; 
            		$emitted = Carbon::now()->subDay()->next($club_data->lockout_start_day)->startOfDay(); 

            	}elseif(Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_date)->startOfDay()){
  		// dump(Carbon::parse($club_data->lockout_start_date)->dayName); 
  		// strtolower($club_data->lockout_start_day) == strtolower(Carbon::parse($club_data->lockout_start_date)->dayName) || 
            		if(	Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_end_day) < Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)

            	){
            			$emitted = Carbon::parse($club_data->lockout_start_date)->startOfDay();
            	}else{
            		$emitted = Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)->startOfDay();
            	}
  		// dump($club_data->lockout_start_day); die; 
  		// $emitted = Carbon::now()->next($club_data->lockout_start_day)->startOfDay();
            }elseif(Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_day)->startOfDay()){
            	$emitted = Carbon::now()->next($club_data->lockout_start_day)->startOfDay();
            }else{
            	$emitted = Carbon::now()->startOfDay();
            }
	  	// $emitted = Carbon::now()->next('wednesday');


            $temp_start_time = Carbon::parse('1970-01-01 00:00:00', 'GMT') ; 
		// dump($temp_start_time); 
            $seconds = $temp_start_time->diffInSeconds($emitted); 

  	// dump($seconds); die; 
            $seconds = $seconds + (!empty($club_data->lockout_start_time[0]) ? $club_data->lockout_start_time[0] : 0) * 3600 + (!empty($club_data->lockout_start_time[1]) ? $club_data->lockout_start_time[1] : 0) * 60 ; 
            $seconds = $seconds + 60; 
            $lockout_time_remaining_seconds = $seconds ; 

    // $days = intval(intval($seconds) / (3600*24));


    // $hours = (intval($seconds) / 3600) % 24;


    // $minutes = (intval($seconds) / 60) % 60;

    // dump($days); 
    // dump($hours); 
    // dump($minutes); 
    // die; 
    // $remain_lock_out_time = array('d' => $days , 'h' => $hours , 'm' => $minutes); 

	// dump($temp); die; 



        }


	// dump($lockout_club); die; 

        $twelve_player_select = 0; 
        if(!empty($user_teams_data->twelve_man_card_last_date) && Carbon::now()->startOfWeek() <= Carbon::parse($user_teams_data->twelve_man_card_last_date)->startOfDay() && Carbon::parse($user_teams_data->twelve_man_card_last_date)->startOfDay() <= Carbon::now()->endOfWeek() ){

        	$twelve_player_select = 1; 
        }



        $tot_club_users = 0; 

// rank calc

        $club_id = \Session::get('sess_club_name');
        if(!empty($club_id)){
		// $result = UserTeams::leftJoin('users','users.id','=','user_teams.user_id')->where('club_id', $club_id)
		// 			//->where('user_teams.club_id', $club_id)
		// 			//->leftJoin('')
		// 			->with('getTeamPlayer')
		// 			->select('user_teams.*','users.first_name','users.club_logo')
		// 			->get();

 		/*
        	$user_teams_dt = UserTeams::where('club_id', $club_id)->get(); 



        	$temp = array(); 
		// $temp_last_wk_pt = array(); 
		// dump($user_teams_dt); die; 
        	foreach ($user_teams_dt as $key => $value) {


       

		// dump($value); die; 
				// $value = UserTeams::where('id', 25)->first(); 
				$user_team_player = UserTeamPlayers::where('team_id', $value->id)->get(); 

				// dump($value->id); 
				// dump($user_team_player); 
				// dump($value->capton_id); 
				// dump($user_team_player); die; 
				$tmp_top_pnt = 0 ; 
				foreach ($user_team_player as $k1 => $v1) {



					$vtemp = UserTeamsPlayerTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->whereNull('remove_date')->first(); 

					if(!empty($vtemp)){
						$v1 = $vtemp ; 
					}else{
						$utp_vtemp = UserTeamPlayers::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->first(); 
						if(!empty($utp_vtemp)){
							$v1 = $utp_vtemp; 
						}

					}







				// echo 'hi'; 
				// dump($v1); die; 

				// $tmp_top_pnt += $tmp_pnt ; 

					// dump($v1->player_id); 
					if($v1->player_id == $value->capton_id){

						$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 1)->whereNull('remove_date')->first(); 
						if(!empty($utcvct)){
							$tmp_strt_dt = $utcvct->created_at ; 
						}else{
							// echo 'hi1'; die; 
							$tmp_strt_dt = $v1->created_at ; 
						}
						// echo 'hi'; 
						// dump(Carbon::parse($tmp_strt_dt)->endOfDay()); 

						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)



					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

							->whereHas('fixture', function($q) use ($tmp_strt_dt){
												$q->where('start_date', '>', Carbon::parse($tmp_strt_dt)->endOfDay());
											})

					// ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)







						// dump($tmp_pnt); 
						// echo 'hello'; 
						$tmp_top_pnt += round($tmp_pnt) ; 
					}

					if($v1->player_id == $value->vice_capton_id){


						$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
						if(!empty($utcvct)){
							$tmp_strt_dt = $utcvct->created_at ; 
						}else{
							// echo 'hi2'; die; 
							$tmp_strt_dt = $v1->created_at ; 
						}




						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)

					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

						->whereHas('fixture', function($q) use ($tmp_strt_dt){
																		$q->where('start_date', '>', Carbon::parse($tmp_strt_dt)->endOfDay());
																	})
					// ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)
















						$tmp_top_pnt += round($tmp_pnt * 0.5) ; 
					}


						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)


					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

						->whereHas('fixture', function($q) use ($v1){
									$q->where('start_date', '>', Carbon::parse($v1->created_at)->endOfDay());
								})
					// ->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)







						$tmp_top_pnt += $tmp_pnt ; 



					// dump($tmp_top_pnt); 



					}

				// die; 
					$tmp_top_pnt += $value->extra_team_point ; 

					$temp[$value->id] = $tmp_top_pnt; 


					// UserTeams::where('id', $value->id)->update(['total_team_point' => $tmp_top_pnt]); // update user team value 


				





				}

	*/ 

				// $temp = UserTeams::where('club_id', $club_id)->pluck('total_team_point', 'id')->toArray(); 

				$club_team_ids = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 
				// $temp = UserTeamsGWExtraPTTrack::whereIn('team_id', $club_team_ids)->orderBy('id', 'desc')->pluck('overall_pt', 'id')->toArray();


				 // $temp = DB::table('user_teams_gw_extra_pt_track')
				 //                 ->select('id', DB::raw('max(overall_pt) as total', 'team_id'))
				 //                 ->groupBy('team_id')
				 //                 ->whereIn('id', $club_team_ids)
				 //                 ->get();

				// dump($club_team_ids); 

// $temp = UserTeamsGWExtraPTTrack::whereIn('t.team_id', $club_team_ids)->select(DB::raw('t.*'))
//             ->from(DB::raw('(SELECT * FROM user_teams_gw_extra_pt_track ORDER BY gw_end_date DESC) t'))
//             ->groupBy('t.team_id')
//             ->get();

			// dump($club_team_ids); 

			$temp =  UserTeamsGWExtraPTTrack::select(DB::raw('max(id) as gw_id, team_id'))  // get the id of most last gameweek table record 
			                     ->whereIn('team_id', $club_team_ids)
			                     ->groupBy('team_id')
			                     ->pluck('gw_id', 'team_id')->toArray();




			$temp = UserTeamsGWExtraPTTrack::whereIn('id', $temp)->pluck('overall_pt', 'team_id')->toArray();  // get the over all point 

				// dump($temp); die; 



// dump($user_teams_data); 
// dump(Auth::user());
// 		dump($temp); die; 
				arsort($temp); 
				$temp_points_arr = $temp; 

				// array_map('round', $temp_points_arr);
				$temp_points_arr=array_map("round",$temp_points_arr); // remove decimal points 

				// dump($temp_points_arr); die; 
		// $temp_points_arr_past_wk = $temp_last_wk_pt; 
				$i = 1; 
				foreach ($temp as $key => $value) {
					$temp[$key] = $i++ ; 
				}

				$team_ranks_arr = $temp; 





				// get gameweek points 

				// dump($temp_gw); die; 
			if(!empty($user_teams_data))
				$gameweek_fantasy_points = UserTeamsGWExtraPTTrack::where('gw_end_date', Carbon::now()->subWeek()->endOfWeek())->where('team_id', $user_teams_data->id)->first(); 
			
			$gameweek_fantasy_points = !empty($gameweek_fantasy_points->gw_pt) ? round($gameweek_fantasy_points->gw_pt) : 0; 
		// dump($user_teams_data); 
		// dump($user_teams_dt); 
		// dump($temp_points_arr); 
		// dump($team_ranks_arr); 
		// die; 
				$tot_club_users = UserTeams::where('club_id', $club_id)->count(); 

		// dump($tot_club_users); die; 

			}



// rank calc




		// dump($club_data->lockout_start_day); die; 

		// dump(Carbon::parse('tuesday')); die; 

		// dump($club_data); die; 


	 // dump($user_teams_data); die; 
	 // dump($userteam_fantasy_points); die; 

		// capton_card_count  capton_card_last_date  twelve_man_card_count  twelve_man_card_last_date  twelveth_man_id

		// dump($user_teams_data); die; 

			if(!empty($user_teams_data)){
				if(!empty($user_teams_data->capton_card_last_date) &&  Carbon::parse($user_teams_data->capton_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null]);
				if(!empty($user_teams_data->twelve_man_card_last_date) &&  Carbon::parse($user_teams_data->twelve_man_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['twelve_man_card_last_date' => null]);
				if(!empty($user_teams_data->dealer_card_last_date) &&  Carbon::parse($user_teams_data->dealer_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['dealer_card_last_date' => null]);
				if(!empty($user_teams_data->flipper_card_last_date) &&  Carbon::parse($user_teams_data->flipper_card_last_date)->endOfWeek() < Carbon::now())
					UserTeams::where('id', $user_teams_data->id)->update(['flipper_card_last_date' => null]);

			}


			$booster_active = array( 'name' => false, 'status' => false );  
			if(!empty($user_teams_data) && !empty(UserTeams::where('id', $user_teams_data->id)->first()->capton_card_last_date)){
				// UserTeams::where('id', $user_teams_data->id)->update(['twelve_man_card_last_date' => null, 'dealer_card_last_date' => null]);

				$booster_active['name'] = '3x'; 
				$booster_active['status'] = true; 
			}
		// if(!empty($user_teams_data->twelve_man_card_last_date)){
			if(!empty($user_teams_data) && !empty(UserTeams::where('id', $user_teams_data->id)->first()->twelve_man_card_last_date)){
				// UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null, 'dealer_card_last_date' => null]);
				$booster_active['name'] = '12th'; 
				$booster_active['status'] = true; 
			}

			if(!empty($user_teams_data) && !empty(UserTeams::where('id', $user_teams_data->id)->first()->dealer_card_last_date)){
				// UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null, 'twelve_man_card_last_date' => null ]);
				$booster_active['name'] = 'Dealer'; 
				$booster_active['status'] = true; 
			}

			if(!empty($user_teams_data) && !empty(UserTeams::where('id', $user_teams_data->id)->first()->flipper_card_last_date)){
				// UserTeams::where('id', $user_teams_data->id)->update(['capton_card_last_date' => null, 'twelve_man_card_last_date' => null ]);
				$booster_active['name'] = 'Flipper'; 
				$booster_active['status'] = true; 
			}


			$availability = Availability::where('is_active',1)->where('club', $club_id)->where('date_till', '>=', Carbon::now())->get(); 

			$temp = array(); 
			foreach ($availability as $key => $value) {
			// dump(Carbon::parse($value->date_from)); die; 
				$temp[$value->player] = $value; 
			}
			$availability = $temp; 
		// dump($availability); die; 
			$club_points = null; 
			if(!empty($club_id)){
				$club_points = GamePoint::where('club', $club_id)->get(); 
			// dump($club_points); die; 
			}

		// dump($players); die; 
			$club_players =  Player::where('club', $club_name)->where('is_active', 1)->pluck('id')->toArray(); 


			$club_players_fantasy_points = FixtureScorcard::whereIn('player_id', $club_players)->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray(); 

		  // dump($club_players_fantasy_points); die; 
			if(!empty($club_data->lockout_start_date) && Carbon::parse($club_data->lockout_start_date) > Carbon::now()){
				$cur_gameweek_no = ''; 
			}elseif(!empty($club_data->lockout_start_date)){
				$cur_gameweek_no = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now());
				$cur_gameweek_no++;
			}else{
				$cur_gameweek_no = 1; 
			}

			$locked_player = FixtureScorcard::where('status', '=', 2)->whereDate('created_at', '<=', Carbon::now()->startOfDay())->pluck('player_id')->toArray(); 


	// dump($locked_player); die; 
			$force_unlock = false; 
			if(!empty($club_data->is_lockout) && $club_data->is_lockout == 2){

				// $lockout_club = false; 
				// $force_unlock = true; 

		// dump($club_data->lockout_end_time); 
		// dump(explode(':',date('H:i'))); die; 
			// dump($club_data); die; 
			// dump(Carbon::now()->is('Monday'));
			// dump(Carbon::parse($club_data->force_logout_date)->next($club_data->lockout_end_day)->endOfDay()); 
			// dump( Carbon::now()->startOfDay()); die; 

			// dump(Carbon::parse($club_data->force_logout_date)->subDay()->next($club_data->lockout_end_day)->startOfDay()); 
			// dump(Carbon::now()->startOfDay()); die; 

				if(!empty($club_data->force_logout_date) && Carbon::parse($club_data->force_logout_date)->subDay()->next($club_data->lockout_end_day)->startOfDay() == Carbon::now()->startOfDay()){
					$curt = explode(':',date('H:i')); 

				// echo 'hi1'; die; 
					if(($club_data->lockout_end_time[0] * 60 + $club_data->lockout_end_time[1]) >= ($curt[0] * 60 + $curt[1])){
						$lockout_club = false; 
						$force_unlock = true; 
					}else{
						$temp = User::find($club_data->id);
						$temp->is_lockout = 0;
						$temp->force_logout_date = NULL;
						$temp->save();
					}

				}elseif(!empty($club_data->force_logout_date) && Carbon::parse($club_data->force_logout_date)->subDay()->next($club_data->lockout_end_day)->startOfDay() > Carbon::now()->startOfDay()){
				// echo 'hi2'; die; 
					$lockout_club = false; 
					$force_unlock = true; 
				}else{
	// echo 'hi3'; die; 
					$temp = User::find($club_data->id);
					$temp->is_lockout = 0;
					$temp->force_logout_date = NULL;
					$temp->save();

				}



			}
	// $force_unlock = false; 
		// dump($club_data); die; 



	  // $club_players_fantasy_points = array(); 
	  // if(!empty($user_teams_data->created_at)){
		 //  $club_players_fantasy_points = FixtureScorcard::whereIn('player_id', $club_players)->whereDate('created_at', '>=', Carbon::parse($user_teams_data->created_at)->startOfDay())->where('status', 3)->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray(); 
	  // }

// dump($players); die; 
		// dump($user_teams); die; 




	 // get fixture detail of this gameweek start ******




        // $club_id = \Session::get('sess_club_name');
    // print_r($club_id); 
        $DB                     =   Fixture::query();
        if(!empty($club_id))
            $DB->where('fixtures.club', $club_id); 

       if(!empty($club_id)){
        $temp = array(); 
// whereDate('created_at', '>', Carbon::now()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->endOfWeek())
        // Carbon::now()->subDays(30)

        $DB->leftJoin('teams','teams.id','=','fixtures.team')
                        ->leftJoin('dropdown_managers','dropdown_managers.id','=','fixtures.grade')
                        ->leftJoin('grades','grades.id','=','fixtures.grade')
                        ->where('fixtures.is_active',1); 

        $result_fix =   $DB
                       ->whereBetween('fixtures.start_date',  [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()] )

                       // ->whereDate('fixtures.created_at', '<=', Carbon::now()->subDays(7 * $i)->endOfWeek() )
                       ->select('teams.name as team_name','grades.grade as grade_name', 'fixtures.*')->get(); 


            if(!empty($result_fix)){
                foreach ($result_fix as $k => $val) {
                    // dump($val); die; 
                    $val->tot_fp = $val->fixture_scorecard->sum('fantasy_points'); 

                }
            
                // $result_fix = $result_fix->sortByDesc('tot_fp');
            }

    }else{
        $result_fix = null; 
    }



    // dump($result_fix); die; 
    
if(!empty($result_fix)){
        foreach ($result_fix as $k1 => $v1) {
            // dump($v1->id); 
    $DB                     =   FixtureScorcard::query();
    $result_fixtt =     $DB->leftjoin('players','fixture_scorecards.player_id','=','players.id')
                        //->leftjoin('fixture','fixture.id','=','fixture_scorecards.fixture_id')
                        ->select('fixture_scorecards.*','players.full_name as player_name','players.position as player_position')

                        ->where('fixture_scorecards.fixture_id',$v1->id)->get();
                $v1->fixture_scorecards = $result_fixtt ; 
        }
}



// dump($result_fix); 


	 // get fixture detail of this gameweek end *****






		// picked in team start 



	// dump($players); 

        $tot_team = UserTeams::where('club_id', $club_id)->count(); 
        // dump($tot_team); 
        // dump($result); die; 

        foreach ($players as $key => $value) {
            // $team_selected = UserTeamPlayers::where('player_id',  $value->id)
                //          ->whereHas('teams', function($q) use ($club_id) {
                //                                      $q->where('club_id', $club_id);
                //                                  })
            //              ->pluck('team_id')->toArray(); 


            $team_selected = UserTeams::where('club_id', $club_id)->whereHas('getTeamPlayer', function($q) use ($value) {
                                    $q->where('player_id', $value->id);
                                })->count();


            // $team_selected = array_unique($team_selected);
            $cap_count = 0; // UserTeams::where('club_id', $club_id)->where('capton_id', $value->id)->count(); 
            $vice_cap_count = 0; // UserTeams::where('club_id', $club_id)->where('vice_capton_id', $value->id)->count(); 
            // dump($team_selected); 
            // $team_selected = count($team_selected); 
            $selected_by = 0;
            if(!empty($team_selected) && !empty($tot_team) && ($team_selected > ($cap_count + $vice_cap_count))){
                $selected_by = round((($team_selected-($cap_count + $vice_cap_count))/$tot_team)*100); 
                // dump($selected_by); 
            }
            $value->selected_by = $selected_by; 
        }

// echo 'hello';

    // dump($players); die; 


		// picked in team end 

        // dump($user_teams); 


        // dump($user_teams); 
        // dump($dummy_player); 
        // die; 
        // dump($user_teams_data); die; 
        if(!empty($user_teams_data->id)){
       		$tot_salary = UserTeamValueLog::where('team_id', $user_teams_data->id)->max('team_value');  // get the maximum team value of the team till now\
       		$tot_salary = $tot_salary < 100 ? 100 : $tot_salary; 
        }else{
        	$tot_salary = 100; 
        }


		$all_other_club_team_arr = UserTeams::with('userdata')->where('user_id', '!=', auth()->guard('web')->user()->id)->where('club_id', $club_name)->orderByRaw("RAND()")->get();


		// dump($all_other_club_team_arr); die; 

			return View::make('front.user.myteam',compact('players','position','svalue','user_teams','chosen_player','capton_id','vice_capton_id', 'my_team_name', 'club_name', 'userteam_fantasy_points', 'club_data', 'no_of_trade', 'gameweek_fantasy_points', 'user_teams_data', 'lockout_club', 'twelve_player_select', 'remain_lock_out_time', 'team_ranks_arr', 'tot_club_users', 'temp_points_arr', 'lockout_time_remaining_seconds', 'booster_active', 'availability', 'club_points', 'club_players_fantasy_points', 'cur_gameweek_no', 'locked_player', 'force_unlock', 'sess_club_name_lobby', 'clubLists_lobby', 'result_fix', 'tot_salary', 'all_other_club_team_arr'));
		}




		public function removeBooster(Request $request){
			$club_id = \Session::get('sess_club_name');
			if(!empty($club_id)){
				UserTeams::where('club_id', $club_id)->update(['capton_card_last_date' => null, 'twelve_man_card_last_date' => null]); 
				return 1; 
			}else{
				return 0; 
			}


		}

		public function saveMyTeam(Request $request){

			$all_input = Input::all() ; 
		// print_r($all_input); die; 
		// print_r($all_input['twelveth_player_id']);  die; 
			if(Auth::user() && !empty($all_input['message']) && !empty($all_input['capton_id']) && !empty($all_input['vice_capton_id']) && !empty($all_input['club_id'])){

			$club_data = User::find($all_input['club_id']); // where('id', $all_input['club_id'])->first(); 
			// print_r(Carbon::parse($club_data->lockout_start_date)->startOfDay()); die; 
			$trade_flag = false; 

			$is_team_exists = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->exists(); 


			// check if trade can be done 
			if (
				$is_team_exists	 &&

				!empty($club_data->lockout_start_date) && !empty($club_data->lockout_start_day) && Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)->startOfDay()


			) {
				$trade_flag = true; 
			}

			$player_list = $all_input['message']; 

			$temp_ut = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 

// print_r($player_list); 
// print_r($all_input['capton_id']); 
// print_r($all_input['vice_capton_id']); 
			$all_team_pl_pr_sum = 0 ; 
			$all_team_pl_players_points = array() ; 
			$all_team_pl_players_price = array() ; 

			// calculate complete points of all current players of team
			// $all_team_pl_players_points = FixtureScorcard::whereIn('player_id', $player_list)->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray(); 


			// remove the 12th player from the player list 
			// echo $temp_ut->twelveth_man_id; die; 
			$temp_12th_pl = 0; 
			if(!empty($temp_ut) && !empty($temp_ut->twelveth_man_id) ){
				$temp_12th_pl = $temp_ut->twelveth_man_id; 
			}
			if(!empty($all_input['twelveth_player_id'])){
				$temp_12th_pl = $all_input['twelveth_player_id'];  
			}


/* // commented on 11-02-20 as now points will be updated from backend admin side when club will enter points 

			foreach ($player_list as $kp => $vp) {
				$temp_player = Player::where('id', $vp)->first(); 

				if($temp_12th_pl != $vp) // remove 12th man price if present
					$all_team_pl_pr_sum += $temp_player->svalue ;       // sum of price of all current player
				$all_team_pl_players_price[$vp] = $temp_player->svalue ;  // all price by player 
				$all_team_pl_players_points[$vp] = isset($all_team_pl_players_points[$vp]) ? $all_team_pl_players_points[$vp] : 0 ;  // points of players
			}

*/ 



// die; 
			// commented because we want to save 12th player also in the user_team_players table 
			// if (count($player_list) > 11 && ($key = array_search($all_input['twelveth_player_id'], $player_list)) !== false) {
			// 	unset($player_list[$key]);
			// }
// print_r($player_list); die; 
			$user_terms = UserTeams::firstOrNew(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']]);  // get old team or create new team

			if(!empty($user_terms) && !empty($user_terms->flipper_card_last_date) && $user_terms->flipper_card_status == 0 && !is_null($user_terms->flipper_card_status)){
				$trade_flag = false; 
				UserTeams::where('id', $user_terms->id)->update(['flipper_card_status' => 1]);

			}








			// For 12th man trades count will not increase
			if(!empty($user_terms) && (!empty($user_terms->twelve_man_card_last_date) && empty($user_terms->twelveth_man_id)) && !empty($all_input['twelveth_player_id'])){ // 
				$trade_flag = false; 
			}

// old_capt_id  old_vc_capt_id
			$old_capt_id = 0; 
			if(!empty($user_terms->capton_id)){
				$old_capt_id = $user_terms->capton_id ; 
			}
			$old_vc_capt_id = 0; 
			if(!empty($user_terms->vice_capton_id)){
				$old_vc_capt_id = $user_terms->vice_capton_id ; 
			}

			// $user_terms->player_ids = serialize($player_list); 
			$user_terms->capton_id = $all_input['capton_id']; 
			$user_terms->vice_capton_id = $all_input['vice_capton_id'];
			$user_terms->my_team_name = empty($all_input['my_team_name'])? Auth::user()->my_team_name : $all_input['my_team_name'];
			$user_terms->club_id = $all_input['club_id'];

			$user_terms->game_mode = $all_input['my_game_mode'];
			// print_r($player_list); echo count($player_list);  die; 
			if( !empty($all_input['twelveth_player_id'])){
				$user_terms->twelveth_man_id = !empty($all_input['twelveth_player_id']) ? $all_input['twelveth_player_id'] : null;
			}




			if($is_team_exists){
				$old_player_arr = UserTeamPlayers::where('team_id', $user_terms->id)->pluck('player_id')->toArray(); 


				$removed_player = array_diff($old_player_arr, $player_list);             // get the old team player
				$newly_added_player = array_diff($player_list, $old_player_arr);       // get the new team players


				// print_r($removed_player);

				// print_r($newly_added_player); 
				//  die; 

				$common_val = array_intersect($player_list, $old_player_arr);
				$no_of_trade = count($player_list) - count($common_val);   // get no of trade


				// if dealer power is activated for this week, then set trade flag to false .
				
			if(!empty($user_terms->dealer_card_last_date) &&  Carbon::parse($user_terms->dealer_card_last_date)->endOfWeek() > Carbon::now() && Carbon::now() > Carbon::parse($user_terms->dealer_card_last_date)->startOfWeek() &&  $user_terms->dealer_card_status == 0 && $no_of_trade == 1){
// echo 'hi'; die;

					$trade_flag = false; 
			        UserTeams::where('id', $user_terms->id)->update(['dealer_card_status' => 1]);

			        // if dealer power with multiple trade is attempted then return error code 3
			}elseif(!empty($user_terms->dealer_card_last_date) &&  Carbon::parse($user_terms->dealer_card_last_date)->endOfWeek() > Carbon::now() && Carbon::now() > Carbon::parse($user_terms->dealer_card_last_date)->startOfWeek() &&  $user_terms->dealer_card_status == 0 && $no_of_trade > 1){  

				return 3; 

			}

// echo 'aasdf'; die;

			}



			if($trade_flag){

				// echo '<pre>'; 
				// print_r($player_list);
				// print_r($old_player_arr); 
				$common_val = array_intersect($player_list, $old_player_arr);
				$no_of_trade = count($player_list) - count($common_val);   // get no of trade




				$old_no_of_trade = $user_terms->no_of_trade; 
				if($old_no_of_trade + $no_of_trade <= MAX_NO_OF_TRADE){
					$user_terms->no_of_trade += $no_of_trade;

					$utl = new UserTeamLogs;
					$utl->log_type = 1;
					$utl->user_id = Auth::user()->id;
					$utl->club_id = $all_input['club_id'];
					$utl->team_id = $user_terms->id;
					$utl->save();

					$uttl = new UserTeamTradeLog; 
					$uttl->team_id = $user_terms->id ; 
					$uttl->no_of_trade = $no_of_trade ; 

					$uttl->save();


				}


				if($old_no_of_trade + $no_of_trade > MAX_NO_OF_TRADE){
					return 2; 
					die; 
				}

				$user_terms->save(); 

				foreach ($removed_player as $key => $value) {
					// player_id in_out
					$ptio = new PlayerTradeInOut;
					$ptio->team_id = $user_terms->id; 
					$ptio->player_id = $value; 
					$ptio->in_out = 0; 
					$ptio->save();

				}

				foreach ($newly_added_player as $key => $value) {
					// PlayerTradeInOut
					$ptio = new PlayerTradeInOut;
					$ptio->team_id = $user_terms->id; 
					$ptio->player_id = $value; 
					$ptio->in_out = 1; 
					$ptio->save();
				}

			}else{


				$user_terms->save(); 

			}
			// if (UserTeamPlayers::where('team_id', $user_terms->id)->exists()) {


			/* // commented on 24/02/20 as no use in the current scenario

			$utpl = new UserTeamPlayerLogs;
			$utpl->team_id = $user_terms->id ; 
			$utpl->player_id =  $user_terms->capton_id; 
			$utpl->player_type = 1 ; 
			$utpl->save();

			$utpl = new UserTeamPlayerLogs;
			$utpl->team_id = $user_terms->id ; 
			$utpl->player_id =  $user_terms->vice_capton_id ; 
			$utpl->player_type = 2; 
			$utpl->save();


			*/ 



			$capt_change_detected = false; 
			$v_capt_change_detected = false; 


/* // commented on 11-02-20 as now points will be updated from backend admin side when club will enter points 

			if($is_team_exists){

// print_r($player_list); die; 


				if(!empty($removed_player)){


						// FixtureScorcard::whereIn('player_id', $removed_player)->groupBy('player_id')
						// 	   ->selectRaw('sum(fantasy_points) as sum, player_id')->orderBy('sum', 'DESC')
						// 	   ->pluck('sum','player_id')->toArray();
					$etp = 0; 
					$gw_etp = 0 ; 

					// $past_gw_etp = 0; 

					foreach ($removed_player as $key => $value) {

						$temp_utpt = UserTeamsPlayerTrack::where('player_id', $value)->where('team_id', $user_terms->id)->whereNull('remove_date')->first(); 
						if(empty($temp_utpt)){
							$temp_utpt = UserTeamPlayers::where('player_id', $value)->where('team_id', $user_terms->id)->first(); 
						}

// old_capt_id
						$capton_start_date = null; 
						if($old_capt_id == $value){
							$tc_utcvct = UserTeamsCVCTrack::where('player_id', $value)->where('team_id', $user_terms->id)->where('c_vc', 1)->whereNull('remove_date')->first(); 
							if(!empty($tc_utcvct)){
								$capton_start_date = $tc_utcvct->created_at ; 

							}else{
								$capton_start_date = $temp_utpt->created_at ; 
							}
							$capt_change_detected = true; 
						}

						$v_capton_start_date = null; 
						if($old_vc_capt_id == $value){
							$tvc_utcvct = UserTeamsCVCTrack::where('player_id', $value)->where('team_id', $user_terms->id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
							if(!empty($tvc_utcvct)){
								$v_capton_start_date = $tvc_utcvct->created_at ; 
							}else{
								$v_capton_start_date = $temp_utpt->created_at ; 
							}
							$v_capt_change_detected = true; 
						}


						if(!empty($temp_utpt)){

						// $tmp_price = FixtureScorcard::where('player_id', $value)->where('created_at', '>', Carbon::parse($temp_utpt->created_at))->sum('fantasy_points'); // ->where('status', 3)

							// add points of all player
							$tmp_price = FixtureScorcard::where('player_id', $value)
							->whereHas('fixture', function($q) use ($temp_utpt){
								$q->where('start_date', '>', Carbon::parse($temp_utpt->created_at));
							})

								// ->where('created_at', '>', Carbon::parse($temp_utpt->created_at))   // commented 

								->sum('fantasy_points'); // ->where('status', 3)






					 	// $tmp_price = FixtureScorcard::where('player_id', $value)
							// 	 	->whereHas('fixture', function($q) use ($temp_utpt){
							// 			$q->where('start_date', '>', Carbon::parse($temp_utpt->created_at)->endOfDay());
							// 		})
					 	//            ->sum('fantasy_points'); // ->where('status', 3)

								// add extra capton points for capton only 
								$c_tmp_price = 0 ; 
								if(!empty($capton_start_date)){
							// $c_tmp_price = FixtureScorcard::where('player_id', $value)->where('created_at', '>', Carbon::parse($capton_start_date))->sum('fantasy_points');  // ->where('status', 3)


									$c_tmp_price = FixtureScorcard::where('player_id', $value)

									->whereHas('fixture', function($q) use ($capton_start_date){
										$q->where('start_date', '>', Carbon::parse($capton_start_date));
									})

							// ->where('created_at', '>', Carbon::parse($capton_start_date)) // commented 
							->sum('fantasy_points');  // ->where('status', 3)							








						}

						// add extra vice capton points  for vice capton only 
						$vc_tmp_price = 0 ; 
						if(!empty($v_capton_start_date)){
							// $vc_tmp_price = FixtureScorcard::where('player_id', $value)->where('created_at', '>', Carbon::parse($v_capton_start_date))->sum('fantasy_points');  // ->where('status', 3)


							$vc_tmp_price = FixtureScorcard::where('player_id', $value)

							->whereHas('fixture', function($q) use ($v_capton_start_date){
								$q->where('start_date', '>', Carbon::parse($v_capton_start_date));
							})

						// ->where('created_at', '>', Carbon::parse($v_capton_start_date))
						->sum('fantasy_points');  // ->where('status', 3)





						$vc_tmp_price = round($vc_tmp_price * 0.5); 

					}


					$etp = $etp + ($tmp_price + $c_tmp_price + $vc_tmp_price); 



						// code commented as it is calculating the  point of current gameweek

					$player_start_date = Carbon::parse($temp_utpt->created_at); 

					while (Carbon::parse($player_start_date) <= Carbon::now()->endOfWeek()) {






						// $tmp_price = FixtureScorcard::where('player_id', $value)->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)


						$tmp_price = FixtureScorcard::where('player_id', $value)

						->whereHas('fixture', function($q) use ($player_start_date){
							$q->where('start_date', '>', $player_start_date->copy()->startOfWeek())->where('start_date', '<=', $player_start_date->copy()->endOfWeek()); 

						})

						// ->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())


						->sum('fantasy_points'); // ->where('status', 3)


						// dump($tmp_price); 
						// dump($player_start_date); 

						



						$c_tmp_price = 0 ; 
						if(!empty($capton_start_date) && (Carbon::parse($capton_start_date) < $player_start_date->copy()->endOfWeek() )){ // check the capton start date is in the corresponding game week
							// $c_tmp_price = FixtureScorcard::where('player_id', $value)->where('created_at', '>', Carbon::parse($capton_start_date))->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)

							$c_tmp_price = FixtureScorcard::where('player_id', $value)

							->whereHas('fixture', function($q) use ($player_start_date, $capton_start_date){

								$q->where('start_date', '>', Carbon::parse($capton_start_date))->where('start_date', '>', $player_start_date->copy()->startOfWeek())->where('start_date', '<=', $player_start_date->copy()->endOfWeek()); 


							})

							// ->where('created_at', '>', Carbon::parse($capton_start_date))->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())

							->sum('fantasy_points'); // ->where('status', 3)





						}
						$vc_tmp_price = 0 ; 
						if(!empty($v_capton_start_date) && (Carbon::parse($v_capton_start_date) < $player_start_date->copy()->endOfWeek() )){
							// $vc_tmp_price = FixtureScorcard::where('player_id', $value)->where('created_at', '>', Carbon::parse($v_capton_start_date))->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)


							$vc_tmp_price = FixtureScorcard::where('player_id', $value)


							->whereHas('fixture', function($q) use ($player_start_date, $v_capton_start_date){

								$q->where('start_date', '>', Carbon::parse($v_capton_start_date))->where('start_date', '>', $player_start_date->copy()->startOfWeek())->where('start_date', '<=', $player_start_date->copy()->endOfWeek()); 

							})

							// ->where('created_at', '>', Carbon::parse($v_capton_start_date))->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())

							->sum('fantasy_points'); // ->where('status', 3)




							




							$vc_tmp_price = round($vc_tmp_price * 0.5); 

						}
						// $tmp_price = FixtureScorcard::where('player_id', $value)

						// 				->whereHas('fixture', function($q) use ($temp_utpt){
						// 					$q->where('start_date', '>', Carbon::parse($temp_utpt->created_at)->endOfDay())->where('start_date', '>=', Carbon::now()->startOfWeek())->where('start_date', '<=', Carbon::now()->endOfWeek());
						// 				})

						//             ->sum('fantasy_points'); // ->where('status', 3)

						$gw_etp = ($tmp_price  + $c_tmp_price + $vc_tmp_price); 



						// save points according to each gameweek


						if(UserTeamsGWExtraPTTrack::where(['team_id' => $user_terms->id, 'gw_end_date' => $player_start_date->copy()->endOfWeek()->endOfDay() ])->exists()){

							$tmp = UserTeamsGWExtraPTTrack::where('team_id', $user_terms->id)->where('gw_end_date',  $player_start_date->copy()->endOfWeek()->endOfDay())->first(); 
							if($gw_etp >= 0)	
								$tmp->gw_extra_pt = round($tmp->gw_extra_pt + $gw_etp) ; 
							$tmp->save(); 

						}else{

							$tmp = new UserTeamsGWExtraPTTrack; 
							$tmp->team_id = $user_terms->id ; 
							if($gw_etp >= 0)
								$tmp->gw_extra_pt = round( $gw_etp) ; 
							$tmp->gw_end_date = $player_start_date->copy()->endOfWeek()->endOfDay() ; 
							$tmp->save(); 

						}





						$player_start_date->addWeek(); 


					}





				}

			}

					// print_r($etp); 
			// store points of removed player 
			if($etp > 0){

				$tmp = UserTeams::find($user_terms->id); 
				$tmp->extra_team_point =  round($tmp->extra_team_point + $etp) ; 
				$tmp->save(); 
				

			}






 					}


 				}


*/ 



/* // commented on 11-02-20 as now points will be updated from backend admin side when club will enter points 

// old_capt_id  old_vc_capt_id  !empty($all_input['capton_id']) && !empty($all_input['vice_capton_id']) 

 				if(!$capt_change_detected && !empty($old_capt_id) && $old_capt_id != $all_input['capton_id']){

						// $tmp = UserTeams::find($user_terms->id); 
						// $tmp->extra_team_point += $etp ; 
						// $tmp->save(); 

 					$temp_fst = UserTeamsCVCTrack::where('player_id', $old_capt_id)->where('team_id', $user_terms->id)->where('c_vc', 1)->whereNull('remove_date')->first(); 





 					$tmp_price = 0; 

 					if(!empty($temp_fst)){
 						$temp_fst->remove_date = Carbon::now(); 
 						$temp_fst->save(); 
						// $tmp_price = FixtureScorcard::where('player_id', $old_capt_id)->where('created_at', '>', Carbon::parse($temp_fst->created_at))->sum('fantasy_points');

 						$tmp_price = FixtureScorcard::where('player_id', $old_capt_id)

 						->whereHas('fixture', function($q) use ($temp_fst){
 							$q->where('start_date', '>', Carbon::parse($temp_fst->created_at));
 						})

							// ->where('created_at', '>', Carbon::parse($temp_fst->created_at))

 						->sum('fantasy_points');


						// $tmp_price = FixtureScorcard::where('player_id', $old_capt_id)

						// 		 	->whereHas('fixture', function($q) use ($temp_fst){
						// 				$q->where('start_date', '>', Carbon::parse($temp_fst->created_at)->endOfDay());
						// 			})
						//           ->sum('fantasy_points');





						 // ->where('status', 3)
 					}else{


 						$utpt = UserTeamsPlayerTrack::where('player_id', $old_capt_id)->where('team_id', $user_terms->id)->whereNull('remove_date')->first(); 
 						if(empty($utpt))
 							$utpt = UserTeamPlayers::where('player_id', $old_capt_id)->where('team_id', $user_terms->id)->first(); 




						// $tmp_price = FixtureScorcard::where('player_id', $old_capt_id)->where('created_at', '>', Carbon::parse($utpt->created_at))->sum('fantasy_points'); // ->where('status', 3)
 						$tmp_price = FixtureScorcard::where('player_id', $old_capt_id)

 						->whereHas('fixture', function($q) use ($utpt){
 							$q->where('start_date', '>', Carbon::parse($utpt->created_at));
 						})
					// ->where('created_at', '>', Carbon::parse($utpt->created_at))

					->sum('fantasy_points'); // ->where('status', 3)








	// $tmp_price = FixtureScorcard::where('player_id', $old_capt_id)

	// 					 	->whereHas('fixture', function($q) use ($user_terms){
	// 							$q->where('start_date', '>', Carbon::parse($user_terms->created_at)->endOfDay());
	// 						})
	// ->sum('fantasy_points'); // ->where('status', 3)





				}


				// store extra team points
				if($tmp_price >=0 ){   // adding overall extra team point 
					$tmp = UserTeams::find($user_terms->id); 
					$tmp->extra_team_point = round($tmp->extra_team_point + $tmp_price) ; 
					$tmp->save(); 
				}



				$gw_etp = 0 ; 


				if(!empty($temp_fst)){
					$player_start_date = Carbon::parse($temp_fst->created_at); 
				}else{



					$utpt = UserTeamsPlayerTrack::where('player_id', $old_capt_id)->where('team_id', $user_terms->id)->whereNull('remove_date')->first(); 
					if(empty($utpt))
						$utpt = UserTeamPlayers::where('player_id', $old_capt_id)->where('team_id', $user_terms->id)->first(); 
					

					$player_start_date = Carbon::parse($utpt->created_at); 

				}



						while (Carbon::parse($player_start_date) <= Carbon::now()->endOfWeek()) {  // adding team points by gameweek 
							





						// $tmp_price = FixtureScorcard::where('player_id', $old_capt_id)->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)

							$tmp_price = FixtureScorcard::where('player_id', $old_capt_id)

							->whereHas('fixture', function($q) use ($player_start_date){
								$q->where('start_date', '>', $player_start_date->copy()->startOfWeek())->where('start_date', '<=', $player_start_date->copy()->endOfWeek()); 

							})

			// ->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())


			->sum('fantasy_points'); // ->where('status', 3)








			$gw_etp = $tmp_price; ; 



						// save points according to each gameweek


			if(UserTeamsGWExtraPTTrack::where(['team_id' => $user_terms->id, 'gw_end_date' => $player_start_date->copy()->endOfWeek()->endOfDay() ])->exists()){

				$tmp = UserTeamsGWExtraPTTrack::where('team_id', $user_terms->id)->where('gw_end_date',  $player_start_date->copy()->endOfWeek()->endOfDay())->first(); 
				if($gw_etp >= 0)	
					$tmp->gw_extra_pt = round($tmp->gw_extra_pt + $gw_etp) ; 
				$tmp->save(); 

			}else{

				$tmp = new UserTeamsGWExtraPTTrack; 
				$tmp->team_id = $user_terms->id ; 
				if($gw_etp >= 0)
					$tmp->gw_extra_pt = round( $gw_etp) ; 
				$tmp->gw_end_date = $player_start_date->copy()->endOfWeek()->endOfDay() ; 
				$tmp->save(); 

			}





						// save points according to each gameweek 












			$player_start_date->addWeek(); 


		}






	}
	if(!$v_capt_change_detected && !empty($old_vc_capt_id) && $old_vc_capt_id != $all_input['vice_capton_id']){
// print_r($old_vc_capt_id); print_r($all_input['vice_capton_id']); die; 
						// $tmp = UserTeams::find($user_terms->id); 
						// $tmp->extra_team_point += $etp ; 
						// $tmp->save(); 
		$tmp_price = 0; 

		$temp_fst = UserTeamsCVCTrack::where('player_id', $old_vc_capt_id)->where('team_id', $user_terms->id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
		if(!empty($temp_fst)){
			$temp_fst->remove_date = Carbon::now(); 
			$temp_fst->save(); 
						// $tmp_price = FixtureScorcard::where('player_id', $old_vc_capt_id)->where('created_at', '>', Carbon::parse($temp_fst->created_at))->sum('fantasy_points'); // ->where('status', 3)

			$tmp_price = FixtureScorcard::where('player_id', $old_vc_capt_id)

			->whereHas('fixture', function($q) use ($temp_fst){
				$q->where('start_date', '>', Carbon::parse($temp_fst->created_at)->endOfDay());
			})

						// ->where('created_at', '>', Carbon::parse($temp_fst->created_at))
						->sum('fantasy_points'); // ->where('status', 3)





						// $tmp_price = FixtureScorcard::where('player_id', $old_vc_capt_id)

						//  	->whereHas('fixture', function($q) use ($temp_fst){
						// 		$q->where('start_date', '>', Carbon::parse($temp_fst->created_at)->endOfDay());
						// 	})


						// ->sum('fantasy_points'); // ->where('status', 3)






					}else{




						$utpt = UserTeamsPlayerTrack::where('player_id', $old_vc_capt_id)->where('team_id', $user_terms->id)->whereNull('remove_date')->first(); 
						if(empty($utpt))
							$utpt = UserTeamPlayers::where('player_id', $old_vc_capt_id)->where('team_id', $user_terms->id)->first(); 




						// $tmp_price = FixtureScorcard::where('player_id', $old_vc_capt_id)->where('created_at', '>', Carbon::parse($utpt->created_at))->sum('fantasy_points'); // ->where('status', 3)


						$tmp_price = FixtureScorcard::where('player_id', $old_vc_capt_id)

						->whereHas('fixture', function($q) use ($utpt){
							$q->where('start_date', '>', Carbon::parse($utpt->created_at)->endOfDay());
						})
					// ->where('created_at', '>', Carbon::parse($utpt->created_at))


					->sum('fantasy_points'); // ->where('status', 3)






	// $tmp_price = FixtureScorcard::where('player_id', $old_vc_capt_id)

	//  	->whereHas('fixture', function($q) use ($user_terms){
	// 							$q->where('start_date', '>', Carbon::parse($user_terms->created_at)->endOfDay());
	// 						})
	// ->sum('fantasy_points'); // ->where('status', 3)






				}

				if($tmp_price>=0){   // adding overall extra team point 
					$tmp = UserTeams::find($user_terms->id); 
					$tmp->extra_team_point = round($tmp->extra_team_point + round($tmp_price * 0.5)) ; 
					$tmp->save(); 
				}



				$gw_etp = 0 ; 


				if(!empty($temp_fst)){
					$player_start_date = Carbon::parse($temp_fst->created_at); 
				}else{



					$utpt = UserTeamsPlayerTrack::where('player_id', $old_vc_capt_id)->where('team_id', $user_terms->id)->whereNull('remove_date')->first(); 
					if(empty($utpt))
						$utpt = UserTeamPlayers::where('player_id', $old_vc_capt_id)->where('team_id', $user_terms->id)->first(); 
					

					$player_start_date = Carbon::parse($utpt->created_at); 

				}



						while (Carbon::parse($player_start_date) <= Carbon::now()->endOfWeek()) {  // adding team points by game week 
							





						// $tmp_price = FixtureScorcard::where('player_id', $old_vc_capt_id)->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)

							$tmp_price = FixtureScorcard::where('player_id', $old_vc_capt_id)

							->whereHas('fixture', function($q) use ($player_start_date){
								$q->where('start_date', '>', $player_start_date->copy()->startOfWeek())->where('start_date', '<=', $player_start_date->copy()->endOfWeek());

							})
				// ->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())

				->sum('fantasy_points'); // ->where('status', 3)








				$gw_etp = round($tmp_price * 0.5) ; 



						// save points according to each gameweek


				if(UserTeamsGWExtraPTTrack::where(['team_id' => $user_terms->id, 'gw_end_date' => $player_start_date->copy()->endOfWeek()->endOfDay() ])->exists()){

					$tmp = UserTeamsGWExtraPTTrack::where('team_id', $user_terms->id)->where('gw_end_date',  $player_start_date->copy()->endOfWeek()->endOfDay())->first(); 
					if($gw_etp >= 0)	
						$tmp->gw_extra_pt = round($tmp->gw_extra_pt + $gw_etp) ; 
					$tmp->save(); 

				}else{

					$tmp = new UserTeamsGWExtraPTTrack; 
					$tmp->team_id = $user_terms->id ; 
					if($gw_etp >= 0)
						$tmp->gw_extra_pt = round( $gw_etp) ; 
					$tmp->gw_end_date = $player_start_date->copy()->endOfWeek()->endOfDay() ; 
					$tmp->save(); 

				}





						// save points according to each gameweek 





				$player_start_date->addWeek(); 


			}







		}


		*/ 

				//UserTeamsPlayerTrack

/* 
			// update team players start 

		UserTeamPlayers::where('team_id', $user_terms->id)->update(['player_id' => 0]);
		foreach ($player_list as $key => $value) {
			$temp = UserTeamPlayers::firstOrNew(array('team_id' => $user_terms->id, 'player_id' => 0 ));
			$temp->player_id = $value;
			$temp->save();

			$utpl = new UserTeamPlayerLogs;
			$utpl->team_id = $user_terms->id ; 
			$utpl->player_id =  $value;
			$utpl->player_type = 0; 
			$utpl->save();

		}
		UserTeamPlayers::where('team_id', $user_terms->id)->where('player_id', 0 )->delete();

			// update players end 

*/ 



		// update database now start 

		
		if(!empty($removed_player)){
		    foreach ($removed_player as $key => $value) {
		        if(UserTeamsPlayerTrack::where('player_id', $value)->where('team_id', $user_terms->id)->exists()){
					UserTeamsPlayerTrack::where('player_id', $value)->where('team_id', $user_terms->id)->update(['remove_date' => Carbon::now()]); // ->delete();
				}else{
				    $temp_temp = new UserTeamsPlayerTrack; 
				    $temp_temp->player_id = $value ; 
				    $temp_temp->team_id = $user_terms->id ; 
				    $temp_temp->remove_date = Carbon::now() ; 
				    $temp_temp->save(); 

				}
			}
		}
		if($is_team_exists){
		    if(!empty($newly_added_player)){
		        foreach ($newly_added_player as $key => $value) {
		            $temp_utpt = new UserTeamsPlayerTrack;
		            $temp_utpt->team_id = $user_terms->id;
		            $temp_utpt->player_id =  $value;
		            $temp_utpt->save();
		        }
		    }
		}else{
		    foreach ($player_list as $key => $value) {
		        $temp_utpt = new UserTeamsPlayerTrack;
		        $temp_utpt->team_id = $user_terms->id;
		        $temp_utpt->player_id =  $value;
		        $temp_utpt->save();
		    }
		}




			// update team players start 

		UserTeamPlayers::where('team_id', $user_terms->id)->whereNotIn('player_id', $player_list)->update(['player_id' => 0]);
		foreach ($player_list as $key => $value) {
			$temp = UserTeamPlayers::firstOrNew(array('team_id' => $user_terms->id, 'player_id' => $value ));
			$temp->player_id = $value;
			$temp->save();

			$utpl = new UserTeamPlayerLogs;
			$utpl->team_id = $user_terms->id ; 
			$utpl->player_id =  $value;
			$utpl->player_type = 0; 
			$utpl->save();

		}
		UserTeamPlayers::where('team_id', $user_terms->id)->where('player_id', 0 )->delete();

			// update players end 



		// update capton id and voice capton id 
 if(!empty($old_capt_id) && $old_capt_id != $all_input['capton_id']){

                $utcvct = new UserTeamsCVCTrack; 
                $utcvct->player_id = $all_input['capton_id']; 
                $utcvct->team_id = $user_terms->id; 
                $utcvct->c_vc = 1; 
                $utcvct->save(); 

 
                UserTeamsCVCTrack::where('player_id', $old_capt_id)->where('team_id', $user_terms->id)->where('c_vc', 1)->update(['remove_date' => Carbon::now()]);

}

    if(!empty($old_vc_capt_id) && $old_vc_capt_id != $all_input['vice_capton_id']){

                $utcvct = new UserTeamsCVCTrack; 
                $utcvct->player_id = $all_input['vice_capton_id'] ; 
                $utcvct->team_id = $user_terms->id; 
                $utcvct->c_vc = 0; 
                $utcvct->save(); 


                UserTeamsCVCTrack::where('player_id', $old_vc_capt_id)->where('team_id', $user_terms->id)->where('c_vc', 0)->update(['remove_date' => Carbon::now()]);


    }




    // update database end 





/* // commented on 11-02-20 as now points will be updated from backend admin side when club will enter points 



		// save the new fantasy value of the team start 


				$user_terms = UserTeams::find($user_terms->id); 


				$user_team_player = UserTeamPlayers::where('team_id', $user_terms->id)->get(); 

				// dump($value->id); 
				// dump($user_team_player); 
				// dump($value->capton_id); 
				// dump($user_team_player); die; 
				$tmp_top_pnt = 0 ; 
				foreach ($user_team_player as $k1 => $v1) {



					$vtemp = UserTeamsPlayerTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->whereNull('remove_date')->first(); 

					if(!empty($vtemp)){
						$v1 = $vtemp ; 
					}else{
						$utp_vtemp = UserTeamPlayers::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->first(); 
						if(!empty($utp_vtemp)){
							$v1 = $utp_vtemp; 
						}

					}








				// echo 'hi'; 
				// dump($v1); die; 

				// $tmp_top_pnt += $tmp_pnt ; 

					// dump($v1->player_id); 
					if($v1->player_id == $user_terms->capton_id){

						$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 1)->whereNull('remove_date')->first(); 
						if(!empty($utcvct)){
							$tmp_strt_dt = $utcvct->created_at ; 
						}else{
							// echo 'hi1'; die; 
							$tmp_strt_dt = $v1->created_at ; 
						}
						// echo 'hi'; 
						// dump(Carbon::parse($tmp_strt_dt)->endOfDay()); 

						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)

					// if 12th man then change the date accordingly

					if($v1->player_id == $user_terms->twelveth_man_id && !empty($user_terms->twelve_man_card_last_date)){
						$tmp_strt_dt = $user_terms->twelve_man_card_last_date ; 
					}




					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

							->whereHas('fixture', function($q) use ($tmp_strt_dt){
												$q->where('start_date', '>', Carbon::parse($tmp_strt_dt));
											})

					// ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)







						// dump($tmp_pnt); 
						// echo 'hello'; 
						$tmp_top_pnt += round($tmp_pnt) ; 
					}

					if($v1->player_id == $user_terms->vice_capton_id){


						$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
						if(!empty($utcvct)){
							$tmp_strt_dt = $utcvct->created_at ; 
						}else{
							// echo 'hi2'; die; 
							$tmp_strt_dt = $v1->created_at ; 
						}


					// if 12th man then change the date accordingly
					if($v1->player_id == $user_terms->twelveth_man_id && !empty($user_terms->twelve_man_card_last_date)){
						$tmp_strt_dt = $user_terms->twelve_man_card_last_date ; 
					}


						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)

					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

						->whereHas('fixture', function($q) use ($tmp_strt_dt){
																		$q->where('start_date', '>', Carbon::parse($tmp_strt_dt));
																	})
					// ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)
















						$tmp_top_pnt += round($tmp_pnt * 0.5) ; 
					}


						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)

					$tmp_strt_dt = $v1->created_at; 

					// if 12th man then change the date accordingly
					if($v1->player_id == $user_terms->twelveth_man_id && !empty($user_terms->twelve_man_card_last_date)){
						$tmp_strt_dt = $user_terms->twelve_man_card_last_date ; 
					}




					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

						->whereHas('fixture', function($q) use ($tmp_strt_dt){
									$q->where('start_date', '>', Carbon::parse($tmp_strt_dt));
								})
					// ->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)


					// print_r($v1->player_id); 
					// print_r($v1->created_at); 
					// print_r($tmp_pnt); 




						$tmp_top_pnt += $tmp_pnt ; 



					// dump($tmp_top_pnt); 



					}

					// print_r($user_terms->total_team_point); 
					// echo round($user_terms->extra_team_point + $tmp_top_pnt + $user_terms->three_cap_point); 
					// echo $user_terms->dealer_card_minus_points;  

					$user_terms->total_team_point = round($user_terms->extra_team_point + $tmp_top_pnt + $user_terms->three_cap_point) - $user_terms->dealer_card_minus_points;  // save extra team point + player 11 point + captain cap booster extra point - dealer card booster negative point
					// print_r($user_terms->total_team_point); 

					$user_terms->save(); 



			// team rank start 
				$temp = UserTeams::where('club_id', $all_input['club_id'])->pluck('total_team_point', 'id')->toArray(); 

				arsort($temp); 
		// $temp_points_arr_past_wk = $temp_last_wk_pt; 
				$i = 1; 
				foreach ($temp as $key => $value) {
					$temp[$key] = $i++ ; 
				}

				$team_ranks_arr = $temp; 


			// team rank end 



			$utvl = new UserTeamValueLog; 
			$utvl->team_id = $user_terms->id; 
			$utvl->team_value = $all_team_pl_pr_sum; 
			$utvl->team_player = serialize($player_list); 
			$utvl->capton_id = $all_input['capton_id']; 
			$utvl->v_capton_id = $all_input['vice_capton_id']; 
			$utvl->tot_team_point = $user_terms->total_team_point; 
			$utvl->players_points = serialize($all_team_pl_players_points) ; 
			$utvl->players_price = serialize($all_team_pl_players_price) ; 
			$utvl->team_rank = isset($team_ranks_arr[$user_terms->id]) ? $team_ranks_arr[$user_terms->id] : 0; 
			$utvl->save(); 



		// save the new fantasy value of the tam end 


*/ 

		return 1; 
	}else{
		return 0;
	}

}


public function activateCaptonCard(Request $request){

	$all_input = Input::all() ; 
		// print_r($all_input['club_id']); die; 
	if (UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->exists()) {
			// auth()->guard('web')->user()->id
			// Carbon::now()->startOfWeek()

		$user_teams = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 

				// MAX_NO_OF_CAPTON_CARDS
		if($user_teams->capton_card_count >= MAX_NO_OF_CAPTON_CARDS){
			return 3; 

		}else if(!empty($user_teams->capton_card_last_date) && Carbon::now()->startOfWeek() <= Carbon::parse($user_teams->capton_card_last_date)->startOfDay() && Carbon::parse($user_teams->capton_card_last_date)->startOfDay() <= Carbon::now()->endOfWeek() ){
			return 2; 
		}else{
			UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])
			->update([
				'capton_card_count'=> DB::raw('capton_card_count+1'), 
				'capton_card_last_date' => Carbon::now(), 
				'dealer_card_last_date' => null,
				'twelve_man_card_last_date' => null, 
				'flipper_card_last_date' => null,
				'shield_steal_card_last_date' => null
			]);	
			$temp_team = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 

			$utl = new UserTeamLogs;
			$utl->log_type = 2;
			$utl->user_id = Auth::user()->id;
			$utl->club_id = $all_input['club_id'];
			$utl->team_id = $temp_team->id;
			$utl->save();



			return 1; 
		}
	}else{
		return 0 ; 
	}
}


public function activateTwelveManCard(Request $request){

		// echo 'hihello'; die; 
		// MAX_NO_OF_12_MAN_CARDS

	$all_input = Input::all() ; 
		// print_r($all_input['club_id']); die; 
	if (UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->exists()) {
			// auth()->guard('web')->user()->id
			// Carbon::now()->startOfWeek()

		$user_teams = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 

				// MAX_NO_OF_CAPTON_CARDS
		if($user_teams->twelve_man_card_count >= MAX_NO_OF_12_MAN_CARDS){
			return 3; 

		}else if(!empty($user_teams->twelve_man_card_last_date) && Carbon::now()->startOfWeek() <= Carbon::parse($user_teams->twelve_man_card_last_date)->startOfDay() && Carbon::parse($user_teams->twelve_man_card_last_date)->startOfDay() <= Carbon::now()->endOfWeek() ){
			return 2; 
		}else{
			UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])
			->update([
				'twelve_man_card_count'=> DB::raw('twelve_man_card_count+1'), 
				'twelve_man_card_last_date' => Carbon::now(), 
				'dealer_card_last_date' => null,
				'capton_card_last_date' => null, 
				'flipper_card_last_date'=> null,
				'shield_steal_card_last_date' => null
			]);	


			$temp_team = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 

			$utl = new UserTeamLogs;
			$utl->log_type = 3;
			$utl->user_id = Auth::user()->id;
			$utl->club_id = $all_input['club_id'];
			$utl->team_id = $temp_team->id;
			$utl->save();


			return 1; 
		}
	}else{
		return 0 ; 
	}



}

public function activateDealerCard(Request $request){
  

		// echo 'hihello'; die; 
		// MAX_NO_OF_12_MAN_CARDS

	$all_input = Input::all() ; 
		// print_r($all_input['club_id']); die; 
	if (UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->exists()) {
			// auth()->guard('web')->user()->id
			// Carbon::now()->startOfWeek()

		$user_teams = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 

				// MAX_NO_OF_CAPTON_CARDS
		if($user_teams->dealer_card_count >= MAX_NO_OF_DEALER_CARDS){
			return 3; 

		}else if(!empty($user_teams->dealer_card_last_date) && Carbon::now()->startOfWeek() <= Carbon::parse($user_teams->dealer_card_last_date)->startOfDay() && Carbon::parse($user_teams->dealer_card_last_date)->startOfDay() <= Carbon::now()->endOfWeek() ){
			return 2; 
		}else{
			UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])
			->update([
				'dealer_card_count'=> DB::raw('dealer_card_count+1'), 
				'dealer_card_status' => 0, 
				'dealer_card_minus_points'=> DB::raw('dealer_card_minus_points+50'), 
				'dealer_card_last_date' => Carbon::now(),
				'capton_card_last_date' => null, 
				'twelve_man_card_last_date' => null, 
				'flipper_card_last_date'=> null,
				'shield_steal_card_last_date' => null


			]);	


			$temp_team = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 

			$utl = new UserTeamLogs;
			$utl->log_type = 4; // dealer card 4 
			$utl->user_id = Auth::user()->id;
			$utl->club_id = $all_input['club_id'];
			$utl->team_id = $temp_team->id;
			$utl->save();


/* 
   // no need to update point deduction here, we will update it from backend (FixtureController)

        if(UserTeamsGWExtraPTTrack::where(['team_id' => $temp_team->id, 'gw_end_date' => Carbon::now()->endOfWeek()->endOfDay() ])->exists()){


    		UserTeamsGWExtraPTTrack::where('team_id', $temp_team->id)->where('gw_end_date', Carbon::now()->endOfWeek()->endOfDay())->update(['dealer_card_minus_points' => -50, 'gw_pt' => DB::raw('gw_pt-50') ]);


        }else{

          $utgweptt = new UserTeamsGWExtraPTTrack; 

          $utgweptt->team_id = $temp_team->id; 
          $utgweptt->gw_end_date = Carbon::now()->endOfWeek()->endOfDay(); 
          $utgweptt->shield_steal_type = $utl->log_type; 
          $utgweptt->gw_pt = -50; 


          $utgweptt->save(); 
          
        }


*/ 



			return 1; 
		}
	}else{
		return 0 ; 
	}




}


public function activateFlipperCard(Request $request){



		// echo 'hihello'; die; 
		// MAX_NO_OF_12_MAN_CARDS

	$all_input = Input::all() ; 
		// print_r($all_input['club_id']); die; 
	if (UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->exists()) {
			// auth()->guard('web')->user()->id
			// Carbon::now()->startOfWeek()

		$user_teams = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 

				// MAX_NO_OF_CAPTON_CARDS
		if($user_teams->flipper_card_count >= MAX_NO_OF_FLIPPER_CARDS){
			return 3; 

		}else if(!empty($user_teams->flipper_card_last_date) && Carbon::now()->startOfWeek() <= Carbon::parse($user_teams->flipper_card_last_date)->startOfDay() && Carbon::parse($user_teams->flipper_card_last_date)->startOfDay() <= Carbon::now()->endOfWeek() ){
			return 2; 
		}else{
			UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])
			->update([
				'flipper_card_count'=> DB::raw('flipper_card_count+1'), 
				'flipper_card_status'=> 0, 
				'flipper_card_last_date' => Carbon::now(),
				'capton_card_last_date' => null, 
				'twelve_man_card_last_date' => null, 
				'dealer_card_last_date' => null,
				'shield_steal_card_last_date' => null


			]);	


			$temp_team = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 

			$utl = new UserTeamLogs;
			$utl->log_type = 5; // flipper card 5
			$utl->user_id = Auth::user()->id;
			$utl->club_id = $all_input['club_id'];
			$utl->team_id = $temp_team->id;
			$utl->save();


			return 1; 
		}
	}else{
		return 0 ; 
	}






}



public function activateShieldStealCard(Request $request){



		// echo 'hihello'; die; 
		// MAX_NO_OF_12_MAN_CARDS

	$all_input = Input::all() ; 

 return $all_input['power_type']; die; 

		// print_r($all_input['club_id']); die; 
	if (UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->exists()) {
			// auth()->guard('web')->user()->id
			// Carbon::now()->startOfWeek()

		$user_teams = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 

				// MAX_NO_OF_CAPTON_CARDS
		if($user_teams->shield_steal_card_count >= MAX_NO_OF_FLIPPER_CARDS){
			return 3; 

		}else if(!empty($user_teams->shield_steal_card_last_date) && Carbon::now()->startOfWeek() <= Carbon::parse($user_teams->shield_steal_card_last_date)->startOfDay() && Carbon::parse($user_teams->shield_steal_card_last_date)->startOfDay() <= Carbon::now()->endOfWeek() ){
			return 2; 
		}else{
			UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])
			->update([
				'shield_steal_card_count'=> DB::raw('shield_steal_card_count+1'), 
				// 'shield_steal_card_status'=> 0, 
				'shield_steal_card_last_date' => Carbon::now(),
				'capton_card_last_date' => null, 
				'twelve_man_card_last_date' => null, 
				'dealer_card_last_date' => null,
				'flipper_card_last_date' => null
			]);	


			$temp_team = UserTeams::where(['user_id' => Auth::user()->id, 'club_id' => $all_input['club_id']])->first(); 




			$utl = new UserTeamLogs;
			if($all_input['power_type'] == 'shield'){
				$utl->log_type = 6; // Shield  6
			}else{  // if not shield then user_id will come to steal point from
				$utl->log_type = 7; // Steal  7
				$utl->steal_team_id = $all_input['power_type']; // team id from which point to be steal
			}
			$utl->user_id = Auth::user()->id;
			$utl->club_id = $all_input['club_id'];
			$utl->team_id = $temp_team->id;
			$utl->save();



/* 
   // no need to update point deduction here, we will update it from backend (FixtureController)

		// update Shield/Steal information in table UserTeamsGWExtraPTTrack

        if(UserTeamsGWExtraPTTrack::where(['team_id' => $temp_team->id, 'gw_end_date' => Carbon::now()->endOfWeek()->endOfDay() ])->exists()){


		UserTeamsGWExtraPTTrack::where('team_id', $temp_team->id)->where('gw_end_date', Carbon::now()->endOfWeek()->endOfDay())->update(['shield_steal_type' => $utl->log_type ]);


        }else{

        	$utgweptt = new UserTeamsGWExtraPTTrack; 

        	$utgweptt->team_id = $temp_team->id; 
        	$utgweptt->gw_end_date = Carbon::now()->endOfWeek()->endOfDay(); 
        	$utgweptt->shield_steal_type = $utl->log_type; 


        	$utgweptt->save(); 
          
        }

*/ 





			return 1; 
		}
	}else{
		return 0 ; 
	}






}



public function availability(){
	$club_id = \Session::get('sess_club_name');
	$club = new User();
	$seniorClub = $club->getClubList();
	$gameName = User::where(['club_type'=>1])
	->orderBy('club_name','ASC')
	->orderBy('created_at','DESC')
	->value('game_name');

	$sortBy=$query_string ='';

	$sort_by_data = Input::get("sortBy");
	$search       = Input::get("search");
	$sortBy       = (Input::get('sortBy')) ? Input::get('sortBy') : 'id';
	$order        = (Input::get('order')) ? Input::get('order')   : 'DESC'; 
	$data = []; 
	if(!empty($club_id)){
		$data = Availability::where('is_active',1)->where('club', $club_id)
				// ->where("availabilities.player_name",'like','%'.$search.'%')
		->whereHas('player_data', function($q) use ($search){
			$q->where('first_name', 'like', '%'.$search.'%');
		})
		->with('player_data')
		->orderBy($sortBy,$order)
		->get();

	}
	 //    else{
		// $data = Availability::where('is_active',1)
		// 		// ->where("availabilities.player_name",'like','%'.$search.'%')
		// 		->whereHas('player_data', function($q) use ($search){
		// 		    $q->where('first_name', 'like', '%'.$search.'%');
		// 		})
		// 		->with('player_data')
		// 		->orderBy($sortBy,$order)
		// 		->get();

	 //    }

		// print_r($data); die; 
		// $complete_string		=	Input::query('search');
		// $query_string			=	http_build_query($complete_string);

	return View::make('front.user.availability',compact('data','seniorClub','gameName','sortBy','query_string','order','search'));
}


public function getPlayerData(){
	$club_id = Session::get('sess_club_name'); 
	$all_input = Input::all() ; 

	$fixture_card = FixtureScorcard::where('player_id', $all_input['player_id'])->get(); 
	$fantasy_points = $fixture_card->sum('fantasy_points');  


	$player_data = Player::leftJoin('dropdown_managers','dropdown_managers.id','=','players.bat_style')
	->leftJoin('dropdown_managers as dm','dm.id','=','players.bowl_style')
	->leftJoin('teams','teams.id','=','players.team_id')
	->select('players.*','dropdown_managers.name as player_bat_style','dm.name as player_bowl_style','teams.name as team_name')
	->where('players.id', $all_input['player_id'])
	->first(); 
	$playr_club_name = ''; 
	$playr_game_name = ''; 
	$playr_bat_style = ''; 
	$playr_bowl_style = ''; 
	$playerPosition = ''; 
	if(!empty($player_data->player_bat_style)){
		$playr_bat_style = $player_data->player_bat_style;
	}if(!empty($player_data->player_bowl_style)){
		$playr_bowl_style = $player_data->player_bowl_style;
	}if(!empty($player_data->position)){
		$drop_down =	new DropDown();
		$position =	$drop_down->get_master_list("position");
		$playerPosition = $position[$player_data->position];
	}
	if($player_data->club)
		$playr_club_name = User::where('id', $player_data->club)->first()->club_name; 
	$playr_game_name = User::where('id', $player_data->club)->first()->game_name; 

	$playerClubMode = User::where('id',$player_data->club)->value('game_mode');
	$clubMode = "";
	if(!empty($playerClubMode)){
		$clubMode = Config::get('home_club')[$playerClubMode];
	}
	$player_name = $player_data->full_name; 
	if(!empty($player_data->image) && File::exists(PLAYER_IMAGE_ROOT_PATH.$player_data->image)){
		$player_image = $player_data->image; 
	}else{
		$player_image = $player_data->position;
	}
	$drop_down =	new DropDown();
	$svalue	=	$drop_down->get_master_list("svalue");
	$svalue = Config::get('player_price_list'); 
	$player_price = $player_data->svalue;
	$player_team = $player_data->team_id;
	$player_team_name = $player_data->team_name;

	    // $team_selected = UserTeamPlayers::where('player_id',  $all_input['player_id'])->pluck('team_id')->toArray(); 

	    // $team_selected = array_unique($team_selected); playr_club_name player_image
	    // $team_selected = count($team_selected); 
	$team_selected = UserTeams::where('club_id', $club_id)->whereHas('getTeamPlayer', function($q) use ($all_input) {
		$q->where('player_id', $all_input['player_id']);
	})->count();

	$tot_team = UserTeams::where('club_id', $club_id)->count(); 

	$cap_count = 0; // UserTeams::where('club_id', $club_id)->where('capton_id', $all_input['player_id'])->count(); 
	$vc_count = 0; // UserTeams::where('club_id', $club_id)->where('vice_capton_id', $all_input['player_id'])->count(); 



	$selected_by = 0;



	if($team_selected > 0 && ($team_selected - ($cap_count + $vc_count) > 0) && $tot_team > 0){
		$selected_by = (100 * ($team_selected - ($cap_count + $vc_count))) / $tot_team  ; 
		$selected_by = round($selected_by); 
	}

	   //  if($tot_team > 0){
		 	// $selected_by = round(($team_selected/$tot_team)*100); 
	   //  }
			$last_5_record = FixtureScorcard::with('fixture.teamdata', 'player')->where('player_id', $all_input['player_id'])->orderBy('id', 'desc')->take(10)->get(); // ->where('status', 3)

		// print_r($last_5_record); die; 

			$temp = array(); 
			$i = 0; 
			foreach ($last_5_record as $key => $value) {

				$tpsl_val =PlayerSvalueLog::where('player_id', $value->player->id)->where('created_at', '>=', $value->created_at)->orderBy('id', 'asc')->first(); 

				if(empty($tpsl_val)){
					$tpsl_val =PlayerSvalueLog::where('player_id', $value->player->id)->where('created_at', '<=', $value->created_at)->orderBy('id', 'desc')->first(); 
				}

				$temp[$i]['team_name'] = $value->fixture->teamdata->name; 
				$temp[$i]['runs'] = $value->rs ; 
				$temp[$i]['four'] =  $value->fours ; 
				$temp[$i]['six'] =  $value->sixes ; 
				$temp[$i]['maiden'] =  $value->mdns ; 
				$temp[$i]['wks'] =  $value->wks ; 
				$temp[$i]['fp'] =  $value->fantasy_points ; 
				$temp[$i]['fv'] = empty($tpsl_val->svalue) ? 0 : $tpsl_val->svalue  ; //$value->player->svalue ; 
				$i++; 
			}
			$last_5_record = $temp; 

			$last_10_record = FixtureScorcard::where('player_id', $all_input['player_id'])->orderBy('id', 'desc')->take(10)->get(); // ->where('status', 3)
			$fx_month = array(); 
			$fx_month_fp = array(); 
			$i = 0; 
			foreach ($last_10_record as $key => $value) {
				$fx_month[$i] = $value->created_at->format('M') ; 
				$fx_month_fp[$i] = $value->fantasy_points ; 
				$i++; 
			}
			$last_10_record = array('fx_month' => $fx_month, 'fx_month_fp' => $fx_month_fp); 
		// print_r($last_5_record); 
		// print_r($last_10_record); die; 
			$result = array('fantasy_points' => $fantasy_points, 'player_name' => $player_name, 'player_image' => $player_image, 'player_price' => $player_price, 'last_5_record' => $last_5_record, 'last_10_record' => $last_10_record, 'selected_by' => $selected_by,'club_mode'=>$clubMode, 'playr_club_name' => $playr_club_name,'playr_game_name'=>$playr_game_name,'playr_bat_style'=>$playr_bat_style,'playr_bowl_style'=>$playr_bowl_style,'playerPosition'=>$playerPosition,'player_team_name'=>$player_team_name); 

			return  response()->json($result);

		}
		public static function updateProfile(){ 
			$countryObj	= new Country();
			$dropdown =	new DropDown();
			$countryList =	$countryObj->get_country();
			$positionList = $dropdown->get_master_list('position');
			$aboutUs = $dropdown->get_master_list('about-us');
			$userDetails = User::where('id',auth()->guard('web')->user()->id)->first();
			$club = new Club();
		// dump($userDetails->game_mode); die; 
		$seniorClub = $club->get_club_mode(1); // $userDetails->game_mode
		$juniorClub = $club->get_club_mode(2);
		$leagueClub = $club->get_club_mode(3);
		// dump($juniorClub); dump($leagueClub); die; 
		//prd($userDetails);
		return View::make('front.user.update_profile',compact('countryList','positionList','userDetails','seniorClub','aboutUs', 'juniorClub', 'leagueClub'));
	}

	public function saveUpdateProfile(){
		Input::replace($this->arrayStripTags(Input::all()));
		$formData			=	Input::all();
		$login_user		 	= 	Auth::user();
		$user_id		 	=   auth()->guard('web')->user()->id;
		$validator = Validator::make(
			Input::all(),
			array(
				//'club_name'	=> 'required',
				'first_name'=> 'required',
				'my_team_name' => 'bail|required|max:35|regex:/(^[A-Za-z0-9 ]+$)+/',
				'my_club_name' => 'required',
				'my_game_mode' => 'required',
				'email'=> 'required',
				'last_name'	=> 'required',
				'country'	=> 'required',
				'state'	=> 'required',
				'city'	=> 'required',
				'upload_file' => 'required',
				'email' => "required|email|unique:users,email,$user_id",
			),
			['upload_file.required'=>"Please upload image.", 'my_team_name.max' => 'Team name must be less than 35 characters.', 'my_team_name.regex' => 'Only alphabets and numbers are allowed in Team name.', 'my_team_name.required' => 'Team name is required.']
		);
		
		if ($validator->fails()){
			$response	=	array(
				'success' 	=> false,
				'errors' 	=> $validator->errors()
			);
			return Response::json($response); 
			die; 
		}else{
			$obj = 	User::find($user_id);
			$obj->first_name =  	ucfirst(Input::get('first_name'));
			$obj->last_name =  	ucfirst(Input::get('last_name'));
			$obj->full_name =   $obj->first_name." ".$obj->last_name;
			$obj->email =   Input::get('email');
			/*$obj->phone =    !empty(Input::get('phone')) ? Input::get('phone') :0;*/
			$obj->country =  !empty(Input::get('country')) ? Input::get('country') :0;
			$obj->state =  !empty(Input::get('state')) ? Input::get('state') :0;
			$obj->city =   !empty(Input::get('city')) ? Input::get('city') :'';
			$obj->game_mode =  !empty(Input::get('my_game_mode')) ? Input::get('my_game_mode') :'';
			$obj->my_team_name =  !empty(Input::get('my_team_name')) ? Input::get('my_team_name') :'';
			$obj->senior_club_name =   !empty(Input::get('my_club_name')) ? Input::get('my_club_name') : 0;
			$obj->junior_club_name =   !empty(Input::get('junior_club_name')) ? Input::get('junior_club_name') :'';
			$obj->league_club_name =   !empty(Input::get('league_club_name')) ? Input::get('league_club_name') :'';
			$obj->is_subscribe		=  !empty(Input::get('newslater')) ? 1 :0;
			$obj->lockout_notification		=  !empty(Input::get('lockout_notification')) ? 1 :0;
			$obj->trading_lockout_notification =  !empty(Input::get('trading_lockout_notification')) ? 1 :0;
			//$obj->game_name =   !empty(Input::get('game_name')) ? Input::get('game_name') :'';
			// $obj->dob =    !empty(Input::get('dob')) ? Input::get('dob') :null;
			$obj->dob =    !empty(Input::get('dob')) ? Carbon::createFromFormat('d/m/Y', Input::get('dob'))  :null;
			
			if(!empty($obj->my_team_name)){
				UserTeams::where('user_id',  $user_id)->update(['my_team_name' => $obj->my_team_name]);
			}


			if(!empty(Input::get('is_avatar')) && Input::get('file_option')==2){
				$obj->image = Input::get('is_avatar');
				$obj->is_avatar = 1;
			}if(Input::get('file_option')==1){
				$obj->is_avatar = 0;
			}

			$obj->gender =   !empty(Input::get('gender')) ? Input::get('gender') :'';
			$obj->about_us =    !empty(Input::get('about_us')) ? Input::get('about_us') :'';
			$obj->is_profile_completed =   1;
			if(Input::hasFile('image')){
				$extension 			=	Input::file('image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	USER_PROFILE_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-user_profile.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('image')->move($folderPath, $userImageName)){
					$obj->image		=	$image;
					$obj->is_avatar = 0;
				}
			}

			/*if(input::hasFile('club_logo')){
				$userDetails	=	User::findOrFail($user_id); 
				if(File::exists(CLUB_IMAGE_ROOT_PATH.$obj->club_logo)){
					@unlink(CLUB_IMAGE_ROOT_PATH.$obj->club_logo);
				}
				$extension 			=	Input::file('club_logo')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	CLUB_IMAGE_ROOT_PATH.$newFolder;   
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-club.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('club_logo')->move($folderPath, $userImageName)){
					$obj->club_logo		=	$image;
				}
			}*/
			$obj->save();
			$user = User::find($user_id);
			Auth::guard('web')->setUser($user);
			$response	=	array(
				'success' 	=>	'1',
				'errors' 	=>	 trans("User profile updated successfully.")
			); 
			Session::flash('flash_notice', trans("User profile updated successfully."));
			return  Response::json($response); 
			die;
		}
	}











} 

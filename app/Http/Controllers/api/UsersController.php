<?php
/**
 * ApisController Controller
 */
namespace App\Http\Controllers\api;
use App\Http\Controllers\BaseController;
use App\Model\Product;
use App\Model\ProductDescription;
use App\Model\Banner;
use App\Model\Category;
use App\Model\User;
use App\Model\Tv;
use App\Model\TvDescription;
use App\Model\Wishlist;
use App\Model\FavoriteProduct;
use App\Model\FollowCelebrity;
use App\Model\UserWalletTransaction;
use App\Model\Brand;
use App\Model\EmailAction;
use App\Model\Celebrity;
use App\Model\EmailTemplate;
use App\Model\CelebrityVideo;
use App\Model\CelibrityProduct;
use App\Model\CelebrityEarning;
use App\Model\OrderDetail;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App,Auth,Config,Cache,Cookie,DB,File,Ajax,Hash,Mail,Redirect,Response,Session,URL,Validator;

class UsersController extends BaseController {

	public function celebrities(){
		## Celebrities Details ##
		
		/*$limit 					= 	Config::get('Reading.record_front_per_page');
		$serachData  = Input::all();
		Session::put('searchQuery','');
		if(!empty($serachData['page']))
		{
			$offset = $serachData['page'];
		} else {
			$offset 				= 	0;
		} */
		
		$celebrityObj 				= 	new Celebrity();
		$celebrityDetail			=	$celebrityObj->getMobileCelebrityListnig();
		
		$response	=	array(
			
			'celebrity_path'	=>  USER_PROFILE_IMAGE_URL,
			'status'		=>	1,
			'data'		=>	$celebrityDetail,
		); 
		return  Response::json($response); 
		die;
	}
	
	public function userWishlist($id = null)
	{
		$lang        =         App::getLocale();
		$langCode = DB::table('languages')->where('lang_code',$lang)->value('id');
		
		// Get user wishlists
		$customer_wishlists = Wishlist::select('wishlists.id as wishlistid','products.*',DB::raw("(select title from brand_descriptions where brand_id=(select brand_id from products where products.id = wishlists.product_id ) AND language_id='$langCode') as brand_title"))
									->join('products', 'wishlists.product_id', '=','products.id')
									->where('user_id',$id)
									->where('status',1)
									->get();
	    if(!empty($customer_wishlists)) {
			
			$response	=	array(
						'product_path'	=>  PRODUCTS_IMAGE_URL,
						'status' 	=>	1,
						'wishlist'	=> $customer_wishlists,
						'msg' 		=>	trans('messages.home.wishlist_found')
					); 			 
		}else{
			$response	=	array(
					'status' 	=> 0,
					'msg' 	=> trans('messages.global.no_record_found_message')
				);
		}
		return  Response::json($response);
	
	}
	
	public function userFavoriteList($id = null)
	{
		$lang        =         App::getLocale();
		$langCode = DB::table('languages')->where('lang_code',$lang)->value('id');
		
		$customer_favoritelist = FavoriteProduct::select('favorite_products.id as favoriteProductsid','products.*',DB::raw("(select title from brand_descriptions where brand_id=(select brand_id from products where products.id = favorite_products.product_id ) AND language_id='$langCode') as brand_title"))->join('products', 'favorite_products.product_id', '=','products.id')
									->where('user_id',$id)
									->where('status',1)
									->get();
	    if(!empty($customer_favoritelist)) {
			
			$response	=	array(
						'product_path'	=>  PRODUCTS_IMAGE_URL,
						'status' 	=>	1,
						'favoriteList'	=> $customer_favoritelist,
						'msg' 		=>	trans('messages.home.favoritelist_found')
					); 			 
		}else{
			$response	=	array(
					'status' 	=> 0,
					'msg' 	=> trans('messages.global.no_record_found_message')
				);
		}
		return  Response::json($response);
	
	}
	
	
	/** 
	*  Function :followCelebrity
	*  Purpose: This function is used to For Follow and UnFollow Celebrity
	*  Pre-condition:  None
	*  Post-condition: None. 
	*  Parameters: 
	*  Returns: void
	*/
	public function followCelebrity(){
		Input::replace($this->arrayStripTags(Input::all()));
		$formData	=	Input::all();
		$celebrity_id = Input::get('celebrity_id');
		//$action_type = Input::get('action_type');
		$user_id = Input::get('user_id');
		
		$UserFollow = DB::table('follow_celebrities')->where('user_id',$user_id)->where('celebrity_id',$celebrity_id)->first();
		//pr($product_id); die;
		
		if(empty($UserFollow)){
			if(Input::get('followStatus')==1){
				
				   $obj 			 	=  new FollowCelebrity;
				   $obj->user_id		=  $user_id ;
				   $obj->celebrity_id	 	=  $celebrity_id ;
				   $obj->status			=  1;
				   $obj->save();
				   
				$response	=	array(
						'status' 	=>	1,
						'followStatus' => 0,
						'msg' 		=>	trans('messages.home.celebrity_followed')
					);
				}
				
		}else{
			 if(Input::get('followStatus')==0){
				 
				 //$UserFaviourite->delete();
				 DB::table('follow_celebrities')->where('user_id',$user_id)->where('celebrity_id',$celebrity_id)->delete();
				
				$response	=	array(
						'status' 	=>	1,
						'followStatus' => 1,
						'msg' 		=>	trans('messages.home.celebrity_un_followed')
					);
			 }  
		}
		return  Response::json($response);
	}
	
	public function userFollowList($id = null)
	{

		$followers = FollowCelebrity::with('getCelebrityDesription')->select('follow_celebrities.*',DB::raw("(select image from celebrities where id=follow_celebrities.celebrity_id) as image"))->where('user_id',$id)->get();
		
		 if(!empty($followers)) {
			
			$response	=	array(
						'celebrity_path'	=>  USER_PROFILE_IMAGE_URL,
						'status' 	=>	1,
						'followList'	=> $followers,
						'msg' 		=>	trans('messages.home.followlist_found')
					); 			 
		}else{
			$response	=	array(
					'status' 	=> 0,
					'msg' 	=> trans('messages.global.no_record_found_message')
				);
		}
		return  Response::json($response);
		
		//return  Response::json($followers);
	}
	
	public function userWalletTransactions($id = null)
	{
		
		if (empty($id)){
			$response	=	array(
					'status' 	=> 0,
					'errors' 	=> trans('messages.home.you_are_not_valid_user')
				);
		}else{
			$customer_wallets = DB::table('user_wallet_transactions')->where('user_id',$id)->get();

			$customer_balance = DB::table('user_wallets')->where('user_id',$id)->first();
		    if(!empty($customer_wallets)) {
				
				$response	=	array(
							'status' 	=>	1,
							'transactionList' => $customer_wallets,
							'balance' => $customer_balance,
							'msg' 		=>	trans('messages.home.walletlist_found')
						); 			 
			}else{
				$response	=	array(
						'status' 	=> 0,
						'msg' 	=> trans('messages.global.no_record_found_message')
					);
			}
		}
		return  Response::json($response);
	}

	public function userWalletBalance($id = null)
	{
		if (empty($id)){
			$response	=	array(
					'status' 	=> 0,
					'errors' 	=> trans('messages.home.you_are_not_valid_user')
				);
		}else{
			$customer_balance = DB::table('user_wallets')->where('user_id',$id)->first();
		    if(!empty($customer_wallets)) {
				
				$response	=	array(
							'status' 	=>	1,
							'balance'	=> $customer_balance,
							'msg' 		=>	trans('messages.home.walletlist_found')
						); 			 
			}else{
				$response	=	array(
						'status' 	=> 0,
						'msg' 	=> trans('messages.global.no_record_found_message')
					);
			}
		}
		return  Response::json($response);
	}
	
	public function celebrityDetails($id = null, $user_id = null){
		$lang	= 	App::getLocale();
		
		$celebrityObj 					= 	new Celebrity();
		$celebrityDesc			=	$celebrityObj->celebrityDetailsMobile($id,$user_id);
		//$celebrityDesc = User::where('id', $id)
			//			->first();
		$langCode = DB::table('languages')->where('lang_code',$lang)->value('id');
		$celebrity_products_ids = CelibrityProduct::where('user_id', $id)
									->pluck('product_id','product_id')->all();			

		$celebrityProducts 	= Product::select('products.id as id','products.main_image as main_image','products.price as price','products.discount_price as discount_price','products.category_id as category_id',DB::raw("(SELECT title FROM brand_descriptions WHERE brand_id =products.brand_id AND language_id='$langCode') as brand_title"),DB::raw("(SELECT name FROM product_descriptions WHERE product_id =products.id AND language_id='$langCode') as product_name,(select product_id from favorite_products where product_id=products.id AND user_id='".$user_id."') as favorite_product"))
								->whereIn('id', $celebrity_products_ids)
								->get();	

       $celebrityVideos = Tv::where('celebrity',$id)
								->select('tvs.*',DB::raw("(SELECT title FROM tv_descriptions WHERE tv_id =tvs.id AND language_id='$langCode') as tv_title"))->get();
		//pr($celebrityVideos); die;				
						
		$response	=	array(
			'celebrity_path'	=>  USER_PROFILE_IMAGE_URL,
			'product_path'	=>  PRODUCTS_IMAGE_URL,
			'status'		=>	1,
			'celibrity'		=>	$celebrityDesc,
			'videos'        => 	$celebrityVideos,
			'products'		=> $celebrityProducts
		); 
		return  Response::json($response); 
		die;
	}


	/** 
	*  Function : getCelebrityEarning
	*  Purpose: This function is used to get celebrity earning
	*  Parameters: 
	*  Returns: response
	*/

	public function getCelebrityEarning(Request $request)
	{
		if (!empty($request->input('celebrity_id'))) {

			$celebrityearningtrans = array();
			$celebrity_id = $request->input('celebrity_id');

			$celebrity_earning_list = CelebrityEarning::select('order_id','user_id','id','amount','created_at')->where('celebrity_id',$celebrity_id);

			if (!empty($request->input('date_to'))) {
				$todate = date("Y-m-d", strtotime($request->input('date_to')));
				$celebrity_earning_list->whereDate('celebrity_earnings.created_at','>=',$todate);
			}

			if (!empty($request->input('date_from'))) {
				$fromdate = date("Y-m-d", strtotime($request->input('date_from')));
				$celebrity_earning_list->whereDate('celebrity_earnings.created_at','<=',$fromdate);
			}

			if (!empty($request->input('date_to')) && !empty($request->input('date_from'))) {
				$todate = date("Y-m-d", strtotime($request->input('date_to')));
				$fromdate = date("Y-m-d", strtotime($request->input('date_from')));
				$celebrity_earning_list->whereBetween('celebrity_earnings.created_at', [$fromdate, $todate]);
			}

			$result = $celebrity_earning_list->get();

			if (!empty($result->toArray())) {

				foreach ($result as $celebrity) {

					$commsionamount = CelebrityEarning::where('celebrity_id',$celebrity_id)->where('order_id',$celebrity['order_id'])->sum('amount');

					$product_ids = CelebrityEarning::where('celebrity_id',$celebrity_id)->where('order_id',$celebrity['order_id'])->pluck('product_id')->all();

					$productamount = OrderDetail::whereIn('product_id',$product_ids)->where('order_id',$celebrity['order_id'])->sum('total_amount');
					
					$celebrityearningtrans[] = array(
							'id' 		=> $celebrity['id'],
							'order_number' 	=> '#'.(ORDER_ID+$celebrity['order_id']),
							'user_id' 	=> $celebrity['user_id'],
							'commision' => $commsionamount,
							'total_amount' =>$productamount,
							'date' 		=>date("d/m/Y", strtotime($celebrity['created_at']))
						);
				}
				$celebrity_total_earning = Celebrity::select('earnings')->where('id',$celebrity_id)->first();

				$celebrityearningtrans_details = array();
				foreach($celebrityearningtrans as $key => $value){
				   	if(!isset($celebrityearningtrans_details[$value['order_number']])){
				    	$celebrityearningtrans_details[$value['order_number']] = $value;
				   	}
				}

				$celebrityearningtrans_details = array_values($celebrityearningtrans_details);

				$response	=	array(
					'status'	=> 1,
					'total_earning' => $celebrity_total_earning->earnings,
					'data'		=> $celebrityearningtrans_details,
					'msg'		=> trans('messages.global.record_found_message')
				);
			}else{
				$response	=	array(
					'status'	=> 0,
					'msg'		=> trans('messages.global.no_record_found_message')
				);
			}
		}else{
			$response = array(
				'status'	=> 0,
				'msg'		=> trans('messages.id_number_is_required')
			);
		}

		return  Response::json($response);
	}
}//end Class()
<?php
/**
 * ApisController Controller
 */
namespace App\Http\Controllers\api;
use App\Http\Controllers\BaseController;
use App\Model\Product;
use App\Model\CelibrityProduct;
use App\Model\ProductDescription; 
use App\Model\ProductImage; 
use App\Model\ProductReview; 
use App\Model\Banner;
use App\Model\Category;
use App\Model\CelebrityEarning;
use App\Model\User;
use App\Model\UserAddress;
use App\Model\UserPoint;
use App\Model\UserWallet;
use App\Model\UserWalletTransaction;
use App\Model\Brand;
use App\Model\Tv;
use App\Model\MiddleBanner;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\CashRequestGift;
use App\Model\PromoCode;
use App\Model\PromoCodeTrans;
use App\Model\EmailAction;
use App\Model\Celebrity;
use App\Model\EmailTemplate;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App,Auth,Config,Cache,Cookie,DB,File,Ajax,Hash,Mail,Redirect,Response,Session,URL,Validator;

class ProductApisController extends BaseController {
	
	public function products($user_id = null){
		### Product Details ###
		$productArray = array();
		$productObj 				= 	new Product();
		$productsDetail				=	$productObj->getProductHomeListnig($user_id); 
		
		//pr($productsDetail);die;
		## Banner Details ##
		$bannerObj 					= 	new Banner();
		$bannerDetail				=	$bannerObj->getBannerListnig(); 
		
		## Category Details ##
		$categoryObj 				= 	new Category();
		$categoryDetail				=	$categoryObj->getCategoryListnig(); 
		
		## Celebrities Details ##
		$celebrityObj 				= 	new Celebrity();
		$celebrityDetail			=	$celebrityObj->getCelebrityListnig();
		
		## Middle Banner Details ##
		$middlebannerObj 				= 	new MiddleBanner();
		$middlebannerDetail			=	$middlebannerObj->getMiddleBanner();
		
		/*$middleBanner  = [];
		if(!empty($middlebannerDetail))
		{
			$middleBanner['title_1'] = $middlebannerDetail->title_1;
			$middleBanner['link_1'] = $middlebannerDetail->link_1;
			$middleBanner['image_1'] = $middlebannerDetail->image_1;
			
			$middleBanner['title_2'] = $middlebannerDetail->title_2;
			$middleBanner['link_2'] = $middlebannerDetail->link_2;
			$middleBanner['image_2'] = $middlebannerDetail->image_2;
			
		} */
		
		//pr($middlebannerDetail); die;
		## Brands Details ##
		$brandObj 					= 	new Brand();
		$brandDetail				=	$brandObj->getBrandListnig();
		
		
		## TV Details ##	
		$tvObj 							= 	new Tv();
		$tvDetail						=	$tvObj->getPopulerVideo(); 
	
		
		$response	=	array(
			'product_path'	=>  PRODUCTS_IMAGE_URL,
			'brand_path'	=>  BRAND_URL,
			'banner_path'	=>  BANNER_URL,
			'category_path'	=>  CATEGORY_URL,
			'celebrity_path'=>  USER_PROFILE_IMAGE_URL,
			'middle_banner_path'=>  MIDDLE_BANNER_URL,
			'status'		=>	1,
			'products'		=>	$productsDetail,
			'banners'		=>	$bannerDetail,
			'middle_banner'	=>	$middlebannerDetail,
			'categories'	=>	$categoryDetail,
			'celebrities'	=>	$celebrityDetail,
			'brands'		=>	$brandDetail,
			'tv_slider'		=>	$tvDetail,
			
		); 
		return  Response::json($response); 
		die;
	}

	/** 
	 * Function to used to apply Coupon code by user
	 *
	 * @param 
	 * 
	 * @return response
 	*/	

	public function applyPromoCode(Request $request){

		$validator = Validator::make(
			Input::all(),
			array(
				'coupon_code'	=> 'required'	
			)
		);
		
		if ($validator->fails()){
			$response	=	array(
				'status'	=>  0,
				'msg'		=> trans("messages.global.required")
			);
		}else{
			$coupon_code = Input::get('coupon_code');

			// Check particular coupon code is exists and active or not 
			if (PromoCode::where('coupan_code',$coupon_code)->where('is_active',1)->exists()) {
				$promocode_details = PromoCode::where('coupan_code',$coupon_code)->where('is_active',1)->first();
				// Check particular user is used coupon code or not
				if (in_array(Input::get('user_id'),explode(',', $promocode_details['user_ids'])) && $promocode_details['minimum_amount'] < Input::get('cart_amount')) {

					if ($promocode_details['type'] == 0 && !PromoCodeTrans::where('user_id',Input::get('user_id'))->where('coupon_id',$promocode_details['id'])->exists()) {
						$response = array(
							'status'	=> 1,
							'data'		=> $promocode_details,
							'msg' 		=> "Coupon Code apply successfully"
						);
					
					}elseif ($promocode_details['type'] == 1) {
						$response = array(
							'status'	=> 1,
							'data'		=> $promocode_details,
							'msg' 		=> "Coupon Code apply successfully"
						);
					
					}else{
						$response = array(
							'status'	=> 0,
							'msg' 		=> "Coupon Code already used by users"
						);
					}
				}else{
					$response = array(
							'status'	=> 0,
							'msg' 		=> "Your are not authorised person used this code or Cart Amount is less than Minimum amount."
						);
				}
			}else{
				$response = array(
							'status'	=> 0,
							'msg' 		=> "Invalid Coupon Code / Coupon code is not active."
						);
			}
		}

		return response()->json($response);
	}
	
	// complete payment
	public function saveCheckoutData(Request $request){ 
		
		$paytmentType = $request->input('payment_type');
		$totalAmount = $request->input('total_amount');
		$celebrityId = '';

		if ($request->input('is_gift') == 0) {
			$addressId = $request->input('address_id');
			$userAddress = UserAddress::where('id',$addressId)->first();
		}

		if ($request->input('is_gift') == 1) {
			$celebrityId = $request->input('celebrity_id');
			$userAddress = Celebrity::select('celebrities.full_name','celebrities.email','celebrities.address','celebrities.mobile',DB::raw("(select name from cities where cities.id = celebrities.city) as city,(select name from states where states.id = celebrities.state) as state,(select name from countries where countries.id = celebrities.country) as country"))->where('id',$celebrityId)->first();
		}

		
		if(!empty($request->input('products'))){
			$obj 						= 	new Order();
			$obj->user_id				=   $request->input('user_id');
			$obj->shipping_name			=  	$userAddress->full_name;
			$obj->shipping_email		=  	$userAddress->email;
			$obj->shipping_address		=   $userAddress->address;
			$obj->shipping_mobile		=  	$userAddress->mobile;
			$obj->shipping_country		=   $userAddress->country;
			$obj->shipping_state		=   $userAddress->state;
			$obj->shipping_city			=  	$userAddress->city;
			$obj->shipping_pincode		=   $userAddress->pincode;
			$obj->payment_type			=   $paytmentType;
			$obj->is_gift				=   $request->input('is_gift');
			$obj->gift_celebrity_id		=   !empty($celebrityId)?$celebrityId:0;
			$obj->total_amount			=  	$request->input('total_amount');
			$obj->status				=	ACTIVE;
			$obj->save();

			$id = $obj->id; 

			DB::table('orders')->where('id',$id)->update(array('order_no' => ($id+ORDER_ID)));
			//$cart_products	=	$request->input('products');
			$cart_products = json_decode($request->input('products'));

			foreach($cart_products as $key => $product){

				$order 					=  new OrderDetail();
				$order->order_id		=  $id;
				$order->user_id			=  $request->input('user_id'); 
				$order->product_id  	=  $product->product_id;
				$order->qty				=  $product->total_qty;
				$order->attributes		=  !empty($product->attribute) ? json_encode($product->attribute) : '';
				$order->price		 	=  $product->total_price;
				$order->total_amount	=  $totalAmount;
				$order ->save();
				//find prooduct Remaining Quantity 
				$productQuantity = DB::table('products')->where('id',$product->product_id)->select("products.remaining_quantity","user_points","charity_points")->first();
				
				DB::table('products')->where('id',$product->product_id)
									->update(array('remaining_quantity' => (($productQuantity->remaining_quantity)-($product->total_qty))));

				if((($productQuantity->remaining_quantity)-($product->total_qty)) == 0){
					DB::table('products')->where('id',$product->product_id)
										 ->update(array('out_of_stock' => 1));
				}
				//Celebrity Earnings
				if(!empty($product->celebrity_id)){
					$celebrityPercentage = Celebrity::where('id',$product->celebrity_id)->select('commission','earnings')->first();
					$calculateCommissionAmount = ($celebrityPercentage->commission / 100) * $totalAmount;
					$updateAmoount = ($celebrityPercentage->earnings + $calculateCommissionAmount);
					Celebrity::where('id',$product->celebrity_id)->update(['earnings'=>$updateAmoount]);
					$celebrityObj = new CelebrityEarning();
					$celebrityObj->user_id		=  $request->input('user_id');
					$celebrityObj->product_id	=  $product->product_id;
					$celebrityObj->celebrity_id	=  $product->celebrity_id;
					$celebrityObj->order_id		=  $id;
					$celebrityObj->amount		=  $calculateCommissionAmount;
					$celebrityObj->save();
				}

				// add product points to user 

				if (($productQuantity->user_points != 0) || ($productQuantity->charity_points != 0)) {
					$userpoints = new UserPoint();
					$userpoints->user_id =  $request->input('user_id');
					$userpoints->product_id = $product->product_id;
					$userpoints->order_id = $id;
					$userpoints->user_points = $productQuantity->user_points;
					$userpoints->charity_points = $productQuantity->charity_points;
					if ($userpoints->save()) {
						$useroldpoints = User::where('id', $request->input('user_id'))->select("total_user_points","total_charity_points")->first();

						$usernew_user_points = ($useroldpoints->total_user_points)+($productQuantity->user_points);
						$usernew_charity_points = ($useroldpoints->total_charity_points)+($productQuantity->charity_points);
						$updatepoint = array('total_user_points' => $usernew_user_points,'total_charity_points' => $usernew_charity_points);
						
						User::where('id', $request->input('user_id'))->update($updatepoint);
					}
				}
			}

			if ($paytmentType == 2) {
				$user_wallet_remaining_balance = 0;
				if(!empty($request->input('user_available_balance')) && !empty($request->input('total_amount'))){
					
					$user_wallet_remaining_balance = $request->input('user_available_balance') - $request->input('total_amount');

				}
				
				UserWallet::where('user_id', $request->input('user_id'))->update(['balance' => $user_wallet_remaining_balance]);
				$wallet_id = UserWallet::where('user_id', $request->input('user_id'))->value('id');

				$wallet_Trans = new UserWalletTransaction();
				$wallet_Trans->user_id =  $request->input('user_id');
				$wallet_Trans->wallet_id = $wallet_id;
				$wallet_Trans->trans_id = uniqid();
				$wallet_Trans->order_id = $id;
				$wallet_Trans->amount = $request->input('total_amount');
				$wallet_Trans->type = "Debit";
				$wallet_Trans->trans_type = "Order Payment";
				$wallet_Trans->fee = "0";
				$wallet_Trans->description = "";
				$wallet_Trans->save();
			}
			if (!empty($request->input('coupon_id'))) {
				$promo_trans = new PromoCodeTrans();
				$promo_trans->order_id = $id;
				$promo_trans->user_id =  $request->input('user_id');
				$promo_trans->coupon_id = $request->input('coupon_id');
				$promo_trans->save();
			}

			$authuser = User::select('country_code','email','mobile','full_name')->where('id',$request->input('user_id'))->first();

			if (!empty($authuser['email'])) {
				$this->ordercomplete_email($id,$authuser['full_name'],$authuser['email']);
			}

			if (!empty($authuser['mobile'])) {
				$mobile = $authuser['country_code'].''.$authuser['mobile'];
				$data = "Your order is successfully placed With order no ".($id+ORDER_ID);
				$this->sendMessages($mobile,$data);
			}
			
			$response	=	array(
					'status' 	=>	1,
					'msg' 		=>	trans('messages.home.product_checkout')
				); 
			
		}else{
			$response	=	array(
					'status' 	=>	0,
					'msg' 		=>	trans('messages.global.somethingwrong')
				);
		}
		return  Response::json($response);
	}

	public function ordercomplete_email($orderid='',$name,$email)
	{
		//mail Send To User of Order Succes
		$settingsEmail 	= Config::get('Site.email');
		$order_number	= ($orderid+ORDER_ID);

		$emailActions	= EmailAction::where('action','=','order_success_mail_user')->get()->toArray();
		$emailTemplates	= EmailTemplate::where('action','=','order_success_mail_user')->get()->toArray();
		$cons 			= explode(',',$emailActions[0]['options']);
		$constants 		= array();
		 
		foreach($cons as $key=>$val){
			$constants[] = '{'.$val.'}';
		}
		
		$subject 		= $emailTemplates[0]['subject'];
		$rep_Array 		= array($name,$order_number); 
		$messageBody	= str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
		
		//mail Send To Admin of Order Succes
		
		$adminemailActions	= EmailAction::where('action','=','order_success_mail_admin')->get()->toArray();
		$adminemailTemplates	= EmailTemplate::where('action','=','order_success_mail_admin')->get()->toArray();
		$options 			= explode(',',$adminemailActions[0]['options']);
		$constant 		= array();
		 
		foreach($options as $key=>$val){
			$constant[] = '{'.$val.'}';
		}
		
		$adminsubject 		= $adminemailTemplates[0]['subject'];
		$adminrep_Array 	= array($order_number,$name); 
		$adminmessageBody	= str_replace($constant, $adminrep_Array, $adminemailTemplates[0]['body']);

		try{
			$mail	= $this->sendMail($email,$name,$subject,$messageBody,$settingsEmail);
			$adminmail	= $this->sendMail($settingsEmail,"Admin",$adminsubject,$adminmessageBody,$email);
		}catch (Exception $e) {
		    Log::warning($e->getCode() . ' : ' . $e->getMessage());
		}

		return true;
	}

	/** 
	*  Function : getCelebrityGiftOrder
	*  Purpose: This function is used to For get order which is send as gift to celebrity
	*  Parameters: 
	*  Returns: response
	*/

	public function getCelebrityGiftOrders($celebrity_id ='')
	{
		if (!empty($celebrity_id)) {
			$orderDetailids	= Order::where('gift_celebrity_id',$celebrity_id)->where('is_gift',1)->pluck('id')->all();

			$giftproductdetails = OrderDetail::select('product_id','user_id','order_id',DB::raw("(select products.main_image from products where order_details.product_id = products.id ) as image,(select users.full_name from users where order_details.user_id = users.id ) as name,(select users.email from users where order_details.user_id = users.id ) as email"))->whereIn('order_id',$orderDetailids)->get();
			
			if (!empty($giftproductdetails->toArray())) {

				$giftorderdetils = array();
				foreach($giftproductdetails as $key => $value){
				   	if(!isset($giftorderdetils[$value['order_id']])){
				    	$giftorderdetils[$value['order_id']] = $value;
				   	}
				}

				$giftorderdetils = array_values($giftorderdetils);

				$response = array(
					'status'	=> 1,
					'product_path'	=>  PRODUCTS_IMAGE_URL,
					'data' 		=> $giftorderdetils,
					'msg' 		=> trans('messages.global.record_found_message')
				);
			}else{
				$response = array(
					'status' 	=> 0,
					'msg' 	=> trans('messages.global.no_record_found_message')
				);
			}
		}else{
			$response = array(
				'status' 	=> 0,
				'msg' 	=> trans('messages.home.you_are_not_valid_user')
			);
		}
		
		return  Response::json($response);
	}


	/** 
	*  Function : giftReedemAsCash
	*  Purpose: This function is used to convert gift into cash by celebrity
	*  Parameters: 
	*  Returns: response
	*/

	public function giftReedemAsCash(Request $request)
	{
		if (!empty($request->input('order_id')) && !empty($request->input('celebrity_id'))) {
			
			if (!CashRequestGift::where('order_id',$request->input('order_id'))->where('celebrity_id',$request->input('celebrity_id'))->exists()) {

				$cashrequest = new CashRequestGift();
				$cashrequest->order_id = $request->input('order_id');
				$cashrequest->celebrity_id = $request->input('celebrity_id');

				if ($cashrequest->save()) {
					
					$response = array(
						'status'	=> 1,
						'msg' 		=> trans('messages.request.submitted')
					);
				}else{
					$response = array(
						'status' 	=> 0,
						'msg' 	=> trans('messages.global.somethingwrong')
					);
				}
			}else{
				$response = array(
						'status' 	=> 0,
						'msg' 	=> trans('messages.record_already_exists')
					);
			}
		}else{
			$response = array(
				'status' 	=> 0,
				'msg' 	=> trans('messages.id_number_is_required')
			);
		}
		
		return  Response::json($response);
	}
	
	public function productList($user_id=null){
		$serachData  = Input::all();
		Session::put('searchQuery','');
		### Product Details ###
		$productArray = array();
		$productObj 		= 	new Product();
		$limit 				= 	Config::get('Reading.record_front_per_page');
		$serachData  = Input::all();
		Session::put('searchQuery','');
		if(!empty($serachData['page']))
		{
			$offset = $serachData['page'];
		} else {
			$offset 	= 	0;
		}
		//echo $offset; die; 
		$productsDetail			=	$productObj->getProductListnig($user_id,$limit,$offset); 

		$response	=	array(
			'product_path'	=>  PRODUCTS_IMAGE_URL,
			'status'		=>	1,
			'data'		=>	$productsDetail,
		); 
		return  Response::json($response); 
		die;
	}
	
	public function productDetails($id=null,$user_id=null){ 
		$lang	= 	App::getLocale();
		$DB		=	Product::query();
		$DB1	=	ProductDescription::query();
		$DB2	=	ProductImage::query();
		$data   =  	array();
		
		$productArray 	= 	$DB->where('is_deleted',0)
								->where('is_active',1)
								->pluck('id','id')->all();	
		$langCode = DB::table('languages')->where('lang_code',$lang)->value('id');
		$productDesc = ProductDescription::where('language_id',DB::raw("(select id from languages WHERE languages.lang_code = '$lang')"))->with('ProductImage:product_id,image')
			->select("product_descriptions.*",DB::raw("(select main_image from products where id=product_descriptions.product_id ) as image,(select slug from products where id=product_descriptions.product_id ) as slug,(select price from products where id=product_descriptions.product_id ) as price,(select discount_price from products where id=product_descriptions.product_id ) as discount_price, (select category_id from products where id=product_descriptions.product_id ) as category_id, (select long_description from products where id=product_descriptions.product_id ) as description,(select title from brand_descriptions where brand_id=(select brand_id from products where products.id=product_descriptions.product_id AND language_id='$langCode')) as brand_title,(select product_id from 	wishlists where product_id=product_descriptions.product_id AND user_id='".$user_id."') as wishlist_product,(select product_id from 	favorite_products where product_id=product_descriptions.product_id AND user_id='".$user_id."') as favorite_product"))
			->where('product_id', $id)
			->first();
			//pr($productDesc); die;
			if(!empty($productDesc)){
				$data['id'] = $productDesc->id;
				$data['product_id'] = $productDesc->product_id;
				$data['language_id'] = $productDesc->language_id;
				$data['name'] = $productDesc->name;
				$data['long_description'] = $productDesc->long_description;
				$data['brand_id'] = $productDesc->brand_id;
				$data['image'] = $productDesc->image;
				$data['price'] = $productDesc->price;
				$data['discount_price'] = $productDesc->discount_price;
				$data['category_id'] = $productDesc->category_id;
				$data['description'] = $productDesc->description;
				$data['brand_title'] = $productDesc->brand_title;
				//$data['product_size'] = !empty($productDesc->product_size) ? $productDesc->product_size : '';
				$data['product_image'] = $productDesc->ProductImage;
				$data['wishlist_product'] = $productDesc->wishlist_product;
				$data['favorite_product'] = $productDesc->favorite_product;
			}
		   
		$productObj 		= 	new Product();
		$similarProducts	=	$productObj->similarMobileProducts($productDesc->category_id, $id,$user_id);
		//pr($similarProducts); die;	
		$productAttributeDetail 	=	$productObj->productAttributeMoblie($productDesc->slug);
		
		$celebrity =  DB::table('celebrity_products')->where('product_id', $productDesc->id)->select('user_id')->first();
		
		$celebrityObj 		= 	new Celebrity();
		$CelebrityDetails = array();
		if(!empty($celebrity)){
			$CelebrityDetails[] =	$celebrityObj->celebrityDetails($celebrity->user_id);
		}

		$response	=	array(
			'product_path'	 	=> PRODUCTS_IMAGE_URL,
			'celebrity_path' 	=> USER_PROFILE_IMAGE_URL,
			'status'		 	=> 1,
			'data'			 	=> $data,
			'similarProduct' 	=> $similarProducts,
			'celebrityDetails'  => $CelebrityDetails,
			'productAttributes' => $productAttributeDetail,
		); 
		return  Response::json($response); 
	}

	public function getSortingSidebar(){
		$celebrityNames = Celebrity::with('getCelebrityDesription:celebrity_id,full_name')
									->select('celebrities.id')->where(['is_active'=>1,'is_deleted'=>0])
									->get();

		$categoryList =	Category::with("getCategoryDescription:parent_id,category_name")->select('categories.id')->get();


		$brandList = Brand::with("getBrandDescription:brand_id,title")->select('brands.id')->get();
		$data =[];
		$data['celebrity_list'] = $celebrityNames;
		$data['brand_list'] = $brandList;
		$data['category_list'] = $categoryList;
		return  Response::json($data); 
	}

	public function getCelebrityProductList($celebrity_id=null)
	{
		$lang	= 	App::getLocale();
		$langcode = Config::get('language_code')[$lang];
		$productObj = new CelibrityProduct();
		$celebrityproductlist	=	$productObj->getCelebrityProductListing($celebrity_id,$langcode);

		if (!empty($celebrityproductlist)) {
			$response	=	array(
				'product_path'	=>  PRODUCTS_IMAGE_URL,
				'status'		=>	1,
				'data'			=>	$celebrityproductlist,
				'msg'			=> trans('messages.global.record_found_message')
			); 
			
		}else{
			$response	=	array(
				'status'	=>	0,
				'msg'		=> trans('messages.global.no_record_found_message')
			); 
		}

		return  Response::json($response);
	}

	public function getProductList($celebrityid = '' , Request $request)
	{
		$lang	= 	App::getLocale();
		// Get language id 
		$langid = Config::get('language_code')[$lang];
		// Get all product with status celebrity add product or not.

		$celebrityproductlist = CelibrityProduct::pluck('product_id')->toArray();

		$productlist = Product::where('is_deleted',0)->where('is_active',1)
						->select("products.id","products.slug","products.sku_number","products.main_image","products.price","products.discount_price",DB::raw("(select name from product_descriptions where products.id=product_descriptions.product_id and product_descriptions.language_id =".$langid.") as product_name,(select title from brand_descriptions where products.brand_id=brand_descriptions.brand_id and brand_descriptions.language_id =".$langid." ) as brand_name"));

		if (!empty($celebrityproductlist)) {
			$productlist->whereNotIn('id',$celebrityproductlist);
		}
				
		if (!empty($request->input('sort'))) {
			$sorted_at = $request->input('sort');
			$productlist->orderBy('products.name',$sorted_at);
		}
		if (!empty($request->input('brand_id'))) {
			$brand_ids = $request->input('brand_id');
			$brand_id = explode(" ",trim($brand_ids,","));
			$productlist->whereIn('products.brand_id',$brand_id);
		}

		if (!empty($request->input('category_id'))) {
			$category_ids = $request->input('category_id');
			$category_id = explode(" ",trim($category_ids,","));
			$productlist->whereIn('products.category_id',$category_id);
		}

		$productlist->orderBy("products.created_at", 'DESC');
		$result =	$productlist->get();
		
		if (!empty($result->toArray())) {
			$response	=	array(
				'product_path'	=>  PRODUCTS_IMAGE_URL,
				'status'		=>	1,
				'data'			=>	$result,
				'msg'			=> trans('messages.global.record_found_message')
			); 
			
		}else{
			$response	=	array(
				'status'	=>	0,
				'msg'		=> trans('messages.global.no_record_found_message')
			); 
		}

		return  Response::json($response);
	}

	/** 
		*  Function : addCelebrityProducts
		*  Purpose: This function is used to For add product in celebrity list
		*  Parameters: 
		*  Returns: response
	*/

	public function addCelebrityProducts(Request $request)
	{
		if (!empty($request->input('celebrity_id')) && !empty($request->input('product_id'))) {
			$products = explode(",", trim($request->input('product_id'),","));
			
			foreach ($products as $product) {
				$addproducts = new CelibrityProduct();
				$addproducts->user_id = $request->input('celebrity_id');
				$addproducts->product_id = $product;
				$addproducts->save();
			}
			$response	=	array(
				'status'	=>	1,
				'msg'		=> trans('messages.celebrity.product_added')
			);
		}else{
			$response	=	array(
				'status'	=>	0,
				'msg'		=> trans('messages.celebrity.id_required')
			);
		}
		return  Response::json($response);
	}

	/** 
		*  Function : removeCelebrityProducts
		*  Purpose: This function is used to For remove product in celebrity list
		*  Parameters: 
		*  Returns: response
	*/

	public function removeCelebrityProducts(Request $request)
	{
		if (!empty($request->input('celebrity_id')) && !empty($request->input('product_id'))) {

			CelibrityProduct::where('user_id',$request->input('celebrity_id'))->where('product_id',$request->input('product_id'))->delete();
			
			$response	=	array(
				'status'	=>	1,
				'msg'		=> trans('messages.celebrity.product_removed')
			);
		}else{
			$response	=	array(
				'status'	=>	0,
				'msg'		=> trans('messages.celebrity.id_required')
			);
		}
		return  Response::json($response);
	}

	/** 
	*  Function : getCategoryDetail
	*  Purpose: This function is used get category detail like subcategory, products and video
	*  Parameters: category_id,user_id
	*  Returns: response
	*/

	public function getCategoryDetail(Request $request)
	{
		$lang	= 	App::getLocale();
		// Get language id 
		$langid = Config::get('language_code')[$lang];
		

		if (!empty($request->input('category_id'))) {
			
			$category_id = $request->input('category_id');
			$user_id	 = !empty($request->input('user_id'))?$request->input('user_id'):0;
			

			$data = array();

			$categorydetail = Category::select(DB::raw("(select category_name from category_descriptions where categories.id=category_descriptions.parent_id and language_id = ".$langid.") as category_name"))->where('id',$category_id)->where('is_active',1)->first();
			
			if (!empty($categorydetail)) {

				$subcategorydetail = Category::select('categories.id',DB::raw("(select category_name from category_descriptions where categories.id=category_descriptions.parent_id and language_id = ".$langid.") as subcategory_name"))->where('parent_id',$category_id)->where('is_active',1)->get();

				$productdetail = Product::select('products.discount_price','products.price','products.id','products.main_image',DB::raw("(select name from product_descriptions where products.id=product_descriptions.product_id and language_id = ".$langid.") as product_name,(select title from brand_descriptions where products.brand_id=brand_descriptions.brand_id and language_id = ".$langid.") as brand_name,(select product_id from favorite_products where product_id=products.id AND user_id='".$user_id."') as favorite_product"))->where('category_id',$category_id)->where('is_active',1)->orderBy('created_at','DESC')->get();

				$video = Tv::select('tvs.video','tvs.id','tvs.video_thumb',DB::raw("(select title from  tv_descriptions where tvs.id= tv_descriptions.tv_id and language_id = ".$langid.") as title"))->where('category_id',$category_id)->where('is_active',1)->orderBy('created_at','DESC')->get();

				$data['category']['name'] = $categorydetail['category_name'];
				$data['category']['subcategory'] = $subcategorydetail;
				$data['product']['imagepath'] = PRODUCTS_IMAGE_URL;
				$data['product']['productdetail'] = $productdetail;
				$data['video'] = $video;

				$response	=	array(
					'status'	=>	1,
					'msg'		=> trans('messages.global.record_found_message'),
					'data'		=> $data
				);

			}else{

				$response	=	array(
					'status'	=>	0,
					'msg'		=> trans('messages.global.no_record_found_message')
				);
			}
		}else{
			$response	=	array(
				'status'	=>	0,
				'msg'		=> trans('messages.id_number_is_required')
			);
		}

		return  Response::json($response);
	}

	/** 
	*  Function : getUserReviews
	*  Purpose: This function is used for get product review of a Particular user
	*  Parameters: category_id,user_id
	*  Returns: response
	*/

	public function getUserReviews($user_id)
	{
		$lang	= 	App::getLocale();
		// Get language id 
		$langcode = Config::get('language_code')[$lang];

 		$user_reviews = ProductReview::where('user_id',$user_id)
 					->select('product_reviews.rating','product_reviews.review','product_reviews.id',DB::raw("(select name from product_descriptions WHERE product_descriptions.product_id = product_reviews.product_id and product_descriptions.language_id = '$langcode') as product_name,(select main_image from products WHERE product_reviews.product_id = products.id) as image"))
		 			->orderBy('product_reviews.created_at','DESC')
		 			->get();

    	if (!empty($user_reviews->toArray())) {
    		$response = array(
					'status'		=> 1,
					'product_path'	=>  PRODUCTS_IMAGE_URL,
					'data' 			=> $user_reviews,
					'msg' 			=> trans('messages.global.record_found_message')
				);
    	}else{

    		$response = array(
					'status' 	=> 0,
					'msg' 	=> trans('messages.global.no_record_found_message')
				);
    	}

    	return  Response::json($response);
	}
}//end Class()
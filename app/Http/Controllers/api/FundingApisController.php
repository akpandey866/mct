<?php

/**
* FundingApisController Controller
*/

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Model\User;
use App\Model\FundMember;
use App\Model\FundDetail;
use App\Model\EmailAction;
use App\Model\EmailTemplate;
use Illuminate\Support\Facades\Input;
use App,Auth,Config,Cache,Cookie,DB,File,Ajax,Hash,Mail,Redirect,Response,Session,URL,Validator,Log;

class FundingApisController extends BaseController {

	
	/**
     * Instantiate a new controller instance.
     *
     * @return void
     */

	public function __construct(){}

	/** 
	 * Function Used for show funding list
	 *
	 * @param null
	 * 
	 * @return view page
	*/	

	public function funding_list(Request $request)
	{
		if (!empty($request->get('user_id'))) {
			$getfundlists = FundMember::select("fund_id",DB::raw("(select total_fund_amount from funding_details where id = fund_members.fund_id) as total_fund_amount,(select total_member from funding_details where id = fund_members.fund_id) as total_member,(select fund_start_date from funding_details where id = fund_members.fund_id) as fund_start_date,(select fund_end_date from funding_details where id = fund_members.fund_id) as fund_end_date,(select status from funding_details where id = fund_members.fund_id) as status"))->where('user_id',$request->get('user_id'))->get();

			$response	=	array(
				'status' => 1,
				'data'	 => $getfundlists,
				'msg' 	 => trans('messages.global.record_found_message')
			);
		}else{

			$response	=	array(
				'status' => 0,
				'msg' 	 => trans('messages.home.user_id_is_required')
			);
		}

		return Response::json($response);
	}

	public function createFunding(Request $request)
	{
		Validator::extend('total_users_count', function($attribute, $value, $parameters) {
			$members = Input::get('total_member');
			if (count($value) == $members) {
				return true;
			} else {
				return false;
			}
		});

		$validator   = Validator::make(
        	$request->all(),array(
        		'total_amount' => 'required|numeric',
        		'total_member' => 'required|numeric',
        		'payment_type' => 'required',
        		'user'		   => 'required|total_users_count',
        	),
        	array(
        		'total_amount.required'  => trans('messages.global.required'),
        		'total_amount.numeric' 	 => trans('messages.msg.amount_field_is_numeric'),
        		'total_member.required'  => trans('messages.global.required'),
        		'total_member.numeric'   => trans('messages.msg.amount_field_is_numeric'),
        		'payment_type.required'  => trans('messages.global.required'),
        		'user.total_users_count' => trans('messages.funding.total_member_count')
        	)
        );
        if($validator->fails()){
        	$errors 	=	$validator->messages();
        	$response	=	array(
				'status' => 0,
				'msg' 	 => trans('messages.global.somethingwrong'),
				'error'  => $errors
			);
		}else {

			$funddetails = new FundDetail();

			$per_person_fund = ($request->input('total_amount')/$request->input('total_member'));

			$start_date = date('Y-m-d');
			$total_members = $request->input('total_member');
			$paymenttype = $request->input('payment_type');

			if ($paymenttype == Config::get('fund_payment_type.Weekly')) {
				$totaldays = (7*$total_members);
				// if start date today than end date will total member multply by 7
				$end_date = date('Y-m-d', strtotime("+".($totaldays)." days", strtotime($start_date)));
			}

			if ($paymenttype == Config::get('fund_payment_type.Monthly')) {
				// if start date today than end date will total member
				$end_date = date('Y-m-d', strtotime("+".$total_members." months", strtotime($start_date)));
			}

			$funddetails->total_fund_amount	= $request->input('total_amount');
			$funddetails->total_member 		= $total_members;
			$funddetails->payment_type 		= $request->input('payment_type');
			$funddetails->amount_per_member	= $per_person_fund;
			$funddetails->fund_start_date 	= $start_date;
			$funddetails->fund_end_date 	= $end_date;

			if ($funddetails->save()) {

				$fundid = $funddetails->id;
				$members = $request->input('user');

				foreach ($members as $key => $value) {

					$fund_members = new FundMember();

					$fund_members->fund_id = $fundid; 
					$fund_members->user_id = $value['user_id']; 
					$fund_members->fund_get_position = $value['user_rank'];

					$member_position = $value['user_rank'];

					if ($value['user_rank'] == 1) {
						// user id of user who get fund in 1 position
						$fundget_userid = $value['user_id'];
					}

					// payment type  weekly 
					if ($paymenttype == Config::get('fund_payment_type.Weekly')) {
						
						$totaldays = (($member_position-1)*7);
						// If member position is 1 than start date is today else start date is next date end date of previews member.
						$payment_start = date('Y-m-d', strtotime("+".$totaldays." days"));
						
						// if member position is 1 than end date is date after 7 days else end date is member position multiply by 7.
						$payment_end = date('Y-m-d', strtotime("+6 days", strtotime($payment_start)));
					}
					// payment type  Monthly 
					if ($paymenttype == Config::get('fund_payment_type.Monthly')) {
						
						// If member position is 1 than start date is today else start date is next date of end date of previews member.
						$payment_start = date('Y-m-d', strtotime("+".($member_position-1)." months"));

						// if member position is 1 than end date is date after 1 month else end date is affter member position month.
						$payment_end = date('Y-m-d', strtotime("+".$member_position." months -1 days"));
					}

					$fund_members->payment_start_date 	= $payment_start;
					$fund_members->payment_end_date 	= $payment_end;
					$fund_members->is_link_send 		= '1';
					if (!$fund_members->save()) {
						FundDetail::where('id', $fundid)->delete();
						$response	=	array(
							'status' => 0,
							'msg' 	 => trans('messages.global.somethingwrong')
						);
						return Response::json($response);
					}else{
						$data = "fund_id==".$fundid."&&fund_member_id==".$value['user_id']."&&payment_month==".date("Fy")."&&fund_amount==".$per_person_fund."&&fund_get_user_id==".$fundget_userid;
						$encodeddata = base64_encode($data);

						$link = "<a href='".route('Funding.payment',$encodeddata)."'>Click Here </a>";

						$this->sendPaymentLinkMail($value['user_id'],$link);
					}
				}
				
				$response	=	array(
					'status' => 1,
					'msg' 	 => trans('messages.funding.create_successfully')
				);
			}else{
				$response	=	array(
					'status' => 0,
					'msg' 	 => trans('messages.global.somethingwrong')
				);
			}
		}
		return Response::json($response);
	}

	/** 
	 * Function Used for send Email with link to users.
	 *
	 * @param null
	 * 
	 * @return null
	*/

	public function sendPaymentLinkMail($user_id = '',$link = '')
	{
		// Send mail to users

		$userdetails = User::Select('email','full_name')->where('id',$user_id)->first();

		$to 			=  $userdetails['email'];
		$settingsEmail 	=  Config::get('Site.email');
		$to_name 		=  $userdetails['full_name'];

		$emailActions		=	EmailAction::where('action','=','send_fund_payment_link')->get()->toArray();
		$emailTemplates		=	EmailTemplate::where('action','=','send_fund_payment_link')->get(array('name','subject','action','body'))->toArray();
		$cons = explode(',',$emailActions[0]['options']);
		$constants = array();
		
		foreach($cons as $key=>$val){
			$constants[] = '{'.$val.'}';
		}

		$subject 		=  $emailTemplates[0]['subject'];
		$rep_Array 		=  array($to_name,$link,date("Fy")); 
		$messageBody	=  str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
		
		
		try{
			$this->sendMail($to,$to_name,$subject,$messageBody,$settingsEmail);
		}catch (Exception $e) {
		    Log::warning($e->getCode() . ' : ' . $e->getMessage());
		}

		return true;
	}

	public function funding_member(Request $request)
	{
		if (!empty($request->get('user_id'))) {

			$id = $request->get('user_id');
			
			$userlists = User::select("full_name",'id')->where([
								    ['is_deleted', '=', 0],
								    ['user_role_id', '!=',1],
								    ['is_active', '=',1],
								    ['id', '!=',$id],
								])->get();

			if (!empty($userlists)) {
				
				$response	=	array(
						'status' => 1,
						'data'	 => $userlists,
						'msg' 	 => trans('messages.global.record_found_message')
					);
			}else{

				$response	=	array(
					'status' => 0,
					'msg' 	 => trans('messages.global.no_record_found_message')
				);
			}
		}else{
			$response	=	array(
					'status' => 0,
					'msg' 	 => trans('messages.home.user_id_is_required')
				);
		}	

		return Response::json($response);
	}
}
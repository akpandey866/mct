<?php
/**
 * ApisController Controller
 */
namespace App\Http\Controllers\api;
use App\Http\Controllers\BaseController;
use App\Model\Product;
use App\Model\Banner;
use App\Model\Category;
use App\Model\ContactUs;
use App\Model\User;
use App\Model\Brand;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\UserAddress;
use App\Model\Tv;
use App\Model\Celebrity;
use App\Model\CelebrityEarning;
use App\Model\BrandDescription; 
use App\Model\EmailAction;
use App\Model\EmailTemplate;
use App\Model\FavoriteProduct;
use App\Model\Wishlist;
use App\Model\Gift;
use App\Model\CelebrityNomination;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App,Auth,Config,Cache,Cookie,DB,File,Ajax,Hash,Mail,Redirect,Response,Session,URL,Validator;

class ApisController extends BaseController {
	
	
	public function categories(){
		
		## Category Details ##
		$categoryObj 		= 	new Category();
		$categoryDetail		=	$categoryObj->getCategoryListnig();
		$response	=	array(
			'category_path'	=>  CATEGORY_URL,
			'status'		=>	1,
			'data'	=>	$categoryDetail,
		); 
		return  Response::json($response); 
		die;	
	}
	
	public function getContactDetails(){
		 
		$data  = []; 
        $cotactEmail =   DB::table('settings')->where('key','Site.contact_email')->select('value')->first();
		$phonenumber =   DB::table('settings')->where('key','Site.contact_number')->select('value')->first();
		$facebook =   DB::table('settings')->where('key','Social.facebook')->select('value')->first();
		$twitter =   DB::table('settings')->where('key','Social.twitter')->select('value')->first();
		$linkedIn =   DB::table('settings')->where('key','Social.linkedin_url')->select('value')->first();
		$youtube =   DB::table('settings')->where('key','Social.linkedin_url')->select('value')->first();
		 
		$data['email'] = $cotactEmail->value; 
		$data['moible_no'] = $phonenumber->value;
		$data['facebook'] = $facebook->value;
		$data['twitter'] = $twitter->value;
		$data['linkedIn'] = $linkedIn->value;
		$data['youtube'] = $youtube->value;
		//pr($data); die;
		
		$response	=	array(
			
			'status'		=>	1,
			'data'	=>	$data,
		); 
		return  Response::json($response); 
		die;
	}
	
	public function saveContactDetails(){
		
		Input::replace($this->arrayStripTags(Input::all()));
		$formData			=	Input::all();
		
		$validator = Validator::make(
			Input::all(),
			array(
				'name'					=> 'required',
				'message'						=> 'required',
				'email' 						=> 'required|email',
				'phone' 						=> 'required',
				'countarycode' 						=> 'required',
				
				//'image' 						=> 'mimes:'.IMAGE_EXTENSION,			
			),
			array(
				
				"name.required"			=>	trans("messages.home.name_is_required"),
				"countarycode.required"	=>	trans("messages.home.countarycode_is_required"),
				"message.required"		=>	trans("messages.home.message_is_required"),
				"mobile.required"		=>	trans("messages.home.mobile_is_required"),
				"email.required"		=>	trans("messages.home.email_is_required"),
			)
		);
		if ($validator->fails()){
			$errors 				=	$validator->messages();
			$response	=	array(
				'status' 	=> 0,
				'errors' 	=> $errors
			);
			return Response::json($response); 
			die;
			//die;
		}else {
			$obj 					=  new ContactUs;
			$obj->name 				=  Input::get('name');
			$obj->email 			=  Input::get('email');
			$obj->phone 			=  Input::get('countarycode').Input::get('phone');
			$obj->message 			=  Input::get('message');
			$obj->save();
			
			$response	=	array(
					'status' 	=>	1,
					'msg' 		=>	trans('messages.home.contact_message_send')
					); 
			return  Response::json($response); 
			die;	
		}
	}
		 
	public function showCms($slug){
		$lang			=	App::getLocale();
		$cmsPagesDetail	=	DB::select( DB::raw("SELECT * FROM cms_page_descriptions WHERE foreign_key = (select id from cms_pages WHERE cms_pages.slug = '$slug') AND language_id = (select id from languages WHERE languages.lang_code = '$lang')") );
		
		if(empty($cmsPagesDetail)){
			return Redirect::to('/');
		}
		$result	=	array();
		
		foreach($cmsPagesDetail as $cms){
			$key	=	$cms->source_col_name;
			$value	=	$cms->source_col_description;
			$result[$cms->source_col_name]	=	$cms->source_col_description;
		}
		//return View::make('front.cms.index' , compact('result','slug'));
		$response	=	array(
			'status'		=>	1,
			'data'		=>	$result,
		); 
		return  Response::json($response); 
		die;
	}//end showCms()
	
	public function brands(){
		## Brands Details ##
		
		$brandObj 		= 	new Brand();
		$brandDetail	=	$brandObj->getMobileBrandListnig();
		
		$response	=	array(
			'brand_path'	=>  BRAND_URL,
			'status'		=>	1,
			'data'		=>	$brandDetail,
		); 
		return  Response::json($response); 
		die;
	}

	public function brandDetails($id = null,$user_id = null){
		if (empty($id)){
			$response	=	array(
					'status' 	=> 0,
					'errors' 	=> trans('messages.home.you_are_not_valid_user')
				);
		}
		
		$lang	= 	App::getLocale();
		$langCode = DB::table('languages')->where('lang_code',$lang)->value('id');	
		$DB		=	Brand::query();
		$DB1	=	BrandDescription::query();
		
		$brandArray 	= 	$DB->where('is_active',1)
								->pluck('id','id')->all();

		$brandDesc 		= 	$DB1->whereIn("brand_id",$brandArray)
								->where("language_id",DB::raw("(select id from languages WHERE languages.lang_code = '$lang')"))
								->select("title",DB::raw("
									(select logo from brands where id=brand_descriptions.brand_id ) as image,
									(select slug from brands where id=brand_descriptions.brand_id ) as slug,
									(select id from brands where id=brand_descriptions.brand_id ) as id"))
								->where(DB::raw("(select id from brands where brands.id=brand_descriptions.brand_id )"),$id)
								->first();
								

		$relatedProducts = Product::where('brand_id',$id)->select('products.*',DB::raw("(SELECT name FROM product_descriptions WHERE product_id =products.id AND language_id='$langCode') as product_name"),DB::raw("(SELECT long_description FROM product_descriptions WHERE product_id =products.id AND language_id='$langCode') as product_long_description,(select product_id from favorite_products where product_id=products.id AND user_id='".$user_id."') as favorite_product"))->get();	


		$brandVideo = 	Tv::where('brand',$id)->select('tvs.*',DB::raw("(SELECT title FROM tv_descriptions WHERE tv_id =tvs.id AND language_id='$langCode') as product_name"))->get();
		
		$response	=	array(
			'brand_path'	=>  BRAND_URL,
			'product_path'	 =>  PRODUCTS_IMAGE_URL,
			'status'		=>	1,
			'brand'		=>	$brandDesc,
			'brandProducts'=> $relatedProducts,
			'brandVideo'=> $brandVideo
		); 

		return  Response::json($response); 
		die;						
	}

	public function get_celebrity_video($id = null){
		$lang	= 	App::getLocale();
		$DB		=	Tv::query();
		$langCode = DB::table('languages')->where('lang_code',$lang)->value('id');	
		$celebrityVideos = Tv::where('id',$id)->select('tvs.*',DB::raw("(SELECT title FROM tv_descriptions WHERE tv_id =tvs.id AND language_id='$langCode') as tv_title"))->first();
								
		$brandProducts = DB::table('products')
						->leftJoin('product_descriptions', 'product_descriptions.product_id', '=', 'products.id')
						->select('products.id as id','products.main_image as main_image','products.price as price','products.discount_price as discount_price','products.category_id as category_id', 'product_descriptions.name as product_name','product_descriptions.language_id as language_id')
						->where('products.is_deleted' , 0)
						->where('products.is_active' , 1)
						->where('product_descriptions.language_id',DB::raw("(select id from languages WHERE languages.lang_code = '$lang')"))
						->where('products.brand_id', $id)
						->orderBy('products.id' , 'desc')
						->get();

		$relatedProducts = Product::where('category_id',$celebrityVideos->category_id)->orWhere('sub_category_id',$celebrityVideos->sub_category_id)->select('products.*',DB::raw("(SELECT name FROM product_descriptions WHERE product_id =products.id AND language_id='$langCode') as product_name"),DB::raw("(SELECT long_description FROM product_descriptions WHERE product_id =products.id AND language_id='$langCode') as product_long_description"))->get();	

		$response	=	array(
			'product_path'	 =>  PRODUCTS_IMAGE_URL,
			'status'		=>	1,
			'brandProducts'=> [],
			'data'=> $celebrityVideos,
			'related_products'=> $relatedProducts,
		); 
		return  Response::json($response); 
		die;						
	}

	public function getCustomerAddresses($id = null){

		if (empty($id)){
			$response	=	array(
					'status' 	=> 0,
					'errors' 	=> trans('messages.home.you_are_not_valid_user')
				);
		}

		$customer_addresses = UserAddress::where('user_id',$id)->orderBy('updated_at','DESC')->get();
		if(!empty($customer_addresses)) {
			
			$response	=	array(
						'status' 	=>	1,
						'address'	=> $customer_addresses,
						'msg' 		=>	trans('messages.home.address_found')
					); 			 
		}else{
			$response	=	array(
					'status' 	=> 0,
					'msg' 	=> trans('messages.global.no_record_found_message')
				);
		}
		return  Response::json($response);
	}
	
	
	public function getDefaultCustomerAddresses($id = null){

		if (empty($id)){
			$response	=	array(
					'status' 	=> 0,
					'errors' 	=> trans('messages.home.you_are_not_valid_user')
				);
		}

		$customer_addresses = UserAddress::where('user_id',$id)->where('is_shipping',1)->orderBy('updated_at','DESC')->first();
		if(!empty($customer_addresses)) {
			
			$response	=	array(
						'status' 	=>	1,
						'default_address'	=> $customer_addresses,
						'msg' 		=>	trans('messages.home.address_found')
					); 			 
		}else{
			$response	=	array(
					'status' 	=> 0,
					'msg' 	=> trans('messages.global.no_record_found_message')
				);
		}
		return  Response::json($response);
	}

	/** 
		*  Function : Favourite
		*  Purpose: This function is used to For Favourite and UnFavourite product
		*  Pre-condition:  None
		*  Post-condition: None. 
		*  Parameters: 
		*  Returns: void
	*/
	public function favouriteProduct(){
		Input::replace($this->arrayStripTags(Input::all()));
		$formData	=	Input::all();
		$product_id = Input::get('product_id');
		$action_type = Input::get('action_type');
		$user_id = Input::get('user_id');
		
		$UserFaviourite = DB::table('favorite_products')->where('user_id',$user_id)->where('product_id',$product_id)->first();
		//pr($product_id); die;
		
		if(empty($UserFaviourite)){
			if(Input::get('favouriteStatus')==1){
				
				   $obj 			 	=  new FavoriteProduct;
				   $obj->user_id		=  $user_id ;
				   $obj->product_id	 	=  $product_id ;
				   $obj->action_type	=  $action_type ;
				   $obj->status			=  1;
				   $obj->save();
				   
				$response	=	array(
						'status' 	=>	1,
						'favouriteStatus' => 0,
						'msg' 		=>	trans('messages.home.product_add_favourite')
					);
				}
				
		}else{
			 if(Input::get('favouriteStatus')==0){
				 
				 //$UserFaviourite->delete();
				 DB::table('favorite_products')->where('user_id',$user_id)->where('product_id',$product_id)->delete();
				
				$response	=	array(
						'status' 	=>	1,
						'favouriteStatus' => 1,
						'msg' 		=>	trans('messages.home.product_remove_favourite')
					);
			 }  
		}
		return  Response::json($response);
	}
	
	
	/** 
	  *  Function : wishList
		*  Purpose: This function is used to For add wishList and remove wishList products
	  *  Pre-condition:  None
	  *  Post-condition: None. 
	  *  Parameters: 
	  *  Returns: void
	*/
	public function wishList(){ 
		Input::replace($this->arrayStripTags(Input::all()));
		$formData	=	Input::all();
		$product_id = Input::get('product_id');
		//$action_type = Input::get('action_type');
		$user_id =Input::get('user_id');
		
		$UserWishlist = DB::table('wishlists')->where('user_id',$user_id)->where('product_id',$product_id)->first();
		
		if(empty($UserWishlist)){
			if(Input::get('wishlistStatus')==1){
				
				   $obj 			 	=  new Wishlist;
				   $obj->user_id		=  $user_id ;
				   $obj->product_id	 	=  $product_id ;
				   $obj->status			=  1;
				   $obj->save();
				   
					$response	=	array(
						'status' 	=>	1,
						'wishlistStatus' =>0,
						'msg' 		=>	trans('messages.home.product_add_wishlist')
					);
				}
				
		}else{
			 if(Input::get('wishlistStatus')==0){
				 
				 //$UserFaviourite->delete();
				 DB::table('wishlists')->where('user_id',$user_id)->where('product_id',$product_id)->delete();
				
				$response	=	array(
						'status' 	=>	1,
						'wishlistStatus' => 1,
						'msg' 		=>	trans('messages.home.product_remove_wishlist')
					);
			 } 
		}
		return  Response::json($response);
	}
	
	public function addCustomerAddress(Request $request){

		$validator = Validator::make(
			Input::all(),
			array(
				'user_id'		=> 'required',
				'first_name'	=> 'required',
				'last_name'		=> 'required',
				'mobile' 		=> 'required',
				'city' 			=> 'required',
				'state' 		=> 'required',
				'country' 		=> 'required',
				'address' 		=> 'required',
				'pincode' 		=> 'required',		
			),
			array(
				"user_id.required"		=>	trans("messages.home.user_id_is_required"),
				"first_name.required"	=>	trans("messages.home.name_is_required"),
				"last_name.required"	=>	trans("messages.home.name_is_required"),
				"mobile.required"		=>	trans("messages.home.mobile_is_required"),
				"city.required"			=>	trans("messages.home.city_is_required"),
				"state.required"		=>	trans("messages.home.state_is_required"),
				"country.required"		=>	trans("messages.home.country_is_required"),
				"address.required"		=>	trans("messages.home.address_is_required"),
				"pincode.required"		=>	trans("messages.home.pincode_is_required"),
			)
		);

		if ($validator->fails()){
			$errors 	=	$validator->messages();
			$response	=	array(
					'status' 	=> 0,
					'errors' 	=> $errors
				);
		}else{

			$customer_address = new UserAddress;

			$customer_address->user_id = $request->input('user_id');
			$customer_address->first_name = $request->input('first_name');
	        $customer_address->last_name = $request->input('last_name');
	        $customer_address->full_name = $request->input('first_name').' '.$request->input('last_name');
	        $customer_address->mobile = $request->input('mobile');
	        $customer_address->city = $request->input('city');
	        $customer_address->state = $request->input('state');
	        $customer_address->address = $request->input('address');
	        $customer_address->country = $request->input('country');
	        $customer_address->pincode = $request->input('pincode');
	        $customer_address->country_code = $request->input('country_code');
	        $customer_address->id_number = $request->input('IDNumber');

			if (!UserAddress::where('user_id', '=', $request->user_id)->exists()) {
				$customer_address->is_shipping = 1;
				$customer_address->is_billing = 1;	
			}else{
				if (!empty($request->input('is_billing')) && $request->input('is_billing') == 1) {
		        	UserAddress::where('user_id',  $request->user_id)->update(['is_billing' => 0]);
		        	$customer_address->is_billing = $request->input('is_billing');
		        }
		        if (!empty($request->input('is_shipping')) && $request->input('is_shipping') == 1) {
		        	UserAddress::where('user_id', $request->user_id)->update(['is_shipping' => 0]);
		        	$customer_address->is_shipping = $request->input('is_shipping');
		        }
			}
			if($customer_address->save()) {
				$response	=	array(
							'status' 	=>	1,
							'msg' 		=>	trans('messages.home.address_added_success')
						); 			 
			}else{
				$response	=	array(
						'status' 	=> 0,
						'msg' 	=> trans('messages.home.something_wrong')
					);
			}
		}

		return  Response::json($response);
	}

	public function updateCustomerAddress(Request $request)
	{
		$validator = Validator::make(
			Input::all(),
			array(
				'address_id'	=> 'required',
				'user_id'		=> 'required',
				'first_name'	=> 'required',
				'last_name'		=> 'required',
				'mobile' 		=> 'required',
				'city' 			=> 'required',
				'state' 		=> 'required',
				'country' 		=> 'required',
				'address' 		=> 'required',
				'pincode' 		=> 'required',		
			),
			array(
				"address_id.required"	=>	trans("messages.home.address_id_is_required"),
				"user_id.required"		=>	trans("messages.home.user_id_is_required"),
				"first_name.required"	=>	trans("messages.home.name_is_required"),
				"last_name.required"	=>	trans("messages.home.name_is_required"),
				"mobile.required"		=>	trans("messages.home.mobile_is_required"),
				"city.required"			=>	trans("messages.home.city_is_required"),
				"state.required"		=>	trans("messages.home.state_is_required"),
				"country.required"		=>	trans("messages.home.country_is_required"),
				"address.required"		=>	trans("messages.home.address_is_required"),
				"pincode.required"		=>	trans("messages.home.pincode_is_required"),
			)
		);

		$customer_address = UserAddress::find($request->input('address_id'));
		if ($validator->fails()){
			$errors 	=	$validator->messages();
			$response	=	array(
					'status' 	=> 0,
					'errors' 	=> $errors
				);
		}else{

			$customer_address->user_id = $request->input('user_id');
			$customer_address->first_name = $request->input('first_name');
	        $customer_address->last_name = $request->input('last_name');
	        $customer_address->full_name = $request->input('first_name').' '.$request->input('last_name');
	        $customer_address->mobile = $request->input('mobile');
	        $customer_address->city = $request->input('city');
	        $customer_address->state = $request->input('state');
	        $customer_address->address = $request->input('address');
	        $customer_address->country = $request->input('country');
	        $customer_address->pincode = $request->input('pincode');
	        $customer_address->country_code = $request->input('country_code');
	        $customer_address->id_number = $request->input('IDNumber');

			if (!empty($request->input('is_billing')) && $request->input('is_billing') == 1) {
	        	UserAddress::where('user_id',  $request->user_id)->update(['is_billing' => 0]);
	        	$customer_address->is_billing = $request->input('is_billing');
	        }
	        if (!empty($request->input('is_shipping')) && $request->input('is_shipping') == 1) {
	        	UserAddress::where('user_id',  $request->user_id)->update(['is_shipping' => 0]);
	        	$customer_address->is_shipping = $request->input('is_shipping');
	        }
			
			if($customer_address->save()) {
				$response	=	array(
							'status' 	=>	1,
							'msg' 		=>	trans('messages.home.address_added_success')
						); 			 
			}else{
				$response	=	array(
						'status' 	=> 0,
						'msg' 	=> trans('messages.home.something_wrong')
					);
			}
		}
		return  Response::json($response);
	}
   
	public function deleteCustomerAddress(Request $request){
		if(!empty($request->address_id) && !empty($request->user_id)){
			UserAddress::where('id', $request->address_id)->where('user_id', $request->user_id)->delete();
			$response	=	array(
							'status' 	=>	1,
							'msg' 		=>	trans('messages.home.address_delete_success')
						);
		}else{
			$response	=	array(
						'status' 	=> 0,
						'msg' 	=> trans('messages.home.something_wrong')
					);
		}
		return  Response::json($response);
	}
	
	public function updateProfile()
	{   
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData		=	Input::all();
	    $userId = Input::get('id');
		$validator = Validator::make(
			Input::all(),
			array(
				'id_number'	=> 'nullable|unique:users,id_number,'.$userId,
					
			),
			array(
				"id_number.unique"	=>	trans("messages.home.id_number_is_unique"),
			)
		);
		if ($validator->fails()){
			$errors 	=	$validator->messages();
			$response	=	array(
					'status' 	=> 0,
					'errors' 	=> $errors
				);
		}else{
			## Update user's information in users table ##
			
			
			$obj	=  	User::find($userId);
			if($obj)
				{
			//pr($obj); die;
			$fullName					=	Input::get('first_name').' '.Input::get('last_name');
			$obj->full_name 			=   ucwords($fullName);
			$obj->first_name			=  Input::get('first_name');
			$obj->last_name				=  Input::get('last_name');
			//$obj->username				=  Input::get('username');
			$obj->address				=  Input::get('address');
			//$obj->email					=  Input::get('email');
			//$obj->mobile				=  Input::get('mobile');
			$obj->video					=  Input::get('video');
			$obj->gender				=  Input::get('gender');
			$obj->city					=  Input::get('city');
			$obj->country				=  Input::get('country');
			$obj->state					=  Input::get('state');
			$obj->dob					=  Input::get('dob');
			$obj->social_url			=  Input::get('social_url');
			$obj->id_number			=  Input::get('id_number');
			//$obj->user_role_id			=  $userRoleId;
			$obj->is_approved			=  1; 
			$obj->is_profile_complete	=  1; 
			 // pr(Input::get('video')); 	die;
			if(Input::hasFile('image')){ 
				$image 					=	User::where('id',$userId)->value('image');
				@unlink(USER_PROFILE_IMAGE_ROOT_PATH.$image);
				$extension 			=	Input::file('image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	USER_PROFILE_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-user.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('image')->move($folderPath, $userImageName)){
					$obj->image		=	$image;
				}
			}
		
			if(!empty(Input::get('video')))
			{
				$thumb_name = explode("v=", Input::get('video'));
				$obj->video_thumb = "https://img.youtube.com/vi/".$thumb_name[1]."/0.jpg";
			}	
			
			if($obj->save()) {
				$response	=	array(
							'status' 	=>	1,
							'msg' 		=>	trans('messages.home.profile_updated')
						); 			 
			}else{
				$response	=	array(
						'status' 	=> 0,
						'msg' 	=> trans('messages.home.something_wrong')
					);
			}
				} else {
					$response	=	array(
						'status' 	=> 0,
						'msg' 	=> trans('messages.home.something_wrong')
					);
				}
		}
		return  Response::json($response);
	}	

	public function getUserProfile(Request $request){
		if (empty($request->input('id'))){
			$response	=	array(
					'status' 	=> 0,
					'errors' 	=> trans('messages.home.you_are_not_valid_user')
				);
		}else{
			$user_profile = User::where('id',$request->input('id'))->first();
			if(!empty($user_profile)) {
				
				$response	=	array(
							'status' 		=>	1,
							'image_path'	=> USER_PROFILE_IMAGE_URL,
							'data'			=> $user_profile,
							'msg' 			=>	trans('messages.global.record_found_message')
						); 			 
			}else{
				$response	=	array(
						'status' => 0,
						'msg' 	 => trans('messages.global.no_record_found_message')
					);
			}
		}
		
		return  Response::json($response);
	}

	public function getCelebrityProfile(Request $request){
		if (empty($request->input('id'))){
			$response	=	array(
					'status' 	=> 0,
					'errors' 	=> trans('messages.home.you_are_not_valid_user')
				);
		}else{
			$user_profile = Celebrity::where('id',$request->input('id'))->first();
			if(!empty($user_profile)) {
				
				$response	=	array(
							'status' 		=>	1,
							'image_path'	=> USER_PROFILE_IMAGE_URL,
							'data'			=> $user_profile,
							'msg' 			=>	trans('messages.global.record_found_message')
						); 			 
			}else{
				$response	=	array(
						'status' => 0,
						'msg' 	 => trans('messages.global.no_record_found_message')
					);
			}
		}
		
		return  Response::json($response);
	}

	/** 
	  *  Function : getUserOrderLists
	  *  Purpose: This function is used For Fetch user orders list
	  *  Pre-condition:  None
	  *  Post-condition: None. 
	  *  Parameters:  userid
	*/

	public function getUserOrderLists(Request $request)
	{
		
		if (!empty($request->input('user_id'))) {
			$status = !empty($request->input('status')) ? $request->input('status'):0;
			$orderDetail	=	Order::where('user_id',$request->input('user_id'))->where('status',$status)->orderBy('created_at','DESC')->get();
			//$totalOrders	=	Order::where('user_id',$user_id)->count();

			if (!empty($orderDetail->toArray())) {
				$response = array(
					'status' => 1,
					'data' 	=> $orderDetail,
					'msg' 	=>	trans('messages.global.record_found_message')
				);
			}else{
				$response = array(
					'status' 	=> 0,
					'errors' 	=> trans('messages.global.no_record_found_message')
				);
			}
		}else{
			$response = array(
				'status' 	=> 0,
				'errors' 	=> trans('messages.home.you_are_not_valid_user')
			);
		}
		
		return  Response::json($response);
	}

	/** 
	  *  Function : getCelebrityOrderLists
	  *  Purpose: This function is used For Fetch Celebrity orders list which has celebrity product
	  *  Pre-condition:  None
	  *  Post-condition: None. 
	  *  Parameters:  userid
	*/

	public function getCelebrityOrderLists(Request $request)
	{
		if (!empty($request->input('celebrity_id'))) {
			$status 		= !empty($request->input('status')) ? $request->input('status'):0;
			$orderDetail	= CelebrityEarning::select('orders.*')->leftjoin('orders','celebrity_earnings.order_id','orders.id')->where('celebrity_earnings.celebrity_id',$request->input('celebrity_id'))->where('orders.status',$status)->orderBy('orders.created_at','DESC');

			if (!empty($request->input('date_to'))) {
				$todate = date("Y-m-d", strtotime($request->input('date_to')));
				$orderDetail->whereDate('orders.created_at','>=',$todate);
			}

			if (!empty($request->input('date_from'))) {
				$fromdate = date("Y-m-d", strtotime($request->input('date_from')));
				$orderDetail->whereDate('orders.created_at','<=',$fromdate);
			}

			if (!empty($request->input('date_to')) && !empty($request->input('date_from'))) {
				$todate = date("Y-m-d", strtotime($request->input('date_to')));
				$fromdate = date("Y-m-d", strtotime($request->input('date_from')));
				$orderDetail->whereBetween('orders.created_at', [$fromdate, $todate]);
			}

			$result = $orderDetail->get();
			

			if (!empty($result->toArray())) {
				$response = array(
					'status' => 1,
					'data' 	=> $result,
					'msg' 	=>	trans('messages.global.record_found_message')
				);
			}else{
				$response = array(
					'status' 	=> 0,
					'errors' 	=> trans('messages.global.no_record_found_message')
				);
			}
		}else{
			$response = array(
				'status' 	=> 0,
				'errors' 	=> trans('messages.home.you_are_not_valid_user')
			);
		}
		
		return  Response::json($response);
	}

	/** 
	  *  Function : getUserOrderDetails
	  *  Purpose: This function is used For Fetch user orders list
	  *  Pre-condition:  None
	  *  Post-condition: None. 
	  *  Parameters:  orderid
	*/

	public function getOrderDetails(Request $request){

		if(!empty($request->input('order_id'))){
			$orderData	=	DB::table('orders')->where('orders.id',$request->input('order_id'))->first();
			$order		=	new OrderDetail();
			$order_product	=	$order->getProduct($request->input('order_id'));
						
			if (!empty($orderData) && !empty($order_product)) {
				$data = array('product_path' =>  PRODUCTS_IMAGE_URL,'orderdetail' => $orderData, "orderproduct"=>$order_product);
				$response = array(
					'status' => 1,
					'data' 	=> $data,
					'msg' 	=>	trans('messages.global.record_found_message')
				);
			}else{
				$response = array(
					'status' 	=> 0,
					'errors' 	=> trans('messages.global.no_record_found_message')
				);
			}
		}else{
			$response = array(
				'status' 	=> 0,
				'errors' 	=> trans('messages.global.invalid_id')
			);
		}

		return  Response::json($response);
	}

	/** 
	 * Function to display cms page as Webview in api
	 *
	 * @param slug as slug of cms page
	 * 
	 * @return view page
	 */

	public function getContentpages($slug,$lang){
	
		$cmsPagesDetail	=	DB::select( DB::raw("SELECT * FROM cms_page_descriptions WHERE foreign_key = (select id from cms_pages WHERE cms_pages.slug = '$slug') AND language_id = (select id from languages WHERE languages.lang_code = '$lang')") );
		
		$result	=	array();
		if(!empty($cmsPagesDetail)){
			foreach($cmsPagesDetail as $cms){
				$key	=	$cms->source_col_name;
				$value	=	$cms->source_col_description;
				$result[$cms->source_col_name]	=	$cms->source_col_description;
			}
		}

		return View('front.api.page' , compact('result'));
	}//end showCms()

	public function zahmaVideo()
	{
		$lang	= 	App::getLocale();
		$langCode = DB::table('languages')->where('lang_code',$lang)->value('id');
		$zahmaVideos = Tv::select('tvs.id','tvs.brand','tvs.video','tvs.video_thumb',DB::raw("(SELECT title FROM tv_descriptions WHERE tv_id = tvs.id AND language_id='$langCode') as tv_title"))->orderBy('created_at','DESC')->get();

		if(!empty($zahmaVideos->toArray())) {
			
			list($recentvideo, $popularvideo) = array_chunk($zahmaVideos->toArray(), 5);
			$response	=	array(
						'status' 	=>	1,
						'recentvideo'	=> $recentvideo,
						'popularvideo'	=> $popularvideo,
						'msg' 		=>	trans('messages.global.record_found_message')
					); 			 
		}else{
			$response	=	array(
					'status' 	=> 0,
					'msg' 	=> trans('messages.global.no_record_found_message')
			);
		}
		return  Response::json($response);
	}

	public function get_gift_products(Request $request){
		$point_type = '';
		$serachData	=	array(); 
		if(!empty(Input::all())){
			$serachData  = Input::all();
		}
		$giftObj = 	new Gift();
		$limit 	= Config::get('Reading.record_front_per_page');
		$offset = 0;
		$giftsDetail =	$giftObj->getGiftDetail($limit,$offset,$serachData);
		$response	=	array(
						'status' 	=>	1,
						'data'	=> $giftsDetail,
						'msg' 		=>	trans('messages.global.record_found_message')
					); 
		return  Response::json($response);
	}

	public function giftDetails($id=null){
		$giftDetails = Gift::leftJoin('gift_descriptions', function($join)
						 { $lang	= 	App::getLocale();
						   $join->on('gift_descriptions.gift_id', '=', 'gifts.id');
						   $join->on('gift_descriptions.language_id', '=', DB::raw("(select id from languages WHERE languages.lang_code = '$lang')"));
						 })
						->select('gifts.id','gifts.point_type','gifts.point_type','gifts.quantity','gifts.points','gift_descriptions.name','gift_descriptions.description') 
						->where('gifts.id',$id)
						->orderBy('gift_descriptions.name','ASC')
						->first();

		$response	=	array(
						'status' 	=>	1,
						'data'	=> $giftDetails,
						'msg' 		=>	trans('messages.global.record_found_message')
					); 
		return  Response::json($response);				
	}

	public function celebrityNomination(Request $request){
		Input::replace($this->arrayStripTags(Input::all()));
		$formData			=	Input::all();
		$validator = Validator::make(
			Input::all(),
			array(
				'email' 						=> 'unique:celebrity_nominations',	
			),
			array(
				"email.unique"					=>	trans("messages.home.email_allready"),
			)
		);
		if ($validator->fails()){
			 $errors 				=	$validator->messages();
			 $response	=	array(
				'status' 	=> 0,
				'errors' 	=> $errors
			);
			return Response::json($response); 
			die;
		}else{
			$allData= $request->all();
			$obj = new CelebrityNomination;
			$obj->name  = $request->name;
			$obj->fb_details  = $request->fb_details;
			$obj->instagram_details  = $request->instagram_details;
			$obj->twitter_details  = $request->twitter_details;
			$obj->other_details  = $request->other_details;
			$obj->email  = $request->email;
			$obj->user_id  = $request->user_id;
			$obj->save();
			$response	=	array(
							'status' 	=>	1,
							'msg' 		=>	trans('success')
						); 
			return  Response::json($response);		
		}
		
	}	

}//end Class()
<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use App\Model\User;
use App\Model\Fixture;
use App\Model\UserTeamValueLog;
use App\Model\UserTeamPlayers;  
use App\Model\UserTeams;
use App\Model\FixtureScorcard;
use App\Model\Player;
use App\Model\EmailAction;
use App\Model\EmailTemplate;
use App\Model\LockoutMailSendToUserLog;
use Carbon\Carbon;
use App,Auth,DB,View,Config;
use App\Model\UserTeamTradeLog; 
use App\Model\UserTeamsGWExtraPTTrack; 
use App\Model\UserTeamsPlayerTrack; 
use App\Model\UserTeamsCVCTrack; 
use App\Model\UserTeamsMonthPTTrack; 

/**
 * Language Controller
 *
 * Add your methods in the class below
 */

class CronController extends BaseController{
/** 
 * Function to switchLang
 *
 * @param $lang
 * 
 * @return view page
 */
    public function setLockout(){
	    $userList = User::where('user_role_id',CLUBUSER)->get();
	    if(!$userList->isEmpty()){
	    	foreach ($userList as $key => $value) {
	    		$club_data = User::where('id',$value->id)->first();
				$temp_day_arr = array(    'sunday' => 0,
				    'monday' => 1,
				    'tuesday' => 2,
				    'wednesday' => 3,
				    'thursday' => 4,
				    'friday' => 5,
				    'saturday' => 6);

				        

				$flg = false; 
				$i = 0; 
				        // dump($club_data); 
				if(!empty($club_data)){
			        foreach ($temp_day_arr as $key => $value) {
			            if(strtolower($club_data->lockout_start_day) == $key) $flg = true; 
			            if($flg){
			                $temp_day_arr[$key] = $i++; 
			            }
			        }
			        foreach ($temp_day_arr as $key => $value) {
			            if(strtolower($club_data->lockout_start_day) == $key) break; 
			            $temp_day_arr[$key] = $i++; 
			        }
				}

				$lockout_club = false; 


				if(!empty($club_data->lockout_start_day) && !empty($club_data->lockout_end_day) && !empty($club_data->lockout_start_time) && !empty($club_data->lockout_end_time)){
				        $club_data->lockout_start_time = explode(':', $club_data->lockout_start_time);
				        $club_data->lockout_end_time = explode(':', $club_data->lockout_end_time);
				        if($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] > $temp_day_arr[strtolower($club_data->lockout_start_day)] && 
				            $temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] < $temp_day_arr[strtolower($club_data->lockout_end_day)]  
				        ){
				            $lockout_club = true;    
				        }else if(
				($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] == $temp_day_arr[strtolower($club_data->lockout_start_day)]  ) && 
				(Carbon::now()->isoFormat('H') > $club_data->lockout_start_time[0] || (Carbon::now()->isoFormat('H') == $club_data->lockout_start_time[0] && Carbon::now()->isoFormat('m') > $club_data->lockout_start_time[1])) 
				        ){
				            // echo 'hello'; die; 
				            $lockout_club = true; 
				        }else if(
				($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] == $temp_day_arr[strtolower($club_data->lockout_end_day)] ) && 
				(Carbon::now()->isoFormat('H') < $club_data->lockout_end_time[0] || (Carbon::now()->isoFormat('H') == $club_data->lockout_end_time[0] && Carbon::now()->isoFormat('m') < $club_data->lockout_end_time[1]))
				        ){
				            // echo 'hihihihh'; die; 
				            $lockout_club = true; 
				        }

				    }
				if(!empty($club_data->lockout_start_date) && !(Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay() &&   Carbon::now()->startOfDay() <= Carbon::parse($club_data->lockout_end_date)->startOfDay()) && $lockout_club == true ){
				      $lockout_club = false; 
				}
				if($lockout_club==true){
					User::where('id', Auth::guard('admin')->user()->id)->update(['is_lockout'=>1]);
				}else{
					
				}
	    	}
	    }
		echo 1;die;
    }// end switchLang()

    public function changeCompleteStatus(){ 
    	$todayDate = Date('Y-m-d H:i:s',time());
    	$endTime   = Date('H:i',time());
    	$fixtures = Fixture::whereDate('end_date',"<=",$todayDate)->where('end_time','<=',$endTime)->where('is_completed','!=',3)->orderBy('created_at','desc')->get();
    	if(!$fixtures->isEmpty()){
    		foreach ($fixtures as $key => $value) {
    			$fixtureScorcard = FixtureScorcard::where('fixture_id',$value->id)->count();
    			if($fixtureScorcard > 0){
    				Fixture::where('id',$value->id)->update(['is_completed'=>3]);
    				FixtureScorcard::where('fixture_id',$value->id)->update(['status'=>3]);
    				$this->updatePlayerPrice($value->id);
    			}
    		}
    	}
    	echo 1;die;
    }

    public function updatePlayerPrice($fixture_id = null){
	    if(empty($fixture_id)){
	        Session::flash('flash_notice', trans("Internal Error occured. Please try again."));
	        return Redirect::back();
	    }
	    $pep_price = 0.00; 
	    $tspm = 0; 
	    $ideal_price = 0.00; 
	    $expected_price = array(); 
	    $temp_player = array(); 
	    $fantasy_range = array(); 
	    $price_change = array(); 
	    $flg = false; 
	    $all_fx_score_in1 = FixtureScorcard::where('fixture_id', $fixture_id)->where('inning', 1)->get()->toArray(); 
	    $all_fx_score_in2 = FixtureScorcard::where('fixture_id', $fixture_id)->where('inning', '<>', 1)->get()->toArray(); 
	    $all_fx_score_in1_temp = array(); 
	    $player_fp_arr = array(); 
	    $al_player_arr = array(); 
	    foreach ($all_fx_score_in1 as $key => $value) {
	        $all_fx_score_in1_temp[$value['player_id']] = $value; 
	        $al_player_arr[] = $value['player_id'] ; 
	    }
	    $all_fx_score_in1 = $all_fx_score_in1_temp ; 
	    $all_fx_score_in2_temp = array(); 
	    foreach ($all_fx_score_in2 as $key => $value) {
	        $all_fx_score_in2_temp[$value['player_id']] = $value; 
	        $al_player_arr[] = $value['player_id'] ; 
	    }
	    $all_fx_score_in2 = $all_fx_score_in2_temp ; 
	    sort($al_player_arr); 
	    $al_player_arr = array_unique($al_player_arr); 
	    foreach ($al_player_arr as $key => $value) {
	        $playerType = Player::where('id',$value)->first();
	        $old_value = $playerType->svalue; //  Old Price (OP) of players
	        $temp_player[$playerType->id] = $playerType ; 
	        $pep_price = (float) $pep_price + $old_value; // Player Eleven Price (PEP) of the team playing the match (i.e. total of price of the 11 (or more) players in the match)
	        $tspm = (int) $tspm + (isset($all_fx_score_in1[$value]) ? $all_fx_score_in1[$value]['fantasy_points'] : 0) + (isset($all_fx_score_in2[$value]) ? $all_fx_score_in2[$value]['fantasy_points'] : 0); // Total Scoring Points of the Match (i.e. total of fantasy points scored by all players in the team of a match)
	        $player_fp_arr[$value] = (int)(isset($all_fx_score_in1[$value]) ? $all_fx_score_in1[$value]['fantasy_points'] : 0) + (isset($all_fx_score_in2[$value]) ? $all_fx_score_in2[$value]['fantasy_points'] : 0); 
	    }
	    $ideal_price = $tspm/ (float) $pep_price;  // Ideal Price (i.e. Total Scoring Points of the Match (TPSM) / Player Eleven Price (PEP))
	    $ideal_price = round($ideal_price, 1); 
	    foreach ($temp_player as $key => $value) {
	        $expected_price[$key] = (float)$value->svalue * $ideal_price ; 
	        $fantasy_range[$key] = round((float)$expected_price[$key] * 0.1) ; 
	        $lfr = (int)$expected_price[$key] - $fantasy_range[$key] ; 
	        $ufr = (int)$expected_price[$key] + $fantasy_range[$key] ; 
	        $price_change[$key] = $player_fp_arr[$key] < $lfr ? -0.25 : ($player_fp_arr[$key] > $ufr ? +0.25 : 0.00); 
	    }
	    $cur_fixture = Fixture::where('id', $fixture_id)->first();
	    if($cur_fixture->match_type == 27){ // for oneday
	        if(FixtureScorcard::where('fixture_id',  $fixture_id)->where('inning', 1)->exists()){
	            Fixture::where('id', $fixture_id)->update(['pep'=> $pep_price, 'tspm' => $tspm ]);
	        }
	    }else{
	        if(FixtureScorcard::where('fixture_id',  $fixture_id)->where('inning', 1)->exists() && FixtureScorcard::where('fixture_id',  $fixture_id)->where('inning', 2)->exists()){
	            Fixture::where('id', $fixture_id)->update(['pep'=> $pep_price, 'tspm' => $tspm ]);
	        }
	    }            
	    // pep
	    // tspm
	    // ideal_price
	    // expected_price
	    // fantasy_range
	    // price_change
	    // $pep_price
	    // $tspm
	    // $ideal_price
	    // $expected_price
	    // $fantasy_range
	    // $price_change
	    // $fixture_id
	    foreach ($price_change as $key => $value) {
	        if($cur_fixture->match_type == 27){ // for oneday
	            if(FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $key)->where('inning', 1)->where('price_updated', 0)->exists()){
	                Player::where('id', $key)->update(['ideal_price' => $ideal_price, 'expected_price' => (isset($expected_price[$key]) ? $expected_price[$key] : 0), 'fantasy_range' => (isset($fantasy_range[$key]) ? $fantasy_range[$key] : 0), 'price_change' => $value, 'svalue' => DB::raw('svalue+'.$value)]);
	                FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $key)->update(['price_updated' => 1]); 
	                $flg = true; 
	            }
	        }else{
	            if(FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $key)->where('inning', 1)->where('price_updated', 0)->exists() && FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $key)->where('inning', 2)->where('price_updated', 0)->exists()){
	                Player::where('id', $key)->update(['ideal_price' => $ideal_price, 'expected_price' => (isset($expected_price[$key]) ? $expected_price[$key] : 0), 'fantasy_range' => (isset($fantasy_range[$key]) ? $fantasy_range[$key] : 0), 'price_change' => $value, 'svalue' => DB::raw('svalue+'.$value)]);
	                FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $key)->update(['price_updated' => 1]); 
	                $flg = true; 
	            }
	        }
	    }
	    // update the UserTeamValueLog and UserTeams table ************ start 
	    $check_team_arr = array(); 
	    $temp_all_team_arr = array(); 
	    foreach ($al_player_arr as $key => $value) {
	        $temp_all_team = UserTeamPlayers::where('player_id', $value)->pluck('team_id')->toArray();
	        $temp_all_team = array_unique($temp_all_team);
	        $temp_sum = FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $value)->sum('fantasy_points'); 
	       foreach ($temp_all_team as $k2 => $v2) {
	            $temp_all_team_arr[] = $v2; 
	            $temp_tm = UserTeams::find($v2); 
	            if($temp_tm->capton_id == $value){
	                $temp_tm->total_team_point += ($temp_sum * 2); 
	            }elseif($temp_tm->vice_capton_id == $value){
	                $temp_tm->total_team_point += ($temp_sum * 1.5); 
	            }else{
	                $temp_tm->total_team_point += $temp_sum; 
	            }
	            $temp_tm->save(); 
	        }
	    }
	    $temp_all_team = array_unique($temp_all_team_arr); 
		foreach ($temp_all_team as $k2 => $v2) {
			$temp_tm = UserTeams::find($v2); 
			$player_list = UserTeamPlayers::where('team_id', $temp_tm->id)->pluck('player_id')->toArray(); 
			$all_team_pl_pr_sum = 0 ; 
			$all_team_pl_players_points = array() ; 
			$all_team_pl_players_price = array() ; 
			$all_team_pl_players_points = FixtureScorcard::whereIn('player_id', $player_list)->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray(); 
			foreach ($player_list as $kp => $vp) {
			    $temp_player = Player::where('id', $vp)->first(); 
			    $all_team_pl_pr_sum += $temp_player->svalue ; 
			    $all_team_pl_players_price[$vp] = $temp_player->svalue ; 
			    $all_team_pl_players_points[$vp] = isset($all_team_pl_players_points[$vp]) ? $all_team_pl_players_points[$vp] : 0 ; 
			}

			$utvl = new UserTeamValueLog; 
			$utvl->team_id = $temp_tm->id; 
			$utvl->team_value = $all_team_pl_pr_sum; 
			$utvl->team_player = serialize($player_list); 
			$utvl->capton_id = $temp_tm->capton_id; 
			$utvl->v_capton_id = $temp_tm->vice_capton_id; 
			$utvl->tot_team_point = $temp_tm->total_team_point; 
			$utvl->players_points = serialize($all_team_pl_players_points) ; 
			$utvl->players_price = serialize($all_team_pl_players_price) ; 
			$utvl->save(); 
		}
	    /*if($flg){
	        Session::flash('flash_notice', trans("Player price successfully updated."));
	    }else{
	        Session::flash('flash_notice', trans("Player price has already updated for this fixture."));
	    }*/
	    return 1;
	}

	public function sendLockoutMailByCron(){
	    $temp = User::where('user_role_id', 3)->get();
	    foreach ($temp as $key => $value) {
	        $club_data = $value; 
	        if(!empty($club_data->lockout_start_date) ||  (!empty($club_data->is_lockout) && $club_data->is_lockout == 2) ){
	            $now = Carbon::now();

	            if(!empty($club_data->is_lockout) && $club_data->is_lockout == 2){
	                $emitted = Carbon::now()->subDay()->next($club_data->lockout_start_day)->startOfDay(); 

	            }elseif(Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_date)->startOfDay()){
	                if(    Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_end_day) < Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)

	            ){
	                    $emitted = Carbon::parse($club_data->lockout_start_date)->startOfDay();
	            }else{
	                $emitted = Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)->startOfDay();
	            }
	        }elseif(Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_day)->startOfDay()){
	            $emitted = Carbon::now()->next($club_data->lockout_start_day)->startOfDay();
	        }else{
	            $emitted = Carbon::now()->startOfDay();
	        }
	        if(!empty($club_data->lockout_start_time)){
	            $club_data->lockout_start_time = explode(':', $club_data->lockout_start_time); 
	            // dump($club_data->lockout_start_time); 
	            !empty($club_data->lockout_start_time[0]) ? $emitted->addHours($club_data->lockout_start_time[0]) : 0; 
	            !empty($club_data->lockout_start_time[1]) ? $emitted->addMinutes($club_data->lockout_start_time[1]) : 0; 
	        }
	        $all_club_team = UserTeams::where('club_id', $club_data->id)->with('userdata')->get(); 
	        foreach ($all_club_team as $key => $value) {
	            if(LockoutMailSendToUserLog::where('team_id', $value->id)->where('club_id', $club_data->id)->where('mail_for', 1)->whereBetween('mail_send_date',  [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()] )->doesntExist()){
	                if(!empty($value->userdata) && ($value->userdata->lockout_notification == 1) && (Carbon::now()->diffInHours($emitted) <= 24)){
	                try {
	                    $settingsEmail     = Config::get('Site.email');
	                    $full_name        = $value->userdata->full_name; 
	                    $game_name = $club_data->club_name ; 
	                    $start_time = $emitted->isoFormat('MMMM Do YYYY, h:mm:ss a'); 

	                    if(!empty($club_data->lockout_end_day) && !empty($club_data->lockout_end_time)){
	                        $tmp_emitted = $emitted->copy()->next($club_data->lockout_end_day); 
	                        $temp_lockout_end_time = explode(':', $club_data->lockout_end_time); 
	                        // dump($club_data->lockout_end_time); 
	                        !empty($temp_lockout_end_time[0]) ? $tmp_emitted->addHours($temp_lockout_end_time[0]) : 0; 
	                        !empty($temp_lockout_end_time[1]) ? $tmp_emitted->addMinutes($temp_lockout_end_time[1]) : 0; 
	                        $end_time = $tmp_emitted->isoFormat('MMMM Do YYYY, h:mm a'); 
	                    }else{
	                        $end_time = $emitted->isoFormat('MMMM Do YYYY, h:mm a'); 
	                    }
	                    $email        = $value->userdata->email;  // $value->userdata->email;
	                    $emailActions    = EmailAction::where('action','=','send_lockout_start_time')->get()->toArray();
	                    $emailTemplates    = EmailTemplate::where('action','=','send_lockout_start_time')->get(array('name','subject','action','body'))->toArray();           
	                    $cons             = explode(',',$emailActions[0]['options']);
	                    $constants         = array();
	                    
	                    foreach($cons as $key => $val){
	                        $constants[] = '{'.$val.'}';
	                    }
	                    
	                    $subject         = $emailTemplates[0]['subject'];
	                    $rep_Array         = array($full_name, $game_name, $start_time, $end_time); 
	                    $messageBody    = str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
	                    $mail            = $this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);


	                    // notification code here  (for lockout start)

	                    $lmstul = new LockoutMailSendToUserLog; 
	                    $lmstul->user_id = $value->user_id; 
	                    $lmstul->team_id = $value->id ; 
	                    $lmstul->club_id =  $club_data->id ; 
	                    $lmstul->mail_for = 1; 
	                    $lmstul->mail_send_date = Carbon::now(); 
	                    $lmstul->text = "Trading window of ".$game_name." ends at ".$start_time." tomorrrow. Make changes to your fantasy team before the lockout starts on myclubtap.com!";
	                    $lmstul->save(); 
	            } catch (Exception $e) {
	                echo 'Caught exception: ',  $e->getMessage(), "\n";
	            }
	        }

	        }
	        if(LockoutMailSendToUserLog::where('team_id', $value->id)->where('club_id', $club_data->id)->where('mail_for', 2)->whereBetween('mail_send_date',  [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()] )->doesntExist()){
	            if(!empty($value->userdata) &&  ($value->userdata->trading_lockout_notification == 1) && !empty($club_data->lockout_end_day) && !empty($club_data->lockout_end_time) ){
	                    $tmp_emitted = $emitted->copy()->previous($club_data->lockout_end_day); 
	                    $temp1_lockout_end_time = explode(':', $club_data->lockout_end_time); 
	                    !empty($temp1_lockout_end_time[0]) ? $tmp_emitted->addHours($temp1_lockout_end_time[0]) : 0; 
	                    !empty($temp1_lockout_end_time[1]) ? $tmp_emitted->addMinutes($temp1_lockout_end_time[1]) : 0; 
	                    if(Carbon::now() > $tmp_emitted){
	                        try {
	                            $settingsEmail     = Config::get('Site.email');
	                            $full_name        = $value->userdata->full_name; 
	                            $game_name = $club_data->club_name ; 
	                            $email     =  $value->userdata->email;

	                            $emailActions    = EmailAction::where('action','=','send_lockout_end_time')->get()->toArray();
	                            $emailTemplates    = EmailTemplate::where('action','=','send_lockout_end_time')->get(array('name','subject','action','body'))->toArray();
	                        
	                            $cons             = explode(',',$emailActions[0]['options']);
	                            $constants         = array();
	                            
	                            foreach($cons as $key => $val){
	                                $constants[] = '{'.$val.'}';
	                            }
	                            
	                            $subject         = $emailTemplates[0]['subject'];
	                            $rep_Array         = array($full_name, $game_name); 
	                            $messageBody    = str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
	                            $mail            = $this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);
	                            $lmstul = new LockoutMailSendToUserLog; 
	                            $lmstul->user_id = $value->user_id; 
	                            $lmstul->team_id = $value->id ; 
	                            $lmstul->club_id =  $club_data->id ; 
	                            $lmstul->mail_for = 2; 
	                            $lmstul->mail_send_date = Carbon::now(); 
	                            $lmstul->text = "With scores for the recent gameweek updated, the trading window of ".$game_name." is open now! Check out your team ranking, player stats and make trades in your fantasy team on myclubtap.com!"; 
	                            $lmstul->save(); 

	                        } catch (Exception $e) {
	                            echo 'Caught exception: ',  $e->getMessage(), "\n";
	                        }
	                    }

	                }

	        } 
	    }

	    }

	    }
	    return 1; 
	    die; 
	}

	public function show_endorsed(){
    	return  View::make('admin.common.show_endorsed',compact('result','searchVariable','sortBy','order','type','query_string'));
    }




	public function updateGWPoint(){


ini_set('memory_limit', '-1');

			// if (UserTeamPlayers::where('team_id', $user_terms->id)->exists()) {





				$all_teams = UserTeams::all(); 



				foreach ($all_teams as $t_key => $t_value) {
					// dump($t_value); die; 
			
					$user_terms = $t_value; 


					$old_capt_id  = $user_terms->capton_id ; 
					$old_vc_capt_id = $user_terms->vice_capton_id ; 

					$all_player = UserTeamPlayers::where('team_id', $user_terms->id)->pluck('player_id'); 

					// dump($all_player); die; 
					$gw_etp = 0 ; 
					$gw_player_points_arr = array(); 


					$all_team_pl_pr_sum = 0 ; 
					$all_team_pl_players_points = array() ; 
					$all_team_pl_players_price = array() ; 
					$player_list = $all_player; 

					foreach ($all_player as $key => $value) {
	
						// FixtureScorcard::whereIn('player_id', $removed_player)->groupBy('player_id')
						// 	   ->selectRaw('sum(fantasy_points) as sum, player_id')->orderBy('sum', 'DESC')
						// 	   ->pluck('sum','player_id')->toArray();
					// $etp = 0; 
				
// 
					// $past_gw_etp = 0; 

					// foreach ($removed_player as $key => $value) {

						$temp_utpt = UserTeamsPlayerTrack::where('player_id', $value)->where('team_id', $user_terms->id)->whereNull('remove_date')->first(); 
						if(empty($temp_utpt)){
							$temp_utpt = UserTeamPlayers::where('player_id', $value)->where('team_id', $user_terms->id)->first(); 
						}

// old_capt_id
						$capton_start_date = null; 
						if($old_capt_id == $value){
							$tc_utcvct = UserTeamsCVCTrack::where('player_id', $value)->where('team_id', $user_terms->id)->where('c_vc', 1)->whereNull('remove_date')->first(); 
							if(!empty($tc_utcvct)){
								$capton_start_date = $tc_utcvct->created_at ; 

							}else{
								$capton_start_date = $temp_utpt->created_at ; 
							}
							$capt_change_detected = true; 
						}

						$v_capton_start_date = null; 
						if($old_vc_capt_id == $value){
							$tvc_utcvct = UserTeamsCVCTrack::where('player_id', $value)->where('team_id', $user_terms->id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
							if(!empty($tvc_utcvct)){
								$v_capton_start_date = $tvc_utcvct->created_at ; 
							}else{
								$v_capton_start_date = $temp_utpt->created_at ; 
							}
							$v_capt_change_detected = true; 
						}


						if(!empty($temp_utpt)){


						// code commented as it is calculating the  point of current gameweek

					$player_start_date = Carbon::parse($temp_utpt->created_at); 



						// $tmp_price = FixtureScorcard::where('player_id', $value)->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)


					$tmp_price = FixtureScorcard::where('player_id', $value)

					->whereHas('fixture', function($q) use ($player_start_date){
						$q->where('start_date', '>', $player_start_date)->where('start_date', '>', Carbon::now()->startOfWeek())->where('start_date', '<=', Carbon::now()->endOfWeek()); 

					})

					// ->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())


					->sum('fantasy_points'); // ->where('status', 3)


						// dump($tmp_price); 
						// dump($player_start_date); 

						



						$c_tmp_price = 0 ; 
						if(!empty($capton_start_date) ){ // check the capton start date is in the corresponding game week
							// $c_tmp_price = FixtureScorcard::where('player_id', $value)->where('created_at', '>', Carbon::parse($capton_start_date))->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)

							$c_tmp_price = FixtureScorcard::where('player_id', $value)

							->whereHas('fixture', function($q) use ( $capton_start_date){

								$q->where('start_date', '>', Carbon::parse($capton_start_date))->where('start_date', '>', Carbon::now()->startOfWeek())->where('start_date', '<=', Carbon::now()->endOfWeek()); 


							})

							// ->where('created_at', '>', Carbon::parse($capton_start_date))->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())

							->sum('fantasy_points'); // ->where('status', 3)





						}
						$vc_tmp_price = 0 ; 
						if(!empty($v_capton_start_date)){
							// $vc_tmp_price = FixtureScorcard::where('player_id', $value)->where('created_at', '>', Carbon::parse($v_capton_start_date))->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())->sum('fantasy_points'); // ->where('status', 3)


							$vc_tmp_price = FixtureScorcard::where('player_id', $value)


							->whereHas('fixture', function($q) use ( $v_capton_start_date){

								$q->where('start_date', '>', Carbon::parse($v_capton_start_date))->where('start_date', '>', Carbon::now()->startOfWeek())->where('start_date', '<=', Carbon::now()->endOfWeek()); 

							})

							// ->where('created_at', '>', Carbon::parse($v_capton_start_date))->where('created_at', '>', $player_start_date->copy()->startOfWeek())->where('created_at', '<=', $player_start_date->copy()->endOfWeek())

							->sum('fantasy_points'); // ->where('status', 3)




							




							$vc_tmp_price = round($vc_tmp_price * 0.5); 

						}
						// $tmp_price = FixtureScorcard::where('player_id', $value)

						// 				->whereHas('fixture', function($q) use ($temp_utpt){
						// 					$q->where('start_date', '>', Carbon::parse($temp_utpt->created_at)->endOfDay())->where('start_date', '>=', Carbon::now()->startOfWeek())->where('start_date', '<=', Carbon::now()->endOfWeek());
						// 				})

						//             ->sum('fantasy_points'); // ->where('status', 3)

						$gw_etp += ($tmp_price  + $c_tmp_price + $vc_tmp_price); 


						$gw_player_points_arr[$value] = ($tmp_price  + $c_tmp_price + $vc_tmp_price);  //  store player points of gameweek according to each player ids






		$temp_player = Player::where('id', $value)->first(); 

		if($user_terms->twelveth_man_id != $value) // remove 12th man price if present
			$all_team_pl_pr_sum += $temp_player->svalue ;       // sum of price of all current player
		$all_team_pl_players_price[$value] = $temp_player->svalue ;  // all price by player 
		$all_team_pl_players_points[$value] = ($tmp_price  + $c_tmp_price + $vc_tmp_price);  //  store player points of gameweek according to each player ids




				}

			// }

					// print_r($etp); 
			// store points of removed player 
			// if($etp > 0){

			// 	$tmp = UserTeams::find($user_terms->id); 
			// 	$tmp->extra_team_point =  round($tmp->extra_team_point + $etp) ; 
			// 	$tmp->save(); 
				

			// }









// extra_team_point
			}



						// save points according to each gameweek


						if(UserTeamsGWExtraPTTrack::where(['team_id' => $user_terms->id, 'gw_end_date' => Carbon::now()->endOfWeek()->endOfDay() ])->exists()){
//gw_extra_pt  gw_pt  overall_pt  gw_rank  overall_rank  dealer_card_minus_points    three_cap_point
							$tmp = UserTeamsGWExtraPTTrack::where('team_id', $user_terms->id)->where('gw_end_date',  Carbon::now()->endOfWeek()->endOfDay())->first(); 
							if($gw_etp >= 0){	
								// $tmp->gw_extra_pt = round($tmp->gw_extra_pt + $gw_etp) ; 
								$tmp->gw_pt = $gw_etp + $tmp->three_cap_point + $tmp->gw_extra_pt - $tmp->dealer_card_minus_points; 
								$last_overall_pt = UserTeamsGWExtraPTTrack::where('team_id', $user_terms->id)->where('overall_pt', '>', 0)->orderBy('id', 'DESC')->first(); 

								if(!empty($last_overall_pt)){
									$last_overall_pt = $last_overall_pt->overall_pt ; 
									$tmp->overall_pt = $last_overall_pt + $tmp->gw_pt; //  store the overall point for this gw by adding the current gw point into the previous overall point
									
								}else{
									$tmp->overall_pt = $tmp->gw_pt; // store the overall point same as gameweek if current gameweek is the first gameweek to be stored.
								}
							}
							$tmp->gw_player_points = !empty($gw_player_points_arr) ? serialize($gw_player_points_arr) : null; 
							$tmp->save(); 

						}else{

							$tmp = new UserTeamsGWExtraPTTrack; 
							$tmp->team_id = $user_terms->id ; 
							if($gw_etp >= 0){
								// $tmp->gw_extra_pt = round( $gw_etp) ; 

								$tmp->gw_pt = $gw_etp + $tmp->three_cap_point + $tmp->gw_extra_pt - $tmp->dealer_card_minus_points; 
								$last_overall_pt = UserTeamsGWExtraPTTrack::where('team_id', $user_terms->id)->where('overall_pt', '>', 0)->orderBy('id', 'DESC')->first(); 

								if(!empty($last_overall_pt)){
									$last_overall_pt = $last_overall_pt->overall_pt ; 
									$tmp->overall_pt = $last_overall_pt + $tmp->gw_pt; //  store the overall point for this gw by adding the current gw point into the previous overall point
								}else{
									$tmp->overall_pt = $tmp->gw_pt; // store the overall point same as gameweek if current gameweek is the first gameweek to be stored.
								}

								
							}
							$tmp->gw_player_points = !empty($gw_player_points_arr) ? serialize($gw_player_points_arr) : null; 
							$tmp->gw_end_date = Carbon::now()->endOfWeek()->endOfDay() ; 
							$tmp->save(); 

						}





						// save points according to each gameweek 







	$utvl = new UserTeamValueLog; 
	$utvl->team_id = $user_terms->id; 
	$utvl->team_value = $all_team_pl_pr_sum; 
	$utvl->team_player = serialize($player_list); 
	$utvl->capton_id = $user_terms->capton_id; 
	$utvl->v_capton_id = $user_terms->vice_capton_id; 
	$utvl->tot_team_point = $tmp->overall_pt; 
	$utvl->players_points = serialize($all_team_pl_players_points) ; 
	$utvl->players_price = serialize($all_team_pl_players_price) ; 
	// $utvl->team_rank = isset($team_ranks_arr[$user_terms->id]) ? $team_ranks_arr[$user_terms->id] : 0;  // already in gameweek track table 
	$utvl->save(); 










}






	// gameweek rank and overall rank 


	// gw_rank   overall_rank

// gw_pt overall_pt
   // update overall rank according to club 


	$all_club = User::where('user_role_id', CLUBUSER)->pluck('id')->toArray(); 

	foreach ($all_club as $club_key => $club_val) {
		$temp_all_team_arr = UserTeams::where('club_id', $club_val)->pluck('id')->toArray();

	    $temp = UserTeamsGWExtraPTTrack::whereIn('team_id', $temp_all_team_arr)->where('gw_end_date' , Carbon::now()->endOfWeek()->endOfDay())->orderBy('overall_pt', 'DESC')->orderBy('id', 'ASC')->pluck('overall_pt', 'id')->toArray(); 

		$i = 1; 
		foreach ($temp as $key => $value) {
			// $temp[$key] = $i++ ; 
			// update overall rank in the table 
			if($value > 0)
				UserTeamsGWExtraPTTrack::where('id', $key)->where('gw_end_date' , Carbon::now()->endOfWeek()->endOfDay())->update(['overall_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}
		// $team_overall_rank_arr = $temp; 
	   // update gameweek rank 
	    $temp = UserTeamsGWExtraPTTrack::whereIn('team_id', $temp_all_team_arr)->where('gw_end_date' , Carbon::now()->endOfWeek()->endOfDay())->orderBy('gw_pt', 'DESC')->orderBy('id', 'ASC')->pluck('gw_pt', 'id')->toArray(); 

		$i = 1; 
		foreach ($temp as $key => $value) {
			// $temp[$key] = $i++ ; 
			// update gw rank in the table 
			if($value > 0)
				UserTeamsGWExtraPTTrack::where('id', $key)->where('gw_end_date' , Carbon::now()->endOfWeek()->endOfDay())->update(['gw_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}
	}

	// $team_gw_rank_arr = $temp; 






		return 1; 
	// }else{
	// 	return 0;
	// }

}



 public function updateMonthPoint(){



ini_set('memory_limit', '-1');


// UserTeamsMonthPTTrack
/*
$temp_lockout_start_date = Carbon::now()->subMonths(7); 

// dump($temp_lockout_start_date); die; 

	$all_teams = UserTeams::all(); 

while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->endOfWeek()) {

	foreach ($all_teams as $key => $value) {
		if(UserTeamsGWExtraPTTrack::where('team_id', $value->id)->where('gw_end_date', $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->count() > 1){
			

			dump(UserTeamsGWExtraPTTrack::where('team_id', $value->id)->where('gw_end_date', $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->count());  

			// $tmp = UserTeamsGWExtraPTTrack::where('team_id', $value->id)->where('gw_end_date', $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->get(); 

			// foreach ($tmp as $tkey => $tvalue) {
			// 	dump($tvalue->id); 
			// 	UserTeamsGWExtraPTTrack::where('id', $tvalue->id)->delete();
			// 	break; 
			// }


		}

	}

	$temp_lockout_start_date->addWeek(); 

}

die; 

*/ 


				$all_teams = UserTeams::all(); 

				$all_month_gw_data = array(); 

				foreach ($all_teams as $t_key => $t_value) {
					// dump($t_value); die; 
			

					// dump(Carbon::now()->subWeeks(2)->startOfMonth()); dump(Carbon::now()->subWeeks(2)->endOfMonth()); die; 



					$utgwept = UserTeamsGWExtraPTTrack::where('team_id', $t_value->id)->whereBetween('gw_end_date', [Carbon::now()->subWeeks(2)->startOfMonth(), Carbon::now()->subWeeks(2)->endOfMonth()])->orderBy('gw_end_date', 'ASC')->get();   // subWeek is to ensure the correct month 


					foreach ($utgwept as $key => $value) {
						// dump($utgwept); die; 
						// if(Carbon::parse($value->gw_end_date)->startOfWeek() >= Carbon::now()->subWeeks(2)->startOfMonth() && Carbon::parse($value->gw_end_date)->endOfWeek() <= Carbon::now()->subWeeks(2)->endOfMonth()){

						// include those gameweek in the count whose end date lies in the current month
						if(Carbon::parse($value->gw_end_date)->endOfWeek() <= Carbon::now()->subWeeks(2)->endOfMonth() ){

							// dump($value);  
							$all_month_gw_data[$t_value->id][] =  $value; 
						}
					
					}
// dump($all_month_gw_data);

				}

// echo 'hi'; die; 
				// dump($all_month_gw_data); die; 



				foreach ($all_month_gw_data as $key => $value) {

					$gw_total = 0 ; 
					$overall_total = 0 ; 
					foreach ($value as $rec_key => $rec_val) {

						$gw_total += $rec_val->gw_pt; // add all the gw point that comes inside the gameweek
						$overall_total = $rec_val->overall_pt; // storing the last overall point 
					}


// month_pt	overall_pt	month_rank	overall_rank	month_end_date
					// save points in the user_teams_month_pt_track table 

						if(UserTeamsMonthPTTrack::where(['team_id' => $key, 'month_end_date' => Carbon::now()->subWeeks(2)->endOfMonth()->endOfDay() ])->exists()){
//gw_extra_pt  gw_pt  overall_pt  gw_rank  overall_rank  dealer_card_minus_points    three_cap_point
							$tmp = UserTeamsMonthPTTrack::where('team_id', $key)->where('month_end_date',  Carbon::now()->subWeeks(2)->endOfMonth()->endOfDay())->first(); 

							$tmp->month_pt = $gw_total; 
							$tmp->overall_pt = $overall_total; 

							$tmp->save(); 

						}else{

							$tmp = new UserTeamsMonthPTTrack; 
							$tmp->team_id = $key ; 

							$tmp->month_pt = $gw_total; 
							$tmp->overall_pt = $overall_total; 

							$tmp->month_end_date = Carbon::now()->subWeeks(2)->endOfMonth()->endOfDay() ; 
							$tmp->save(); 

						}




					
				}




	// update month rank and overall rank 

	$all_club = User::where('user_role_id', CLUBUSER)->pluck('id')->toArray(); 

	foreach ($all_club as $club_key => $club_val) {
		$temp_all_team_arr = UserTeams::where('club_id', $club_val)->pluck('id')->toArray();

	    $temp = UserTeamsMonthPTTrack::whereIn('team_id', $temp_all_team_arr)->where('month_end_date' , Carbon::now()->subWeeks(2)->endOfMonth()->endOfDay())->orderBy('overall_pt', 'DESC')->orderBy('id', 'ASC')->pluck('overall_pt', 'id')->toArray(); 

		$i = 1; 
		foreach ($temp as $key => $value) {
			// $temp[$key] = $i++ ; 
			// update overall rank in the table 
			if($value > 0)
				UserTeamsMonthPTTrack::where('id', $key)->where('month_end_date' , Carbon::now()->subWeeks(2)->endOfMonth()->endOfDay())->update(['overall_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}
		// $team_overall_rank_arr = $temp; 
	   // update gameweek rank 
	    $temp = UserTeamsMonthPTTrack::whereIn('team_id', $temp_all_team_arr)->where('month_end_date' , Carbon::now()->subWeeks(2)->endOfMonth()->endOfDay())->orderBy('month_pt', 'DESC')->orderBy('id', 'ASC')->pluck('month_pt', 'id')->toArray(); 

		$i = 1; 
		foreach ($temp as $key => $value) {
			// $temp[$key] = $i++ ; 
			// update gw rank in the table 
			if($value > 0)
				UserTeamsMonthPTTrack::where('id', $key)->where('month_end_date' , Carbon::now()->subWeeks(2)->endOfMonth()->endOfDay())->update(['month_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}
	}



 }

// This function is only for to update the old gw data, before run this take the backup of original database 


  public function updateOldMonthData(){

ini_set('memory_limit', '-1');


	$temp_lockout_start_date = Carbon::now()->subMonths(6); 

	$all_teams = UserTeams::all(); 

		while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->endOfWeek()) {



dump($temp_lockout_start_date); 
			

				$all_month_gw_data = array(); 

				foreach ($all_teams as $t_key => $t_value) {
					// dump($t_value); die; 
			

					// dump(Carbon::now()->subWeeks(2)->startOfMonth()); dump(Carbon::now()->subWeeks(2)->endOfMonth()); die; 



					$utgwept = UserTeamsGWExtraPTTrack::where('team_id', $t_value->id)->whereBetween('gw_end_date', [$temp_lockout_start_date->copy()->startOfMonth(), $temp_lockout_start_date->copy()->endOfMonth()])->orderBy('gw_end_date', 'ASC')->get();   // subWeek is to ensure the correct month 


					foreach ($utgwept as $key => $value) {
						// dump($utgwept); die; 
						if(Carbon::parse($value->gw_end_date)->startOfWeek() >= $temp_lockout_start_date->copy()->startOfMonth() && Carbon::parse($value->gw_end_date)->endOfWeek() <= $temp_lockout_start_date->copy()->endOfMonth()){
							// dump($value);  
								$all_month_gw_data[$t_value->id][] =  $value; 
						}
					
					}
// dump($all_month_gw_data);

				}


				// dump($all_month_gw_data); die; 



				foreach ($all_month_gw_data as $key => $value) {

					$gw_total = 0 ; 
					$overall_total = 0 ; 
					foreach ($value as $rec_key => $rec_val) {

						$gw_total += $rec_val->gw_pt; // add all the gw point that comes inside the gameweek
						$overall_total = $rec_val->overall_pt; // storing the last overall point 
					}


// month_pt	overall_pt	month_rank	overall_rank	month_end_date
					// save points in the user_teams_month_pt_track table 

						if(UserTeamsMonthPTTrack::where(['team_id' => $key, 'month_end_date' => $temp_lockout_start_date->copy()->endOfMonth()->endOfDay() ])->exists()){
//gw_extra_pt  gw_pt  overall_pt  gw_rank  overall_rank  dealer_card_minus_points    three_cap_point
							$tmp = UserTeamsMonthPTTrack::where('team_id', $key)->where('month_end_date',  $temp_lockout_start_date->copy()->endOfMonth()->endOfDay())->first(); 

							$tmp->month_pt = $gw_total; 
							$tmp->overall_pt = $overall_total; 

							$tmp->save(); 

						}else{

							$tmp = new UserTeamsMonthPTTrack; 
							$tmp->team_id = $key ; 

							$tmp->month_pt = $gw_total; 
							$tmp->overall_pt = $overall_total; 

							$tmp->month_end_date = $temp_lockout_start_date->copy()->endOfMonth()->endOfDay() ; 
							$tmp->save(); 

						}




					
				}






			$temp_lockout_start_date->addMonth(); 
		}

	
	// update month rank and overall rank 




	$temp_lockout_start_date = Carbon::now()->subMonths(6); 

	$all_teams = UserTeams::all(); 

		while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->endOfWeek()) {



			$all_club = User::where('user_role_id', CLUBUSER)->pluck('id')->toArray(); 

			foreach ($all_club as $club_key => $club_val) {
				$temp_all_team_arr = UserTeams::where('club_id', $club_val)->pluck('id')->toArray();

			    $temp = UserTeamsMonthPTTrack::whereIn('team_id', $temp_all_team_arr)->where('month_end_date' , $temp_lockout_start_date->copy()->endOfMonth()->endOfDay())->orderBy('overall_pt', 'DESC')->orderBy('id', 'ASC')->pluck('overall_pt', 'id')->toArray(); 

				$i = 1; 
				foreach ($temp as $key => $value) {
					// $temp[$key] = $i++ ; 
					// update overall rank in the table 
					if($value > 0)
						UserTeamsMonthPTTrack::where('id', $key)->where('month_end_date' , $temp_lockout_start_date->copy()->endOfMonth()->endOfDay())->update(['overall_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
				}
				// $team_overall_rank_arr = $temp; 
			   // update gameweek rank 
			    $temp = UserTeamsMonthPTTrack::whereIn('team_id', $temp_all_team_arr)->where('month_end_date' , $temp_lockout_start_date->copy()->endOfMonth()->endOfDay())->orderBy('month_pt', 'DESC')->orderBy('id', 'ASC')->pluck('month_pt', 'id')->toArray(); 

				$i = 1; 
				foreach ($temp as $key => $value) {
					// $temp[$key] = $i++ ; 
					// update gw rank in the table 
					if($value > 0)
						UserTeamsMonthPTTrack::where('id', $key)->where('month_end_date' , $temp_lockout_start_date->copy()->endOfMonth()->endOfDay())->update(['month_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
				}
			}
			$temp_lockout_start_date->addMonth(); 
		}



return 1; 

  }



// This function is only for to update the old gw data, before run this take the backup of original database 

public function updateOldGWData(){


ini_set('memory_limit', '-1');


	$all_club = User::where('user_role_id', CLUBUSER)->orderBy('id', 'ASC')->pluck('id')->toArray(); 
	dump($all_club);  
	$save_club_cnt = 0; 
	foreach ($all_club as $club_id_key => $club_id) {
			$save_club_cnt++; 
		if($save_club_cnt != 3) continue; 
		
		dump($club_id); 
	

		$all_user_team_arr = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 

		foreach ($all_user_team_arr as $aut_key => $aut_val) {



$temp_last_wk_pt = array(); 

	$top_player_by_gwk_point = array(); 
	$user_teams = UserTeams::where('id', $aut_val)->first(); 

		if(!empty($user_teams)){


$all_club_team = UserTeams::where('club_id', $club_id)->pluck('id')->toArray(); 

			$all_temp_utp = array(); 

			$temp_club_data = User::where('id', $club_id)->first(); 
			
			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date)->startOfWeek(); 


			$i = 1; 
			$page_cnt = 1; 

			while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->endOfWeek()) {

				// if($i > 1) break; 



				$gameweek_team_value =  UserTeamValueLog::where('team_id', $user_teams->id)->whereNotNull('team_player')->whereDate('created_at', '>=', $temp_lockout_start_date)->whereDate('created_at', '<=', $temp_lockout_start_date->copy()->endOfWeek())->orderBy('id', 'DESC')->first(); 


				if(empty($gameweek_team_value)){  // if gameweek is empty search previous old record
					// $gameweek_team_value = UserTeamValueLog::where('team_id', $user_teams->id)->whereNotNull('team_player')->where('created_at', '<', $temp_lockout_start_date)->orderBy('id', 'DESC')->first(); 

$gameweek_team_value = UserTeamValueLog::where('team_id', $user_teams->id)->whereNotNull('team_player')->whereBetween('created_at', [$temp_lockout_start_date->copy()->subWeek()->startOfWeek(), $temp_lockout_start_date->copy()->subWeek()->endOfWeek()])->orderBy('id', 'DESC')->first(); 


				}
				if(empty($gameweek_team_value)){  // if empty search next  future record
					// $gameweek_team_value = UserTeamValueLog::where('team_id', $user_teams->id)->whereNotNull('team_player')->where('created_at', '>', $temp_lockout_start_date)->orderBy('id', 'ASC')->first(); 

$gameweek_team_value = UserTeamValueLog::where('team_id', $user_teams->id)->whereNotNull('team_player')->whereBetween('created_at', [$temp_lockout_start_date->copy()->addWeek()->startOfWeek(), $temp_lockout_start_date->copy()->addWeek()->endOfWeek()])->orderBy('id', 'ASC')->first(); 


				}	



// dump($gameweek_team_value); die; 



		

if(!empty($gameweek_team_value)){ // get the fixed no of record 



				$no_of_trade = UserTeamTradeLog::where('team_id', $user_teams->id)->where('created_at', '>', $temp_lockout_start_date->copy()->startOfWeek())->where('created_at', '<', $temp_lockout_start_date->copy()->endOfWeek())->sum('no_of_trade') ; 




		$temp_utp = unserialize($gameweek_team_value->team_player); 
// print_r($temp_utp); 
				$tot_sum_fp = 0; 
				$top_sum_rs = 0; 
				// dump($temp_lockout_start_date); 
				foreach ($temp_utp as $key3 => $value3) {



$sum_fp = FixtureScorcard::where('player_id', $value3)

                        ->whereHas('fixture', function($q) use ($temp_lockout_start_date){
   							$q->where('start_date', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('start_date', '<=', $temp_lockout_start_date->copy()->endOfWeek()); 


                                 })

                        // ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->where('created_at', '>=', Carbon::now()->subWeek()->startOfWeek())->where('created_at', '<=', Carbon::now()->subWeek()->endOfWeek())

                        ->sum('fantasy_points'); 



					if($value3 == $user_teams->capton_id){
						$sum_fp = $sum_fp * 2; 

					}
					if($value3 == $user_teams->v_capton_id){
						$sum_fp = round($sum_fp * 1.5);
					}
					$tot_sum_fp += $sum_fp; 
					// $top_sum_rs += $sum_rs; 

$gw_player_points_arr[$value3] = $sum_fp;  //  store player points of gameweek according to each player ids

				}

				$overall_pts = (!empty($gameweek_team_value)) ? $gameweek_team_value->tot_team_point : 0 ; 
				$team_value= (!empty($gameweek_team_value)) ? $gameweek_team_value->team_value : 0 ; 
				$team_rank= (!empty($gameweek_team_value)) ? $gameweek_team_value->team_rank : 0 ; 



                if(UserTeamsGWExtraPTTrack::where(['team_id' => $user_teams->id, 'gw_end_date' => $temp_lockout_start_date->copy()->endOfWeek()->endOfDay() ])->exists()){

                	$tmp = UserTeamsGWExtraPTTrack::where('team_id', $user_teams->id)->where('gw_end_date', $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->first(); 
			// dump($tmp); 
// echo 'hi'; die; 
                		$tot_sum_fp += round($tmp->gw_extra_pt)  ; 



				$tmp->gw_pt = $tot_sum_fp; 
				$tmp->overall_pt = $overall_pts; 
				$tmp->overall_rank = $team_rank; 
				$tmp->gw_end_date = $temp_lockout_start_date->copy()->endOfWeek()->endOfDay(); 
				$tmp->gw_player_points  = !empty($gw_player_points_arr) ? serialize($gw_player_points_arr) : null; 

				$tmp->save(); 


                }else{

				$utgwe =  UserTeamsGWExtraPTTrack::firstOrNew(['team_id' =>  $user_teams->id, 'gw_end_date' =>  $temp_lockout_start_date->copy()->endOfWeek()->endOfDay()]);


				$utgwe->gw_pt = $tot_sum_fp; 
				$utgwe->overall_pt = $overall_pts; 
				$utgwe->overall_rank = $team_rank; 
				$utgwe->gw_end_date = $temp_lockout_start_date->copy()->endOfWeek()->endOfDay(); 
				$utgwe->gw_player_points  = !empty($gw_player_points_arr) ? serialize($gw_player_points_arr) : null; 

				$utgwe->save(); 


                }



				// $overall_pts = (!empty($gameweek_team_value)) ? $gameweek_team_value->tot_team_point : 0 ; 
				// $team_value= (!empty($gameweek_team_value)) ? $gameweek_team_value->team_value : 0 ; 
				// $team_rank= (!empty($gameweek_team_value)) ? $gameweek_team_value->team_rank : 0 ; 



				// 		$top_player_by_gwk_point[$i]['gw'] = $i; 
				// 		$top_player_by_gwk_point[$i]['gw_pts'] = $tot_sum_fp; 
				// 		// $top_player_by_gwk_point[$i]['gw_run'] = $top_sum_rs; 
				// 		$top_player_by_gwk_point[$i]['trades_made'] = $no_of_trade; 
				// 		$top_player_by_gwk_point[$i]['overall_pts'] = $overall_pts; 
				// 		$top_player_by_gwk_point[$i]['overall_rank'] = $team_rank; 
				// 		$top_player_by_gwk_point[$i]['team_value'] = $team_value; 








// break; 

}





		$temp_lockout_start_date->addWeek(); 

				$i++; 
				 
			}




		}




			
		}






	}

	





/* code to calculate rank in the table, before executing this code take backup of database first
	$all_club = User::where('user_role_id', CLUBUSER)->pluck('id')->toArray(); 



	foreach ($all_club as $club_id_key => $club_id) {

			$temp_club_data = User::where('id', $club_id)->first(); 
			
			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date)->startOfWeek(); 

			// if($club_id != 42) continue; 

			while (Carbon::parse($temp_lockout_start_date) < Carbon::now()->endOfWeek()) {

// print_r($temp_lockout_start_date); 

		$temp_all_team_arr = UserTeams::where('club_id', $club_id)->pluck('id')->toArray();

	    $temp = UserTeamsGWExtraPTTrack::whereIn('team_id', $temp_all_team_arr)->where('gw_end_date' , $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->orderBy('overall_pt', 'DESC')->orderBy('id', 'ASC')->pluck('overall_pt', 'id')->toArray(); 
	    // echo 'hi'; 
	    // dump($temp); 
		$i = 1; 
		foreach ($temp as $key => $value) {
			// $temp[$key] = $i++ ; 
			// update overall rank in the table 
			if( $value > 0 )
				UserTeamsGWExtraPTTrack::where('id', $key)->where('gw_end_date' , $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->update(['overall_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}
		// $team_overall_rank_arr = $temp; 
	   // update gameweek rank 
	    $temp_gw = UserTeamsGWExtraPTTrack::whereIn('team_id', $temp_all_team_arr)->where('gw_end_date' , $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->orderBy('gw_pt', 'DESC')->orderBy('id', 'ASC')->pluck('gw_pt', 'id')->toArray(); 
	    // echo 'hello'; 
	    // dump($temp_gw); 
		$j = 1; 
		foreach ($temp_gw as $key1 => $value) {
			// $temp[$key] = $i++ ; 
			// update gw rank in the table 
			if( $value > 0 )
				UserTeamsGWExtraPTTrack::where('id', $key1)->where('gw_end_date' , $temp_lockout_start_date->copy()->endOfWeek()->endOfDay())->update(['gw_rank'=> $j++]) ; // point in descending order so rank will be in ascending order 
		}
	

		$temp_lockout_start_date->addWeek(); 

			}



}



die; 




*/ 



}




}// end LanguageController

<?php
namespace App\Http\Controllers\Auth;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use App\Http\Controllers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Model\User,Illuminate\Routing\Controller;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

   // use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }
	
	public function getSlug($title, $modelName,$limit = 30){
		$slug 		= 	 substr(\Str::slug($title),0 ,$limit);
		$Model		=	"\App\Model\\$modelName";
		$slugCount 	=  count($Model::where('slug', 'regexp', "/^{$slug}(-[0-9]*)?$/i")->get());
		return ($slugCount > 0) ? $slug."-".$slugCount : $slug;
	}//end getSlug()

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
	 /**
	 * Redirect the user to the facebook authentication page.
	 *
	 * @return Response
	 */
	public function redirectToProvider($provider)
    {
		
		if(Session::has('SocialUserRole')){
			Session::forget('SocialUserRole');
		}
		
		$userRoleId			=	$provider;
		Session::put('SocialUserRole',$userRoleId);
		switch ($provider) {
			case 'facebook':
				  return Socialite::driver($provider)->fields(['first_name', 'last_name', 'email', 'gender', 'verified','birthday','address'])->redirect();
				break;
		}
    }

    /**
     * Obtain the user information from facebook.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {	
		$errorFacebook	=	Input::get('error');
		$errorTwitter	=	Input::get('denied');
		if($errorFacebook!='' || $errorTwitter!=''){
			//return Redirect::to('/');
		}

		$provider		=	Session::get('SocialUserRole');	
        $user 			=	Socialite::driver($provider)->user();
		$socialField	=	$provider.'_id'; 
		$first_name 	= 	'';
		$last_name 		= 	'';
		$email 			= 	'';
		$gender 		= 	'';
		
		switch($provider){
			case 'facebook':
				$full_name					=	(!empty($user->name)) ? explode(",",$user->name) : "";
				$first_name 				= 	(!empty($full_name[0])) ? $full_name[0] : "";
				$last_name 					= 	(!empty($full_name[1])) ? $full_name[1] : "";
				$email 						= 	isset($user->email) ? $user->email  : '';
				$socialId 					= 	isset($user->id) ?  $user->id : ''; 
				$gender 					= 	isset($user->gender) ? $user->gender  : ''; 
				$profilePic 				= 	$user->avatar_original;
				break;
		}
		
		if($email!=''){		
			$emailCount	=	User::where('email',$email)->count();
			if($emailCount>0){
				User::where('email',$email)->update(array("$socialField"=>$socialId));
			}
		}
		
		$userAlreadyRegister		=	User::where($socialField,$socialId)->count();
		if($userAlreadyRegister == 0){
			$userData										=	array();
			$userData['email']								=	$email;
			$userData['username']							=	$email;
			$userData['password']							= 	'';
			$userData['first_name']							=	$first_name;
			$userData['last_name']							=	$last_name;
			$userData['full_name']							= 	$first_name.' '.$last_name;
			//$userData['user_role_id']						= 	FITNESS_ENTHUSIAST;
			$userData[$socialField]	 						=	$socialId;
			$userData['is_active']							=	1;
			$userData['is_verified']						=	1;
			$userData['gender']								=	!empty($gender) ? $gender  : '';
			$userData['slug'] 								=  $this->getSlug($userData['full_name'],'User');
			$userImage    									= 	file_get_contents($profilePic);
			$userImageName     								= 	$socialId ."_$provider.jpg";
			$newFolder     									= 	strtoupper(date('M'). date('Y')).'/';
			$folderPath										=	USER_PROFILE_IMAGE_ROOT_PATH.$newFolder;
		  
			// get profile image  from social url
			if(!File::exists($folderPath)) {
				File::makeDirectory($folderPath, $mode = 0777,true);
			}
			file_put_contents($folderPath.$userImageName,$userImage);
			$userData['image'] = $newFolder.$userImageName;
			$userData['created_at'] = date("Y-m-d H:i:s");
			$userData['updated_at'] = date("Y-m-d H:i:s");
			User::insert($userData);
		}
					
		$userId		=	User::where($socialField,$socialId)->pluck("id");
		Auth::loginUsingId($userId);
		if(Auth::user()->user_role_id == 0){
			$yourURL	=	URL::to('update-role');
		}else{
			$yourURL	=	URL::to('dashboard');
		}
		echo ("<script>location.href='$yourURL'</script>");
    }
 
}

<?php


namespace App\Http\Controllers\Auth;


use App\Model\User;
use App\Http\Controllers\BaseController;
use Socialite;
use Exception;
use URL,Redirect;


class FacebookController extends BaseController
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {   
        return Socialite::driver('facebook')->redirect();
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
            $userAlreadyRegister   =   User::where('facebook_id',$user->getId())->count();
            if($userAlreadyRegister==0){
                $full_name = $user->getName();
                $userModel = new User;
                $userModel->full_name = $full_name;
                $userModel->email = $user->getEmail();
                $userModel->facebook_id = $user->getId();
                $userModel->is_active    =   1;
                $userModel->user_role_id    =   2;
                $userModel->is_verified =   1;
                $userModel->is_deleted =   0;
                //$userModel->slug   =  $this->getSlug($full_name,'User');
                $userModel->save();
            } 

            $userId        =   User::where('facebook_id',$user->getId())->value('id'); 
            \Auth::loginUsingId($userId,true);
            return Redirect::to('customer');
        } catch (Exception $e) {
            return redirect('auth/facebook');
        }
    }
}
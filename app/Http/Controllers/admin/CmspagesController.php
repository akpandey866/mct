<?php
namespace App\Http\Controllers\admin;

use App\Http\Controllers\BaseController;
use App\Model\Cms;
use App\Model\CmsDescription;
use App\Model\Language;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Redirect,Request,Response,Session,URL,View,Validator;

/**
* Cms Controller
* Add your methods in the class below
* This file will render views from views/Cms
*/
class CmsPagesController extends BaseController {
/**
* Function for display all cms page
*
* @param null
*
* @return view page. 
*/
	public function listCms(){	
		$DB							=	Cms::query();
		$searchVariable				=	array(); 
		$inputGet					=	Input::get();
		if((Input::get() && isset($inputGet['display'])) || isset($inputGet['page']) ){
			$searchData				=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);
			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue)){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
					$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
				}
			}
		}
		$sortBy 					= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  					= 	(Input::get('order')) ? Input::get('order')   : 'DESC';
		$result 					= 	$DB->orderBy($sortBy, $order)->paginate(Config::get("Reading.records_per_page"));
		$complete_string			=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string				=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		
		return  View::make('admin.Cms.index',compact('result','searchVariable','sortBy','order','query_string'));
	}
/**
* Function for display page  for add new cms page 
*
* @param null
*
* @return view page. 
*/
	public function addCms(){
		return  View::make('admin.Cms.add');
	}
/**
* Function for save added cms page
*
* @param null
*
* @return redirect page. 
*/
	function saveCms(){
		//Input::replace($this->arrayStripTags(Input::all()));
		$thisData					=	Input::all();
		$validator = Validator::make(
			array(
				'name' 				=> Input::get('name'),
				'title' 			=> Input::get('title'),
				'body' 				=> Input::get('body'),
				'meta_title' 		=> Input::get('meta_title'),
				'meta_description'  => Input::get('meta_description'),
				'meta_keywords' 	=> Input::get('meta_keywords'),
			),
			array(
				'name' 				=> 'required',
				'title' 			=> 'required',
				'body' 				=> 'required',
				'meta_title' 		=> 'required',
				'meta_description' 	=> 'required',
				'meta_keywords' 	=> 'required',
			)
		);
		
		if ($validator->fails()) {	
			return Redirect::to('admin/cms-manager/add-cms')
				->withErrors($validator)->withInput();
		}else{
			
			$cms 					= new Cms;
			$cms->name    			= Input::get('name');
			$cms->title   			= Input::get('title');
			$cms->body   			= Input::get('body');
			$cms->meta_title   		= Input::get('meta_title');
			$cms->meta_description  = Input::get('meta_description');
			$cms->meta_keywords   	= Input::get('meta_keywords');
			$cms->slug   			= $this->getSlug(Input::get('title'),'title','Cms');
			$cmspags				= $cms->save();
			
			Session::flash('flash_notice', trans("Cms page added successfully")); 
			return Redirect::to('admin/cms-manager');
		}
	}
/**
* Function for display page  for edit cms page
*
* @param $Id ad id of cms page
*
* @return view page. 
*/	
	public function editCms($Id){
		$result				=	Cms::find($Id);
		if(empty($result)) {
			return Redirect::to('admin/cms-manager');
		}
		return  View::make('admin.Cms.edit',compact('result'));
	}
/**
* Function for update cms page
*
* @param $Id ad id of cms page
*
* @return redirect page. 
*/
	function updateCms($Id){
		//Input::replace($this->arrayStripTags(Input::all()));
		$this_data				=	Input::all();		
		$validator = Validator::make(
			array(
				'name' 				=> Input::get('name'),
				'title' 			=> Input::get('title'),
				'body' 				=> Input::get('body'),
				'meta_title' 		=> Input::get('meta_title'),
				'meta_description'  => Input::get('meta_description'),
				'meta_keywords' 	=> Input::get('meta_keywords'),
			),
			array(
				'name' 				=> 'required',
				'title' 			=> 'required',
				'body' 				=> 'required',
				'meta_title' 		=> 'required',
				'meta_description' 	=> 'required',
				'meta_keywords' 	=> 'required'
			)
		);
		
		if($validator->fails()){	
			return Redirect::to('admin/cms-manager/edit-cms/'.$Id)
				->withErrors($validator)->withInput();
		}else{
			$Cms_response			=	Cms::where('id', $Id)->update(array(
				'name'   	 		=>  Input::get('name'),
				'title' 			=>  Input::get('title'),
				'body' 				=>  Input::get('body'),
				'meta_title' 		=>  Input::get('meta_title'),
				'meta_description' 	=>  Input::get('meta_description'),
				'meta_keywords' 	=>  Input::get('meta_keywords'),
				'updated_at' 		=>  DB::raw('NOW()')
			));
			Session::flash('flash_notice', trans("Cms page updated successfully")); 
			return Redirect::to('admin/cms-manager');
		}
	}
/**
* Function for update cms page status
*
* @param $Id as id of cms page
* @param $Status as status of cms page
*
* @return redirect page. 
*/	
	public function updateCmsStatus($Id = 0, $Status = 0){
		/* DB::beginTransaction();
		$response			=	DB::statement("CALL UpdateCms($Id,$Status)");
		if(!$response) {
			DB::rollback();
			Session::flash('error', trans("Something went wrong.")); 
			return Redirect::back();
		}
		DB::commit(); */
		if($Status == 0	){
			$statusMessage	=	trans("Cms page deactivated successfully");
		}else{
			$statusMessage	=	trans("Cms page activated successfully");
		}
		$this->_update_all_status('cms_pages',$Id,$Status);
		Session::flash('flash_notice',  $statusMessage); 
		return Redirect::to('admin/cms-manager');
	}
}
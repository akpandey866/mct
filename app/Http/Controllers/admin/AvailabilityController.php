<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Availability;
use App\Model\Player;
use App\Model\User;
use App\Model\Club;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use Carbon\Carbon;

class AvailabilityController extends BaseController {
/**
* Function for display all event
*
* @param null
*
* @return view page.
*/
	public function index(){
		$DB 					= 	Availability::query();
		$DB->leftJoin('users','users.id','=','availabilities.club')->select('users.game_mode as mode');
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("availabilities.$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			} 
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';
 
	    if(Auth::guard('admin')->user()->user_role_id == 1){
			$result = 	$DB->leftJoin('players','players.id','=','availabilities.player')
							->orderBy($sortBy, $order)
							->select('availabilities.*','players.full_name as player_name')
							->paginate(Config::get("Reading.records_per_page"));
			$club = new Club;
			$clubList = $club->get_club_list();
		}else{ 
			$result= $DB->leftJoin('players','players.id','=','availabilities.player')
						->orderBy($sortBy, $order)
						->where('availabilities.club',Auth::guard('admin')->user()->id)
						->select('availabilities.*','players.full_name as player_name')
						->paginate(Config::get("Reading.record_game_per_page"));
			$player = new Player;
			$playerList = $player->get_players_by_club(Auth::guard('admin')->user()->id);
		}
		

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.availabilities.index', compact('result','searchVariable','sortBy','order','query_string','clubList','playerList'));
	 }
	/**
	* Function for add Forum
	*
	* @param null
	*
	* @return view page.  
	*/
	public function addAvailability(){
		if(Auth::guard('admin')->user()->user_role_id != 1){
			$player = new Player;
			$playerList = $player->get_players_by_club(Auth::guard('admin')->user()->id);
		}else{
			$club = new Club;
			$clubList = $club->get_club_list();
			$player = new Player;
			$playerList = $player->get_players();
		}
		return  View::make('admin.availabilities.add',compact('playerList','clubList'));
	}//end addPlayer()

	public function saveAvailability(){ 
	Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			if(Auth::guard('admin')->user()->user_role_id == 1){
				$validator 					=	Validator::make(
					Input::all(),
					array(
						'player'=> 'required',
						'club'=> 'required',
						'date_from'	=>'required',
						'date_till' =>'required',
					)				);
			}else{
				$validator 					=	Validator::make(
					Input::all(),
					array(
						'player'=> 'required',
						'date_from'	=> 'required',
						'date_till' => 'required',
					)
				);
			}


			if ($validator->fails()){
				$errors 	=	$validator->messages();
				$response	=	array(
					'success' 	=> false,
					'errors' 	=> $errors
				);
				return Response::json($response); 
				die;				 
			}else{ 

				$club = !empty(Input::get('club')) ? Input::get('club') : Auth::guard('admin')->user()->id;
				$availabiltyDetail = Availability::where('club',$club)
											->where('player',Input::get('player'))
											->where('date_from',Input::get('date_from'))
											->where('date_till',Input::get('date_till'))
											->count();

				if ($availabiltyDetail > 0) {
					Session::flash('error',trans("Player dates already in records.Please try with another dates."));
					$response	=	array(
				    	'success' 	=>	'2',
				    	'message' 	=>	trans("Player dates already in records.Please try with another dates.")
				    ); 
				    return Response::json($response);
				}

				$mode = User::where('id',$club)->value('game_mode');
				$obj =  new Availability;
				$obj->player =  !empty(Input::get('player')) ? Input::get('player'):'';
				$obj->date_from =  !empty(Input::get('date_from')) ? Input::get('date_from'):'';
				$obj->date_till	=  !empty(Input::get('date_till')) ? Input::get('date_till'):'';
				$obj->reason	=  !empty(Input::get('reason')) ? Input::get('reason'):'';
				$obj->club	=  $club;
				$obj->mode	=  $mode;
				$obj->save();
				Session::flash('success',trans("Availability has been upated successfully"));
				$response	=	array(
			    	'success' 	=>	'1',
			    	'message' 	=>	trans("Availability has been added successfully.")
			    ); 
			    return Response::json($response);
			}
		}
	}//end saveForum

	/**
	* Function for edit Forums
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editAvailability($userId = 0){  
		$availabilityDetails			=	Availability::findOrFail($userId); 
		if(Auth::guard('admin')->user()->user_role_id != 1){
			$player = new Player;
			$playerList = $player->get_players_by_club(Auth::guard('admin')->user()->id);
		}else{
			$club = new Club;
			$clubList = $club->get_club_list();
			$player = new Player;
			$playerList = $player->get_players();
		}
		return View::make('admin.availabilities.edit', compact('availabilityDetails','playerList','clubList'));
	} // 

	/**
* Function for update Forum
*
* @param null
*
* @return view page. 
*/
	public function updateAvailability(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData	=	Input::all();
		$availability_id = Input::get('availability_id');
		if(!empty($thisData)){
			if(Auth::guard('admin')->user()->user_role_id == 1){
				$validator 					=	Validator::make(
					Input::all(),
					array(
						'player'=> 'required',
						'date_from'	=> 'required',
						'date_till' => 'required',
						'club' => 'required',
					)
				);
			}else{
				$validator 					=	Validator::make(
					Input::all(),
					array(
						'player'=> 'required',
						'date_from'	=> 'required',
						'date_till' => 'required',
					)
				);

			}
			if ($validator->fails()){
				$errors 	=	$validator->messages();
				$response	=	array(
					'success' 	=> false,
					'errors' 	=> $errors
				);
				return Response::json($response); 
				die;
			}else{ 
					$club = !empty(Input::get('club')) ? Input::get('club') : Auth::guard('admin')->user()->id;
					$availabiltyDetail = Availability::where('club',$club)
												->where('player',Input::get('player'))
												->where('date_from',Input::get('date_from'))
												->where('date_till',Input::get('date_till'))
												->first();
					$availabiltyDetailCount = Availability::where('club',$club)
												->where('player',Input::get('player'))
												->where('date_from',Input::get('date_from'))
												->where('date_till',Input::get('date_till'))
												->count();
					if(!empty($availabiltyDetail) && $availabiltyDetail->id != $availability_id && $availabiltyDetailCount > 0 ){ 
						Session::flash('error',trans("Player dates already in records.Please try with another dates."));
						$response	=	array(
					    	'success' 	=>	'1',
					    	'message' 	=>	trans("Player dates already in records.Please try with another dates.")
					    ); 
					    return Response::json($response);
					}
					$mode = User::where('id',$club)->value('game_mode');
					$obj 		=  Availability::find($availability_id);
					$obj->player =  !empty(Input::get('player')) ? Input::get('player'):'';
					$obj->date_from =  !empty(Input::get('date_from')) ? Input::get('date_from'):'';
					$obj->date_till	=  !empty(Input::get('date_till')) ? Input::get('date_till'):'';
					$obj->reason	=  !empty(Input::get('reason')) ? Input::get('reason'):'';
					
					$club = $mode;
					$obj->save();
				    Session::flash('success',trans("Availability has been upated successfully"));
					$response	=	array(
				    	'success' 	=>	'1',
				    	'message' 	=>	trans("Player has been upated successfully")
				    ); 
				     return Response::json($response);
			}
		}
	}//end update Forum-manager



	/**
	 * Function for change is_active of Forum
	 *
	 * @param $Id as id of Forum
	 * @param $Forum is_active as is_active of Forum
	 *
	 * @return redirect page. 
	 */	
	public function updateStatus($Id = 0, $Status = 0){
		Availability::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/availability');
	} // end updateStatus()
		
	/**
	/**
	* Function for mark a Forum as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deleteAvailability($Id=0){
		$userDetails	=	Availability::findOrFail($Id); 
		$userModel		=	Availability::where('id',$Id)->delete();
		return Redirect::to('admin/availability');
	}// end deleteForum

	public function get_club_player_list(){
		$clubId = Input::get('id');
		$player = new Player;
		$getClubPlayer = $player->get_players_by_club($clubId); 
		return View::make('admin.availabilities.get_club_player_list',compact('getClubPlayer'));
	}
	public function getClubModeList(){ 
		$mode = Input::get('id');
		$club_id = Input::get('club_id');
		$searchVariable = Input::get('searchVariable');
		$club = new Club;
		$getClubMode = $club->get_club_mode($mode);
		return View::make('admin.availabilities.get_club_mode_list',compact('getClubMode','searchVariable','club_id'));
	}

	public function getClubPlayerList(){ 
		$club = Input::get('id');
		$player_id = Input::get('player_id');
		$player = new Player;
		$getClubPlayer = $player->get_players_by_club($club);
		return View::make('admin.availabilities.get_club_player_list',compact('getClubPlayer','player_id'));
	}
	
}// end ClubController class
<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Slider;
use Illuminate\Support\Facades\Input;
use mjanssen\BreadcrumbsBundle\Breadcrumbs as Breadcrumb;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
/**
* SliderController Controller
*
* Add your methods in the class below
*
* This file will render views from views/Slider
*/
	class SliderController extends BaseController {
/**
* Function for display all Slider    
*
* @param $type as category of Slider 
*
* @return view page. 
*/
	public function listSlider($type=''){
		if(empty($type)) {
			return Redirect::to('admin/dashboard');
		}
		$DB				=	Slider::query()->where('slider_type',$type);
		$searchVariable	=	array(); 
		$inputGet		=	Input::get();
		if (Input::get()) { 
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		
		$sortBy = (Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  = (Input::get('order')) ? Input::get('order')   : 'DESC';
		$result = $DB->orderBy($sortBy, $order)->paginate(Config::get("Reading.records_per_page")); 
		
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		
		return  View::make('admin.slider.index',compact('result','searchVariable','sortBy','order','type','query_string'));
	}// end listSlider()
/**
* Function for display page  for add new Slider  
*
* @param $type as category of Slider 
*
* @return view page. 
*/
	public function addSlider($type=''){	
		return  View::make('admin.slider.add',compact('type'));
	} //end addSlider()
/**
* Function for save added Slider page
*
* @param null
*
* @return redirect page. 
*/
	function saveSlider($type=''){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData										=	Input::all();
		$validator = Validator::make(
			Input::all(),	
			array(
				'image' => 'required|mimes:'.IMAGE_EXTENSION,
				
			)
		);
		if ($validator->fails()){	
			return Redirect::to('admin/slider-manager/add-slider/'.$type)
				->withErrors($validator)->withInput();
		}else{
			$Slider = new Slider;
			$Slider->slider_type = 	$type;
			if(input::hasFile('image')){
				$extension 			=	Input::file('image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	SLIDER_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-slider.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('image')->move($folderPath, $userImageName)){
					$Slider->image		=	$image;
				}
			}
			$Slider->save(); 
			Session::flash('flash_notice', trans(ucfirst($type).' added successfully')); 
			return Redirect::to('admin/slider-manager/'.$type);
		}
	}//end saveSlider()
/**
* Function for display page  for edit Slider page
*
* @param $Id ad id of Slider 
* @param $type as category of Slider 
*
* @return view page. 
*/	
	public function editSlider($Id,$type){
		$slider				=	Slider::find($Id);
		if(empty($slider)) {
			return Redirect::to('admin/slider-manager/'.$type);
		}
		$slider	=	Slider::where('id', '=',  $Id)->first();
		return  View::make('admin.slider.edit',compact('slider','type'));
	}// end editSlider()
/**
* Function for update Slider 
*
* @param $Id ad id of Slider 
* @param $type as category of Slider 
*
* @return redirect page. 
*/
	function updateSlider($Id,$type=''){
		Input::replace($this->arrayStripTags(Input::all()));
		$validator 										= 	Validator::make(
			Input::all(),
			array(
				'image' =>  'mimes:'.IMAGE_EXTENSION,
			)
		);
		if ($validator->fails()){	
			return Redirect::to('admin/slider-manager/edit-slider/'.$Id.'/'.$type)
				->withErrors($validator)->withInput();
		}else{
			$slider = 	Slider:: find($Id);
			if(Input::hasFile('image')){
				$image 					=	Slider::where('id',$Id)->value('image');
				@unlink(SLIDER_IMAGE_ROOT_PATH.$image);
				$extension 			=	Input::file('image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	SLIDER_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-slider.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('image')->move($folderPath, $userImageName)){
					$slider->image		=	$image;
				}
			} 
			$slider->save();
			Session::flash('flash_notice',trans(ucfirst($type)." updated successfully")); 
			return Redirect::intended('admin/slider-manager/'.$type);
		}
	}// end updateSlider()
/**
* Function for update Slider  status
*
* @param $Id as id of Slider 
* @param $Status as status of Slider 
* @param $type as category of Slider 
*
* @return redirect page. 
*/	
	public function updateSliderStatus($Id = 0, $Status = 0,$type=''){
		if($Status == 0	){
			$statusMessage	=	trans(ucfirst($type)." deactivated successfully");
		}else{
			$statusMessage	=	trans(ucfirst($type)." activated successfully");
		}
		$this->_update_all_status('Slider_managers',$Id,$Status);
		
		/* if($Status == 1){
			$message				=	trans("messages.master.master_activate_message");
		}else{
			$message				=	trans("messages.master.master_deactivate_message");
		}
		$model						=	Slider::find($Id);
		$model->is_active			=	$Status;
		$model->save(); */
		Session::flash('flash_notice',$statusMessage); 
		return Redirect::to('admin/slider-manager/'.$type);
	}// end updateSliderStatus()
/**
* Function for delete Slider 
*
* @param $Id as id of Slider 
* @param $type as category of Slider 
*
* @return redirect page. 
*/	
	public function deleteSlider($Id = 0,$type=''){
		$slider					=	Slider::find($Id) ;
		if(!empty($slider)){
			if(File::exists(SLIDER_IMAGE_ROOT_PATH.$slider->image)){
		        @unlink(SLIDER_IMAGE_ROOT_PATH.$slider->image);
		    }
			$this->_delete_table_entry('slider_managers',$Id,'id');
			Session::flash('flash_notice', trans(ucfirst($type)." removed successfully"));  
		}else{
			Session::flash('error', trans("Invalid url"));  
		}
		return Redirect::to('admin/slider-manager/'.$type);
	}// end deleteSlider()

}// end SliderController
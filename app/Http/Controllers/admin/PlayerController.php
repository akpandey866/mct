<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Player;
use App\Model\ForumComment;
use App\Model\User;
use App\Model\Club;
use App\Model\DropDown;
use App\Model\Team;
use App\Model\PlayerPackDetail;
use App\Model\ClubActivation;
use App\Model\FixtureScorcard;
use Illuminate\Http\Request;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator;
use Carbon\Carbon;

class PlayerController extends BaseController {
/**
* Function for display all event
*
* @param null
*
* @return view page.
*/
	public function index(){ 
		$DB 					= 	Player::query();
		$DB1 					= 	User::query();
		$DB2 					= 	DropDown::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		$drop_down				=	new DropDown();
		$batStyle = $drop_down->get_master_list("bat-style");
		$bowlStyle = $drop_down->get_master_list("bowl-style");

		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			
			
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					if($fieldName=="game_mode"){
						$DB->where("game_mode",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="game_name"){
						$DB->where("game_name",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="is_active"){
						$DB->where("players.is_active",'like','%'.$fieldValue.'%'); 
					}
					if($fieldName=="position"){
						$DB->where("players.position",'like','%'.$fieldValue.'%'); 
					}
					//$DB->where("players.$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		$sortBy = 	(Input::get('sortBy')) ? Input::get('sortBy') : 'players.created_at';
	    $order  = 	(Input::get('order')) ? Input::get('order')   : 'DESC';

	    $playerCount = Player::where('club',Auth::guard('admin')->user()->id)->count();
		$totalUserSum = 0;
	    if(Auth::guard('admin')->user()->user_role_id == 1){
	    	$result = 	$DB->leftJoin('users' , 'players.club' , '=' , 'users.id')
						->leftJoin('dropdown_managers' , 'players.category' , '=' , 'dropdown_managers.id')
						->leftJoin('dropdown_managers as dd' , 'dd.id' , '=' , 'players.position')
						->select('players.*','users.club_name','dropdown_managers.name','dd.name as position_name','users.game_mode as game_mode','users.game_name as game_name')
						->orderBy($sortBy, $order)
						->paginate(Config::get("Reading.records_per_page"));
	    }else{
			$result	= 	$DB->leftJoin('users' , 'players.club' , '=' , 'users.id')
						->leftJoin('dropdown_managers' , 'players.category' , '=' , 'dropdown_managers.id')
						->leftJoin('dropdown_managers as dd' , 'dd.id' , '=' , 'players.position')
						->select('players.*','users.club_name','dropdown_managers.name','dd.name as position_name')
						->where('players.club',Auth::guard('admin')->user()->id)
						->orderBy($sortBy, $order)
						->paginate(Config::get("Reading.record_game_per_page"));

			$extendedPlanCount = PlayerPackDetail::leftJoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
									->where('club',Auth::guard('admin')->user()->id)
									->select('player_packs.name as player_number')
									->get();
			$sum=0;
			if(!$extendedPlanCount->isEmpty()){ 
				foreach ($extendedPlanCount as $key => $value) {
					$sum +=$value->player_number;
				}
			}
			$totalUserSum = $playerCount + $sum;
	    }

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
			$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		$drop_down				=	new DropDown();
		$position				=	$drop_down->get_master_list("position");
		$category				=	$drop_down->get_master_list("category");
		$svalue					=	$drop_down->get_master_list("svalue");
		$svalue = Config::get('player_price_list'); 
		$club					=	new Club();
		$cludDetails			=	$club->get_club_list();

		/*Start showing player data start here*/
		//Total stored player usedPaidPlayer freeusedPlayer
		$playerCount = Player::where('club',Auth::guard('admin')->user()->id)->count();	
		$freeusedPlayer = 0;
		$availableFreePlayer = 0;
		if(MAXIMUMPLAYER > $playerCount){
			$availableFreePlayer = MAXIMUMPLAYER - $playerCount;
			$freeusedPlayer = MAXIMUMPLAYER - $availableFreePlayer ;
		}else{ 
			$freeusedPlayer = MAXIMUMPLAYER ;
		}
		//pr($playerCount);	 usedPaidPlayer freeusedPlayer
		$paidPlayer = ClubActivation::where('club_id',Auth::guard('admin')->user()->id)->value('paid_player');
		$extendedPlanCount = PlayerPackDetail::leftJoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
								->where('club',Auth::guard('admin')->user()->id)
								->select('player_packs.name as player_number')
								->get();
		$purchasePlayerSum = 0; 
		$playerCountWithPremium = 0; 
		if(!$extendedPlanCount->isEmpty()){ 
			
			foreach ($extendedPlanCount as $key => $value) {
				$purchasePlayerSum +=$value->player_number;
			}
			$playerCountWithPremium = MAXIMUMPLAYER + $paidPlayer + $purchasePlayerSum;
		} 

		//Used Paid Player logic start here   availableFreePlayer 
		$usedPaidPlayer = 0;
		$availablePaidPlayer = 0;
		$sdfsdf = MAXIMUMPLAYER + $paidPlayer;         
		if($playerCountWithPremium > $playerCount ){
			if($playerCount >  $sdfsdf){
				$availablePaidPlayer = $playerCountWithPremium - $playerCount;
				$usedPaidPlayer = $purchasePlayerSum - $availablePaidPlayer;
			}
			
		}else{
			$usedPaidPlayer = $purchasePlayerSum;
		}

		//Used Paid Player logic finish here


		//paid players
	
		$extendedPlayerSum = MAXIMUMPLAYER + $paidPlayer + $purchasePlayerSum;
		$playerCountNew = $playerCount - $paidPlayer;
		/*Start showing player data finish here*/

		return  View::make('admin.player.index', compact('result' ,'searchVariable','sortBy','order','userType','query_string','position','category','svalue','jvalue','cludDetails','playerCount','totalUserSum','cludDetails','svalue','category','position','paidPlayer','playerCountNew','freeusedPlayer','sum','availablePaidPlayer','usedPaidPlayer','purchasePlayerSum','availableFreePlayer', 'batStyle', 'bowlStyle'));
	 }

	 public function addMorePlayer(){ 
	 	$drop_down				=	new DropDown();
		$position				=	$drop_down->get_master_list("position");
		$category				=	$drop_down->get_master_list("category");
		//$svalue					=	$drop_down->get_master_list("svalue");
		$svalue = Config::get('player_price_list'); 
		$club					=	new Club();
		$cludDetails			=	$club->get_club_list();
		$counter  = Input::get('counter');
		$batStyle = $drop_down->get_master_list("bat-style");
		$bowlStyle = $drop_down->get_master_list("bowl-style");
		return  View::make('admin.player.add_more_player', compact('counter','cludDetails','svalue','category','position','batStyle','bowlStyle'));
	}

	public function checkPlayerCount(){
		if(Auth::guard('admin')->user()->game_mode == 1 && Auth::guard('admin')->user()->is_game_activate == 1){ 
			$extendedPlanCount = PlayerPackDetail::leftJoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
									->where('club',Auth::guard('admin')->user()->id)
									->select('player_packs.name as player_number')
									->get();
			$playerCount = Player::where('club',Auth::guard('admin')->user()->id)->count();						
			$sum=0;
			if(!$extendedPlanCount->isEmpty()){ 
				foreach ($extendedPlanCount as $key => $value) {
					$sum +=$value->player_number;
				}
			}
			$paidPlayer = ClubActivation::where('club_id',Auth::guard('admin')->user()->id)->value('paid_player');
			$extendedPlayerSum = MAXIMUMPLAYER + $paidPlayer + $sum;
			if($playerCount >= $extendedPlayerSum){
				$response	=	array(
					'success' 	=> 1,
					'msg' 	=> "Your $extendedPlayerSum free player quota has been used. Please purchase player block to add new players."
				);
				return Response::json($response); 
			}else{
				$response	=	array(
					'success' 	=> 2,
				);
				return Response::json($response); 
			}
		}else{
			$response	=	array(
				'success' 	=> 2,
			);
			return Response::json($response); 
		}

		//Above is c
		//$response = array('success' => 2,);
		//return Response::json($response); 

	}
	/**
	* Function for add Forum
	*
	* @param null
	*
	* @return view page. 
	*/ 
	public function addPlayer(){ 
		//$playerCount = Player::where('club',Auth::guard('admin')->user()->id)->where('category',6)->count();
		$drop_down				=	new DropDown();
		$position				=	$drop_down->get_master_list("position");
		$category				=	$drop_down->get_master_list("category");
		$svalue					=	$drop_down->get_master_list("svalue");
		$batStyle = $drop_down->get_master_list("bat-style");
		$bowlStyle = $drop_down->get_master_list("bowl-style");
		$svalue = Config::get('player_price_list'); 
		// print_r($svalue); die; 
		$jvalue					=	$drop_down->get_master_list("jvalue");
		$club					=	new Club();
		$cludDetails			=	$club->get_club_list();
		
		if(Auth::guard('admin')->user()->user_role_id != 1){
			$club_id = Auth::guard('admin')->user()->id;
			$team =	new Team();
			$teamList =	$team->get_team_by_club($club_id);
			$extendedPlanCount = PlayerPackDetail::leftJoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
									->where('club',Auth::guard('admin')->user()->id)
									->select('player_packs.name as player_number')
									->get();

			$playerCount = Player::where('club',Auth::guard('admin')->user()->id)->count();						
			$sum=0;

			if(!$extendedPlanCount->isEmpty()){ 
				foreach ($extendedPlanCount as $key => $value) {
					$sum +=$value->player_number;
				}
			}


			$extendedPlayerSum = MAXIMUMPLAYER + $sum;
			if($playerCount >= $extendedPlayerSum){
				Session::flash('flash_notice', trans("Your 20 free player quota has been used. Please purchase player block to add new players.")); 
				return Redirect::to('admin/player');
			}
		}

	
		return  View::make('admin.player.add',compact('position','category','svalue','cludDetails','teamList', 'batStyle', 'bowlStyle'));
	}//end addPlayer()
	 
	public function checkPlayerCountSavePlayer(){
		$playerCount = Player::where('club',Auth::guard('admin')->user()->id)->count();			
		$extendedPlanCount = PlayerPackDetail::leftJoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
								->where('club',Auth::guard('admin')->user()->id)
								->select('player_packs.name as player_number')
								->get();				
		$sum=0;
		if(!$extendedPlanCount->isEmpty()){ 
			foreach ($extendedPlanCount as $key => $value) {
				$sum +=$value->player_number;
			}
		}
		$paidPlayer = ClubActivation::where('club_id',Auth::guard('admin')->user()->id)->value('paid_player');
		$extendedPlayerSum = MAXIMUMPLAYER + $paidPlayer + $sum;
		if($playerCount >= $extendedPlayerSum){
			return 1;
		}else{
			return 0;
		}
	}
	
	
	/*public function savePlayer(){
	Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		//prd($thisData);
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'first_name'=> 'required',
					'last_name' => 'required',
					'position' 	=> 'required',
					'category'=> 'required',
					'svalue' => 'required',
					'club' 	=> 'required',
					'image' => 'required|max:2048|mimes:'.IMAGE_EXTENSION,
					
				),
				array(
					'svalue.required' => "The value field is required.",
				)
			);
			if ($validator->fails()){
				$errors 	=	$validator->messages();
				$response	=	array(
					'success' 	=> false,
					'errors' 	=> $errors
				);
				return Response::json($response); 
				die;
			}else{ 
				$obj 							=  new Player;
				$fullName						=  ucwords(Input::get('first_name').' '.Input::get('last_name'));
				$obj->first_name 				=  Input::get('first_name');
				$obj->last_name 				=  Input::get('last_name');
				$obj->full_name 				=  $fullName;
				$obj->nickname 					=  Input::get('nickname');
				$obj->position 					=  !empty(Input::get('position')) ? Input::get('position'):0;
				$obj->category 					=  !empty(Input::get('category')) ? Input::get('category'):0;
				$obj->svalue 					=  !empty(Input::get('svalue')) ? Input::get('svalue'):0;
				//$obj->jvalue 					=  !empty(Input::get('jvalue')) ? Input::get('jvalue'):0;
				$obj->club 						=  !empty(Input::get('club')) ? Input::get('club'):0;
				//$obj->team_id 					=  !empty(Input::get('team')) ? Input::get('team'):0;
				//$obj->is_active					=  1; 
				
				if(input::hasFile('image')){
				$extension 			=	Input::file('image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	PLAYER_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-player.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('image')->move($folderPath, $userImageName)){
					$obj->image		=	$image;
						}
					}
				$obj->save();
				$response	=	array(
			    	'success' 	=>	'1',
			    	'message' 	=>	trans("Player has been added successfully.")
			    ); 
			    return Response::json($response);
			}
		}
	}*///end saveForum

	public function savePlayer(){
	Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
			if(!empty(current($thisData['data']))){
				$validator 					=	Validator::make(
					Input::all(),
					array(
						'data.*.first_name'=> 'required',
						'data.*.last_name' => 'required',
						'data.*.value' => 'required',
						'data.*.position' => 'required',
						'data.*.image' => 'max:2048|mimes:'.IMAGE_EXTENSION,
						
					),
					array(
						'data.*.first_name.required' => "The first name field is required.",
						'data.*.last_name.required' => "The last name field is required.",
						'data.*.value.required' => "The value field is required.",
						'data.*.position.required' => "The position field is required.",
					)
				);
				if ($validator->fails()){
					$errors 	=	$validator->messages();
					$response	=	array(
						'success' 	=> false,
						'errors' 	=> $errors
					);
					return Response::json($response); 
					die;
				}else{
 
					foreach ($thisData['data'] as $key => $value) {
						$obj 							=  new Player;
						$fullName						=  ucwords($value['first_name']." ".$value['last_name']);
						$obj->first_name 				=  $value['first_name'];
						$obj->last_name 				=  $value['last_name'];
						$obj->full_name 				=  $fullName;
						$obj->position 					= !empty($value['position']) ? $value['position']:0;
						$obj->category 					=  !empty($value['category']) ? $value['category']:0;
						$obj->svalue 					=  !empty($value['value']) ? $value['value']:0;
						$obj->club 						=  !empty($value['club']) ? $value['club']:0;	
						$obj->bat_style = !empty($value['bat_style']) ? $value['bat_style']:0;
						$obj->bowl_style = !empty($value['bowl_style']) ? $value['bowl_style']:0;
						if(!empty($value['image'])){
						$extension 			=	$value['image']->getClientOriginalExtension();
						$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
						$folderPath			=	PLAYER_IMAGE_ROOT_PATH.$newFolder; 
						if(!File::exists($folderPath)){
							File::makeDirectory($folderPath, $mode = 0777,true);
						}
						$userImageName = time().'-player.'.$extension;
						$image = $newFolder.$userImageName;
						if($value['image']->move($folderPath, $userImageName)){
								$obj->image		=	$image;
							}
						}
						$obj->save();
						if(Auth::guard('admin')->user()->game_mode == 1){
							if(Auth::guard('admin')->user()->is_game_activate == 1){
								$checkPlayerCount = $this->checkPlayerCountSavePlayer();
								if($checkPlayerCount == 1){
									Session::flash('flash_notice', trans("Some records couldn't added because player pack has been exhausted.Please purchase player pack to add player.")); 
									break;
								}
							}
						}						
					}
				}
			}	
		Session::flash('flash_notice', trans("Player has been added successfully.")); 
		$response	=	array(
	    	'success' 	=>	'1',
	    	'message' 	=>	trans("Player has been added successfully.")
	    ); 
	    return Response::json($response);
	}//end saveForum

	/**
	* Function for edit Forums
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editPlayer($userId = 0){   
		$userDetails			=	Player::find($userId); 
		if(empty($userDetails)) {
			return Redirect::to('admin/player');
		}
		if($userId){
			//$userDetails =	Player::find($userId);
			$userDetails =	Player::leftJoin('dropdown_managers as dm','dm.id','=','players.position')
									->leftJoin('dropdown_managers','dropdown_managers.id','=','players.bat_style')
									->leftJoin('dropdown_managers as DB','DB.id','=','players.bowl_style')
									->where('players.id',$userId)
									->select('players.*','dm.name as player_position','dropdown_managers.name as player_bat_style','DB.name as player_bowl_style')
									->first();

			$drop_down =	new DropDown();
		    $position =	$drop_down->get_master_list("position");
		    $svalue	=	$drop_down->get_master_list("svalue");
		    $svalue = Config::get('player_price_list'); 
		    // print_r($svalue); die; 
		    $jvalue =	$drop_down->get_master_list("jvalue");
		    $club	=	new Club();
		    $cludDetails =	$club->get_club_list();
		    $team =	new Team();
			$teamList =	$team->get_team_by_club($userDetails->club);
			$batStyle = $drop_down->get_master_list("bat-style");
			$bowlStyle = $drop_down->get_master_list("bowl-style");
			return View::make('admin.player.edit', compact('userDetails','position','userId','position','svalue','jvalue','cludDetails','teamList','batStyle','bowlStyle'));
		}
	} // 

	/**
* Function for update Forum
*
* @param null
*
* @return view page. 
*/
	public function updatePlayer(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		// print_r($thisData); die; 
		$user_id = Input::get('player_id');
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'first_name'		=> 'required',
					'last_name'		    => 'required',
					'position' 			=> 'required',
					'svalue' 			=> 'required',
					'club' 				=> 'required',
					'image' 		    => 'max:2048|mimes:'.IMAGE_EXTENSION,
					
				),
				array(
					'svalue.required' => "The value field is required.",
				)
			);
			if ($validator->fails()){
				$errors 	=	$validator->messages();
				$response	=	array(
					'success' 	=> false,
					'errors' 	=> $errors
				);
				return Response::json($response); 
				die;
			}else{ 
				$obj 							=  Player::find($user_id);
				$first_name	=  Input::get('first_name');
				$last_name =  Input::get('last_name');
				$fullName =  $first_name." ".$last_name;
				$obj->first_name 				=  Input::get('first_name');
				$obj->full_name =  $fullName;
				$obj->last_name =  Input::get('last_name');
				$obj->position =  !empty(Input::get('position')) ? Input::get('position'):0;
				$obj->svalue =  !empty(Input::get('svalue')) ? Input::get('svalue'):0;
				$obj->club 	=  !empty(Input::get('club')) ? Input::get('club'):0;
				$obj->team_id =  !empty(Input::get('team')) ? Input::get('team'):0;
				$obj->bat_style =  !empty(Input::get('bat_style')) ? Input::get('bat_style'):0;
				$obj->bowl_style =  !empty(Input::get('bowl_style')) ? Input::get('bowl_style'):0;
				$obj->description =  !empty(Input::get('description')) ? Input::get('description'):'';
				if(input::hasFile('image')){
				$extension 			=	Input::file('image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	PLAYER_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-player.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('image')->move($folderPath, $userImageName)){
				$obj->image		=	$image;
					}
				}
				$obj->save();
				$response	=	array(
			    	'success' 	=>	'1',
			    	'message' 	=>	trans("Player has been upated successfully")
			    ); 
			     return Response::json($response);
			}
		}
	}//end update Forum-manager
	
	public function viewPlayer($userId = 0){
		
		$userDetails		=	Player::find($userId); 				
		if(empty($userDetails)) {
			return Redirect::to('admin/player/');
		}
		$userDetails = Player::leftJoin('dropdown_managers','dropdown_managers.id','=','players.position')
							   ->select('players.*','dropdown_managers.name as position')
							   ->where('players.id',$userId)
							   ->first();
		//prd($userDetails);die;		
		return View::make('admin.player.view', compact('userDetails'));
	} // 

	
	
	

	/**
	 * Function for change is_active of Forum
	 *
	 * @param $Id as id of Forum
	 * @param $Forum is_active as is_active of Forum
	 *
	 * @return redirect page. 
	 */	
	public function updateStatus($Id = 0, $Status = 0){
		Player::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/player');
	} // end updateStatus()
	
	
	/**
	/**
	* Function for mark a Forum as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deletePlayer($Id=0){
		$userDetails	=	Player::find($Id); 
		if(empty($userDetails)) {
			return Redirect::to('admin/player');
		}
		if($Id){
			$userModel		=	Player::where('id',$Id)->delete();
		}
		return Redirect::to('admin/player');
	}// end deleteForum

	public function getTeamList(){
		$clubId = Input::get('club_id');
		$oldClubId = Input::get('club_id');
		$team =	new Team();
		$teamList =	$team->get_team_by_club($clubId);
		return View::make('admin.player.get_team_dropdown', compact('teamList','clubId','oldClubId'));
	}

	public function addImportPlayer(){
		return View::make('admin.player.add_import_player');
	}
	public function importPlayer(){
		if (isset($_POST["import"])) { 
		    $fileName = $_FILES["file"]["tmp_name"];
		    if ($_FILES["file"]["size"] > 0) {
		        $file = fopen($fileName, "r");
		        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
		        	$playerObj = new Player();
		        	$fullName =  ucwords($column[0]." ".$column[1]);
		        	$playerObj->first_name = $column[0];
		        	$playerObj->last_name = $column[1];
		        	$playerObj->full_name = $fullName;
		        	$playerObj->club = Auth::guard('admin')->user()->id;
		        	$playerObj->is_active = 0;
		        	$playerObj->save();
		        	if(Auth::guard('admin')->user()->game_mode == 1){
						if(Auth::guard('admin')->user()->is_game_activate == 1){
							$checkPlayerCount = $this->checkPlayerCountSavePlayer();
							if($checkPlayerCount == 1){
								break;
							}
						}
					}


		        }
		        $checkPlayerCount = $this->checkPlayerCountSavePlayer();
				if($checkPlayerCount == 1){
					Session::flash('error', trans("Some records couldn't added because player pack has been exhausted.Please purchase player pack to add player.")); 
					return Redirect::to('admin/player');
				}else{
					Session::flash('flash_notice', trans("Player successfully imported.")); 
					return Redirect::to('admin/player');
				}
		       
		    }
		}else{
			Session::flash('flash_notice', trans("no files found.")); 
			return Redirect::back();
		}
	}

	public function getPlayelistForClub(Request $request){
		$playerId = $request->player_id;
		if(!empty($playerId)){
			$playerScoreSum = FixtureScorcard::where('player_id',$playerId) 
							->groupBy('player_id')
							->select(DB::raw('SUM(run) as runs,SUM(fours) as fours,SUM(sixes) as sixes,SUM(overs) as overs,SUM(mdns) as mdns,SUM(wks) as wks,SUM(cs) as cs,SUM(cwks) as cwks,SUM(sts) as sts,SUM(rods) as rods,SUM(roas) as roas,SUM(dks) as dks,SUM(hattrick) as hattrick,SUM(fantasy_points) as fantasy_points,(select full_name from players where players.id=fixture_scorecards.player_id) as player_name'))
                			->first();
            return View::make('admin.player.get_player_list_for_club', compact('playerScoreSum'));
		}
	}
}// end ClubController class
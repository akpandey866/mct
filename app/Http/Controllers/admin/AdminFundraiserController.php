<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\User;
use App\Model\Fundraiser;
use App\Model\FundraiserMessage;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator,Stripe;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminFundraiserController extends BaseController {
/**
* Function for display all fundraiser page
*
* @param null
*
* @return view page. 
*/
	public function index(){	
		$DB							=	User::query();
		$searchVariable				=	array(); 
		$inputGet					=	Input::get();
		if((Input::get() || isset($inputGet['display'])) || isset($inputGet['page']) ){
			$searchData				=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);
			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue)){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
					$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
				}
			}
		}
		$sortBy 					= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'users.updated_at';
	    $order  					= 	(Input::get('order')) ? Input::get('order')   : 'desc';
		$result 					= 	$DB->leftJoin('fundraisers', 'users.id', '=', 'fundraisers.club_id')
										->select('entry_price as amount','fundraising_target as target','game_name','users.id','is_active')
										->selectRaw('sum(fundraisers.amount) AS raised')
										->where('user_role_id',CLUBUSER)
										->where('is_deleted',0)
										->where('game_name','<>','')
										->groupBy('users.id')
										->orderBy($sortBy,$order)
										->paginate(Config::get("Reading.records_per_page"));

		$complete_string			=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string				=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.adminfundraiser.index',compact('result','searchVariable','sortBy','order','query_string'));
	} 

	public function editMessage($Id){  
		$result	=	FundraiserMessage::where('club_id',$Id)->first();
		return  View::make('admin.adminfundraiser.edit_message',compact('result','Id'));
	}

	public function updateMessage($Id){
		$this_data				=	Input::all();		
		$validator = Validator::make(
			array(
				'message' 				=> Input::get('message'),
			),
			array(
				'message' 				=> 'required',
			)
		);
		
		if($validator->fails()){	
			return Redirect::to('admin/admin-fundraiser/edit-message/'.$Id)->withErrors($validator)->withInput();
		}else{
			FundraiserMessage::updateOrCreate(['club_id' => $Id],['message'=>Input::get('message')]);
			//FundraiserMessage::where('club_id', $Id)->update(array('message'=>Input::get('message')));
			Session::flash('flash_notice', trans("Fundraiser message updated successfully")); 
			return Redirect::to('admin/admin-fundraiser');
		}
	}

	public function editAmount($Id){
		$result	=	User::findOrFail($Id);
		return  View::make('admin.adminfundraiser.edit_amount',compact('result'));
	}

	public function updateAmount($Id){ 
		$this_data				=	Input::all();		
		$validator = Validator::make(
			array(
				'entry_price' 	=> Input::get('entry_price'),
				'fundraising_target' => Input::get('fundraising_target'),
			),
			array(
				'entry_price' => 'required',
				'fundraising_target' => 'required',
			),
			['fundraising_target.required'=>'Fundraising target is required.','entry_price.required'=>'Fundraising amount is required.']
		);
		
		if($validator->fails()){	
			return Redirect::to('admin/admin-fundraiser/edit-amount/'.$Id)->withErrors($validator)->withInput();
		}else{
			User::where('id', $Id)->update(array('entry_price'=>Input::get('entry_price'),'fundraising_target'=>Input::get('fundraising_target')));
			Session::flash('flash_notice', trans("Fundraiser amount updated successfully")); 
			return Redirect::to('admin/admin-fundraiser');
		}
	}

	public function adminGetGameName(){
		$mode = Input::get('mode');
		$game_name = Input::get('game_name');
		$getModesGames = User::where('game_mode',$mode)->where('user_role_id',CLUBUSER)->where('game_name','!=','')->orderBy('club_name','ASC')->pluck('game_name','game_name')->all();
		return  View::make('admin.adminfundraiser.admin_get_game_name',compact('getModesGames','game_name'));
	}

}// end ClubController class
<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Branding;
use App\Model\PlayerPack;
use App\Model\PlayerPackDetail;
use App\Model\ScorerAccess;
use App\Model\User;
use App\Model\VerifyUser;
use App\Model\Player;
use App\Model\Grade;
use App\Model\Fixture;
use App\Model\GamePrize;
use App\Model\Team;
use App\Model\ClubActivation;
use App\Model\GameSetting;
use App\Model\LockoutLog;
use App\Model\UserTeams;
use App\Model\UserTeamLogs; 
use App\Model\FundraiserMessage;
use App\Model\UserTeamsCVCTrack; 
use App\Model\PlayerTradeInOut; 
use App\Model\FixtureScorcard; 
use App\Model\LockoutMailSendToUserLog; 
use App\Model\EmailAction; 
use App\Model\EmailTemplate; 
use App\Model\Fundraiser;
use App\Model\Club;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator,Stripe;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CommonController extends BaseController {
	/**
	* Function for edit gameprize
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editBranding(){  
		$userDetails = Branding::where(['club_id'=>Auth::guard('admin')->user()->id])->first();
		//prd($userDetails);
		return View::make('admin.common.edit_branding', compact('userDetails'));

	} // 

/**
* Function for update gameprize
*
* @param null
*
* @return view page. 
*/
	public function updateBranding(Request $request){ 
		$gameMode = Auth::guard('admin')->user()->game_mode;
		$price = 0;
		if($gameMode == 1){
			$price = SENIORBRANDING;
		}elseif ($gameMode == 2) {
			$price = 100;
		}elseif ($gameMode == 3) {
			$price = 150;
		}
		Input::replace($this->arrayStripTags(Input::all()));
		$brandingCount = Branding::where('club_id',Auth::guard('admin')->user()->id)->count();
		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $getValues = Stripe\Charge::create ([
            "amount" => $price*100,
            "currency" => "aud",
            "source" => $request->stripeToken,
            "description" => "Brainding activatd successfully." 
        ]);
		$brandingID = Branding::where('club_id',Auth::guard('admin')->user()->id)->value('id');
		$obj = Branding::find($brandingID);
		$obj->transaction_id = $getValues->id;
    	$obj->payment_description = $getValues->description;
    	$obj->amount = $price;
    	$obj->is_paid = 1;
    	$obj->save();
	    Session::flash('success',trans("Thanks for activate branding."));
		return Redirect::to('admin/branding');
	}//end update Branding

	public function brandingPayment(Request $request){
		$gameMode = Auth::guard('admin')->user()->game_mode;
		$price = 0;
		if($gameMode == 1){
			$price = SENIORBRANDING;
		}elseif ($gameMode == 2) {
			$price = JUNIORBRANDING;
		}elseif ($gameMode == 3) {
			$price = LEAGUEBRANDING;
		}
		$brandingCount = Branding::where('club_id',Auth::guard('admin')->user()->id)->count();
		if($brandingCount > 0){
			$brandingID = Branding::where('club_id',Auth::guard('admin')->user()->id)->value('id');
			$obj = Branding::find($brandingID);
			$obj->name =  $request->name;
			$obj->url =  $request->url;
			if(Input::hasFile('logo')){
				$image 					=	Branding::where('id',$brandingID)->value('logo');
				@unlink(BRANDING_IMAGE_ROOT_PATH.$image);
				$extension 			=	Input::file('logo')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	BRANDING_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-branding.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('logo')->move($folderPath, $userImageName)){
					$obj->logo		=	$image;
				}
			}
        	$obj->amount = $price;
        	$obj->is_paid = 0;
        	$obj->save();
		}else{
			$obj = new Branding();
			$obj->name =  Input::get('name');
			$obj->url =  Input::get('url');
			$obj->club_id =  Auth::guard('admin')->user()->id;
			if(Input::hasFile('logo')){
				$extension 			=	Input::file('logo')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	BRANDING_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-PRIZE.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('logo')->move($folderPath, $userImageName)){
					$obj->logo		=	$image;
				}
			}
        	$obj->amount = $price;
        	$obj->is_paid = 0;
			$obj->save();
		}
		return View::make('admin.common.activate_game_branding',compact('price'));
	}

	public function updateBrandingForm(Request $request){
		$thisData = $request->all();
		$validator 					=	Validator::make(
			$request->all(),
			array(
				'name'			=> 'required',
				'url'			=> 'required',
			)
		);
		if ($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}else{
			$brandingID = Branding::where('club_id',Auth::guard('admin')->user()->id)->value('id');
			$obj = Branding::find($brandingID);
			$obj->name =  $request->name;
			$obj->url =  $request->url;
			if(Input::hasFile('logo')){
				$image 					=	Branding::where('id',$brandingID)->value('logo');
				@unlink(BRANDING_IMAGE_ROOT_PATH.$image);
				$extension 			=	Input::file('logo')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	BRANDING_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-branding.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('logo')->move($folderPath, $userImageName)){
					$obj->logo		=	$image;
				}
			}
        	$obj->save();
			Session::flash('success',trans("Branding has been upated successfully"));
			return Redirect::to('admin/branding');
		}
	}
	/**
	* Function for update gameprize
	*
	* @param null
	*
	* @return view page. 
	*/
	public function updateBrandingNewOne(){ 
		Input::replace($this->arrayStripTags(Input::all()));
		$brandingCount = Branding::where('club_id',Auth::guard('admin')->user()->id)->count();
 
		if($brandingCount > 0){ 
			$brandId = Branding::where('club_id',Auth::guard('admin')->user()->id)->value('id');
			$obj = Branding::find($brandId);
			$obj->name =  Input::get('name');
			$obj->url =  Input::get('url');
			if(Input::hasFile('logo')){
				$image 					=	Branding::where('id',1)->value('logo');
				@unlink(BRANDING_IMAGE_ROOT_PATH.$image);
				$extension 			=	Input::file('logo')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	BRANDING_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-PRIZE.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('logo')->move($folderPath, $userImageName)){
					$obj->logo		=	$image;
				}
			}
			$obj->save();
		}else{
			$obj = new Branding();
			$obj->name =  Input::get('name');
			$obj->url =  Input::get('url');
			$obj->club_id =  Auth::guard('admin')->user()->id;
			if(Input::hasFile('logo')){
				$extension 			=	Input::file('logo')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	BRANDING_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-PRIZE.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('logo')->move($folderPath, $userImageName)){
					$obj->logo		=	$image;
				}
			}
			$obj->save();
		}	
	    Session::flash('success',trans("Branding has been upated successfully"));
		return Redirect::to('admin/branding');
	}//end update Branding
	public function additionalPlayer(Request $request){
		$packsDetails = PlayerPack::get();
		if(\Request::isMethod('post')){
			$thisData = Input::all();
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'name.*'			=> 'required|numeric',
					'price.*'			=> 'required|numeric',
				),
				array(
					'name.required' => "Please Enter Name.",
					'price.required' => "Please Enter Price.",
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
				 
			}else{
				if(!empty($thisData['name']) && !empty($thisData['price'])){
					foreach ($thisData['name'] as $key => $value) {
						if(!empty($thisData['id'][$key])){
							$obj = PlayerPack::find($thisData['id'][$key]);
							$obj->name = $thisData['name'][$key];
							$obj->price = $thisData['price'][$key];
							$obj->save();
						}else{
							$obj = new PlayerPack();
							$obj->name = $thisData['name'][$key];
							$obj->price = $thisData['price'][$key];
							$obj->save();
						}
						
					}
				}
				Session::flash('success',trans("Player Pack has been upated successfully"));
				return Redirect::to('admin/player-packs');
			}
		}else{
			return View::make('admin.common.additional_players',compact('packsDetails'));
		}
	}
	public function addMorePlayerPacks(){ 
		$counter  = Input::get('counter');
		return  View::make('admin.common.add_more_player_packs', compact('counter'));
	}

	public function deleteAddMorePlayerPacks(){
		$id = Input::get('id');
		$flight = PlayerPack::find($id);
		$flight->delete();
		die;
	}


	public function purchasePlanPacks(Request $request){ 
		if($request->isMethod('post')){
			$thisData = Input::all();
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'plan'			=> 'required',
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
				 
			}else{
				$obj = new PlayerPackDetail();
				$obj->player_pack_id = $request->plan;
				$obj->club = Auth::guard('admin')->user()->id;
				$obj->save();
				Session::flash('success',trans("Thanks for subscribing the plan."));
				return Redirect::to('admin/purchase-player-packs');
			}
		}else{
			$playerPacks =  PlayerPack::get();
			$playerData = [];
			if(!$playerPacks->isEmpty()){ 
				foreach ($playerPacks as $key => $value) {
					$playerData[$value->id] = 'Add '.$value->name." players for $".$value->price;
				}
			}
			// dump($playerData); die; 
			return  View::make('admin.common.purchase_player_packs', compact('playerData'));
		}
	}


	public function additionalPlayerLogs(){ 
		if(Auth::guard('admin')->user()->user_role_id == 1){
			$result = PlayerPackDetail::leftJoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
						->leftJoin('users','users.id','=','player_pack_details.club')
						->select('player_pack_details.*','player_packs.name as player_number','player_packs.price as player_price','users.full_name as club_name')
						->paginate(Config::get("Reading.records_per_page"));
		}else{
			$result = PlayerPackDetail::leftJoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
					->leftJoin('users','users.id','=','player_pack_details.club')
					->where('player_pack_details.club',Auth::guard('admin')->user()->id)
					->select('player_pack_details.*','player_packs.name as player_number','player_packs.price as player_price','users.full_name as club_name')
					->paginate(Config::get("Reading.record_game_per_page"));
		}
		return  View::make('admin.common.additional_player_logs',compact('result'));
	}

	public function scorerAccess(){ 
		$user = new User();
		if(auth()->guard('admin')->user()->id != 1){
			$userList = UserTeams::leftJoin('users','users.id','=','user_teams.user_id')
							->where('club_id',Auth::guard('admin')->user()->id)
							->where('user_teams.is_active',1)
							->pluck('users.full_name','user_teams.user_id')->all();
			
			if(!empty($userList)){
				unset($userList[Auth::guard('admin')->user()->id]);
			}
			$teams = Team::where('club', auth()->guard('admin')->user()->id)->pluck('name', 'id'); 
		}else{
			$userList = $user->get_user_list(); 
			$teams = Team::pluck('name', 'id'); 
		}
		
		// dump($teams); die; 

		$playerIds = ScorerAccess::pluck('user_id','user_id')->all();
		$result = ScorerAccess::leftJoin('users','users.id','=','scorer_access.user_id')
								->where('club_id',auth()->guard('admin')->user()->id)
								->select('scorer_access.*','users.full_name as username','users.email as email')
								->paginate(Config::get("Reading.record_game_per_page")); 
		// dump($result); die; 
		return View::make('admin.common.scorer_access',compact('userList','playerIds','result', 'teams'));
	}

	public function adminScorerAccess(Request $request){ 
		$requestData = !empty($request->all()) ? $request->all() :'';
		$club = !empty($requestData['club']) ? $requestData['club']:'';
		$result = ScorerAccess::leftJoin('users','users.id','=','scorer_access.user_id')
								->where('club_id',auth()->guard('admin')->user()->id)
								->select('scorer_access.*','users.full_name as username','users.email as email')
								->paginate(Config::get("Reading.record_game_per_page")); 
		if(!empty($club)){ 
			$userList = UserTeams::leftJoin('users','users.id','=','user_teams.user_id')
							->where('club_id',$club)
							->where('user_teams.is_active',1)
							->pluck('users.full_name','user_teams.user_id')->all();
			
			if(!empty($userList)){
				unset($userList[Auth::guard('admin')->user()->id]);
			}
			$teams = Team::where('club', $club)->pluck('name', 'id'); 
			$result = ScorerAccess::leftJoin('users','users.id','=','scorer_access.user_id')
								->where('club_id',$club)
								->select('scorer_access.*','users.full_name as username','users.email as email')
								->paginate(Config::get("Reading.record_game_per_page"));
		}
		

		return View::make('admin.common.admin_scorer_access',compact('userList','playerIds','result', 'teams','requestData'));
	}
	public function getScorerUsersListings(){
		$clubId = Input::get('club_id');
		$userList = UserTeams::leftJoin('users','users.id','=','user_teams.user_id')
						->where('club_id',$clubId)
						->where('user_teams.is_active',1)
						->pluck('users.full_name','user_teams.user_id')->all();
		
		if(!empty($userList)){
			unset($userList[$clubId]);
		}	
		return View::make('admin.common.get_scorer_users',compact('userList','playerlist'));
	}
	public function getScorerTeamListings(){
		$clubId = Input::get('club_id');
		$teams = Team::where('club', $clubId)->pluck('name', 'id');
		return View::make('admin.common.get_scorer_team_listing',compact('teams'));
	}

	public function adminSaveScorerAccess(){ 
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();

		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'user_id'			=> 'required',
					'team_id'			=> 'required'
				),
				array(
					'user_id.required' => "Please Select User.",
					'team_id.required' => "Please Select Team."
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{  

				$obj =  new ScorerAccess();
				$obj->club_id = Input::get('club_id');
				$obj->user_id = Input::get('user_id');
				$obj->team_id = Input::get('team_id');
				$obj->save();
				User::where('id', $obj->user_id)->update(['is_approved' => 1,'user_role_id'=>SCORER]);
		    	Session::flash('success',trans("Scorer added successfully"));
				return Redirect::back();
			    }
		}
	}
	public function saveScorerAccess(){ 
		// dump(Input::all()); die; 

		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'user_id'			=> 'required',
					'team_id'			=> 'required'
				),
				array(
					'user_id.required' => "Please Select User.",
					'team_id.required' => "Please Select Team."
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{  
				$obj =  new ScorerAccess();
				$obj->club_id = auth()->guard('admin')->user()->id;
				$obj->user_id = Input::get('user_id');
				$obj->team_id = Input::get('team_id');
				$obj->save();
				User::where('id', $obj->user_id)->update(['is_approved' => 1,'user_role_id'=>SCORER]);
		    	Session::flash('success',trans("Scorer added successfully"));
				return Redirect::back();
			    }
		}
	}//end saveForum

	/**
	/**
	* Function for mark a Scorer as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deleteCommonController($Id=0){
		$userDetails =	ScorerAccess::findOrFail($Id); 
		
		if($Id){
			User::where('id', $userDetails->user_id)->update(['user_role_id'=>USER]);
			$userModel =	ScorerAccess::where('id',$Id)->delete();
		}
		return Redirect::back();
	}// end deleteCommonController

	/**
	* Function for verify Users listing by club 
	* 
	* @return redirect page.  
	*/ 
	public function verifyUsers(){  
		//$userIds = VerifyUser::pluck('user_id','user_id')->all();
		$playerIds = VerifyUser::where('club_id',Auth::guard('admin')->user()->id)->pluck('player_id','player_id')->all();
		$result = VerifyUser::where('club_id',Auth::guard('admin')->user()->id)->leftJoin('users','users.id','=','verify_users.user_id')
							->leftJoin('players','players.id','=','verify_users.player_id')
							->select('verify_users.*','users.full_name as username','players.full_name as player_name')
							->paginate(Config::get("Reading.record_game_per_page")); 
							
		$player	=	new Player();
		$playerlist	= $player->get_players_by_club(Auth::guard('admin')->user()->id);
		/*$user = new User();
		$userList = $user->get_user_list();*/
		$userList = UserTeams::leftJoin('users','users.id','=','user_teams.user_id')
								->where('club_id',Auth::guard('admin')->user()->id)
								->where('user_teams.is_active',1)
								->pluck('users.full_name','user_teams.user_id')->all();

		return View::make('admin.common.verify_users',compact('playerIds','result','userList','playerlist'));
	}

	public function saveVerifyUser(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'user_id'			=> 'required',
					'player_id'			=> 'required',
				),
				array(
					'user_id.required' => "Please Select User.",
					'player_id.required' => "Please Select Player.",
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{  
				$obj =  new VerifyUser();
				$obj->club_id = auth()->guard('admin')->user()->id;
				$obj->user_id = Input::get('user_id');
				$obj->player_id = Input::get('player_id');
				$obj->save();
		    	Session::flash('success',trans("User verified successfully"));
				return Redirect::back();
		    }
		}
	}
	public function getClubModeVerifyUserList(){ 
		$mode = Input::get('id');
		$club_id = Input::get('club_id');
		$searchVariable = Input::get('searchVariable');
		$club = new Club;
		$getClubMode = $club->get_club_mode($mode);
		return View::make('admin.common.get_club_verify_users',compact('getClubMode','searchVariable','club_id'));
	}
	public function adminVerifyUsers(Request $request){ 
		$requestData = !empty($request->all()) ? $request->all() :'';
		$result = VerifyUser::where('club_id',SUPER_ADMIN_ROLE_ID)->leftJoin('users','users.id','=','verify_users.user_id')
					->leftJoin('players','players.id','=','verify_users.player_id')
					->select('verify_users.*','users.full_name as username','players.full_name as player_name')
					->paginate(Config::get("Reading.record_game_per_page"));
		if(!empty($requestData)){
			$result = VerifyUser::where('club_id',$requestData['club'])->leftJoin('users','users.id','=','verify_users.user_id')
						->leftJoin('players','players.id','=','verify_users.player_id')
						->select('verify_users.*','users.full_name as username','players.full_name as player_name')
						->paginate(Config::get("Reading.record_game_per_page"));
		}
		 
		return View::make('admin.common.admin_verify_users',compact('result','requestData'));
	}
	public function getVerifyUsersListings(){
		$clubId = Input::get('club_id');
		$playerIds = VerifyUser::where('club_id',$clubId)->pluck('player_id','player_id')->all();
		$userList = UserTeams::leftJoin('users','users.id','=','user_teams.user_id')
					->where('club_id',$clubId)
					->where('user_teams.is_active',1)
					->pluck('users.full_name','user_teams.user_id')->all();
		$player	=	new Player();
		$playerlist	= $player->get_players_by_club($clubId);
		return View::make('admin.common.get_verify_user_listing',compact('userList','playerlist'));
	}
	public function getVerifyPlayersListings(){
		$clubId = Input::get('club_id');
		$userId = Input::get('user_id');
		$playerIds = VerifyUser::where('club_id',$clubId)->pluck('player_id','player_id')->all();
		$playerlist	= Player::where('club',$clubId)->orderBy('full_name','ASC')->pluck('full_name','id')->all();
		return View::make('admin.common.get_verify_players_listing',compact('playerlist','playerIds'));
	}
	public function adminSaveVerifyUser(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'user_id'			=> 'required',
					'player_id'			=> 'required',
				),
				array(
					'user_id.required' => "Please Select User.",
					'player_id.required' => "Please Select Player.",
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{  
				$obj =  new VerifyUser();
				$obj->club_id = Input::get('club_id');
				$obj->user_id = Input::get('user_id');
				$obj->player_id = Input::get('player_id');
				$obj->save();
		    	Session::flash('success',trans("User verified successfully"));
				return Redirect::back();
		    }
		}
	}
	public function deleteVerifyUser($Id=0){
		$userDetails =	VerifyUser::findOrFail($Id); 
		if($Id){
			$userModel =	VerifyUser::where('id',$Id)->delete();
		}
		return Redirect::back();
	}
	public function stripePayment(){
		$packid = Input::get('id');
		$price = PlayerPack::where('id',$packid)->value('price');
		$arrayValues = [];
		$arrayValues['club_id'] = Input::get('club');
		$arrayValues['player_pack_id'] = Input::get('id');
		$arrayValues['price'] = $price;
		Session::put('player_packs',$arrayValues);
		return View::make('admin.common.stripe_payment',compact('price'));
	}

	public function activateGame(){
		$clubId = Auth::guard('admin')->user()->id;
		$playerCount = Player::where('club',$clubId)->count();
		$gradeCount = Grade::where('club',$clubId)->count();
		$teamCount = Team::where('club',$clubId)->count();
		$fixtureCount = Fixture::where('club',$clubId)->count();
		$prizeCount = GamePrize::where('club',$clubId)->count();
		$playerPosition = Player::where('club',$clubId)->where('position',3)->count();

		$oneWK = Player::where('club',$clubId)->where('position',4)->count();
		$threeBAT = Player::where('club',$clubId)->where('position',1)->count();
		$twoAR = Player::where('club',$clubId)->where('position',3)->count();
		$threeBOW = Player::where('club',$clubId)->where('position',2)->count();

		$authUser = Auth::guard('admin')->user();
		/*pr($oneWK);
		pr($threeBAT);
		pr($twoAR);
		pr($threeBOW);
		*/
		//$oneWK<=1 && $threeBAT <=3 && $twoAR <=2 && $threeBOW <=3 
			//echo $playerCount;die;
		//Have at least 1 WK/3 BAT/2 AR/ 3 BWL as players; extraPlayerPrice
		$html = [];
		if($playerCount <= 11){
			$html[]="Create at least 12 player records<br>";
		}if($gradeCount <= 0){
			$html[]="Create at least one grade in game.<br>";
		}if($teamCount <= 0){
			$html[]="Create at least one team in game.<br>";
		}if($fixtureCount <= 0){
			$html[]="Create at least one fixture in game.<br>";
		}if($prizeCount <= 0){
			$html[]="Create at least one prize in game.<br>";
		}if($oneWK < 1){
			$html[]="Have at least 1 WK as players.<br>";
		}if($threeBAT < 3){
			$html[]="Have at least 3 BAT as players.<br>";
		}if($twoAR < 2){
			$html[]="Have at least 2 AR as players.<br>";
		}if($threeBOW < 3){
			$html[]="Have at least 3 BWL as players.<br>";
		}if(empty($authUser->lockout_start_day) && empty($authUser->lockout_end_day) && empty($authUser->lockout_start_time) && empty($authUser->lockout_end_time)){
			$html[]="Set the weekly Lockout Period for the game.<br>";
		}/*if($authUser->is_game_activate == 0){
			$html[]="Pay the fees to activate the game.<br>";
		}*/
		if(!empty($html)){
			$response	=	array(
		    	'success' 	=>	0,
		    	'data' 	=>	$html
		    ); 
		}else{
			$response	=	array(
		    	'success' 	=>	1,
		    	'data' 	=>	$html
		    ); 
		}
		
	    return Response::json($response);
	} 
	public function saveActivateGame(){ 
		$gameMode = Auth::guard('admin')->user()->game_mode;

		$extendedPlanCount = PlayerPackDetail::leftJoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
									->where('club',Auth::guard('admin')->user()->id)
									->select('player_packs.name as player_number')
									->get();

		$playerCount = Player::where('club',Auth::guard('admin')->user()->id)->count();						
		$extraPlayer = 0;
		$extraPlayerPrice = 0;

		if(!empty($playerCount) && $playerCount > 20){
			$sum=0;
			if(!$extendedPlanCount->isEmpty()){ 
				foreach ($extendedPlanCount as $key => $value) {
					$sum +=$value->player_number;
				}
			}else{
				$extraPlayer = $playerCount - MAXIMUMPLAYER;
			}
			//$extraPlayer = $playerCount - $sum;
			$extendedPlayerSum = MAXIMUMPLAYER + $sum;
			$extraPlayerPrice  =$extraPlayer * PLAYERFEE;
		}
				
		//echo $extraPlayer;die;
		$brandingPrice = 0;
		if($gameMode == 1){
			$price = Config::get("Site.senior_game_fee");
			$brandingPrice = SENIORBRANDING;
		}elseif ($gameMode == 2) {
			$price = Config::get("Site.junior_game_fee");
			$brandingPrice = JUNIORBRANDING;
		}elseif ($gameMode == 3) {
			$price = Config::get("Site.league_game_fee");
			$brandingPrice = LEAGUEBRANDING;
		}
		$discount = ((BIRDDISCOUNT / 100)*$price);
		//$offeredPrice = ($price-$discount) ;
		$offeredPrice = $price ;
		
		if(Auth::guard('admin')->user()->game_mode == 1){
			$totalPrice = $offeredPrice + $extraPlayerPrice;
		}else{
			$totalPrice = $offeredPrice;
		}
		$brandingActvation = Branding::where('club_id',Auth::guard('admin')->user()->id)->first();
		$brandingCount = Branding::where('club_id',Auth::guard('admin')->user()->id)->count();
		$is_branding_activated = 0;
		if(($brandingCount > 0) && ($brandingActvation->is_paid == 0)){ 
			$totalPrice = $totalPrice + $brandingPrice;
			$is_branding_activated = 1;
		} 
		return View::make('admin.common.activate_game_modal',compact('price','offeredPrice','extraPlayer','extraPlayerPrice','totalPrice','is_branding_activated'));
	}

	public function activeGameSubmit(Request $request){
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $price =  floor($request->price);
        $newPrice= $price *100;

        $getValues = Stripe\Charge::create ([
                "amount" => $newPrice,
                "currency" => "aud",
                "source" => $request->stripeToken,
                "description" => "Game Activation" 
        ]);
        $premiumMember = GameSetting::where('club_id',Auth::guard('admin')->user()->id)->count('id');
        $obj = new ClubActivation();
        $obj->club_id = Auth::guard('admin')->user()->id;
        $obj->transaction_id = $getValues->id;
        $obj->description = $getValues->description;
        $obj->amount = $price;
        $obj->is_premium = $premiumMember;
        $obj->paid_player = $request->extraPlayer;
        $obj->save();

        /*For branding activation*/
        if($request->is_branding_activated == 1){
        	$obj1 = Branding::find(Auth::guard('admin')->user()->id);
        	Branding::where('club_id',Auth::guard('admin')->user()->id)
        			->update(['transaction_id'=>$getValues->id,'payment_description'=>$getValues->description,'amount'=>$price,'is_paid'=>1]);
        }
		
        User::where('id',Auth::guard('admin')->user()->id)->update(['is_game_activate'=>1]);
        Session::flash('success', 'Game Activated successfully!');
        return back();
	}

	public function createStripeAccount($user_data) {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));   
        try{
            $account = \Stripe\Account::create([
            'country' => 'US',
            'type' => 'custom',
            'email' =>$user_data['email'],
            "business_profile" => [
                "mcc"=>"8099",
                "name"=>$user_data['name'],
                "product_description"=>"Club Commission",
                "support_address"=>null,
                "support_email"=>null,
                "support_phone"=>$user_data['mobile'],
                "support_url"=> null,
                "url" => WEBSITE_URL
            ], 
            "country"=> "US",
            "default_currency"=>  "usd",
            "email"=> $user_data['email'],  
            'tos_acceptance' => [
                'date' => time(),
                'ip' => $_SERVER['REMOTE_ADDR']
            ],
            'external_account' => [
                    "object"=>"bank_account", 
                    "country" => "US",
                    "currency" => "usd",
                    "account_holder_name" => $user_data['account_holder_name'],
                    "account_holder_type" => 'individual',
                    "account_number" => $user_data['account_number'],
                    "routing_number" => $user_data['routing_number'],  
                    "ssn_last_4" => $user_data['ssn'],  
                ]
            ]); 
            
            return ['result' => 1,'data' => $account];
        }catch (\Exception $e) {   
            return ['result' => 0,'msg' => $e->getMessage()]; 
        } 
    }

    public function gameSetting(){
    	$this_club = User::where('id', Auth::guard('admin')->user()->id)->first(); 
    	$gameSettingCount = GameSetting::where('club_id',Auth::guard('admin')->user()->id)->count();
    	return View::make('admin.common.game_setting', compact('this_club','gameSettingCount'));
    }

    public function saveOtherGameSetting(Request $request){

	  	$thisData     = Input::all();
	    parse_str($thisData['form_data'], $result); 
		$thisData = $result; 

		// print_r($thisData); die; 

	  if(!empty($thisData)){
	      $validator          = Validator::make(
	        $thisData,
	        array(
	          'entry_price'     => 'required|numeric|max:100',
	          'fundraising_target'=>'required',
	        ),
	        array(
	          'entry_price.required' => "Please enter fundraising amount.",
	          'entry_price.max' => "Fundraising amount should be less then 100.",
	          'fundraising_target.required' => "Please enter fundraising target.",
	        )
	      );
	    }

      if ($validator->fails()){
         return $validator->errors(); 
      }else{  
   		$entry_price =  $thisData['entry_price']; 
   		$fundraising_target =  $thisData['fundraising_target']; 
   		User::where("id", Auth::guard('admin')->user()->id)->update(['entry_price'=> $entry_price, 'fundraising_target' => $fundraising_target]);
        return 1; 
      }

    }

    public function saveGameSetting(Request $request){
    	if(!empty($request->code)){
	        $api_key = 'sk_live_x5qmfFqJZEhq1FnBmVaXybEA';
			$curl = curl_init();
			curl_setopt_array($curl, [
			  CURLOPT_URL => 'https://connect.stripe.com/oauth/token',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_HTTPHEADER => ["Authorization: Bearer $api_key"],
			  CURLOPT_POST => true,
			  CURLOPT_POSTFIELDS => http_build_query([
			    'client_id' => 'sk_live_x5qmfFqJZEhq1FnBmVaXybEA',
			    'code' => $request->code,
			    'grant_type' => 'authorization_code',
			  ])
			]); 
			$result = curl_exec($curl);
    		$obj = new GameSetting();
	    	$obj->club_id = Auth::guard('admin')->user()->id;
	        $obj->stripe_account_data = $result;
	        $obj->save();
	        Session::flash('success',trans("Stripe Account added successfully"));
			return Redirect::to('admin/game-setting');
    	}else{
    		Session::flash('success',trans("Something went wrong"));
			return Redirect::to('admin/game-setting');
    	}
    	/*$data = $request->all();
        $rules = [
            'account_holder_name' => 'required',
            'account_number' => 'required',
            'routing_number' => 'required',
            'ssn' => 'required',
            'share' => 'required|numeric'
        ];
        $validator = Validator::make($data,$rules);
		if($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$stripe_data = $this->createStripeAccount($request->all());
        if($stripe_data['result'] == 1) {
        	$obj = new GameSetting();
        	$obj->club_id = Auth::guard('admin')->user()->id;
            $obj->stripe_id = $stripe_data['data']['id'];
            $obj->share = $request->share;
            $obj->fee = $request->fee;
            $obj->stripe_account_data = json_encode($stripe_data['data']);
           
        } else {
        	return Redirect::back()->withErrors($stripe_data['msg'])->withInput();
        }
        $obj->save();
        Session::flash('success',trans("Stripe Account added successfully"));
		return Redirect::to('admin/game-setting');*/
    }
    public function getDaysName(){   
		$club_data = User::where('id',Auth::guard('admin')->user()->id)->first();
		$temp_day_arr = array(    'sunday' => 0,
		    'monday' => 1,
		    'tuesday' => 2,
		    'wednesday' => 3,
		    'thursday' => 4,
		    'friday' => 5,
		    'saturday' => 6);

		        

		$flg = false; 
		$i = 0; 
		        // dump($club_data); 
		if(!empty($club_data)){
	        foreach ($temp_day_arr as $key => $value) {
	            if(strtolower($club_data->lockout_start_day) == $key) $flg = true; 
	            if($flg){
	                $temp_day_arr[$key] = $i++; 
	            }
	        }
	        foreach ($temp_day_arr as $key => $value) {
	            if(strtolower($club_data->lockout_start_day) == $key) break; 
	            $temp_day_arr[$key] = $i++; 
	        }
		}

		$lockout_club = false; 


		if(!empty($club_data->lockout_start_day) && !empty($club_data->lockout_end_day) && !empty($club_data->lockout_start_time) && !empty($club_data->lockout_end_time)){
		        $club_data->lockout_start_time = explode(':', $club_data->lockout_start_time);
		        $club_data->lockout_end_time = explode(':', $club_data->lockout_end_time);
		        if($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] > $temp_day_arr[strtolower($club_data->lockout_start_day)] && 
		            $temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] < $temp_day_arr[strtolower($club_data->lockout_end_day)]  
		        ){
		            $lockout_club = true;    
		        }else if(
		($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] == $temp_day_arr[strtolower($club_data->lockout_start_day)]  ) && 
		(Carbon::now()->isoFormat('H') > $club_data->lockout_start_time[0] || (Carbon::now()->isoFormat('H') == $club_data->lockout_start_time[0] && Carbon::now()->isoFormat('m') > $club_data->lockout_start_time[1])) 
		        ){
		            // echo 'hello'; die; 
		            $lockout_club = true; 
		        }else if(
		($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] == $temp_day_arr[strtolower($club_data->lockout_end_day)] ) && 
		(Carbon::now()->isoFormat('H') < $club_data->lockout_end_time[0] || (Carbon::now()->isoFormat('H') == $club_data->lockout_end_time[0] && Carbon::now()->isoFormat('m') < $club_data->lockout_end_time[1]))
		        ){
		            // echo 'hihihihh'; die; 
		            $lockout_club = true; 
		        }

		    }

		 
		  if(!empty($club_data->lockout_start_date) && !(Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay() &&   Carbon::now()->startOfDay() <= Carbon::parse($club_data->lockout_end_date)->startOfDay()) && $lockout_club == true ){
		          $lockout_club = false; 
		  }

			if($lockout_club==false){
			    User::where('id', Auth::guard('admin')->user()->id)->update(['is_lockout'=>0]);
			    $obj = new LockoutLog();
				$obj->club_id = Auth::guard('admin')->user()->id;
				$obj->date_from = Auth::guard('admin')->user()->lockout_start_date;
				$obj->date_to = Auth::guard('admin')->user()->lockout_end_date;
				$obj->day_from = Auth::guard('admin')->user()->lockout_start_day;
				$obj->day_to = Auth::guard('admin')->user()->lockout_end_day;
				$obj->start_time = Auth::guard('admin')->user()->lockout_start_time;
				$obj->end_time = Auth::guard('admin')->user()->lockout_end_time;
				$obj->save();
			}else{
				User::where('id', Auth::guard('admin')->user()->id)->update(['is_lockout'=>1]);
			}
	}
    public function addLockout(Request $request){
    	$sesonStartDate = Auth::guard('admin')->user()->lockout_start_date;
    	$seasonEndDate = Auth::guard('admin')->user()->lockout_end_date;
    	if(empty($sesonStartDate) && empty($seasonEndDate) ){
    		Session::flash('error',trans("Please set lockout start and lockout end date."));
			return Redirect::to('admin/club/edit-club/'.Auth::guard('admin')->user()->id);
    	}
    	$thisData = $request->all();
    	if($request->isMethod('post')){
    		if(!empty($thisData)){
			      $validator          = Validator::make(
			        $thisData,
			        array(
			          'lockout_start_day'     => 'required',
			          'lockout_start_time'     => 'required',
			          'lockout_end_day'     => 'required',
			          'lockout_end_time'     => 'required',
			        )
			      );
			    }

		    if ($validator->fails()){
		         return Redirect::back()->withErrors($validator)->withInput();
		    }else{ 
		    	$obj = User::find(Auth::guard('admin')->user()->id);
	    		$obj->lockout_start_day = $request->lockout_start_day;
	    		$obj->lockout_end_day = $request->lockout_end_day;
	    		$obj->lockout_start_time = $request->lockout_start_time;
	    		$obj->lockout_end_time = $request->lockout_end_time;
	    		$obj->save();
	    		$this->getDaysName();
	    		if($obj->is_lockout == 1){
	    			Session::flash('success',trans("Game has been successfully locked out."));
	    		}else{
	    			Session::flash('success',trans("Game lockout has been successfully updated."));
	    		}
	    		return Redirect::to('admin/lockout');
		    }
    	}else{
    		$lockoutDetails =  User::where('id',Auth::guard('admin')->user()->id)->first();
    		return View::make('admin.common.lockout', compact('lockoutDetails'));
    	}
    }

    public function forceUnlock(Request $request){
    	$forceLockoutDate = date('Y-m-d H:i:s',time());
    	$userId = !empty($request->user_id) ? $request->user_id : Auth::guard('admin')->user()->id;
    	User::where('id',$userId)->update(['is_lockout'=>2,'force_logout_date'=>$forceLockoutDate]);
    	/*Force lockout email send here*/
    	$userList = UserTeams::where('club_id',$userId)
            ->where('user_teams.is_active',1)
            ->pluck('user_id')
            ->all();
		$temp = User::where('is_deleted',0)->whereIn('id',$userList)->get();
        foreach ($temp as $key => $value) {
            $club_data = $value; 
            if(!empty($club_data->lockout_start_date) ||  (!empty($club_data->is_lockout) && $club_data->is_lockout == 2) ){
                $now = Carbon::now();

                if(!empty($club_data->is_lockout) && $club_data->is_lockout == 2){
                    $emitted = Carbon::now()->subDay()->next($club_data->lockout_start_day)->startOfDay(); 

                }elseif(Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_date)->startOfDay()){
                    if(    Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_end_day) < Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)

                ){
                        $emitted = Carbon::parse($club_data->lockout_start_date)->startOfDay();
                }else{
                    $emitted = Carbon::parse($club_data->lockout_start_date)->next($club_data->lockout_start_day)->startOfDay();
                }
            }elseif(Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_day)->startOfDay()){
                $emitted = Carbon::now()->next($club_data->lockout_start_day)->startOfDay();
            }else{
                $emitted = Carbon::now()->startOfDay();
            }
            if(!empty($club_data->lockout_start_time)){
                $club_data->lockout_start_time = explode(':', $club_data->lockout_start_time); 
                // dump($club_data->lockout_start_time); 
                !empty($club_data->lockout_start_time[0]) ? $emitted->addHours($club_data->lockout_start_time[0]) : 0; 
                !empty($club_data->lockout_start_time[1]) ? $emitted->addMinutes($club_data->lockout_start_time[1]) : 0; 
            }
            $all_club_team = UserTeams::where('club_id', $club_data->id)->with('userdata')->get(); 
            foreach ($all_club_team as $key => $value) {
            if(LockoutMailSendToUserLog::where('team_id', $value->id)->where('club_id', $club_data->id)->where('mail_for', 2)->whereBetween('mail_send_date',  [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()] )->doesntExist()){
                if(!empty($value->userdata) &&  ($value->userdata->trading_lockout_notification == 1) && !empty($club_data->lockout_end_day) && !empty($club_data->lockout_end_time) ){
                        $tmp_emitted = $emitted->copy()->previous($club_data->lockout_end_day); 
                        $temp1_lockout_end_time = explode(':', $club_data->lockout_end_time); 
                        !empty($temp1_lockout_end_time[0]) ? $tmp_emitted->addHours($temp1_lockout_end_time[0]) : 0; 
                        !empty($temp1_lockout_end_time[1]) ? $tmp_emitted->addMinutes($temp1_lockout_end_time[1]) : 0; 
                        if(Carbon::now() > $tmp_emitted){
                            try {
                                $settingsEmail     = Config::get('Site.email');
                                $full_name        = $value->userdata->full_name; 
                                $game_name = $club_data->club_name ; 
                                $email     =  $value->userdata->email;

                                $emailActions    = EmailAction::where('action','=','send_lockout_end_time')->get()->toArray();
                                $emailTemplates    = EmailTemplate::where('action','=','send_lockout_end_time')->get(array('name','subject','action','body'))->toArray();
                            
                                $cons             = explode(',',$emailActions[0]['options']);
                                $constants         = array();
                                
                                foreach($cons as $key => $val){
                                    $constants[] = '{'.$val.'}';
                                }
                                
                                $subject         = $emailTemplates[0]['subject'];
                                $rep_Array         = array($full_name, $game_name); 
                                $messageBody    = str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
                                $mail            = $this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);
                                $lmstul = new LockoutMailSendToUserLog; 
                                $lmstul->user_id = $value->user_id; 
                                $lmstul->team_id = $value->id ; 
                                $lmstul->club_id =  $club_data->id ; 
                                $lmstul->mail_for = 2; 
                                $lmstul->mail_send_date = Carbon::now(); 
                                $lmstul->text = "With scores for the recent gameweek updated, the trading window of ".$game_name." is open now! Check out your team ranking, player stats and make trades in your fantasy team on myclubtap.com!"; 
                                $lmstul->save(); 

                            } catch (Exception $e) {
                                echo 'Caught exception: ',  $e->getMessage(), "\n";
                            }
                        }

                    }

            } 
        }

        }

        }
    	/*Force lockout email send finish here*/
    	$userLockoutData = User::where('id',$userId)->select('lockout_start_date','lockout_end_date','lockout_start_day','lockout_end_day','lockout_start_time','lockout_end_time')->first();

    	$obj = new LockoutLog();
    	$obj->club_id = $userId;
    	$obj->date_from =!empty($userLockoutData->lockout_start_date) ? $userLockoutData->lockout_start_date :"";
    	$obj->date_to = !empty($userLockoutData->lockout_end_date) ? $userLockoutData->lockout_end_date :"";
    	$obj->day_from = !empty($userLockoutData->lockout_start_day) ? $userLockoutData->lockout_start_day :"";
    	$obj->day_to = !empty($userLockoutData->lockout_end_day) ? $userLockoutData->lockout_end_day :"";
    	$obj->start_time = !empty($userLockoutData->lockout_start_time) ? $userLockoutData->lockout_start_time: "";
    	$obj->end_time =!empty($userLockoutData->lockout_end_time) ? $userLockoutData->lockout_end_time: "";
    	$obj->save();
    	return response()->json(1);
    }

    public function lockoutLog(){
    	// $result = LockoutLog::where('club_id',Auth::guard('admin')->user()->id)->paginate(Config::get("Reading.records_per_page"));

    		// echo Auth::guard('admin')->user()->id; 

			$temp_club_data = User::where('id', Auth::guard('admin')->user()->id)->first(); 
			
			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date)->startOfWeek(); 
			$temp_lockout_end_date = Carbon::parse($temp_club_data->lockout_end_date)->endOfWeek(); 
			// dump($temp_lockout_start_date->startOfWeek()); 
			// dump($temp_lockout_start_date->endOfWeek()); 
			// dump($temp_lockout_start_date->addWeek()); 
			// dump($temp_lockout_start_date->startOfWeek()); 
			// dump($temp_lockout_start_date->endOfWeek()); 
			// die; 
			// dump(Auth::user()->id); die; 

	// GW - GWP - GWR - Trades Made - Overall Pts - Overall Rank - Team Value - More


			$i = 1; 
			$result = array(); 
			$i = 1; 
			while (Carbon::parse($temp_lockout_start_date) < $temp_lockout_end_date) {

				// dump($temp_lockout_start_date); 
				$result[$i]['current'] = $temp_lockout_start_date >= Carbon::now()->startOfWeek() && $temp_lockout_start_date <= Carbon::now()->endOfWeek() ? 1 : 0; 
				$result[$i]['gw_start'] = $temp_lockout_start_date->copy()->startOfWeek();
				$result[$i]['gw_end'] =  $temp_lockout_start_date->copy()->endOfWeek();

				$i++; 
				$temp_lockout_start_date->addWeek(); 

			}
			// dump($result); 
			// die; 


/*
			// power data start 
$trades_count = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 1)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_trades_count'))->pluck('total_trades_count', 'user_id' )->toArray(); 			
$capton_card = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 2)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_capton_card'))->pluck('total_capton_card', 'user_id' )->toArray(); 
$twelve_man_card = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 3)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_twelve_man_card'))->pluck('total_twelve_man_card', 'user_id')->toArray(); 
$dealer_card = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 4)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_dealer_card'))->pluck('total_dealer_card', 'user_id')->toArray(); 
$flipper_card = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 5)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_flipper_card'))->pluck('total_flipper_card', 'user_id')->toArray(); 
$shield_steal = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 6)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_shield_steal'))->pluck('total_shield_steal', 'user_id')->toArray(); 

$all_users = UserTeams::where('club_id', Auth::guard('admin')->user()->id)->pluck('user_id', 'id')->toArray(); 

$all_team_id_arr = array_keys($all_users); 
// dump($all_users); 
// dump($all_team_id_arr); 
// die; 

$all_users = User::whereIn('id', $all_users)->get(); 
// dump($all_users); 




	// most picked captain start 


		$most_picked_capton = UserTeamsCVCTrack::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $all_team_id_arr)
		// ->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())
		->where('c_vc', 1)
		->groupBy('player_id')->orderBy('total', 'DESC')
		->pluck('total','player_id')->toArray();

		// dump($most_picked_capton); die; 

		// dump($most_picked_capton); die; 
		$most_picked_capton = array_key_first($most_picked_capton); 
		if(!empty($most_picked_capton))
			$most_picked_capton = Player::where('id', $most_picked_capton)->first(); 


	// most picked captain end 

	// most picked v.captain start 


		$most_picked_v_capton = UserTeamsCVCTrack::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $all_team_id_arr)
		// ->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())
		->where('c_vc', 0)
		->groupBy('player_id')->orderBy('total', 'DESC')
		->pluck('total','player_id')->toArray();

		// dump($most_picked_v_capton); die; 

		// dump($most_picked_v_capton); die; 
		$most_picked_v_capton = array_key_first($most_picked_v_capton); 
		if(!empty($most_picked_v_capton))
			$most_picked_v_capton = Player::where('id', $most_picked_v_capton)->first(); 


	// most picked v.captain end 

// dump($all_team_id_arr);

$most_trade_in = PlayerTradeInOut::whereIn('team_id', $all_team_id_arr)->whereNotNull('team_id')->where('in_out', 1)->groupBy('player_id')->select('player_id', DB::raw('count(*) as most_trade_in'))->orderby('most_trade_in', 'DESC')->pluck('most_trade_in', 'player_id')->toArray(); 
$most_trade_out = PlayerTradeInOut::whereIn('team_id', $all_team_id_arr)->whereNotNull('team_id')->where('in_out', 0)->groupBy('player_id')->select('player_id', DB::raw('count(*) as most_trade_out'))->orderby('most_trade_out', 'DESC')->pluck('most_trade_out', 'player_id')->toArray(); 

$most_trade_in = array_key_first($most_trade_in); 
		if(!empty($most_trade_in))
			$most_trade_in = Player::where('id', $most_trade_in)->first(); 

$most_trade_out = array_key_first($most_trade_out); 
		if(!empty($most_trade_out))
			$most_trade_out = Player::where('id', $most_trade_out)->first(); 

*/ 

// dump($most_trade_in); dump($most_trade_out); die; 


// dump($capton_card); dump($twelve_man_card); dump($dealer_card); dump($flipper_card); dump($shield_steal); die; 

			// trade(team player change) - 1, capton card - 2, 12th player card - 3, Dealer card - 4, Flipper card - 5, Shield/Stea

			// power data end 






			// gameweek player point start 

/*



	$club_id = Auth::guard('admin')->user()->id; 



		if(!empty($club_id)){
			$club_data = User::where('id', $club_id)->first(); 
			if(!empty($club_data->lockout_start_date)){
				$cur_gameweek_no = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now());
				// $cur_gameweek_no++;
				$cur_gameweek_no--; 
			}else{
				$cur_gameweek_no = 0; 
			}
		}else{
			$cur_gameweek_no = 0; 
		}

		$most_last_gw = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now()); 

		 // dump(Input::get('gw_no')); 
		 // dump(Input::get('month_no')); 

		 $gw_no = !empty(Input::get('gw_no')) && is_numeric(Input::get('gw_no')) &&  Input::get('gw_no') >= 0 ? Input::get('gw_no') : $cur_gameweek_no; 


		 // dump($gw_no); 
		 // dump($month_no);

		 // dump($cur_gameweek_no); 
		 // dump($gw_no); 


	// end ***************************************** end 

	$top_player_by_gwk_point = array(); 
	$all_players = Player::where('club', $club_id)->pluck('id')->toArray(); 

	// dump($user_teams); die; 
// $user_teams = null; 
		if(!empty($club_id) && !empty($all_players)){



			$temp_club_data = User::where('id', $club_id)->first(); 
			
			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 
			// dump($temp_lockout_start_date); 
			$temp_lockout_start_date = $temp_lockout_start_date->addWeek($gw_no); 

			// dump($temp_lockout_start_date); 

			// dump(Carbon::parse($temp_lockout_start_date_month)->endOfMonth()); 

			$temp_gw_data = array(); 
			$temp_gw_data_month = array();

			foreach ($all_players as $key => $value) {


		$tmp_pnt = FixtureScorcard::where('player_id', $value)

				->whereHas('fixture', function($q) use ($temp_lockout_start_date){
							$q->where('start_date', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('start_date', '<=', $temp_lockout_start_date->copy()->endOfWeek()); 
						})
			// ->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())

			->sum('fantasy_points'); // ->where('status', 3)


				// gameweek calculation 

			$temp_gw_data[$value]['player_point'] = !empty($tmp_pnt) ? $tmp_pnt : 0 ; 

				// monthly calculation 
			}


				// dump($temp_gw_data_month); 

				// dump($temp_gw_data);  die; 
				arsort($temp_gw_data);
	

				// dump($temp_gw_data_month);  

				foreach ($temp_gw_data as $key => $value) {
					$temp_gw_data[$key]['player_data'] = Player::find($key); 
				}

		}


*/ 

	// gameweek player point end 

    	return View::make('admin.common.lockout_logs', compact('result', 'all_users', 'trades_count', 'capton_card', 'twelve_man_card', 'dealer_card', 'flipper_card', 'shield_steal', 'most_picked_capton', 'most_picked_v_capton', 'most_trade_in', 'most_trade_out', 'gw_no', 'temp_gw_data', 'most_last_gw'));					
    }
    public function showActivateGame(){

    	$premiumMember = GameSetting::where('club_id',Auth::guard('admin')->user()->id)->count('id');

		$gameMode = Auth::guard('admin')->user()->game_mode;

		$extendedPlanCount = PlayerPackDetail::leftJoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
									->where('club',Auth::guard('admin')->user()->id)
									->select('player_packs.name as player_number')
									->get();

		$playerCount = Player::where('club',Auth::guard('admin')->user()->id)->count();	

		$extraPlayer = 0;
		$price = 0;
		$extraPlayerPrice = 0;
		if(!empty($playerCount) && $playerCount > 20){
			$sum=0;
			if(!$extendedPlanCount->isEmpty()){ 
				foreach ($extendedPlanCount as $key => $value) {
					$sum +=$value->player_number;
				}
			}else{
				$extraPlayer = $playerCount - MAXIMUMPLAYER;
			}
			$extendedPlayerSum = MAXIMUMPLAYER + $sum;
			$extraPlayerPrice  =$extraPlayer * PLAYERFEE;
		}
		
		$price = 0;
		$brandingPrice = 0;
		if($gameMode == 1){
			$price = Config::get("Site.senior_game_fee"); 
			$brandingPrice = SENIORBRANDING;
		}elseif ($gameMode == 2) {
			$price = Config::get("Site.junior_game_fee");
			$brandingPrice = JUNIORBRANDING;
		}elseif ($gameMode == 3) {
			$price = Config::get("Site.league_game_fee");
			$brandingPrice = LEAGUEBRANDING;
		}
		$discount = ((BIRDDISCOUNT / 100)*$price);
		$offeredPrice = ($price-$discount) ;
		if(Auth::guard('admin')->user()->game_mode == 1){
			//$totalPrice = $offeredPrice + $extraPlayer;
			$totalPrice = $offeredPrice + $extraPlayerPrice;
		}else{
			$totalPrice = $offeredPrice;
		} 
		$brandingActvation = Branding::where('club_id',Auth::guard('admin')->user()->id)->first();
		$brandingCount = Branding::where('club_id',Auth::guard('admin')->user()->id)->count();
		$is_branding_activated = 0;
		if($brandingCount > 0){  
			$totalPrice = $totalPrice + $brandingPrice;
			$is_branding_activated = 1;
		}
    	return View::make('admin.common.show_activate_game', compact('premiumMember','price','offeredPrice','extraPlayer','extraPlayerPrice','totalPrice','is_branding_activated','brandingPrice'));					
    }
    public function bookIntroSession(){
    	return View::make('admin.common.book_intro_session');					
    }
    public function helpSupport(){
    	return View::make('admin.common.help_support');					
    }
    public function fundraiserMessage(Request $request){
    	$id = $request->messageId;
    	$result =[];
    	$result = FundraiserMessage::where('club_id',Auth::guard('admin')->user()->id)->first();
    	if(\Request::isMethod('post')){
    		$thisData = $request->all();
			$validator 					=	Validator::make(
				$request->all(),
				array(
					'message'			=> 'required',
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else{
				if(empty($result->id)){
					$obj = new FundraiserMessage();
					$obj->message = $request->message;
					$obj->club_id = Auth::guard('admin')->user()->id;
					$obj->save();
					Session::flash('success',trans("Fundraiser message has been added successfully."));
					return Redirect::to('admin/fundraiser-message');
				}else{ 
					$obj = FundraiserMessage::find($result->id);
	    			$obj->message = $request->message;
		        	$obj->save();
		        	Session::flash('success',trans("Fundraiser message has been upated successfully."));
					return Redirect::to('admin/fundraiser-message');
				}				
			}
    	}else{
    		return View::make('admin.common.fundraiser_message',compact('result'));			
    	}
    }

    public function fundraiserHistory(){
		$DB				=	Fundraiser::query();
		$searchVariable	=	array(); 
		$inputGet		=	Input::get();
		if (Input::get()) { 
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		
		$sortBy = (Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  = (Input::get('order')) ? Input::get('order')   : 'DESC';
		$result = $DB->leftJoin('users','users.id','fundraisers.user_id')
					->select('fundraisers.*','users.full_name as club_name')
					->where('club_id',Auth::guard('admin')->user()->id)
					->orderBy($sortBy, $order)
					->paginate(Config::get("Reading.record_game_per_page")); 
		
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.common.fundraiser_history',compact('result','searchVariable','sortBy','order','type','query_string'));
    }

    public function adminbranding(){ 
    	$DB 					= 	Branding::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) { 
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					if($fieldName=="is_active"){
						$DB->where("game_branding.is_active",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="club"){
						$DB->where("club_id",'like','%'.$fieldValue.'%'); 
					}
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			} 
		} 
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'ASC';
	    $result = 	$DB
					->orderBy($sortBy, $order)
					->paginate(Config::get("Reading.record_game_per_page"));		

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.common.admin_brandings', compact('result','searchVariable','sortBy','order','query_string','cludDetails'));
    }
    public function adminEditBranding($id = null){ 
    	$userDetails = Branding::where(['id'=>$id])->first();
		return View::make('admin.common.edit_admin_branding', compact('userDetails'));
    }

    public function adminUpdateBranding(Request $request){  
    	$resuestArray = $request->all();
		$obj = Branding::findOrFail($resuestArray['brand_id']);
    	$obj->name =  $request->name;
		$obj->url =  $request->url; 
		$obj->is_paid =  Input::get('is_paid');
		if(Input::hasFile('logo')){
			$image 					=	Branding::where('id',$resuestArray['brand_id'])->value('logo');
			@unlink(BRANDING_IMAGE_ROOT_PATH.$image);
			$extension 			=	Input::file('logo')->getClientOriginalExtension();
			$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
			$folderPath			=	BRANDING_IMAGE_ROOT_PATH.$newFolder; 
			if(!File::exists($folderPath)){
				File::makeDirectory($folderPath, $mode = 0777,true);
			}
			$userImageName = time().'-branding.'.$extension;
			$image = $newFolder.$userImageName;
			if(Input::file('logo')->move($folderPath, $userImageName)){
				$obj->logo		=	$image;
			}
		}
		$obj->save();
		Session::flash('success',trans("Branding has been upated successfully"));
		return Redirect::to('admin/admin-branding');
	}//end update Branding

	public function adminAddBranding(){
		return View::make('admin.common.add_admin_branding', compact('userDetails'));
	}
	public function adminSaveBranding(Request $request){
		$resuestArray = $request->all();
		$brandId = Branding::where('club_id',$request->club)->value('id');
		if(empty($brandId)){
			$obj = new Branding();
		}else{
			$obj =  Branding::find($brandId);
		}
		$obj->name =  Input::get('name');
		$obj->url =  Input::get('url');
		$obj->club_id =  $request->club;
		if(Input::hasFile('logo')){
			$extension 			=	Input::file('logo')->getClientOriginalExtension();
			$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
			$folderPath			=	BRANDING_IMAGE_ROOT_PATH.$newFolder; 
			if(!File::exists($folderPath)){
				File::makeDirectory($folderPath, $mode = 0777,true);
			}
			$userImageName = time().'-branding.'.$extension;
			$image = $newFolder.$userImageName;
			if(Input::file('logo')->move($folderPath, $userImageName)){
				$obj->logo		=	$image;
			}
		}
    	$obj->amount = 0;
    	$obj->is_paid = 1;
		$obj->save();
		Session::flash('success',trans("Branding has been upated successfully"));
		return Redirect::to('admin/admin-branding');
	}

	public function adminLockout(){
		$DB 					= 	User::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					if($fieldName == "club"){
						$DB->where("id",'like','%'.$fieldValue.'%'); 
					}if($fieldName == "game_mode"){
						$DB->where("game_mode",'like','%'.$fieldValue.'%'); 
					}
					//$DB->where("$fieldName",'like','%'.$fieldValue.'%'); 
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';

		$result 				= 	$DB
									->where('id','<>',ADMIN_ID)
									->where('lockout_start_day','<>','')
									->orderBy($sortBy, $order)
									->paginate(Config::get("Reading.records_per_page"));

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.common.admin_lockout', compact('result' ,'searchVariable','sortBy','order','query_string'));
	}

	public function adminEditLockout($id=0){
		$lockoutDetails = User::where('id',$id)->first();
		return  View::make('admin.common.admin_edit_lockout', compact('lockoutDetails'));
	}
	public function adminUpdateLockout(Request $request){
    	$thisData = $request->all();
		if(!empty($thisData)){
		      $validator          = Validator::make(
		        $thisData,
		        array(
		          'lockout_start_day'     => 'required',
		          'lockout_start_time'     => 'required',
		          'lockout_end_day'     => 'required',
		          'lockout_end_time'     => 'required',
		        )
		      );
		    }

	    if ($validator->fails()){
	         return Redirect::back()->withErrors($validator)->withInput();
	    }else{ 
	    	$obj = User::find($request->user_id);
    		$obj->lockout_start_day = $request->lockout_start_day;
    		$obj->lockout_end_day = $request->lockout_end_day;
    		$obj->lockout_start_time = $request->lockout_start_time;
    		$obj->lockout_end_time = $request->lockout_end_time;
    		$obj->save();
    		$this->getDaysName();
    		if($obj->is_lockout == 1){
    			Session::flash('success',trans("Game has been successfully locked out."));
    		}else{
    			Session::flash('success',trans("Game lockout has been successfully updated."));
    		}
    		return Redirect::to('admin/admin-lockout');
	    }
	}

	public function adminLockoutLogs(Request $request){
		$searchVariable = $request->all();
		if(!empty($request->club)){
			$temp_club_data = User::where('id', $request->club)->first(); 
		}else{
			$temp_club_data = User::where('id', Auth::guard('admin')->user()->id)->first(); 
		}
		
		$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date)->startOfWeek(); 
		$temp_lockout_end_date = Carbon::parse($temp_club_data->lockout_end_date)->endOfWeek(); 
		$result = array(); 
		$i = 1; 
		if($temp_club_data->id !=1){
			while (Carbon::parse($temp_lockout_start_date) < $temp_lockout_end_date) {
				$result[$i]['current'] = $temp_lockout_start_date >= Carbon::now()->startOfWeek() && $temp_lockout_start_date <= Carbon::now()->endOfWeek() ? 1 : 0; 
				$result[$i]['gw_start'] = $temp_lockout_start_date->copy()->startOfWeek();
				$result[$i]['gw_end'] =  $temp_lockout_start_date->copy()->endOfWeek();
				$i++; 
				$temp_lockout_start_date->addWeek(); 
			}
		}		
    	return View::make('admin.common.admin_lockout_logs', compact('result','searchVariable'));	
	}

	public function adminGameFee(){
		$DB							=	User::query();
		$searchVariable				=	array(); 
		$inputGet					=	Input::get();
		if((Input::get() || isset($inputGet['display'])) || isset($inputGet['page']) ){
			$searchData				=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);
			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue)){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
					$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
				}
			}
		}
		$sortBy 					= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'users.updated_at';
	    $order  					= 	(Input::get('order')) ? Input::get('order')   : 'desc';
		$result 					= 	$DB->leftJoin('game_branding', 'users.id', '=', 'game_branding.club_id')
										->leftJoin('club_activations','users.id','=','club_activations.club_id')
										->select('is_game_activate as is_game_activate','entry_price as amount','game_mode as game_mode','fundraising_target as target','game_name','users.id','is_active','club_activations.amount as total_amount','club_activations.is_premium as is_premium','club_activations.paid_player as paid_player','game_branding.amount as branding_price','game_branding.created_at as branding_activated_on','game_branding.is_paid as is_paid')
										//->selectRaw('sum(fundraisers.amount) AS raised')
										->where('user_role_id',CLUBUSER)
										->where('is_deleted',0)
										->where('game_name','<>','')
										->groupBy('users.id')
										->orderBy($sortBy,$order)
										->paginate(Config::get("Reading.records_per_page"));
										// prd($result);
		$complete_string			=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string				=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.common.admin_game_fee',compact('result','searchVariable','sortBy','order','query_string'));
	}

	public function adminActivateGame($userId = 0, $userStatus = 0){
		if($userStatus == 0	){
			$statusMessage	=	trans("Game deactivated successfully");
			User::where('id', '=', $userId)->update(array('is_game_activate' =>0));
		}else{
			$statusMessage	=	trans("Game sctivated successfully");
			User::where('id', '=', $userId)->update(array('is_game_activate' => 1));
		}
		Session::flash('flash_notice', $statusMessage); 
		return Redirect::back();
	}

	public function adminPlayerFee(Request $request,$user_id = null){ 

		if($request->isMethod('post')){
			$thisData = Input::all();
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'club'			=> 'required',
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else{
				$obj = new PlayerPackDetail();
				$obj->player_pack_id = $request->plan;
				$obj->club = $request->club;
				$obj->save();
				Session::flash('success',trans("Player pack successfully activated."));
				return Redirect::to('admin/admin-pay-activate');
			}
		}else{
			$userDetails  =User::where('id',$user_id)->first();
			$playerPacks =  PlayerPack::get();
			$playerData = [];
			if(!$playerPacks->isEmpty()){ 
				foreach ($playerPacks as $key => $value) {
					$playerData[$value->id] = 'Add '.$value->name." players for $".$value->price;
				}
			}
			return  View::make('admin.common.admin_player_fee', compact('playerData','userDetails'));
		}
	}

	public function adminFundraiserHistory(){
		$DB				=	Fundraiser::query();
		$searchVariable	=	array(); 
		$inputGet		=	Input::get();
		if (Input::get()) { 
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		
		$sortBy = (Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  = (Input::get('order')) ? Input::get('order')   : 'DESC';
		$result = $DB->leftJoin('users','users.id','fundraisers.user_id')
					->select('fundraisers.*','users.full_name as club_name')
					->orderBy($sortBy, $order)
					->paginate(Config::get("Reading.record_game_per_page")); 
		
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.common.admin_fundraiser_history',compact('result','searchVariable','sortBy','order','type','query_string'));
    }
}// end ClubController class
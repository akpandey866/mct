<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\ClubSlider;
use App\Model\User;
use App\Model\Club;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use Carbon\Carbon;

class ClubSliderController extends BaseController {
/**
* Function for display all event
*
* @param null
*
* @return view page.
*/
	public function index(){
		$DB 					= 	ClubSlider::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			} 
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';
		$result 				= 	$DB
									->orderBy($sortBy, $order)
									->paginate(Config::get("Reading.records_per_page"));

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		$userList = User::where('is_active',1)->where('is_deleted',0)->where('user_role_id',2)->pluck('full_name','full_name')->all();
		return  View::make('admin.club_slider.index', compact('result','searchVariable','sortBy','order','query_string','userList'));
	 }

	/**
	* Function for add Forum
	*
	* @param null
	*
	* @return view page.  
	*/
	public function addClubSlider(){
		$club					=	new Club();
		$cludDetails			=	$club->get_club_list();
		$userList = User::where('is_active',1)->where('is_deleted',0)->where('user_role_id',2)->pluck('full_name','full_name')->all();
		return  View::make('admin.user_slider.add',compact('userList','cludDetails'));
	}//end addPlayer()

	public function saveClubSlider(){ 
	Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'name'=> 'required',
					'image' => 'required|max:5048|mimes:'.IMAGE_EXTENSION,
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
				$obj =  new ClubSlider;
				$obj->name =  Input::get('name');
				$obj->club_name =  Input::get('club_name');
				$obj->description =  Input::get('description');
				if(input::hasFile('image')){
					$extension 			=	Input::file('image')->getClientOriginalExtension();
					$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
					$folderPath			=	USER_SLIDER_IMAGE_ROOT_PATH.$newFolder; 
					if(!File::exists($folderPath)){
						File::makeDirectory($folderPath, $mode = 0777,true);
					}
					$userImageName = time().'-user-slider.'.$extension;
					$image = $newFolder.$userImageName;
					if(Input::file('image')->move($folderPath, $userImageName)){
						$obj->image		=	$image;
					}
				}
				$obj->save();
			    Session::flash('success',trans("User Slider has been added successfully"));
				return Redirect::to('admin/user-slider');
			    }
		}
	}//end saveSponsor
	/**
	* Function for edit Sposnsor
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editClubSlider($id = 0){  
		$details			=	ClubSlider::findOrFail($id); 
		$club					=	new Club();
		$cludDetails			=	$club->get_club_list();
		$userList = User::where('is_active',1)->where('is_deleted',0)->where('user_role_id',2)->pluck('full_name','full_name')->all();
		return View::make('admin.user_slider.edit', compact('details','userList','cludDetails'));
	} // 

	/**
* Function for update Forum
*
* @param null
*
* @return view page. 
*/
	public function updateClubSlider(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData	=	Input::all();
		$id = Input::get('id');
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'name' => 'required',
					'image' => 'max:2048|mimes:'.IMAGE_EXTENSION,
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
					$obj = ClubSlider::find($id);
					$obj->name =  Input::get('name');
					$obj->description =  Input::get('description');
					$obj->club_name =  Input::get('club_name');
					if(Input::hasFile('image')){
						$image 					=	ClubSlider::where('id',$id)->value('image');
						@unlink(USER_SLIDER_IMAGE_ROOT_PATH.$image);
						$extension 			=	Input::file('image')->getClientOriginalExtension();
						$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
						$folderPath			=	USER_SLIDER_IMAGE_ROOT_PATH.$newFolder; 
						if(!File::exists($folderPath)){
							File::makeDirectory($folderPath, $mode = 0777,true);
						}
						$userImageName = time().'-user-slider.'.$extension;
						$image = $newFolder.$userImageName;
						if(Input::file('image')->move($folderPath, $userImageName)){
							$obj->image		=	$image;
						}
					}
					$obj->save();
			    Session::flash('success',trans("User Slider has been upated successfully"));
				return Redirect::to('admin/user-slider');
			}
		}
	}//end update Forum-manager



	/**
	 * Function for change is_active of Forum
	 *
	 * @param $Id as id of Forum
	 * @param $ClubSlider is_active as is_active of Forum
	 *
	 * @return redirect page. 
	*/	
	public function updateStatus($Id = 0, $Status = 0){
		ClubSlider::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/user-slider');
	} // end updateStatus()
	
	
	/**
	/**
	* Function for mark a Forum as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deleteClubSlider($Id=0){
		$userDetails	=	ClubSlider::findOrFail($Id); 
		$userModel		=	ClubSlider::where('id',$Id)->delete();
		Session::flash('flash_notice',trans("User Slider has been removed successfully")); 
		return Redirect::to('admin/user-slider');
	}// end deleteForum

	
}// end ClubController class
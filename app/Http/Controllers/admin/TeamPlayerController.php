<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Team;
use App\Model\Fixture;
use App\Model\User;
use App\Model\Club;
use App\Model\Player;
use App\Model\TeamPlayer;
use App\Model\FixtureScorcard;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use Carbon\Carbon;

class TeamPlayerController extends BaseController {
/**
* Function for display all event
*
* @param null
*
* @return view page.
*/
	public function index($fixture_id=null){ 
		$clubId = Fixture::where('id',$fixture_id)->value('club');
		$DB 					= 	TeamPlayer::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			} 
			$date_from	=	'';
			$date_to	=	'';
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'team_players.created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';

	    if(Auth::guard('admin')->user()->user_role_id == 1){
			$result = 	$DB->leftJoin('players' , 'team_players.player_id' , '=' , 'players.id')
							->select('team_players.*','players.full_name as player_name')
							->where('fixture_id',$fixture_id)
							->orderBy('id', 'ASC')
							->paginate(15);
		}else{
			$result = 	$DB
						->leftJoin('players' , 'team_players.player_id' , '=' , 'players.id')
						->select('team_players.*','players.full_name as player_name')
						->where('fixture_id',$fixture_id)
						->orderBy('id', 'ASC')
						->paginate(15);
		}

		$is_lock = TeamPlayer::where('fixture_id',$fixture_id)->orderBy('id','DESC')->value('is_lock');
		$playerCount = TeamPlayer::where('fixture_id',$fixture_id)->count(); 
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		$club =	new Club();
		$cludDetails =	$club->get_club_list();
 
		$player	=	new Player();
		$playerlist	=	$player->get_players_by_club($clubId);
		//pr(array_values($playerlist));
		//prd(array_keys($playerlist));
		$playerIds = TeamPlayer::where('fixture_id',$fixture_id)->pluck('player_id','player_id')->all();
  
		$fixtureDetails = Fixture::where('fixtures.id',$fixture_id)
								->leftJoin('teams' , 'fixtures.team' , '=' , 'teams.id')
								->leftJoin('grades as grades' , 'fixtures.grade' , '=' , 'grades.id')
								->leftJoin('dropdown_managers as dropdown_managers2' , 'fixtures.match_type' , '=' , 'dropdown_managers2.id')
								->select('grades.grade','teams.name  as team_name','fixtures.start_date','fixtures.start_date','fixtures.end_date','dropdown_managers2.name  as match_type')
								->first();
		return  View::make('admin.team_players.index', compact('result' ,'searchVariable','sortBy','order','userType','query_string','gradeName','teamType','teamCategory','cludDetails','fixture_id','playerlist','playerIds','is_lock','playerCount','fixtureDetails'));
	 }	
	public function saveTeamPlayer(){ 
		$team_id  = Input::get('team_id');
		$fixture_id  = Input::get('fixture_id');
		$club_id = Team::where('id',$team_id)->value('club');
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'player_id'			=> 'required',
				),
				array(
					'player_id.required' => "Please Select Player.",
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{  
				if(!empty(Input::get('player_hidden_ids'))){
					//prd(Input::get('player_hidden_ids'));
					$playersIds = explode(',', Input::get('player_hidden_ids'));
					$player	=	new Player();
					$clubId = Fixture::where('id',$fixture_id)->value('club');
					$playerlist	=	array_keys($player->get_players_by_club($clubId));
					$player_id_arr = Input::get('player_id'); 
					foreach ($playersIds as $key => $value) {
						$obj =  new TeamPlayer();
						$obj->player_id =  $playerlist[$value];
						$obj->fixture_id =  $fixture_id;
						//$obj->team_id =  $team_id;
						$obj->save();
					}

		    		Session::flash('success',trans("Player added successfully"));					
				}else{

		    		Session::flash('error',trans("Failed!"));
				}

				return Redirect::back();
			    }
		}
	}//end saveForum

	/**
	/**
	* Function for mark a Forum as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deleteTeamPlayer($player_id,$fixture_id){
		$userDetails	=	TeamPlayer::findOrFail($player_id); 
		if($userDetails){
			$userModel		=	TeamPlayer::where('id',$player_id)->delete();
			FixtureScorcard::where(['player_id'=>$userDetails->player_id,'fixture_id'=>$fixture_id])->delete();
		}
		return Redirect::back();
	}// end deleteTeamPlayer
	
	public function lockPlayer($fixture_id=null){
		TeamPlayer::where('fixture_id', $fixture_id) ->update(['is_lock' => 1]);
		return Redirect::back();
	}

}// end ClubController class
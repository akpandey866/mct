<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Platform;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use Carbon\Carbon;

class PlatformController extends BaseController {
/**
* Function for display all event
*
* @param null
*
* @return view page.
*/
	public function index(){
		$DB 					= 	Platform::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			} 
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';
		$result 				= 	$DB
									->orderBy($sortBy, $order)
									->paginate(Config::get("Reading.records_per_page"));

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.platform.index', compact('result','searchVariable','sortBy','order','query_string'));
	 }

	/**
	* Function for add Forum
	*
	* @param null
	*
	* @return view page.  
	*/
	public function addPlatform(){
		return  View::make('admin.platform.add');
	}//end addPlayer()

	public function savePlatform(){ 
	Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'title'=> 'required',
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
				$obj =  new Platform;
				$obj->title =  Input::get('title');
				$obj->description =  !empty(Input::get('description')) ? Input::get('description'):'';
				if(input::hasFile('logo')){
					$extension 			=	Input::file('logo')->getClientOriginalExtension();
					$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
					$folderPath			=	PLATFORM_IMAGE_ROOT_PATH.$newFolder; 
					if(!File::exists($folderPath)){
						File::makeDirectory($folderPath, $mode = 0777,true);
					}
					$userImageName = time().'-platform.'.$extension;
					$image = $newFolder.$userImageName;
					if(Input::file('logo')->move($folderPath, $userImageName)){
						$obj->logo		=	$image;
					}
				}
				$obj->save();
			    Session::flash('success',trans("Platofrm has been added successfully"));
				return Redirect::to('admin/platform');
			    }
		}
	}//end saveSponsor
	/**
	* Function for edit Sposnsor
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editPlatform($id = 0){  
		$details			=	Platform::findOrFail($id); 
		return View::make('admin.platform.edit', compact('details'));
	} // 

	/**
* Function for update Forum
*
* @param null
*
* @return view page. 
*/
	public function updatePlatform(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData	=	Input::all();
		$id = Input::get('id');
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'title' => 'required',
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
					$obj = Platform::find($id);
					$obj->title =  Input::get('title');
					$obj->description =  !empty(Input::get('description')) ? Input::get('description'):'';
					if(Input::hasFile('logo')){
						$image 					=	Platform::where('id',$id)->value('logo');
						@unlink(PLATFORM_IMAGE_ROOT_PATH.$image);
						$extension 			=	Input::file('logo')->getClientOriginalExtension();
						$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
						$folderPath			=	PLATFORM_IMAGE_ROOT_PATH.$newFolder; 
						if(!File::exists($folderPath)){
							File::makeDirectory($folderPath, $mode = 0777,true);
						}
						$userImageName = time().'-sponsor.'.$extension;
						$image = $newFolder.$userImageName;
						if(Input::file('logo')->move($folderPath, $userImageName)){
							$obj->logo		=	$image;
						}
					}
					$obj->save();
			    Session::flash('success',trans("Platform has been upated successfully"));
				return Redirect::to('admin/platform');
			}
		}
	}//end update Forum-manager



	/**
	 * Function for change is_active of Forum
	 *
	 * @param $Id as id of Forum
	 * @param $platform is_active as is_active of Forum
	 *
	 * @return redirect page. 
	*/	
	public function updateStatus($Id = 0, $Status = 0){
		Platform::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/platform');
	} // end updateStatus()
	
	
	/**
	/**
	* Function for mark a Forum as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deletePlatform($Id=0){
		$userDetails	=	Platform::findOrFail($Id); 
		$userModel		=	Platform::where('id',$Id)->delete();
		Session::flash('flash_notice',trans("Platform has been removed successfully")); 
		return Redirect::to('admin/platform');
	}// end deleteForum

	
}// end ClubController class
<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\GamePrize;
use App\Model\User;
use App\Model\Club;
use App\Model\GamePrizeMessage;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GamePrizesController extends BaseController {
/**
* Function for display all event
*
* @param null
*
* @return view page.
*/
	public function index(){ 
		$clubPrize = GamePrize::where('club',Auth::guard('admin')->user()->id)->count();
		if($clubPrize <= 0){ 
			$prizesDetails = GamePrize::where('club',ADMIN_ID)->get();
			foreach ($prizesDetails as $key => $value) {
				$obj =  new GamePrize;
				$obj->category_id =  $value->category_id;
				$obj->title 	=  $value->title;
				$obj->description =  $value->description;
				$obj->is_active =  0;
				$obj->club =  Auth::guard('admin')->user()->id;
				$obj->image		=	$value->image;
				$obj->save();
			}
		}

		$DB 					= 	GamePrize::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			
			
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					if($fieldName=="is_active"){
						$DB->where("users.is_active",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="category_id" && !empty($fieldValue)){
						$DB->where("category_id",'=',$fieldValue); 
					}if($fieldName=="game_mode"){
						$DB->where("game_mode",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="game_name"){
						$DB->where("game_name",'like','%'.$fieldValue.'%'); 
					}
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
		$order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';

		if(Auth::guard('admin')->user()->user_role_id == 1){
			$result = 	$DB->select('game_prizes.*','users.game_mode as game_mode','users.game_name as game_name')
							->orderBy($sortBy, $order)
							->leftJoin('users','game_prizes.club','=','users.id')
							->paginate(Config::get("Reading.records_per_page"));
		}else{
			$result = 	$DB->where('club',Auth::guard('admin')->user()->id)
						->orderBy($sortBy, $order)
						->paginate(Config::get("Reading.records_per_page"));
		}

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		$club					=	new Club();
		$clubLists			=	$club->get_club_list();
		return  View::make('admin.game_prizes.index', compact('result' ,'searchVariable','sortBy','order','query_string','clubLists'));
	}
/**
* Function for add Forum
*
* @param null
*
* @return view page. 
*/
public function addGamePrize(){ 
	$club					=	new Club();
	$clubList			=	$club->get_club_list();
	return  View::make('admin.game_prizes.add',compact('gradeName','teamType','teamCategory','clubList'));
}//end editGamePrize()
	
		/**
* Function for update Forum
*
* @param null
*
* @return view page. 
*/
	public function saveGamePrize(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'category_id'			=> 'required',
					'title'	=> 'required',
					'description' => 'required',
					'image' => 'required',
					'club' => 'required',
				),
				array(
					'category_id.required' => "Please select category.",
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
				 
			}else{ 
				$obj =  new GamePrize;
				$obj->category_id =  Input::get('category_id');
				$obj->title 	=  !empty(Input::get('title')) ? Input::get('title'):0;
				$obj->description =  !empty(Input::get('description')) ? Input::get('description'):0;
				$obj->club =  !empty(Input::get('club')) ? Input::get('club'):0;
				if(input::hasFile('image')){
					$extension 			=	Input::file('image')->getClientOriginalExtension();
					$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
					$folderPath			=	PRIZE_IMAGE_ROOT_PATH.$newFolder; 
					if(!File::exists($folderPath)){
						File::makeDirectory($folderPath, $mode = 0777,true);
					}
					$userImageName = time().'-game-prize.'.$extension;
					$image = $newFolder.$userImageName;
					if(Input::file('image')->move($folderPath, $userImageName)){
						$obj->image		=	$image;
					}
				}
				$obj->save();
			    Session::flash('success',trans("Prize has been added successfully"));
				return Redirect::to('admin/game-prizes');
		    }
		}
	}

	/**
	* Function for edit gameprize
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editGamePrize($userId = 0){  
		$userDetails			=	GamePrize::findOrFail($userId); 
		$club		=	new Club();
		$clubList =	$club->get_club_list();
		return View::make('admin.game_prizes.edit', compact('userDetails','clubList','userId'));

	} // 

/**
* Function for update gameprize
*
* @param null
*
* @return view page. 
*/
	public function updateGamePrize(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData	=	Input::all();
		$id = Input::get('game_prize_id');
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'image' => 'max:2048|mimes:'.IMAGE_EXTENSION,
					'title' => 'required',
					'description' => 'required',
					'category_id' => 'required',
				),
				array(
					'category_id.required' => "Please select category.",
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
				$obj = GamePrize::find($id);
				$obj->title =  Input::get('title');
				$obj->description =  Input::get('description');
				$obj->category_id =  Input::get('category_id');
				$obj->club =  !empty(Input::get('club')) ? Input::get('club'):'';
				if(Input::hasFile('image')){
					$image 					=	GamePrize::where('id',$id)->value('image');
					@unlink(PRIZE_IMAGE_ROOT_PATH.$image);
					$extension 			=	Input::file('image')->getClientOriginalExtension();
					$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
					$folderPath			=	PRIZE_IMAGE_ROOT_PATH.$newFolder; 
					if(!File::exists($folderPath)){
						File::makeDirectory($folderPath, $mode = 0777,true);
					}
					$userImageName = time().'-PRIZE.'.$extension;
					$image = $newFolder.$userImageName;
					if(Input::file('image')->move($folderPath, $userImageName)){
						$obj->image		=	$image;
					}
				}
				$obj->save();
			    Session::flash('success',trans("Prize has been upated successfully"));
				return Redirect::to('admin/game-prizes');
			}
		}
	}//end update Forum-manager

	/**
	 * Function for change is_active of Forum
	 *
	 * @param $Id as id of Forum
	 * @param $sponsor is_active as is_active of Forum
	 *
	 * @return redirect page. 
	*/	
	public function updateStatus($Id = 0, $Status = 0){
		GamePrize::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/game-prizes');
	} // end updateStatus()

	/**
	/**
	* Function for mark a Forum as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deleteGamePrize($Id=0){
		$userDetails	=	GamePrize::findOrFail($Id); 
		if(File::exists(PRIZE_IMAGE_ROOT_PATH.$userDetails->logo)){
	        @unlink(PRIZE_IMAGE_ROOT_PATH.$userDetails->logo);
	    }
		if(empty($userDetails)) {
			return Redirect::to('game-prizes');
		}
		Session::flash('flash_notice', trans("Prize has been deleted successfully.")); 
		$userModel		=	GamePrize::where('id',$Id)->delete();
		return Redirect::to('admin/game-prizes');
	}// end deleteForum
 
    public function gamePrizeMessage(Request $request,$admin_club_id=null){ 
    	if(\Request::isMethod('post')){
    		$thisData = $request->all();
			$validator 					=	Validator::make(
				$request->all(),
				array(
					'message'			=> 'required',
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else{
				if(!empty($admin_club_id)){
					GamePrizeMessage::updateOrCreate(['club_id'=>$admin_club_id],['message' => $request->message]);
					Session::flash('success',trans("Game prize message has been upated successfully."));
					return Redirect::to('admin/game-prizes/game-prizes-message/'.$admin_club_id);	
				}else{
					GamePrizeMessage::updateOrCreate(['club_id'=>Auth::guard('admin')->user()->id],['message' => $request->message]);
					Session::flash('success',trans("Game prize message has been upated successfully."));
					return Redirect::to('admin/game-prizes/game-prizes-message');	
				}
						
		 	}
    	}else{
    		if(!empty($admin_club_id)){
    			$result = GamePrizeMessage::where('club_id',$admin_club_id)->first();
    			return View::make('admin.game_prizes.game_prize_message',compact('result','admin_club_id'));				
    		}else{
    			$result = GamePrizeMessage::where('club_id',Auth::guard('admin')->user()->id)->first();
    			return View::make('admin.game_prizes.game_prize_message',compact('result','admin_club_id'));				
    		}
    	}
    }

    public function adminGamePrizeListing(){ 
    	$DB 					= 	GamePrize::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		// prd($inputGet);die;
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			
			
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					if($fieldName=="is_active"){
						$DB->where("users.is_active",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="game_mode"){
						$DB->where("game_mode",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="game_name"){
						$DB->where("game_name",'like','%'.$fieldValue.'%'); 
					}
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
			/*$DB->where(function($q) {
				$inputGet				=	Input::get();
			          $q->orWhere('users.is_active', $inputGet['is_active'])
			            ->where('game_name',$inputGet['game_name'])
			            ->where('game_mode',$inputGet['game_mode']);
			      });*/
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'game_prizes.created_at';
		$order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';

		$result = 	$DB
					->select('game_prizes.*','users.game_mode as game_mode','users.game_name as game_name')
					->orderBy($sortBy, $order)
					->leftJoin('users','game_prizes.club','=','users.id')
					->paginate(Config::get("Reading.records_per_page"));

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		$club					=	new Club();
		$clubLists			=	$club->get_club_list();
		return  View::make('admin.game_prizes.admin_index', compact('result' ,'searchVariable','sortBy','order','query_string','clubLists'));
    }

    public function adminGetGameName(){
		$mode = Input::get('mode');
		$game_name = Input::get('game_name');
		$getModesGames = User::where('game_mode',$mode)->where('user_role_id',CLUBUSER)->where('game_name','!=','')->pluck('game_name','game_name')->all();
		return  View::make('admin.game_prizes.admin_get_game_name',compact('getModesGames','game_name'));
	}



}// end ClubController class
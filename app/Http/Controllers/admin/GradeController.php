<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Branding;
use App\Model\Grade;
use App\Model\User; 
use App\Model\Club;
use App\Model\Player;
use App\Model\PlayerPackDetail;
use App\Model\Team;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;

class GradeController extends BaseController {


	public function listGrade(){

		$DB 					= 	Grade::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					//$DB->where("grades.$fieldName",'like','%'.$fieldValue.'%');
					if($fieldName=="is_active"){
						$DB->where("grades.is_active",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="game_mode"){
						$DB->where("game_mode",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="game_name"){
						$DB->where("game_name",'like','%'.$fieldValue.'%'); 
					}
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			} 
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';
 
	    if(Auth::guard('admin')->user()->user_role_id == 1){ 
			$result = 	$DB->select('grades.*','users.game_mode as game_mode','users.game_name as game_name')
						->leftJoin('users','grades.club','=','users.id')
						->paginate(Config::get("Reading.records_per_page"));
			$club = new Club;
			$clubList = $club->get_club_list();
		}else{  
			$result= $DB->where('club',Auth::guard('admin')->user()->id)->paginate(Config::get("Reading.record_game_per_page"));
			$player = new Player;
			$playerList = $player->get_players_by_club(Auth::guard('admin')->user()->id);
		}
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.grade.list_grade', compact('result','searchVariable','sortBy','order','query_string','clubList','playerList'));


	}

	public function addGrade(){



	if (Request::isMethod('post')){  
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			if(Auth::guard('admin')->user()->user_role_id == 1){
				$validator 					=	Validator::make(
					Input::all(),
					array(
						'grade'=> 'required',
						'club'=> 'required',
						'mode'	=> 'required',
					)
				);
			}else{
				$validator 					=	Validator::make(
					Input::all(),
					array(
						'grade'=> 'required',
					)
				);
			}


			if ($validator->fails()){
				$errors 	=	$validator->messages();
				$response	=	array(
					'success' 	=> false,
					'errors' 	=> $errors
				);
				return Response::json($response); 
				die;				 
			}else{ 

				$club = !empty(Input::get('club')) ? Input::get('club') : Auth::guard('admin')->user()->id;
				// print_r($club); die; 
				$mode = User::where('id',$club)->value('game_mode');
				// print_r($mode); die; 
				$obj =  new Grade;
				$obj->grade	=  !empty(Input::get('grade')) ? Input::get('grade'):'';
				$obj->club	=  $club;
				$obj->mode	=  $mode;
				$obj->save();
				$response	=	array(
			    	'success' 	=>	'1',
			    	'message' 	=>	trans("Grade has been added successfully.")
			    ); 
			    return Response::json($response);
			}
		}

	} 




		return  View::make('admin.grade.add_grade', []);

	}

	public function editGrade($id = null){


	if (Request::isMethod('post')){  
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			if(Auth::guard('admin')->user()->user_role_id == 1){
				$validator 					=	Validator::make(
					Input::all(),
					array(
						'grade'=> 'required',
						'club'=> 'required',
						'mode'	=> 'required',
					)
				);
			}else{
				$validator 					=	Validator::make(
					Input::all(),
					array(
						'grade'=> 'required',
					)
				);
			}


			if ($validator->fails()){
				$errors 	=	$validator->messages();
				$response	=	array(
					'success' 	=> false,
					'errors' 	=> $errors
				);
				return Response::json($response); 
				die;				 
			}else{ 

				$club = !empty(Input::get('club')) ? Input::get('club') : Auth::guard('admin')->user()->id;
				$mode = User::where('id',$club)->value('game_mode');
				$obj =  Grade::findOrFail($id);
				$obj->grade	=  !empty(Input::get('grade')) ? Input::get('grade'):'';
				$obj->club	=  $club;
				$obj->mode	=  $mode;
				$obj->save();
				$response	=	array(
			    	'success' 	=>	'1',
			    	'message' 	=>	trans("Grade has been added successfully.")
			    ); 
			    return Response::json($response);
			}
		}

	} 


		$availabilityDetails			=	Grade::findOrFail($id); 

		// dump($availabilityDetails); die; 
		$club = new Club;
		$clubList = $club->get_club_list();

		return  View::make('admin.grade.edit_grade', compact('availabilityDetails','clubList','id'));


	}

	public function getClubModeList(){ 
		$mode = Input::get('id');
		$club_id = Input::get('club_id');
		$club = new Club;
		$getClubMode = $club->get_club_mode($mode);
		return View::make('admin.grade.get_club_mode_list',compact('getClubMode','club_id'));
	}




	public function updateStatus($Id = 0, $Status = 0){
		Grade::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/list-grade');
	} // end updateStatus()
		

	public function deleteGrade($Id=0){
		$userDetails	=	Grade::findOrFail($Id); 
		$userModel		=	Grade::where('id',$Id)->delete();
		return Redirect::to('admin/list-grade');
	}// end deleteForum

}// end ClubController class
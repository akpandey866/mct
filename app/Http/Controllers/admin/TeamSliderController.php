<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\TeamSlider;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use Carbon\Carbon;

class TeamSliderController extends BaseController {
/**
* Function for display all event
*
* @param null
*
* @return view page.
*/
	public function index(){
		$DB 					= 	TeamSlider::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			} 
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';
		$result 				= 	$DB
									->orderBy($sortBy, $order)
									->paginate(Config::get("Reading.records_per_page"));

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.team_slider.index', compact('result','searchVariable','sortBy','order','query_string'));
	 }

	/**
	* Function for add Forum
	*
	* @param null
	*
	* @return view page.  
	*/
	public function addTeamSlider(){
		return  View::make('admin.team_slider.add');
	}//end addPlayer()

	public function saveTeamSlider(){ 
	Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'name'=> 'required',
					'image' => 'required|max:5048|mimes:'.IMAGE_EXTENSION,
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
				$obj =  new TeamSlider;
				$obj->name =  Input::get('name');
				$obj->position =  Input::get('position');
				$obj->description =  Input::get('description');
				$obj->facebook =  Input::get('facebook');
				$obj->twitter =  Input::get('twitter');
				$obj->instagram =  Input::get('instagram');
				$obj->linkedin =  Input::get('linkedin');
				if(input::hasFile('image')){
					$extension 			=	Input::file('image')->getClientOriginalExtension();
					$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
					$folderPath			=	TEAM_SLIDER_IMAGE_ROOT_PATH.$newFolder; 
					if(!File::exists($folderPath)){
						File::makeDirectory($folderPath, $mode = 0777,true);
					}
					$userImageName = time().'-team-slider.'.$extension;
					$image = $newFolder.$userImageName;
					if(Input::file('image')->move($folderPath, $userImageName)){
						$obj->image		=	$image;
					}
				}
				$obj->save();
			    Session::flash('success',trans("Team slider has been added successfully"));
				return Redirect::to('admin/team-slider');
			    }
		}
	}//end saveTeamSlider
	/**
	* Function for edit Sposnsor
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editTeamSlider($id = 0){  
		$details			=	TeamSlider::findOrFail($id); 
		return View::make('admin.team_slider.edit', compact('details'));
	} // 

	/**
* Function for update Forum
*
* @param null
*
* @return view page. 
*/
	public function updateTeamSlider(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData	=	Input::all();
		$id = Input::get('id');
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'image' => 'max:2048|mimes:'.IMAGE_EXTENSION,
					'name' => 'required',
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
					$obj = TeamSlider::find($id);
					$obj->name =  Input::get('name');
					$obj->position =  Input::get('position');
					$obj->description =  Input::get('description');
					$obj->facebook =  Input::get('facebook');
					$obj->twitter =  Input::get('twitter');
					$obj->instagram =  Input::get('instagram');
					$obj->linkedin =  Input::get('linkedin');
					if(Input::hasFile('image')){
						$image 					=	TeamSlider::where('id',$id)->value('image');
						@unlink(TEAM_SLIDER_IMAGE_ROOT_PATH.$image);
						$extension 			=	Input::file('image')->getClientOriginalExtension();
						$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
						$folderPath			=	TEAM_SLIDER_IMAGE_ROOT_PATH.$newFolder; 
						if(!File::exists($folderPath)){
							File::makeDirectory($folderPath, $mode = 0777,true);
						}
						$userImageName = time().'-team-slider.'.$extension;
						$image = $newFolder.$userImageName;
						if(Input::file('image')->move($folderPath, $userImageName)){
							$obj->image		=	$image;
						}
					}
					$obj->save();
			    Session::flash('success',trans("Team Slider has been upated successfully"));
				return Redirect::to('admin/team-slider');
			}
		}
	}//end update Forum-manager



	/**
	 * Function for change is_active of Forum
	 *
	 * @param $Id as id of Forum
	 * @param $team-slider is_active as is_active of Forum
	 *
	 * @return redirect page. 
	*/	
	public function updateStatus($Id = 0, $Status = 0){
		TeamSlider::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/team-slider');
	} // end updateStatus()
	
	
	/**
	/**
	* Function for mark a Forum as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deleteTeamSlider($Id=0){
		$userDetails	=	TeamSlider::findOrFail($Id); 
		if(File::exists(TEAM_SLIDER_IMAGE_ROOT_PATH.$userDetails->image)){
	        @unlink(TEAM_SLIDER_IMAGE_ROOT_PATH.$userDetails->image);
	    }
		$userModel		=	TeamSlider::where('id',$Id)->delete();
		return Redirect::to('admin/team-slider');
	}// end deleteForum

	
}// end ClubController class
<?php

/**

 * Forum Controller

 */

namespace App\Http\Controllers\admin;

use App\Http\Controllers\BaseController;

use App\Model\Fixture;

use App\Model\User;

use App\Model\Club;

use App\Model\Grade; 

use App\Model\Team;

use App\Model\UserTeams; 

use App\Model\DropDown;

use App\Model\FixtureScorcard;

use App\Model\TeamPlayer;

use App\Model\GamePoint;

use App\Model\Player;

use App\Model\PlayerPoint;

use App\Model\ScorerAccess;

use App\Model\LockoutLog;

use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Model\UserTeamPlayers; 
use App\Model\UserTeamValueLog; 
use App\Model\UserTeamsPlayerTrack; 
use App\Model\PlayerSvalueLog; 
use App\Model\UserTeamLogs; 
use App\Model\UserTeamsGWExtraPTTrack; 

use App\Model\UserTeamsCVCTrack; 
use App\Model\UserTeamsMonthPTTrack; 


class FixtureController extends BaseController {

/**

* Function for display all event

*

* @param null

*

* @return view page.

*/
	public function getDaysName(){   
		$club_data = User::where('id',Auth::guard('admin')->user()->id)->first();
		$temp_day_arr = array(    'sunday' => 0,
		    'monday' => 1,
		    'tuesday' => 2,
		    'wednesday' => 3,
		    'thursday' => 4,
		    'friday' => 5,
		    'saturday' => 6);

		        

		$flg = false; 
		$i = 0; 
		        // dump($club_data); 
		if(!empty($club_data)){
	        foreach ($temp_day_arr as $key => $value) {
	            if(strtolower($club_data->lockout_start_day) == $key) $flg = true; 
	            if($flg){
	                $temp_day_arr[$key] = $i++; 
	            }
	        }
	        foreach ($temp_day_arr as $key => $value) {
	            if(strtolower($club_data->lockout_start_day) == $key) break; 
	            $temp_day_arr[$key] = $i++; 
	        }
		}

		$lockout_club = false; 


		if(!empty($club_data->lockout_start_day) && !empty($club_data->lockout_end_day) && !empty($club_data->lockout_start_time) && !empty($club_data->lockout_end_time)){
		        $club_data->lockout_start_time = explode(':', $club_data->lockout_start_time);
		        $club_data->lockout_end_time = explode(':', $club_data->lockout_end_time);
		        if($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] > $temp_day_arr[strtolower($club_data->lockout_start_day)] && 
		            $temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] < $temp_day_arr[strtolower($club_data->lockout_end_day)]  
		        ){
		            $lockout_club = true;    
		        }else if(
		($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] == $temp_day_arr[strtolower($club_data->lockout_start_day)]  ) && 
		(Carbon::now()->isoFormat('H') > $club_data->lockout_start_time[0] || (Carbon::now()->isoFormat('H') == $club_data->lockout_start_time[0] && Carbon::now()->isoFormat('m') > $club_data->lockout_start_time[1])) 
		        ){
		            // echo 'hello'; die; 
		            $lockout_club = true; 
		        }else if(
		($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] == $temp_day_arr[strtolower($club_data->lockout_end_day)] ) && 
		(Carbon::now()->isoFormat('H') < $club_data->lockout_end_time[0] || (Carbon::now()->isoFormat('H') == $club_data->lockout_end_time[0] && Carbon::now()->isoFormat('m') < $club_data->lockout_end_time[1]))
		        ){
		            // echo 'hihihihh'; die; 
		            $lockout_club = true; 
		        }

		    }

		 
		  if(!empty($club_data->lockout_start_date) && !(Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay() &&   Carbon::now()->startOfDay() <= Carbon::parse($club_data->lockout_end_date)->startOfDay()) && $lockout_club == true ){
		        $lockout_club =false; 
		  }
		  if((Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_date)->startOfDay())  && $lockout_club == true ){
			$lockout_club = false; 
		   }

			if($lockout_club==false){
			    User::where('id', Auth::guard('admin')->user()->id)->update(['is_lockout'=>0]);
			    $obj = new LockoutLog();
				$obj->club_id = Auth::guard('admin')->user()->id;
				$obj->date_from = Auth::guard('admin')->user()->lockout_start_date;
				$obj->date_to = Auth::guard('admin')->user()->lockout_end_date;
				$obj->day_from = Auth::guard('admin')->user()->lockout_start_day;
				$obj->day_to = Auth::guard('admin')->user()->lockout_end_day;
				$obj->start_time = Auth::guard('admin')->user()->lockout_start_time;
				$obj->end_time = Auth::guard('admin')->user()->lockout_end_time;
				$obj->save();
			}else{
				if(Auth::guard('admin')->user()->is_lockout != 2){
					User::where('id', Auth::guard('admin')->user()->id)->update(['is_lockout'=>1]);
				}
			}
			/*$fixtureOneDayList = Fixture::where('end_date','<',Auth::guard('admin')->user()->lockout_start_date)
								->where('is_completed',0)->where('match_type',ONEDAYMATCH)->get();

			if(!$fixtureOneDayList->isEmpty()){
				foreach ($fixtureOneDayList as $key => $value) {
					FixtureScorcard::where('fixture_id',$value->id)->update(['status'=>3]);
				}
				Fixture::where('end_date','<',Auth::guard('admin')->user()->lockout_start_date)
				->where('is_completed',0)->where('match_type',ONEDAYMATCH)->update(['is_completed'=>1]);
			}*/
	}

	public function index(){   

		$userType = null; 

		$this->getDaysName();
		$DB 					= 	Fixture::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		$drop_down				=	new DropDown();
		$matchTypeList			=	$drop_down->get_master_list("matchtype");
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {

			$searchData			=	Input::get();

			unset($searchData['display']);

			unset($searchData['_token']);

			if(isset($searchData['order'])){

				unset($searchData['order']);

			}

			if(isset($searchData['sortBy'])){

				unset($searchData['sortBy']);

			}

			if(isset($searchData['page'])){

				unset($searchData['page']);

			}

			$date_from	=	'';

			$date_to	=	'';

			

			// dump($searchData); die; 

			foreach($searchData as $fieldName => $fieldValue){
				if($fieldName == 'keyword'){
					// $DB->leftJoin('teams AS team1', 'team1.name' ,'like','%'.$fieldValue.'%'); 
					$DB->leftJoin('teams AS teams1' , 'fixtures.team' , '=' , 'teams1.id')->where('teams1.name', 'like', '%'.$fieldValue.'%'); 
				}else if(!empty($fieldValue) || $fieldValue==0){
					if($fieldName=="game_mode"){
						$DB->where("clubs.game_mode",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="game_name"){
						$DB->where("clubs.game_name",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="team"){
						$DB->where("team",'=',$fieldValue); 
					}
					//$DB->where("fixtures.$fieldName",'like','%'.$fieldValue.'%');

				}

				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));

			} 

		}

		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'fixtures.created_at';

	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';



	    if(Auth::guard('admin')->user()->user_role_id == 1){ 
 
			$result	= 	$DB->leftJoin('users' , 'fixtures.opposition_club' , '=' , 'users.id')

						->leftJoin('user_teams as user_teams' , 'fixtures.team' , '=' , 'user_teams.id')

						->leftJoin('grades as grades' , 'fixtures.grade' , '=' , 'grades.id')

						->leftJoin('users as clubs' , 'fixtures.club' , '=' , 'clubs.id')

						->leftJoin('dropdown_managers' , 'fixtures.team_category' , '=' , 'dropdown_managers.id')

						->leftJoin('dropdown_managers as dropdown_managers1' , 'fixtures.team_type' , '=' , 'dropdown_managers1.id')

						->leftJoin('dropdown_managers as dropdown_managers2' , 'fixtures.match_type' , '=' , 'dropdown_managers2.id')

						->leftJoin('dropdown_managers as dropdown_managers3' , 'fixtures.grade' , '=' , 'dropdown_managers3.id')

						->leftJoin('teams' , 'fixtures.team' , '=' , 'teams.id')

						->select('fixtures.*','dropdown_managers.name  as team_category','dropdown_managers1.name  as team_type','dropdown_managers2.name  as match_type','dropdown_managers1.name  as grade_name','teams.name  as team_name','clubs.club_name  as club_name', 'grades.grade', 'user_teams.my_team_name','clubs.game_mode as game_mode','clubs.game_name as game_name') 
						->orderBy($sortBy, $order)
						->paginate(Config::get("Reading.records_per_page"));
			$team					=	new Team();
			$teamList			=	$team->get_team();
			$gradeList = Grade::pluck('grade','id')->all(); 


		}else{

			if(Auth::guard('admin')->user()->user_role_id==SCORER){
				$scorerDetails = ScorerAccess::where('user_id',Auth::guard('admin')->user()->id)
										->select('club_id','team_id')
										->first();

				$gradeList = Grade::where('club',$scorerDetails->club_id)->pluck('grade','id')->all(); 
				$teamList =	Team::where('club',$scorerDetails->club_id)->pluck('name','id')->all();
				$result = 	$DB->leftJoin('users' , 'fixtures.opposition_club' , '=' , 'users.id')

						->leftJoin('user_teams as user_teams' , 'fixtures.team' , '=' , 'user_teams.id')

						->leftJoin('grades as grades' , 'fixtures.grade' , '=' , 'grades.id')

						->leftJoin('users as clubs' , 'fixtures.club' , '=' , 'clubs.id')

						->leftJoin('dropdown_managers' , 'fixtures.team_category' , '=' , 'dropdown_managers.id')

						->leftJoin('dropdown_managers as dropdown_managers1' , 'fixtures.team_type' , '=' , 'dropdown_managers1.id')

						->leftJoin('dropdown_managers as dropdown_managers2' , 'fixtures.match_type' , '=' , 'dropdown_managers2.id')

						->leftJoin('dropdown_managers as dropdown_managers3' , 'fixtures.grade' , '=' , 'dropdown_managers3.id')
						->leftJoin('teams' , 'fixtures.team' , '=' , 'teams.id')
						->select('fixtures.*','clubs.club_name','dropdown_managers.name  as team_category','dropdown_managers1.name  as team_type','dropdown_managers2.name  as match_type','dropdown_managers1.name  as grade_name','teams.name  as team_name', 'grades.grade', 'user_teams.my_team_name')

						->where('fixtures.club',$scorerDetails->club_id)
						->where('fixtures.team',$scorerDetails->team_id)
						->orderBy($sortBy, $order)
						->paginate(Config::get("Reading.record_game_per_page"));

			}else{
				$gradeList = Grade::where('club',Auth::guard('admin')->user()->id)->pluck('grade','id')->all(); 
				$teamList =	Team::where('club',Auth::guard('admin')->user()->id)->pluck('name','id')->all();
				$result = 	$DB->leftJoin('users' , 'fixtures.opposition_club' , '=' , 'users.id')

					->leftJoin('user_teams as user_teams' , 'fixtures.team' , '=' , 'user_teams.id')

					->leftJoin('users as clubs' , 'fixtures.club' , '=' , 'clubs.id')

					->leftJoin('grades as grades' , 'fixtures.grade' , '=' , 'grades.id')

					->leftJoin('dropdown_managers' , 'fixtures.team_category' , '=' , 'dropdown_managers.id')

					->leftJoin('dropdown_managers as dropdown_managers1' , 'fixtures.team_type' , '=' , 'dropdown_managers1.id')

					->leftJoin('dropdown_managers as dropdown_managers2' , 'fixtures.match_type' , '=' , 'dropdown_managers2.id')

					->leftJoin('dropdown_managers as dropdown_managers3' , 'fixtures.grade' , '=' , 'dropdown_managers3.id')

					->leftJoin('teams' , 'fixtures.team' , '=' , 'teams.id')

					->select('fixtures.*','clubs.club_name','dropdown_managers.name  as team_category','dropdown_managers1.name  as team_type','dropdown_managers2.name  as match_type','dropdown_managers1.name  as grade_name','teams.name  as team_name', 'grades.grade', 'user_teams.my_team_name')

					->where('fixtures.club',Auth::guard('admin')->user()->id)

					->orderBy($sortBy, $order)

					->paginate(Config::get("Reading.record_game_per_page"));

			}

		}



		// print_r($result); die;  teamList

 

		$complete_string		=	Input::query();

		unset($complete_string["sortBy"]);

		unset($complete_string["order"]);

		$query_string			=	http_build_query($complete_string);

		$result->appends(Input::all())->render();

		$drop_down				=	new DropDown();

		$gradeName				=	$drop_down->get_master_list("gradename");

		$teamType				=	$drop_down->get_master_list("teamtype");

		$teamCategory			=	$drop_down->get_master_list("teamcategory");

		$club					=	new Club();

		$cludDetails			=	$club->get_club_list();

		

		
		return  View::make('admin.fixture.index', compact('result','searchVariable','sortBy','order','userType','query_string','gradeName','teamType','teamCategory','cludDetails','teamList', 'gradeList', 'matchTypeList'));

	 }



	/**

	* Function for add Forum

	*

	* @param null

	*

	* @return view page. 

	*/

	public function addFixture(){

		$drop_down				=	new DropDown();

		$club					=	new Club();

		$gradeName				=	$drop_down->get_master_list("gradename");

		if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID){

			$gradeName =	Grade::pluck('grade','id')->all();

		}else{

			$gradeName =	Grade::where('club', Auth::guard('admin')->user()->id )->pluck('grade','id')->all();

		}

		

		// print_r($gradeName); die; 



		$teamType				=	$drop_down->get_master_list("teamtype");

		$teamCategory			=	$drop_down->get_master_list("teamcategory");

		$matchTypeList			=	$drop_down->get_master_list("matchtype");

		$cludDetails			=	$club->get_club_list();

		if(Auth::guard('admin')->user()->user_role_id != SUPER_ADMIN_ROLE_ID){

			if(Auth::guard('admin')->user()->user_role_id == CLUBUSER){

				$team = new Team;

			  	$clubId = ScorerAccess::where('user_id',Auth::guard('admin')->user()->id)->value('club_id');

				$teamList = $team->get_team_by_club(Auth::guard('admin')->user()->id);

			}else{

				$teamList = $team->get_team_by_club(Auth::guard('admin')->user()->id);

			}

		}

		return  View::make('admin.fixture.add',compact('gradeName','teamType','teamCategory','cludDetails','teamList','matchTypeList'));

	}//end addPlayer()



	public function saveFixture(){ 

	Input::replace($this->arrayStripTags(Input::all()));

		$thisData			=	Input::all();
		// print_r($thisData);die; 
		if(!empty($thisData)){

			if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID){

				$validator 					=	Validator::make(

					Input::all(),

					array(

						'grade'			=> 'required',

						'start_date'	=> 'required',

						'end_date'	=> 'required',

						'start_time'=> 'required',

						'end_time'=> 'required',

						'mode'	=> 'required',

						'club'	=> 'required',

						'team' => 'required',

						'match_type' => 'required',

					)

				);

			}else {

				$validator 					=	Validator::make(

					Input::all(),

					array(

						'grade'			=> 'required',

						'start_date'	=> 'required',

						'end_date'	=> 'required',

						'start_time'=> 'required',

						'end_time'=> 'required',

						// 'mode'	=> 'required',

						'club'	=> 'required',

						'team' => 'required',
						'match_type' => 'required',

					)

				);

			}



			if ($validator->fails()){

				$errors 	=	$validator->messages();

				$response	=	array(

					'success' 	=> false,

					'errors' 	=> $errors

				);

				return Response::json($response); 

				die;

			}else{ 

				$obj =  new Fixture;

				$obj->grade 	=  !empty(Input::get('grade')) ? Input::get('grade'):0;

				$obj->team 		=  Input::get('team');

				$obj->team_type	=  !empty(Input::get('team_type')) ? Input::get('team_type'):0;

				$obj->team_category	=  !empty(Input::get('team_category')) ? Input::get('team_category'):0;

				$obj->opposition_club	=  !empty(Input::get('opposition_club')) ? Input::get('opposition_club'):0;
// 
				$obj->start_date	=  !empty(Input::get('start_date')) ? Carbon::createFromFormat('d/m/Y', Input::get('start_date')): null;
				$obj->end_date	=  !empty(Input::get('end_date')) ? Carbon::createFromFormat('d/m/Y', Input::get('end_date')): null;
				$obj->start_time	=  !empty(Input::get('start_time')) ? date("G:i", strtotime(Input::get('start_time'))):0;
				$obj->end_time	=  !empty(Input::get('end_time')) ? date("G:i", strtotime(Input::get('end_time'))):0;
				$obj->match_type	=  !empty(Input::get('match_type')) ? Input::get('match_type'):0;

				$obj->vanue	=  !empty(Input::get('vanue')) ? Input::get('vanue'):0;

				$obj->mode 	=  !empty(Input::get('mode')) ? Input::get('mode'):0;

				$obj->club 	=  !empty(Input::get('club')) ? Input::get('club'):0;

				$obj->save();

			    Session::flash('success',trans("Fixture has been added successfully"));

				$response	=	array(

			    	'success' 	=>	'1',

			    	'message' 	=>	trans("Fixture has been added successfully.")

			    ); 

			    return Response::json($response);

			}

		}

	}//end saveForum



	/**

	* Function for edit Forums

	*

	* @param null

	*

	* @return view page. 

	*/

	public function editFixture($userId = 0){  
		if(Auth::guard('admin')->user()->is_lockout == 1){ 
			return Redirect::to('admin/fixture');
		}

		$userDetails			=	Fixture::find($userId); 

		if(empty($userDetails)) {

			return Redirect::to('admin/fixture');

		}

		if($userId){

			$userDetails		=	Fixture::find($userId);

			$drop_down	=	new DropDown();

			$club	=	new Club();

			$gradeName	=	$drop_down->get_master_list("gradename");

			if(Auth::guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID){

				$gradeName =	Grade::pluck('grade','id')->all();

			}else{

				$gradeName =	Grade::where('club', Auth::guard('admin')->user()->id )->pluck('grade','id')->all();

			}

			

			$teamType	=	$drop_down->get_master_list("teamtype");

			$teamCategory=	$drop_down->get_master_list("teamcategory");

			$matchTypeList=	$drop_down->get_master_list("matchtype");

			$cludDetails=	$club->get_club_list();

			$team =	new Team();

			$teamList =	$team->get_team();

			return View::make('admin.fixture.edit', compact('userDetails','teamType','userId','teamType','teamCategory','cludDetails','gradeName','teamList','matchTypeList'));

		}

	} // 



	/**

* Function for update Forum

*

* @param null

*

* @return view page. 

*/

	public function updateFixture(){

		Input::replace($this->arrayStripTags(Input::all()));

		$thisData			=	Input::all();
		$fixture_id = Input::get('fixture_id');

		if(!empty($thisData)){

			if(Auth::guard('admin')->user()->user_role_id == 1){

				$validator 					=	Validator::make(

					Input::all(),

					array(

						'grade'			=> 'required',

						'start_date'	=> 'required',

						'end_date'	=> 'required',

						'start_time'=> 'required',

						'end_time'=> 'required',

						'mode'	=> 'required',

						'club'	=> 'required',

						'team' => 'required',
						'match_type' => 'required',

					)

				);

			}else{

			$validator 					=	Validator::make(

				Input::all(),

				array(

					'grade'			=> 'required',

					'start_date'	=> 'required',

					'end_date'	=> 'required',

					'start_time'=> 'required',

					'end_time'=> 'required',

					// 'mode'	=> 'required',

					'club'	=> 'required',

					'team' => 'required',
					'match_type' => 'required',

				)

			);

			}



			if ($validator->fails()){

				$errors 	=	$validator->messages();

				$response	=	array(

					'success' 	=> false,

					'errors' 	=> $errors

				);

				return Response::json($response); 

				die;

			}else{ 

				$startDate = Input::get('start_date');
				$endDate = Input::get('end_date');;
				if($startDate){
					$startDate = str_replace('/', '-', $startDate);
					$startDate  = Date('Y-m-d',strtotime($startDate));
				}if(!empty($endDate)){
					$endDate = str_replace('/', '-', $endDate);
					$endDate  = Date('Y-m-d',strtotime($endDate));
				}
				$obj 		=  Fixture::find($fixture_id);

				$obj->grade 	=  !empty(Input::get('grade')) ? Input::get('grade'):0;

				$obj->team 		=  Input::get('team');

				$obj->team_type	=  !empty(Input::get('team_type')) ? Input::get('team_type'):0;

				$obj->team_category	=  !empty(Input::get('team_category')) ? Input::get('team_category'):0;

				$obj->opposition_club	=  !empty(Input::get('opposition_club')) ? Input::get('opposition_club'):0;

				// $obj->start_date	=  !empty(Input::get('start_date')) ? Input::get('start_date'):0;
				$obj->start_date	=  $startDate;

				$obj->end_date	=  $endDate;

				$obj->start_time	=  !empty(Input::get('start_time')) ? date("G:i", strtotime(Input::get('start_time'))):0;

				$obj->end_time	=  !empty(Input::get('end_time')) ? date("G:i", strtotime(Input::get('end_time'))):0;

				$obj->match_type	=  !empty(Input::get('match_type')) ? Input::get('match_type'):0;

				$obj->vanue	=  !empty(Input::get('vanue')) ? Input::get('vanue'):0;

				$obj->club 	=  !empty(Input::get('club')) ? Input::get('club'):0;

				$obj->save();

				Session::flash('success',trans("Fixture has been upated successfully"));

				$response	=	array(

			    	'success' 	=>	'1',

			    	'message' 	=>	trans("Fixture has been added successfully.")

			    ); 

			    return Response::json($response);

			}

		}

	}//end update Forum-manager

	

	/*View Details*/

	public function viewFixture($userId = 0){

		$fixtureDetails		=	Fixture::where('fixtures.id',$userId)

									->leftJoin('users' , 'fixtures.opposition_club' , '=' , 'users.id')

									->leftJoin('dropdown_managers' , 'fixtures.team_category' , '=' , 'dropdown_managers.id')

									->leftJoin('dropdown_managers as dropdown_managers1' , 'fixtures.team_type' , '=' , 'dropdown_managers1.id')

									->leftJoin('dropdown_managers as dropdown_managers2' , 'fixtures.match_type' , '=' , 'dropdown_managers2.id')

									->leftJoin('dropdown_managers as dropdown_managers3' , 'fixtures.grade' , '=' , 'dropdown_managers3.id')

									->leftJoin('teams' , 'fixtures.team' , '=' , 'teams.id')

									->select('fixtures.*','users.club_name','dropdown_managers.name  as team_category','dropdown_managers1.name  as team_type','dropdown_managers1.name  as match_type','dropdown_managers1.name  as match_type','dropdown_managers1.name  as grade_name','teams.name  as team_name')

									->first(); 		

		if(empty($fixtureDetails)) {

			return Redirect::to('admin/fixture');

		}							

		return View::make('admin.fixture.view', compact('fixtureDetails'));

	} // 





	/**

	 * Function for change is_active of Forum

	 *

	 * @param $Id as id of Forum

	 * @param $Forum is_active as is_active of Forum

	 *

	 * @return redirect page. 

	 */	

	public function updateStatus($Id = 0, $Status = 0){
		if(Auth::guard('admin')->user()->is_lockout == 1){ 
			return Redirect::to('admin/fixture');
		}

		Fixture::where('id', '=', $Id)->update(array('is_active' => $Status));

		Session::flash('flash_notice', trans("Status updated successfully.")); 

		return Redirect::to('admin/fixture');

	} // end updateStatus()

	

	

	/**

	/**

	* Function for mark a Forum as deleted 

	*

	* @param $Id as id of Forum

	*

	* @return redirect page. 

	*/

	public function deleteFixture($Id=0){
		if(Auth::guard('admin')->user()->is_lockout == 1){ 
			return Redirect::to('admin/fixture');
		}
		$userDetails	=	Fixture::find($Id); 
		if(empty($userDetails)) {
			return Redirect::to('admin/fixture');
		}

		if($Id){
			$userModel =	Fixture::where('id',$Id)->delete();
			$deleteFixtureScorcards = FixtureScorcard::where('fixture_id',$Id)->delete();
			$teamPlayer = TeamPlayer::where('fixture_id',$Id)->count();
			if($teamPlayer > 1){
				TeamPlayer::where('fixture_id',$Id)->delete();
			}
		}

		return Redirect::to('admin/fixture');

	}// end deleteForum



	public function scorecards($fixture_id=null){ 

		$team_id = null; 

		$status = FixtureScorcard::where('fixture_id',$fixture_id)->orderBy('created_at','DESC')->value('status');
		$isSaved = FixtureScorcard::where('fixture_id',$fixture_id)->orderBy('id','DESC')->first();
		$getTeamPlayer = TeamPlayer::where('fixture_id',$fixture_id)->select('player_id')->get();
		$fixtureData = Fixture::where('fixtures.id',$fixture_id)
						->leftJoin('grades','grades.id','=','fixtures.grade')
						->leftJoin('teams','teams.id','=','fixtures.team')
						->select('fixtures.*','grades.grade as grade','teams.name as team_name','teams.id as team_id')
						->first();
		if($status == 3){
			Session::flash('flash_notice', trans("Scorecards is completed.")); 
			return Redirect::to('admin/fixture');
		} 
		$teamPlayerCount = TeamPlayer::where('fixture_id',$fixture_id)->count();
		$fixtureScorecards = FixtureScorcard::where('fixture_id',$fixture_id)->count(); 
		if($fixtureData->match_type == ONEDAYMATCH){
			if($teamPlayerCount < 7){ 
			$needToAdd = (7 - $teamPlayerCount);
				Session::flash('flash_notice', trans("Please add $needToAdd more player to add scorecard.")); 
				return Redirect::to('admin/fixture');
			}if($fixtureScorecards < $teamPlayerCount){
				foreach ($getTeamPlayer as $key => $value) { 
    				FixtureScorcard::updateOrCreate(['fixture_id' => $fixture_id,'player_id' => $value->player_id],['inning'=>1]);
    			}
			}
		}if($fixtureData->match_type==TWODAYMATCH){
			$playerSecondInningPlayers = $teamPlayerCount *2;
			if($fixtureScorecards < $playerSecondInningPlayers){ 
				foreach ($getTeamPlayer as $key => $value) { 
    				FixtureScorcard::updateOrCreate(['fixture_id' => $fixture_id,'player_id' => $value->player_id],['inning'=>1]);
    				FixtureScorcard::updateOrCreate(['fixture_id' => $fixture_id,'player_id' => $value->player_id,'inning' => 2]);
    			}
			}
			if($fixtureScorecards > $playerSecondInningPlayers){ 
				foreach ($getTeamPlayer as $key => $value) { 
    				FixtureScorcard::updateOrCreate(['fixture_id' => $fixture_id,'player_id' => $value->player_id],['inning'=>1]);
    				FixtureScorcard::updateOrCreate(['fixture_id' => $fixture_id,'player_id' => $value->player_id,'inning' => 2]);
    			}
			}
		}
		

		$DB 					= 	FixtureScorcard::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);
			if(isset($searchData['order'])){

				unset($searchData['order']);

			}

			if(isset($searchData['sortBy'])){

				unset($searchData['sortBy']);

			}

			if(isset($searchData['page'])){

				unset($searchData['page']);

			}

			$date_from	=	'';

			$date_to	=	'';



			foreach($searchData as $fieldName => $fieldValue){

				if(!empty($fieldValue) || $fieldValue==0){

					$DB->where("fixture_scorecards.$fieldName",'like','%'.$fieldValue.'%');

				}

				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));

			} 

		}

		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'fixture_scorecards.created_at';

	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';


	    

	    $scoreData = FixtureScorcard::where(['fixture_id'=>$fixture_id])->count();
	   
	    if($scoreData==0){ 
	    	if(!$getTeamPlayer->isEmpty()){
	    		if($fixtureData->match_type == ONEDAYMATCH){
	    			foreach ($getTeamPlayer as $key => $value) { 

		    			$obj = new  FixtureScorcard;

				    	$obj->player_id = $value->player_id;

				    	$obj->fixture_id =$fixture_id ;

				    	$obj->inning = 1;

				    	$obj->save();

		    		}
	    		}else{
	    			foreach ($getTeamPlayer as $key => $value) { 

		    			$obj = new  FixtureScorcard;

				    	$obj->player_id = $value->player_id;

				    	$obj->fixture_id =$fixture_id ;

				    	$obj->inning = 1;

				    	$obj->save();

		    		}
		    		foreach ($getTeamPlayer as $key => $value) { 

		    			$obj = new  FixtureScorcard;

				    	$obj->player_id = $value->player_id;

				    	$obj->fixture_id =$fixture_id ;

				    	$obj->inning = 2;

				    	$obj->save();

		    		}
	    		}
	    	}

	    }else{

	    }
		$result = 	$DB->leftjoin('players','fixture_scorecards.player_id','=','players.id')
						//->leftjoin('fixture','fixture.id','=','fixture_scorecards.fixture_id')
						->select('fixture_scorecards.*','players.full_name as player_name','players.position as player_position')
						->where('fixture_scorecards.fixture_id',$fixture_id)
						->orderBy('id', 'ASC')
						->paginate(40);
		$playerScorcards = FixtureScorcard::where('fixture_id',$fixture_id)->where('player_card','!=','')->value('player_card');
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		$playerImages = FixtureScorcard::where('fixture_id',$fixture_id)->where('player_card','!=','')->first();
		return  View::make('admin.fixture.scorcards', compact('result','searchVariable','sortBy','order','query_string','fixture_id','team_id','fixtureData','status','playerImages','playerScorcards','isSaved'));

	}

	public function updateScorecardStatus(Request $request){

		$val  = $request->status;

		$fixture_id = $request->fixture_id;
		FixtureScorcard::where('fixture_id',$fixture_id)->update(['status'=>$val]);
		if($val == 3){ 
		
			$this->updatePlayerPrice($fixture_id);
		}
		echo 1;die;
	}

	public function editFixtureScorcard(Request $request){    
		Input::replace($this->arrayStripTags(Input::all())); 

		$fixture_id = Input::get('fixture_id');

		$team_id = Input::get('team_id');

		$inning = Input::get('inning');

		$thisData	=	Input::all();

		$pep_price = 0.00; 

		$tspm = 0; 

		$ideal_price = 0.00; 

		$expected_price = array(); 

		$temp_player = array(); 

		$fantasy_range = array(); 

		$price_change = array(); 
		
		if(!empty($thisData)){
			$validator 					=	Validator::make(Input::all(),array('inning'=> 'required','player_card'=>'nullable|max:2000'));
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}
			else{ 
				$userId = Fixture::where('id',$fixture_id)->value('club');
				$gamePointsValue = GamePoint::whereIn('attribute_key',array('rs','fours','sixes','wks','mdns','cs','cwks','sts','rods','roas','dks','mtch','30rs','50rs','75rs','100rs','200rs','3wks','4wks','5wks','6wks','7wks','8pluswks','0to4econs','4to6econs','6plusecons','ovrs','hattrick'))->where('club',$userId)->get();
				$newArr = [];
				$n=1;
				$imageName = "";
				foreach($thisData['data'] as $key=>$value){
					//3=>AR;4=>WK;2=>Bowler;1=>BATS;
					$playerType = Player::where('id',$value['player_id'])->first();

					if($playerType->position == 2){
						if(!empty($gamePointsValue)){
							foreach ($gamePointsValue as $key2 => $value2) { 
								$newArr[$value2->attribute_key] =  $value2->bowler;
							}
						}
					}else{ 
					
						if(!empty($gamePointsValue)){
							foreach ($gamePointsValue as $key2 => $value2) { 
								$newArr[$value2->attribute_key] =  $value2->bats_wk_ar;
							}
						}
					}
					$obj =  FixtureScorcard::find($key);
					$obj->rs = !empty($value['rs']) ?$value['rs'] :0;
					$obj->fours = !empty($value['fours']) ? $value['fours'] :0;
					$obj->sixes = !empty($value['sixes']) ? $value['sixes']:0;
					$obj->wks = !empty($value['wks']) ? $value['wks']:0;
					$obj->mdns = !empty($value['mdns']) ? $value['mdns'] :0;
					$obj->cs = !empty($value['cs']) ? $value['cs'] :0;
					$obj->cwks = !empty($value['cwks']) ? $value['cwks'] :0;
					$obj->sts = !empty($value['sts']) ? $value['sts'] :0;
					$obj->rods = !empty($value['rods']) ? $value['rods']:0;
					$obj->roas = !empty($value['roas']) ? $value['roas'] :0;
					$obj->dks = !empty($value['dks']) ? $value['dks'] :0;
					$obj->hattrick = !empty($value['hattrick']) ? $value['hattrick'] :0;
					$obj->run = !empty($value['run']) ? $value['run'] :0;
					$obj->overs = !empty($value['overs']) ? $value['overs'] :0;
					$rs = $fours= $sixes= $wks= $mdns= $cs= $cwks= $sts= $rods= $roas= $dks= $match= $extraWkcts =  0;


					if(!empty($newArr['rs']) && $value['rs']){
						$rs = $newArr['rs']*$value['rs'];

					}if(!empty($newArr['fours']) && $value['fours']){
						$fours = $newArr['fours']*$value['fours'];
					}
					if(!empty($newArr['sixes']) && $value['sixes']){
						$sixes = $newArr['sixes']*$value['sixes'];
					}
					if(!empty($newArr['wks']) && $value['wks']){
						$wks = $newArr['wks']*$value['wks'];
					}
					if(!empty($newArr['mdns']) && $value['mdns']){
						$mdns = $newArr['mdns']*$value['mdns'];
					}
					if(!empty($newArr['cs']) && $value['cs']){
						$cs = $newArr['cs']*$value['cs'];
					}
					if(!empty($newArr['cwks']) && $value['cwks']){
						$cwks = $newArr['cwks']*$value['cwks'];
					}
					if(!empty($newArr['sts']) && $value['sts']){
						$sts = $newArr['sts']*$value['sts'];
					}
					if(!empty($newArr['rods']) && $value['rods']){
						$rods = $newArr['rods']*$value['rods'];
					}
					if(!empty($newArr['roas']) && $value['roas']){
						$roas = $newArr['roas']*$value['roas'];
					}
					if(!empty($newArr['dks']) && $value['dks']){
						$dks = $newArr['dks']*$value['dks'];
					}			
																
					if((!empty($newArr['30rs'])) && ($value['rs'] >= 30 && $value['rs'] < 50)){ 
						$rs = $rs + $newArr['30rs'];
					}

					if((!empty($newArr['50rs'])) && ($value['rs'] >= 50 && $value['rs'] < 75)){ 
						$rs = $rs + $newArr['50rs'];
					}
					if((!empty($newArr['75rs'])) && ($value['rs'] >= 75 && $value['rs'] < 100)){ 
						$rs = $rs + $newArr['75rs'];
					}

					if((!empty($newArr['100rs'])) && ($value['rs'] >= 100 && $value['rs'] < 200)){ 
						$rs = $rs + $newArr['100rs'];
					}

					if((!empty($newArr['200rs'])) && ($value['rs'] >= 200)){ 
						$rs = $rs + $newArr['200rs'];
					}
						
					if(!empty($value['wks'])){		

						if((!empty($newArr['3wks'])) && ($value['wks'] > 2) && ($value['wks'] <= 3)){   
							$extraWkcts = $newArr['3wks'];
						}  											
						if((!empty($newArr['4wks'])) && ($value['wks'] >= 4) && ($value['wks'] < 5)){  
							$extraWkcts = $extraWkcts + $newArr['4wks'];
						}												
						if((!empty($newArr['5wks'])) && ($value['wks'] >= 5 ) && ($value['wks'] < 6)){   
							$extraWkcts = $extraWkcts + $newArr['5wks'];
						}
						if((!empty($newArr['6wks'])) && ($value['wks'] >= 6 ) && ($value['wks'] < 7)){ 
							$extraWkcts = $extraWkcts + $newArr['6wks'];
						}

						if((!empty($newArr['7wks'])) && ($value['wks'] >= 7 ) && ($value['wks'] < 8)){ 
							$extraWkcts = $extraWkcts + $newArr['7wks']; 
						}
                      													
						if((!empty($newArr['8pluswks'])) && ($value['wks'] >= 8)){;
							$extraWkcts = $extraWkcts + $newArr['8pluswks'];
						}
					}					     
					/*hat-Trick*/
					$hattrick = 0;
					if(!empty($newArr['mtch'])){
							$match = $newArr['mtch'];
					}
					if((!empty($newArr['hattrick'])) && !empty($value['hattrick'])){ 
						$hattrick = $newArr['hattrick']*$value['hattrick'];
					}

					/*Econ*/
					$econ = $zerotofourecons = $fourtosixecons  = $sixplusecons = 0;
					if(!empty($value['overs']) && !empty($value['run'])){
						$econ = $value['run'] / $value['overs'];
					}


					if(!empty($econ)){
						if((!empty($newArr['0to4econs'])) &&($econ<=4)  ){
							$zerotofourecons = $newArr['0to4econs'];
						}
						//econ = 5
						if(!empty($newArr['4to6econs']) && ($econ > 4) && ($econ <= 6)){ 
							$fourtosixecons = $newArr['4to6econs'];
						}
						if((!empty($newArr['6plusecons'])) &&($econ > 6)  ){
							$sixplusecons = $newArr['6plusecons'];
						}
					}
					//echo $sixplusecons;die;
					$fantasyPoints = ($rs+$fours+$sixes+$wks+$mdns+$cs+$cwks+$sts+$rods+$roas+$dks+$zerotofourecons+$fourtosixecons+$sixplusecons+$hattrick+$match+$extraWkcts);

					$obj->fantasy_points = $fantasyPoints;
					$obj->inning = $inning; 
					$obj->fall_of_wickets = !empty(Input::get('fall_of_wickets')) ? Input::get('fall_of_wickets'):'';
					$obj->match_report = !empty(Input::get('match_report')) ? Input::get('match_report') :'';

					/*Multipe scorecards upload option*/
					if(!empty($request->player_card)){
						$imageName = [];
						foreach ($request->player_card as $key => $socrecardValue) {
							$extension 			=	$socrecardValue->getClientOriginalExtension();
							$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
							$folderPath			=	PLAYER_CARD_IMAGE_ROOT_PATH.$newFolder; 
							if(!File::exists($folderPath)){
								File::makeDirectory($folderPath, $mode = 0777,true);
							}
							$userImageName = time().$key.'-player-card.'.$extension;
							$image = $newFolder.$userImageName;
							if($n==1){
								if($socrecardValue->move($folderPath, $userImageName)){
									$imageName[] =	$image;
								}
							}
							$obj->player_card =	implode(",",$imageName);	
						}
					}
					/*if(Input::hasFile('player_card')[0]){
						$extension 			=	Input::file('player_card')->getClientOriginalExtension();
						$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
						$folderPath			=	PLAYER_CARD_IMAGE_ROOT_PATH.$newFolder; 
						if(!File::exists($folderPath)){
							File::makeDirectory($folderPath, $mode = 0777,true);
						}
						$userImageName = time().'-player-card.'.$extension;
						$image = $newFolder.$userImageName;
						if($n==1){
							if(Input::file('player_card')->move($folderPath, $userImageName)){
								$imageName =	$image;
							}
						}
						$obj->player_card =	$imageName;						
					}*/

					$obj->save();
					$playerPoints = PlayerPoint::firstOrNew(['score_id' => $key]);
					$playerPoints->fixture_id = $fixture_id;
					$playerPoints->score_id = $key;
					$playerPoints->player_id = $value['player_id'];
					$playerPoints->inning = $inning;
					$playerPoints->fantasy_points = $fantasyPoints;
					$playerPoints->save();
					Session::flash('flash_notice', trans("Scorecards has been updated successfully."));
					$n++;
				}

			}

		}
	    return Redirect::to('admin/fixture/scorecards/'.$fixture_id.'/'.$team_id);

	}



	public function updatePlayerPrice($fixture_id = null){
		// echo 'hi'; die; 
		ini_set('memory_limit', '-1');
		if(empty($fixture_id)){

			Session::flash('flash_notice', trans("Internal Error occured. Please try again."));
			return Redirect::back();
		}

		$pep_price = 0.00; 
		$tspm = 0; 
		$ideal_price = 0.00; 
		$expected_price = array(); 
		$temp_player = array(); 
		$fantasy_range = array(); 
		$price_change = array(); 
		$flg = false; 
			// if(FixtureScorcard::where('fixture_id',  $fixture_id)->where('inning', 1)->exists() && FixtureScorcard::where('fixture_id',  $fixture_id)->where('inning', 2)->exists()){

			// }
				$all_fx_score_in1 = FixtureScorcard::where('fixture_id', $fixture_id)->where('inning', 1)->get()->toArray(); 
				$all_fx_score_in2 = FixtureScorcard::where('fixture_id', $fixture_id)->where('inning', '<>', 1)->get()->toArray(); 
				$all_fx_score_in1_temp = array(); 
				$player_fp_arr = array(); 
				$al_player_arr = array(); 
				foreach ($all_fx_score_in1 as $key => $value) {
					$all_fx_score_in1_temp[$value['player_id']] = $value; 
					$al_player_arr[] = $value['player_id'] ; 
				}
				$all_fx_score_in1 = $all_fx_score_in1_temp ; 
				// echo '<pre>'; 
				// print_r($all_fx_score_in1); die; 
				$all_fx_score_in2_temp = array(); 
				foreach ($all_fx_score_in2 as $key => $value) {
					$all_fx_score_in2_temp[$value['player_id']] = $value; 
					$al_player_arr[] = $value['player_id'] ; 
				}
				$all_fx_score_in2 = $all_fx_score_in2_temp ; 
				// echo '<pre>'; 
				sort($al_player_arr); 
				// pr($al_player_arr); 
				$al_player_arr = array_unique($al_player_arr); 

				// pr($al_player_arr); die; 

				foreach ($al_player_arr as $key => $value) {
					
					$playerType = Player::where('id',$value)->first();

					$old_value = $playerType->svalue; //  Old Price (OP) of players
					$temp_player[$playerType->id] = $playerType ; 
					$pep_price = (float) $pep_price + $old_value; // Player Eleven Price (PEP) of the team playing the match (i.e. total of price of the 11 (or more) players in the match)
					$tspm = (int) $tspm + (isset($all_fx_score_in1[$value]) ? $all_fx_score_in1[$value]['fantasy_points'] : 0) + (isset($all_fx_score_in2[$value]) ? $all_fx_score_in2[$value]['fantasy_points'] : 0); // Total Scoring Points of the Match (i.e. total of fantasy points scored by all players in the team of a match)

					$player_fp_arr[$value] = (int)(isset($all_fx_score_in1[$value]) ? $all_fx_score_in1[$value]['fantasy_points'] : 0) + (isset($all_fx_score_in2[$value]) ? $all_fx_score_in2[$value]['fantasy_points'] : 0); 

					// print_r($playerType); 
				 
					// pep
					// tspm
					// ideal_price
					// expected_price
					// fantasy_range



				}

				// print_r($tspm); die; 
				// print_r($pep_price); die; 

				// print_r($al_player_arr); 
				// print_r($all_fx_score_in1); 
				// print_r($all_fx_score_in2); die; 

				$ideal_price = $tspm/ (float) $pep_price;  // Ideal Price (i.e. Total Scoring Points of the Match (TPSM) / Player Eleven Price (PEP))
				$ideal_price = round($ideal_price, 1); 



				foreach ($temp_player as $key => $value) {
					$expected_price[$key] = (float)$value->svalue * $ideal_price ; 
					$fantasy_range[$key] = round((float)$expected_price[$key] * 0.1) ; 
					$lfr = (int)$expected_price[$key] - $fantasy_range[$key] ; 
					$ufr = (int)$expected_price[$key] + $fantasy_range[$key] ; 
					// echo $player_fp_arr[$key].'<br>'; 
					// echo $lfr.'<br>'; 
					// echo $ufr.'<br>'; 
					$price_change[$key] = $player_fp_arr[$key] < $lfr ? -0.25 : ($player_fp_arr[$key] > $ufr ? +0.25 : 0.00); 
					// echo $price_change[$key].'<br>'; 
				}

				// print_r($expected_price); 
				// print_r($fantasy_range); 
				// print_r($price_change); 

				// die; 

				$cur_fixture = Fixture::where('id', $fixture_id)->first(); 

				// echo $fixture_id; 
				// print_r($cur_fixture); 
				// print_r($ideal_price); die;

				if($cur_fixture->match_type == 27){ // for oneday
					if(FixtureScorcard::where('fixture_id',  $fixture_id)->where('inning', 1)->exists()){
						Fixture::where('id', $fixture_id)->update(['pep'=> $pep_price, 'tspm' => $tspm ]);
					}
				}else{
					if(FixtureScorcard::where('fixture_id',  $fixture_id)->where('inning', 1)->exists() && FixtureScorcard::where('fixture_id',  $fixture_id)->where('inning', 2)->exists()){
						Fixture::where('id', $fixture_id)->update(['pep'=> $pep_price, 'tspm' => $tspm ]);
					}
				}
				

// pep
// tspm
// ideal_price
// expected_price
// fantasy_range
// price_change




// $pep_price
// $tspm
// $ideal_price
// $expected_price
// $fantasy_range
// $price_change
// $fixture_id


				foreach ($price_change as $key => $value) {

					$tmp_plyr  = Player::where('id', $key)->first(); 

					if(!empty($tmp_plyr) && $tmp_plyr->svalue > 6){ // change the player value only if its value > 6

						if($cur_fixture->match_type == 27){ // for oneday
							if(FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $key)->where('inning', 1)->where('price_updated', 0)->exists()){
								Player::where('id', $key)->update(['ideal_price' => $ideal_price, 'expected_price' => (isset($expected_price[$key]) ? $expected_price[$key] : 0), 'fantasy_range' => (isset($fantasy_range[$key]) ? $fantasy_range[$key] : 0), 'price_change' => $value, 'svalue' => DB::raw('svalue+'.$value)]);
								FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $key)->update(['price_updated' => 1]); 
								// echo 'hi'; die; 
								$flg = true; 
							}
						}else{
							if(FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $key)->where('inning', 1)->where('price_updated', 0)->exists() && FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $key)->where('inning', 2)->where('price_updated', 0)->exists()){
								Player::where('id', $key)->update(['ideal_price' => $ideal_price, 'expected_price' => (isset($expected_price[$key]) ? $expected_price[$key] : 0), 'fantasy_range' => (isset($fantasy_range[$key]) ? $fantasy_range[$key] : 0), 'price_change' => $value, 'svalue' => DB::raw('svalue+'.$value)]);
								FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $key)->update(['price_updated' => 1]); 
								// echo 'hello'; die; 
								$flg = true; 
							}
						}


					}



				}



		// update the UserTeamValueLog and UserTeams table ************ start 

	
	foreach ($al_player_arr as $key => $value) {
		$tmppl = Player::find($value); 
		if(!empty($tmppl)){
			$psl = new  PlayerSvalueLog; 
			$psl->player_id = $tmppl->id; 
			$psl->svalue = $tmppl->svalue; 
			$psl->save(); 
		}

	}


	$cur_fixture_data = Fixture::find($fixture_id); 


	$club_val = $cur_fixture_data->club; 

		$temp_all_team_arr = UserTeams::where('club_id', $club_val)->pluck('id')->toArray();

	/* commented on 11-02-19 as now we give player points directly to each team 
	$check_team_arr = array(); 
	$temp_all_team_arr = array(); 
	foreach ($al_player_arr as $key => $value) {

	    $temp_all_team = UserTeamPlayers::where('player_id', $value)->pluck('team_id')->toArray();
	    $temp_all_team = array_unique($temp_all_team);
	    // echo 'all team '; 
	    // print_r($temp_all_team); 
	    $temp_sum = FixtureScorcard::where('fixture_id',  $fixture_id)->where('player_id', $value)->sum('fantasy_points'); 

	    // echo 'sum fantasy '.$temp_sum; 




	   foreach ($temp_all_team as $k2 => $v2) {
	   		$temp_all_team_arr[] = $v2; 
	    	// if(in_array($v2, $check_team_arr)) continue;
	    	// $check_team_arr[] = $v2; 

	        $temp_tm = UserTeams::find($v2); 



		$player_in_date = UserTeamsPlayerTrack::where('player_id', $value)->where('team_id', $v2)->whereNull('remove_date')->first(); 
		if(empty($player_in_date)){
			$player_in_date = UserTeamPlayers::where('player_id', $value)->where('team_id', $v2)->first(); 
		}

		if(!empty($player_in_date)  && Carbon::parse($cur_fixture_data->start_date) >= Carbon::parse($player_in_date->created_at) ){
			// print_r($player_in_date); 
	        if($temp_tm->capton_id == $value){

	        	//if capton card is activated in the current week 
    	if(UserTeamLogs::where('team_id', $temp_tm->id)->where('log_type', 2)->whereBetween('created_at',  [Carbon::parse($cur_fixture_data->start_date)->startOfWeek(), Carbon::parse($cur_fixture_data->start_date)->endOfWeek()] )->exists()){
    		$temp_tm->total_team_point += ($temp_sum * 3); 
    		$temp_tm->three_cap_point += $temp_sum ; 

    		// store extra point of capton in gameweek start 


				if(UserTeamsGWExtraPTTrack::where(['team_id' => $temp_tm->id, 'gw_end_date' => Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay() ])->exists()){

					$tmp = UserTeamsGWExtraPTTrack::where('team_id', $temp_tm->id)->where('gw_end_date',  Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay())->first(); 
					$tmp->gw_extra_pt = round($tmp->gw_extra_pt + $temp_sum) ; 
					$tmp->save(); 

				}else{

					$tmp = new UserTeamsGWExtraPTTrack; 
					$tmp->team_id = $temp_tm->id ; 

					$tmp->gw_extra_pt = round( $temp_sum) ; 
					$tmp->gw_end_date = Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay() ; 
					$tmp->save(); 

				}

    		// store extra point of capton in gameweek end 



    	}else{
    		$temp_tm->total_team_point += ($temp_sum * 2); 
    	}

	        
	        }elseif($temp_tm->vice_capton_id == $value){
	        	$temp_tm->total_team_point += ($temp_sum * 1.5); 
	        }else{
	        	$temp_tm->total_team_point += $temp_sum; 
	        }
	        $temp_tm->save(); 

		}


	    }
	}


	$temp_all_team = array_unique($temp_all_team_arr); 




			// team rank start 
				$temp = UserTeams::where('club_id', $cur_fixture_data->club)->pluck('total_team_point', 'id')->toArray(); 

				arsort($temp); 
		// $temp_points_arr_past_wk = $temp_last_wk_pt; 
				$i = 1; 
				foreach ($temp as $key => $value) {
					$temp[$key] = $i++ ; 
				}

				$team_ranks_arr = $temp; 


			// team rank end 
*/ 

	// code to update the gameweek point start , code copied from the updateGWPoint function of CronController 



	foreach ($al_player_arr as $key => $value) {

		$tmp_fs = FixtureScorcard::where('player_id', $value)->where('fixture_id', $fixture_id)->sum('fantasy_points'); 

		$org_tmp_fs = $tmp_fs ; 


		$tmp_team = UserTeamPlayers::where('player_id', $value)->pluck('team_id')->toArray();  // the team ids in which this player belong

		$tmptmp_team = $tmp_team; 

		foreach ($tmp_team as $tt_k => $tt_v) {
			if(UserTeamsPlayerTrack::where('team_id', $tt_v)->where('player_id', $value)->exists()){  // if related record found in UserTeamsPlayerTrack table then delete it now we will get it from UserTeamsPlayerTrack table later 
				if (($k1 = array_search($tt_v, $tmptmp_team)) !== false) {
				    unset($tmptmp_team[$k1]);
				}
			}
		}



		// get the team from  UserTeamsPlayerTrack table for this player if at the time of fixture player was in his team 
    $utpt_tmp_team = UserTeamsPlayerTrack::where('player_id', $value)
              ->where(function($q) use ($cur_fixture_data) {
                    $q->where(function($query) use ($cur_fixture_data) {
                            $query->where('created_at', '<', Carbon::parse($cur_fixture_data->start_date))->whereNull('remove_date'); 
                        })
                      ->orWhere(function($query) use ($cur_fixture_data) {
                            $query->where('created_at', '<', Carbon::parse($cur_fixture_data->start_date))->where('remove_date', '>', Carbon::parse($cur_fixture_data->start_date)); 
                        });
                    })->pluck('team_id')->toArray(); // get the team ids in which this player belong at the time of fixture



	$tmp_team = array_unique(array_merge($tmptmp_team, $utpt_tmp_team), SORT_REGULAR);



		foreach ($tmp_team as $tt_key => $tt_val) {

			$tmp_team_data = UserTeams::find($tt_val); 

			$is_capton = false ; 
			$is_vice_capton = false; 

			$tmp_fs = $org_tmp_fs; // assign orignal tmp_fs again  to restore to original 

			// if record exist for this team in UserTeamsCVCTrack table then check from that table 
			if(UserTeamsCVCTrack::where('team_id', $tt_val)->where('created_at', '<', Carbon::parse($cur_fixture_data->start_date))->exists()){
				$utcvct = UserTeamsCVCTrack::where('team_id', $tt_val)->where('player_id', $value)->where('created_at', '<', Carbon::parse($cur_fixture_data->start_date))->where('remove_date', '>', Carbon::parse($cur_fixture_data->start_date))->first(); 
				if(!empty($utcvct)){
					$utcvct->c_vc == 0 ? ($is_vice_capton = true) : ($is_capton = true); 
				}
			}else{ // else check form UserTeams table 

				  $tmp_team_data->capton_id == $value ? ($is_capton = true) : ($tmp_team_data->vice_capton_id == $value ? ($is_vice_capton = true) : '' ); 
			}

			if($is_capton){ // if player is captain
				// if capton card active
				if(UserTeamLogs::where('team_id', $tmp_team_data->id)->where('log_type', 2)->whereBetween('created_at',  [Carbon::parse($cur_fixture_data->start_date)->startOfWeek(), Carbon::parse($cur_fixture_data->start_date)->endOfWeek()] )->exists()){
		    		$tmp_fs = $tmp_fs * 3; // in case of capton card user will get point multiple of 3
		    		$three_cap_extra_point = $tmp_fs; // extra captain point got in this gameweek 
		    	}else{
					$tmp_fs = $tmp_fs * 2; 	
		    	}


			}elseif($is_vice_capton){
				$tmp_fs = $tmp_fs * 1.5; 
			}else{
				$tmp_fs = $org_tmp_fs;  // else again assign the original points 
			}

			$tmp_fs = round($tmp_fs); 

	// dealer card deducation 
	$utl_d_d = UserTeamLogs::where(['team_id' => $tt_val, 'log_type' => 4, 'dealer_card_status' => 0])->whereBetween('created_at',  [Carbon::parse($cur_fixture_data->start_date)->startOfWeek(), Carbon::parse($cur_fixture_data->start_date)->endOfWeek()] )->first();
	if(!empty($utl_d_d)){
		$tmp_fs = $tmp_fs - 50; // deduct 50 point of this team if they used dealer card for this game week 
		$utl_d_d->dealer_card_status = 1; // update dealer_card_status field to 1 as point deducted here
		$utl_d_d->save(); 
	}

	// shield steal deduction , In case of success steal 25% of upcoming gameweek points of second team will be added in first team , and in case of failed steal (shield applied by second team) then 25% of game week point deducted from first team


	$utl_ss_d = UserTeamLogs::where(['team_id' => $tt_val, 'log_type' => 7])->whereBetween('created_at',  [Carbon::parse($cur_fixture_data->start_date)->subWeek()->startOfWeek(), Carbon::parse($cur_fixture_data->start_date)->subWeek()->endOfWeek()] )->first();

		// if steal record exist for the previous gameweek of fixture and  fixture gameweek is already completed , then point deduction code
	if(!empty($utl_ss_d) && Carbon::parse($cur_fixture_data->start_date)->endOfWeek() < Carbon::now()){

		// if shield activated by the opposite team for this fixture date gameweek, then 25% of the first team will be deducted and provide to the second team otherwise reverse 
		$utl_s_done = UserTeamLogs::where(['team_id' => $utl_ss_d->steal_team_id, 'log_type' => 6])->whereBetween('created_at',  [Carbon::parse($cur_fixture_data->start_date)->startOfWeek(), Carbon::parse($cur_fixture_data->start_date)->endOfWeek()] )->first();

		if(!empty($utl_s_done)){ // its mean shield activated by counter team

			// get the gameweek record of the first team
			$tmp_ss_first = UserTeamsGWExtraPTTrack::where('team_id', $tt_val)->where('gw_end_date',  Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay())->first(); 

			$tmp_ss_second = UserTeamsGWExtraPTTrack::where('team_id', $utl_ss_d->steal_team_id)->where('gw_end_date',  Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay())->first();

			if(!empty($tmp_ss_first->gw_pt)){
				if($tmp_ss_first->shield_steal_status == 0){ 
					// deduct 25% of the first team
					$deduct_25 = (round($tmp_ss_first->gw_pt * 0.25, 2) + round($tmp_fs * 0.25, 2)); 
					$tmp_fs = $tmp_fs - $deduct_25; 
					
					$tmp_ss_first->shield_steal_status = 1; // update shield_steal_status to 1 
					$tmp_ss_first->save();	

				}else{  // if shield_steal_status == 1 
					$deduct_25 = round($tmp_fs * 0.25, 2);
				}

				// now add this 25% to second team 

				if(!empty($tmp_ss_second)){

					$tmp_ss_second->gw_pt += $deduct_25; 
					$tmp_ss_second->save();
				}else{


							$tmp = new UserTeamsGWExtraPTTrack; 
							$tmp->team_id = $utl_ss_d->steal_team_id ; 
							if($deduct_25 >= 0){	
								// $tmp->gw_extra_pt = round($tmp->gw_extra_pt + $gw_etp) ; 
								$tmp->gw_pt += $deduct_25 ; //  $gw_etp + $tmp->three_cap_point + $tmp->gw_extra_pt - $tmp->dealer_card_minus_points; 
								$last_overall_pt = UserTeamsGWExtraPTTrack::where('team_id', $utl_ss_d->steal_team_id)->where('overall_pt', '>', 0)->orderBy('id', 'DESC')->first(); 
								if(!empty($last_overall_pt)){
									$tmp->overall_pt = $last_overall_pt->overall_pt + $deduct_25; //  store the overall point for this gw by adding the deduct_25 point into the previous overall point
								}else{
									$tmp->overall_pt = $deduct_25; // store the overall point same as deduct_25 point if current gameweek is the first gameweek to be stored.
								}
							}

							$tmp->gw_end_date = Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay() ; 
							$tmp->save(); 

				}


				// update all overall point of future record start

	          
if(UserTeamsGWExtraPTTrack::where('team_id', $utl_ss_d->steal_team_id)->where('gw_end_date', '>', Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay()->addDay())->exists()){
  // for gw
$future_gw_data = UserTeamsGWExtraPTTrack::where('team_id', $utl_ss_d->steal_team_id)->where('gw_end_date', '>', Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay()->addDay())->get();
	foreach ($future_gw_data as $fgd_ky => $fgd_val) {
		$fgd_val->overall_pt += $deduct_25; 
		$fgd_val->save(); 
	}

}


// for month

if(UserTeamsMonthPTTrack::where('team_id', $utl_ss_d->steal_team_id)->where('month_end_date', '>', Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay()->addDay())->exists()){

$future_m_data = UserTeamsMonthPTTrack::where('team_id', $utl_ss_d->steal_team_id)->where('month_end_date', '>', Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay()->addDay())->get();
	
	foreach ($future_m_data as $fmd_ky => $fmd_val) {

		$fmd_val->overall_pt += $deduct_25; 
		$fmd_val->save(); 
	}

}


				// update all overall point future record end 






				
			}else{

			}


		}else{ // else shield not activated by counter team

		}


	}




			// update UserTeamsGWExtraPTTrack table start 

						if(UserTeamsGWExtraPTTrack::where(['team_id' => $tt_val, 'gw_end_date' => Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay() ])->exists()){
//gw_extra_pt  gw_pt  overall_pt  gw_rank  overall_rank  dealer_card_minus_points    three_cap_point
							$tmp = UserTeamsGWExtraPTTrack::where('team_id', $tt_val)->where('gw_end_date',  Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay())->first(); 
							if($tmp_fs >= 0){	
								// $tmp->gw_extra_pt = round($tmp->gw_extra_pt + $gw_etp) ; 
								$tmp->gw_pt += $tmp_fs ; //  $gw_etp + $tmp->three_cap_point + $tmp->gw_extra_pt - $tmp->dealer_card_minus_points; 
								if(!empty($three_cap_extra_point))
									$tmp->three_cap_point = $three_cap_extra_point ;  // store the extra three cap point in this game week 
								
								if(!empty($tmp->overall_pt)){
									$tmp->overall_pt += $tmp_fs;  // add the player point to the overall point of this gameweek 
								}else{
									$last_overall_pt = UserTeamsGWExtraPTTrack::where('team_id', $tt_val)->where('overall_pt', '>', 0)->orderBy('id', 'DESC')->first(); 
									if(!empty($last_overall_pt)){
										$tmp->overall_pt = $last_overall_pt->overall_pt + $tmp_fs; //  store the overall point for this gw by adding the current gw point into the previous overall point
									}else{
										$tmp->overall_pt = $tmp_fs; // store the overall point same as gameweek if current gameweek is the first gameweek to be stored.
									}
								}


							}

							// store gameweek player points array as serialized array 
							if(!empty($tmp->gw_player_points)){
								$tmp->gw_player_points = @unserialize($tmp->gw_player_points) ; 

								if(!empty($tmp->gw_player_points) && is_array($tmp->gw_player_points)){
									// $tmp_gpp = array(); $tmp_gpp1 = array(); 
									$tmp_gpp = $tmp->gw_player_points; 
									$tmp_gpp[$value] = !empty($tmp_gpp[$value]) ? $tmp_gpp[$value] + $tmp_fs : $tmp_fs; 
// $tmp_gpp[$v] = !empty($tmp_gpp[$v]) ? $tmp_gpp[$v] + 20 : 20; 
									$tmp->gw_player_points = @serialize($tmp_gpp) ; 

								}else{
									$tmp->gw_player_points = @serialize(array($value => $tmp_fs));  // if empty then assign the player points as serrialized array
								}



							}else{
								$tmp->gw_player_points = @serialize(array($value => $tmp_fs));  // if empty then assign the player points as serrialized array
							}

							// $tmp->gw_player_points = !empty($gw_player_points_arr) ? @serialize($gw_player_points_arr) : null; 
							$tmp->save(); 

						}else{

							$tmp = new UserTeamsGWExtraPTTrack; 
							$tmp->team_id = $tt_val ; 
							if($tmp_fs >= 0){	
								// $tmp->gw_extra_pt = round($tmp->gw_extra_pt + $gw_etp) ; 
								$tmp->gw_pt += $tmp_fs ; //  $gw_etp + $tmp->three_cap_point + $tmp->gw_extra_pt - $tmp->dealer_card_minus_points; 
								if(!empty($three_cap_extra_point))
									$tmp->three_cap_point = $three_cap_extra_point ;  // store the extra three cap point in this game week 

								$last_overall_pt = UserTeamsGWExtraPTTrack::where('team_id', $tt_val)->where('overall_pt', '>', 0)->orderBy('id', 'DESC')->first(); 
								if(!empty($last_overall_pt)){
									$tmp->overall_pt = $last_overall_pt->overall_pt + $tmp_fs; //  store the overall point for this gw by adding the current gw point into the previous overall point
								}else{
									$tmp->overall_pt = $tmp_fs; // store the overall point same as gameweek if current gameweek is the first gameweek to be stored.
								}
							}



							$tmp->gw_player_points = @serialize(array($value => $tmp_fs));  //  empty so assign the player points as serrialized array
							// $tmp->gw_player_points = !empty($gw_player_points_arr) ? serialize($gw_player_points_arr) : null; 
							$tmp->gw_end_date = Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay() ; 
							$tmp->save(); 

						}

					// // update UserTeamsGWExtraPTTrack table end 


// month_pt  overall_pt  month_rank  overall_rank






			// update UserTeamsMonthPTTrack table start 

						if(UserTeamsMonthPTTrack::where(['team_id' => $tt_val, 'month_end_date' => Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay() ])->exists()){
//gw_extra_pt  gw_pt  overall_pt  gw_rank  overall_rank  dealer_card_minus_points    three_cap_point
							$tmp = UserTeamsMonthPTTrack::where('team_id', $tt_val)->where('month_end_date',  Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay())->first(); 
							if($tmp_fs >= 0){	
								// $tmp->gw_extra_pt = round($tmp->gw_extra_pt + $gw_etp) ; 
								$tmp->month_pt += $tmp_fs ; //  $gw_etp + $tmp->three_cap_point + $tmp->gw_extra_pt - $tmp->dealer_card_minus_points; 
		
								if(!empty($tmp->overall_pt)){
									$tmp->overall_pt += $tmp_fs;  // add the player point to the overall point of this gameweek 
								}else{
									// $last_overall_pt = UserTeamsMonthPTTrack::where('team_id', $tt_val)->where('overall_pt', '>', 0)->orderBy('id', 'DESC')->first();
									// we will get overall point from the gameweek table if overall point is not  
									$last_overall_pt = UserTeamsGWExtraPTTrack::where('team_id', $tt_val)->where('overall_pt', '>', 0)->orderBy('id', 'DESC')->first(); 
									if(!empty($last_overall_pt)){
										$tmp->overall_pt = $last_overall_pt->overall_pt ; // + $tmp_fs; //  store the overall point for this gw by adding the current gw point into the previous overall point , we are not adding tmp_fs because it is already added in above gw
									}else{
										$tmp->overall_pt = $tmp_fs; // store the overall point same as gameweek if current gameweek is the first gameweek to be stored.
									}
								}


							}

			

							// $tmp->gw_player_points = !empty($gw_player_points_arr) ? @serialize($gw_player_points_arr) : null; 
							$tmp->save(); 

						}else{

							$tmp = new UserTeamsMonthPTTrack; 
							$tmp->team_id = $tt_val ; 
							if($tmp_fs >= 0){	
								// $tmp->gw_extra_pt = round($tmp->gw_extra_pt + $gw_etp) ; 
								$tmp->month_pt += $tmp_fs ; //  $gw_etp + $tmp->three_cap_point + $tmp->gw_extra_pt - $tmp->dealer_card_minus_points; 

								// $last_overall_pt = UserTeamsMonthPTTrack::where('team_id', $tt_val)->where('overall_pt', '>', 0)->orderBy('id', 'DESC')->first(); 
								$last_overall_pt = UserTeamsGWExtraPTTrack::where('team_id', $tt_val)->where('overall_pt', '>', 0)->orderBy('id', 'DESC')->first(); 
								if(!empty($last_overall_pt)){
									$tmp->overall_pt = $last_overall_pt->overall_pt ; // + $tmp_fs; //  store the overall point for this gw by adding the current gw point into the previous overall point , we are not adding tmp_fs because it is already added in above gw
								}else{
									$tmp->overall_pt = $tmp_fs; // store the overall point same as gameweek if current gameweek is the first gameweek to be stored.
								}
							}



							// $tmp->gw_player_points = !empty($gw_player_points_arr) ? serialize($gw_player_points_arr) : null; 
							$tmp->month_end_date = Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay() ; 
							$tmp->save(); 

						}

					// // update UserTeamsGWExtraPTTrack table end 










	        $player_list = UserTeamPlayers::where('team_id', $tt_val)->pluck('player_id')->toArray(); 

		        // echo 'team player'; 
		        // print_r($player_list); 

		        $all_team_pl_pr_sum = 0 ; 
		        $all_team_pl_players_points = array() ; 
		        $all_team_pl_players_price = array() ; 


		        $all_team_pl_players_points = FixtureScorcard::whereIn('player_id', $player_list)->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray(); 


				$temp_12th_pl = 0; 
				if(!empty($tmp_team_data) && !empty($tmp_team_data->twelveth_man_id) ){
					$temp_12th_pl = $tmp_team_data->twelveth_man_id; 
				}


		        foreach ($player_list as $kp => $vp) {
		            $temp_player = Player::where('id', $vp)->first(); 
		            if(!empty($temp_player)){
						
						if($temp_12th_pl != $vp) // remove 12th man price if present
					        $all_team_pl_pr_sum += $temp_player->svalue ; 
			            $all_team_pl_players_price[$vp] = $temp_player->svalue ; 
			            $all_team_pl_players_points[$vp] = isset($all_team_pl_players_points[$vp]) ? $all_team_pl_players_points[$vp] : 0 ; 
		            }

		        }







		        $utvl = new UserTeamValueLog; 
		        $utvl->team_id = $tmp_team_data->id; 
		        $utvl->team_value = $all_team_pl_pr_sum; 
		        $utvl->team_player = serialize($player_list); 
		        $utvl->capton_id = $tmp_team_data->capton_id; 
		        $utvl->v_capton_id = $tmp_team_data->vice_capton_id; 
		        $utvl->tot_team_point = $tmp_team_data->total_team_point; 
		        $utvl->players_points = serialize($all_team_pl_players_points) ; 
		        $utvl->players_price = serialize($all_team_pl_players_price) ; 
		        $utvl->team_rank = isset($team_ranks_arr[$tmp_team_data->id]) ? $team_ranks_arr[$tmp_team_data->id] : 0; 
		        $utvl->save(); 

		        // echo 'utvl'; 

		        // print_r($utvl); 



		        // if $cur_fixture_data->start_date is of the past gameweek, In that case we need to update the overall points of all the future gameweek and month    start 

		          
if(UserTeamsGWExtraPTTrack::where('team_id', $tt_val)->where('gw_end_date', '>', Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay()->addDay())->exists()){
  // for gw
$future_gw_data = UserTeamsGWExtraPTTrack::where('team_id', $tt_val)->where('gw_end_date', '>', Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay()->addDay())->get();
	foreach ($future_gw_data as $fgd_ky => $fgd_val) {
		$fgd_val->overall_pt += $tmp_fs; 

		$fgd_val->save(); 


	// $club_val = $cur_fixture_data->club; 

	// 	$temp_all_team_arr = UserTeams::where('club_id', $club_val)->pluck('id')->toArray();

	    $temp = UserTeamsGWExtraPTTrack::whereIn('team_id', $temp_all_team_arr)->where('gw_end_date' , Carbon::parse($fgd_val->gw_end_date)->endOfDay())->orderBy('overall_pt', 'DESC')->orderBy('id', 'ASC')->pluck('overall_pt', 'id')->toArray(); 

		$i = 1; 
		foreach ($temp as $tk => $tv) {
			// $temp[$key] = $i++ ; 
			// update overall rank in the table 
			if($tv > 0)
				UserTeamsGWExtraPTTrack::where('id', $tk)->where('gw_end_date' , Carbon::parse($fgd_val->gw_end_date)->endOfDay())->update(['overall_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}


	}

}


// for month

if(UserTeamsMonthPTTrack::where('team_id', $tt_val)->where('month_end_date', '>', Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay()->addDay())->exists()){

$future_m_data = UserTeamsMonthPTTrack::where('team_id', $tt_val)->where('month_end_date', '>', Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay()->addDay())->get();
	
	foreach ($future_m_data as $fmd_ky => $fmd_val) {

		$fmd_val->overall_pt += $tmp_fs; 

		$fmd_val->save(); 

	// $club_val = $cur_fixture_data->club; 

	// 	$temp_all_team_arr = UserTeams::where('club_id', $club_val)->pluck('id')->toArray();

  $temp = UserTeamsMonthPTTrack::whereIn('team_id', $temp_all_team_arr)->where('month_end_date' , Carbon::parse($fmd_val->month_end_date)->endOfDay())->orderBy('overall_pt', 'DESC')->orderBy('id', 'ASC')->pluck('overall_pt', 'id')->toArray(); 

		$i = 1; 
		foreach ($temp as $tk => $tv) {
			// $temp[$key] = $i++ ; 
			// update gw rank in the table 
			if($tv > 0)
				UserTeamsMonthPTTrack::where('id', $tk)->where('month_end_date' , Carbon::parse($fmd_val->month_end_date)->endOfDay())->update(['overall_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}


	}

}



		        // if $cur_fixture_data->start_date is of the past gameweek, In that case we need to update the overall points of all the future gameweek and month    end 




		}
		
	}






	// update rank of the team in this club in the UserTeamsGWExtraPTTrack table  start 


	// $club_val = $cur_fixture_data->club; 

	// 	$temp_all_team_arr = UserTeams::where('club_id', $club_val)->pluck('id')->toArray();

	    $temp = UserTeamsGWExtraPTTrack::whereIn('team_id', $temp_all_team_arr)->where('gw_end_date' , Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay())->orderBy('overall_pt', 'DESC')->orderBy('id', 'ASC')->pluck('overall_pt', 'id')->toArray(); 

		$i = 1; 
		foreach ($temp as $key => $value) {
			// $temp[$key] = $i++ ; 
			// update overall rank in the table 
			if($value > 0)
				UserTeamsGWExtraPTTrack::where('id', $key)->where('gw_end_date' , Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay())->update(['overall_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}
		// $team_overall_rank_arr = $temp; 
	   // update gameweek rank 
	    $temp = UserTeamsGWExtraPTTrack::whereIn('team_id', $temp_all_team_arr)->where('gw_end_date' , Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay())->orderBy('gw_pt', 'DESC')->orderBy('id', 'ASC')->pluck('gw_pt', 'id')->toArray(); 

		$i = 1; 
		foreach ($temp as $key => $value) {
			// $temp[$key] = $i++ ; 
			// update gw rank in the table 
			if($value > 0)
				UserTeamsGWExtraPTTrack::where('id', $key)->where('gw_end_date' , Carbon::parse($cur_fixture_data->start_date)->endOfWeek()->endOfDay())->update(['gw_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}


	// $team_gw_rank_arr = $temp; 



		// update rank of the team in the UserTeamsMonthPTTrack table 


	    $temp = UserTeamsMonthPTTrack::whereIn('team_id', $temp_all_team_arr)->where('month_end_date' , Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay())->orderBy('overall_pt', 'DESC')->orderBy('id', 'ASC')->pluck('overall_pt', 'id')->toArray(); 

		$i = 1; 
		foreach ($temp as $key => $value) {
			// $temp[$key] = $i++ ; 
			// update overall rank in the table 
			if($value > 0)
				UserTeamsMonthPTTrack::where('id', $key)->where('month_end_date' , Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay())->update(['overall_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}
		// $team_overall_rank_arr = $temp; 
	   // update gameweek rank 
	    $temp = UserTeamsMonthPTTrack::whereIn('team_id', $temp_all_team_arr)->where('month_end_date' , Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay())->orderBy('month_pt', 'DESC')->orderBy('id', 'ASC')->pluck('month_pt', 'id')->toArray(); 

		$i = 1; 
		foreach ($temp as $key => $value) {
			// $temp[$key] = $i++ ; 
			// update gw rank in the table 
			if($value > 0)
				UserTeamsMonthPTTrack::where('id', $key)->where('month_end_date' , Carbon::parse($cur_fixture_data->start_date)->endOfMonth()->endOfDay())->update(['month_rank'=> $i++]) ; // point in descending order so rank will be in ascending order 
		}








	// update rank of the team in this club end 



	 // code to update the gameweek point end 



	/* 

	// code commented on 10-02-20 (this code was updating the total team point in the database but now we will calculate points from the gameweek table ) 


	   foreach ($temp_all_team as $k2 => $v2) {
		    	// if(in_array($v2, $check_team_arr)) continue;
		    	// $check_team_arr[] = $v2; 
		        $temp_tm = UserTeams::find($v2); 
		        // $temp_tm->total_team_point += $temp_sum; 
		        // $temp_tm->save(); 
		        // echo 'team updated'; 
		        // print_r($temp_tm); 
		        $player_list = UserTeamPlayers::where('team_id', $temp_tm->id)->pluck('player_id')->toArray(); 

		        // echo 'team player'; 
		        // print_r($player_list); 

		        $all_team_pl_pr_sum = 0 ; 
		        $all_team_pl_players_points = array() ; 
		        $all_team_pl_players_price = array() ; 


		        $all_team_pl_players_points = FixtureScorcard::whereIn('player_id', $player_list)->groupBy('player_id')->selectRaw('sum(fantasy_points) as sum, player_id')->pluck('sum', 'player_id')->toArray(); 


				$temp_12th_pl = 0; 
				if(!empty($temp_tm) && !empty($temp_tm->twelveth_man_id) ){
					$temp_12th_pl = $temp_tm->twelveth_man_id; 
				}


		        foreach ($player_list as $kp => $vp) {
		            $temp_player = Player::where('id', $vp)->first(); 
		            if(!empty($temp_player)){
						
						if($temp_12th_pl != $vp) // remove 12th man price if present
					        $all_team_pl_pr_sum += $temp_player->svalue ; 
			            $all_team_pl_players_price[$vp] = $temp_player->svalue ; 
			            $all_team_pl_players_points[$vp] = isset($all_team_pl_players_points[$vp]) ? $all_team_pl_players_points[$vp] : 0 ; 
		            }

		        }







		        $utvl = new UserTeamValueLog; 
		        $utvl->team_id = $temp_tm->id; 
		        $utvl->team_value = $all_team_pl_pr_sum; 
		        $utvl->team_player = serialize($player_list); 
		        $utvl->capton_id = $temp_tm->capton_id; 
		        $utvl->v_capton_id = $temp_tm->vice_capton_id; 
		        $utvl->tot_team_point = $temp_tm->total_team_point; 
		        $utvl->players_points = serialize($all_team_pl_players_points) ; 
		        $utvl->players_price = serialize($all_team_pl_players_price) ; 
		        $utvl->team_rank = isset($team_ranks_arr[$temp_tm->id]) ? $team_ranks_arr[$temp_tm->id] : 0; 
		        $utvl->save(); 

		        // echo 'utvl'; 

		        // print_r($utvl); 








		        // all related team value save start 





				$user_terms = $temp_tm ; // assign current team  // UserTeams::find($user_terms->id); 


				$user_team_player = UserTeamPlayers::where('team_id', $user_terms->id)->get(); 

				// dump($value->id); 
				// dump($user_team_player); 
				// dump($value->capton_id); 
				// dump($user_team_player); die; 
				$tmp_top_pnt = 0 ; 
				foreach ($user_team_player as $k1 => $v1) {



					$vtemp = UserTeamsPlayerTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->whereNull('remove_date')->first(); 

					if(!empty($vtemp)){
						$v1 = $vtemp ; 
					}else{
						$utp_vtemp = UserTeamPlayers::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->first(); 
						if(!empty($utp_vtemp)){
							$v1 = $utp_vtemp; 
						}

					}







				// echo 'hi'; 
				// dump($v1); die; 

				// $tmp_top_pnt += $tmp_pnt ; 

					// dump($v1->player_id); 
					if($v1->player_id == $user_terms->capton_id){

						$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 1)->whereNull('remove_date')->first(); 
						if(!empty($utcvct)){
							$tmp_strt_dt = $utcvct->created_at ; 
						}else{
							// echo 'hi1'; die; 
							$tmp_strt_dt = $v1->created_at ; 
						}
						// echo 'hi'; 
						// dump(Carbon::parse($tmp_strt_dt)->endOfDay()); 

						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)




					// if 12th man then change the date accordingly

					if($v1->player_id == $user_terms->twelveth_man_id && !empty($user_terms->twelve_man_card_last_date)){
						$tmp_strt_dt = $user_terms->twelve_man_card_last_date ; 
					}




					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

							->whereHas('fixture', function($q) use ($tmp_strt_dt){
												$q->where('start_date', '>', Carbon::parse($tmp_strt_dt));
											})

					// ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)







						// dump($tmp_pnt); 
						// echo 'hello'; 
						$tmp_top_pnt += round($tmp_pnt) ; 
					}

					if($v1->player_id == $user_terms->vice_capton_id){


						$utcvct = UserTeamsCVCTrack::where('player_id', $v1->player_id)->where('team_id', $v1->team_id)->where('c_vc', 0)->whereNull('remove_date')->first(); 
						if(!empty($utcvct)){
							$tmp_strt_dt = $utcvct->created_at ; 
						}else{
							// echo 'hi2'; die; 
							$tmp_strt_dt = $v1->created_at ; 
						}


					// if 12th man then change the date accordingly
					if($v1->player_id == $user_terms->twelveth_man_id && !empty($user_terms->twelve_man_card_last_date)){
						$tmp_strt_dt = $user_terms->twelve_man_card_last_date ; 
					}


						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)

					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

						->whereHas('fixture', function($q) use ($tmp_strt_dt){
																		$q->where('start_date', '>', Carbon::parse($tmp_strt_dt));
																	})
					// ->where('created_at', '>=', Carbon::parse($tmp_strt_dt)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)
















						$tmp_top_pnt += round($tmp_pnt * 0.5) ; 
					}


						// $tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())->sum('fantasy_points'); // ->where('status', 3)




					$tmp_strt_dt = $v1->created_at; 

					// if 12th man then change the date accordingly
					if($v1->player_id == $user_terms->twelveth_man_id && !empty($user_terms->twelve_man_card_last_date)){
						$tmp_strt_dt = $user_terms->twelve_man_card_last_date ; 
					}





					$tmp_pnt = FixtureScorcard::where('player_id', $v1->player_id)

						->whereHas('fixture', function($q) use ($tmp_strt_dt){
									$q->where('start_date', '>', Carbon::parse($tmp_strt_dt));
								})
					// ->where('created_at', '>=', Carbon::parse($v1->created_at)->endOfDay())

					->sum('fantasy_points'); // ->where('status', 3)


					// print_r($v1->player_id); 
					// print_r($v1->created_at); 
					// print_r($tmp_pnt); 




						$tmp_top_pnt += $tmp_pnt ; 



					// dump($tmp_top_pnt); 



					}


					$user_terms->total_team_point = round($user_terms->extra_team_point + $tmp_top_pnt + $user_terms->three_cap_point) - $user_terms->dealer_card_minus_points;  // save extra team point + player 11 point + captain cap booster extra point - dealer card booster negative point

					$user_terms->save(); 





		        // all related team value save end












	    }


	    */ 



	// update the UserTeamValueLog and UserTeams table ************ end 



		if($flg){
			Session::flash('flash_notice', trans("Player price successfully updated."));
		}else{
			Session::flash('flash_notice', trans("Player price has already updated for this fixture."));
		}

		return 1;

	}





	public function getClubTeamList(){ 

		$club = Input::get('id');

		$team_id = Input::get('team_id');

		$team = new Team;

		$getClubTeam = $team->get_team_by_club($club);



		// print_r($getClubTeam); die; 

		return View::make('admin.availabilities.get_club_team_list',compact('getClubTeam','team_id'));

	}





	public function getClubModeGrade(){

		$club = Input::get('id');

		$grade = new Grade;

		$getGradeList = $grade->get_grade($club);



		// print_r($getClubTeam); die; 

		return View::make('admin.availabilities.get_club_grade_list',compact('getGradeList'));		

	}

	public function getTeamGrade(){ 
		$gradeId = Input::get('id');
		$team_id = Input::get('team_id');
		if(!empty($gradeId)){
			$teamLists = Team::where('grade_name',$gradeId)->pluck('name','id')->all();
			return View::make('admin.fixture.get_team_grade',compact('teamLists','team_id'));
		}
	}

	public function showScorecard($fixture_id=null){ 

		$team_id = null; 
		$status = FixtureScorcard::where('fixture_id',$fixture_id)->orderBy('created_at','DESC')->value('status');
		$teamPlayerCount = TeamPlayer::where('fixture_id',$fixture_id)->count();
		$fixtureScorecards = FixtureScorcard::where('fixture_id',$fixture_id)->count(); 
		if($teamPlayerCount < 7){
			$needToAdd = (7 - $teamPlayerCount);
			Session::flash('flash_notice', trans("Please add $needToAdd more player to add scorecard.")); 
			return Redirect::to('admin/fixture');
		}if($fixtureScorecards < 7){
			FixtureScorcard::where('fixture_id',$fixture_id)->delete();
		}

		$DB 					= 	FixtureScorcard::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);
			if(isset($searchData['order'])){

				unset($searchData['order']);

			}

			if(isset($searchData['sortBy'])){

				unset($searchData['sortBy']);

			}
			if(isset($searchData['page'])){

				unset($searchData['page']);

			}

			$date_from	=	'';

			$date_to	=	'';
			foreach($searchData as $fieldName => $fieldValue){

				if(!empty($fieldValue) || $fieldValue==0){

					$DB->where("fixture_scorecards.$fieldName",'like','%'.$fieldValue.'%');

				}

				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));

			} 

		}

		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'fixture_scorecards.created_at';

	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';
	    $fixtureData = Fixture::where('fixtures.id',$fixture_id)
						->leftJoin('grades','grades.id','=','fixtures.grade')
						->leftJoin('teams','teams.id','=','fixtures.team')
						->select('fixtures.*','grades.grade as grade','teams.name as team_name','teams.id as team_id')
						->first();
	    $scoreData = FixtureScorcard::where(['fixture_id'=>$fixture_id])->count();
	    $getTeamPlayer = TeamPlayer::where('fixture_id',$fixture_id)->select('player_id')->get();
	    if($scoreData==0){ 
	    	if(!$getTeamPlayer->isEmpty()){
	    		if($fixtureData->match_type == ONEDAYMATCH){
	    			foreach ($getTeamPlayer as $key => $value) { 

		    			$obj = new  FixtureScorcard;

				    	$obj->player_id = $value->player_id;

				    	$obj->fixture_id =$fixture_id ;

				    	$obj->inning = 1;

				    	$obj->save();

		    		}
	    		}else{
	    			foreach ($getTeamPlayer as $key => $value) { 

		    			$obj = new  FixtureScorcard;

				    	$obj->player_id = $value->player_id;

				    	$obj->fixture_id =$fixture_id ;

				    	$obj->inning = 1;

				    	$obj->save();

		    		}
		    		foreach ($getTeamPlayer as $key => $value) { 

		    			$obj = new  FixtureScorcard;

				    	$obj->player_id = $value->player_id;

				    	$obj->fixture_id =$fixture_id ;

				    	$obj->inning = 2;

				    	$obj->save();

		    		}
	    		}
	    	}

	    }else{

	    }
		$result = 	$DB->leftjoin('players','fixture_scorecards.player_id','=','players.id')
						->select('fixture_scorecards.*','players.full_name as player_name','players.position as player_position')

						->where('fixture_scorecards.fixture_id',$fixture_id)

						->orderBy($sortBy, $order)

						->paginate(40);
		$playerScorcards = FixtureScorcard::where('fixture_id',$fixture_id)->where('player_card','!=','')->value('player_card');
		$complete_string		=	Input::query();

		unset($complete_string["sortBy"]);

		unset($complete_string["order"]);

		$query_string			=	http_build_query($complete_string);

		$result->appends(Input::all())->render();
		$playerImages = FixtureScorcard::where('fixture_id',$fixture_id)->where('player_card','!=','')->first();
		return  View::make('admin.fixture.show_scorecard', compact('result','searchVariable','sortBy','order','query_string','fixture_id','team_id','fixtureData','status','playerImages','playerScorcards'));

	}

	public function showSquad($fixture_id=null){ 
		$clubId = Fixture::where('id',$fixture_id)->value('club');
		$DB 					= 	TeamPlayer::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			} 
			$date_from	=	'';
			$date_to	=	'';
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'team_players.created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';

	    if(Auth::guard('admin')->user()->user_role_id == 1){
			$result = 	$DB->leftJoin('players' , 'team_players.player_id' , '=' , 'players.id')
							->select('team_players.*','players.full_name as player_name')
							->where('fixture_id',$fixture_id)
							->orderBy($sortBy, $order)
							->paginate(15);
		}else{
			$result = 	$DB
						->leftJoin('players' , 'team_players.player_id' , '=' , 'players.id')
						->select('team_players.*','players.full_name as player_name')
						->where('fixture_id',$fixture_id)
						->orderBy($sortBy, $order)
						->paginate(15);
		}

		$is_lock = TeamPlayer::where('fixture_id',$fixture_id)->orderBy('id','DESC')->value('is_lock');
		$playerCount = TeamPlayer::where('fixture_id',$fixture_id)->count();
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		$club =	new Club();
		$cludDetails =	$club->get_club_list();
 
		$player	=	new Player();
		$playerlist	=	$player->get_players_by_club($clubId);
		$playerIds = TeamPlayer::where('fixture_id',$fixture_id)->pluck('player_id','player_id')->all();
		$fixtureDetails = Fixture::where('fixtures.id',$fixture_id)
								->leftJoin('teams' , 'fixtures.team' , '=' , 'teams.id')
								->leftJoin('grades as grades' , 'fixtures.grade' , '=' , 'grades.id')
								->leftJoin('dropdown_managers as dropdown_managers2' , 'fixtures.match_type' , '=' , 'dropdown_managers2.id')
								->select('grades.grade','teams.name  as team_name','fixtures.start_date','fixtures.start_date','fixtures.end_date','dropdown_managers2.name  as match_type')
								->first();
		return  View::make('admin.fixture.show_squad', compact('result' ,'searchVariable','sortBy','order','userType','query_string','gradeName','teamType','teamCategory','cludDetails','fixture_id','playerlist','playerIds','is_lock','playerCount','fixtureDetails'));
	 }	

	public function getTeam(){
		$gameName = Input::get('game_name');
		$teamId = Input::get('team_id');
		$clubId = User::where('game_name',$gameName)->value('id');
		$teamList =	Team::where('club',$clubId)->pluck('name','id')->all();
		return  View::make('admin.fixture.get_team', compact('teamList','teamId'));
	}
	public function getGrade(){
		$gameName = Input::get('game_name');
		$teamId = Input::get('team_id');
		$clubId = User::where('game_name',$gameName)->value('id');
		$teamList =	Team::where('club',$clubId)->pluck('name','id')->all();
		return  View::make('admin.fixture.get_team', compact('teamList','teamId'));
	}
}// end ClubController class
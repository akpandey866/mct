<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\NewsLetter;
use App\Model\NewsLetterTemplate;
use App\Model\NewsLettersubscriber;
use App\Model\Subscriber;
use App\Model\User;
use App\Model\EmailAction;
use App\Model\EmailTemplate;
use mjanssen\BreadcrumbsBundle\Breadcrumbs as Breadcrumb;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;

/**
 * NewsLetter Controller
 *
 * Add your methods in the class below
 *
 * This file will render views from views/admin/newsletter
 */
 
class NewsLetterController extends BaseController {

	
/**
 * Function for display all newslatter template
 *
 * @param null
 *
 * @return view page. 
 */
	public function listTemplate() { 
					
		$DB	=	NewsLetter::query();
		$searchVariable	=	array(); 
		$inputGet		=	Input::get();
		
		if ((Input::get() && isset($inputGet['display'])) || isset($inputGet['page']) ) {
			$searchData	=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);
			
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue)){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
					$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
				}
			}
		}
		
		
		$sortBy = (Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  = (Input::get('order')) ? Input::get('order')   : 'DESC';
		
		$result = $DB->orderBy($sortBy, $order)->paginate(Config::get("Reading.records_per_page"));
		
		return  View::make('admin.newsletter.index',compact('breadcrumbs','result','searchVariable','sortBy','order'));
	}// end listTemplate()
	
/**
 * Function for display page  for edit newslatter template
 *
 * @param null
 *
 * @return view page. 
 */
	function editTemplate($Id){		
		$result						=	Newsletter::find($Id);
		$newsletter_template_id		=	$result->newsletter_template_id;
		
		$allReadySubscriberArray	=	NewsLettersubscriber::
										where('status', '=', 1)->
										whereIn('id',
								function($query) use ($newsletter_template_id)
									{
										$query->select('newsletter_subscriber_id')
											  ->from('subscribers')
											  ->whereRaw('subscribers.newsletter_id = '.$newsletter_template_id);
									})->
										pluck('id','id')->all(); 
		$subscriberArray			=	NewsLettersubscriber::where('status', '=', 1)->pluck('email','id')->all();
		return  View::make('admin.newsletter.edit',compact('result','subscriberArray','allReadySubscriberArray'));
	}//end editTemplate()
	
/**
 * Function for save updated newslatter template
 *
 * @param $Id as id of template 
 *
 * @return redirect page. 
 */
	function updateTemplate($Id){
		$validator = Validator::make(
			Input::all(),
			array(
				'scheduled_time' 	=> 'required',
				'subject' 			=> 'required',
				'newsletter_subscriber_id' 	=> 'required',
				'body' 				=> 'required'
			),array('newsletter_subscriber_id.required' => 'The newsletter subscribers field is required.')
		);
		
		if ($validator->fails())
		{	
			return Redirect::to('admin/news-letter/edit-template/'.$Id)
				->withErrors($validator)->withInput();
		}else{
			
			$subscriberArray	=	NewsLettersubscriber::where('status', '=', 1)->pluck('email','id'); 
			
			NewsLetter::where('id','=',$Id)
				->update(array(
					'scheduled_time'  		 => 	Input::get('scheduled_time'),
					'subject'  				 => 	Input::get('subject'),
					'body' 	   				 => 	Input::get('body'),
					'newsletter_template_id' => 	$Id,
					'status'				 => 	0
				));
			
			Subscriber::where('newsletter_id', '=', $Id)->delete();
			if(Input::get('subscribers') == MANUAL){
				if(Input::get('newsletter_subscriber_id') == ''){
					 foreach($subscriberArray as $to =>$email){
						Subscriber::insert(
							array(
								'newsletter_subscriber_id' =>  $to,
								'newsletter_id' =>  $Id
							));
					}
				}else{
					foreach(Input::get('newsletter_subscriber_id') as $to){
						Subscriber::insert(
							array(
								'newsletter_subscriber_id' =>  $to,
								'newsletter_id' =>  $Id
							));
					}   
				}
			}
			Session::flash('flash_notice',trans("Newsletter has been updated successfully.")); 
			return Redirect::to('admin/news-letter');
		}
	}// end updateTemplate()
	
/**
 * Function for display all newslatter template
 *
 * @param null
 *
 * @return view page. 
 */
	function newsletterTemplates(){	
		$DB				=	NewsLetterTemplate::query();
		$searchVariable	=	array(); 
		$inputGet		=	Input::get();
		
		if ((Input::get() && isset($inputGet['display'])) || isset($inputGet['page']) ) {
			$searchData	=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);
			
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue)){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
					$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
				}
			}
		}
		
		
		$sortBy = (Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  = (Input::get('order')) ? Input::get('order')   : 'DESC';
		
		$result = $DB->orderBy($sortBy, $order)->paginate(Config::get("Reading.records_per_page"));
		
		return  View::make('admin.newsletter.newsletter_templates',compact('breadcrumbs','result','searchVariable','sortBy','order'));
	}// end newsletterTemplates()
	
/**
 * Function for display page for add new newslatter template
 *
 * @param null
 *
 * @return view page. 
 */
	function addTemplates(){
		return  View::make('admin.newsletter.add_newsletter_templates',compact('breadcrumbs'));
	}// end addTemplates()
	
/**
 * Function for save created template
 *
 * @param null
 *
 * @return redirect page. 
 */
	function saveTemplates(){
		$validator = Validator::make(
			Input::all(),
			array(
				'subject' 	=> 'required',
				'body' 		=> 'required'
			)
		);
		
		if ($validator->fails())
		{	
			return Redirect::to('admin/news-letter/add-template')
				->withErrors($validator)->withInput();
		}else{
		
			NewsletterTemplate::insert(array(
					'subject'  		=> 	Input::get('subject'),
					'body' 	   		=> 	Input::get('body'),
					'created_at' 	=>	 DB::raw('NOW()'),
					'updated_at' 	=>	 DB::raw('NOW()')
				));
			
			Session::flash('flash_notice', trans("Your Newsletter Template has been saved successfully.")); 
			return Redirect::to('admin/news-letter/newsletter-templates');
		}
	}
	
/**
 * Function for display page for edit newslatter template
 *
 * @param $Id as id of newslatter
 *
 * @return view page. 
 */
	function editNewsletterTemplate($Id){		
		$result		    =	NewsletterTemplate::find($Id);
		return  View::make('admin.newsletter.edit_newsletter_templates',compact('result'));
	}// end editNewsletterTemplate()
	
/**
 * Function for save updated newslatter
 *
 * @param $Id as id of newslatter
 *
 * @return redirect page. 
 */
	function updateNewsletterTemplate($Id){
		$validator = Validator::make(
			Input::all(),
			array(
				'subject' 	=> 'required',
				'body' 		=> 'required'
			)
		);
		
		if ($validator->fails())
		{	
			return Redirect::to('admin/news-letter/edit-newsletter-templates/'.$Id)
				->withErrors($validator)->withInput();
		}else{
		
				NewsletterTemplate::where('id', $Id)
				->update(array(
					'subject'  		=> 	Input::get('subject'),
					'body' 	   		=> 	Input::get('body'),
					'updated_at' 	=> DB::raw('NOW()')
				));
			
			Session::flash('flash_notice',trans("Newsletter Template has been updated successfully.")); 
			return Redirect::to('admin/news-letter/newsletter-templates');
		}
	}//end updateNewsletterTemplate()
	
/**
 * Function for send newslatter template
 *
 * @param $Id as id of newslatter
 *
 * @return view page. 
 */
	function sendNewsletterTemplate($Id){		
		$subscriberArray	=	NewsLettersubscriber::where('status', '=', 1)->pluck('email','id'); 
		$result				=	NewsletterTemplate::find($Id);
		return  View::make('admin.newsletter.send_newsletter_templates',compact('result','subscriberArray'));
	}// end sendNewsletterTemplate()
	
/**
 * Function for update send newslatter
 *
 * @param $Id as id of newslatter
 *
 * @return redirect page. 
 */
	function updateSendNewsletterTemplate($Id){
		$formdata = Input::all();
		$settingsEmail =  Config::get('Site.email');
		$constants = array();
        $cons = Config::get('newsletter_template_constant'); 
		foreach($cons as $key=>$val){
			$constants[] = '{'.$val.'}';
		}
		$website_url_path  =	URL::to('/');
		$website_url       = 	'<a style="text-decoration:none;" href="'.$website_url_path.'">'.Config::get("Site.title").'</a>';
		$newsletterSubject = NewsLetter::where('id',$Id)->first();
		//prd($newsletterSubject);
		$validator = Validator::make(
			Input::all(),
			array(
				//'scheduled_time' 	=> 'required',
				'subject' 			=> 'required',
				'body' 				=> 'required'
			)
		);
		
		if ($validator->fails())
		{	
			return Redirect::to('admin/news-letter/send-newsletter-templates/'.$Id)
				->withErrors($validator)->withInput();
		}else{
			 $newsLetterInsertId			=	NewsLetter::insertGetId(array(
					'scheduled_time'  		 => 	Input::get('scheduled_time'),
					'subject'  				 => 	Input::get('subject'),
					'body' 	   				 => 	Input::get('body'),
					'subscribe_type'		=>		Input::get('subscribers'),
					'newsletter_template_id' => 	$Id,
					'created_at' => 	date('Y-m-d H:i').':00',
					'updated_at' => 	date('Y-m-d H:i').':00',
					'scheduled_time' => date('Y-m-d H:i').':00',
					'status'				 => 	0
				)); 
			if(Input::get('subscribers') == MANUAL){
				if(Input::get('newsletter_subscriber_id') == ''){  
					$subscriberArray = NewsLettersubscriber::where('status', '=', 1)->get(); 
					
					 foreach($subscriberArray as $to =>$value){
						Subscriber::insert(
						array( 
							'newsletter_subscriber_id' =>  $value->id,
							'newsletter_id' =>  $newsLetterInsertId,
							'created_at' => 	date('Y-m-d H:i').':00',
							'updated_at' => 	date('Y-m-d H:i').':00',
						));

						$to						 =	 $value->email;
						$from					 =	 $settingsEmail;
						$replyTo			     =   '';
						$subject				 =	 $newsletterSubject->subject;
						$enc_id			 		 =	 $value->enc_id;
						$route_url      		 =   URL::to('unsubscribe/'.$enc_id);
						$unsubscribe_link        = 	'<a style="text-decoration:none;" href="'.$route_url.'">click here</a>';
						$rep_Array               =   array($value->name,$to,$website_url,$unsubscribe_link); 
						$message 			     =   str_replace($constants, $rep_Array, $newsletterSubject->body);	
						$this->sendMail($to,'Admin',$subject,$message,$from);
					}
				}else{ 
					$subscriberIds = NewsLettersubscriber::whereIn('id',Input::get('newsletter_subscriber_id'))->where('status', '=', 1)->get(); 
					foreach($subscriberIds as $value){
						Subscriber::insert(
						array(
							'newsletter_subscriber_id' =>  $value->id,
							'newsletter_id' =>  $Id,
							'created_at' => 	date('Y-m-d H:i').':00',
							'updated_at' => 	date('Y-m-d H:i').':00',
						)); 

						$to						 =	 $value->email;
						$from					 =	 $settingsEmail;
						$replyTo			     =   '';
						$subject				 =	 $newsletterSubject->subject;
						$enc_id			 		 =	 $value->enc_id;
						$route_url      		 =   URL::to('unsubscribe/'.$enc_id);
						$unsubscribe_link        = 	'<a style="text-decoration:none;" href="'.$route_url.'">click here</a>';
						$rep_Array               =   array($value->name,$to,$website_url,$unsubscribe_link); 
						$message 			     =   str_replace($constants, $rep_Array, $newsletterSubject->body);	
						$this->sendMail($to,'Admin',$subject,$message,$from);
					}
				}
			}
			
			if(Input::get('subscribers') == CLUBUSER){  
				$userLists = User::where(['user_role_id'=>CLUBUSER,'is_deleted'=>0,'is_subscribe'=>1,'is_active'=>1])->select('email','id')->get();
				if(!empty($userLists)){
					foreach ($userLists as $key => $value) {
						$userEncId = md5(time() . $value->email);
						User::where('id',$value->id)->update(['enc_id'=>$userEncId]);
						$to						 =	 $value->email;
						$from					 =	 $settingsEmail;
						$replyTo			     =   '';
						$subject				 =	 $newsletterSubject->subject;
						$enc_id			 		 =	 $userEncId;
						$route_url      		 =   URL::to('unsubscribe/'.$enc_id);
						$unsubscribe_link        = 	'<a style="text-decoration:none;" href="'.$route_url.'">click here</a>';
						$rep_Array               =   array($value->full_name,$to,$website_url,$unsubscribe_link); 
						$message 			     =   str_replace($constants, $rep_Array, $newsletterSubject->body);	
						$this->sendMail($to,'Admin',$subject,$message,$from);
					}
				}
			}
			if(Input::get('subscribers') == USER){
				$userLists = User::where(['user_role_id'=>USER,'is_deleted'=>0,'is_subscribe'=>1,'is_active'=>1])->select('email','id')->get();
				if(!empty($userLists)){
					foreach ($userLists as $key => $value) {
						$userEncId = md5(time() . $value->email);
						User::where('id',$value->id)->update(['enc_id'=>$userEncId]);
						$to						 =	 $value->email;
						$from					 =	 $settingsEmail;
						$replyTo			     =   '';
						$subject				 =	 $newsletterSubject->subject;
						$enc_id			 		 =	 $userEncId;
						$route_url      		 =   URL::to('unsubscribe/'.$userEncId);
						$unsubscribe_link        = 	'<a style="text-decoration:none;" href="'.$route_url.'">click here</a>';
						$rep_Array               =   array($value->full_name,$to,$website_url,$unsubscribe_link); 
						$message 			     =   str_replace($constants, $rep_Array, $newsletterSubject->body);	
						$this->sendMail($to,'Admin',$subject,$message,$from);
					}
				}
			}

			Session::flash('flash_notice', trans("Newsletter sent successfully") ); 
			return Redirect::to('admin/news-letter/newsletter-templates');
		}
	}//end updateSendNewsletterTemplate()
		
	public function sendNewsLetterMail($subscriber_list,$type){
		if(!empty($subscriber_list)){
			foreach($subscriber_list as $sub_list){
				$to						 =	 $sub_list->email;
				$from					 =	 $settingsEmail;
				$replyTo			     =   '';
				$subject				 =	 $newsletters->subject;
				$enc_id			 		 =	 $sub_list->enc_id;
				$route_url      		=  	URL::to('unsubscribe/'.$enc_id);
				$unsubscribe_link       = 	'<a style="text-decoration:none;" href="'.$route_url.'">click here</a>';
				$rep_Array               =   array($to,$website_url,$unsubscribe_link); 
				$message 			     =   str_replace($constants, $rep_Array, $newsletters->body);	
				$this->sendMail($to,'Admin',$subject,$message,$from);
			}
		}
	}
	
/**
 * Function for delete template
 *
 * @param $Id id of template
 *
 * @return redirect page. 
 */
	function templateDelete($Id){
		if($Id){
			$obj	=  Newsletter::find($Id);
			$obj->delete();
		}
		Session::flash('flash_notice', trans("Newsletter Template deleted successfully.") ); 
		return Redirect::to('admin/news-letter');
	}//end templateDelete()
	
/**
 * Function for delete news template
 *
 * @param $Id id of template
 *
 * @return redirect page. 
 */
	function deleteNewsTemplate($Id){
		if($Id){
			NewsletterTemplate::where('id' ,'=', $Id)->delete();
		}
		Session::flash('flash_notice', trans("Newsletter deleted successfully.")); 
		return Redirect::to('admin/news-letter/newsletter-templates');
	}//end deleteNewsTemplate()

/**
 * Function for display list of all newslatter subscriber
 *
 * @param null
 *
 * @return view page. 
 */
	function subscriberList(){ 		
		$DB				=	NewsLettersubscriber::query();
		$searchVariable	=	array(); 
		$inputGet		=	Input::get();
	
		if ((Input::get() || isset($inputGet['display'])) || isset($inputGet['page']) ) {
			$searchData	=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);
			
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue)){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
					$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
				}
			}
		}
		$sortBy = (Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  = (Input::get('order')) ? Input::get('order')   : 'DESC';
		
		$result = $DB->orderBy($sortBy, $order)->paginate(Config::get("Reading.records_per_page"));
		
		return  View::make('admin.newsletter.subscriber_list',compact('result','searchVariable','sortBy','order'));
	}// end subscriberList()
	
/**
 * Function for change status of subscriber
 *
 * @param $Id  as id of subscriber
 * @param $status  as status of subscriber
 *
 * @return redirect page. 
 */
	function subscriberActive($Id,$status){
		NewsLettersubscriber::where('id', '=', $Id)->update(array('status' => $status));
		Session::flash('flash_notice', trans("Status Changed Successfully.")); 		
		return Redirect::to('admin/news-letter/subscriber-list');
	}//end subscriberActive()
	
/**
 * Function for delete newslatter subscriber
 *
 * @param $Id as subscriber id
 *
 * @return redirect page. 
 */
	function subscriberDelete($Id){
		NewsLettersubscriber::where('id', '=', $Id)->delete();
		Session::flash('flash_notice', trans("messages.system_management.newslettersubscriber_deleted_successfully") ); 		
		return Redirect::to('admin/news-letter/subscriber-list');
	}//end subscriberDelete()


/**
 * Function for display list of all newslatter subscriber
 *
 * @param $Id id of template
 *
 * @return view page. 
 */
	function viewSubscrieber($id){
		
		
		$result						=	Newsletter::find($id);
		$newsletter_template_id		=	$result->newsletter_template_id;
		
		$result			=	NewsLettersubscriber::
								where('status', '=', 1)->
								whereIn('id',
								function($query) use ($newsletter_template_id)
									{
										$query->select('newsletter_subscriber_id')
											  ->from('subscribers')
											  ->whereRaw('subscribers.newsletter_id = '.$newsletter_template_id);
									})->
								lists('email','id'); 
		return  View::make('admin.newsletter.view_subscrieber',compact('result'));
	}//end viewSubscrieber()
	
/**
 * Function for add subscriber
 *
 * @param null
 * 
 * @return view page. 
 */
	public function addSubscriber(){		
		if(Request::isMethod('post')){
			
			$validator = Validator::make(
				Input::all(),
				array(
					'name' 		=> 'required',
					'email' 	=> 'required|email|unique:newsletter_subscribers',
				)
			);
		
			if ($validator->fails())
			{	
				return Redirect::back()
					->withErrors($validator)->withInput();
			}else{
				$encId			=	md5(time() . Input::get('email'));
				NewsLettersubscriber::insert(array(
						'email'	  		=>  Input::get('email'),
						'name'	  		=>  Input::get('name'),
						'is_verified' 	=>  1,
						'status' 		=>  1,
						'enc_id' 		=>  $encId,
						'created_at'    =>	DB::raw('NOW()'),
						'updated_at'	=>	DB::raw('NOW()')
					));
				Session::flash('flash_notice', trans("Subscriber add successfully.")); 
				return Redirect::to('admin/news-letter/subscriber-list');
			}
		}
		return  View::make('admin.newsletter.add_subscriber');
	}//end addSubscriber()
	


}// end NewsLetterController class

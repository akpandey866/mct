<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Forum;
use App\Model\ForumComment;
use App\Model\User;
use App\Model\Club;
use App\Model\DropDown;
use App\Model\LockoutLog;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use Carbon\Carbon;

class ClubController extends BaseController {
/**
* Function for display all event
*
* @param null
*
* @return view page.
*/
	public function index(){
		if(Auth::guard('admin')->user()->user_role_id != 1){
			return Redirect::to('admin/club/edit-club/'.Auth::guard('admin')->user()->id);
		}
		$DB 					= 	User::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			if((!empty($searchData['date_to']) && !empty($searchData['date_from']))){
				$date_from	=	$searchData['date_from'];
				$date_to	=	$searchData['date_to'];
				$DB->whereBetween('users.created_at', [$date_from." 00:00:00", $date_to." 23:59:59"]); 
					
			}else if(!empty($searchData['date_to'])){
				$date_to	=	$searchData['date_to'];
				$DB->whereBetween('users.created_at',	[$date_to." 00:00:00", $date_to." 23:59:59"]); 
				
			}else if(!empty($searchData['date_from'])){
				$date_from	=	$searchData['date_from'];
				$DB->whereBetween('users.created_at',	[$date_from." 00:00:00", $date_from." 23:59:59"]); 				
			}
			unset($searchData['date_to']);
			unset($searchData['date_from']);
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%'); 
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';

		$result 				= 	$DB
									->where('id','<>',ADMIN_ID)
									->where('user_role_id',3)
									->orderBy($sortBy, $order)
									->paginate(Config::get("Reading.records_per_page"));

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.clubs.index', compact('result' ,'searchVariable','sortBy','order','userType','query_string','user_role_id','date_from','date_to','totalUsers','totalActivatedUsers','totalDeactivatedUsers','currentYearTotalUsers'));
	 }

	/**
	* Function for add Forum
	*
	* @param null
	*
	* @return view page. 
	*/
	public function addClub(){
		if(Auth::guard('admin')->user()->user_role_id != 1){
			return Redirect::to('admin/club/edit-club/'.Auth::guard('admin')->user()->id);
		}
		//$drop_down				=	new DropDown();
		//$categories				=	$drop_down->get_master_list("forum-categories");
		$countryList		=	DB::table('countries')->orderBy('name','ASC')->pluck('name','id')->toArray();
		$countryCodeList = DB::table('countries')->pluck('phonecode','phonecode')->all();

	
		
		return  View::make('admin.clubs.add',compact('countryList','countryCodeList'));
	}//end addForum()
	
	
	public function getState($id = null) { 
		$stateList = DB::table('states')->where('country_id',$id)->pluck('name','id')->all();
		return  View::make('admin.clubs.state',compact("stateList"));
	}

	public function getStateEdit($country_id = null,$state_id = null) { 
		$stateList = DB::table('states')->where('country_id',$country_id)->pluck('name','id')->all();
		return  View::make('admin.clubs.state',compact("stateList",'state_id'));
	}

	public function getCity($id = null) { 
		$cityList = DB::table('cities')->where('state_id',$id)->pluck('name','id')->all();
		return  View::make('admin.clubs.city',compact("cityList"));
	}

	public function getCityEdit($state_id = null,$city_id = null) { 
		$cityList = DB::table('cities')->where('state_id',$state_id)->pluck('name','id')->all();
		return  View::make('admin.clubs.city',compact("cityList","city_id"));
	}

	public function getCountaryTimezone($id=null){
		$time = DB::table('countries')->where('id',$id)->pluck('TimeZone')->all();
		return $time[0];
	}
	/**
	* Function for save added Event
	*
	* @param null
	*
	* @return redirect page. 
	*/
	public function saveClub(){
	Input::replace($this->arrayStripTags(Input::all()));
	//$user_id	=	Auth::user()->id;
		$thisData			=	Input::all();
		// print_r($thisData); die; 
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'club_name'		=> 'required',
					'game_name' => 'required',
					'sport_name' => 'required',
					'username' => 'required|unique:users',
					'first_name'		=> 'required',
					'last_name' 		=> 'required',
					'dob' => 'required',
					'email' 			=> 'required|email|unique:users',
					'phone' 			=> 'required|regex:/^([0-9\s\-\+\(\)]*)$/|digits_between:8,15|unique:users',
					'gender' 			=> 'required',
					'club_logo' 		=> 'max:2048|mimes:'.IMAGE_EXTENSION,
					'timezone' => 'required'
					
				),
				array(
					//'image.dimensions' => "Please upload min width 221 px and min height 266 px image",
				)
			);
			if ($validator->fails()){
				 $errors 	=	$validator->messages();
					$response	=	array(
						'success' 	=> false,
						'errors' 	=> $errors
					);
					return Response::json($response); 
					die;
			}else{ 
					$fullName						=  ucwords(Input::get('first_name').' '.Input::get('last_name'));
					$obj 							=  new User;
					$obj->game_mode 				=  Input::get('game_mode');
					$obj->club_name 				=  Input::get('club_name');
					$obj->username 				=  Input::get('username');
					$obj->game_name 				=  Input::get('game_name');
					$obj->sport_name 				=  Input::get('sport_name');
					$obj->post_code 				=  !empty(Input::get('post_code')) ? Input::get('post_code'):'';
					$obj->email 					=  Input::get('email');
					$obj->first_name				=  ucfirst(Input::get('first_name'));
					$obj->last_name					=  ucfirst(Input::get('last_name'));
					$obj->full_name 				=  $fullName;
					$obj->email 					=  Input::get('email');
					$obj->phone 					=  !empty(Input::get('phone')) ? Input::get('phone'):'';
					//$obj->slug	 					=  $this->getSlug($fullName,'full_name','Club');
					$obj->password	 				=  Hash::make(Input::get('password'));
					$obj->user_role_id				=  3;
				
					$obj->dob						=  Input::get('dob');
					$obj->gender					=  Input::get('gender');
					$obj->facebook					=  Input::get('facebook');
					$obj->twitter					=  Input::get('twitter');
					$obj->instagram					=  Input::get('instagram');
					$obj->website					=  Input::get('website');


					$obj->country					=  !empty(Input::get('country')) ? Input::get('country'):0;
					$obj->state						=  !empty(Input::get('state')) ? Input::get('state'):0;
					$obj->city						=  !empty(Input::get('city')) ? Input::get('city'):0;
					$obj->timezone =  !empty(Input::get('timezone')) ? Input::get('timezone'):0;

					$startDate = str_replace('/', '-', Input::get('start_date'));
					$endDate = str_replace('/', '-', Input::get('end_date'));
					$obj->lockout_start_date = date('Y-m-d',strtotime($startDate));
					$obj->lockout_end_date = date('Y-m-d',strtotime($endDate));

					$obj->is_verified				=  1; 
					$obj->is_active					=  1; 
					$obj->is_approved				=  1; 

					//$obj->is_profile_complete		=  1; 
				    //echo '<pre>'; print_r(Input::file('club_logo')); die;
					if(input::hasFile('club_logo')){
						$extension 			=	Input::file('club_logo')->getClientOriginalExtension();
						$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
						$folderPath			=	CLUB_IMAGE_ROOT_PATH.$newFolder; 
						if(!File::exists($folderPath)){
							File::makeDirectory($folderPath, $mode = 0777,true);
						}
						$userImageName = time().'-club.'.$extension;
						$image = $newFolder.$userImageName;
						if(Input::file('club_logo')->move($folderPath, $userImageName)){
							$obj->club_logo		=	$image;
						}
					}
					
				$obj->save();
				//DB::commit();
			    $response	=	array(
			    	'success' 	=>	'1',
			    	'message' 	=>	trans("Club successfully added")
			    ); 
			     //Session::flash('flash_notice', trans("account  has been registered"));
			     return Response::json($response);
			}
		}
	}//end saveForum

	/**
	* Function for edit Forums
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editClub($userId = 0){  

		if (Auth::guard('admin')->user()->user_role_id != 1) {
			if(Auth::guard('admin')->user()->id != $userId){
				return Redirect::to('admin/club/edit-club/'.Auth::guard('admin')->user()->id);
			}
		}
		$userDetails			=	User::find($userId); 
		if(empty($userDetails)) {
			return Redirect::to('admin/club');
		}
		if($userId){
			$userDetails		=	User::find($userId);
			$regionList			=	array();
			$cityList			=	array();
			$old_country		=	Input::old('country'); 
			if(!empty($old_country)){
				$countryCode	=  Input::old('country');
			}else{
				$countryCode	=  $userDetails->country;
			}
			$countryList		=	DB::table('countries')->orderBy('name','ASC')->pluck('name','id')->toArray();
			$stateList			=	DB::table('states')->where('country_id', $userDetails->country)->orderBy('name','ASC')->pluck('name','id')->toArray();
			$cityList			=	DB::table('cities')->where('state_id', $userDetails->state)->orderBy('name','ASC')->pluck('name','id')->toArray();
			$countryCodeList = DB::table('countries')->pluck('phonecode','phonecode')->all();

			$state = !empty($userDetails->state) ? $userDetails->state :0;
			$city = !empty($userDetails->city) ? $userDetails->city:0;
			$country = !empty($userDetails->country) ? $userDetails->country:0;
			return View::make('admin.clubs.edit', compact('userDetails','countryList','userId','user_role_id','imageDetails','roles','uRoles','stateList','cityList','countryCodeList','TimeZone','state','city','country'));
			
		}

	}

	/*Check Lockout Time start here*/
	public function getDaysName(){   
		$club_data = User::where('id',Auth::guard('admin')->user()->id)->first();
		$temp_day_arr = array(    'sunday' => 0,
		    'monday' => 1,
		    'tuesday' => 2,
		    'wednesday' => 3,
		    'thursday' => 4,
		    'friday' => 5,
		    'saturday' => 6);

		        

		$flg = false; 
		$i = 0; 
		        // dump($club_data); 
		if(!empty($club_data)){
	        foreach ($temp_day_arr as $key => $value) {
	            if(strtolower($club_data->lockout_start_day) == $key) $flg = true; 
	            if($flg){
	                $temp_day_arr[$key] = $i++; 
	            }
	        }
	        foreach ($temp_day_arr as $key => $value) {
	            if(strtolower($club_data->lockout_start_day) == $key) break; 
	            $temp_day_arr[$key] = $i++; 
	        }
		}

		$lockout_club = false; 


		if(!empty($club_data->lockout_start_day) && !empty($club_data->lockout_end_day) && !empty($club_data->lockout_start_time) && !empty($club_data->lockout_end_time)){
		        $club_data->lockout_start_time = explode(':', $club_data->lockout_start_time);
		        $club_data->lockout_end_time = explode(':', $club_data->lockout_end_time);
		        if($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] > $temp_day_arr[strtolower($club_data->lockout_start_day)] && 
		            $temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] < $temp_day_arr[strtolower($club_data->lockout_end_day)]  
		        ){
		            $lockout_club = true;    
		        }else if(
		($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] == $temp_day_arr[strtolower($club_data->lockout_start_day)]  ) && 
		(Carbon::now()->isoFormat('H') > $club_data->lockout_start_time[0] || (Carbon::now()->isoFormat('H') == $club_data->lockout_start_time[0] && Carbon::now()->isoFormat('m') > $club_data->lockout_start_time[1])) 
		        ){
		            // echo 'hello'; die; 
		            $lockout_club = true; 
		        }else if(
		($temp_day_arr[strtolower(Carbon::now()->isoFormat('dddd'))] == $temp_day_arr[strtolower($club_data->lockout_end_day)] ) && 
		(Carbon::now()->isoFormat('H') < $club_data->lockout_end_time[0] || (Carbon::now()->isoFormat('H') == $club_data->lockout_end_time[0] && Carbon::now()->isoFormat('m') < $club_data->lockout_end_time[1]))
		        ){
		            // echo 'hihihihh'; die; 
		            $lockout_club = true; 
		        }

		    }

		 
		if(!empty($club_data->lockout_start_date) && !(Carbon::now()->startOfDay() >= Carbon::parse($club_data->lockout_start_date)->startOfDay() &&   Carbon::now()->startOfDay() <= Carbon::parse($club_data->lockout_end_date)->startOfDay()) && $lockout_club == true ){
		      $lockout_club = false; 
		}
		if((Carbon::now()->startOfDay() < Carbon::parse($club_data->lockout_start_date)->startOfDay())  && $lockout_club == true ){
			$lockout_club = false; 
		}
		if($lockout_club==false){
            User::where('id', Auth::guard('admin')->user()->id)->update(['is_lockout'=>0]);
            $obj = new LockoutLog();
	    	$obj->club_id = Auth::guard('admin')->user()->id;
	    	$obj->date_from = Auth::guard('admin')->user()->lockout_start_date;
	    	$obj->date_to = Auth::guard('admin')->user()->lockout_end_date;
	    	$obj->day_from = Auth::guard('admin')->user()->lockout_start_day;
	    	$obj->day_to = Auth::guard('admin')->user()->lockout_end_day;
	    	$obj->start_time = Auth::guard('admin')->user()->lockout_start_time;
	    	$obj->end_time = Auth::guard('admin')->user()->lockout_end_time;
	    	$obj->save();
	    }else{
	    	User::where('id', Auth::guard('admin')->user()->id)->update(['is_lockout'=>1]);
	    }

	}
	/*Check Lockout Time finish here*/

/**
* Function for update Forum
*
* @param null
*
* @return view page. 
*/
	public function updateClub(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		$user_id = Input::get('user_id');
		Validator::extend('custom_password', function($attribute, $value, $parameters) {
			if(preg_match('/^(?=.*[a-z])(?=.*\d).{8,}$/', $value)) {
				return true;
			} else {
				return false;
			}
		});
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'club_name'		=> 'required',
					//'club_type'		=> 'required',
					'game_name' => 'required',
					//'username' => "required|unique:users,username,$user_id",
					'sport_name' => 'required',
					'dob' => 'required',
					'first_name'		=> 'required',
					'last_name' 		=> 'required',
					'email' 			=> 'required|email|unique:users,email,'.$user_id,
					//'phone' 			=> 'required|regex:/^([0-9\s\-\+\(\)]*)$/|digits_between:8,15',
					'gender' 			=> 'required',
					'club_logo' 		=> 'max:2048|mimes:'.IMAGE_EXTENSION,
					'image' 		=> 'max:2048|mimes:'.IMAGE_EXTENSION,
					'timezone' => 'required',
					'start_date' => 'required',
					'end_date' => 'required',
					'password'	=> 'min:8|custom_password',
					'confirm_password' => 'min:8|same:password', 
					
				),
				array(
					"password.custom_password"	=>	trans("Password must have be a combination of numeric, alphabet and special characters.")
				)
			);
			if ($validator->fails()){
				$errors 	=	$validator->messages();
				$response	=	array(
					'success' 	=> false,
					'errors' 	=> $errors
				);
				return Response::json($response); 
				die;
			}else{  
					$obj 						    =  User::find($user_id);
					$fullName						=  ucwords(Input::get('first_name').' '.Input::get('last_name'));
					$obj->club_name 				=  Input::get('club_name'); 
					//$obj->game_mode 				=  Input::get('club_type');
					$obj->game_name 				=  Input::get('game_name');
					$obj->sport_name 				=  Input::get('sport_name');
					//$obj->username 				=  Input::get('username');
					$obj->post_code 				=  !empty(Input::get('post_code')) ? Input::get('post_code'):'';
					$obj->email 					=  Input::get('email');
					$obj->first_name				=  ucfirst(Input::get('first_name'));
					$obj->last_name					=  ucfirst(Input::get('last_name'));
					$obj->full_name 				=  $fullName;
					$obj->email 					=  Input::get('email');
					$obj->phone 					=  !empty(Input::get('phone')) ? Input::get('phone'):'';
					$obj->gender					=  Input::get('gender'); 
					$obj->facebook					=  Input::get('facebook');
					$obj->twitter					=  Input::get('twitter');
					$obj->instagram					=  Input::get('instagram');
					$obj->website					=  Input::get('website');
					$obj->country					=  !empty(Input::get('country')) ? Input::get('country'):0;
					$obj->state	=  !empty(Input::get('state')) ? Input::get('state'):0;
					$obj->city	=  !empty(Input::get('city')) ? Input::get('city'):0;
					$obj->timezone =  !empty(Input::get('timezone')) ? Input::get('timezone'):0;
					
					$startDate = str_replace('/', '-', Input::get('start_date'));
					$endDate = str_replace('/', '-', Input::get('end_date'));

					if(!empty(Input::get('password')) && Input::get('password') != ""){
						$obj->password	 			=  Hash::make(Input::get('password'));
					}else {
						$obj->password	 			=  $obj->password;
					}
					$obj->lockout_start_date =  date('Y-m-d',strtotime($startDate));
					$obj->lockout_end_date = date('Y-m-d',strtotime($endDate));

					if(input::hasFile('club_logo')){
						$extension 			=	Input::file('club_logo')->getClientOriginalExtension();
						$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
						$folderPath			=	CLUB_IMAGE_ROOT_PATH.$newFolder; 
						if(!File::exists($folderPath)){
							File::makeDirectory($folderPath, $mode = 0777,true);
						}
						$userImageName = time().'-club.'.$extension;
						$image = $newFolder.$userImageName;
						if(Input::file('club_logo')->move($folderPath, $userImageName)){
							$obj->club_logo		=	$image;
						}
					}


					if(input::hasFile('image')){
						$extension 			=	Input::file('image')->getClientOriginalExtension();
						$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
						$folderPath			=	USER_PROFILE_IMAGE_ROOT_PATH.$newFolder; 
						if(!File::exists($folderPath)){
							File::makeDirectory($folderPath, $mode = 0777,true);
						}
						$userImageName = time().'-user_profile.'.$extension;
						$image = $newFolder.$userImageName;
						if(Input::file('image')->move($folderPath, $userImageName)){
							$obj->image		=	$image;
						}
					}

				$obj->save();
				$this->getDaysName();
			    $response	=	array(
			    	'success' 	=>	'1',
			    	'message' 	=>	trans("Club successfully updated")
			    ); 
			     //Session::flash('flash_notice', trans("account  has been registered"));
			     return Response::json($response);
			}
		}
	}//end update Forum-manager
	
	public function viewClub($userId = 0){
		$userDetails		=	User::find($userId); 				
		if(empty($userDetails)) {
			return Redirect::to('admin/club/');
		}
		#### Getting country name ###
		$countryName	=	DB::table('countries')
							->where('id','=',$userDetails->country)
							->value('name');
		$stateName	=	DB::table('states')
							->where('id','=',$userDetails->state)
							->value('name');
	    $cityName	=	DB::table('cities')
							->where('id','=',$userDetails->city)
							->value('name');
							
		return View::make('admin.clubs.view', compact('userDetails','countryName','user_role_id','userId','stateName','cityName'));
	} // 

	
	
	

	/**
	 * Function for change is_active of Forum
	 *
	 * @param $Id as id of Forum
	 * @param $Forum is_active as is_active of Forum
	 *
	 * @return redirect page. 
	 */	
	public function updateStatus($Id = 0, $Status = 0){
		User::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/club');
	} // end updateForumStatus()
	
	/**
	 * Function for change approved status of Forum 
	 *
	 * @param $Id as id of Forum
	 * @param $Forumis_active as is_active of Forum
	 *
	 * @return redirect page. 
	 */	
	public function updateApprovedForumStatus($Id = 0, $Status = 0){
			Forum::where('id', '=', $Id)->update(array('is_approved' => $Status));
			if($Status == 1){
				Session::flash('flash_notice', trans("Forum has been approved successfully."));
			}else{
				Session::flash('flash_notice', trans("Forum not approved successfully."));
			}	
			
			return Redirect::to('admin/forum-manager');
	} // end updateApprovedEventStatus()	 

	/**
	/**
	* Function for mark a Forum as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deleteClub($Id=0){
		$userDetails	=	User::find($Id); 
		if(empty($userDetails)) {
			return Redirect::to('admin/club');
		}
		if($Id){
			//$userModel		=	Forum::where('id',$Id)->delete();
			$userModel		=	User::where('id',$Id)->delete();
		}
		return Redirect::to('admin/club');
	}// end deleteForum

	/**
	* Function for display forum comment
	*
	* @param $Id as id of forum
	*
	* @return view page. 
	*/	
	public function commentForum($Id = 0,$sub_category_id=0){
		$forumDetail		=	 Forum::where('id', '=',  $Id)->first();
		$forumComment 		=	 ForumComment::
								leftJoin('users',	'users.id', '=','forum_comments.user_id')
								->select('forum_comments.*','users.full_name as name')
								->get()
								->toArray();
	
		return View::make("admin.Forum.comment", compact('forumDetail','forumComment','Id'));
	}// end viewForum()
	
	/**
	 * Function for save Comment  for Forum
	 *
	 * @param null
	 *
	 * @return redirect page. 
	 */	
	 public function saveComment(){
		Input::replace($this->arrayStripTags(Input::all()));
		$validator = Validator::make(
			array(
				'comment' 	=> Input::get('comment')
			),
			array(
				'comment'	=> 'required'
			)
		);
		if ($validator->fails()){
			return Redirect::back()
				->withErrors($validator)->withInput();	
		}else{
			DB::beginTransaction();
			$comment					= 	new ForumComment;
			$comment->user_id 	 		=   Auth::user()->id;
			$comment->message 	 		=  	Input::get('comment');
			$comment->save();
		}
		DB::commit();
		Session::flash('flash_notice',trans("Comment added successfully.")); 
		return Redirect::back();
	 }// end saveForum()
	/**
	 * Function for save Comment  for Forum
	 *
	 * @param null
	 *
	 * @return redirect page. 
	 */
	 public function deleteComment($id=0){
		$model = ForumComment::findorFail($id);
		$model->delete();
		Session::flash('flash_notice',trans("Comment deleted successfully.")); 
		return Redirect::back();
	} //end deleteComment

/**
	* Function for edit Game
	*
	* @param null
	*
	* @return view page. 
	*/
	public function updateGameView(){  
		$userId	=	Auth::guard('admin')->user()->id;
		$userDetails			=	User::find($userId); 
		if(empty($userDetails)) {
			return Redirect::to('admin/club');
		}
		if($userId){
			$userDetails		=	User::find($userId);
			$regionList			=	array();
			$cityList			=	array();
			$old_country		=	Input::old('country'); 
			if(!empty($old_country)){
				$countryCode	=  Input::old('country');
			}else{
				$countryCode	=  $userDetails->country;
			}
			$countryList		=	DB::table('countries')->orderBy('name','ASC')->pluck('name','id')->toArray();
			$stateList			=	DB::table('states')->where('country_id', $userDetails->country)->orderBy('name','ASC')->pluck('name','id')->toArray();
			$cityList			=	DB::table('cities')->where('state_id', $userDetails->state)->orderBy('name','ASC')->pluck('name','id')->toArray();
			$countryCodeList = DB::table('countries')->pluck('phonecode','phonecode')->all();
			return View::make('admin.clubs.updateGame', compact('userDetails','countryList','userId','user_role_id','imageDetails','roles','uRoles','stateList','cityList','countryCodeList'));
		}
	}

/**
* Function for update Game
*
* @param null
*
* @return view page. 
*/
	public function updateGame(){
		Input::replace($this->arrayStripTags(Input::all()));
		$user_id	=	Auth::guard('admin')->user()->id;
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'club_name'		=> 'required',
					'club_type'		=> 'required',
					'game_name' => 'required',
					'sport_name' => 'required',
					'first_name'		=> 'required',
					'last_name' 		=> 'required',
					'email' 			=> 'required|email|unique:users,email,'.$user_id,
					'phone' 			=> 'required|regex:/^([0-9\s\-\+\(\)]*)$/|digits_between:8,15|unique:users,phone,'.$user_id,
					'gender' 			=> 'required',
					'club_logo' 		=> 'max:2048|mimes:'.IMAGE_EXTENSION,
					
				),
				array(
					//'image.dimensions' => "Please upload min width 221 px and min height 266 px image",
				)
			);
			if ($validator->fails()){
				 $errors 	=	$validator->messages();
					$response	=	array(
						'success' 	=> false,
						'errors' 	=> $errors
					);
					return Response::json($response); 
					die;
			}else{ 
					$obj 						    =  User::find($user_id);
					$fullName						=  ucwords(Input::get('first_name').' '.Input::get('last_name'));
					$obj->club_name 				=  Input::get('club_name');
					$obj->game_mode 				=  Input::get('club_type');
					$obj->game_name 				=  Input::get('game_name');
					$obj->sport_name 				=  Input::get('sport_name');
					$obj->post_code 				=  !empty(Input::get('post_code')) ? Input::get('post_code'):'';
					$obj->email 					=  Input::get('email');
					$obj->first_name				=  ucfirst(Input::get('first_name'));
					$obj->last_name					=  ucfirst(Input::get('last_name'));
					$obj->full_name 				=  $fullName;
					$obj->email 					=  Input::get('email');
					$obj->phone 					=  !empty(Input::get('phone')) ? Input::get('phone'):'';
					$obj->password	 				=  Hash::make(Input::get('password'));
					$obj->dob						=  Input::get('dob');
					$obj->gender					=  Input::get('gender'); 
					$obj->country					=  !empty(Input::get('country')) ? Input::get('country'):0;
					$obj->state						=  !empty(Input::get('state')) ? Input::get('state'):0;
					$obj->city						=  !empty(Input::get('city')) ? Input::get('city'):0;
					if(input::hasFile('club_logo')){
					$extension 			=	Input::file('club_logo')->getClientOriginalExtension();
					$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
					$folderPath			=	CLUB_IMAGE_ROOT_PATH.$newFolder; 
					if(!File::exists($folderPath)){
						File::makeDirectory($folderPath, $mode = 0777,true);
					}
					$userImageName = time().'-club.'.$extension;
					$image = $newFolder.$userImageName;
					if(Input::file('club_logo')->move($folderPath, $userImageName)){
						$obj->club_logo		=	$image;
					}
				}
				$obj->save();
			    $response	=	array(
			    	'success' 	=>	'1',
			    	'message' 	=>	trans("Club successfully updated")
			    ); 
			    return Response::json($response);
			}
		}
	}//end update Forum-manager

}// end ClubController class
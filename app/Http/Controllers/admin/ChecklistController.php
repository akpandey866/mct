<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Checklist;
use mjanssen\BreadcrumbsBundle\Breadcrumbs as Breadcrumb;
use Auth,Blade,DB,Input,Mail,Redirect,Request,Response,Session,URL,View,Validator,Config;

/**
 * NewsLetter Controller
 *
 * Add your methods in the class below
 *
 * This file will render views from views/admin/newsletter
 */
 
class ChecklistController extends BaseController {
 /**
 * Function for display all checklist template
 *
 * @param null
 *
 * @return view page. 
 */
	public function listChecklist() { 
		$DB				=	Checklist::query();
		$searchVariable	=	array(); 
		$inputGet		=	Input::get();
		if ((Input::get() && isset($inputGet['display'])) || isset($inputGet['page']) ){
			$searchData	=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);
			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue)){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
					$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
				}
			}
		}
		
		$sortBy = (Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  = (Input::get('order')) ? Input::get('order')   : 'DESC';
		
		$result	 	= 	$DB->orderBy($sortBy, $order)->paginate(Config::get("Reading.records_per_page"));
		
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.checklist.index', compact('result','searchVariable','sortBy','order','query_string'));
	}// end listTemplate()

	/**
	* Function for add Checklist
	*
	* @param null
	*
	* @return view page.  
	*/
	public function addChecklist(){
		return  View::make('admin.checklist.add');
	}//end addCheclist()

	public function saveChecklist(){ 
	Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'title'=> 'required',
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
			$obj =  new Checklist;
			$obj->title =  Input::get('title');
			$obj->description =  !empty(Input::get('description')) ? Input::get('description'):'';
			$obj->save();
		    Session::flash('success',trans("Checklist has been added successfully"));
			return Redirect::to('admin/checklist');
		    }
		}
	}//end saveChecklist
	/**
	* Function for edit Sposnsor
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editChecklist($id = 0){  
		$details			=	Checklist::findOrFail($id); 
		return View::make('admin.checklist.edit', compact('details'));
	} // 

		/**
* Function for update Forum
*
* @param null
*
* @return view page. 
*/
	public function updateChecklist(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData	=	Input::all();
		$id = Input::get('id');
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'title' => 'required',
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
					$obj = Checklist::find($id);
					$obj->title =  Input::get('title');
					$obj->description =  !empty(Input::get('description')) ? Input::get('description'):'';
					$obj->save();
			    Session::flash('success',trans("Checklist has been upated successfully"));
				return Redirect::to('admin/checklist');
			}
		}
	}//end update Forum-manager

	public function updateStatus($Id = 0, $Status = 0){
		Checklist::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/checklist');
	} // end updateStatus()

	public function deleteChecklist($Id=0){
		$userModel		=	Checklist::where('id',$Id)->delete();
		Session::flash('flash_notice',trans("Checklist has been removed successfully")); 
		return Redirect::to('admin/checklist');
	}// end deleteForum

	public function viewChecklist($id=0){
		$details			=	Checklist::findOrFail($id); 
		return View::make('admin.checklist.view', compact('details'));
	}
}
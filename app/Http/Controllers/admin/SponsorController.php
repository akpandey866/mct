<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Sponsor;
use App\Model\Club;
use App\Model\User;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use Carbon\Carbon;

class SponsorController extends BaseController {
/**
* Function for display all event
*
* @param null
*
* @return view page.
*/
	public function index(){
		$DB 					= 	Sponsor::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					if($fieldName=="is_active"){
						$DB->where("grades.is_active",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="club"){
						$DB->where("user_id",'like','%'.$fieldValue.'%'); 
					}
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			} 
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'sponsors.created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'ASC';
	    if(Auth::guard('admin')->user()->user_role_id == 1){
	    	$result = 	$DB
					->orderBy($sortBy, $order)
					//->where('user_id',auth()->guard('admin')->user()->id)
					->paginate(Config::get("Reading.record_game_per_page"));

	    }else{
	    	$result = 	$DB
				->orderBy($sortBy, $order)
				->where('user_id',auth()->guard('admin')->user()->id)
				->paginate(Config::get("Reading.record_game_per_page"));
	    }
		

		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();

		$club					=	new Club();
		$cludDetails			=	$club->get_club_list();
		return  View::make('admin.sponsor.index', compact('result','searchVariable','sortBy','order','query_string','cludDetails'));
	 }

	/**
	* Function for add Forum
	*
	* @param null
	*
	* @return view page.  
	*/
	public function addSponsor(){
		$club =	new Club();
		$clubLists =	$club->get_club_list();
		return  View::make('admin.sponsor.add',compact('clubLists'));
	}//end addPlayer()

	public function saveSponsor(){ 
	Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'name'=> 'required',
					'logo' => 'required|max:5048|mimes:'.IMAGE_EXTENSION,
				)
			);
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
				$obj =  new Sponsor;
				$obj->name =  Input::get('name');
				$obj->user_id = !empty(Input::get('club')) ? Input::get('club') : auth()->guard('admin')->user()->id;
				$obj->website =  Input::get('website');
				$obj->instagram =  Input::get('instagram');
				$obj->twitter =  Input::get('twitter');
				$obj->facebook =  Input::get('facebook');
				$obj->about =  Input::get('about');
				$obj->offer =  Input::get('offer');
				if(input::hasFile('logo')){
					$extension 			=	Input::file('logo')->getClientOriginalExtension();
					$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
					$folderPath			=	SPONSOR_IMAGE_ROOT_PATH.$newFolder; 
					if(!File::exists($folderPath)){
						File::makeDirectory($folderPath, $mode = 0777,true);
					}
					$userImageName = time().'-sponsor.'.$extension;
					$image = $newFolder.$userImageName;
					if(Input::file('logo')->move($folderPath, $userImageName)){
						$obj->logo		=	$image;
					}
				}
				$obj->save();
			    Session::flash('success',trans("Sponsor has been added successfully"));
				return Redirect::to('admin/sponsor');
			    }
		}
	}//end saveSponsor
	/**
	* Function for edit Sposnsor
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editSponsor($id = 0){  
		$details			=	Sponsor::findOrFail($id); 
		$club =	new Club();
		$clubLists =	$club->get_club_list();
		$gameMode = User::where('id',$details->user_id)->value('game_mode');
		//echo $gameMode;die;
		return View::make('admin.sponsor.edit', compact('details','clubLists','gameMode'));
	} // 

	/**
* Function for update Forum
*
* @param null
*
* @return view page. 
*/
	public function updateSponsor(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData	=	Input::all();
		$id = Input::get('id');
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'logo' => 'max:2048|mimes:'.IMAGE_EXTENSION,
					'name' => 'required',
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
					$obj = Sponsor::find($id);
					$obj->name =  Input::get('name');
					$obj->website =  Input::get('website');
					$obj->instagram =  Input::get('instagram');
					$obj->twitter =  Input::get('twitter');
					$obj->facebook =  Input::get('facebook');
					$obj->about =  Input::get('about');
					$obj->offer =  Input::get('offer');
					$obj->user_id = !empty(Input::get('club')) ? Input::get('club') : auth()->guard('admin')->user()->id;
					if(Input::hasFile('logo')){
						$image 					=	Sponsor::where('id',$id)->value('logo');
						@unlink(SPONSOR_IMAGE_ROOT_PATH.$image);
						$extension 			=	Input::file('logo')->getClientOriginalExtension();
						$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
						$folderPath			=	SPONSOR_IMAGE_ROOT_PATH.$newFolder; 
						if(!File::exists($folderPath)){
							File::makeDirectory($folderPath, $mode = 0777,true);
						}
						$userImageName = time().'-sponsor.'.$extension;
						$image = $newFolder.$userImageName;
						if(Input::file('logo')->move($folderPath, $userImageName)){
							$obj->logo		=	$image;
						}
					}
					$obj->save();
			    Session::flash('success',trans("Sponsor has been upated successfully"));
				return Redirect::to('admin/sponsor');
			}
		}
	}//end update Forum-manager



	/**
	 * Function for change is_active of Forum
	 *
	 * @param $Id as id of Forum
	 * @param $sponsor is_active as is_active of Forum
	 *
	 * @return redirect page. 
	*/	
	public function updateStatus($Id = 0, $Status = 0){
		Sponsor::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/sponsor');
	} // end updateStatus()
	
	
	/**
	/**
	* Function for mark a Forum as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deleteSponsor($Id=0){
		$userDetails	=	Sponsor::findOrFail($Id); 
		if(File::exists(SPONSOR_IMAGE_ROOT_PATH.$userDetails->logo)){
	        @unlink(SPONSOR_IMAGE_ROOT_PATH.$userDetails->logo);
	    }
		$userModel		=	Sponsor::where('id',$Id)->delete();
		return Redirect::to('admin/sponsor');
	}// end deleteForum

	
}// end ClubController class
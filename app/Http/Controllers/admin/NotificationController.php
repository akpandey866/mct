<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Notification;
use App\Model\User;
use mjanssen\BreadcrumbsBundle\Breadcrumbs as Breadcrumb;
use Illuminate\Http\Request;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator;
/**
* Subscribes Controller
*
* Add your methods in the class below
*
* This file will render views from views/admin/Subscribe
*/
 
class NotificationController extends BaseController {
/**
* Function for display list of  all Notification
*
* @param null
*
* @return view page. 
*/
	public function listing(){          
		$DB 								= 	Notification::query();
		$searchVariable						=	array(); 
		$inputGet							=	Input::get();
		if (Input::get()) {
			$searchData						=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue)){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
					$searchVariable			=	array_merge($searchVariable,array($fieldName => $fieldValue));
				}
			}
		}
		$sortBy 							= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
	    $order  							= 	(Input::get('order')) ? Input::get('order')   : 'DESC';
		$result  								= 	$DB->orderBy($sortBy, $order)->paginate(Config::get("Reading.records_per_page"));
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make("admin.notifications.index",compact('result' ,'searchVariable','sortBy','order','query_string'));
	} // end listNotification()

	public function addNotification(){	
		return  View::make('admin.notifications.add',compact('type'));
	} //end addNotification()

/**
* Function for save added Notification page
*
* @param null
*
* @return redirect page. 
*/
	function saveNotification(Request $request){
		$validator = Validator::make(
			Input::all(),	
			array(
				'body' => 'required',
				
			)
		);
		if ($validator->fails()){	
			return Redirect::route('notification.addNotificaiton')
				->withErrors($validator)->withInput();
		}else{
			$notification = new Notification;
			$notification->body = $request->body;
			$notification->save(); 
			return redirect()->route('notification.listing')->with('success','Notification added successfully.');
			// Session::flash('flash_notice','Notification added successfully')); 
			// return Redirect::route('notification.listing');
		}
	}//end saveNotification()


	/**
* Function for display page  for edit Notification page
*
* @param $Id ad id of Notification 
* @param $type as category of Notification 
*
* @return view page. 
*/	
	public function editNotification($Id){ 
		//$notificationId = $request->notification_id;
		$notifications	=	Notification::find($Id);
		if(empty($notifications)) {
			return Redirect::route('notification.listing');
		}
		$notifications	=	Notification::where('id', '=',  $Id)->first();
		return  View::make('admin.notifications.edit',compact('notifications','Id'));
	}// end editNotification()

	/**
* Function for update Notification 
*
* @param $Id ad id of Notification 
* @param $type as category of Notification 
*
* @return redirect page. 
*/
	function updateNotification(Request $request,$Id){
		Input::replace($this->arrayStripTags(Input::all()));
		$validator 										= 	Validator::make(
			Input::all(),
			array(
				'body' =>  'required',
			)
		);
		if ($validator->fails()){	
			return Redirect::route('notification.editNotification',$Id)
				->withErrors($validator)->withInput();
		}else{
			$notification = 	Notification:: find($Id);
			$notification->body = $request->body;
			$notification->save();
			return redirect()->route('notification.listing')->with('success','Notification updated successfully.');
		}
	}// end updateNotification()

/**
* Function for delete Subscribe
* 
* @param $modelId as id 
*
* @return redirect page. 
*/
	public function deleteNotification($modelId = 0){
		if($modelId){
			$model = Notification::findorFail($modelId);
			$model->delete();
			Session::flash('flash_notice',trans("Notification successfully deleted.")); 
		}
		return Redirect::route("notification.listing");
	}// end deleteSubscribe()

/**
* Function for delete notification
* 
* @param $modelId as id 
*
* @return redirect page.  
*/
	public function sendEmail($notificationId = 0){ 
		$notificationsDescription = Notification::where('id',$notificationId)->value('body');
		$userList =	User::where(['is_deleted'=>0,'is_active'=>1])->where('user_role_id','<>',1)->pluck('full_name','id');
		$userIds = User::pluck('id','id')->all();
		return  View::make('admin.notifications.send_emails',compact('notificationsDescription','notificationId','userList','userIds'));

	}// end deleteSubscribe()

	public function sendnotificationEmail (Request $request){
		$notificationId = $request->notification_id;
		$playerids = $request->player_id;
		$message = Notification::where('id',$notificationId)->value('body');
		if(!empty($request->single_user) && !empty($request->player_id)){
			$userDetails = User::whereIn('id',$playerids)->select('email')->get();
			foreach($userDetails as $key=> $sub_list){
				$settingsEmail =  Config::get('Site.email');
				$constants = array();
		        $cons = Config::get('notification_email_constant'); 
				foreach($cons as $key=>$val){
					$constants[] = '{'.$val.'}';
				}
				$website_url_path  =	URL::to('/');
				$website_url       = 	'<a style="text-decoration:none;" href="'.$website_url_path.'">'.Config::get("Site.title").'</a>';
				$to						 =	 $sub_list->email;
				$from					 =	 Config::get('Site.email');
				$replyTo			     =   '';
				$subject				 =	 'Admin Site'; 
				$enc_id			 		 =	 $sub_list->enc_id;
				$route_url      		=  	URL::to('unsubscribe/'.$enc_id);  
				$unsubscribe_link       = 	'<a style="text-decoration:none;" href="'.$route_url.'">click here</a>';
				$rep_Array               =   array($to,$website_url,$unsubscribe_link); 
				$message 			     =   str_replace($constants, $rep_Array, $message);	
				$this->sendMail($to,'Admin',$subject,$message,$from);
				Session::flash('flash_notice',trans("Notification has been successfully sent.")); 
				return Redirect::to("admin/user-notification");
			}
		}if(!empty($request->game_admin)){
			$userDetails = User::where('user_role_id',CLUBUSER)->where('is_active',1)->where('is_deleted',0)->select('email')->get();
			foreach($userDetails as $key=> $sub_list){
				$settingsEmail =  Config::get('Site.email');
				$constants = array();
		        $cons = Config::get('notification_email_constant'); 
				foreach($cons as $key=>$val){
					$constants[] = '{'.$val.'}';
				}
				$website_url_path  =	URL::to('/');
				$website_url       = 	'<a style="text-decoration:none;" href="'.$website_url_path.'">'.Config::get("Site.title").'</a>';
				$to						 =	 $sub_list->email;
				$from					 =	 Config::get('Site.email');
				$replyTo			     =   '';
				$subject				 =	 'Admin Site'; 
				$enc_id			 		 =	 $sub_list->enc_id;
				$route_url      		=  	URL::to('unsubscribe/'.$enc_id);  
				$unsubscribe_link       = 	'<a style="text-decoration:none;" href="'.$route_url.'">click here</a>';
				$rep_Array               =   array($to,$website_url,$unsubscribe_link); 
				$message 			     =   str_replace($constants, $rep_Array, $message);	
				$this->sendMail($to,'Admin',$subject,$message,$from);
			}
			Session::flash('flash_notice',trans("Notification has been successfully sent.")); 
			return Redirect::to("admin/user-notification");
		}
	}
/**
* Function to reply a user 
* 
* @param $modelId as id 
*
* @return view page. 
*/	
	public function replyToUser($Id){
		Input::replace($this->arrayStripTags(Input::all()));
		if(!empty(Input::all())){
			$validationRules		=	array('message'	=> 'required');
			$validator 				=	Validator::make(
				Input::all(),
				$validationRules
			);
			if($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
				$userData			=	Subscribe::where('id',$Id)->first();
				##### send email to user from admin,to inform user that your message has been received successfully #####
				$emailActions		=	EmailAction::where('action','=','replay_to_user')->get()->toArray();
				$emailTemplates		=	EmailTemplate::where('action','=','replay_to_user')->get(array('name','subject','action','body'))->toArray();
				$cons 				=	explode(',',$emailActions[0]['options']);
				$constants 			=	array();
				foreach($cons as $key=>$val){
					$constants[] 	=	'{'.$val.'}';
				}
				$name				=	$userData->name;
				$email				=	$userData->email;
				$message			=	Input::get('club');
				
				$subject 			=  	$emailTemplates[0]['subject'];
				$rep_Array 			=  	array($name,$message); 
				$messageBody		=  	str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
				$this->sendMail($email,$name,$subject,$messageBody,Config::get("Site.Subscribe_email"));
				Session::flash('success','You have Successfully replied to '. $name);
				return Redirect::route("$this->model.index");
			}	
		}		
	}//end replyToUser()
}// end SubscribesController
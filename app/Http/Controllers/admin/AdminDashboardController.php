<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\GameSetting;
use App\Model\User; 
use App\Model\Player; 
use App\Model\Fixture; 
use App\Model\Team; 
use App\Model\PlayerPackDetail; 
use App\Model\ClubActivation; 
use App\Model\Grade; 
use App\Model\Availability; 
use App\Model\FixtureScorcard; 
use App\Model\VerifyUser; 
use App\Model\ScorerAccess; 
use App\Model\Sponsor; 
use App\Model\Branding; 
use App\Model\Fundraiser; 
use App\Model\UserTeams; 
use App\Model\DropDown; 
use App\Model\GamePrize; 
use App\Model\Checklist; 
use Carbon\Carbon; 

use App\Model\UserTeamLogs; 

use App\Model\UserTeamsCVCTrack; 
use App\Model\PlayerTradeInOut; 

use Illuminate\Support\Facades\Input;
use mjanssen\BreadcrumbsBundle\Breadcrumbs as Breadcrumb;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
/**
* AdminDashBoard Controller
*
* Add your methods in the class below
*
* This file will render views\admin\dashboard
*/
	class AdminDashBoardController extends BaseController {
	/**
	* Function for display admin dashboard
	*
	* @param null
	*
	* @return view page. 
	*/
	public function showdashboard(){ 
		if(Auth::guard('admin')->user()->user_role_id == CLUBUSER){
			$totalGrades = Grade::where('is_active',1)->count();
			return Redirect::to('admin/club-dashboard');
		}if(Auth::guard('admin')->user()->user_role_id == SCORER){
			return Redirect::to('admin/fixture');
		}
 
 		$totalClubUser = User::where('user_role_id',CLUBUSER)->where('is_deleted',0)->count(); 
		$totalFrontUser = User::where('user_role_id',USER)->where('is_deleted',0)->count();

		$clubUser = User::where('user_role_id',CLUBUSER)->where('is_deleted',0)->limit(3)->orderBy('created_at','desc')->get();
		
		$frontUser = User::where('user_role_id',USER)->where('is_deleted',0)->limit(3)->orderBy('created_at','desc')->get();

		$totalActivatedGame = User::where('user_role_id',CLUBUSER)->where('is_deleted',0)->where('is_game_activate',1)->count();
		$totalInActivatedGame = User::where('user_role_id',CLUBUSER)->where('is_deleted',0)->where('is_game_activate',0)->count();

		$clubIncome = ClubActivation::sum('amount'); 
		$brandingFee = Branding::sum('amount'); 

		$playersIncome = PlayerPackDetail::leftjoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
						->sum('player_packs.price');

		$recentGame = User::where("is_deleted",0)->where("user_role_id",CLUBUSER)->where("is_active",1)->limit(3)->orderBy('created_at','DESC')->get();	
		$totalGrades = Grade::count();
		$totalTeams = Team::count();

		$DB1			=	User::query();
		$users			=	$DB1->where('is_deleted',0)->get()->toArray();
		$month			=	date('m');
		$year			=	date('Y');
		for ($i = 0; $i < 12; $i++) {
			$months[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
		}
		$months				=	array_reverse($months);
		$num				=	0;
		$thisMothUsers		=	0;
		foreach($months as $month){
			$month_start_date	=	 date('Y-m-01 00:00:00', strtotime($month));
			$month_end_date		=	 date('Y-m-t 23:59:59', strtotime($month));
			$allClubUsers[$num]['month']	=	$month;
			$frontUsers[$num]['month']		=	$month;
			$allUsers[$num]['month']				=	$month;
			$totalStorePayment[$num]['month']		=	$month;
			$totalEventPayment[$num]['month']		=	$month;
			$DB2			=	User::query();
			$DB3			=	User::query();
			$DB4			=	User::query();
			
			$allClubUsers[$num]['users']	=	$DB2->where('is_deleted',0)->where('user_role_id',CLUBUSER)->where('created_at','>=',$month_start_date)->where('created_at','<=',$month_end_date)->count();
			
			$frontUsers[$num]['users']	=	$DB3->where('is_deleted',0)->where('user_role_id',USER)->where('created_at','>=',$month_start_date)->where('created_at','<=',$month_end_date)->count();
						
			$allUsers[$num]['users']	=	$DB4->where('is_deleted',0)->where('created_at','>=',$month_start_date)->where('created_at','<=',$month_end_date)->count();
			
			if($month_start_date == date( 'Y-m-01 00:00:00', strtotime( 'first day of ' . date( 'F Y')))){
				$thisMothUsers	=	$allUsers[$num]['users'];
			} 
			$num ++;
		}
		$totalGame = User::where('game_name','<>','')->where('user_role_id',CLUBUSER)->count('id'); 
		$activeGame = User::where('game_name','<>','')->where('user_role_id',CLUBUSER)->where('is_game_activate',1)->count('id'); 
		$inActiveGame = User::where('game_name','<>','')->where('user_role_id',CLUBUSER)->where('is_game_activate',0)->count('id'); 
		$totalFixtures = Fixture::count('id');
		$totalPlayers = Player::count('id');
		$totalAvailability = Availability::count();
		$totalVerifyUsers = VerifyUser::count();
		$totalScorers = ScorerAccess::count();
		$totalSponsors = Sponsor::count();
		$totalBranding = Branding::count();
		$totalPrize = GamePrize::count();
		$fixtureCount = Fixture::where('is_active',1)->count('id');
		/*MAtch type variales*/
		$seniormens = Team::where('type',1)->count();
		$seniorWomens = Team::where('type',2)->count();
		$veterans = Team::where('type',3)->count();
		$juniorBoys = Team::where('type',4)->count();
		$juniorGirls = Team::where('type',5)->count();
		$oneDayMatch = Fixture::where('match_type',ONEDAYMATCH)->count();
		$twoDayMatch = Fixture::where('match_type',TWODAYMATCH)->count();
		$aboutUsList = DropDown::get_master_list("about-us");
		$usersCountries = User::leftjoin('countries','countries.id','=','users.country')
							->where('users.country','<>',0)
							->select('countries.name as country_name', DB::raw('COUNT(users.country) as country_count'))
					    	->groupBy('users.country')
					    	->get();
		$usersStates = User::leftjoin('states','states.id','=','users.state')
        						->where('users.state','<>',0)
        						->select('states.name as states_name', DB::raw('COUNT(users.state) as states_count'))
						    	->groupBy('users.state')
						    	->get();
		$playerScoreSum = Player::where('players.is_active',1) 
							->leftJoin('fixture_scorecards','fixture_scorecards.player_id','=','players.id')
							->select('players.full_name as player_name',DB::raw('SUM(fixture_scorecards.rs) as runs,SUM(fixture_scorecards.fours) as fours,SUM(fixture_scorecards.sixes) as sixes,SUM(fixture_scorecards.overs) as overs,SUM(fixture_scorecards.mdns) as mdns,SUM(fixture_scorecards.wks) as wks,SUM(fixture_scorecards.cs) as cs,SUM(fixture_scorecards.cwks) as cwks,SUM(fixture_scorecards.sts) as sts,SUM(fixture_scorecards.rods) as rods,SUM(fixture_scorecards.roas) as roas,SUM(fixture_scorecards.dks) as dks,SUM(fixture_scorecards.hattrick) as hattrick,SUM(fixture_scorecards.fantasy_points) as fantasy_points'))
							->groupBy('players.id')
							->paginate(10);
		return View::make('admin.dashboard.dashboard',compact('totalClubUser','totalFrontUser','totalActivatedGame','totalInActivatedGame','clubIncome','playersIncome','recentGame','clubUser','frontUser','allClubUsers','users','frontUsers','totalGrades','totalTeams','brandingFee','totalGame','activeGame','inActiveGame','totalFixtures','totalPlayers','totalAvailability','totalVerifyUsers','totalScorers','totalSponsors','totalBranding','totalPrize','fixtureCount','seniormens','seniorWomens','veterans','juniorBoys','juniorGirls','oneDayMatch','twoDayMatch','aboutUsList','usersCountries','usersStates','playerScoreSum'));
	}

	/**
	* Function for display admin dashboard
	*
	* @param null
	*
	* @return view page. 
	*/
	public function showClubDashboard(){   
		if(Auth::guard('admin')->user()->user_role_id == SCORER){
			return Redirect::to('admin/fixture');
		}
		$userList = UserTeams::where('club_id',Auth::guard('admin')->user()->id)
					->where('user_teams.is_active',1)
					->pluck('user_id')
					->all();
		//$total_user_cnt = User::where('home_club', Auth::guard('admin')->user()->id)->count(); 
		$total_user_cnt = 	User::whereIn('users.id',$userList)->count();

		$total_player_cnt = Player::where('club', Auth::guard('admin')->user()->id)->count(); 
		$total_fixture_cnt = Fixture::where('club', Auth::guard('admin')->user()->id)->count('id'); 
		$total_team_cnt = Team::where('club', Auth::guard('admin')->user()->id)->count(); 
		$premiumMember = GameSetting::where('club_id',Auth::guard('admin')->user()->id)->count('id');

		/*Show Activation Calculation*/

		$gameMode = Auth::guard('admin')->user()->game_mode;

		$extendedPlanCount = PlayerPackDetail::leftJoin('player_packs','player_packs.id','=','player_pack_details.player_pack_id')
									->where('club',Auth::guard('admin')->user()->id)
									->select('player_packs.name as player_number')
									->get();

		$playerCount = Player::where('club',Auth::guard('admin')->user()->id)->count();						
		$extraPlayer = 0;
		$price = 0;
		$extraPlayerPrice = 0;
		if(!empty($playerCount) && $playerCount > 20){
			$sum=0;
			if(!$extendedPlanCount->isEmpty()){ 
				foreach ($extendedPlanCount as $key => $value) {
					$sum +=$value->player_number;
				}
			}
			$extraPlayer = $playerCount - $sum;
			$extendedPlayerSum = MAXIMUMPLAYER + $sum;
			$extraPlayerPrice  =$extraPlayer * PLAYERFEE;
		}
				
		//echo $extraPlayer;die;
		$price = 0;
		if($gameMode == 1){
			$price = Config::get("Site.senior_game_fee"); ;
		}elseif ($gameMode == 2) {
			$price = Config::get("Site.junior_game_fee");;
		}elseif ($gameMode == 3) {
			$price = Config::get("Site.league_game_fee");;
		}
		$discount = ((BIRDDISCOUNT / 100)*$price);
		$offeredPrice = ($price-$discount) ;
		
		if(Auth::guard('admin')->user()->game_mode == 1){
			$totalPrice = $offeredPrice + $extraPlayer;
		}else{
			$totalPrice = $offeredPrice;
		} 

		$totalGrades = Grade::where('is_active',1)->where('club',Auth::guard('admin')->user()->id)->count();

		$fixtureCount = Fixture::where('is_active',1)->where('club',Auth::guard('admin')->user()->id)->count('id');

		//$fixtureCount = Fixture::where('is_active',1)->leftjoin('fixture_scorecards','fixture_scorecards.fixture_id','fixtures.id')->count('fixture_scorecards.id');


		$oneDayMatch = Fixture::where('match_type',ONEDAYMATCH)->where('club',Auth::guard('admin')->user()->id)->count();
		$twoDayMatch = Fixture::where('match_type',TWODAYMATCH)->where('club',Auth::guard('admin')->user()->id)->count();

		$authUser = Auth::guard('admin')->user();
		if($authUser->game_mode ==1){
			$seniormens = Team::where('club',$authUser->id)->where('type',1)->count();
			$seniorWomens = Team::where('club',$authUser->id)->where('type',2)->count();
			$veterans = Team::where('club',$authUser->id)->where('type',3)->count();
		}elseif($authUser->game_mode ==2){
			$juniorBoyes = Team::where('club',$authUser->id)->where('type',1)->count();
			$juniorGirls = Team::where('club',$authUser->id)->where('type',2)->count();

		}elseif ($authUser->game_mode ==3) {
			$seniormens = Team::where('club',$authUser->id)->where('type',1)->count();
			$seniorWomens = Team::where('club',$authUser->id)->where('type',2)->count();
			$veterans = Team::where('club',$authUser->id)->where('type',3)->count();
			$juniorBoyes = Team::where('club',$authUser->id)->where('type',4)->count();
			$juniorGirls = Team::where('club',$authUser->id)->where('type',5)->count();

		}
		$allPlayerIds = Player::where('club',$authUser->id)->where('is_active',1)->pluck('id','id')->all();
 
		$playerScoreSum = Player::where('players.club',$authUser->id)->where('players.is_active',1) 
							->leftJoin('fixture_scorecards','fixture_scorecards.player_id','=','players.id')
							->select('players.full_name as player_name',DB::raw('SUM(fixture_scorecards.rs) as runs,SUM(fixture_scorecards.fours) as fours,SUM(fixture_scorecards.sixes) as sixes,SUM(fixture_scorecards.overs) as overs,SUM(fixture_scorecards.mdns) as mdns,SUM(fixture_scorecards.wks) as wks,SUM(fixture_scorecards.cs) as cs,SUM(fixture_scorecards.cwks) as cwks,SUM(fixture_scorecards.sts) as sts,SUM(fixture_scorecards.rods) as rods,SUM(fixture_scorecards.roas) as roas,SUM(fixture_scorecards.dks) as dks,SUM(fixture_scorecards.hattrick) as hattrick,SUM(fixture_scorecards.fantasy_points) as fantasy_points'))
							->groupBy('players.id')
							->get();  
	/*	prd($sumPlayerScore);
		$playerScoreSum = FixtureScorcard::whereIn('player_id',$allPlayerIds) 
							->groupBy('player_id') fixtureCount 
							->select(DB::raw('SUM(run) as runs,SUM(fours) as fours,SUM(sixes) as sixes,SUM(overs) as overs,SUM(mdns) as mdns,SUM(wks) as wks,SUM(cs) as cs,SUM(cwks) as cwks,SUM(sts) as sts,SUM(rods) as rods,SUM(roas) as roas,SUM(dks) as dks,SUM(hattrick) as hattrick,SUM(fantasy_points) as fantasy_points,(select full_name from players where players.id=fixture_scorecards.player_id) as player_name'))
                			->get();*/

        
	
        $recentUsers = User::where('is_active',1)->where('is_deleted',0)->whereIn('id',$userList)->orderBy('created_at','DESC')->limit(8)->get();
        $totalUsers = User::where('is_deleted',0)->whereIn('id',$userList)->count();

        $totalActiveUsers = User::where('is_active',1)->where('is_deleted',0)->whereIn('id',$userList)->count();
        $totalInActiveUsers = User::where('is_active',0)->where('is_deleted',0)->whereIn('id',$userList)->count();
        $totalSubscribedUsers = User::where('is_subscribe',1)->where('is_deleted',0)->whereIn('id',$userList)->count();

       /* $percentage = 50;
		$totalWidth = 350;

		$new_width = ($percentage / 100) * $totalWidth;*/
		$totalActiveUsersPercentage = 0;
		$totalInActiveUsersPercentage = 0;
		$totalSubscribedPercentage = 0;
		if(!empty($totalUsers) && !empty($totalActiveUsers)){
        	$totalActiveUsersPercentage = ($totalActiveUsers / $totalUsers) * 100;

		}
		if(!empty($totalUsers) && !empty($totalInActiveUsers)){
        	$totalInActiveUsersPercentage = ($totalInActiveUsers / $totalUsers) * 100;
		}
		if(!empty($totalUsers) && !empty($totalSubscribedUsers)){
       		$totalSubscribedPercentage = ($totalSubscribedUsers / $totalUsers) * 100;
		}
 
       	$maleUsers = User::where('gender','male')->where('is_deleted',0)->whereIn('id',$userList)->count();
       	$femaleUsers = User::where('gender','female')->where('is_deleted',0)->whereIn('id',$userList)->count();
       	$doNotWishToSpecifyUsers = User::where('gender','Do not wish to specify')->where('is_deleted',0)->whereIn('id',$userList)->count();

       	$mPercentageUsers = $dnwtsPercentageUsers = $fPercentageUsers  = 0;
       	if(!empty($maleUsers) && !empty($totalUsers)){
       		$mPercentageUsers = ($maleUsers / $totalUsers) * 100;
       	}
       	if(!empty($femaleUsers) && !empty($totalUsers)){
       		$fPercentageUsers = ($femaleUsers / $totalUsers) * 100;
       	}
   		if(!empty($doNotWishToSpecifyUsers) && !empty($totalUsers)){
   			$dnwtsPercentageUsers = ($doNotWishToSpecifyUsers / $totalUsers) * 100;
   		}

        $playerList = Player::where('club',$authUser->id)->where('is_active',1)->pluck('full_name','id')->all();
        $totalAvailability = Availability::where('club',$authUser->id)->count();
        $totalVerifyUsers = VerifyUser::where('club_id',$authUser->id)->count();
        $totalScorers = ScorerAccess::where('club_id',$authUser->id)->count();
        $totalSponsors = Sponsor::where('user_id',$authUser->id)->count();
        $totalBrandingActive = Branding::where('club_id',$authUser->id)->where('is_paid',1)->count();
        $totalBrandingInActive = Branding::where('club_id',$authUser->id)->where('is_paid',0)->count();

        /*Fundraiser amount start here*/
        $fundraiserAmount = User::where('id',$authUser->id)->value('entry_price');
        $fundraiserTarget =  User::where('id',$authUser->id)->value('fundraising_target');
        $fundraiserRaised = Fundraiser::where('club_id',$authUser->id)->sum('amount');

        if($fundraiserTarget > 0){
	        $fundraiserAmountPercentage = (($fundraiserAmount/$fundraiserTarget)*100);
	        $fundraiserRaisedPercentage = (($fundraiserRaised/$fundraiserTarget)*100);
	    }else{
	    	$fundraiserAmountPercentage = 0;
	        $fundraiserRaisedPercentage = 0;
	    }

        /*FUndraiser amount finish here*/

        $aboutUsList = DropDown::get_master_list("about-us");

        $usersCountries = User::whereIn('users.id',$userList)->leftjoin('countries','countries.id','=','users.country')
        						->where('users.country','<>',0)
        						->select('countries.name as country_name', DB::raw('COUNT(users.country) as country_count'))
						    	->groupBy('users.country')
						    	->get();

		$usersStates = User::whereIn('users.id',$userList)->leftjoin('states','states.id','=','users.state')
        						->where('users.state','<>',0)
        						->select('states.name as states_name', DB::raw('COUNT(users.state) as states_count'))
						    	->groupBy('users.state')
						    	->get();
		// power data start 
		$trades_count = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 1)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_trades_count'))->pluck('total_trades_count', 'user_id' )->toArray(); 			
		$capton_card = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 2)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_capton_card'))->pluck('total_capton_card', 'user_id' )->toArray(); 
		$twelve_man_card = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 3)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_twelve_man_card'))->pluck('total_twelve_man_card', 'user_id')->toArray(); 
		$dealer_card = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 4)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_dealer_card'))->pluck('total_dealer_card', 'user_id')->toArray(); 
		$flipper_card = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 5)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_flipper_card'))->pluck('total_flipper_card', 'user_id')->toArray(); 
		$shield_steal = UserTeamLogs::where('club_id', Auth::guard('admin')->user()->id)->where('log_type', 6)->groupBy('user_id')->select('user_id', DB::raw('count(*) as total_shield_steal'))->pluck('total_shield_steal', 'user_id')->toArray(); 
		$all_users = UserTeams::where('club_id', Auth::guard('admin')->user()->id)->pluck('user_id', 'id')->toArray(); 
		$all_team_id_arr = array_keys($all_users); 
		$all_users = User::whereIn('id', $all_users)->get(); 
	// most picked captain start 
		$most_picked_capton = UserTeamsCVCTrack::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $all_team_id_arr)
		// ->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())
		->where('c_vc', 1)
		->groupBy('player_id')->orderBy('total', 'DESC')
		->pluck('total','player_id')->toArray();
		$most_picked_capton = array_key_first($most_picked_capton); 
		if(!empty($most_picked_capton))
			$most_picked_capton = Player::where('id', $most_picked_capton)->first(); 
	// most picked captain end 
	// most picked v.captain start 
		$most_picked_v_capton = UserTeamsCVCTrack::select('player_id', DB::raw('count(*) as total'))->whereIn('team_id', $all_team_id_arr)
		// ->whereDate('created_at', '>', Carbon::now()->subWeek()->startOfWeek())->whereDate('created_at', '<', Carbon::now()->subWeek()->endOfWeek())
		->where('c_vc', 0)
		->groupBy('player_id')->orderBy('total', 'DESC')
		->pluck('total','player_id')->toArray();
		// dump($most_picked_v_capton); die; 
		// dump($most_picked_v_capton); die; 
		$most_picked_v_capton = array_key_first($most_picked_v_capton); 
		if(!empty($most_picked_v_capton))
			$most_picked_v_capton = Player::where('id', $most_picked_v_capton)->first(); 
			// most picked v.captain end 
			// dump($all_team_id_arr);
			$most_trade_in = PlayerTradeInOut::whereIn('team_id', $all_team_id_arr)->whereNotNull('team_id')->where('in_out', 1)->groupBy('player_id')->select('player_id', DB::raw('count(*) as most_trade_in'))->orderby('most_trade_in', 'DESC')->pluck('most_trade_in', 'player_id')->toArray(); 
			$most_trade_out = PlayerTradeInOut::whereIn('team_id', $all_team_id_arr)->whereNotNull('team_id')->where('in_out', 0)->groupBy('player_id')->select('player_id', DB::raw('count(*) as most_trade_out'))->orderby('most_trade_out', 'DESC')->pluck('most_trade_out', 'player_id')->toArray(); 

			$most_trade_in = array_key_first($most_trade_in); 
					if(!empty($most_trade_in))
						$most_trade_in = Player::where('id', $most_trade_in)->first(); 

			$most_trade_out = array_key_first($most_trade_out); 
					if(!empty($most_trade_out))
						$most_trade_out = Player::where('id', $most_trade_out)->first(); 
		// gameweek player point start 
			$club_id = Auth::guard('admin')->user()->id; 
		if(!empty($club_id)){
			$club_data = User::where('id', $club_id)->first(); 
			if(!empty($club_data->lockout_start_date)){
				$cur_gameweek_no = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now());
				// $cur_gameweek_no++;
				$cur_gameweek_no--; 
			}else{
				$cur_gameweek_no = 0; 
			}
		}else{
			$cur_gameweek_no = 0; 
		}

		$most_last_gw = Carbon::parse($club_data->lockout_start_date)->startOfWeek()->diffInWeeks(Carbon::now()); 
		$gw_no = !empty(Input::get('gw_no')) && is_numeric(Input::get('gw_no')) &&  Input::get('gw_no') >= 0 ? Input::get('gw_no') : $cur_gameweek_no; 
		// end ***************************************** end 
		$top_player_by_gwk_point = array(); 
		$all_players = Player::where('club', $club_id)->pluck('id')->toArray(); 
		if(!empty($club_id) && !empty($all_players)){
			$temp_club_data = User::where('id', $club_id)->first(); 			
			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date); 
			// dump($temp_lockout_start_date); 
			$temp_lockout_start_date = $temp_lockout_start_date->addWeek($gw_no); 
			// dump($temp_lockout_start_date); 
			// dump(Carbon::parse($temp_lockout_start_date_month)->endOfMonth()); 
			$temp_gw_data = array(); 
			$temp_gw_data_month = array();
			foreach ($all_players as $key => $value) {
			$tmp_pnt = FixtureScorcard::where('player_id', $value)

				->whereHas('fixture', function($q) use ($temp_lockout_start_date){
							$q->where('start_date', '>=', $temp_lockout_start_date->copy()->startOfWeek())->where('start_date', '<=', $temp_lockout_start_date->copy()->endOfWeek()); 
						})->sum('fantasy_points'); // ->where('status', 3)
				$temp_gw_data[$value]['player_point'] = !empty($tmp_pnt) ? $tmp_pnt : 0 ; 
				// monthly calculation 
			}
			arsort($temp_gw_data);
			foreach ($temp_gw_data as $key => $value) {
				$temp_gw_data[$key]['player_data'] = Player::find($key); 
			}
		}
		// gameweek player point end 
		/*Top Traded In / traded out start here*/ 
		$top_trade_in = array(); 
		$top_trade_in = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->where('in_out', 1)
		->whereHas('player', function($q) use ($club_id) {
			$q->where('club', $club_id);
		})
		->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();

  
		$top_trade_out = array(); 
		$top_trade_out = PlayerTradeInOut::select('player_id', DB::raw('count(*) as total'))->where('in_out', 0)
		->whereHas('player', function($q) use ($club_id) {
			$q->where('club', $club_id);
		})
		->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();

		$top_trade_in_count = $top_trade_in ; 

		$top_trade_in = array_keys($top_trade_in); 

		$temp_top_trade_in = array(); 

		foreach ($top_trade_in as $key => $value) {
			$temp_top_trade_in[]=Player::where('id', $value)->first(); 
		}

		$top_trade_in = $temp_top_trade_in ; 
		/*Top Traded In / traded out finish here  full_name */ 
 
		/*Vice captain/captain section start here*/
		$top_captain_in = array(); 
		$top_captain_in = UserTeamsCVCTrack::select('player_id', DB::raw('count(*) as total'))->where('c_vc', 1)
		->whereHas('player', function($q) use ($club_id) {
			$q->where('club', $club_id);
		})
		->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();


		$top_vice_captain_out = array(); 
		$top_vice_captain_out = UserTeamsCVCTrack::select('player_id', DB::raw('count(*) as total'))->where('c_vc', 0)
		->whereHas('player', function($q) use ($club_id) {
			$q->where('club', $club_id);
		})
		->groupBy('player_id')->orderBy('total', 'DESC')->pluck('total','player_id')->toArray();

		$top_captain_in_count = $top_captain_in ; 

		$top_captain_in = array_keys($top_captain_in); 

		$temp_top_captain_in = array(); 

		foreach ($top_captain_in as $key => $value) {
			$temp_top_captain_in[]=Player::where('id', $value)->first(); 
		}

		$top_captain_in = $temp_top_captain_in ; 
		/*Vice captain/captain section finish here  top_trade_in_count  */





	$temp_club_data = User::where('id', Auth::guard('admin')->user()->id)->first(); 
			
			$temp_lockout_start_date = Carbon::parse($temp_club_data->lockout_start_date)->startOfWeek(); 
			$temp_lockout_end_date = Carbon::parse($temp_club_data->lockout_end_date)->endOfWeek(); 


			$i = 1; 
			$cur_gw_dta = array(); 
			$i = 1; 
			while (Carbon::parse($temp_lockout_start_date) < $temp_lockout_end_date) {
				// dump($temp_lockout_start_date); 
				if($temp_lockout_start_date >= Carbon::now()->startOfWeek() && $temp_lockout_start_date <= Carbon::now()->endOfWeek()){
					$cur_gw_dta['current'] =  $i; 
					$cur_gw_dta['gw_start'] = $temp_lockout_start_date->copy()->startOfWeek();
					$cur_gw_dta['gw_end'] =  $temp_lockout_start_date->copy()->endOfWeek();
				}
				$i++; 
				$temp_lockout_start_date->addWeek(); 

			}


			// dump($cur_gw_dta); die; 




		return View::make('admin.dashboard.club_dashboard',compact('premiumMember', 'total_user_cnt', 'total_player_cnt', 'total_fixture_cnt', 'total_team_cnt','price','offeredPrice','extraPlayer','extraPlayerPrice','totalPrice','totalGrades','fixtureCount','oneDayMatch','twoDayMatch','seniormens','seniorWomens','veterans','juniorBoyes','juniorGirls','playerScoreSum','playerList','totalAvailability','totalScorers','totalSponsors','totalVerifyUsers','totalBrandingActive','totalBrandingInActive','fundraiserAmount','fundraiserTarget','fundraiserRaised','recentUsers','totalActiveUsersPercentage','totalInActiveUsersPercentage','totalSubscribedPercentage','mPercentageUsers','fPercentageUsers','dnwtsPercentageUsers','aboutUsList','usersCountries', 'all_users', 'trades_count', 'capton_card', 'twelve_man_card', 'dealer_card', 'flipper_card', 'shield_steal', 'most_picked_capton', 'most_picked_v_capton', 'most_trade_in', 'most_trade_out','usersStates','fundraiserAmountPercentage','fundraiserRaisedPercentage', 'gw_no', 'temp_gw_data', 'most_last_gw','top_trade_in','top_trade_in_count', 'top_trade_out','top_captain_in','top_captain_in_count','top_vice_captain_out', 'cur_gw_dta'));
	}
/**
* Function for display admin account detail
*
* @param null
*
* @return view page. 
*/
	public function myaccount(){
		return  View::make('admin.dashboard.myaccount');
	}// end myaccount()
/**
* Function for change_password
*
* @param null
*
* @return view page. 
*/	
	public function change_password(){
		return  View::make('admin.dashboard.change_password');
	}// end myaccount()
/**
* Function for update admin account update
*
* @param null
*
* @return redirect page. 
*/
	public function myaccountUpdate(){
		$thisData				=	Input::all(); 
		Input::replace($this->arrayStripTags($thisData));
		$old_password     		= 	Input::get('old_password');
        $password        		= 	Input::get('new_password');
        $confirm_password 		= 	Input::get('confirm_password');
		$ValidationRule = array(
            'full_name' 		=> 'required',
            'email' 			=> 'required|email',
        );
        $validator 				= 	Validator::make(Input::all(), $ValidationRule);
		if ($validator->fails()){	
			return Redirect::back()
				->withErrors($validator)->withInput();
		}else{
			$user 				= 	User::find(Auth::user()->id);
			$user->full_name 	= 	Input::get('full_name'); 
			$user->email	 	= 	Input::get('email'); 
			if($user->save()) {
				return Redirect::intended('admin/myaccount')
					->with('success', 'Information updated successfully.');
			}
		}
	}// end myaccountUpdate()
/**
* Function for changedPassword
*
* @param null
*
* @return redirect page. 
*/	
	public function changedPassword(){
		$thisData				=	Input::all(); 
		Input::replace($this->arrayStripTags($thisData));
		$old_password    		= 	Input::get('old_password');
        $password         		= 	Input::get('new_password');
        $confirm_password 		= 	Input::get('confirm_password');
		Validator::extend('custom_password', function($attribute, $value, $parameters) {
			if (preg_match('#[0-9]#', $value) && preg_match('#[a-zA-Z]#', $value) && preg_match('#[\W]#', $value)) {
				return true;
			} else {
				return false;
			}
		});
		$rules        		  	= 	array(
			'old_password' 		=>	'required',
			'new_password'		=>	'required|min:8|custom_password',
			'confirm_password'  =>	'required|same:new_password'
		);
		$validator 				= 	Validator::make(Input::all(), $rules,
		array(
			"new_password.custom_password"	=>	"Password must have combination of numeric, alphabet and special characters.",
		));
		if ($validator->fails()){	
			return Redirect::to('admin/change-password')
				->withErrors($validator)->withInput();
		}else{
			$user 				= User::find(Auth::user()->id);
			$old_password 		= Input::get('old_password'); 
			$password 			= Input::get('new_password');
			$confirm_password 	= Input::get('confirm_password');
			if($old_password !=''){
				if(!Hash::check($old_password, $user->getAuthPassword())){
					/* return Redirect::intended('change-password')
						->with('error', 'Your old password is incorrect.');
						 */
					Session::flash('error',trans("Your old password is incorrect."));
					return Redirect::to('admin/change-password');
				}
			}
			if(!empty($old_password) && !empty($password ) && !empty($confirm_password )){
				if(Hash::check($old_password, $user->getAuthPassword())){
					$user->password = Hash::make($password);
				// save the new password
					if($user->save()) {
						Session::flash('success',trans("Password changed successfully."));
						return Redirect::to('admin/change-password');
					}
				} else {
					/* return Redirect::intended('change-password')
						->with('error', 'Your old password is incorrect.'); */
					Session::flash('error',trans("Your old password is incorrect."));
					return Redirect::to('admin/change-password');
				}
			}else{
				$user->username = $username;
				if($user->save()) {
					Session::flash('success',trans("Password changed successfully."));
					return Redirect::to('admin/change-password');
					/* return Redirect::intended('change-password')
						->with('success', 'Password changed successfully.'); */
				}
			}
		}
	}// end myaccountUpdate()
/* 
* For User Listing Demo 
*/
	public function usersListing(){
		return View::make('admin.user.user');
	}
	public function setUpChecklist(){
		$checklistDetails = Checklist::where('is_active',1)->get();;
		return View::make('admin.dashboard.set_up_checklist',compact('checklistDetails'));
	}
} //end AdminDashBoardController()
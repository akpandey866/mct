<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\User;
use App\Model\EmailTemplate;
use App\Model\DropDown;
use App\Model\EmailAction;
use App\Model\UserTeamPlayers; 
use App\Model\FixtureScorcard; 
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\UserTeams;
use mjanssen\BreadcrumbsBundle\Breadcrumbs as Breadcrumb;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator;

/**
* Users Controller
*
* Add your methods in the class below
*
* This file will render views from views/admin/usermgmt
*/
 
class UsersController extends BaseController {

/**
* Function for display list of all users
*
* @param null
*
* @return view page. 
*/
	public function listUsers($user_role_id = 0,Request $request){
		$DB 					= 	User::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		$sort_by_data			=	Input::get("sortBy");
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) { 
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			if((!empty($searchData['start_from'])) && (!empty($searchData['start_to']))){
					$dateS = $searchData['start_from'];
					$dateE = $searchData['start_to'];
					$DB->whereBetween('users.created_at', [$dateS." 00:00:00", $dateE." 23:59:59"]); 						
				}elseif(!empty($searchData['start_from'])){
					$dateS = $searchData['start_from'];
					$DB->whereBetween('users.created_at', [$dateS." 00:00:00", $dateS." 23:59:59"]); 
			}elseif(!empty($searchData['start_to'])){
					$dateE = $searchData['start_to'];
					$DB->whereBetween('users.created_at', [$dateE." 00:00:00", $dateE." 23:59:59"]); 			
			}
			unset($searchData['start_from']);
			unset($searchData['start_to']);
			// dump($searchData); die; 
			foreach($searchData as $fieldName => $fieldValue){ 
				if(isset($fieldValue)){
					if($fieldName == 'my_team_name'){
						$DB->whereHas('user_teams', function($q) use($fieldName, $fieldValue)
			              {
			                  $q->where("$fieldName",'like','%'.$fieldValue.'%');

			              }); 
					}if($fieldName == 'is_game_activate'){ 
						$DB->where("users.$fieldName",'=',$fieldValue);
					}if($fieldName == 'club'){

					}else{
						$DB->where("users.$fieldName",'like','%'.$fieldValue.'%');
					}

					// dump($DB->get()); die; 
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';

		if(auth()->guard('admin')->user()->user_role_id == SUPER_ADMIN_ROLE_ID){
			if(!empty($request->club)){
				$userList = UserTeams::where('club_id',$request->club)
								->where('user_teams.is_active',1)
								->pluck('user_id')
								->all();
				$result = 	$DB
							->whereIn('users.id',$userList)
							->orderBy($sortBy,$order)
							->paginate(Config::get("Reading.record_game_per_page"));
			}else{
				$result = 	$DB
							->select('users.*')
							->where('users.user_role_id','<>',SUPER_ADMIN_ROLE_ID)
							->where('user_role_id',$user_role_id)
							->where('users.is_deleted',0)
							->orderBy($sortBy,$order)
							->paginate(Config::get("Reading.records_per_page"));
			}
			
		}else{
			$userList = UserTeams::where('club_id',Auth::guard('admin')->user()->id)
								->where('user_teams.is_active',1)
								->pluck('user_id')
								->all();
								// prd($userList);
			$result = 	$DB
						->whereIn('users.id',$userList)
						->orderBy($sortBy,$order)
						->paginate(Config::get("Reading.record_game_per_page"));
		}

								
		$totalUsers				=	User::where('is_deleted',0)->where('user_role_id','!=',SUPER_ADMIN_ROLE_ID)->where('user_role_id',$user_role_id)->count();
		
		$thisYearServiceSeeker	=	User::whereBetween('created_at',[date("y-01-01").' 00:00:00',date("y-m-d").' 23:59:59'])->where('is_deleted',0)->where('user_role_id','!=',SUPER_ADMIN_ROLE_ID)->where('user_role_id',$user_role_id)->count();
		
		$lastMonthServiceSeeker	=	User::whereBetween('created_at',[date("y-m-01", strtotime("-1 month")).' 00:00:00',date("y-m-31", strtotime("-1 month")).' 23:59:59'])->where('user_role_id','!=',SUPER_ADMIN_ROLE_ID)->where('is_deleted',0)->where('user_role_id',$user_role_id)->count();
		
		$thisMonthServiceSeeker	=	User::whereBetween('created_at',[date("y-m-01").' 00:00:00',date("y-m-d").' 23:59:59'])->where('user_role_id','!=',SUPER_ADMIN_ROLE_ID)->where('is_deleted',0)->where('user_role_id',$user_role_id)->count();
		
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]); 
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		if(auth()->guard('admin')->user()->user_role_id == 1){
			return  View::make('admin.usermgmt.index', compact('result', 'result_filter' ,'searchVariable','sortBy','order','userType','query_string','country_list','totalUsers','thisYearServiceSeeker','lastMonthServiceSeeker','thisMonthServiceSeeker','dateE','dateS','sort_by_data','user_role_id', 'temp_points_arr', 'team_ranks_arr'));
		}else{
			return  View::make('admin.usermgmt.club_user_index', compact('result', 'result_filter' ,'searchVariable','sortBy','order','userType','query_string','country_list','totalUsers','thisYearServiceSeeker','lastMonthServiceSeeker','thisMonthServiceSeeker','dateE','dateS','sort_by_data','user_role_id', 'temp_points_arr', 'team_ranks_arr'));
		}
		
	}// end listUsers()
/**
* Function for add users
*
* @param null
*
* @return view page. 
*/	
	public function addUser($user_role_id = 0){
		$aboutUsList = DropDown::get_master_list("about-us");
		$countryList = Country::get_country();
		return  View::make('admin.usermgmt.add',compact('user_role_id','aboutUsList','countryList'));
	}
/**
* Function for save added users
*
* @param null
*
* @return view page. 
*/	
	public function saveUser(){
		Input::replace($this->arrayStripTags(Input::all()));
		$formData						=	Input::all();
		Validator::extend('custom_password', function($attribute, $value, $parameters) {
			if(preg_match('/^(?=.*[a-z])(?=.*\d).{8,}$/', $value)) {
				return true;
			} else {
				return false;
			}
		});
		if(!empty($formData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'first_name'				=> 'required',
					'last_name'					=> 'required',
					'email' 					=> 'required|email|unique:users',
					'password'					=> 'required|min:8|custom_password',
					'confirm_password' 			=> 'required|min:8|same:password', 
					'username'					=> 'required|unique:users',
				),array(
					"password.custom_password"	=>	'Password must have be a combination of numeric, alphabet and special characters.'
				)
			);
			$password 					= 	Input::get('password');
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
				$first_name	=  Input::get('first_name');
				$last_name =  Input::get('last_name');
				$fullName =  $first_name." ".$last_name;
				$obj =  new User;
				$validateString	=  md5(time() . Input::get('email'));
				$obj->validate_string =  $validateString;					
				$obj->first_name =  $first_name;
				$obj->last_name =  $last_name; 
				$obj->full_name =  $fullName;
				$obj->email =  Input::get('email');
				$obj->slug	=  $this->getSlug($fullName,'full_name','User');
				$obj->password =  Hash::make(Input::get('password'));
				$obj->address =  Input::get('address');
				$obj->phone	=  Input::get('phone');
				$obj->country	=  !empty(Input::get('country')) ? Input::get('country'):0;
				$obj->state	=  !empty(Input::get('state')) ? Input::get('state'):0;
				$obj->city	=  !empty(Input::get('city')) ? Input::get('city'):0;
				$obj->zipcode	=  !empty(Input::get('zipcode')) ? Input::get('zipcode'):0;

				/*Club Details*/
				$obj->game_mode	=  Input::get('home_club');
				$obj->club_name	=  Input::get('club_name');
				$obj->game_name	=  Input::get('game_name');
				$obj->sport_name	=  Input::get('Cricket');
				/*Club Details*/
				
				$obj->about_us	=  Input::get('about_us');
				$obj->gender	=  Input::get('gender');
				$obj->dob	=  !empty(Input::get('dob')) ? Input::get('dob'):'0000-00-00';
				$obj->username	=  Input::get('username');

				$obj->user_role_id				=  2;
				$obj->is_verified				=  1; 
				$obj->is_active					=  1; 
				
				if(input::hasFile('profile_image')){
					 $extension  = Input::file('profile_image')->getClientOriginalExtension();
					$newFolder  = strtoupper(date('M') . date('Y')) . '/';
					$folderPath = USER_PROFILE_IMAGE_ROOT_PATH . $newFolder;
					
					if (!File::exists($folderPath)) {
						File::makeDirectory($folderPath, $mode = 0777, true);
					}
					$userImages = time() . '-customer-profile.' . $extension;
					$image            = $newFolder . $userImages;
					if (Input::file('profile_image')->move($folderPath, $userImages)) {
						$obj->image = $image;
					}
				}
				
				$obj->save();
				$userId					=	$obj->id;		
				if(!$userId) {
					DB::rollback();
					Session::flash('error', trans("Something went wrong.")); 
					return Redirect::back()->withInput();
				}				
				$encId					=	md5(time() . Input::get('email'));
				//mail email and password to new registered user
				$settingsEmail 			=	Config::get('Site.email');
				$full_name				= 	$obj->full_name; 
				$email					= 	$obj->email;
				$password				= 	Input::get('password');
				$route_url     			= 	URL::to('/');
				$click_link   			=   $route_url;
				$emailActions			= 	EmailAction::where('action','=','user_registration')->get()->toArray();
				$emailTemplates			= 	EmailTemplate::where('action','=','user_registration')->get(array('name','subject','action','body'))->toArray();
				$cons 					= 	explode(',',$emailActions[0]['options']);
				$constants 				= 	array();
				foreach($cons as $key => $val){
					$constants[] 		= 	'{'.$val.'}';
				} 
				$subject 				= 	$emailTemplates[0]['subject'];
				$rep_Array 				= 	array($full_name,$email,$password,$click_link,$route_url); 
				$messageBody			= 	str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
				//$mail					= 	$this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);	
				Session::flash('success',trans("User has been added successfully"));
				return Redirect::to('admin/users/2');
			}
		}
	}// saveUser()

	/**
* Function for display user detail
*
* @param $userId 	as id of user
*
* @return view page. 
*/
	public function viewUser($userId = 0){
		$userDetails	=	DB::table("users")
							->leftJoin('countries','users.country','=','countries.id')
							->leftJoin('states','users.state','=','states.id')
							->leftJoin('cities','users.state','=','cities.id')
							->leftJoin('dropdown_managers','users.about_us','=','dropdown_managers.id')
							->select('users.*','countries.name as country','states.name as state','cities.name as city','dropdown_managers.name as aboutus')
							->where("users.id",$userId)
							->first();
		if(empty($userDetails)) {
			return Redirect::to('admin/users');
		}
	
		return View::make('admin.usermgmt.view', compact('userDetails'));
	} // end viewUser()
/**
* Function for display page for edit user
*
* @param $userId as id of user
*
* @return view page. 
*/
	public function editUser($userId = 0){
		$userDetails			=	User::find($userId);
		$aboutUsList = DropDown::get_master_list("about-us");
		$countryList = Country::get_country(); 
		if(empty($userDetails)) {
			return Redirect::to('admin/users');
		}
		return View::make('admin.usermgmt.edit', compact('userDetails','aboutUsList','countryList'));
	} // end editUser()
/**
* Function for update user detail
*
* @param $userId as id of user
*
* @return redirect page. 
*/
	public function updateUser($userId = 0){ 
		$userDetails	=	User::find($userId); 
		if(empty($userDetails)) {
			return Redirect::to('admin/users');
		}
		
		Input::replace($this->arrayStripTags(Input::all()));
		$formData						=	Input::all();
		if(!empty($formData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'first_name' => 'required',
					'last_name'	=> 'required',
					'email' => "required|unique:users,email,$userId",
				)
			);
			if(!empty($thisData['password'])){
				$validator 					=	Validator::make(
					Input::all(),
					array(
						'password'			=> 'required|min:8',
						'confirm_password'  => 'required|min:8|same:password'
					),array(
						"password.custom_password"	=>	trans("messages.home.password_must_have_combination")
					)
				);
			}
			if ($validator->fails()){
				 return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
			
				$first_name						=  Input::get('first_name');
				$last_name						=  Input::get('last_name');
				$fullName						=  $first_name." ".$last_name;
				$obj 							=  User::find($userId);
				$validateString					=  md5(time() . Input::get('email'));
				$obj->validate_string			=  $validateString;					
				$obj->full_name 				=  $fullName;
				$obj->first_name 				=  Input::get('first_name');
				$obj->last_name 				=  Input::get('last_name');
				$obj->address =  Input::get('address');
				$obj->phone	=  Input::get('phone');  
				$obj->country =  !empty(Input::get('country')) ? Input::get('country'):0;
				$obj->state	=  !empty(Input::get('state')) ? Input::get('state'):0;
				$obj->city	=  !empty(Input::get('city')) ? Input::get('city'):0;
				$obj->zipcode =  !empty(Input::get('zipcode')) ? Input::get('zipcode'):0;
				$obj->home_club	=  Input::get('home_club');
				$obj->about_us =  Input::get('about_us');
				$obj->gender =  Input::get('gender');
				$obj->dob =  !empty(Input::get('dob')) ? Input::get('dob'):'0000-00-00';
				$obj->username	=  Input::get('username');



				if(!empty(Input::get('password')) && Input::get('password') != ""){
					$obj->password	 			=  Hash::make(Input::get('password'));
				}else {
					$obj->password	 			=  $userDetails->password;
				}
				$obj->address					=  Input::get('address');
				if(input::hasFile('profile_image')){
					$extension 	=	 Input::file('profile_image')->getClientOriginalExtension();
					$fileName	=	time().'-customer-profile.'.$extension;
					if(Input::file('profile_image')->move(USER_PROFILE_IMAGE_ROOT_PATH, $fileName)){
						$obj->image			=	$fileName;
					}
				}
				$obj->save();
				Session::flash('success',trans("User has been updated successfully"));
				return Redirect::to('admin/users/2');
			}
		}
	}// end updateUser()
/**
* Function for mark a user as deleted 
*
* @param $userId as id of user
*
* @return redirect page. 
*/
	public function deleteUser($userId = 0){
		$userDetails	=	User::find($userId); 
		if(empty($userDetails)) {
			return Redirect::to('admin/users');
		}
		if($userId){		
			$email 						=	'delete_'.$userId .'_'.$userDetails->email;
			$username 					=	'delete_'.$userId .'_'.$userDetails->username;
			$userModel					=	User::where('id',$userId)->update(array('is_deleted'=>1,'email'=>$email,'username'=>$username));
			Session::flash('flash_notice',trans("User has been removed successfully")); 
		}
		return Redirect::to('admin/users');
	} // end deleteUser()
/**
* Function for update user status
*
* @param $userId as id of user
* @param $userStatus as status of user
*
* @return redirect page. 
*/
	public function updateUserStatus($userId = 0, $userStatus = 0){
		if($userStatus == 0	){
			$statusMessage	=	trans("User deactivated successfully");
		}else{
			$statusMessage	=	trans("User activated successfully");
		}
		$this->_update_all_status('users',$userId,$userStatus);
		Session::flash('flash_notice', $statusMessage); 
		return Redirect::back();
	} // end updateUserStatus()
/**
* Function for verify user
*
* @param $userId as id of user
*
* @return redirect page. 
*/
	public function verifiedUser($userId = 0, $userStatus = 0){
		if($userStatus == 0	){
			$statusMessage	=	trans("User not verified successfully");
			User::where('id', '=', $userId)->update(array('is_verified' =>0));
		}else{
			$statusMessage	=	trans("User verified successfully");
			User::where('id', '=', $userId)->update(array('is_verified' => 1));
		}
		Session::flash('flash_notice', $statusMessage); 
			
		return Redirect::back();
	} // end verifiedUser()
/**
* Function for send credential to user
*
* @param $id as id of users
*
* @return redirect page. 
*/
	public function sendCredential($id){
		$obj			=	User::find($id);
		$settingsEmail 	= 	Config::get('Site.email');
		//$full_name		= 	$obj->full_name; 
		$username		= 	$obj->username;
		$email			= 	$obj->email;
		$password		=	substr(uniqid(rand(10,1000),false),rand(0,10),8);
		$obj->password	=	Hash::make($password);
		$obj->save();
		$route_url      =	URL::to('/');
		$click_link   	=   $route_url;
		$emailActions	= 	EmailAction::where('action','=','send_login_credentials')->get()->toArray();
		$emailTemplates	= 	EmailTemplate::where('action','=','send_login_credentials')->get(array('name','subject','action','body'))->toArray();
		$cons 			= 	explode(',',$emailActions[0]['options']);
		$constants 		= 	array();
		foreach($cons as $key => $val){
			$constants[] = '{'.$val.'}';
		} 
		$subject 		= 	$emailTemplates[0]['subject'];
		$rep_Array 		= 	array($username,$username,$password,$click_link,$route_url); 
		$messageBody	= 	str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
		$mail			= 	$this->sendMail($email,$username,$subject,$messageBody,$settingsEmail);
		Session::flash('flash_notice', trans("Login credientials send successfully"));
		return Redirect::back();
	}	
	
	public function getState(){
		$country_id = Input::get('country_id');
		$stateList = State::get_states($country_id);
		$state_old_id = !empty(Input::get('state_old_id')) ? Input::get('state_old_id') : '';
		return View::make('admin.usermgmt.state',compact('stateList','state_old_id','country_id'));
	}
	public function getCity(){
		$state_id = Input::get('state_id');
		$cityList = City::get_cities($state_id);
		$city_old_id = !empty(Input::get('city_old_id')) ? Input::get('city_old_id') : '';
		return View::make('admin.usermgmt.city',compact('cityList','city_old_id','state_id'));
	}
	public function userReferList($user_id = 0){
		$DB				=	User::query();
		$searchVariable	=	array(); 
		$inputGet		=	Input::get();
		if (Input::get()) { 
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		
		$sortBy = (Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  = (Input::get('order')) ? Input::get('order')   : 'DESC';
		$result = $DB->where('user_referral_id',$user_id)
					->orderBy($sortBy, $order)
					->paginate(Config::get("Reading.records_per_page")); 
		
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		return  View::make('admin.usermgmt.user_refer',compact('result','searchVariable','sortBy','order','user_id','query_string'));
	}
}//end UsersController

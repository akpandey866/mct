<?php
/**

* Forum Controller

*/
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\GamePoint;
use App\Model\User;
use App\Model\Club;
use App, Auth, Blade, Config, Cache, Cookie, DB, File, Hash, Input, Mail, mongoDate, Redirect, Request, Response, Session, URL, View, Validator;
use Carbon\Carbon;
class GamePointsController extends BaseController
{
    /**
    
    * Function for display all event
    
    *
    
    * @param null
    
    *
    
    * @return view page.
    
    */
    public function index()
    {
        $DB             = GamePoint::query();
        $searchVariable = array();
        $inputGet       = Input::get();
        /* seacrching on the basis of username and email */
        if ((Input::get()) || isset($inputGet['display']) || isset($inputGet['page'])) {
            $searchData = Input::get();
            unset($searchData['display']);
            unset($searchData['_token']);
            if (isset($searchData['order'])) {
                unset($searchData['order']);
            }
            if (isset($searchData['sortBy'])) {
                unset($searchData['sortBy']);
            }
            if (isset($searchData['page'])) {
                unset($searchData['page']);
            }
            $date_from = '';
            $date_to   = '';
            foreach ($searchData as $fieldName => $fieldValue) {
                if (!empty($fieldValue) || $fieldValue == 0) {
                    $DB->where("$fieldName", 'like', '%' . $fieldValue . '%');
                }
                $searchVariable = array_merge($searchVariable, array(
                    $fieldName => $fieldValue
                ));
            }
        }
        $sortBy = (Input::get('sortBy')) ? Input::get('sortBy') : 'created_at';
        $order  = (Input::get('order')) ? Input::get('order') : 'ASC';
        if (Auth::guard('admin')->user()->user_role_id == 1) {
        	if(!empty($searchData['club'])){
           	 	$result = $DB->orderBy($sortBy, $order)->paginate(70);
        	}else{
        		$result = $DB->orderBy($sortBy, $order)->where('club',0)->paginate(70);
        	}
        } else {
            $rowCount = GamePoint::where('club', Auth::guard('admin')->user()->id)->count();

            if ($rowCount == 0) {
                $defaultData = GamePoint::where('club', 0)->orderBy('id', 'ASC')->get();
                if (!$defaultData->isEmpty()) {
                    foreach ($defaultData as $key => $value) {
                        $gameObj                 = new GamePoint;
                        $gameObj->attribute_name = $value->attribute_name;
                        $gameObj->attribute_key = $value->attribute_key;
                        $gameObj->bats_wk_ar     = $value->bats_wk_ar;
                        $gameObj->bowler         = $value->bowler;
                        $gameObj->club           = Auth::guard('admin')->user()->id;
                        $gameObj->save();
                    }
                }
            }
            $result = $DB->where('club', Auth::guard('admin')->user()->id)->orderBy('id', 'ASC')->paginate(70);
        }
        $complete_string = Input::query();
        unset($complete_string["sortBy"]);
        unset($complete_string["order"]);
        $query_string = http_build_query($complete_string);
        $result->appends(Input::all())->render();
        $userList    = User::where('is_active', 1)->where('is_deleted', 0)->where('user_role_id', 2)->pluck('full_name', 'full_name')->all();
        $club        = new Club();
        $cludDetails = $club->get_club_list();

        $userDetails = Auth::guard('admin')->user();
        $SeasonStartDate ='';
        if($userDetails->user_role_id !=1){ 
          $SeasonStartDate = strtotime($userDetails->lockout_start_date);
        }

        return View::make('admin.game_points.index', compact('result', 'searchVariable', 'sortBy', 'order', 'query_string', 'cludDetails','SeasonStartDate'));
    }
    /**
    
    * Function for edit Gamepoints
    
    *
    
    * @param null
    
    *
    
    * @return view page. 
    
    */
    public function editGamePoint($id = 0)
    {
        $result = GamePoint::find($id);
        return View::make('admin.game_points.edit', compact('id', 'result'));
    } // 
    /**
    
    * Function for update GamePoint
    
    *
    
    * @param null
    
    *
    
    * @return view page. 
    
    */
    public function updateGamePoint()
    {	
        Input::replace($this->arrayStripTags(Input::all()));
        $thisData = Input::all();
        //prd($thisData['data']);die;
        foreach ($thisData['data'] as $key => $value) {
            $obj             = GamePoint::find($key);
            $obj->bowler     = $value['bowler'];
            $obj->bats_wk_ar = $value['bats_wk_ar'];
            $obj->save();
        }
        Session::flash('flash_notice', trans("Scorecards has been updated successfully."));
        return Redirect::back();

        /*$response = array(
            'success' => '1',
            'message' => trans("Points successfully updated successfully.")
        );
        Session::flash('flash_notice', trans("Game Points has been updated successfully."));
        return Response::json($response);*/
    } //end update Forum-manager
    /**
    
    * Function for change is_active of Forum
    
    *
    
    * @param $Id as id of Forum
    
    * @param $GamePoint is_active as is_active of Forum
    
    *
    
    * @return redirect page. 
    
    */
    public function updateStatus($Id = 0, $Status = 0)
    {
        GamePoint::where('id', '=', $Id)->update(array(
            'is_active' => $Status
        ));
        Session::flash('flash_notice', trans("Status updated successfully."));
        return Redirect::to('admin/game-points');
    } // end updateStatus()
    /**
    
    /**
    
    * Function for mark a Forum as deleted 
    
    *
    
    * @param $Id as id of Forum
    
    *
    
    * @return redirect page. 
    
    */
    public function deleteGamePoint($Id = 0)
    {
        $userDetails = GamePoint::findOrFail($Id);
        $userModel   = GamePoint::where('id', $Id)->delete();
        Session::flash('flash_notice', trans("Game Points has been removed successfully"));
        return Redirect::to('admin/game-slider');
    } // end deleteForum
} // end ClubController class
<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\DropDown;
use App\Model\Language;
use App\Model\DropDownDescription;
use Illuminate\Support\Facades\Input;
use mjanssen\BreadcrumbsBundle\Breadcrumbs as Breadcrumb;
use Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
/**
* DropDownController Controller
*
* Add your methods in the class below
*
* This file will render views from views/dropdown
*/
	class DropDownController extends BaseController {
/**
* Function for display all DropDown    
*
* @param $type as category of dropdown 
*
* @return view page. 
*/
	public function listDropDown($type=''){
		if(empty($type)) {
			return Redirect::to('admin/dashboard');
		}
		$DB				=	DropDown::query()->where('dropdown_type',$type);
		$searchVariable	=	array(); 
		$inputGet		=	Input::get();
		if (Input::get()) { 
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					$DB->where("$fieldName",'like','%'.$fieldValue.'%');
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		
		$sortBy = (Input::get('sortBy')) ? Input::get('sortBy') : 'updated_at';
	    $order  = (Input::get('order')) ? Input::get('order')   : 'DESC';
		$result = $DB->orderBy($sortBy, $order)->paginate(Config::get("Reading.records_per_page")); 
		
		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		
		return  View::make('admin.dropdown.index',compact('result','searchVariable','sortBy','order','type','query_string'));
	}// end listDropDown()
/**
* Function for display page  for add new DropDown  
*
* @param $type as category of dropdown 
*
* @return view page. 
*/
	public function addDropDown($type=''){	
		return  View::make('admin.dropdown.add',compact('type'));
	} //end addDropDown()
/**
* Function for save added DropDown page
*
* @param null
*
* @return redirect page. 
*/
	function saveDropDown($type=''){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData										=	Input::all();
		$validator = Validator::make(
			array(
				'name' 			=>  Input::get('name'),
				'dropdown_type'	=>	$type,
				
			),	
			array(
				'name' 			=> 'required',
				
			)
		);
		if ($validator->fails()){	
			return Redirect::to('admin/dropdown-manager/add-dropdown/'.$type)
				->withErrors($validator)->withInput();
		}else{
			$dropdown = new DropDown;
			$dropdown->slug    							= 	$this->getSlugWithoutModel($type ,'slug', 'dropdown_managers');
			$dropdown->name    							= 	Input::get('name');
			$dropdown->dropdown_type    				= 	$type;
			$dropdown->save(); 
			Session::flash('flash_notice', trans(ucfirst($type).' added successfully')); 
			return Redirect::to('admin/dropdown-manager/'.$type);
		}
	}//end saveDropDown()
/**
* Function for display page  for edit DropDown page
*
* @param $Id ad id of DropDown 
* @param $type as category of dropdown 
*
* @return view page. 
*/	
	public function editDropDown($Id,$type){
		$dropdown				=	DropDown::find($Id);
		if(empty($dropdown)) {
			return Redirect::to('admin/dropdown-manager/'.$type);
		}
		$dropdown	=	DropDown::where('id', '=',  $Id)->first();
		return  View::make('admin.dropdown.edit',compact('dropdown','type'));
	}// end editDropDown()
/**
* Function for update DropDown 
*
* @param $Id ad id of DropDown 
* @param $type as category of dropdown 
*
* @return redirect page. 
*/
	function updateDropDown($Id,$type=''){
		Input::replace($this->arrayStripTags(Input::all()));
		$validator 										= 	Validator::make(
			array(
				'name' 		=> Input::get('name'),
			),
			array(
				'name' 		=> 'required',
			)
		);
		if ($validator->fails()){	
			return Redirect::to('admin/dropdown-manager/edit-dropdown/'.$Id.'/'.$type)
				->withErrors($validator)->withInput();
		}else{
			$dropdown = 	DropDown:: find($Id);
			$dropdown->name	= 	Input::get('name');
			$dropdown->save();
			$dropdownId	=	$dropdown->id;
			$dropdownId	=	$Id;
			Session::flash('flash_notice',trans(ucfirst($type)." updated successfully")); 
			return Redirect::intended('admin/dropdown-manager/'.$type);
		}
	}// end updateDropDown()
/**
* Function for update DropDown  status
*
* @param $Id as id of DropDown 
* @param $Status as status of DropDown 
* @param $type as category of dropdown 
*
* @return redirect page. 
*/	
	public function updateDropDownStatus($Id = 0, $Status = 0,$type=''){
		if($Status == 0	){
			$statusMessage	=	trans(ucfirst($type)." deactivated successfully");
		}else{
			$statusMessage	=	trans(ucfirst($type)." activated successfully");
		}
		$this->_update_all_status('dropdown_managers',$Id,$Status);
		
		/* if($Status == 1){
			$message				=	trans("messages.master.master_activate_message");
		}else{
			$message				=	trans("messages.master.master_deactivate_message");
		}
		$model						=	DropDown::find($Id);
		$model->is_active			=	$Status;
		$model->save(); */
		Session::flash('flash_notice',$statusMessage); 
		return Redirect::to('admin/dropdown-manager/'.$type);
	}// end updateDropDownStatus()
/**
* Function for delete DropDown 
*
* @param $Id as id of DropDown 
* @param $type as category of dropdown 
*
* @return redirect page. 
*/	
	public function deleteDropDown($Id = 0,$type=''){
		$dropdown					=	DropDown::find($Id) ;
		if(!empty($dropdown)){
			$this->_delete_table_entry('dropdown_managers',$Id,'id');
			Session::flash('flash_notice', trans(ucfirst($type)." removed successfully"));  
		}else{
			Session::flash('error', trans("Invalid url"));  
		}
		return Redirect::to('admin/dropdown-manager/'.$type);
	}// end deleteDropDown()

}// end DropDownController
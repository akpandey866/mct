<?php
/**
 * Forum Controller
 */
namespace App\Http\Controllers\admin;
use App\Http\Controllers\BaseController;
use App\Model\Team;
use App\Model\ForumComment;
use App\Model\User;
use App\Model\Club;
use App\Model\DropDown;
use App\Model\Grade;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Hash,Input,Mail,mongoDate,Redirect,Request,Response,Session,URL,View,Validator;
use Carbon\Carbon;

class TeamController extends BaseController {
/**
* Function for display all event
*
* @param null
*
* @return view page.
*/
	public function index(){
		$DB 					= 	Team::query();
		$DB1 					= 	User::query();
		$DB2 					= 	DropDown::query();
		$searchVariable			=	array(); 
		$inputGet				=	Input::get();
		/* seacrching on the basis of username and email */ 
		if ((Input::get()) ||isset($inputGet['display']) || isset($inputGet['page']) ) {
			$searchData			=	Input::get();
			unset($searchData['display']);
			unset($searchData['_token']);

			if(isset($searchData['order'])){
				unset($searchData['order']);
			}
			if(isset($searchData['sortBy'])){
				unset($searchData['sortBy']);
			}
			if(isset($searchData['page'])){
				unset($searchData['page']);
			}
			$date_from	=	'';
			$date_to	=	'';
			
			
			foreach($searchData as $fieldName => $fieldValue){
				if(!empty($fieldValue) || $fieldValue==0){
					if($fieldName=="is_active"){
						$DB->where("teams.is_active",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="name"){ 
						$DB->where("teams.name",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="game_mode"){
						$DB->where("game_mode",'like','%'.$fieldValue.'%'); 
					}if($fieldName=="game_name"){
						$DB->where("game_name",'like','%'.$fieldValue.'%'); 
					}
				}
				$searchVariable	=	array_merge($searchVariable,array($fieldName => $fieldValue));
			}
		}
		$sortBy 				= 	(Input::get('sortBy')) ? Input::get('sortBy') : 'teams.created_at';
	    $order  				= 	(Input::get('order')) ? Input::get('order')   : 'DESC';

	    if(Auth::guard('admin')->user()->user_role_id == 1){
			$result = 	$DB->leftJoin('users' , 'teams.club' , '=' , 'users.id')
							->leftJoin('dropdown_managers' , 'teams.team_category' , '=' , 'dropdown_managers.id')
							->select('teams.*','teams.name as team_name','users.club_name','dropdown_managers.name')
							->orderBy($sortBy, $order)
							->paginate(Config::get("Reading.records_per_page"));
		}else{
			$result = 	$DB->leftJoin('users' , 'teams.club' , '=' , 'users.id')
						->leftJoin('dropdown_managers' , 'teams.team_category' , '=' , 'dropdown_managers.id')
						->select('teams.*','teams.name as team_name','users.club_name','dropdown_managers.name')
						->where('teams.club',Auth::guard('admin')->user()->id)
						->orderBy($sortBy, $order)
						->paginate(Config::get("Reading.record_game_per_page"));
		}



		$complete_string		=	Input::query();
		unset($complete_string["sortBy"]);
		unset($complete_string["order"]);
		$query_string			=	http_build_query($complete_string);
		$result->appends(Input::all())->render();
		$drop_down				=	new DropDown();
		$gradeName				=	$drop_down->get_master_list("gradename");
		$teamType				=	$drop_down->get_master_list("teamtype");
		$teamCategory			=	$drop_down->get_master_list("teamcategory");
		$club					=	new Club();
		$cludDetails			=	$club->get_club_list();
		return  View::make('admin.team.index', compact('result' ,'searchVariable','sortBy','order','userType','query_string','gradeName','teamType','teamCategory','cludDetails'));
	 }

	/**
	* Function for add Forum
	*
	* @param null
	*
	* @return view page. 
	*/
	public function addTeam(){
		$authUser = Auth::guard('admin')->user();
		$drop_down				=	new DropDown();
		$grades				=	new Grade();
		if($authUser->user_role_id !=1){
			$gradeName				=	$grades->get_grade(Auth::guard('admin')->user()->id);
		}
		if(Auth::guard('admin')->user()->user_role_id == 1){
			$teamType = Config::get('senior_team_type');
		}else{
			if($authUser->game_mode ==1){
				$teamType = Config::get('senior_team_type');
			}elseif($authUser->game_mode ==2){
				$teamType = Config::get('junior_team_type');
			}elseif ($authUser->game_mode ==3) {
				$teamType = Config::get('league_team_type');
			}
		}
		//$teamType				=	$drop_down->get_master_list("teamtype");
		$teamCategory			=	$drop_down->get_master_list("teamcategory");
		$club					=	new Club();
		$cludDetails			=	$club->get_club_list();
		return  View::make('admin.team.add',compact('gradeName','teamType','teamCategory','cludDetails'));
	}//end addPlayer()

	public function adminAddTeam(){
		$authUser = Auth::guard('admin')->user();
		$drop_down				=	new DropDown();
		$grades				=	new Grade();
		if($authUser->user_role_id !=1){
			$gradeName				=	$grades->get_grade(Auth::guard('admin')->user()->id);
		}
		if(Auth::guard('admin')->user()->user_role_id == 1){
			$teamType = Config::get('senior_team_type');
		}else{
			if($authUser->game_mode ==1){
				$teamType = Config::get('senior_team_type');
			}elseif($authUser->game_mode ==2){
				$teamType = Config::get('junior_team_type');
			}elseif ($authUser->game_mode ==3) {
				$teamType = Config::get('league_team_type');
			}
		}
		$teamCategory			=	$drop_down->get_master_list("teamcategory");
		$club					=	new Club();
		$cludDetails			=	$club->get_club_list();
		return  View::make('admin.team.admin_add',compact('gradeName','teamType','teamCategory','cludDetails'));
	}//end addPlayer()
	
	
	
	
	public function saveTeam(){
	Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'name'			=> 'required',
					'grade_name'	=> 'required',
					'team_category' => 'required',
					'type' 			=> 'required',
					'club'			=> 'required',
				)
			);

			if ($validator->fails()){
				$errors 	=	$validator->messages();
				$response	=	array(
					'success' 	=> false,
					'errors' 	=> $errors
				);
				return Response::json($response);die;
				//return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
				$obj 							=  new Team;
				$obj->name 						=  Input::get('name');
				$obj->grade_name 				=  !empty(Input::get('grade_name')) ? Input::get('grade_name'):0;
				$obj->team_category 			=  !empty(Input::get('team_category')) ? Input::get('team_category'):0;
				$obj->type 						=  !empty(Input::get('type')) ? Input::get('type'):0;
				$obj->club 						=  !empty(Input::get('club')) ? Input::get('club'):0;
				$obj->is_active					=  1; 
				$obj->save();
				$response	=	array(
			    	'success' 	=>	'1',
			    	'message' 	=>	trans("Team has been added successfully.")
			    ); 
			    return Response::json($response);
			    Session::flash('success',trans("Team has been added successfully"));
				/*return Redirect::to('admin/team');*/
			  }
		}
	}//end saveTeam
	public function adminSaveTeam(){
		Input::replace($this->arrayStripTags(Input::all()));
		$thisData			=	Input::all();
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'name'			=> 'required',
					'grade_name'	=> 'required',
					'team_category' => 'required',
					'type' 			=> 'required',
					'club'			=> 'required',
				)
			);

			if ($validator->fails()){
				$errors 	=	$validator->messages();
				$response	=	array(
					'success' 	=> false,
					'errors' 	=> $errors
				);
				return Response::json($response);die;
				//return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
				$obj 							=  new Team;
				$obj->name 						=  Input::get('name');
				$obj->grade_name 				=  !empty(Input::get('grade_name')) ? Input::get('grade_name'):0;
				$obj->team_category 			=  !empty(Input::get('team_category')) ? Input::get('team_category'):0;
				$obj->type 						=  !empty(Input::get('type')) ? Input::get('type'):0;
				$obj->club 						=  !empty(Input::get('club')) ? Input::get('club'):0;
				$obj->is_active					=  1; 
				$obj->save();
				$response	=	array(
			    	'success' 	=>	'1',
			    	'message' 	=>	trans("Team has been added successfully.")
			    ); 
			    return Response::json($response);
			    Session::flash('success',trans("Team has been added successfully"));
				/*return Redirect::to('admin/team');*/
			  }
		}
	}//end saveForum

	/**
	* Function for edit Forums
	*
	* @param null
	*
	* @return view page. 
	*/
	public function editTeam($userId = 0){ 
		$authUser = Auth::guard('admin')->user(); 
		$userDetails			=	Team::find($userId); 
		if(empty($userDetails)) {
			return Redirect::to('admin/Team');
		}
		if($userId){  
			$userDetails		=	Team::find($userId);
			$drop_down				=	new DropDown();
			$grades =	new Grade();
			$gradeName = Grade::where('club',$userDetails->club)->pluck('grade','id')->all();
			if($authUser->game_mode ==1){
				$teamType = Config::get('senior_team_type');
			}elseif($authUser->game_mode ==2){
				$teamType = Config::get('junior_team_type');
			}elseif ($authUser->game_mode ==3) {
				$teamType = Config::get('league_team_type');
			}
			$teamCategory			=	$drop_down->get_master_list("teamcategory");
			$club					=	new Club();
			$cludDetails			=	$club->get_club_list();
			return View::make('admin.team.edit', compact('userDetails','teamType','userId','teamType','teamCategory','cludDetails','gradeName'));
		}
	} // 

	/**
* Function for update Forum
*
* @param null
*
* @return view page. 
*/
	public function updateTeam(){
	
		Input::replace($this->arrayStripTags(Input::all()));
	//$user_id	=	Auth::user()->id;
		$thisData			=	Input::all();
		$user_id = Input::get('team_id');
		if(!empty($thisData)){
			$validator 					=	Validator::make(
				Input::all(),
				array(
					'name'			=> 'required',
					'grade_name'	=> 'required',
					'team_category' => 'required',
					'type' 			=> 'required',
					'club'			=> 'required',
				),
				array(
					//'image.dimensions' => "Please upload min width 221 px and min height 266 px image",
				)
			);
			if ($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else{ 
					$obj 							=  Team::find($user_id);
					$obj->name 						=  Input::get('name');
					$obj->grade_name 				=  !empty(Input::get('grade_name')) ? Input::get('grade_name'):0;
					$obj->team_category 			=  !empty(Input::get('team_category')) ? Input::get('team_category'):0;
					$obj->type 						=  !empty(Input::get('type')) ? Input::get('type'):0;
					$obj->club 						=  !empty(Input::get('club')) ? Input::get('club'):0;
					$obj->save();
			    Session::flash('success',trans("Team has been upated successfully"));
				return Redirect::to('admin/team');
			}
		}
	}//end update Forum-manager
	
	public function viewTeam($userId = 0){
		
		$userDetails		=	Team::find($userId); 				
		if(empty($userDetails)) {
			return Redirect::to('admin/team/');
		}
		
		#### Getting  name ###
		$clubName		=	DB::table('users')
							->where('id','=',$userDetails->club)
							->value('club_name');
						
		$gradeName		=	DB::table('dropdown_managers')
							->where('id','=',$userDetails->grade_name)
							->value('name');
	    $categoryName	=	DB::table('dropdown_managers')
							->where('id','=',$userDetails->team_category)
							->value('name');
		$teamType		=	DB::table('dropdown_managers')
							->where('id','=',$userDetails->type)
							->value('name');
							
							
		return View::make('admin.player.view', compact('userDetails','clubName','gradeName','categoryName','teamType'));
	} // 

	
	
	

	/**
	 * Function for change is_active of Forum
	 *
	 * @param $Id as id of Forum
	 * @param $Forum is_active as is_active of Forum
	 *
	 * @return redirect page. 
	 */	
	public function updateStatus($Id = 0, $Status = 0){
		Team::where('id', '=', $Id)->update(array('is_active' => $Status));
		Session::flash('flash_notice', trans("Status updated successfully.")); 
		return Redirect::to('admin/team');
	} // end updateStatus()
	
	
	/**
	/**
	* Function for mark a Forum as deleted 
	*
	* @param $Id as id of Forum
	*
	* @return redirect page. 
	*/
	public function deleteTeam($Id=0){
		$userDetails	=	Team::find($Id); 
		if(empty($userDetails)) {
			return Redirect::to('admin/team');
		}
		if($Id){
			//$userModel		=	Forum::where('id',$Id)->delete();
			$userModel		=	Team::where('id',$Id)->delete();
		}
		return Redirect::to('admin/team');
	}// end deleteForum
	public function getGrade(){
		$clubId = Input::get('id');
		$getGrades = Grade::where('club',$clubId)->pluck('grade','id')->all();
		return View::make('admin.team.get_grades', compact("getGrades"));
	}
	public function getTeamType(){
		$clubId = Input::get('id');
		$clubData = User::where('id',$clubId)->first();
		if($clubData->game_mode ==1){
			$teamType = Config::get('senior_team_type');
		}elseif($clubData->game_mode ==2){
			$teamType = Config::get('junior_team_type');
		}elseif ($clubData->game_mode ==3) {
			$teamType = Config::get('league_team_type');
		}
		return View::make('admin.team.get_team_type', compact("teamType"));
	}

}// end ClubController class
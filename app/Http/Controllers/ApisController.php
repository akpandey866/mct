<?php
/**
 * ApisController Controller
 */
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use App\Model\User;
use App\Model\Celebrity;
use App\Model\CelebrityDescription;
use App\Model\EmailAction;
use App\Model\EmailTemplate;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App,Auth,Blade,Config,Cache,Cookie,DB,File,Ajax,Hash,Mail,mongoDate,Redirect,Response,Session,URL,View,Validator;

class ApisController extends BaseController {
	
	public function uploadProfileImage(){ 
		$authToken	=		Request::header('AuthToken');	
		if(empty($authToken)){
			$response	=	array(
				'status' 	=>	1,
				'errors' 	=>	 trans("Auth token does not exists")
			); 
			return  Response::json($response); 
			die;
		}
		if(empty($_FILES)){
			$response	=	array(
				'status' 	=>	1,
				'errors' 	=>	 trans("File not uploded.")
			); 
			return  Response::json($response); 
			die;
		}
		
		$user_id				=	DB::table('users')->where('validate_string',$authToken)->pluck('id');
		if(input::hasFile('file')){
			$obj 					=  User::findOrFail($user_id);
			@unlink(USER_PROFILE_IMAGE_ROOT_PATH.$obj->image);
			$extension 			=	Input::file('file')->getClientOriginalExtension();
			$newFolder     		= 	strtoupper(date('M'). date('Y')).DS;
			$folderPath			=	USER_PROFILE_IMAGE_ROOT_PATH.$newFolder; 
			if(!File::exists($folderPath)){
				File::makeDirectory($folderPath, $mode = 0777,true);
			}
			$userImageName 		= time().'-user.'.$extension;
			$image = $newFolder.$userImageName;
			if(Input::file('file')->move($folderPath, $userImageName)){
				$obj->image		=	$image;
			}
			$obj->save();
			$imgName	=	$obj->image;
			$response			=	array(
				'status' 	=>	1,
				'data'		=>  $obj->image,
				'message' 	=>	 trans("Image uploaded successfully.")
			); 
			return  Response::json($response); 
			die;
		}else{
			$response			=	array(
				'status' 	=>	1,
				'message' 	=>	 trans("Image not uploaded.")
			); 
			return  Response::json($response); 
			die;
		}		
	}
	
	public function saveQuestionsApi(){
		$formData			=	Input::all();
		$authToken	=		Request::header('AuthToken');	
		if(empty($authToken)){
			$response	=	array(
				'status' 	=>	1,
				'errors' 	=>	 trans("Auth token does not exists")
			); 
			return  Response::json($response); 
			die;
		}
		$user_id				=	DB::table('users')->where('validate_string',$authToken)->pluck('id');	
		if(!empty($formData)){
				$validator = Validator::make(
					array(
						'category' 			=>	Input::get('category'),
						'level' 			=>	Input::get('level'),
						),
					array(
						'category' 				=> 	'required',
						'level' 				=> 	'required',
					)
				);
			if ($validator->fails()){
				$response	=	array(
					'status' 	=> 0,
					'errors' 	=> $validator->errors()
				);
				return Response::json($response); 
				die;
			}else{
				$obj 						= 	new Question();
				$obj->user_id		 		=  	$user_id;
				$obj->category_id			=  	Input::get('category');
				$obj->level_id				=  	Input::get('level');				
				$obj->save();
				
				$questionId					=	$obj->id;
				$quetionIdsArray			=	array();
				if(!empty($formData['data'])){
					$i = 0 ;
					foreach($formData['data'] as $key=>$data){
						if(!empty($data['title'])){
							$dataObj 				= 	new StudentQuestions();
							$dataObj->question_id	=  	$questionId;
							$dataObj->title			=  	$data['title'];  	
							$dataObj->save();
							$quetionIdsArray[]		=	$dataObj->id;
							$i++;
						}
					}
				}
				$response	=	array(
					'status' 	=>	1,
					'questionId'		=>	$quetionIdsArray,
					'question_table_id' =>	$questionId,
					'message' 	=>	trans("Question successfully posted.")
				); 
				return  Response::json($response); 
				die;
			}
		}
	}
	
	public function uploadQuestionFiles(){ 
		$authToken	=	Request::header('AuthToken');	
		$questionId	=	Input::get('questionId');
		if(empty($authToken)){
			$response	=	array(
				'status' 	=>	0,
				'errors' 	=>	 trans("Auth token does not exists")
			); 
			return  Response::json($response); 
			die;
		}if(empty($questionId)){
			$response	=	array(
				'status' 	=>	0,
				'errors' 	=>	 trans("Question can not be empty.")
			); 
			return  Response::json($response); 
			die;
		}
		$questionResult=	DB::table('student_questions')->where('id',$questionId)->first();
		if(empty($questionResult)){
			$response	=	array(
				'status' 	=>	0,
				'errors' 	=>	 trans("No data found of coresponding question.")
			); 
			return  Response::json($response); 
			die;
		}
		$user_id				=	DB::table('users')->where('validate_string',$authToken)->pluck('id');
		if(input::hasFile('file')){ 
			$obj 					=  StudentQuestions::find($questionId);
			@unlink(QUESTION_IMAGE_ROOT_PATH.$obj->image);
			$extension 			=	Input::file('file')->getClientOriginalExtension();
			$newFolder     		= 	strtoupper(date('M'). date('Y')).DS;
			$folderPath			=	QUESTION_IMAGE_ROOT_PATH.$newFolder; 
			if(!File::exists($folderPath)){
				File::makeDirectory($folderPath, $mode = 0777,true);
			}
			$userImageName 		= time().'-questions.'.$extension;
			$image 				= $newFolder.$userImageName;
			if(Input::file('file')->move($folderPath, $userImageName)){
				$obj->file_name		=	$image;
				$obj->orignal_name   =  Input::file('file')->getClientOriginalName();
			}
			$obj->save();
			$response			=	array(
				'status' 	=>	1,
				'message' 	=>	 trans("File uploded successfully.")
			); 
			return  Response::json($response); 
			die;
		}else{
			$response			=	array(
				'status' 	=>	1,
				'message' 	=>	 trans("File not uploaded.")
			); 
			return  Response::json($response); 
			die;
		}				
	}
	
	public function showCmsMobile($slug){ 
		$result		=	DB::table('cms_pages')->where(['slug'=>$slug])->first();			
		$response	=	array(
						'status' 	=> 1,
						'data' 	=> $result
					);
		return Response::json($response); 
		die;	
	}//end showCms()
	
	/** 
	 * Function use for send a forgot password email to user
	 *
	 * @param null
	 * 
	 * @return void
	 */
	public function forgotPasswordApi(){ 
		$validator = Validator::make(
			Input::all(),
			array(
				'email' 			=> 'required|email',
			),
			array(
				'email.required' 			=> trans("Email field is required."),
				'email.email' 				=> trans("Please enter valid email address."),
			)
		);
		if ($validator->fails()){
			$response	=	array(
				'status' 	=> 0,
				'message' 	=> $validator->errors()
			);
			return Response::json($response); 
			die;
		}else{
			$email		=	Input::get('email');   
			$userDetail	=	User::where('email',$email)
									->where('is_active','=',ACTIVE)
									->where('is_verified','=',ACTIVE)
									->where('is_deleted','=',INACTIVE)
									->first();
				
			if(!empty($userDetail)){
				$forgot_password_validate_string	= 	md5($userDetail->email);
				User::where('email',$email)->update(array('forgot_password_validate_string'=>$forgot_password_validate_string));
				
				$settingsEmail 		=  Config::get('Site.email');
				$email 				=  $userDetail->email;
				$username			=  $userDetail->full_name;
				$full_name			=  $userDetail->full_name;  
				$route_url      	=  URL::to('reset-password/'.$forgot_password_validate_string);
				$verify_link   		=   $route_url;
				
				$emailActions		=	EmailAction::where('action','=','forgot_password')->get()->toArray();
				$emailTemplates		=	EmailTemplate::where('action','=','forgot_password')->get(array('name','subject','action','body'))->toArray();
				$cons = explode(',',$emailActions[0]['options']);
				$constants = array();
				
				foreach($cons as $key=>$val){
					$constants[] = '{'.$val.'}';
				}
				$subject 			=  $emailTemplates[0]['subject'];
				$rep_Array 			= array($username,$verify_link,$route_url); 
				$messageBody		=  str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
				$this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);
				$err				=	array();
				$err['status']		=	1;
				$err['message']		=	trans('We have sent reset password link on registered email.');
				return Response::json($err); 
			}else{
				$response	=	array(
					'status' 	=> 0,
					'message' 	=> trans("Your email is not registered with us.")
				);
				return Response::json($response); 
				die;
			}
		}
	} //end ForgotPassword()	

	public function saveProfileMobile(){  
		$formData			=	Input::all();
		$verifyToken 		=   Request::header('AuthToken');
		$user_id			=	DB::table('users')->where('validate_string',$verifyToken)->pluck('id');
		$validator = Validator::make(
			Input::all(),
			array(
				'first_name'		=> 'required',
				'last_name'			=> 'required',
				'gender'			=> 'required',
				'dob'				=> 'required',
			),
			array(
				'dob.required'				=>	trans("Date of birth field is required."),
			)
		);
		if ($validator->fails()){
			$errors 	=	$validator->messages();
		}
		if ($validator->fails()){
			$response	=	array(
				'status' 	=> 0,
				'errors' 	=> $validator->errors()
			);
			return Response::json($response); 
			die; 
		}else{
			$obj 					=  User::find($user_id);
			$obj->first_name 		=  ucfirst(Input::get('first_name'));
			$obj->last_name 		=  ucfirst(Input::get('last_name'));
			$obj->full_name 		=  $obj->first_name." ".$obj->last_name;
			$obj->phone_number		=  !empty(Input::get('phone_number')) ? Input::get('phone_number') : '' ;
			$obj->mobile_number		=  !empty(Input::get('mobile_number')) ? Input::get('mobile_number') : '' ;
			$obj->dob				=  Input::get('dob');

			if (Input::get('gender') == 0) {   
				$obj->gender		=  MALE;
			}else{
				$obj->gender		=  FEMALE;
			}

			if(input::hasFile('document_image')){
				@unlink(DOCUMENT_IMAGE_ROOT_PATH.$obj->document_image);
				$extension 			=	Input::file('document_image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).DS;
				$folderPath			=	DOCUMENT_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-document.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('document_image')->move($folderPath, $userImageName)){
					$obj->document_image		=	$image;
				}
			}
			if(input::hasFile('image')){
				@unlink(USER_PROFILE_IMAGE_ROOT_PATH.$obj->image);
				$extension 			=	Input::file('image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).DS;
				$folderPath			=	USER_PROFILE_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-user.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('image')->move($folderPath, $userImageName)){
					$obj->image		=	$image;
				}
			}
			$obj->save();
			$response	=	array(
				'status' 	=>	1,
				'message' 	=>	 trans("Profile updated successfully.")
			); 
			return  Response::json($response); 
			die;
		}
	}

	public function saveCelebrityProfile(Request $request){  
		$formData			=	Input::all();
		$user_id = $request->input('id');
		$validator = Validator::make(
			Input::all(),
			array(
				'first_name'		=> 'required',
				'last_name'			=> 'required',
				'gender'			=> 'required',
				'dob'				=> 'required',
			),
			array(
				'dob.required'				=>	trans("Date of birth field is required."),
			)
		);
		if ($validator->fails()){
			$errors 	=	$validator->messages();
		}
		if ($validator->fails()){
			$response	=	array(
				'status' 	=> 0,
				'errors' 	=> $validator->errors()
			);
			return Response::json($response); 
			die; 
		}else{
			$obj 					=  Celebrity::find($user_id);
			$obj->first_name 		=  ucfirst(Input::get('first_name'));
			$obj->last_name 		=  ucfirst(Input::get('last_name'));
			$obj->full_name 		=  $obj->first_name." ".$obj->last_name;
			$obj->mobile			=  !empty(Input::get('mobile_number')) ? Input::get('mobile_number') : '' ;
			$obj->dob				=  date("y-m-d",strtotime(Input::get('dob')));
			$obj->celebrity_type	=  !empty(Input::get('celebrity_type')) ? Input::get('celebrity_type') : 1;
			$obj->social_url		=  !empty(Input::get('social_url')) ? Input::get('social_url') : '';
			$obj->video				=  !empty(Input::get('video')) ? Input::get('video') : '';

			if (Input::get('gender') == 0) {   
				$obj->gender		=  MALE;
			}else{
				$obj->gender		=  FEMALE;
			}

			if(Input::hasFile('image')){
				@unlink(USER_PROFILE_IMAGE_ROOT_PATH.$obj->image);
				$extension 			=	Input::file('image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	USER_PROFILE_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-user.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('image')->move($folderPath, $userImageName)){
					$obj->image		=	$image;
				}
			}
			$obj->save();
			$response	=	array(
				'status'	=>	1,
				'msg'		=>	trans("Profile updated successfully.")
			); 
			return  Response::json($response);
		}
	}
	
	public function getLevelListMobile(){
		$drop_down		=	new DropDown();
		$levelList		=	$drop_down->get_master_list("level");
		$response		=	array(
								'status' 	=> 1,
								'data' 		=> $levelList
							);
		return Response::json($response); 
	}
	
	public function getCategoryListMobile(){
		$drop_down		=	new DropDown();
		$categoryList	=	$drop_down->get_master_list("categories");
		$response		=	array(
								'status' 	=> 1,
								'data' 		=> $categoryList
							);
		return Response::json($response); 
	}
	
	public function mobileLogin(){
		$device_token = Input::get('device_token');
		if ((!empty(Input::get('social_type')) && !empty(Input::get('social_id')))) {

			if (Input::get('user_type') == Config::get('user_type')['simple']) {
				$userdetails = User::where('social_id',Input::get('social_id'))->first();
				$user_type 	= 1;
			}

			if (Input::get('user_type') == Config::get('user_type')['Celebrity']) {
				$userdetails = Celebrity::where('social_id',Input::get('social_id'))->first();
				$user_type 	= 2;
			}
			
			if (!empty($userdetails)) {
				$response	=	array(
						'status' 		=> 1,
						'user_type' 	=> $user_type,
						'image_path'	=> USER_PROFILE_IMAGE_URL,
						'data'			=> $userdetails
					);
			}else{
				$response	=	array(
					'status'	=>	0,
					'msg'	=>	trans('messages.login.msg.user_not_exist')
				);
			}
			return Response::json($response);
		}else{
	
			Input::replace($this->arrayStripTags(Input::all()));
			$formData	=	Input::all();
		
			$validator = Validator::make(
				Input::all(),
				array(
					'email' 			=> 'required',
					'password'			=> 'required',
				),
				array(
					'email.required' 	=> trans('messages.msg.email_field_is_required'),
					'password.required'	=> trans('messages.login.msg.password_field_is_required'),
				)
			);
	
			if ($validator->fails()){
				$response	=	array(
					'status' => 0,
					'msg' 	=> $validator->errors()
				);
				return Response::json($response); 
				die;
			}else {
				if (Input::get('user_type') == Config::get('user_type')['simple']) {
					$userdata = array(
						'email' 		=>	 Input::get('email'),
						'password' 		=> 	 Input::get('password'),
						'is_active' 	=>   1,
						'is_verified' 	=>   1,
						'is_deleted' 	=>   0,
					);
					if (Auth::attempt($userdata)){  
						
						$updateData = array(
							'device_token' => $device_token
							);
						
						DB::table('users')->where('email', Input::get('email'))->orWhere('mobile',Input::get('email'))->update($updateData);
						
						
						$response	=	array(
							'status' 		=> 1,
							'user_type' 	=> 1,
							'access_token'  =>  Auth::user()->validate_string,
							'image_path'	=> USER_PROFILE_IMAGE_URL,
							'data'			=> Auth::user()
						);
						return Response::json($response);
					}else{
						$userDetails	=	DB::table('users')
											->where('email',Input::get('email'))
											->where('user_role_id', '!=' , SUPER_ADMIN_ROLE_ID )
											->where('is_deleted', '=' , INACTIVE )
											->first();
						if(!empty($userDetails)) {  
							if($userDetails->is_active == INACTIVE) {
								$err				=	array();
								$err['status']		=	0;
								$err['msg']		=	trans('messages.login.msg.your_account_is_inactive_please_contact_to_admin');
								return Response::json($err); 
							}else if($userDetails->is_verified == INACTIVE) {
								$err				=	array();
								$err['status']		=	0;
								$err['msg']		=	trans('Your account is not verified. please verified your account');
								return Response::json($err); 
							}else {
								$err				=	array();
								$err['status']		=	0;
								$err['msg']		=	trans('messages.login.msg.username_or_password_is_incorrect');
								return Response::json($err); 
							}
						}else {  
							$err				=	array();
							$err['status']		=	0;
							$err['msg']		=	trans('messages.login.msg.username_or_password_is_incorrect');
							return Response::json($err); 
						}
					}
				}
				if (Input::get('user_type') == Config::get('user_type')['Celebrity']) {
					$email = Input::get('email');
					$password = Hash::make(Input::get('password'));

					$celebritydetails = Celebrity::where('email',$email)->orWhere('mobile',$email)->where('password',$password)->first();

					if (!empty($celebritydetails)) {
						$updateData = array(
							'device_token' => $device_token
							);
						
						DB::table('users')->where('email', Input::get('email'))->orWhere('mobile',Input::get('email'))->update($updateData);
						
						$response	=	array(
								'status' 		=> 1,
								'user_type' 	=> 2,
								'image_path'	=> USER_PROFILE_IMAGE_URL,
								'data'			=> $celebritydetails
							);
					}else{
						$response	=	array(
							'status'		=>	0,
							'msg'		=>	trans('messages.login.msg.username_or_password_is_incorrect')
						);
					}
					return Response::json($response); 
				}
			}
		}
	}
	
	public function mobileSignup(Request $request){

		$formData	=	Input::all();

		$validator = Validator::make(
			Input::all(),
			array(
				'email'  => 'email|unique:users',			
				'mobile' => 'required|numeric|unique:users',				
			),
			array(
				"email.unique"	=>	trans("messages.home.email_allready"),
				"mobile.unique"	=>	trans("messages.home.mobile_already")
			)
		);
		if ($validator->fails()){
			$errors 	=	$validator->messages();
			 $response	=	array(
				'status' 	=> 0,
				//'msg' 		=> trans("messages.home.email_allready")
				'msg' 		=> $errors
			);
		}else {
			$obj 				=  new User;
			$validateString		=  md5(time() . Input::get('email'));
			$obj->validate_string =  $validateString;					
			$obj->first_name 	=  ucfirst(Input::get('first_name'));
			$obj->last_name 	=  ucfirst(Input::get('last_name'));
			$obj->full_name 	=  $obj->first_name." ".$obj->last_name;
			$obj->email 		=  Input::get('email');
			$obj->dob 			=  date("y-m-d",strtotime(Input::get('dob')));
			$obj->mobile     	=  !empty(Input::get('mobile')) ? Input::get('mobile'):'';
			$obj->country_code  =  !empty(Input::get('country_code')) ? Input::get('country_code'):'';
			$obj->slug	 		=  $this->getSlug(Input::get('first_name')." ".Input::get('last_name'),'full_name','User');
			$obj->password	 	=  Hash::make(Input::get('password'));
			// $obj->user_role_id		=  Input::get('user_role_id');
			$obj->device_token	=  !empty(Input::get('device_token')) ? Input::get('device_token'):'';
			$obj->device_type	=  !empty(Input::get('device_type')) ? Input::get('device_type'):'';
			$obj->social_type	=  !empty(Input::get('social_type')) ? Input::get('social_type'):'';
			$obj->social_id		=  !empty(Input::get('social_id')) ? Input::get('social_id'):'';

			// Saved User Otp
			$otp = $this->generatePIN();
			$obj->otp	=  $otp;

			// social login profile image 
			if (!empty(Input::get('social_id')) && !empty(Input::get('image'))) {
				$profilePic 		= 	Input::get('image');
				$userprofileImage   = 	file_get_contents($profilePic);
				$socialId = Input::get('social_id');

				$userprofileImageName  = 	$socialId ."_provider.jpg";
				$newFoldername     	= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath		=	USER_PROFILE_IMAGE_ROOT_PATH.$newFoldername;
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}

				file_put_contents($folderPath.$userprofileImageName,$userprofileImage);
				$obj->image	=	$newFoldername.$userprofileImageName;
			}
			
			if (Input::get('gender') == 0) {   
				$obj->gender		=  MALE;
			}else{
				$obj->gender		=  FEMALE;
			}
			
			$obj->is_verified		=  0;
			$obj->is_active			=  1;
			$userRoleId				=  Input::get('user_role_id');

			// normal login profile image
			if(input::hasFile('image')){
				$extension 			=	Input::file('image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	USER_PROFILE_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-user.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('image')->move($folderPath, $userImageName)){
					$obj->image	=	$image;
				}
			}
			$obj->save();
			/* Mail Send */
			$settingsEmail 	= Config::get('Site.email');
			$full_name		= $obj->full_name; 
			$email			= $obj->email;
			$password		= Input::get('password');
			$route_url      = URL::to('account-verification/'.$obj->validate_string);
			$select_url     = "<a href='".$route_url."'>".trans("Click Here")."</a>";
			
			$emailActions	= EmailAction::where('action','=','account_verification')->get()->toArray();
			$emailTemplates	= EmailTemplate::where('action','=','account_verification')->get(array('name','subject','action','body'))->toArray();
		
			$cons 			= explode(',',$emailActions[0]['options']);
			$constants 		= array();
			
			foreach($cons as $key => $val){
				$constants[] = '{'.$val.'}';
			}
			
			$subject 		= $emailTemplates[0]['subject'];
			$rep_Array 		= array($full_name,$select_url,$route_url); 
			$messageBody	= str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
			$mail			= $this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);

			if (!empty(Input::get('mobile'))) {
				$mobile = $obj->country_code.''.$obj->mobile;
				$data = "Please Verify Your Mobile Number. Your Otp is ".$otp;
				$this->sendMessages($mobile,$data);
			}
			
			$response	=	array(
				'status'	=>	1,
				'msg'		=>	trans("Account has been registered successfully.Please Verify Your Account.")
			); 	
		}
		return Response::json($response); 
	}

	public function sendOTP(Request $request)
	{
		$validator = Validator::make(
			Input::all(),
			array(
				'mobile' => 'required|numeric'			
			),
			array(
				"mobile.required"	=>	trans('messages.error.mobile_field_is_required'),
				"mobile.numeric"	=>	trans('messages.msg.amount_field_is_numeric'),
			)
		);
		if ($validator->fails()){

			$response	=	array(
				'status' 	=> 0,
				'msg' 		=> trans("messages.global.somethingwrong")
			);
		}else {

			$otp = $this->generatePIN();
			$country_code = Input::get('country_code');
			$mobile = Input::get('mobile');
			$mobile_number = $country_code.''.$mobile;

			$data = "Please Verify Your Mobile Number. Your Otp is ".$otp;
			$this->sendMessages($mobile_number,$data);

			// If user type = to user than update verified user otp
			if (Input::get('user_type') == Config::get('user_type.simple')) {
				User::where('mobile',$mobile)->update(['otp' => $otp]);
			}
			// If user type = to celebrity than update verified celebrity
			if (Input::get('user_type') == Config::get('user_type.Celebrity')) {
				Celebrity::where('mobile',$mobile)->update(['otp' => $otp]);
			}

			$response	=	array(
				'status' => 1,
				'msg' 	 => trans("messages.otp.send_success")
			);

			return  Response::json($response); 
		}	
	}

	/** 
	 * Function used for verifyOTP
	 *
	 * @param null
	 * 
	 * @return void
	 */

	public function verifyOTP(Request $request)
	{
		$validator = Validator::make(
			Input::all(),
			array(
				'mobile' => 'required|numeric',			
				'otp' => 'required|numeric',			
			),
			array(
				"mobile.required"	=>	trans('messages.error.mobile_field_is_required'),
				"mobile.numeric"	=>	trans('messages.msg.amount_field_is_numeric'),
				"otp.required"	=>	trans('messages.otp.required'),
				"otp.numeric"	=>	trans('messages.msg.amount_field_is_numeric'),
			)
		);
		if ($validator->fails()){
			$response	=	array(
				'status' 	=> 0,
				'msg' 		=> trans("messages.global.somethingwrong")
			);
		}else {
			if (Input::get('user_type') == Config::get('user_type.simple')) {
				$userdetails = User::where('mobile',Input::get('mobile'))->first();
			}

			if (Input::get('user_type') == Config::get('user_type.Celebrity')) {
				$userdetails = Celebrity::where('mobile',Input::get('mobile'))->first();
			}
			
			if (!empty($userdetails)) {

				if ($userdetails['otp'] == Input::get('otp')) {

					// If user type = to user than update user otp
					if (Input::get('user_type') == Config::get('user_type.simple')) {
						User::where('id',$userdetails['id'])->update(['is_verified' => 1]);
					}
					// If user type = to celebrity than update celebrity otp
					if (Input::get('user_type') == Config::get('user_type.Celebrity')) {
						Celebrity::where('id',$userdetails['id'])->update(['is_verified' => 1]);
					}

					$response	=	array(
						'status' => 1,
						'data'	 => $userdetails,
						'msg' 	 => trans("messages.home.your_account_verified_successfully")
					);
				}else{
					$response	=	array(
						'status' => 0,
						'msg' 	 => trans("messages.otp.not_match")
					);
				}
			}else{
				$response	=	array(
					'status' 	=> 0,
					'msg' 		=> trans("messages.login.msg.user_not_exist")
				);
			}
		}
		return  Response::json($response); 
	}

	/** 
	 * Function use for register Celebrity
	 *
	 * @param null
	 * 
	 * @return void
	 */

	public function celebritySignup(Request $request){

		$lang		= App::getLocale();
		$langid 	= DB::table('languages')->where('lang_code',$lang)->value('id');
		$validator 	= Validator::make(
			Input::all(),
			array(
				'email'		=> 'email|unique:celebrities',		
				'mobile'	=> 'required|numeric|unique:celebrities',		
			),
			array(
				"email.unique"	=>	trans("messages.home.email_allready"),
			)
		);
		if ($validator->fails()){
			$errors 	=	$validator->messages();
			$response	=	array(
				'status' 	=> 0,
				'msg' 		=> trans("messages.home.email_allready"),
			);
		}else {
			$obj 					=  new Celebrity;
			$validateString			=  md5(time() . Input::get('email'));			
			$obj->first_name 		=  Input::get('first_name');
			$obj->last_name 		=  Input::get('last_name');
			$obj->full_name 		=  $obj->first_name." ".$obj->last_name;
			$obj->email 			=  Input::get('email');
			$obj->celebrity_type 	=  !empty(Input::get('celebrity_type')) ? Input::get('celebrity_type') :0; 
			$obj->dob 				=  date("y-m-d",strtotime(Input::get('dob')));
			$obj->mobile     		=  !empty(Input::get('mobile')) ? Input::get('mobile'):'';
			$obj->country_code     	=  !empty(Input::get('country_code')) ? Input::get('country_code'):'';
			$obj->slug	 			=  $this->getSlug(Input::get('first_name')." ".Input::get('last_name'),'full_name','User');
			$obj->password	 		=  Hash::make(Input::get('password'));

			$obj->device_token		=  !empty(Input::get('device_token')) ? Input::get('device_token'):'';
			$obj->device_type		=  !empty(Input::get('device_type')) ? Input::get('device_type'):'';
			$obj->social_type		=  !empty(Input::get('social_type')) ? Input::get('social_type'):'';
			$obj->social_id			=  !empty(Input::get('social_id')) ? Input::get('social_id'):'';

			// social login profile image 
			if (!empty(Input::get('social_id')) && !empty(Input::get('image'))) {
				$profilePic 		= 	Input::get('image');
				$userprofileImage   = 	file_get_contents($profilePic);
				$socialId = Input::get('social_id');

				$userprofileImageName  = 	$socialId ."_provider.jpg";
				$newFoldername     	= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath		=	USER_PROFILE_IMAGE_ROOT_PATH.$newFoldername;
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}

				file_put_contents($folderPath.$userprofileImageName,$userprofileImage);
				$obj->image	=	$newFoldername.$userprofileImageName;
			}
		
			if (Input::get('gender') == 0) {   
				$obj->gender		=  MALE;
			}else{
				$obj->gender		=  FEMALE;
			}
			$obj->is_active			=  1;

			if(input::hasFile('image')){
				$extension 			=	Input::file('image')->getClientOriginalExtension();
				$newFolder     		= 	strtoupper(date('M'). date('Y')).'/';
				$folderPath			=	USER_PROFILE_IMAGE_ROOT_PATH.$newFolder; 
				if(!File::exists($folderPath)){
					File::makeDirectory($folderPath, $mode = 0777,true);
				}
				$userImageName = time().'-user.'.$extension;
				$image = $newFolder.$userImageName;
				if(Input::file('image')->move($folderPath, $userImageName)){
					$obj->image	=	$image;
				}
			}

			$otp = $this->generatePIN();

			$obj->otp	= $otp;

			$obj->save();

			$celebrity_id = $obj->id;

			$celebritydescription = new CelebrityDescription();

			$celebritydescription->language_id	= $langid;
			$celebritydescription->celebrity_id	= $celebrity_id;
			$celebritydescription->first_name	= Input::get('first_name');
			$celebritydescription->last_name	= Input::get('last_name');
			$celebritydescription->full_name	= Input::get('first_name').' '.Input::get('last_name');
			$celebritydescription->save();


			/* Mail Send */
			// $settingsEmail 	= Config::get('Site.email');
			// $full_name		= $obj->full_name; 
			// $email			= $obj->email;
			// $password		= Input::get('password');
			// $route_url      = URL::to('account-verification/'.$obj->validate_string);
			// $select_url     = "<a href='".$route_url."'>".trans("Click Here")."</a>";
			
			// $emailActions	= EmailAction::where('action','=','account_verification')->get()->toArray();
			// $emailTemplates	= EmailTemplate::where('action','=','account_verification')->get(array('name','subject','action','body'))->toArray();
		
			// $cons 			= explode(',',$emailActions[0]['options']);
			// $constants 		= array();
			
			// foreach($cons as $key => $val){
			// 	$constants[] = '{'.$val.'}';
			// }
			
			// $subject 		= $emailTemplates[0]['subject'];
			// $rep_Array 		= array($full_name,$select_url,$route_url); 
			// $messageBody	= str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
			// $mail			= $this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);

			if (!empty(Input::get('mobile'))) {
				$mobile = $obj->country_code.''.$obj->mobile;
				$data = "Please Verify Your Mobile Number. Your Otp is ".$otp;
				$this->sendMessages($mobile,$data);
			}
			
			$response	=	array(
				'status'	=>	1,
				'msg'		=>	trans("Account has been registered successfully.")
			); 	
		}
		return  Response::json($response); 
	} 
	
	public function ForgotPassword(){ 
		$validator = Validator::make(
			Input::all(),
			array(
				'email' => 'required|email',
			),
			array(
				'email.required' 	=> trans("messages.home.the_email_field_is_required"),
				'email.email' 		=> trans("messages.msg.please_enter_valid_username"),
			)
		);
		if ($validator->fails()){
			//return Redirect::back()->withErrors($validator)->withInput();
			$errors 				=	$validator->messages();
			 $response	=	array(
				'status' 	=> 0,
				'msg' 	=> trans("messages.home.the_email_field_is_required")
			);
			return Response::json($response); 
			die;
			
		}else{
			$email		=	Input::get('email');   
			$userDetail	=	User::where('email',$email)
									->where('is_active','=',ACTIVE)
									->where('is_verified','=',ACTIVE)
									->where('is_deleted','=',INACTIVE)
									//->where('user_role_id','!=',SUPER_ADMIN_ROLE_ID)
									->first();
			if(!empty($userDetail)){
				$forgot_password_validate_string	= 	md5($userDetail->email);
				User::where('email',$email)->update(array('forgot_password_validate_string'=>$forgot_password_validate_string));
				
				$settingsEmail 		=  Config::get('Site.email');
				$email 				=  $userDetail->email;
				$username			=  $userDetail->full_name;
				$full_name			=  $userDetail->full_name;  
				$route_url      	=  URL::to('reset-password/'.$forgot_password_validate_string);
				$verify_link   		=   $route_url;
				
				$emailActions		=	EmailAction::where('action','=','forgot_password')->get()->toArray();
				$emailTemplates		=	EmailTemplate::where('action','=','forgot_password')->get(array('name','subject','action','body'))->toArray();
				$cons = explode(',',$emailActions[0]['options']);
				$constants = array();
				
				foreach($cons as $key=>$val){
					$constants[] = '{'.$val.'}';
				}
				$subject 			=  $emailTemplates[0]['subject'];
				$rep_Array 			= array($username,$verify_link,$route_url); 
				$messageBody		=  str_replace($constants, $rep_Array, $emailTemplates[0]['body']);
				$this->sendMail($email,$full_name,$subject,$messageBody,$settingsEmail);
				//Session::flash('flash_notice',  trans("messages.home.password_has_been_send_successfully"));  
				//$err				=	array();
				//$err['success']		=	1;
				//$err['message']		=	trans('messages.home.password_has_been_send_successfully');
				//return Response::json($err); 
				//return Redirect::back() ->withInput();
				
				$response	=	array(
					'status' 	=>	1,
					'msg' 		=>	trans('messages.home.password_has_been_send_successfully')
				); 
				return  Response::json($response); 
				die;	
			
			}else{
				$response	=	array(
					'status' 	=> 0,
					'msg' 	=> trans("messages.home.forgotpassword.your_email_is_not_registered_with_us")
				);
				return Response::json($response); 
				die;
				
				//Session::flash('error',  trans("messages.home.forgotpassword.your_email_is_not_registered_with_us")); 
				//return Redirect::back() ->withInput();
			}
		}
	} //end ForgotPassword()

		
	public function question_payment_api(){
		$questionId		=	Input::get('question_id');
		$user_id		=	DB::table('questions')->where('id',$questionId)->pluck('user_id');
		$walletAmount	=	DB::table('users')->where('id',$user_id)->pluck('wallet_balance');
		if(!empty($user_id)){ 
			$response	=	array(
				'status' 	=> 0,
				'msg' 	=> "No record is yet available."
			);
		} 

		if($walletAmount < Config::get('Site.question_fee')){
			$response	=	array(
				'status' 	=> 0,
				'msg' 	=> "Insufficient amount.",
				'question_fee'	=> Config::get('Site.question_fee'),
				'wallet_balance'=> $walletAmount,
			);
			return Response::json($response); 
			die;
		}else{
			$response	=	array(
			'status' 		=>	1,
			'question_fee'	=> Config::get('Site.question_fee'),
			'wallet_balance'=> $walletAmount
			); 
			return Response::json($response); 
			die;
		}
	}
	
	public function pay_question_fee_Wallet(){
		$questionId		=	Input::get('question_id');
		$wallet_balance	=	Input::get('amount');
		$authToken		=	Request::header('AuthToken');	
		$user_id		=	DB::table('users')->where('validate_string',$authToken)->pluck('id');
		$walletAmount	=	DB::table('users')->where('id',$user_id)->pluck('wallet_balance');
		//echo $user_id;
		if($walletAmount < Config::get('Site.question_fee')){
			$response	=	array(
				'status' 	=> 0,
				'msg' 	=> "Insufficient amount."
			);
			return Response::json($response); 
			die;
		}else{   
			$withdrawAmount				=	$walletAmount - Config::get('Site.question_fee');
			$obj 						= 	new Wallet();
			$obj->user_id				=   $user_id;
			$obj->withdraw				=   Config::get('Site.question_fee');
			$obj->status				=	ACTIVE;
			$obj->save();
			$id 						=   $obj->id;
			DB::table('wallets')->where('id',$id)->update(array('order_id' => ($id+ORDER_ID)));
			DB::table('users')->where('id',$user_id)->update(array('wallet_balance' => $withdrawAmount));
			DB::table('questions')->where('id',$questionId)->update(array('is_paid' => 1));
			$response	=	array(
				'status' 	=>	1,
				'msg' 	=>	"Thank for payments.You question posted successfully."
			); 
			return  Response::json($response); 
			die;
		}
	}
	
	/* Get All Question List From Questions tables using user id */
	public function get_user_questions_api(){
		$authToken		=	Request::header('AuthToken');	
		$user_id		=	DB::table('users')->where('validate_string',$authToken)->pluck('id');
		$result 		= 	DB::table('questions')->leftjoin('dropdown_managers','dropdown_managers.id','=','questions.category_id')
							->leftjoin('dropdown_managers as dropdown','dropdown.id','=','questions.level_id')
							->leftjoin('student_questions','student_questions.question_id','=','questions.id')
							->select('questions.*','dropdown_managers.name as category','dropdown.name as level',
							'student_questions.title as title','student_questions.file_name as file_name')
							->orderBy('id', 'DESC')
							->where('user_id',$user_id)
							->get();
		$response	=	array(
			'status' 			=>	1,
			'question_list' 	=>	$result
		); 
		return  Response::json($response); 
		die;
	}
	
	/* get perticuler question from quetion id */
	public function get_question_details_api(){	
		$id					=	Input::get('id');
		if(empty($id)){
			$response	=	array(
				'status' 	=> 0,
				'msg' 	=> "Question details not recieved."
			);
			return Response::json($response); 
			die;
		}
		$questionDetails	=	Question::where('questions.id',$id)
								->with('getStudentQuestions')
								->leftjoin('dropdown_managers','dropdown_managers.id','=','questions.category_id')
								->leftjoin('dropdown_managers as dropdown','dropdown.id','=','questions.level_id')
								->select('questions.*','dropdown_managers.name as category','dropdown.name as level')->first(); 
		$response	=	array(
			'status' 			=>	1,
			'question_details' 	=>	$questionDetails
		); 
		return  Response::json($response); 
		die;
	}
	
	public function save_payment_api(){
		$paymentResponseData		=	Input::all();
		$authToken				=		Request::header('AuthToken');	
		$user_id				=	    DB::table('users')->where('validate_string',$authToken)->pluck('id');
		if(empty($paymentResponseData)){
			$response	=	array(
				'status' 	=> 0,
				'msg' 	=> 'oops!Somrhing went wrong.'
				
			);
			return Response::json($response); 
			die;
		}
		$login_user		 			= 	Auth::user()->id;
		if(!empty($paymentResponseData['response']['response'])){	
			$obj 						= 	new Wallet();
			$obj->user_id				=   $user_id;
			$obj->transaction_data		=   json_encode($paymentResponseData['response']);
			$obj->deposit				=   $paymentResponseData['amount'];
			$obj->status				=	ACTIVE;
			$obj->save();
			
			$id 						= $obj->id;
			DB::table('wallets')->where('id',$id)->update(array('order_id' => ($id+ORDER_ID)));
			$walletAmount				=	DB::table('users')->where('id',$user_id)->pluck('wallet_balance');
			DB::table('users')->where('id',$user_id)->update(array('wallet_balance' => ($paymentResponseData['amount'] + $walletAmount)));
			$response	=	array(
				'success' 	=> true				
			);
			return  Response::json($response); 
		}else{
			$response	=	array(
				'status' 	=> 0,
				'errors' 	=> 1
				
			);
			return Response::json($response); 
			die;
		}
	}

	public function get_wallet_information_api(){
		$authToken	=		Request::header('AuthToken');	
		$user_id    =	    DB::table('users')->where('validate_string',$authToken)->pluck('id');
		$result		=	    DB::table('wallets')
							->orderBy('created_at', 'DESC')
							->where('user_id',$user_id)
							->get();
							
		$userWallet	=	   DB::table('users')
							->where('id',$user_id)
							->pluck('wallet_balance');
							
		$response	=		array(
								'status' 	=> 1,
								'wallte_balance'=>$userWallet,
								'data'		=>$result
							);
							
		return  Response::json($response); 
		die;
	}
	
	public function withdrawMoneyApi(){
		$authToken		=		Request::header('AuthToken');	
		$user_id   	 	=	    DB::table('users')->where('validate_string',$authToken)->pluck('id');
		$userBalance	=		User::where('id',$user_id)->pluck('wallet_balance');
		$result			=	    DB::table('withdraw_money')
								->orderBy('created_at', 'DESC')
								->where('user_id',$user_id)
								->get();
		$status   	 	=  		DB::table('withdraw_money')->where('user_id',$user_id)->orderBy('id','DESC')->limit(1)->pluck('status');
		$response		=		array(
									'status' 	=> 1,
									'show_form'	=> !empty($status)? $status :0,
									'balance'	=> $userBalance,
									'data'		=> $result,
									'message'	=> 'You have already requested withdraw money.We will notify you after process completion.'
								);
							
		return  Response::json($response); 
		die;
	}
	
	public function send_withdraw_request_api(){
		$formData		=		Input::all();
		$authToken		=		Request::header('AuthToken');	
		$user_id   	 	=	    DB::table('users')->where('validate_string',$authToken)->pluck('id');
		Validator::extend('custom_password', function($attribute, $value, $parameters) {
			if (preg_match('#[0-9]#', $value) && preg_match('#[a-zA-Z]#', $value) && preg_match('#[\W]#', $value)) {
				return true;
			} else {
				return false;
			}
		});
		$curentBalance	=	Input::get('wallet_balance');
		$validator = Validator::make(
			Input::all(),
			array(
				'amount'		=> 'required|integer|min:0|max:'.$curentBalance,
				'bank_details'	=> 'required',
			)
		);
		if ($validator->fails()){
			$response	=	array(
				'status' 	=> 0,
				'errors' 	=> $validator->errors()
			);
			return Response::json($response); 
			die; 
		}else{ 
			$obj 				=  new WithdrawMoney();
			$obj->amount 		=  Input::get('amount');
			$obj->user_id 		=  $user_id ;
			$obj->bank_details	=  Input::get('bank_details');
			
			$obj->save();
			$response	=	array(
				'status' 	=>	1,
				'messages' 	=>	 trans("Withdraw request send successfully.")
			); 
			return  Response::json($response); 
			die;
		}
	}
	/** 
	 * Function to saveChangePassword
	 *
	 * @param null
	 * 
	 * @return view page
	 */
	public function changePassword(){ 
		Input::replace($this->arrayStripTags(Input::all()));
		$formData			=	Input::all();
		//$login_user		 	= 	Auth::user();
		$login_user		 	= 	Input::get('user_id');
		$model_id		 	=   $login_user;
		$old_password    	= 	Input::get('old_password');
        $password         	= 	Input::get('new_password');
		Validator::extend('custom_password', function($attribute, $value, $parameters) {
			if (preg_match('#[0-9]#', $value) && preg_match('#[a-zA-Z]#', $value) && preg_match('#[\W]#', $value)) {
				return true;
			} else {
				return false;
			}
		});
		$rules    = 	array(
			'old_password' 	=>	'required',
			'new_password'	=>	'required|custom_password',
		);
		$validator 				= 	Validator::make(Input::all(), $rules,
		array(
			"old_password.required"			=>	trans("messages.globaluser.the_old_password_field_is_required"),
			"new_password.required"			=>	trans("messages.globaluser.the_new_password_field_is_required"),
			"new_password.custom_password"	=>	trans("messages.home.password_must_have_combination"),
		));
		if($validator->fails()){
			$response	=	array(
				'status' => 0,
				'msg' 	=> trans("messages.home.password_must_have_combination")
			);
			return Response::json($response); 
			die;
		}else{ 
			$obj 					=  User::find($model_id);
			$old_password 			= Input::get('old_password'); 
			$password 				= Input::get('new_password');
			
			if(Hash::check($old_password, $obj->getAuthPassword())){
				$obj->password = Hash::make($password);
				if($obj->save()){
					$data					=	array();
					$data['status']			=	1;
					$data['msg'] =   trans("messages.profile.password_update"); 
					return Response::json($data); 
					die;
				}
			}else{
				$err							=	array();
				$err['status']					=	0;
				$err['errors']['old_password']	=	trans("messages.profile.old_password_is_incorrect");
				return Response::json($err); 
				die;
			}
		}
	} //end changePassword()

}//end Class()
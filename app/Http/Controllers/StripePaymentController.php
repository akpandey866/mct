<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\Model\PlayerPackDetail;
use Session;
use Stripe;
   
class StripePaymentController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        return view('stripe');
    }
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        $packsDetails = Session::get('player_packs');
        if(!empty($packsDetails)){
            Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $getValues = Stripe\Charge::create ([
                    "amount" => $packsDetails['price'] *100,
                    "currency" => "aud",
                    "source" => $request->stripeToken,
                    "description" => "Additional player payment from MyClubtap" 
            ]);
        }
        $obj = new PlayerPackDetail();
        $obj->player_pack_id = $packsDetails['player_pack_id'];
        $obj->club = $packsDetails['club_id'];
        $obj->transaction_id = $getValues->id;
        $obj->payment_description = $getValues->description;
        $obj->save();
        Session::flash('success', 'Payment successful!');
        return back();
    }
}
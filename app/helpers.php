<?php
/*
* Custom Helper
*/
use App\Model\Slider;
use App\Model\User;
use App\Model\FixtureScorcard;
use App\Model\ScorerAccess;
use App\Model\Team;
use App\Model\FundraiserMessage;
use App\Model\UserTeamPlayers;
use App\Model\TeamPlayer;
use App\Model\VerifyUser;
use App\Model\Fundraiser;
use App\Model\Fixture;
use App\Model\Dropdown;
use App\Model\UserTeams;
use App\Model\PlayerPackDetail;
use App\Model\LockoutMailSendToUserLog;
if (!function_exists('get_slider')) {
    function get_slider($slider_type=null)
    {	
    	$array =Slider::where('is_active',1)->where('slider_type',$slider_type)->get();
        return $array;
 
    }
}
if (!function_exists('game_name')) {
    function game_name()
    {	$user_id = auth()->guard('web')->user()->id;
    	$getGame =User::where(['id'=>$user_id])->value('game_name');
        return $getGame;
 
    }
}

if (!function_exists('city_name')) {
    function city_name($user_id=null)
    {   
	if(!$user_id){
	$user_id = auth()->guard('web')->user()->id;
	}
        $gameName = User::leftjoin('cities','cities.id','=','users.city')
                        ->where('users.id',$user_id)
                        ->select('cities.name as city_name')
                        ->value('city_name');
        return $gameName;
 
    }
}
if (!function_exists('get_team_type')) {
    function get_team_type($team_id=null)
    {   
        $getTeamType = Team::where('teams.id',$team_id)
                        ->leftJoin('dropdown_managers','dropdown_managers.id','=','teams.type')->select('dropdown_managers.name as team_type')->first();
        return $getTeamType;
 
    }
}
if (!function_exists('getClubList')) {
    function getClubList()
    {	
    	$club = new User();
		$seniorClub = $club->getClubList();
		return $seniorClub;
    }
}

if (!function_exists('userTeamScore')) {
    function userTeamScore($teamArray=null)
    {    
        $teamIds = [];    
        foreach ($teamArray as $key => $value) {
            $teamIds[] =  $value->player_id;
        }
        $sumOfScore = FixtureScorcard::whereIn('player_id',$teamIds)->sum('fantasy_points');
       return $sumOfScore;
    }
}

if (!function_exists('scorer_ids')) {
    function scorer_ids()
    {   
       $scorerData = ScorerAccess::pluck('user_id','user_id')->all();
       return $scorerData;
    }
}

if (!function_exists('getScorecardStatus')) {
    function getScorecardStatus($fixtureId=null)
    {   
       $status = FixtureScorcard::where('fixture_id',$fixtureId)->orderBy('created_at','DESC')->value('status');
       return $status;
    }
}
if (!function_exists('get_admin_role_id')) {
    function get_admin_role_id()
    {   
       $adminId = ScorerAccess::where('user_id',Auth::guard('admin')->user()->id)->value('club_id');
       $is_game_activate = 0;
       if(!empty($adminId)){
            $is_game_activate =User::where('id',$adminId)->value('is_game_activate');
       }
       return $is_game_activate;
    }
}
if (!function_exists('getToolTipMessage')) {
    function getToolTipMessage()
    {   
       $club_id = \Session::get('sess_club_name');
       $getMessage = '';
       if(!empty($club_id)){
          $getMessage = FundraiserMessage::where('club_id',$club_id)->value('message');
       }
       return $getMessage;
    }
}
if (!function_exists('is_fundraiser_paid')) {
    function is_fundraiser_paid()
    {   
        $userId = Auth::guard('web')->user()->id;
        $club_id = \Session::get('sess_club_name');
        $isPaid = DB::table('fundraisers')->where('club_id',$club_id)->where('user_id',$userId)->value('amount');
        return $isPaid;
    }
}
if (!function_exists('getFundraiserAmount')) {
    function getFundraiserAmount()
    {   
        $club_id = \Session::get('sess_club_name');
        $sumAmount = DB::table('fundraisers')->where('club_id',$club_id)->sum('amount');
        return $sumAmount;
    }
}

if (!function_exists('getGameStatus')) {
    function getGameStatus($fixture_id =null)
    {   
        $getMessage = FixtureScorcard::where('fixture_id',$fixture_id)->orderBy('id','DESC')->value('status');
        return $getMessage;
    }
}

if (!function_exists('playerIdInTeam')) {
    function playerIdInTeam($playerId =null)
    {   
      $playerIdUserteamExists = UserTeamPlayers::where('player_id',$playerId)->count();
      $playerIdTeamPlayerExists = TeamPlayer::where('player_id',$playerId)->count();
      $checkId = 0;
      if(!empty($playerIdUserteamExists || !empty($playerIdTeamPlayerExists)) ){
        $checkId = 1;
      }
      return $checkId;
    }
}
if (!function_exists('get_team_type_from_fixture')) {
    function get_team_type_from_fixture($club_id=null)
    {    
        $clubDetails = User::where('id',$club_id)->value('game_mode');
        $teamTypeName ='';
        $teamType = Team::where('club',$club_id)->value('type');
        if($clubDetails == 1){ 
          if(array_key_exists($teamType, config::get('senior_team_type'))){
            $teamTypeName = config::get('senior_team_type')[$teamType];
          }
        }if($clubDetails == 2){
          if(array_key_exists($teamType, config::get('junior_team_type'))){
            $teamTypeName = config::get('junior_team_type')[$teamType];
          }
        }if($clubDetails == 3){
          if(array_key_exists($teamType, config::get('league_team_type'))){
            $teamTypeName = config::get('league_team_type')[$teamType];
          }
        }
        return $teamTypeName;
 
    }
}

if (!function_exists('players_ids')) {
    function players_ids()
    {   
      $userId = Auth::guard('web')->user()->id;
      $club_id = \Session::get('sess_club_name');
      $playersIds = VerifyUser::where('club_id',$club_id)->where('user_id',$userId)->pluck('user_id','user_id')->all();
      return $playersIds;
    }
}

if (!function_exists('showFundraiserUser')) {
    function showFundraiserUser($userId = null)
    {  
      $club_id = \Session::get('sess_club_name');
      $fundraiserExists = Fundraiser::where('club_id',$club_id)->where('user_id',$userId)->count();
      return $fundraiserExists;
    }
}
if (!function_exists('checkIp')) {
    function checkIp($userId = null)
    {  
      $ip = $_SERVER["REMOTE_ADDR"];
      $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
      if($ipdat->geoplugin_countryName == "India"){
        return 1;
      }
      return 0;
    }
}
if (!function_exists('getReferalCode')) {
    function getReferalCode()
    {  
      $referalCode = !empty(Auth::guard('web')->user()->referral_code) ? Auth::guard('web')->user()->referral_code:0;
      return $referalCode;
    }
}
if (!function_exists('getNotificationCount')) {
    function getNotificationCount()
    {  
      $getNotificationCount = LockoutMailSendToUserLog::where('user_id',Auth::guard('web')->user()->id)->where('is_read',0)->count();
      return $getNotificationCount;
    }
}
if (!function_exists('get_user_notification')) {
    function get_user_notification()
    {  
      $getNotification = LockoutMailSendToUserLog::where('user_id',Auth::guard('web')->user()->id)->orderBy('created_at','DESC')->limit(20)->get();
      return $getNotification;
    }
}

if (!function_exists('humanTiming')) {
  function humanTiming($time) {
      $time = strtotime($time);
      $time = time() - $time; // to get the time since that moment
      $tokens = array(
          31536000 => 'year',
          2592000 => 'month',
          604800 => 'week',
          86400 => 'day',
          3600 => 'hour',
          60 => 'minute',
          1 => 'second'
      );
      foreach ($tokens as $unit => $text) {
          if ($time < $unit) {
              continue;
          }
          $numberOfUnits = floor($time / $unit);
          return $numberOfUnits.' '.$text.(($numberOfUnits > 1) ? 's' : '');
      }
  }
}

if (!function_exists('getPrimaryTeamName')) {
    function getPrimaryTeamName($team_id=null)
    {  
      $teamName = Team::where('id',$team_id)->value('name');
      return $teamName;
    }
}

if (!function_exists('fixture_status_details')) {
    function fixture_status_details($status=null)
    {  
      $fixtureids = Fixture::where('is_active',1)->where('club',Auth::guard('admin')->user()->id)->pluck('id','id')->all();
      $fixtureCount = FixtureScorcard::whereIn('fixture_id',$fixtureids)->where('fixture_scorecards.status',$status)->groupBy('fixture_id')->select('fixture_id')->get();      
      $fixtureCountIds = 0;
      if(!$fixtureCount->isEmpty()){
        foreach ($fixtureCount as $key => $value) {
          $fixtureCountIds++;
        }
      }
      return $fixtureCountIds;
    }
}
 
if (!function_exists('admin_fixture_status_details')) {
    function admin_fixture_status_details($status=null)
    {  
      $fixtureids = Fixture::where('is_active',1)->pluck('id','id')->all();
      $fixtureCount = FixtureScorcard::whereIn('fixture_id',$fixtureids)->where('fixture_scorecards.status',$status)->groupBy('fixture_id')->select('fixture_id')->get();      
      $fixtureCountIds = 0;
      if(!$fixtureCount->isEmpty()){
        foreach ($fixtureCount as $key => $value) {
          $fixtureCountIds++;
        }
      }
      return $fixtureCountIds;
    }
}

if (!function_exists('fixture_status_details_not_started')) {
    function fixture_status_details_not_started()
    {  
      $fixtureids = Fixture::where('is_active',1)->where('club',Auth::guard('admin')->user()->id)->pluck('id')->all();


      $fixtureScorecardsIds = FixtureScorcard::whereIn('fixture_id',$fixtureids)->groupBy('fixture_id')->select('fixture_id')->get();
      $firstFixtureScorecardsData=[];
      $fixtureScorecardsData=[];
      
      if(!$fixtureScorecardsIds->isEmpty()){
        foreach ($fixtureScorecardsIds as $key => $value) {
          $fixtureScorecardsData[]  = $value->fixture_id;
        }
      }


      $fixtureCount = FixtureScorcard::whereIn('fixture_id',$fixtureids)->where('fixture_scorecards.status',0)->groupBy('fixture_id')->select('fixture_id')->get();
       $secondFixtureData = 0;
      if(!$fixtureCount->isEmpty()){
        foreach ($fixtureCount as $key => $value) {
          $secondFixtureData++;
        }
      }


      $arr_1 = [];
      $array_1_count= 0 ;
      $fixtureTotalCount = 0;
      if(!empty($fixtureids) && $fixtureScorecardsData){
        $arr_1 = array_diff($fixtureids, $fixtureScorecardsData);
        $array_1_count = count($arr_1);
      }
 
      return $secondFixtureData+$array_1_count;
    }
}

if (!function_exists('admin_fixture_status_details_not_started')) {
    function admin_fixture_status_details_not_started()
    {  
      $fixtureids = Fixture::where('is_active',1)->pluck('id')->all();


      $fixtureScorecardsIds = FixtureScorcard::whereIn('fixture_id',$fixtureids)->groupBy('fixture_id')->select('fixture_id')->get();
      $firstFixtureScorecardsData=[];
      $fixtureScorecardsData=[];
      
      if(!$fixtureScorecardsIds->isEmpty()){
        foreach ($fixtureScorecardsIds as $key => $value) {
          $fixtureScorecardsData[]  = $value->fixture_id;
        }
      }


      $fixtureCount = FixtureScorcard::whereIn('fixture_id',$fixtureids)->where('fixture_scorecards.status',0)->groupBy('fixture_id')->select('fixture_id')->get();
       $secondFixtureData = 0;
      if(!$fixtureCount->isEmpty()){
        foreach ($fixtureCount as $key => $value) {
          $secondFixtureData++;
        }
      }


      $arr_1 = [];
      $array_1_count= 0 ;
      $fixtureTotalCount = 0;
      if(!empty($fixtureids) && $fixtureScorecardsData){
        $arr_1 = array_diff($fixtureids, $fixtureScorecardsData);
        $array_1_count = count($arr_1);
      }
 
      return $secondFixtureData+$array_1_count;
    }
}
if (!function_exists('get_here_about_us')) {
    function get_here_about_us($dropdownId=null) 
    {  

      $userList = UserTeams::where('club_id',Auth::guard('admin')->user()->id)->where('is_active',1)->pluck('user_id')->all();
      $totalUsers = User::where('is_deleted',0)->whereIn('id',$userList)->count();
      $hereAboutUsCount = User::where('about_us',$dropdownId)->whereIn('id',$userList)->count('id');
      $totalSubscribedPercentage = 0;
      if(!empty($hereAboutUsCount)){
        $totalSubscribedPercentage = ($hereAboutUsCount / $totalUsers) * 100;
      }                  

      return $totalSubscribedPercentage;
    } 
}
if (!function_exists('admin_get_here_about_us')) {
    function admin_get_here_about_us($dropdownId=null) 
    {  

      $userList = UserTeams::where('is_active',1)->pluck('user_id')->all();
      $totalUsers = User::where('is_deleted',0)->whereIn('id',$userList)->count();
      $hereAboutUsCount = User::where('about_us',$dropdownId)->whereIn('id',$userList)->count('id');
      $totalSubscribedPercentage = 0;
      if(!empty($hereAboutUsCount)){
        $totalSubscribedPercentage = ($hereAboutUsCount / $totalUsers) * 100;
      }                  

      return $totalSubscribedPercentage;
    } 
}
if(!function_exists('get_player_fee')){
  function get_player_fee($club_id=null){
    $gameMode = PlayerPackDetail::leftJoin('player_packs', 'player_pack_details.player_pack_id', '=', 'player_packs.id')->where('club',$club_id)->select('player_pack_details.*','player_packs.price as price')->selectRaw('sum(player_packs.price) AS player_fee')->first();
    $playerFee = "";
    if(!empty($gameMode)){
      $playerFee = $gameMode->player_fee;
    }
    return $playerFee;
  }
}
<?php
namespace App\Model; 

use Illuminate\Database\Eloquent\Model;
use Eloquent,DB,App;

class PlayerPoint extends Eloquent
{
	protected $table = 'player_points';
	protected $fillable = ['fixture_id','inning'];
}

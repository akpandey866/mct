<?php
namespace App\Model; 

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Eloquent,DB,App;

class Club extends Eloquent implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
	
	
	protected $dates = ['deleted_at'];
	
	protected $table = 'clubs';

    /**
     * The database table used by the model.
     *
     * @var string
     */


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    
	  /* Scope Function 
	 *
	 * @param null 
	 *
	 * return query
	 */
 
	public function scopeActiveConditions($query){
		return $query->where('is_active',1)->where('is_verified',1);
		
	}//end ScopeActiveCondition
  
	/**
	* hasMany function for bind userLastLogin model 
	*
	* @param null 
	*
	* return query
	*/
	public function userLastLogin($query){
		return $this->hasMany('App\Model\userLastLogin','user_id');
	}//end userLastLogin
	
	
	
	
	
	/**
	* Function for get country list
	*
	* @param null 
	*
	* return query
	*/
	public function get_club_list(){
		$clubList		=	DB::table('users')->where('is_active',1)->where('user_role_id',3)->where('is_deleted',0)->pluck('club_name','id')->all();
		return $clubList;
	}
    
	/**
	* Function for get club listing using mode
	*
	* @param null 
	*
	* return query
	*/
	public function get_club_mode($mode = null){
		$clubList		=	DB::table('users')->where('is_active',1)->where('is_game_activate', 1)->where(['user_role_id'=>3,'game_mode'=>$mode])->where('is_deleted',0)->pluck('club_name','id')->all();
		return $clubList;
	}
	

	public function addNew($input)
    {
        $check = static::where('facebook_id',$input['facebook_id'])->first();


        if(is_null($check)){
            return static::create($input);
        }


        return $check;
    }
}

<?php
namespace App\Model; 

use Illuminate\Database\Eloquent\Model;
use Eloquent,DB,App;

class Availability extends Eloquent
{
	protected $table = 'availabilities'; 

    public function player_data()
    {
        return $this->belongsTo('App\Model\Player', 'player');
    }

}

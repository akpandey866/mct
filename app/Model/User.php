<?php
namespace App\Model; 

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Spatie\Permission\Traits\HasRoles;
use Eloquent,DB,App;

class User extends Eloquent implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
    
{
    use Authenticatable, Authorizable, CanResetPassword,HasRoles;
	protected $dates = ['deleted_at'];
	
	protected $table = 'users';

    /**
     * The database table used by the model.
     *
     * @var string
     */


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    
	  /* Scope Function 
	 *
	 * @param null 
	 *
	 * return query
	 */
 
	public function scopeActiveConditions($query){
		return $query->where('is_active',1)->where('is_verified',1);
		
	}//end ScopeActiveCondition
  
	/**
	* hasMany function for bind userLastLogin model 
	*
	* @param null 
	*
	* return query
	*/
	public function userLastLogin($query){
		return $this->hasMany('App\Model\userLastLogin','user_id');
	}//end userLastLogin
	
	
	/**
	* Function for get country list
	*
	* @param null 
	*
	* return query
	*/
	public function getCountry(){
		$countryList		=	DB::table('countries')->where('status',1)->orderBy('name','ASC')->lists('name','id');
		return $countryList;
	}
    

    public static function getClubList(){
        $clubList = User::where(['is_active'=>1,'user_role_id'=>CLUBUSER,'is_deleted'=>0])->orderBy('club_name','ASC')->pluck('club_name','id')->all();
        return $clubList;
    }
    public function get_user_list(){
        $list = User::where('user_role_id',USER)->where('is_active',1)->where('is_deleted',0)->where('is_verified',1)->pluck('full_name','id')->all();
        return $list;
    }

    public function user_teams(){
    	return $this->hasMany('App\Model\UserTeams', 'user_id');
    }

}

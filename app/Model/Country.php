<?php
namespace App\Model; 
use Eloquent;
class Country extends Eloquent
{

	protected $table = 'countries';
	/**
	* Function for get country list
	*
	* @param null 
	*
	* return query
	*/
	public static function get_country(){
		$countries = Country::pluck('name','id')->all();
		return $countries;
	}
}

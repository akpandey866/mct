<?php
namespace App\Model; 

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Eloquent,DB,App;

class Player extends Eloquent 
{
	protected $table = 'players';

	public function get_players(){
		$getList = Player::where('is_active',1)->pluck('full_name','id')->all();
		return $getList;
	}
	public function get_players_by_club($club_id){
		$getList = Player::where('is_active',1)->where('club',$club_id)->pluck('full_name','id')->all();
		return $getList;
	}
	public function get_scorecard(){
		
		 return $this->hasMany('App\Model\FixtureScorcard', 'player_id');
	}
}

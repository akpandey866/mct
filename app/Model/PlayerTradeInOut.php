<?php
namespace App\Model; 
use Eloquent;
/**
 * Contact Model
 */
 
class PlayerTradeInOut extends Eloquent   {
	
	/**
	 * The database collection used by the model.
	 *
	 * @var string
	 */
 
protected $table = 'player_trade_in_out';


    public function player()
    {
        return $this->belongsTo('App\Model\Player', 'player_id');
    }


} // end Contact class

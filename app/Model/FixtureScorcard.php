<?php
namespace App\Model; 

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Eloquent,DB,App;

class FixtureScorcard extends Eloquent 
{
	protected $table = 'fixture_scorecards';
	protected $guarded = [];


 	public function fixture()
    {
        return $this->belongsTo('App\Model\Fixture', 'fixture_id');
    }

	public function player()
	{
	    return $this->belongsTo('App\Model\Player', 'player_id');
	}


}

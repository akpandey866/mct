<?php
include(app_path().'/global_constants.php');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	Route::get('email-test', function(){
  
		$details['email'] = 'ankit123@mailinator.com';
		$details['name'] = 'ankit pandey';
	  
	    dispatch(new App\Jobs\SendEmailJob($details));
	  
	    dd('done');
	});
	Route::get('set-cron-lockout', 'CronController@setLockout');
	Route::get('change-complete-status-cron', 'CronController@changeCompleteStatus');
	Route::get('stripe', 'StripePaymentController@stripe');
	Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
	Route::get('endorspage', 'CronController@show_endorsed')->name('show.endores');
	Route::get("user-sharer/{id}","front\GlobalController@userSharer");
	/*Route::get('send-lockout-mail-by-cron',array('as'=>'Global.sendLockoutMailByCron','uses'=>'front\GlobalController@sendLockoutMailByCron'));*/


	Route::get('update-gw-point', 'CronController@updateGWPoint');

	Route::get('update-month-point', 'CronController@updateMonthPoint'); 

	Route::get('send-lockout-mail-by-cron', 'CronController@sendLockoutMailByCron');

	Route::get('update-old-gw-data', 'CronController@updateOldGWData'); // This function is only for to update the old gw data, before run this take the backup of original database 


	Route::get('update-old-month-data', 'CronController@updateOldMonthData'); // This function is only for to update the old gw data, before run this take the backup of original database 

	

		
	Route::group(array('middleware' => 'App\Http\Middleware\GuestFront','namespace'=>'front'), function() {
		Route::get('/', 'UsersController@index');
		
		/*CMS pages routing start here*/
		Route::get('faqs',array('as'=>'User.faq','uses'=>'UsersController@faq'));
		Route::get('/help&support',array('as'=>'Home.contact','uses'=>'UsersController@contactUs'));
		Route::post('/contact-us',array('as'=>'Home.contact','uses'=>'UsersController@contactUs'));
		Route::post('/subscribe',array('as'=>'Home.subscribe','uses'=>'UsersController@subscribe'));
		Route::get('/pages/{slug}','UsersController@showCms');
		Route::get('/about','UsersController@showAboutUs');
		Route::get('/features','CommonController@aboutClub');
		Route::get('/pricing',array('as'=>'Common.pricing','uses'=>'CommonController@pricing'));
		Route::get('testimonials', 'GlobalController@testimonials')->name('commmon.testimonials');
		//Route::get('/game-rules',array('as'=>'Common.pricing','uses'=>'CommonController@pricing')); 
		/*CMS pages routing finish here*/

		/*Ajax routing apply here*/

		Route::any('get-club-list/', 'CommonController@getClubList')->name('getClubList');
		Route::post('game-name/', 'CommonController@getGameName')->name('getGameName');
		/*Ajax routing finish here*/

		/*State city list start here*/
		Route::any('/get-state','CommonController@getState');	
		Route::post('/get-city','CommonController@getCity');	
		/*State city list finish here*/
		
		/*Signup,login,forgot-password,reset password routing start here*/
		/*Game or Club sign up*/
		Route::get('signupasagame',array('as'=>'Home.signup','uses'=>'HomesController@signupView'));
		Route::post('signupasagame',array('as'=>'Home.saveSignup','uses'=>'HomesController@signup'));
		/*As a User Sign up*/
		Route::get('signupasauser',array('as'=>'Home.userSignup','uses'=>'HomesController@userSignupView'));
		Route::post('signupasauser',array('as'=>'Home.userSignup','uses'=>'HomesController@userSignup'));

		Route::get('/signup',array('as'=>'Home.signupAsFront','uses'=>'HomesController@frontSignup'));


		Route::get('/forgot-password',array('as'=>'Home.forgotpassword','uses'=>'HomesController@ViewForgotPassword'));
		Route::post('/forgot-password',array('as'=>'Home.forgotpassword','uses'=>'HomesController@ForgotPassword'));
		Route::get('account-verification/{validate_string}',array('as'=>'Home.verify','uses'=>'HomesController@Verify'));	
		Route::get('reset-password/{validate_string}',array('as'=>'Home.reset_password','uses'=>'HomesController@resetPassword'));
		Route::post('reset-password',array('as'=>'Home.save_reset_password','uses'=>'HomesController@saveResetPassword'));
		Route::get('/login',array('as'=>'Home.login','uses'=>'HomesController@login'));
		Route::post('/login',array('as'=>'Home.login','uses'=>'HomesController@loginUser'));
		Route::get('/logout', 'HomesController@logout');

		Route::get('/unsubscribe/{enc_id}',array('as'=>'Global.unsubscribe','uses'=>'GlobalController@unsubscribe_newsletter'));
			
		/*Signup,login,forgot-password,reset password routing finish here*/
	});
	/*Auth routing start*/
	Route::group(array('middleware' => 'App\Http\Middleware\AuthFront','namespace'=>'front'), function() {
		Route::get('lobby/{club_id?}/{game_mode?}',array('as'=>'User.lobby','uses'=>'UsersController@lobbyView'));
		Route::get('availability',array('as'=>'User.availability','uses'=>'UsersController@availability'));
		Route::get('fixtures',array('as'=>'Common.fixtures','uses'=>'CommonController@fixtures'));
		Route::get('my-team/{club_id?}/{game_mode?}',array('as'=>'User.myTeam','uses'=>'UsersController@myTeam'));
		Route::post('get-player-data',array('as'=>'User.getPlayerData','uses'=>'UsersController@getPlayerData'));
		Route::get('sponsors',array('as'=>'Common.sposnsor','uses'=>'CommonController@gameSponsor'));
		Route::get('history',array('as'=>'Common.history','uses'=>'CommonController@history'));
		Route::get('load-history-data',array('as'=>'Common.loadHistoryData','uses'=>'CommonController@loadHistoryData'));

		
		Route::get('trade-history',array('as'=>'Common.tradeHistory','uses'=>'CommonController@tradeHistory'));
		Route::any('winner',array('as'=>'Common.winner','uses'=>'CommonController@winner'));
		Route::any('get-winner-data',array('as'=>'Common.getWinnerData','uses'=>'CommonController@getWinnerData'));
		Route::get('partners',array('as'=>'Common.partners','uses'=>'CommonController@partners'));
		Route::get('prizes',array('as'=>'Common.prize','uses'=>'CommonController@prizes'));
		Route::get('stats',array('as'=>'Common.stats','uses'=>'CommonController@stats'));
		Route::get('points/{gw_cnt?}',array('as'=>'Common.points','uses'=>'CommonController@points'));
		Route::post('save-my-team',array('as'=>'User.saveMyTeam','uses'=>'UsersController@saveMyTeam'));
		Route::post('activate-capton-card',array('as'=>'User.activateCaptonCard','uses'=>'UsersController@activateCaptonCard'));

		Route::post('activate-twelve-man-card',array('as'=>'User.activateTwelveManCard','uses'=>'UsersController@activateTwelveManCard'));
		Route::post('activate-dealer-card',array('as'=>'User.activateDealerCard','uses'=>'UsersController@activateDealerCard'));
		Route::post('activate-flipper-card',array('as'=>'User.activateFlipperCard','uses'=>'UsersController@activateFlipperCard'));

		Route::post('activate-shield-steal-card',array('as'=>'User.activateShieldStealCard','uses'=>'UsersController@activateShieldStealCard'));

		Route::get('remove-booster',array('as'=>'User.removeBooster','uses'=>'UsersController@removeBooster'));

		Route::get('logout-admin',array('as'=>'Common.logoutAdmin','uses'=>'CommonController@logoutAdmin'));
		Route::get('rank',array('as'=>'Common.teamRank','uses'=>'CommonController@teamRank'));
		Route::get('player-profile/{player_id?}',array('as'=>'Common.playerProfile','uses'=>'CommonController@playerProfile'));

		Route::get('match-scorecards/{id}',array('as'=>'Global.matchScorecard','uses'=>'GlobalController@matchScorecard'));
		Route::post('player-account',array('as'=>'Gloabal.savePlayerAccount','uses'=>'GlobalController@savePlayerAccount'));
		Route::get('player-account',array('as'=>'Gloabal.playerAccount','uses'=>'GlobalController@playerAccount'));
		Route::get('fundraiser',array('as'=>'Gloabal.fundraiserListing','uses'=>'GlobalController@fundraiserListing'));

		Route::get('chat',array('as'=>'Common.chat','uses'=>'GlobalController@userChat'));
		Route::get('refer-list',array('as'=>'Global.referList','uses'=>'GlobalController@getReferUserList'));


		Route::get('refer',array('as'=>'Common.refer','uses'=>'CommonController@userRefer'));

		Route::get('notification',array('as'=>'Global.notification','uses'=>'GlobalController@notification'));
		Route::get('check-notification',array('as'=>'Global.checkNotification','uses'=>'GlobalController@checkNotification'));

		/*Update-profile routing*/
		Route::get('update-profile',array('as'=>'User.updateProfile','uses'=>'UsersController@updateProfile'));
		Route::post('update-profile',array('as'=>'User.SaveupdateProfile','uses'=>'UsersController@saveUpdateProfile'));
		Route::post('get-club-mode-listing','CommonController@getClubModeList');
		Route::post('get-club-game-name','CommonController@getClubGameName');

		/*My Comps route start here*/
		Route::get('my-comps',array('as'=>'User.mycomps','uses'=>'MyCompsController@index'));
		/*My Comps route finish here*/
		
		Route::post('pay-fundraiser',array('as'=>'User.payFundraiser','uses'=>'CommonController@payFundraiser'));
		Route::post('pay-fundraiser-fee',array('as'=>'Common.payFundraiserFee','uses'=>'CommonController@payFundraiserFee'));

	});
	Route::group(array('prefix' => 'admin'), function() {
		Route::group(array('middleware' => 'App\Http\Middleware\GuestAdmin','namespace'=>'admin'), function() {
			Route::get('','AdminLoginController@login');
			Route::any('/login','AdminLoginController@login')->name('admin.login');
			Route::get('forget_password','AdminLoginController@forgetPassword');
			Route::get('reset_password/{validstring}','AdminLoginController@resetPassword');
			Route::post('send_password','AdminLoginController@sendPassword');
			Route::post('save_password','AdminLoginController@resetPasswordSave');
			
		});
		
		Route::group(array('middleware' =>['AuthAdmin'],'namespace'=>'admin'), function() {
			Route::get('/logout','AdminLoginController@logout');
			Route::get('dashboard','AdminDashBoardController@showdashboard');
			Route::any('club-dashboard','AdminDashBoardController@showClubDashboard');
			Route::any('set-up-checklist','AdminDashBoardController@setUpChecklist');
			Route::get('/myaccount','AdminDashBoardController@myaccount');
			Route::post('/myaccount','AdminDashBoardController@myaccountUpdate');
			Route::get('/change-password','AdminDashBoardController@change_password');
			Route::post('/changed-password','AdminDashBoardController@changedPassword');
			Route::any('/settings',array('as'=>'settings.listSetting','uses'=>'SettingsController@listSetting'));
			Route::get('/settings/add-setting','SettingsController@addSetting');
			Route::post('/settings/add-setting','SettingsController@saveSetting');
			Route::get('/settings/edit-setting/{id}','SettingsController@editSetting');
			Route::post('/settings/edit-setting/{id}','SettingsController@updateSetting');
			Route::get('/settings/prefix/{slug}','SettingsController@prefix');
			Route::post('/settings/prefix/{slug}','SettingsController@updatePrefix');
			Route::get('/settings/delete-setting/{id}','SettingsController@deleteSetting');

			/** Club routing**/
			Route::any('/club',array('as'=>'club.index','uses'=>'ClubController@index'));
			Route::get('/club/add-club','ClubController@addClub');
			Route::post('/club/add-club',array('as'=>'Club.saveClub','uses'=>'ClubController@saveClub'));
			Route::get('/club/edit-club/{id}','ClubController@editClub');
			Route::get('/club/view-club/{id}','ClubController@viewClub');	
			Route::post('/club/edit-club', array('as'=>'Club.updateClub','uses'=>'ClubController@updateClub'));
			Route::get('/club/delete-club/{id}','ClubController@deleteClub');
			Route::get('getcity/{id}',array('as'=>'Club.getcity','uses'=>'ClubController@getCity'));
			Route::get('club/update-status/{id}/{status}',array('as'=>'Club.status','uses'=>'ClubController@updateStatus'));
			Route::get('getCountaryTimezone/{id}','ClubController@getCountaryTimezone');

			Route::get('getstate/{id}',array('as'=>'Club.getstate','uses'=>'ClubController@getState'));
			Route::get('getstateedit/{state_id}/{country_id}',array('as'=>'Club.getStateEdit','uses'=>'ClubController@getStateEdit'));
			Route::get('getcityedit/{state_id}/{city_id}',array('as'=>'Club.getcityEdit','uses'=>'ClubController@getCityEdit'));

			/*For Club Routing*/
			Route::get('/club/update-game','ClubController@updateGame');
			/** Club routing**/
			
			/** Player routing**/
			Route::get('/player/add-import-player','PlayerController@addImportPlayer');
			Route::post('/player/import-player',array('as'=>'Player.importPlayer','uses'=>'PlayerController@importPlayer'));

			Route::any('/player',array('as'=>'player.index','uses'=>'PlayerController@index'));
			Route::get('/player/add-player','PlayerController@addPlayer');
			Route::post('/player/add-player',array('as'=>'Player.savePlayer','uses'=>'PlayerController@savePlayer'));
			Route::get('/player/edit-player/{id}','PlayerController@editPlayer');
			Route::get('/player/view-player/{id}','PlayerController@viewPlayer');	
			Route::post('/player/edit-player', array('as'=>'Player.updatePlayer','uses'=>'PlayerController@updatePlayer'));
			Route::get('/player/delete-player/{id}','PlayerController@deletePlayer');
			//Route::get('getstate/{id}',array('as'=>'Player.getstate','uses'=>'PlayerController@getState'));
			//Route::get('getcity/{id}',array('as'=>'Player.getcity','uses'=>'PlayerController@getCity'));
			Route::get('player/update-status/{id}/{status}',array('as'=>'Player.status','uses'=>'PlayerController@updateStatus'));
			Route::post('get-team-list',array('as'=>'Team.getTeamList','uses'=>'PlayerController@getTeamList'));
			Route::post('/add-more-player','PlayerController@addMorePlayer');

			Route::post('check-player-count','PlayerController@checkPlayerCount');
			Route::post('/player/get-player-list','PlayerController@getPlayelistForClub');
			/** Player routing**/
			
			/** Team routing**/
			Route::any('/team',array('as'=>'team.index','uses'=>'TeamController@index'));
			Route::get('/team/add-team','TeamController@addTeam');
			Route::post('/team/add-team',array('as'=>'Team.saveTeam','uses'=>'TeamController@saveTeam'));
			Route::get('/team/edit-team/{id}','TeamController@editTeam');
			Route::get('/team/view-team/{id}','TeamController@viewTeam');	
			Route::post('/team/edit-team', array('as'=>'Team.updateTeam','uses'=>'TeamController@updateTeam'));
			Route::get('/team/delete-team/{id}','TeamController@deleteTeam');
			Route::get('team/update-status/{id}/{status}',array('as'=>'Team.status','uses'=>'TeamController@updateStatus'));
			Route::any('/team/get-player',array('as'=>'Team.getTeam','uses'=>'TeamController@getTeam'));
			Route::post('/get-grade',array('as'=>'Team.getGrade','uses'=>'TeamController@getGrade'));
			Route::post('/get-team-type',array('as'=>'Team.getTeamType','uses'=>'TeamController@getTeamType'));

			Route::get('/team/admin-add-team','TeamController@adminAddTeam');
			Route::post('/team/admin-add-team',array('as'=>'Team.adminSaveTeam','uses'=>'TeamController@adminSaveTeam'));
			/** Team routing**/

			/*Team Players start here*/
			Route::any('/team-player/{fixture_id}',array('as'=>'TeamPlayer.index','uses'=>'TeamPlayerController@index'));
			//Route::get('/add-team-player/{team_id}','TeamPlayerController@addTeamPlayer');
			Route::post('/add-team-player',array('as'=>'TeamPlayer.saveTeamPlayer','uses'=>'TeamPlayerController@saveTeamPlayer'));
			Route::get('/edit-team-player/{team_id}/{id}','TeamPlayerController@editTeamPlayer');
			Route::post('/edit-team-player/{team_id}/{id}', array('as'=>'TeamPlayer.updateTeamPlayer','uses'=>'TeamPlayerController@updateTeamPlayer'));
			Route::get('/delete-team-player/{id}/{fixture_id}','TeamPlayerController@deleteTeamPlayer');
			Route::post('/add-more-team-player','TeamPlayerController@addMoreTeamPlayer');
			Route::get('/lock-player/{id}','TeamPlayerController@lockPlayer')->name('lockGame');
			/*Team Players finish here*/


			/*Fixture routing start here*/
			Route::any('/fixture',array('as'=>'fixture.index','uses'=>'FixtureController@index'));
			Route::get('/fixture/add-fixture','FixtureController@addFixture');
			Route::post('/fixture/add-fixture',array('as'=>'Fixture.saveFixture','uses'=>'FixtureController@saveFixture'));
			Route::get('/fixture/edit-fixture/{id}','FixtureController@editFixture');
			Route::get('/fixture/view-fixture/{id}','FixtureController@viewFixture');	
			Route::post('/fixture/edit-fixture', array('as'=>'Fixture.updateFixture','uses'=>'FixtureController@updateFixture'));
			Route::get('/fixture/delete-fixture/{id}','FixtureController@deleteFixture');
			Route::get('fixture/update-status/{id}/{status}',array('as'=>'Fixture.status','uses'=>'FixtureController@updateStatus'));
			Route::any('fixture/scorecards/{id}',array('as'=>'fixture.scorcards','uses'=>'FixtureController@scorecards'));
			Route::post('/fixture/edit-fixture-scorcard', array('as'=>'Fixture.editFixtureScorcard','uses'=>'FixtureController@editFixtureScorcard'));	
			Route::get('fixture/update-player-price/{id}',array('as'=>'Fixture.status','uses'=>'FixtureController@updatePlayerPrice'));
			Route::post('get-club-mode-team','FixtureController@getClubTeamList');	
			Route::post('get-club-mode-grade','FixtureController@getClubModeGrade');			
			Route::post('updateFixtureScorecardStatus','FixtureController@updateScorecardStatus');	

			Route::any('/fixture/show-scorecard/{id}',array('as'=>'fixture.showScorecard','uses'=>'FixtureController@showScorecard'));
			Route::any('/fixture/show-sqad/{id}',array('as'=>'fixture.showSquad','uses'=>'FixtureController@showSquad'));

			Route::post('get-grade-club-mode-listing','FixtureController@getTeamGrade');	

			Route::post('getTeam','FixtureController@getTeam');		
			Route::post('getGrade','FixtureController@getGrade');		
			/*Fixture routing finish here*/

			/*Locout for Game rep Admin*/
			Route::any('/lockout',array('as'=>'lockout.index','uses'=>'CommonController@addLockout'));
			Route::post('/force-unlock',array('as'=>'lockout.forceUnlock','uses'=>'CommonController@forceUnlock'));
			Route::any('/lockout-log',array('as'=>'lockout.lockoutLog','uses'=>'CommonController@lockoutLog'));
			/*Locout for Game rep Admin*/
			
			/*Availability routing start here*/
			Route::any('/availability',array('as'=>'availability.index','uses'=>'AvailabilityController@index'));
			Route::get('/availability/add-availability','AvailabilityController@addAvailability');
			Route::post('/availability/add-availability',array('as'=>'Availability.saveAvailability','uses'=>'AvailabilityController@saveAvailability'));
			Route::get('/availability/edit-availability/{id}','AvailabilityController@editAvailability');
			Route::get('/availability/view-availability/{id}','AvailabilityController@viewAvailability');	
			Route::post('/availability/edit-availability', array('as'=>'Availability.updateAvailability','uses'=>'AvailabilityController@updateAvailability'));
			Route::get('/availability/delete-availability/{id}','AvailabilityController@deleteAvailability');
			Route::get('availability/update-status/{id}/{status}',array('as'=>'Availability.status','uses'=>'AvailabilityController@updateStatus'));
			
			Route::post('get-club-playerlist','AvailabilityController@get_club_player_list'); 
			Route::post('get-club-mode-listing','AvailabilityController@getClubModeList');
			Route::post('get-club-mode-player','AvailabilityController@getClubPlayerList');

			/*Availability routing finish here*/
 
			/*SponsorController routing start here*/
			Route::any('/sponsor',array('as'=>'sponsor.index','uses'=>'SponsorController@index'));
			Route::get('/sponsor/add-sponsor','SponsorController@addSponsor');
			Route::post('/sponsor/add-sponsor',array('as'=>'sponsor.saveSponsor','uses'=>'SponsorController@saveSponsor'));
			Route::get('/sponsor/edit-sponsor/{id}','SponsorController@editSponsor');
			Route::get('/sponsor/view-sponsor/{id}','SponsorController@viewSponsor');	
			Route::post('/sponsor/edit-sponsor', array('as'=>'sponsor.updateSponsor','uses'=>'SponsorController@updateSponsor'));
			Route::get('/sponsor/delete-sponsor/{id}','SponsorController@deleteSponsor');
			Route::get('sponsor/update-status/{id}/{status}',array('as'=>'sponsor.status','uses'=>'SponsorController@updateStatus'));
			/*Sponsor routing finish here*/

			/*GamePointsController routing start here*/
			Route::any('/game-points',array('as'=>'GamePoints.index','uses'=>'GamePointsController@index'));
			Route::get('/game-points/edit-game-points/{id}','GamePointsController@editGamePoint');
			Route::post('/game-points/edit-game-points', array('as'=>'GamePoints.updateGamePoint','uses'=>'GamePointsController@updateGamePoint'));
			Route::get('game-points/update-status/{id}/{status}',array('as'=>'GamePoints.status','uses'=>'GamePointsController@updateStatus'));
			/*GamePointsController routing finish here*/

			/*GamePrizesController routing start here*/
			Route::any('/game-prizes',array('as'=>'GamePrize.index','uses'=>'GamePrizesController@index'));
			Route::get('/game-prizes/add-game-prize','GamePrizesController@addGamePrize');
			Route::post('/game-prizes/add-game-prize',array('as'=>'GamePrize.saveGamePrize','uses'=>'GamePrizesController@saveGamePrize'));
			Route::get('/game-prizes/edit-game-prize/{id}','GamePrizesController@editGamePrize');
			Route::post('/game-prizes/edit-game-prize', array('as'=>'GamePrize.updateGamePrize','uses'=>'GamePrizesController@updateGamePrize'));
			Route::get('/game-prizes/delete-game-prize/{id}','GamePrizesController@deleteGamePrize');
			Route::get('game-prizes/update-status/{id}/{status}',array('as'=>'GamePrize.status','uses'=>'GamePrizesController@updateStatus'));
			Route::any('/game-prizes/game-prizes-message/{id?}','GamePrizesController@gamePrizeMessage');

			Route::any('/admin-game-prizes',array('as'=>'GamePrize.adminIndex','uses'=>'GamePrizesController@adminGamePrizeListing'));
			Route::post('/admin-game-prize-get-game-name',array('as'=>'GamePrize.adminGetGameName','uses'=>'GamePrizesController@adminGetGameName'));
			/*GamePrizesController routing finish here*/



			/* payments route start */
			Route::any('/payments',array('as'=>'Payments.index','uses'=>'PaymentsController@index'));
			Route::get('/payments/add-payments','PaymentsController@addPayments');
			Route::post('/payments/add-payments',array('as'=>'Payments.savePayments','uses'=>'PaymentsController@savePayments'));
			Route::get('/payments/edit-payments/{id}','PaymentsController@editPayments');
			Route::post('/payments/edit-payments', array('as'=>'Payments.updatePayments','uses'=>'PaymentsController@updatePayments'));
			Route::get('/payments/delete-payments/{id}','PaymentsController@deletePayments');
			Route::get('payments/update-status/{id}/{status}',array('as'=>'Payments.status','uses'=>'PaymentsController@updateStatus'));
			/* payments route end */

			/* Premium start here */
			Route::get('/branding',array('as'=>'GamePrize.editBranding','uses'=>'CommonController@editBranding'));
			Route::post('branding','CommonController@updateBranding');
			Route::post('branding-update','CommonController@updateBrandingForm');
			Route::post('branding-update-new','CommonController@updateBrandingNewOne');

			Route::post('activate-game','CommonController@activateGame');
			Route::post('save-activate-game','CommonController@saveActivateGame');
			Route::post('active-game-payment','CommonController@activeGameSubmit')->name('activeGameSubmit');

			Route::get('show-activate-game',array('as'=>'Common.ShowActivateGame','uses'=>'CommonController@showActivateGame'));

			Route::get('book-intro-sesssion',array('as'=>'Common.bookIntroSession','uses'=>'CommonController@bookIntroSession'));
			Route::get('/help&support',array('as'=>'Common.helpSupport','uses'=>'CommonController@helpSupport'));

			Route::get('/admin-branding',array('as'=>'Common.adminbranding','uses'=>'CommonController@adminbranding'));
			Route::get('admin-edit-branding/{id}','CommonController@adminEditBranding');
			Route::post('admin-update-branding','CommonController@adminUpdateBranding');
			Route::get('admin-add-branding','CommonController@adminAddBranding');
			Route::post('admin-save-branding','CommonController@adminSaveBranding');

			/*Game Pay & activate section start here*/
			Route::get('admin-pay-activate',array('as'=>'Common.adminGameFee','uses'=>'CommonController@adminGameFee'));
			Route::get('admin-activate-game/{id}/{status}','CommonController@adminActivateGame');
			Route::any('/admin-pay-activate/admin-player-fee/{id}','CommonController@adminPlayerFee');
			/*Game Pay & activate section finish here*/


			/*adminlockout*/
			Route::get('admin-lockout',array('as'=>'lockout.adminIndex','uses'=>'CommonController@adminLockout'));
			Route::get('admin-edit-lockout/{id}',array('as'=>'lockout.adminEditLockout','uses'=>'CommonController@adminEditLockout'));
			Route::post('admin-update-lockout','CommonController@adminUpdateLockout')->name('updateAdminLockout');
			Route::get('admin-lockout-log',array('as'=>'lockout.adminLockoutLogs','uses'=>'CommonController@adminLockoutLogs'));
			/* Premium finsh here */

			/*Game setting*/
			Route::get('/game-setting',array('as'=>'GameSetting.index','uses'=>'CommonController@gameSetting'));
			Route::get('/save-stripe-connect','CommonController@saveGameSetting');
			Route::post('game-setting','CommonController@saveGameSetting');
			Route::post('save-other-game-setting','CommonController@saveOtherGameSetting');
			Route::any('/fundraiser-message','CommonController@fundraiserMessage');

			Route::get('/fundraiser-history',array('as'=>'Common.fundraiserHistory','uses'=>'CommonController@fundraiserHistory'));

			/*Admin fundraiser settings start here*/
			Route::get('/admin-fundraiser',array('as'=>'AdminFundraiser.index','uses'=>'AdminFundraiserController@index'));
			Route::get('/admin-fundraiser/edit-message/{id}','AdminFundraiserController@editMessage');
			Route::post('/admin-fundraiser/edit-message/{id}', array('as'=>'AdminFundraiser.updateMessage','uses'=>'AdminFundraiserController@updateMessage'));
			Route::get('/admin-fundraiser/edit-amount/{id}','AdminFundraiserController@editAmount');
			Route::post('admin-fundraiser/edit-amount/{id}', array('as'=>'AdminFundraiser.updateAmount','uses'=>'AdminFundraiserController@updateAmount'));

			Route::post('/admin-get-game-name',array('as'=>'AdminFundraiser.adminGetGameName','uses'=>'AdminFundraiserController@adminGetGameName'));
			/*Admin fundraiser settings finish here*/

			/*Game setting*/

			/*Additional Premium Player pack start here*/
			Route::any('/player-packs',array('as'=>'Common.playerPacks','uses'=>'CommonController@additionalPlayer'));
			Route::post('/add-more-player-pack','CommonController@addMorePlayerPacks');
			Route::post('/delete-add-more-player-pack','CommonController@deleteAddMorePlayerPacks');
			Route::any('/purchase-player-packs',array('as'=>'Common.purchasePlanPacks','uses'=>'CommonController@purchasePlanPacks'));

			Route::any('/stripe-payment',array('as'=>'Common.stripePayment','uses'=>'CommonController@stripePayment'));
			Route::any('/branding-payment',array('as'=>'Common.brandingPayment','uses'=>'CommonController@brandingPayment'));

			Route::any('/additional-player-logs',array('as'=>'Common.additionalPlayerLogs','uses'=>'CommonController@additionalPlayerLogs'));
			/*Additional Premium Player pack finish here*/
			Route::any('/list-grade', array('as'=>'Grade.listGrade','uses'=>'GradeController@listGrade')); 
			Route::any('/add-grade', array('as'=>'Grade.addGrade','uses'=>'GradeController@addGrade'));
			Route::any('/edit-grade/{id}', array('as'=>'Grade.editGrade','uses'=>'GradeController@editGrade')); 
			Route::get('/grade/delete-grade/{id}','GradeController@deleteGrade');
			Route::get('grade/update-status/{id}/{status}',array('as'=>'Grade.status','uses'=>'GradeController@updateStatus'));

			Route::any('/get-team-list-grade', array('as'=>'Grade.getTeamGrade','uses'=>'GradeController@getTeamGrade'));

			/*PlatformController routing start here*/
			Route::any('/platform',array('as'=>'platform.index','uses'=>'PlatformController@index'));
			Route::get('/platform/add-platform',array('as'=>'platform.addPlatform','uses'=>'PlatformController@addPlatform'));
			Route::post('/platform/add-platform',array('as'=>'platform.savePlatform','uses'=>'PlatformController@savePlatform'));
			Route::get('/platform/edit-platform/{id}','PlatformController@editPlatform');
			Route::get('/platform/view-platform/{id}','PlatformController@viewPlatform');	
			Route::post('/platform/edit-platform', array('as'=>'platform.updatePlatform','uses'=>'PlatformController@updatePlatform'));
			Route::get('/platform/delete-platform/{id}','PlatformController@deletePlatform');
			Route::get('platform/update-status/{id}/{status}',array('as'=>'platform.status','uses'=>'PlatformController@updateStatus'));
			/*PlatformController routing finish here*/

			/*ServeController routing start here*/
			Route::any('/serve',array('as'=>'serve.index','uses'=>'ServeController@index'));
			Route::get('/serve/add-serve',array('as'=>'serve.addServe','uses'=>'ServeController@addServe'));
			Route::post('/serve/add-serve',array('as'=>'serve.saveServe','uses'=>'ServeController@saveServe'));
			Route::get('/serve/edit-serve/{id}','ServeController@editServe');
			Route::get('/serve/view-serve/{id}','ServeController@viewServe');	
			Route::post('/serve/edit-serve', array('as'=>'serve.updateServe','uses'=>'ServeController@updateServe'));
			Route::get('/serve/delete-serve/{id}','ServeController@deleteServe');
			Route::get('serve/update-status/{id}/{status}',array('as'=>'serve.status','uses'=>'ServeController@updateStatus'));
			/*ServeController routing finish here*/

			/*PartnerController routing start here*/
			Route::any('/partner',array('as'=>'partner.index','uses'=>'PartnerController@index'));
			Route::get('/partner/add-partner',array('as'=>'partner.addPartner','uses'=>'PartnerController@addPartner'));
			Route::post('/partner/add-partner',array('as'=>'partner.savePartner','uses'=>'PartnerController@savePartner'));
			Route::get('/partner/edit-partner/{id}','PartnerController@editPartner');
			Route::get('/partner/view-partner/{id}','PartnerController@viewPartner');	
			Route::post('/partner/edit-partner', array('as'=>'partner.updatePartner','uses'=>'PartnerController@updatePartner'));
			Route::get('/partner/delete-partner/{id}','PartnerController@deletePartner');
			Route::get('partner/update-status/{id}/{status}',array('as'=>'partner.status','uses'=>'PartnerController@updateStatus'));
			/*PartnerController routing finish here*/

			/*UserSliderController routing start here*/
			Route::any('/user-slider',array('as'=>'userSlider.index','uses'=>'UserSliderController@index'));
			Route::get('/user-slider/add-user-slider',array('as'=>'userSlider.addUserSlider','uses'=>'UserSliderController@addUserSlider'));
			Route::post('/user-slider/add-user-slider',array('as'=>'userSlider.saveUserSlider','uses'=>'UserSliderController@saveUserSlider'));
			Route::get('/user-slider/edit-user-slider/{id}','UserSliderController@editUserSlider');
			Route::get('/user-slider/view-user-slider/{id}','UserSliderController@viewUserSlider');	
			Route::post('/user-slider/edit-user-slider', array('as'=>'userSlider.updateUserSlider','uses'=>'UserSliderController@updateUserSlider'));
			Route::get('/user-slider/delete-user-slider/{id}','UserSliderController@deleteUserSlider');
			Route::get('user-slider/update-status/{id}/{status}',array('as'=>'userSlider.status','uses'=>'UserSliderController@updateStatus'));
			/*UserSliderController routing finish here*/

			/*TeamSliderController routing start here*/
			Route::any('/team-slider',array('as'=>'TeamSlider.index','uses'=>'TeamSliderController@index'));
			Route::get('/team-slider/add-team-slider',array('as'=>'TeamSlider.addTeamSlider','uses'=>'TeamSliderController@addTeamSlider'));
			Route::post('/team-slider/add-team-slider',array('as'=>'TeamSlider.saveTeamSlider','uses'=>'TeamSliderController@saveTeamSlider'));
			Route::get('/team-slider/edit-team-slider/{id}','TeamSliderController@editTeamSlider');
			Route::get('/team-slider/view-team-slider/{id}','TeamSliderController@viewTeamSlider');	
			Route::post('/team-slider/edit-team-slider', array('as'=>'TeamSlider.updateTeamSlider','uses'=>'TeamSliderController@updateTeamSlider'));
			Route::get('/team-slider/delete-team-slider/{id}','TeamSliderController@deleteTeamSlider');
			Route::get('team-slider/update-status/{id}/{status}',array('as'=>'TeamSlider.status','uses'=>'TeamSliderController@updateStatus'));
			/*TeamSliderController routing finish here*/

			/*About Club routing started here*/
			Route::any('/about-club',array('as'=>'AboutClub.index','uses'=>'AboutClubController@index'));
			Route::get('/about-club/add-about-club',array('as'=>'AboutClub.addServe','uses'=>'AboutClubController@addAboutClub'));
			Route::post('/about-club/add-about-club',array('as'=>'AboutClub.saveClub','uses'=>'AboutClubController@saveAboutClub'));
			Route::get('/about-club/edit-about-club/{id}','AboutClubController@editAboutClub');
			Route::get('/about-club/view-about-club/{id}','AboutClubController@viewAboutClub');	
			Route::post('/about-club/edit-about-club', array('as'=>'AboutClub.updateClub','uses'=>'AboutClubController@updateAboutClub'));
			Route::get('/about-club/delete-about-club/{id}','AboutClubController@deleteAboutClub');
			Route::get('about-club/update-status/{id}/{status}',array('as'=>'AboutClub.status','uses'=>'AboutClubController@updateStatus'));
			/*About Club routing finish here*/

			/*Scorere Acesss routing start here*/
			Route::get('scorer-access',array('as'=>'scorerAccess.index','uses'=>'CommonController@scorerAccess'));
			Route::get('admin-scorer-access',array('as'=>'scorerAccess.adminIndex','uses'=>'CommonController@adminScorerAccess'));
			Route::post('save-scorer-access',array('as'=>'scorerAccess.saveScorerAccess','uses'=>'CommonController@saveScorerAccess'));
			Route::get('/delete-scorer-access/{id}','CommonController@deleteCommonController');
			Route::get('verify-users',array('as'=>'verifyUsers.verifyUsers','uses'=>'CommonController@verifyUsers'));
			Route::get('admin-verify-users',array('as'=>'adminVerifyUsers.adminVerifyUsers','uses'=>'CommonController@adminVerifyUsers'));
			Route::post('save-verify-users',array('as'=>'verifyUsers.saveVerifyUser','uses'=>'CommonController@saveVerifyUser'));
			Route::get('/delete-verify-users/{id}','CommonController@deleteVerifyUser');
			Route::post('/get-verify-users','CommonController@getVerifyUsersListings');
			Route::post('/get-verify-player-lists','CommonController@getVerifyPlayersListings');
			Route::post('/get-club-mode-verify-users','CommonController@getClubModeVerifyUserList');
			Route::post('admin-save-verify-users',array('as'=>'verifyUsers.adminSaveVerifyUser','uses'=>'CommonController@adminSaveVerifyUser'));

			Route::post('/get-scorer-users','CommonController@getScorerUsersListings');
			Route::post('/get-scorer-team-lists','CommonController@getScorerTeamListings');
			Route::post('admin-save-scorer-access',array('as'=>'scorerAccess.saveScorerAccess','uses'=>'CommonController@adminSaveScorerAccess'));
			/*Scorere Acesss routing finish here*/

			/** settings routing**/
			Route::any('/settings',array('as'=>'settings.listSetting','uses'=>'SettingsController@listSetting'));
			Route::get('/settings/add-setting','SettingsController@addSetting');
			Route::post('/settings/add-setting','SettingsController@saveSetting');
			Route::get('/settings/edit-setting/{id}','SettingsController@editSetting');
			Route::post('/settings/edit-setting/{id}','SettingsController@updateSetting');
			Route::get('/settings/prefix/{slug}','SettingsController@prefix');
			Route::post('/settings/prefix/{slug}','SettingsController@updatePrefix');
			Route::get('/settings/delete-setting/{id}','SettingsController@deleteSetting');
			/** settings routing**/
			
			/** cms-manager routing**/
			Route::any('/cms-manager',array('as'=>'Cms.index','uses'=>'CmspagesController@listCms'));
			Route::get('cms-manager/add-cms','CmspagesController@addCms');
			Route::post('cms-manager/add-cms','CmspagesController@saveCms');
			Route::get('cms-manager/edit-cms/{id}','CmspagesController@editCms');
			Route::post('cms-manager/edit-cms/{id}','CmspagesController@updateCms');
			Route::get('cms-manager/update-status/{id}/{status}','CmspagesController@updateCmsStatus');
			/** cms-manager routing**/
			
			/** email-manager routing**/
			Route::get('/email-manager',array('as'=>'EmailTemplate.index','uses'=>'EmailtemplateController@listTemplate'));
			Route::get('/email-manager/add-template',array('as'=>'EmailTemplate.add','uses'=>'EmailtemplateController@addTemplate'));
			Route::post('/email-manager/add-template','EmailtemplateController@saveTemplate');
			Route::get('/email-manager/edit-template/{id}',array('as'=>'EmailTemplate.edit','uses'=>'EmailtemplateController@editTemplate'));
			Route::post('/email-manager/edit-template/{id}','EmailtemplateController@updateTemplate');
			Route::post('/email-manager/get-constant','EmailtemplateController@getConstant');
			
			### Email Logs Manager routing
			Route::get('/email-logs',array('as'=>'EmailLogs.listEmail','uses'=>'EmailLogsController@listEmail'));
			Route::any('/email-logs/email_details/{id}','EmailLogsController@EmailDetail');
			/** email-manager routing**/
						
			/** Dropdown manager  module  routing start here **/
			Route::get('dropdown-manager/add-dropdown/{type}','DropDownController@addDropDown');
			Route::post('dropdown-manager/add-dropdown/{type}','DropDownController@saveDropDown');
			Route::get('dropdown-manager/edit-dropdown/{id}/{type}','DropDownController@editDropDown');
			Route::post('dropdown-manager/edit-dropdown/{id}/{type}','DropDownController@updateDropDown');
			Route::get('dropdown-manager/update-dropdown/{id}/{status}/{type}',array('as'=>'DropDown.status','uses'=>'DropDownController@updateDropDownStatus'));
			Route::get('dropdown-manager/delete-dropdown/{id}/{type}','DropDownController@deleteDropDown');
			Route::delete('dropdown-manager/delete-dropdown/{id}/{type}','DropDownController@deleteDropDown');
			Route::get('/dropdown-manager/{type}',array('as'=>'DropDown.listDropDown','uses'=>'DropDownController@listDropDown'));
			Route::get('/dropdown-manager/{type}/{isimage}',array('as'=>'DropDown.listDropDown','uses'=>'DropDownController@listDropDown'));
			Route::post('/dropdown-manager/{type}','DropDownController@listDropDown');
			/** Dropdown manager  module  routing start here **/		

			/** Dropdown manager  module  routing start here **/
			Route::get('slider-manager/add-slider/{type}','SliderController@addSlider');
			Route::post('slider-manager/add-slider/{type}','SliderController@saveSlider');
			Route::get('slider-manager/edit-slider/{id}/{type}','SliderController@editSlider');
			Route::post('slider-manager/edit-slider/{id}/{type}','SliderController@updateSlider');
			Route::get('slider-manager/update-slider/{id}/{status}/{type}',array('as'=>'Slider.status','uses'=>'SliderController@updateSliderStatus'));
			Route::get('slider-manager/delete-slider/{id}/{type}','SliderController@deleteSlider');
			Route::delete('slider-manager/delete-slider/{id}/{type}','SliderController@deleteSlider');
			Route::get('/slider-manager/{type}',array('as'=>'slider.listslider','uses'=>'SliderController@listSlider'));
			Route::get('/slider-manager/{type}/{isimage}',array('as'=>'slider.listslider','uses'=>'SliderController@listSlider'));
			Route::post('/slider-manager/{type}','SliderController@listSlider');
			/** Dropdown manager  module  routing start here **/

			# users routing start here //
			Route::get('/users/{role_id}',array('as'=>'Users.index','uses'=>'UsersController@listUsers'));
			Route::post('users/{role_id}','UsersController@listUsers');
			Route::get('users/add-user/{role_id}','UsersController@addUser');
			Route::post('users/add-user/{role_id}','UsersController@saveUser');	
			Route::get('users/edit-user/{id}','UsersController@editUser');
			Route::post('users/edit-user/{id}','UsersController@updateUser');	
			Route::get('users/view-user/{id}','UsersController@viewUser');
			Route::get('users/update-status/{role_id}/{id}/{status}','UsersController@updateUserStatus');
			Route::any('users/delete-user/{id}','UsersController@deleteUser');
			Route::get('users/verify-user/{id}/{status}','UsersController@verifiedUser');
			Route::any('users/send-credential/{id}','UsersController@sendCredential');	
			Route::any('/get-state','UsersController@getState');	
			Route::post('/get-city','UsersController@getCity');	
			Route::get('/users/user-refer/{user_id}',array('as'=>'Users.referList','uses'=>'UsersController@userReferList'));
			# users routing start here //

			### contact manager routing
			Route::any('/contact-manager',array('as'=>'Contact.index','uses'=>'ContactsController@listContact'));
			Route::get('contact-manager/view-contact/{id}',array('as'=>'Contact.view','uses'=>'ContactsController@viewContact'));
			Route::delete('contact-manager/delete-contact/{id}',array('as'=>'Contact.delete','uses'=>'ContactsController@deleteContact'));
			Route::any('/contact-manager/reply-to-user/{id}',array('as'=>'Contact.reply','uses'=>'ContactsController@replyToUser'));
			### contact manager routing

			### subscriber manager routing
			Route::any('/subscriber',array('as'=>'Subscribe.index','uses'=>'SubscriberController@listSubscribe'));
			Route::get('subscriber/view-subscriber/{id}',array('as'=>'Subscribe.view','uses'=>'SubscriberController@viewSubscribe'));
			Route::delete('subscriber-manager/delete-subscriber/{id}',array('as'=>'Subscribe.delete','uses'=>'SubscriberController@deleteSubscribe'));
			Route::any('/subscriber/reply-to-user/{id}',array('as'=>'Subscribe.reply','uses'=>'SubscriberController@replyToUser'));
			### subscriber manager routing 

			### subscriber manager routing
			Route::any('/user-notification','NotificationController@listing')->name('notification.listing');
			Route::get('/user-notification/add-notification','NotificationController@addNotification')->name('notification.addNotificaiton');
			Route::post('/user-notification/add-notification','NotificationController@saveNotification')->name('notification.saveNotification');
			Route::get('user-notification/edit-notification/{id}','NotificationController@editNotification')->name('notification.editNotification');
			Route::post('user-notification/edit-notification/{id}','NotificationController@updateNotification')->name('notification.updateNotification');
			Route::get('user-notification/view-notification/{id}',array('as'=>'notification.view','uses'=>'NotificationController@viewSubscribe'));
			Route::get('user-notification/delete-notification/{id}','NotificationController@deleteNotification')->name('notification.deleteNotification');
			Route::get('user-notification/send-email/{notification_id}','NotificationController@sendEmail')->name('notification.sendEmail');
			Route::post('user-notification/send-email/{notification_id}','NotificationController@sendnotificationEmail')->name('notification.sendnotificationEmail');
			### subscriber manager routing
			
			// language routing
			Route::get('language',array('as'=>'Language.index','uses'=>'LanguageController@listLanguage'));
			Route::get('language/add-language',array('as'=>'Language.add','uses'=>'LanguageController@addLanguage'));
			Route::post('language/save-language',array('as'=>'Language.save','uses'=>'LanguageController@saveLanguage'));
			Route::any('language/delete-language/{id}',array('as'=>'Language.delete','uses'=>'LanguageController@deleteLanguage'));
			Route::get('language/update-status/{id}/{status}',array('as'=>'Language.status','uses'=>'LanguageController@updateLanguageStatus'));
			Route::any('language/default/{id}/{langCode}/{folderCode}',array('as'=>'Language.update_default','uses'=>'LanguageController@updateDefaultLanguage'));
			Route::any('language/multiple-action',array('as'=>'Language.Multipleaction','uses'=>'LanguageController@performMultipleAction'));

			/*Newsletter routing start here*/
			Route::get('/news-letter','NewsLetterController@listTemplate');
			Route::post('/news-letter','NewsLetterController@listTemplate');
			Route::get('/news-letter/edit-template/{id}','NewsLetterController@editTemplate');
			Route::post('/news-letter/edit-template/{id}','NewsLetterController@updateTemplate');
			Route::get('/news-letter','NewsLetterController@listTemplate');
			Route::get('/news-letter',array('as'=>'NewsLetter.listTemplate','uses'=>'NewsLetterController@listTemplate'));
			Route::post('/news-letter','NewsLetterController@listTemplate');
			Route::get('/news-letter/edit-template/{id}','NewsLetterController@editTemplate');
			Route::post('/news-letter/edit-template/{id}','NewsLetterController@updateTemplate');
			Route::get('/news-letter/newsletter-templates',array('as'=>'NewsTemplates.newsletterTemplates','uses'=>'NewsLetterController@newsletterTemplates'));
			Route::get('/news-letter/add-template','NewsLetterController@addTemplates');
			Route::any('/news-letter/add-subscriber','NewsLetterController@addSubscriber');
			Route::post('/news-letter/add-template','NewsLetterController@saveTemplates');
			Route::get('/news-letter/edit-newsletter-templates/{id}','NewsLetterController@editNewsletterTemplate');
			Route::post('/news-letter/edit-newsletter-templates/{id}','NewsLetterController@updateNewsletterTemplate');
			Route::get('/news-letter/send-newsletter-templates/{id}','NewsLetterController@sendNewsletterTemplate');
			Route::post('/news-letter/send-newsletter-templates/{id}','NewsLetterController@updateSendNewsletterTemplate');
			Route::get('/news-letter/subscriber-list',array('as'=>'Subscriber.subscriberList','uses'=>'NewsLetterController@subscriberList'));
			Route::get('/news-letter/subscriber-active/{id}/{status}','NewsLetterController@subscriberActive');
			Route::any('news-letter/subscriber-delete/{id}','NewsLetterController@subscriberDelete');
			Route::any('news-letter/delete-template/{id}','NewsLetterController@templateDelete');
			Route::any('news-letter/view-subscriber/{id}','NewsLetterController@viewSubscrieber');
			Route::any('news-letter/delete-newsletter-template/{id}','NewsLetterController@deleteNewsTemplate');
			Route::post('news-letter/delete-multiple-subscriber','NewsLetterController@deleteMultipleSubscriber');
			/*Newsletter routing finish here*/

			/*Newsletter routing start here*/
			Route::get('/checklist',array('as'=>'checklist.index','uses'=>'ChecklistController@listChecklist'));
			Route::get('/checklist/add-checklist','ChecklistController@addChecklist');
			Route::get('/checklist/edit-checklist/{id}','ChecklistController@editChecklist');
			Route::post('/checklist/edit-checklist','ChecklistController@updateChecklist');
			Route::post('/checklist/add-checklist','ChecklistController@saveChecklist');
			Route::get('/checklist/view-checklist/{id}','ChecklistController@viewChecklist');
			Route::get('/checklist/delete-checklist/{id}','ChecklistController@deleteChecklist');
			Route::get('checklist/update-status/{id}/{status}',array('as'=>'checklist.status','uses'=>'ChecklistController@updateStatus'));

		});
	});

function prd($data){
	echo "<pre>";
	print_r($data);
	echo "</pre>";die;
}
function pr($data){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
}